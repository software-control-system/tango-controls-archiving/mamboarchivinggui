package fr.soleil.mambo.tools;

import javax.swing.JRadioButton;

import fr.soleil.mambo.api.db.DbConnectionParameters;

public class MamboDbUtils {

    private MamboDbUtils() {
        // hide constructor
    }

    public static void updateDbRadioButtons(JRadioButton hdbButton, JRadioButton tdbButton, JRadioButton ttsButton) {
        boolean hdbOk = DbConnectionParameters.isHdbAvailable();
        boolean tdbOk = DbConnectionParameters.isTdbAvailable();
        boolean ttsOk = DbConnectionParameters.isTtsAvailable();
        if (!ttsOk) {
            ttsButton.setEnabled(false);
            if (!hdbOk) {
                // neither TTS nor HDB: select TDB
                hdbButton.setEnabled(false);
                tdbButton.setSelected(true);
            } else if (!tdbOk) {
                // neither TTS nor TDB: select HDB
                tdbButton.setEnabled(false);
                hdbButton.setSelected(true);
            }
        } else if (!hdbOk) {
            hdbButton.setEnabled(false);
            if (!tdbOk) {
                // neither HDB nor TDB: select TTS
                tdbButton.setEnabled(false);
                ttsButton.setSelected(true);
            } else {
                // HDB not available: select TDB
                tdbButton.setSelected(true);
            }
        } else if (!tdbOk) {
            // TDB not available: select HDB
            tdbButton.setEnabled(false);
            ttsButton.setSelected(true);
        } else {
            // All DB available: select HDB by default
            hdbButton.setSelected(true);
        }
    }

    public static Boolean isHistoricAccordingToAvailableDb(boolean hdbOk, boolean tdbOk, boolean ttsOk) {
        Boolean historic;
        if (hdbOk) {
            historic = Boolean.TRUE;
        } else if (tdbOk) {
            historic = Boolean.FALSE;
        } else {
            historic = null;
        }
        return historic;
    }

    public static Boolean isHistoricAccordingToAvailableDb() {
        return isHistoricAccordingToAvailableDb(DbConnectionParameters.isHdbAvailable(),
                DbConnectionParameters.isTdbAvailable(), DbConnectionParameters.isTtsAvailable());
    }

}
