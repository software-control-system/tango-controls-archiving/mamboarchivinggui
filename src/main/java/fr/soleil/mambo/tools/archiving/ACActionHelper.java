package fr.soleil.mambo.tools.archiving;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.mambo.components.archiving.ACAttributesRecapTree;
import fr.soleil.mambo.containers.archiving.ArchivingPanel;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.tools.TreeUtils;

public class ACActionHelper {

    private ACActionHelper() {
        // Hidden to avoid inheritance
    }

    /**
     * Adds in a {@link Collection} all the nodes that represent an attribute, starting from a given node.
     * 
     * @param node The node from which to recover all attributes.
     * @param nodes The {@link Collection}.
     */
    protected static void addAttributeNode(DefaultMutableTreeNode node, Collection<DefaultMutableTreeNode> nodes) {
        if (node != null) {
            // According to ACTreeRenderer, a node is associated to an attribute when it is of level 4
            if (node.getLevel() == 4) {
                nodes.add(node);
            }
            for (int i = 0; i < node.getChildCount(); i++) {
                TreeNode child = node.getChildAt(i);
                if (child instanceof DefaultMutableTreeNode) {
                    addAttributeNode((DefaultMutableTreeNode) child, nodes);
                }
            }
        }
    }

    /**
     * Returns the {@link ArchivingConfigurationAttribute}s that are currently selected in AC.
     * 
     * @param ac The {@link ArchivingConfiguration} from which to recover the attributes.
     * @return An {@link ArchivingConfigurationAttribute} array, never <code>null</code>.
     */
    public static ArchivingConfigurationAttribute[] getSelectedAttributs(ArchivingConfiguration ac) {
        ArchivingConfigurationAttribute[] attributes = null;
        ACAttributesRecapTree tree = ACAttributesRecapTree.getInstance();
        if (tree != null) {
            // Step 1: recover selected attribute nodes
            TreePath[] selectionPaths = tree.getSelectionPaths();
            Collection<DefaultMutableTreeNode> nodes = new LinkedHashSet<DefaultMutableTreeNode>();
            if (selectionPaths != null) {
                for (TreePath path : selectionPaths) {
                    if (path != null) {
                        Object comp = path.getLastPathComponent();
                        if (comp instanceof DefaultMutableTreeNode) {
                            addAttributeNode((DefaultMutableTreeNode) comp, nodes);
                        }
                    }
                }
            }
            // Step 2: recover corresponding attributes
            if (ac != null) {
                Boolean historic = ac.isHistoric();
                Collection<ArchivingConfigurationAttribute> attributeList = new ArrayList<ArchivingConfigurationAttribute>();
                for (DefaultMutableTreeNode node : nodes) {
                    ArchivingConfigurationAttribute attribute = ac
                            .getAttribute(TreeUtils.getFullAttributeNameFromNode(tree, historic, node));
                    if (attribute != null) {
                        attributeList.add(attribute);
                    }
                }
                attributes = attributeList.toArray(new ArchivingConfigurationAttribute[attributeList.size()]);
            }
            nodes.clear();
        }
        if (attributes == null) {
            attributes = new ArchivingConfigurationAttribute[0];
        }
        return attributes;
    }

    /**
     * Returns the {@link Component}, recovered from an {@link ActionEvent}, to put as parameter of {@link JOptionPane}
     * methods.
     * 
     * @param e The {@link ActionEvent} from which to recover the {@link Component}.
     * @return A {@link Component}.
     */
    public static Component getJOptionPaneComponent(ActionEvent e) {
        Component c = (Component) e.getSource();
        Window w = WindowSwingUtils.getWindowForComponent(c);
        if ((w == null) || (!w.isVisible())) {
            c = ArchivingPanel.getInstance();
        }
        return c;
    }

}
