package fr.soleil.mambo.tools;

import java.awt.Component;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.containers.view.AbstractViewSpectrumPanel;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.sub.VCOptions;

/**
 * A class used to save a Spectrum in a {@link File}
 * 
 * @author girardot
 */
public class SpectrumDataWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpectrumDataWriter.class);

    protected final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    /**
     * Writes the {@link String} representation of a spectrum in a {@link File}
     * 
     * @param toSave The {@link File} in which to save the spectrum representation
     * @param readData The {@link DbData} that represents the Read value of the spectrum
     * @param writeData The {@link DbData} that represents the Write value of the spectrum
     * @param selectedRead A filter for read value
     * @param selectedWrite A filter for write value
     * @param spectrumType The expected type of representation
     * @param caller The {@link Component} that called this method. This is used to display warning popups in case of
     *            problems
     */
    public static void writeDataInFile(final File toSave, final DbData readData, final DbData writeData,
            final Collection<Integer> selectedRead, final Collection<Integer> selectedWrite, final int spectrumType,
            final AbstractViewSpectrumPanel caller) {
        final int indexLengthRead = computeLimit(readData, true);
        final int timeLengthRead = computeLimit(readData, false);
        final int indexLengthWrite = computeLimit(writeData, true);
        final int timeLengthWrite = computeLimit(writeData, false);
        final int maxColumnRead, maxColumnWrite;
        if (spectrumType == ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX) {
            maxColumnRead = indexLengthRead;
            maxColumnWrite = indexLengthWrite;
        } else {
            maxColumnRead = timeLengthRead;
            maxColumnWrite = timeLengthWrite;
        }

        if ((maxColumnRead > 0) || (maxColumnWrite > 0)) {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    String firstMessage = WaitingDialog.getFirstMessage();
                    boolean wasOpen = WaitingDialog.isInstanceVisible();
                    WaitingDialog.changeFirstMessage(Messages.getMessage("DIALOGS_FILE_WRITING"));
                    WaitingDialog.openInstance();
                    try {
                        FileWriter fw = new FileWriter(toSave);
                        Calendar calendar = Calendar.getInstance();
                        switch (spectrumType) {
                            case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX:
                                fw.write("Time (s)\t");
                                for (int i = 0; i < indexLengthRead; i++) {
                                    if ((selectedRead == null) || (selectedRead.contains(i))) {
                                        fw.write(Integer.toString(i + 1));
                                        fw.write(" ");
                                        fw.write(Messages.getMessage("VIEW_SPECTRUM_READ"));
                                        fw.write("\t");
                                    }
                                }
                                for (int i = 0; i < indexLengthWrite; i++) {
                                    if ((selectedWrite == null) || (selectedWrite.contains(i))) {
                                        fw.write(Integer.toString(i + 1));
                                        fw.write(" ");
                                        fw.write(Messages.getMessage("VIEW_SPECTRUM_WRITE"));
                                        fw.write("\t");
                                    }
                                }
                                break;
                            case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME:
                            case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME_STACK:
                                fw.write("Index\t");
                                for (int i = 0; i < timeLengthRead; i++) {
                                    if ((selectedRead == null) || (selectedRead.contains(i))) {
                                        calendar.setTimeInMillis(readData.getTimedData()[i].getTime());
                                        fw.write(DATE_FORMAT.format(calendar.getTime()));
                                        fw.write(" ");
                                        fw.write(Messages.getMessage("VIEW_SPECTRUM_READ"));
                                        fw.write("\t");
                                    }
                                }
                                for (int i = 0; i < timeLengthWrite; i++) {
                                    if ((selectedWrite == null) || (selectedWrite.contains(i))) {
                                        calendar.setTimeInMillis(writeData.getTimedData()[i].getTime());
                                        fw.write(DATE_FORMAT.format(calendar.getTime()));
                                        fw.write(" ");
                                        fw.write(Messages.getMessage("VIEW_SPECTRUM_WRITE"));
                                        fw.write("\t");
                                    }
                                }
                                break;
                        }
                        fw.write("\n");
                        fw.flush();

                        switch (spectrumType) {
                            case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX:
                                int timeIndexRead = 0;
                                int timeIndexWrite = 0;
                                while ((timeIndexRead < timeLengthRead) || (timeIndexWrite < timeIndexWrite)) {
                                    boolean canWrite = true;
                                    long minTime;
                                    if ((timeIndexRead < timeLengthRead) && (timeIndexWrite < timeIndexWrite)) {
                                        // Read and Write Values available
                                        if ((readData.getTimedData()[timeIndexRead].getTime() == 0)
                                                && (writeData.getTimedData()[timeIndexWrite].getTime() == 0)) {
                                            // Stupid case with no time: Ignore this
                                            // case
                                            canWrite = false;
                                            timeIndexRead++;
                                            timeIndexWrite++;
                                        } else if (readData.getTimedData()[timeIndexRead].getTime() == 0) {
                                            // Only Write value
                                            appendSecondsToBufferFromMilliseconds(
                                                    writeData.getTimedData()[timeIndexWrite].getTime(), fw);
                                            fillIndexBufferWithNoValueString(fw, indexLengthRead, selectedRead);
                                            appendIndexesToBuffer(fw, writeData, timeIndexWrite, indexLengthWrite,
                                                    selectedWrite);
                                            timeIndexRead++;
                                            timeIndexWrite++;
                                        } else if (writeData.getTimedData()[timeIndexWrite].getTime() == 0) {
                                            // Only Read value
                                            appendSecondsToBufferFromMilliseconds(
                                                    readData.getTimedData()[timeIndexRead].getTime(), fw);
                                            appendIndexesToBuffer(fw, readData, timeIndexRead, indexLengthRead,
                                                    selectedRead);
                                            fillIndexBufferWithNoValueString(fw, indexLengthWrite, selectedWrite);
                                            timeIndexRead++;
                                            timeIndexWrite++;
                                        } else {
                                            // Both Read and Write values are really
                                            // available
                                            minTime = Math.min(readData.getTimedData()[timeIndexRead].getTime(),
                                                    writeData.getTimedData()[timeIndexWrite].getTime());
                                            appendSecondsToBufferFromMilliseconds(minTime, fw);
                                            if (minTime == readData.getTimedData()[timeIndexRead].getTime()) {
                                                // minTime at least for Read value
                                                appendIndexesToBuffer(fw, readData, timeIndexRead, indexLengthRead,
                                                        selectedRead);
                                                timeIndexRead++;
                                                if (minTime == writeData.getTimedData()[timeIndexWrite].getTime()) {
                                                    // minTime for both Read and
                                                    // Write values
                                                    appendIndexesToBuffer(fw, writeData, timeIndexWrite,
                                                            indexLengthWrite, selectedWrite);
                                                    timeIndexWrite++;
                                                } else {
                                                    // minTime for Read value only
                                                    fillIndexBufferWithNoValueString(fw, indexLengthWrite,
                                                            selectedWrite);
                                                }
                                            } // end if (minTime ==
                                              // readData.getTimedData()[timeIndexRead].getTime().longValue())
                                            else {
                                                // minTime for Write value only
                                                fillIndexBufferWithNoValueString(fw, indexLengthRead, selectedRead);
                                                appendIndexesToBuffer(fw, writeData, timeIndexWrite, indexLengthWrite,
                                                        selectedWrite);
                                                timeIndexWrite++;
                                            }
                                        }
                                    } // end if ((timeIndexRead < timeLengthRead) &&
                                      // (timeIndexWrite < timeIndexWrite))
                                    else if (timeIndexRead < timeLengthRead) {
                                        // Only Read value
                                        appendSecondsToBufferFromMilliseconds(
                                                readData.getTimedData()[timeIndexRead].getTime(), fw);
                                        appendIndexesToBuffer(fw, readData, timeIndexRead, indexLengthRead,
                                                selectedRead);
                                        fillIndexBufferWithNoValueString(fw, indexLengthWrite, selectedWrite);
                                        timeIndexRead++;
                                    } // end else if (timeIndexRead <
                                      // timeLengthRead)
                                    else if (timeIndexWrite < timeIndexWrite) {
                                        // Only Write value
                                        appendSecondsToBufferFromMilliseconds(
                                                writeData.getTimedData()[timeIndexWrite].getTime(), fw);
                                        fillIndexBufferWithNoValueString(fw, indexLengthRead, selectedRead);
                                        appendIndexesToBuffer(fw, writeData, timeIndexWrite, indexLengthWrite,
                                                selectedWrite);
                                        timeIndexWrite++;
                                    } // end else if (timeIndexWrite <
                                      // timeIndexWrite)
                                    if (canWrite) {
                                        fw.write("\n");
                                        fw.flush();
                                    }
                                } // end while ((timeIndexRead < timeLengthRead) ||
                                  // (timeIndexWrite < timeIndexWrite))
                                break;
                            case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME:
                            case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME_STACK:
                                int maxIndexLimit = Math.max(indexLengthRead, indexLengthWrite);
                                for (int index = 0; index < maxIndexLimit; index++) {
                                    new StringBuilder();
                                    fw.write(Double.toString(index));
                                    fw.write("\t");
                                    appendTimesToBuffer(fw, readData, timeLengthRead, index, selectedRead);
                                    appendTimesToBuffer(fw, writeData, timeLengthWrite, index, selectedWrite);
                                    fw.write("\n");
                                    fw.flush();
                                }
                                break;
                        } // end switch (spectrumType)
                        fw.close();
                    } catch (Exception e) {
                        LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                        JOptionPane.showMessageDialog(caller,
                                Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_SAVING_ERROR") + e.getMessage());
                    } catch (OutOfMemoryError oome) {
                        caller.outOfMemoryErrorManagement();
                    } finally {
                        if (WaitingDialog.isInstanceVisible()) {
                            if (wasOpen) {
                                WaitingDialog.changeFirstMessage(firstMessage);
                            } else {
                                // Close WaitingDialog, even in case of error
                                WaitingDialog.closeInstance();
                            }
                        }
                    }
                } // end run()
            }); // end Runnable
        } // end if ((maxColumnRead > 0) || (maxColumnWrite > 0))
    }

    protected static int computeLimit(DbData data, boolean index) {
        int limit = 0;
        if (data != null) {
            if (index) {
                for (int timeIndex = 0; timeIndex < data.getTimedData().length; timeIndex++) {
                    if ((data.getTimedData()[timeIndex].getValue() != null)) {
                        int length = Array.getLength(data.getTimedData()[timeIndex].getValue());
                        if (length > limit) {
                            limit = length;
                        }
                    }
                }
            } else {
                limit = data.getTimedData().length;
            }
        }
        return limit;
    }

    /**
     * Fills all indexes with {@link VCOptions#getNoValueString()} for an index representation of spectrum, and writes
     * this String in a {@link FileWriter}.
     * 
     * @param fw the {@link FileWriter} to write.
     * @param maxIndex the maximum index
     * @param selectedIndexes the list of selected indexes
     * @return a {@link StringBuilder} containing the expected string
     * @throws IOException if a problem occurred while file writing
     */
    protected static void fillIndexBufferWithNoValueString(FileWriter fw, int maxIndex,
            Collection<Integer> selectedIndexes) throws IOException {
        if (fw != null) {
            for (int index = 0; index < maxIndex; index++) {
                if ((selectedIndexes == null) || (selectedIndexes.contains(index))) {
                    fw.write(Options.getInstance().getVcOptions().getNoValueString());
                    fw.write("\t");
                    fw.flush();
                }
            }
        }
    }

    /**
     * Writes the index representation of a spectrum at a particular time in a {@link FileWriter}
     * 
     * @param fw the {@link FileWriter}.
     * @param data the {@link DbData} containing the spectrum information
     * @param timeIndex the index in {@link DbData} time table that corresponds to the expected time
     * @param maxIndex the maximum index
     * @param selectedIndexes the list of selected indexes
     * @return a {@link StringBuilder} containing the expected string
     * @throws IOException if a problem occurred while file writing
     */
    protected static void appendIndexesToBuffer(FileWriter fw, DbData data, int timeIndex, int maxIndex,
            Collection<Integer> selectedIndexes) throws IOException {
        if (fw != null) {
            for (int index = 0; index < maxIndex; index++) {
                if ((selectedIndexes == null) || (selectedIndexes.contains(index))) {
                    if ((data.getTimedData()[timeIndex].getValue() != null)) {
                        int length = Array.getLength(data.getTimedData()[timeIndex].getValue());
                        if (index < length) {
                            fw.write(String.valueOf(Array.get(data.getTimedData()[timeIndex].getValue(), index)));
                        }
                    } else {
                        fw.write(Options.getInstance().getVcOptions().getNoValueString());
                    }
                    fw.write("\t");
                    fw.flush();
                }
            }
        }
    }

    /**
     * Writes the {@link String} representation of a time in seconds, based on its value in milliseconds, in a
     * {@link FileWriter}
     * 
     * @param milli the time in milliseconds
     * @param fw the {@link FileWriter}.
     * @return a {@link StringBuilder} containing the expected string
     * @throws IOException if a problem occurred while file writing
     */
    protected static void appendSecondsToBufferFromMilliseconds(long milli, FileWriter fw) throws IOException {
        if (fw != null) {
            long ts = milli / 1000;
            long ms = milli % 1000;

            if (ms == 0) {
                fw.write(Long.toString(ts));
                fw.write("\t");
            } else if (ms < 10) {
                fw.write(Long.toString(ts));
                fw.write(".00");
                fw.write(Long.toString(ms));
                fw.write("\t");
            } else if (ms < 100) {
                fw.write(Long.toString(ts));
                fw.write(".0");
                fw.write(Long.toString(ms));
                fw.write("\t");
            } else {
                fw.write(Long.toString(ts));
                fw.write(".");
                fw.write(Long.toString(ms));
                fw.write("\t");
            }
            fw.flush();
        }
    }

    /**
     * Writes the time representation of a spectrum at a particular index in a {@link FileWriter}
     * 
     * @param fw the {@link FileWriter}.
     * @param data the {@link DbData} containing the spectrum information
     * @param timeLength The supposed length of the time table. If the real length is less than this value, the missing
     *            elements are filled with {@link VCOptions#getNoValueString()}
     * @param currentIndex The index for which the {@link String} representation is desired
     * @param selectedTimes The list of desired indexes in time
     * @return a {@link StringBuilder} containing the expected string
     * @throws IOException if a problem occurred while file writing
     */
    protected static void appendTimesToBuffer(FileWriter fw, DbData data, int timeLength, int currentIndex,
            Collection<Integer> selectedTimes) throws IOException {
        if (fw != null) {
            for (int time = 0; time < timeLength; time++) {
                if ((data.getTimedData()[time].getValue() != null)
                        && (currentIndex < Array.getLength(data.getTimedData()[time].getValue()))
                        && ((selectedTimes == null) || (selectedTimes.contains(time)))) {
                    fw.write(String.valueOf(Array.get(data.getTimedData()[time].getValue(), currentIndex)));
                } else {
                    fw.write(Options.getInstance().getVcOptions().getNoValueString());
                }
                fw.write("\t");
                fw.flush();
            }
        }
    }

}
