package fr.soleil.mambo.tools.xmlhelpers.ac;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.options.manager.ACDefaultsManagerFactory;
import fr.soleil.mambo.options.manager.IACDefaultsManager;

public class ArchivingConfigurationXMLHelperWithForceExportPeriod extends ArchivingConfigurationXMLHelperStandard
        implements IArchivingConfigurationXMLHelper {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(ArchivingConfigurationXMLHelperWithForceExportPeriod.class);

    public ArchivingConfigurationXMLHelperWithForceExportPeriod() {
        super();
    }

    @Override
    public ArchivingConfiguration loadArchivingConfigurationIntoHash(String location) throws ArchivingException {
        return super.loadArchivingConfigurationIntoHash(location);
    }

    @Override
    public ArchivingConfiguration loadArchivingConfigurationIntoHashFromRoot(Node rootNode) throws ArchivingException {
        return super.loadArchivingConfigurationIntoHashFromRoot(rootNode);
    }

    @Override
    protected long defineTdbExportPeriodForCurrentAttributeModes(Map<String, String> DBmodeProperties) {
        long defaultExportValue;
        try {
            defaultExportValue = Long.parseLong(ACDefaultsManagerFactory.getCurrentImpl().getACDefaultsPropertyValue(
                    "TDB_EXPORT_PERIOD"));
        } catch (NumberFormatException e) {
            LOGGER.debug("NumberFormatException has been received during TDB_EXPORT_PERIOD reading");
            defaultExportValue = IACDefaultsManager.DEFAULT_TDB_EXPORT_PERIOD_WHEN_BAD_VALUE;
        }
        return defaultExportValue;
    }
}
