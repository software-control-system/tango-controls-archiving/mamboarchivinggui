package fr.soleil.mambo.tools.xmlhelpers;

import fr.soleil.archiving.gui.tools.GUIUtilities;

public interface XMLHelperUtils {

    public static Boolean stringToBoolean(String str) {
        Boolean result;
        if (str == null || "null".equalsIgnoreCase(str)) {
            result = null;
        } else {
            result = Boolean.valueOf(GUIUtilities.stringToBoolean(str));
        }
        return result;
    }

}
