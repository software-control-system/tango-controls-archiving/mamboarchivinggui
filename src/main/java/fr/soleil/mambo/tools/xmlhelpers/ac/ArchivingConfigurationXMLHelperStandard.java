package fr.soleil.mambo.tools.xmlhelpers.ac;

import java.io.File;
import java.sql.Timestamp;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.manager.XMLManager;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeHDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTTSProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributes;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationData;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.options.manager.IACDefaultsManager;
import fr.soleil.mambo.tools.xmlhelpers.XMLHelperUtils;

public class ArchivingConfigurationXMLHelperStandard {
    /**
     * @param location
     * @return 26 juil. 2005
     * @throws ArchivingException
     */
    public ArchivingConfiguration loadArchivingConfigurationIntoHash(String location) throws ArchivingException {
        File file = new File(location);
        Node rootNode;
        try {
            rootNode = XMLUtils.getRootNode(file);
        } catch (XMLWarning e) {
            throw XMLManager.generateFileArchivingException(location);
        }

        ArchivingConfiguration ret = loadArchivingConfigurationIntoHashFromRoot(rootNode);

        return ret;
    }

    /**
     * @param rootNode
     * @return 26 juil. 2005
     * @throws ArchivingException
     */
    public ArchivingConfiguration loadArchivingConfigurationIntoHashFromRoot(Node rootNode) throws ArchivingException {
        ArchivingConfiguration archConf = null;

        if (rootNode.hasChildNodes()) {
            NodeList attributesNodes = rootNode.getChildNodes();
            archConf = new ArchivingConfiguration();

            Map<String, String> acProperties = XMLUtils.loadAttributes(rootNode);
            try {
                String creationDate_s = acProperties.get(ArchivingConfigurationData.CREATION_DATE_PROPERTY_XML_TAG);
                String lastUpdateDate_s = acProperties
                        .get(ArchivingConfigurationData.LAST_UPDATE_DATE_PROPERTY_XML_TAG);
                String name_s = acProperties.get(ArchivingConfigurationData.NAME_PROPERTY_XML_TAG);
                String path_s = acProperties.get(ArchivingConfigurationData.PATH_PROPERTY_XML_TAG);
                String isModified_s = acProperties.get(ArchivingConfigurationData.IS_MODIFIED_PROPERTY_XML_TAG);
                String isHistoric_s = acProperties.get(ArchivingConfigurationData.IS_HISTORIC_PROPERTY_XML_TAG);

                Timestamp creationDate = Timestamp.valueOf(creationDate_s);
                Timestamp lastUpdateDate = Timestamp.valueOf(lastUpdateDate_s);
                boolean modified = GUIUtilities.stringToBoolean(isModified_s);
                Boolean historic = XMLHelperUtils.stringToBoolean(isHistoric_s);

                archConf.setCreationDate(creationDate);
                archConf.setLastUpdateDate(lastUpdateDate);
                archConf.setName(name_s);
                archConf.setPath(path_s);
                archConf.setModified(modified);
                archConf.setHistoric(historic);
            } catch (Exception e) {
                // do nothing (not obligatory fields)
            }

            ArchivingConfigurationAttributes archConfAttr = new ArchivingConfigurationAttributes();
            archConfAttr.setArchivingConfiguration(archConf);

            for (int i = 0; i < attributesNodes.getLength(); i++) {
                // as many loops as there are attributes in the AC
                Node currentAttributeNode = attributesNodes.item(i);
                if (XMLUtils.isAFakeNode(currentAttributeNode)) {
                    continue;
                }

                String currentAttributeType = currentAttributeNode.getNodeName().trim();// has
                                                                                        // to
                                                                                        // be
                                                                                        // "attribute"
                if (!currentAttributeType.equals(ArchivingConfigurationAttribute.XML_TAG)) {
                    throw new ArchivingException("Invalid xml file");
                }

                Map<String, String> attributeProperties = XMLUtils.loadAttributes(currentAttributeNode);
                String attributeCompleteName = attributeProperties
                        .get(ArchivingConfigurationAttribute.COMPLETE_NAME_PROPERTY_XML_TAG);
                ArchivingConfigurationAttribute currentAttribute = new ArchivingConfigurationAttribute();
                currentAttribute.setCompleteName(attributeCompleteName);
                currentAttribute.setGroup(archConfAttr);

                currentAttribute = loadCurrentAttributeModes(currentAttribute, currentAttributeNode);
                archConfAttr.addAttribute(currentAttribute);
            }

            archConf.setAttributes(archConfAttr);
        }

        return archConf;
    }

    /**
     * @param currentAttribute
     * @param currentAttributeNode
     * @return
     * @throws ArchivingException
     */
    private ArchivingConfigurationAttribute loadCurrentAttributeModes(ArchivingConfigurationAttribute currentAttribute,
            Node currentAttributeNode) throws ArchivingException {
        ArchivingConfigurationAttribute ret = currentAttribute;
        if (currentAttributeNode.hasChildNodes()) {
            NodeList attributeModesNodes = currentAttributeNode.getChildNodes();
            ArchivingConfigurationAttributeProperties currentAttributeProperties = new ArchivingConfigurationAttributeProperties();
            currentAttributeProperties.setAttribute(ret);

            for (int i = 0; i < attributeModesNodes.getLength(); i++) {
                // as many loops as there are modes types for this attributes (at most 2, HDBModes and TDBModes)
                Node currentModesNode = attributeModesNodes.item(i);
                if (XMLUtils.isAFakeNode(currentModesNode)) {
                    continue;
                }

                // has to be "HDBModes" or "TDBModes"
                String currentModesType = currentModesNode.getNodeName().trim();

                boolean currentModeIsHDB = currentModesType
                        .equals(ArchivingConfigurationAttributeHDBProperties.XML_TAG);
                boolean currentModeIsTDB = currentModesType
                        .equals(ArchivingConfigurationAttributeTDBProperties.XML_TAG);
                boolean currentModeIsTTS = currentModesType
                        .equals(ArchivingConfigurationAttributeTTSProperties.XML_TAG);
                Map<String, String> dbModeProperties = XMLUtils.loadAttributes(currentModesNode);

                if (currentModeIsTTS) {
                    ArchivingConfigurationAttributeTTSProperties ttsProperties = new ArchivingConfigurationAttributeTTSProperties();
                    String dedicatedArchiver_s = dbModeProperties
                            .get(ArchivingConfigurationAttributeDBProperties.DEDICATED_ARCHIVER_PROPERTY_XML_TAG);
                    if (dedicatedArchiver_s == null) {
                        dedicatedArchiver_s = ObjectUtils.EMPTY_STRING;
                    }
                    ttsProperties.setDedicatedArchiver(dedicatedArchiver_s);
                    ttsProperties = (ArchivingConfigurationAttributeTTSProperties) loadCurrentDBTypeModes(ttsProperties,
                            currentModesNode);
                    currentAttributeProperties.setTTSProperties(ttsProperties);
                } else if (currentModeIsHDB) {
                    ArchivingConfigurationAttributeHDBProperties hdbProperties = new ArchivingConfigurationAttributeHDBProperties();
                    String dedicatedArchiver_s = dbModeProperties
                            .get(ArchivingConfigurationAttributeDBProperties.DEDICATED_ARCHIVER_PROPERTY_XML_TAG);
                    if (dedicatedArchiver_s == null) {
                        dedicatedArchiver_s = ObjectUtils.EMPTY_STRING;
                    }
                    hdbProperties.setDedicatedArchiver(dedicatedArchiver_s);
                    hdbProperties = (ArchivingConfigurationAttributeHDBProperties) loadCurrentDBTypeModes(hdbProperties,
                            currentModesNode);
                    currentAttributeProperties.setHDBProperties(hdbProperties);
                } else if (currentModeIsTDB) {
                    ArchivingConfigurationAttributeTDBProperties tdbProperties = new ArchivingConfigurationAttributeTDBProperties();
                    tdbProperties.setExportPeriod(defineTdbExportPeriodForCurrentAttributeModes(dbModeProperties));
                    // TDBProperties.setKeepingPeriod( keepingPeriod );
                    String dedicatedArchiver_s = dbModeProperties
                            .get(ArchivingConfigurationAttributeDBProperties.DEDICATED_ARCHIVER_PROPERTY_XML_TAG);
                    if (dedicatedArchiver_s == null) {
                        dedicatedArchiver_s = ObjectUtils.EMPTY_STRING;
                    }
                    tdbProperties.setDedicatedArchiver(dedicatedArchiver_s);
                    tdbProperties = (ArchivingConfigurationAttributeTDBProperties) loadCurrentDBTypeModes(tdbProperties,
                            currentModesNode);
                    currentAttributeProperties.setTDBProperties(tdbProperties);
                } else {
                    throw new ArchivingException("Invalid xml file");
                }
            }

            ret.setProperties(currentAttributeProperties);
        }

        return ret;
    }

    protected long defineTdbExportPeriodForCurrentAttributeModes(Map<String, String> dbModeProperties) {
        String exportPeriod_s = dbModeProperties
                .get(ArchivingConfigurationAttributeTDBProperties.TDB_SPEC_EXPORT_PERIOD_XML_TAG);

        return (exportPeriod_s == null ? IACDefaultsManager.DEFAULT_TDB_EXPORT_PERIOD_WHEN_BAD_VALUE
                : Long.parseLong(exportPeriod_s));
    }

    /**
     * @param properties
     * @param currentModesNode
     * @return 26 juil. 2005
     * @throws ArchivingException
     */
    private ArchivingConfigurationAttributeDBProperties loadCurrentDBTypeModes(
            ArchivingConfigurationAttributeDBProperties properties, Node currentModesNode) throws ArchivingException {
        ArchivingConfigurationAttributeDBProperties ret = properties;

        if (currentModesNode.hasChildNodes()) {
            NodeList modeNodes = currentModesNode.getChildNodes();

            for (int i = 0; i < modeNodes.getLength(); i++) {
                // as many loops as there are mode types for the current mode type for the current attribute
                Node currentModeNode = modeNodes.item(i);
                if (XMLUtils.isAFakeNode(currentModeNode)) {
                    continue;
                }

                String currentModeType = currentModeNode.getNodeName().trim();// has
                                                                              // to
                                                                              // be
                                                                              // "modes"
                if (!currentModeType.equals(ArchivingConfigurationMode.XML_TAG)) {
                    throw new ArchivingException("Invalid xml file");
                }

                Map<String, String> modeProperties = XMLUtils.loadAttributes(currentModeNode);
                Mode mode = getMode(modeProperties);
                ArchivingConfigurationMode currentMode = new ArchivingConfigurationMode(mode);
                ret.addMode(currentMode);
            }
        }

        return ret;
    }

    /**
     * @param modeProperties
     * @return 26 juil. 2005
     */
    private Mode getMode(Map<String, String> modeProperties) {
        String type_s = modeProperties.get(ArchivingConfigurationMode.TYPE_PROPERTY_XML_TAG);
        String period_s = modeProperties.get(ArchivingConfigurationMode.PERIOD_PROPERTY_XML_TAG);
        int type = Integer.parseInt(type_s);
        int period = Integer.parseInt(period_s);

        Mode mode = new Mode();
        switch (type) {
            case ArchivingConfigurationMode.TYPE_A:
                String inf_s = modeProperties.get(ArchivingConfigurationMode.VAL_INF_PROPERTY_XML_TAG);
                String sup_s = modeProperties.get(ArchivingConfigurationMode.VAL_SUP_PROPERTY_XML_TAG);
                String is_ab_slow_drift = modeProperties.get(ArchivingConfigurationMode.VAL_ABS_SLOW_DRIFT_XML_TAG);
                double inf = Double.parseDouble(inf_s);
                double sup = Double.parseDouble(sup_s);
                boolean ab_slow_drift = Boolean.parseBoolean(is_ab_slow_drift);
                ModeAbsolu ModeA = new ModeAbsolu(period, inf, sup, ab_slow_drift);
                mode.setModeA(ModeA);
                break;

            case ArchivingConfigurationMode.TYPE_C:
                String range_s = modeProperties.get(ArchivingConfigurationMode.RANGE_PROPERTY_XML_TAG);
                String calculationType_s = modeProperties.get(ArchivingConfigurationMode.CALC_TYPE_PROPERTY_XML_TAG);
                int range = Integer.parseInt(range_s);
                int calculationType = Integer.parseInt(calculationType_s);
                ModeCalcul ModeC = new ModeCalcul(period, range, calculationType);
                mode.setModeC(ModeC);
                break;

            case ArchivingConfigurationMode.TYPE_R:
                String inf_s1 = modeProperties.get(ArchivingConfigurationMode.PERCENT_INF_PROPERTY_XML_TAG);
                String sup_s1 = modeProperties.get(ArchivingConfigurationMode.PERCENT_SUP_PROPERTY_XML_TAG);
                String is_rel_slow_drift = modeProperties.get(ArchivingConfigurationMode.VAL_REL_SLOW_DRIFT_XML_TAG);
                double inf1 = Double.parseDouble(inf_s1);
                double sup1 = Double.parseDouble(sup_s1);
                boolean rel_slow_drift = Boolean.parseBoolean(is_rel_slow_drift);
                ModeRelatif ModeR = new ModeRelatif(period, inf1, sup1, rel_slow_drift);
                mode.setModeR(ModeR);
                break;

            case ArchivingConfigurationMode.TYPE_T:
                String inf_s2 = modeProperties.get(ArchivingConfigurationMode.THRESHOLD_INF_PROPERTY_XML_TAG);
                String sup_s2 = modeProperties.get(ArchivingConfigurationMode.THRESHOLD_SUP_PROPERTY_XML_TAG);
                double inf2 = Double.parseDouble(inf_s2);
                double sup2 = Double.parseDouble(sup_s2);
                ModeSeuil ModeT = new ModeSeuil(period, inf2, sup2);
                mode.setModeT(ModeT);
                break;

            case ArchivingConfigurationMode.TYPE_D:
                ModeDifference ModeD = new ModeDifference(period);
                mode.setModeD(ModeD);
                break;

            case ArchivingConfigurationMode.TYPE_P:
                ModePeriode ModeP = new ModePeriode(period);
                mode.setModeP(ModeP);
                break;
        }

        return mode;
    }

}
