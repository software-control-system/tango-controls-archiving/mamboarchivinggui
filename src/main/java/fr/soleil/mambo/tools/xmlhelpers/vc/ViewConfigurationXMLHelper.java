package fr.soleil.mambo.tools.xmlhelpers.vc;

import java.awt.Color;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.manager.XMLManager;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.awt.util.FontTool;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;
import fr.soleil.mambo.data.view.ExpressionAttribute;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.data.view.ViewConfigurationAttributeProperties;
import fr.soleil.mambo.data.view.ViewConfigurationAttributes;
import fr.soleil.mambo.data.view.ViewConfigurationData;
import fr.soleil.mambo.data.view.plot.Bar;
import fr.soleil.mambo.data.view.plot.Curve;
import fr.soleil.mambo.data.view.plot.Interpolation;
import fr.soleil.mambo.data.view.plot.Marker;
import fr.soleil.mambo.data.view.plot.MathPlot;
import fr.soleil.mambo.data.view.plot.Polynomial2OrderTransform;
import fr.soleil.mambo.data.view.plot.Smoothing;
import fr.soleil.mambo.tools.AxisPropertiesUtils;
import fr.soleil.mambo.tools.ChartPropertiesUtils;
import fr.soleil.mambo.tools.xmlhelpers.XMLHelperUtils;

public class ViewConfigurationXMLHelper {

    private static AxisProperties loadAxis(Map<String, String> attributeProperties, int gridStyle) {
        AxisProperties ret = new AxisProperties();

        String position_s = attributeProperties.get(AxisPropertiesUtils.POSITION_PROPERTY_XML_TAG);
        String scaleMode_s = attributeProperties.get(AxisPropertiesUtils.SCALE_MODE_PROPERTY_XML_TAG);
        String labelFormat_s = attributeProperties.get(AxisPropertiesUtils.LABEL_FORMAT_PROPERTY_XML_TAG);
        String showSubGrid_s = attributeProperties.get(AxisPropertiesUtils.SHOW_SUB_GRID_PROPERTY_XML_TAG);
        String drawOpposite_s = attributeProperties.get(AxisPropertiesUtils.DRAW_OPPOSITE_PROPERTY_XML_TAG);
        String isVisible_s = attributeProperties.get(AxisPropertiesUtils.IS_VISIBLE_PROPERTY_XML_TAG);
        String autoScale_s = attributeProperties.get(AxisPropertiesUtils.AUTO_SCALE_PROPERTY_XML_TAG);
        String color_s = attributeProperties.get(AxisPropertiesUtils.COLOR_PROPERTY_XML_TAG);
        String scaleMax_s = attributeProperties.get(AxisPropertiesUtils.SCALE_MAX_PROPERTY_XML_TAG);
        String scaleMin_s = attributeProperties.get(AxisPropertiesUtils.SCALE_MIN_PROPERTY_XML_TAG);
        String title_s = attributeProperties.get(AxisPropertiesUtils.TITLE_PROPERTY_XML_TAG);
        String labelInterval_s = attributeProperties.get(AxisPropertiesUtils.LABEL_INTERVAL_XML_TAG);
        String gridVisible_s = attributeProperties.get(AxisPropertiesUtils.GRID_VISIBLE_XML_TAG);

        int position = GUIUtilities.stringToInt(position_s);
        int scaleMode = GUIUtilities.stringToInt(scaleMode_s);
        int labelFormat = GUIUtilities.stringToInt(labelFormat_s);
        double scaleMax = GUIUtilities.stringToDouble(scaleMax_s);
        double scaleMin = GUIUtilities.stringToDouble(scaleMin_s);
        boolean showSubGrid = GUIUtilities.stringToBoolean(showSubGrid_s);
        boolean drawOpposite = GUIUtilities.stringToBoolean(drawOpposite_s);
        boolean isVisible = GUIUtilities.stringToBoolean(isVisible_s);
        boolean autoScale = GUIUtilities.stringToBoolean(autoScale_s);
        Color color = GUIUtilities.stringToColor(color_s);
        double labelInterval = GUIUtilities.stringToDouble(labelInterval_s);
        boolean gribVisible = GUIUtilities.stringToBoolean(gridVisible_s);

        ret.setAutoScale(autoScale);
        ret.setColor(ColorTool.getCometeColor(color));
        ret.setDrawOpposite(drawOpposite);
        ret.setLabelFormat(labelFormat);
        ret.setPosition(position);
        ret.setScaleMax(scaleMax);
        ret.setScaleMin(scaleMin);
        ret.setScaleMode(scaleMode);
        ret.setSubGridVisible(showSubGrid);
        ret.setTitle(title_s);
        ret.setVisible(isVisible);
        ret.setUserLabelInterval(labelInterval);
        ret.setGridVisible(gribVisible);
        ret.setGridStyle(gridStyle);

        return ret;
    }

    private static Bar loadBar(Node currentSubNode) {
        Map<String, String> barProperties = XMLUtils.loadAttributes(currentSubNode);

        String fillColor_s = barProperties.get(Bar.COLOR_PROPERTY_XML_TAG);
        String fillingMethod_s = barProperties.get(Bar.FILL_METHOD_PROPERTY_XML_TAG);
        String fillStyle_s = barProperties.get(Bar.FILL_STYLE_PROPERTY_XML_TAG);
        String width_s = barProperties.get(Bar.WIDTH_PROPERTY_XML_TAG);

        int fillingMethod = GUIUtilities.stringToInt(fillingMethod_s);
        int fillStyle = GUIUtilities.stringToInt(fillStyle_s);
        int width = GUIUtilities.stringToInt(width_s);
        Color fillColor = GUIUtilities.stringToColor(fillColor_s);

        Bar bar = new Bar(ColorTool.getCometeColor(fillColor), width, fillStyle, fillingMethod);

        return bar;
    }

    private static ChartProperties loadChartProperties(Map<String, String> attributeProperties) {
        ChartProperties ret = new ChartProperties();

        String axisGrid_s = attributeProperties.get(ChartPropertiesUtils.AXIS_GRID_PROPERTY_XML_TAG);
        String axisGridStyle_s = attributeProperties.get(ChartPropertiesUtils.AXIS_GRID_STYLE_PROPERTY_XML_TAG);
        String backgroundColor_s = attributeProperties.get(ChartPropertiesUtils.BACKGROUND_COLOR_PROPERTY_XML_TAG);
        String displayDuration_s = attributeProperties.get(ChartPropertiesUtils.DISPLAY_DURATION_PROPERTY_XML_TAG);
        String headerFont_s = attributeProperties.get(ChartPropertiesUtils.HEADER_FONT_PROPERTY_XML_TAG);
        String labelFont_s = attributeProperties.get(ChartPropertiesUtils.LABEL_FONT_PROPERTY_XML_TAG);
        String legendVisible_s = attributeProperties.get(ChartPropertiesUtils.LEGENDS_ARE_VISIBLE_PROPERTY_XML_TAG);
        String legendsPlacement_s = attributeProperties.get(ChartPropertiesUtils.LEGENDS_PLACEMENT_PROPERTY_XML_TAG);
        String title_s = attributeProperties.get(ChartPropertiesUtils.TITLE_PROPERTY_XML_TAG);
        String timePrecision_s = attributeProperties.get(ChartPropertiesUtils.TIME_PRECISION_XML_TAG);
        String autoHighLightOnLegend_s = attributeProperties.get(ChartPropertiesUtils.AUTO_HIGHLIGHT_ON_LEGEND_XML_TAG);
        if (autoHighLightOnLegend_s == null || autoHighLightOnLegend_s.trim().isEmpty()) {
            autoHighLightOnLegend_s = Boolean.toString(true);
        }
        if (timePrecision_s == null) {
            timePrecision_s = "0";
        }

        int axisGrid = GUIUtilities.stringToInt(axisGrid_s);
        int axisGridStyle = GUIUtilities.stringToInt(axisGridStyle_s);
        int legendsPlacement = GUIUtilities.stringToInt(legendsPlacement_s);
        CometeColor backgroundColor = ColorTool.getCometeColor(GUIUtilities.stringToColor(backgroundColor_s));
        boolean autoHighLightOnLegend = GUIUtilities.stringToBoolean(autoHighLightOnLegend_s);
        boolean legendVisible = GUIUtilities.stringToBoolean(legendVisible_s);
        double displayDuration = GUIUtilities.stringToDouble(displayDuration_s);
        CometeFont headerFont = FontTool.getCometeFont(GUIUtilities.stringToFont(headerFont_s));
        CometeFont labelFont = FontTool.getCometeFont(GUIUtilities.stringToFont(labelFont_s));

        ret.setTitle(title_s);
        ChartPropertiesUtils.setAxisGrid(axisGrid, ret);
        ret.getXAxisProperties().setGridStyle(axisGridStyle);
        ret.getY1AxisProperties().setGridStyle(axisGridStyle);
        ret.getY2AxisProperties().setGridStyle(axisGridStyle);
        ret.setLegendPlacement(legendsPlacement);
        ret.setAutoHighlightOnLegend(autoHighLightOnLegend);
        ret.setBackgroundColor(backgroundColor);
        ret.setLegendVisible(legendVisible);
        ret.setDisplayDuration(displayDuration);
        ret.setHeaderFont(headerFont);
        ret.setLabelFont(labelFont);
        try {
            ret.setTimePrecision(Integer.parseInt(timePrecision_s));
        } catch (NumberFormatException e) {
            ret.setTimePrecision(0);
        }

        return ret;
    }

    private static ViewConfigurationAttribute loadCurrentAttribute(ViewConfigurationAttribute currentAttribute,
            Node currentAttributeNode) throws ArchivingException {
        ViewConfigurationAttribute ret = currentAttribute;

        if (currentAttributeNode.hasChildNodes()) {
            NodeList attributeParamsNodes = currentAttributeNode.getChildNodes();
            ViewConfigurationAttributeProperties currentAttributeProperties = new ViewConfigurationAttributeProperties();

            // as many loops as there are modes types for this attributes (at most 2, plotParams and extractParams )
            for (int i = 0; i < attributeParamsNodes.getLength(); i++) {
                Node currentAttributeParamNode = attributeParamsNodes.item(i);
                if (XMLUtils.isAFakeNode(currentAttributeParamNode)) {
                    continue;
                }

                // has to be "plotParams" or "extractParams"
                String currentAttributeParamType = currentAttributeParamNode.getNodeName().trim();
                boolean currentParamsIsPlot = currentAttributeParamType
                        .equals(ViewConfigurationAttributePlotProperties.XML_TAG);
                if (currentParamsIsPlot) {
                    ViewConfigurationAttributePlotProperties plotProperties = loadPlotParams(currentAttributeParamNode);
                    currentAttributeProperties.setPlotProperties(plotProperties);
                } else {
                    continue;
                }
            }
            ret.setProperties(currentAttributeProperties);
        }
        return ret;
    }

    private static Curve loadCurve(Node currentSubNode) {
        Curve curve = new Curve();
        Map<String, String> curveProperties = XMLUtils.loadAttributes(currentSubNode);

        String color_s = curveProperties.get(Curve.COLOR_PROPERTY_XML_TAG);
        String style_s = curveProperties.get(Curve.STYLE_PROPERTY_XML_TAG);
        String width_s = curveProperties.get(Curve.WIDTH_PROPERTY_XML_TAG);

        if (curveProperties.containsKey(Curve.NAME_PROPERTY_XML_TAG)) {
            String name_s = curveProperties.get(Curve.NAME_PROPERTY_XML_TAG);
            curve.setName(name_s);
        }

        Color fillColor = GUIUtilities.stringToColor(color_s);
        int width = GUIUtilities.stringToInt(width_s);
        int style = GUIUtilities.stringToInt(style_s);

        curve.setColor(ColorTool.getCometeColor(fillColor));
        curve.setLineStyle(style);
        curve.setWidth(width);

        return curve;
    }

    /**
     * Load expression's informations from the xml node
     * 
     * @param expressionNode
     *            the expression node
     * @return a new ExpressionAttribute filled with xml informations
     * @throws Exception
     *             if there is an unknown child node
     */
    private static ExpressionAttribute loadExpression(Node expressionNode) throws ArchivingException {

        // look for node's attributes
        Map<String, String> expressionProperties = XMLUtils.loadAttributes(expressionNode);

        String name = expressionProperties.get(ExpressionAttribute.NAME_PROPERTY_XML_TAG);
        String id = expressionProperties.get(ExpressionAttribute.ID_PROPERTY_XML_TAG);
        String expression = expressionProperties.get(ExpressionAttribute.VALUE_PROPERTY_XML_TAG);

        String factorString = expressionProperties.get(ExpressionAttribute.FACTOR_PROPERTY_XML_TAG);
        if (factorString == null) {
            factorString = "1";
        }
        Double factor = Double.valueOf(factorString);

        String variablesAttr = expressionProperties.get(ExpressionAttribute.VARIABLES_PROPERTY_XML_TAG);
        List<String> variables = new ArrayList<String>(3);
        StringTokenizer st = new StringTokenizer(variablesAttr, ExpressionAttribute.VARIABLES_PROPERTY_DELIMITER);
        while (st.hasMoreTokens()) {
            // TANGOARCH-917: id to lower case
            String var = st.nextToken().trim();
            if (var != null && (var.endsWith(ViewConfigurationAttribute.SUFFIX_READ)
                    || var.endsWith(ViewConfigurationAttribute.SUFFIX_WRITE))) {
                var = var.toLowerCase();
            }
            variables.add(var);
        }

        String xAttr = expressionProperties.get(ExpressionAttribute.X_PROPERTY_XML_TAG);
        Boolean isX = new Boolean(xAttr);

        // look for node's children
        ViewConfigurationAttributePlotProperties plotProperties = null;

        if (expressionNode.hasChildNodes()) {
            NodeList expressionChildren = expressionNode.getChildNodes();
            for (int j = 0; j < expressionChildren.getLength(); j++) {
                Node currentChild = expressionChildren.item(j);
                if (XMLUtils.isAFakeNode(currentChild)) {
                    continue;
                }
                if (ViewConfigurationAttributePlotProperties.XML_TAG.equals(currentChild.getNodeName())) {
                    plotProperties = loadPlotParams(currentChild);
                } else {
                    continue;
                }
            }
        }
        if (plotProperties.getCurve().getName().isEmpty()) {
            plotProperties.getCurve().setName(name);
        }

        ExpressionAttribute currentExpression = new ExpressionAttribute(name, expression,
                variables.toArray(new String[0]), isX.booleanValue());
        if (id != null) {
            currentExpression.setId(id);
        } else {
            currentExpression.setId(name);
        }
        currentExpression.setFactor(factor.doubleValue());
        currentExpression.setProperties(plotProperties);

        return currentExpression;
    }

    private static ViewConfigurationData loadGenericPlotParameters(ViewConfigurationData viewDataIn,
            Node genericPlotParametersNode) throws ArchivingException {
        if (genericPlotParametersNode.hasChildNodes()) {
            NodeList genericPlotParametersSubNodes = genericPlotParametersNode.getChildNodes();
            int axisGrid = 0;
            int gridStyle = 0;
            // as many loops as there are sub nodes types in a genericPlotParameters node (3)
            for (int i = 0; i < genericPlotParametersSubNodes.getLength(); i++) {
                Node currentSubNode = genericPlotParametersSubNodes.item(i);
                if (XMLUtils.isAFakeNode(currentSubNode)) {
                    continue;
                }

                // has to be "mainParams" or "Y1Axis" or "Y2Axis"
                String currentSubNodeType = currentSubNode.getNodeName().trim();

                Map<String, String> attributeProperties = XMLUtils.loadAttributes(currentSubNode);

                if (currentSubNodeType.equals(ChartPropertiesUtils.XML_TAG)) {
                    ChartProperties chartProperties = loadChartProperties(attributeProperties);
                    axisGrid = ChartPropertiesUtils.getAxisGrid(chartProperties);
                    gridStyle = chartProperties.getXAxisProperties().getGridStyle();
                    viewDataIn.setChartProperties(chartProperties);
                } else if (currentSubNodeType.equals(AxisPropertiesUtils.XML_TAG_1)) {
                    AxisProperties y1Axis = loadAxis(attributeProperties, gridStyle);
                    viewDataIn.getChartProperties().setY1AxisProperties(y1Axis);
                } else if (currentSubNodeType.equals(AxisPropertiesUtils.XML_TAG_2)) {
                    AxisProperties y2Axis = loadAxis(attributeProperties, gridStyle);
                    viewDataIn.getChartProperties().setY2AxisProperties(y2Axis);
                } else if (currentSubNodeType.equals(AxisPropertiesUtils.XML_TAG_3)) {
                    AxisProperties xAxis = loadAxis(attributeProperties, gridStyle);
                    viewDataIn.getChartProperties().setXAxisProperties(xAxis);
                } else {
                    // ignore unknown params
                    continue;
                }
            }
            // To maintain compatibility with previous versions (< lot6)
            ChartPropertiesUtils.setAxisGrid(axisGrid, viewDataIn.getChartProperties());
        }
        return viewDataIn;
    }

    private static Interpolation loadInterpolation(Node currentSubNode) {
        Interpolation interpolation = new Interpolation();
        Map<String, String> interpolationProperties = XMLUtils.loadAttributes(currentSubNode);

        String hermliteBias_s = interpolationProperties.get(Interpolation.HERMITE_BIAS_PROPERTY_XML_TAG);
        String hermiteTension_s = interpolationProperties.get(Interpolation.HERMITE_TENSION_PROPERTY_XML_TAG);
        String interpolationMethod_s = interpolationProperties.get(Interpolation.INTERPOLATION_METHOD_PROPERTY_XML_TAG);
        String InterpolationStep_s = interpolationProperties.get(Interpolation.INTERPOLATION_STEP_PROPERTY_XML_TAG);

        double hermliteBias = GUIUtilities.stringToDouble(hermliteBias_s);
        double hermiteTension = GUIUtilities.stringToDouble(hermiteTension_s);
        int interpolationMethod = GUIUtilities.stringToInt(interpolationMethod_s);
        int InterpolationStep = GUIUtilities.stringToInt(InterpolationStep_s);

        interpolation.setHermiteBias(hermliteBias);
        interpolation.setHermiteTension(hermiteTension);
        interpolation.setInterpolationMethod(interpolationMethod);
        interpolation.setInterpolationStep(InterpolationStep);

        return interpolation;
    }

    private static Marker loadMarker(Node currentSubNode) {
        Marker marker = new Marker();
        Map<String, String> markerProperties = XMLUtils.loadAttributes(currentSubNode);

        String color_s = markerProperties.get(Marker.COLOR_PROPERTY_XML_TAG);
        String size_s = markerProperties.get(Marker.SIZE_PROPERTY_XML_TAG);
        String style_s = markerProperties.get(Marker.STYLE_PROPERTY_XML_TAG);

        Color color = GUIUtilities.stringToColor(color_s);
        int style = GUIUtilities.stringToInt(style_s);
        int size = GUIUtilities.stringToInt(size_s);

        marker.setColor(ColorTool.getCometeColor(color));
        marker.setLegendVisible(true);
        marker.setSize(size);
        marker.setStyle(style);

        return marker;
    }

    private static MathPlot loadMathPlot(Node currentSubNode) {
        MathPlot mathPlot = new MathPlot();
        Map<String, String> mathProperties = XMLUtils.loadAttributes(currentSubNode);
        String function_s = mathProperties.get(MathPlot.FUNCTION_PROPERTY_XML_TAG);
        int function = GUIUtilities.stringToInt(function_s);
        mathPlot.setFunction(function);
        return mathPlot;
    }

    private static ViewConfigurationAttributePlotProperties loadPlotParams(Node currentNode) {
        ViewConfigurationAttributePlotProperties ret = new ViewConfigurationAttributePlotProperties();
        Map<String, String> hash = XMLUtils.loadAttributes(currentNode);

        String viewType_s = hash.get(ViewConfigurationAttributePlotProperties.VIEW_TYPE_PROPERTY_XML_TAG);
        String axisChoice_s = hash.get(ViewConfigurationAttributePlotProperties.AXIS_CHOICE_PROPERTY_XML_TAG);
        String spectrumViewType_s = hash
                .get(ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_PROPERTY_XML_TAG);
        String hidden_s = hash.get(ViewConfigurationAttributePlotProperties.HIDDEN_PROPERTY_XML_TAG);

        if (viewType_s != null) {
            int viewType = Integer.parseInt(viewType_s);
            ret.setViewType(viewType);
        }
        if (axisChoice_s != null) {
            int axisChoice = Integer.parseInt(axisChoice_s);
            ret.setAxisChoice(axisChoice);
        }
        if (spectrumViewType_s != null) {
            int spectrumViewType = Integer.parseInt(spectrumViewType_s);
            ret.setSpectrumViewType(spectrumViewType);
        }
        if (hidden_s != null) {
            boolean hidden = "true".equalsIgnoreCase(hidden_s.trim());
            ret.setHidden(hidden);
        }

        if (currentNode.hasChildNodes()) {
            NodeList subNodes = currentNode.getChildNodes();

            // as many loops as there are sub nodes types for a plotParams Node (4)
            for (int i = 0; i < subNodes.getLength(); i++) {
                Node currentSubNode = subNodes.item(i);
                if (XMLUtils.isAFakeNode(currentSubNode)) {
                    continue;
                }

                String currentSubNodeType = currentSubNode.getNodeName().trim();// has

                if (currentSubNodeType.equals(Bar.XML_TAG)) {
                    Bar bar = loadBar(currentSubNode);
                    ret.setBar(bar);
                } else if (currentSubNodeType.equals(Curve.XML_TAG)) {
                    Curve curve = loadCurve(currentSubNode);
                    ret.setCurve(curve);
                } else if (currentSubNodeType.equals(Marker.XML_TAG)) {
                    Marker marker = loadMarker(currentSubNode);
                    ret.setMarker(marker);
                } else if (currentSubNodeType.equals(Polynomial2OrderTransform.XML_TAG)) {
                    Polynomial2OrderTransform transform = loadTransform(currentSubNode);
                    ret.setTransform(transform);
                } else if (currentSubNodeType.equals(Interpolation.XML_TAG)) {
                    Interpolation interpolation = loadInterpolation(currentSubNode);
                    ret.setInterpolation(interpolation);
                } else if (currentSubNodeType.equals(Smoothing.XML_TAG)) {
                    Smoothing smoothing = loadSmoothing(currentSubNode);
                    ret.setSmoothing(smoothing);
                } else if (currentSubNodeType.equals(MathPlot.XML_TAG)) {
                    MathPlot mathPlot = loadMathPlot(currentSubNode);
                    ret.setMath(mathPlot);
                } else {
                    // ignore unknown params
                    continue;
                }
            }
        }
        return ret;
    }

    private static Smoothing loadSmoothing(Node currentSubNode) {
        Smoothing smoothing = new Smoothing();
        Map<String, String> smoothingProperties = XMLUtils.loadAttributes(currentSubNode);

        String extrapolation_s = smoothingProperties.get(Smoothing.EXTRAPOLATION_PROPERTY_XML_TAG);
        String gaussSigma_s = smoothingProperties.get(Smoothing.GAUSS_SIGMA_PROPERTY_XML_TAG);
        String Method_s = smoothingProperties.get(Smoothing.METHOD_PROPERTY_XML_TAG);
        String neighbors_s = smoothingProperties.get(Smoothing.NEIGHBORS_PROPERTY_XML_TAG);

        int extrapolation = GUIUtilities.stringToInt(extrapolation_s);
        double smoothingGaussSigma = GUIUtilities.stringToDouble(gaussSigma_s);
        int smoothingMethod = GUIUtilities.stringToInt(Method_s);
        int smoothingNeighbors = GUIUtilities.stringToInt(neighbors_s);

        smoothing.setExtrapolation(extrapolation);
        smoothing.setGaussSigma(smoothingGaussSigma);
        smoothing.setMethod(smoothingMethod);
        smoothing.setNeighbors(smoothingNeighbors);

        return smoothing;
    }

    private static Polynomial2OrderTransform loadTransform(Node currentSubNode) {
        Polynomial2OrderTransform transform = new Polynomial2OrderTransform();
        Map<String, String> transformProperties = XMLUtils.loadAttributes(currentSubNode);

        String a0_s = transformProperties.get(Polynomial2OrderTransform.A0_PROPERTY_XML_TAG);
        String a1_s = transformProperties.get(Polynomial2OrderTransform.A1_PROPERTY_XML_TAG);
        String a2_s = transformProperties.get(Polynomial2OrderTransform.A2_PROPERTY_XML_TAG);

        double a0 = GUIUtilities.stringToDouble(a0_s);
        double a1 = GUIUtilities.stringToDouble(a1_s);
        double a2 = GUIUtilities.stringToDouble(a2_s);

        // To maintain compatibility with previous versions (< lot6)
        if (Double.isNaN(a0)) {
            a0 = 0;
        }
        if (Double.isNaN(a1)) {
            a1 = 1;
        }
        if (Double.isNaN(a2)) {
            a2 = 0;
        }
        transform.setA0(a0);
        transform.setA1(a1);
        transform.setA2(a2);

        return transform;
    }

    private static ViewConfiguration loadViewConfigurationData(Node rootNode) throws ArchivingException {
        ViewConfiguration viewConf = new ViewConfiguration();
        ViewConfigurationData viewConfData = new ViewConfigurationData();

        Map<String, String> vcProperties = XMLUtils.loadAttributes(rootNode);

        String creationDate_s = vcProperties.get(ViewConfigurationData.CREATION_DATE_PROPERTY_XML_TAG);
        String lastUpdateDate_s = vcProperties.get(ViewConfigurationData.LAST_UPDATE_DATE_PROPERTY_XML_TAG);
        String startDate_s = vcProperties.get(ViewConfigurationData.START_DATE_PROPERTY_XML_TAG);
        String endDate_s = vcProperties.get(ViewConfigurationData.END_DATE_PROPERTY_XML_TAG);
        String historic_s = vcProperties.get(ViewConfigurationData.HISTORIC_PROPERTY_XML_TAG);
        String longTerm_s = vcProperties.get(ViewConfigurationData.LONG_TERM_PROPERTY_XML_TAG);
        String name_s = vcProperties.get(ViewConfigurationData.NAME_PROPERTY_XML_TAG);
        String path_s = vcProperties.get(ViewConfigurationData.PATH_PROPERTY_XML_TAG);
        String isModified_s = vcProperties.get(ViewConfigurationData.IS_MODIFIED_PROPERTY_XML_TAG);
        String samplingType_s = vcProperties.get(ViewConfigurationData.SAMPLING_TYPE_PROPERTY_XML_TAG);
        String samplingFactor_s = vcProperties.get(ViewConfigurationData.SAMPLING_FACTOR_PROPERTY_XML_TAG);

        viewConfData.setName(name_s);
        viewConfData.setPath(path_s);

        Boolean historic = XMLHelperUtils.stringToBoolean(historic_s);
        viewConfData.setHistoric(historic);

        boolean longTerm = GUIUtilities.stringToBoolean(longTerm_s);
        viewConfData.setLongTerm(longTerm);

        boolean isModified = GUIUtilities.stringToBoolean(isModified_s);
        viewConf.setModified(isModified);

        boolean dynamicDateRange = ViewConfigurationData.DEFAULT_DYNAMIC_DATE_RANGE;
        String dynamicDateRange_s = vcProperties.get(ViewConfigurationData.DYNAMIC_DATE_RANGE_PROPERTY_XML_TAG);
        if (dynamicDateRange_s != null && !dynamicDateRange_s.trim().isEmpty()) {
            dynamicDateRange = GUIUtilities.stringToBoolean(dynamicDateRange_s);
        }
        viewConfData.setDynamicDateRange(dynamicDateRange);

        String dateRange = vcProperties.get(ViewConfigurationData.DATE_RANGE_PROPERTY_XML_TAG);
        if (dateRange != null && !dateRange.trim().isEmpty()) {
            viewConfData.setDateRange(dateRange);
        }

        if (creationDate_s != null) {
            Timestamp creationDate = Timestamp.valueOf(creationDate_s);
            viewConfData.setCreationDate(creationDate);
        }
        if (lastUpdateDate_s != null) {
            Timestamp lastUpdateDate = Timestamp.valueOf(lastUpdateDate_s);
            viewConfData.setLastUpdateDate(lastUpdateDate);
        }

        try {
            Timestamp startDate = Timestamp.valueOf(startDate_s);
            viewConfData.setStartDate(startDate);
        } catch (Exception e) {
        }

        try {
            Timestamp endDate = Timestamp.valueOf(endDate_s);
            viewConfData.setEndDate(endDate);
        } catch (Exception e) {
        }

        if (samplingType_s != null) {
            SamplingType samplingType = SamplingType.fromString(samplingType_s);
            if (samplingFactor_s != null) {
                // default value is 1
                short samplingFactor = 1;
                try {
                    samplingFactor = Short.parseShort(samplingFactor_s);
                } catch (Exception e) {

                }

                if (samplingFactor > 0) {
                    samplingType.setAdditionalFilteringFactor(samplingFactor);
                    samplingType.setHasAdditionalFiltering(true);
                } else {
                    samplingType.setHasAdditionalFiltering(false);
                }
            }
            viewConfData.setSamplingType(samplingType);
        }

        boolean autoAveraging = ViewConfigurationData.DEFAULT_AUTO_AVERAGING;
        String autoAveraging_s = vcProperties.get(ViewConfigurationData.AUTOMATIC_AVERAGING_PROPERTY_XML_TAG);
        if (autoAveraging_s != null && !autoAveraging_s.trim().isEmpty()) {
            autoAveraging = GUIUtilities.stringToBoolean(autoAveraging_s);
        }
        viewConfData.setAutoAveraging(autoAveraging);

        viewConf.setData(viewConfData);

        return viewConf;
    }

    // MULTI-CONF
    public static ViewConfiguration loadViewConfigurationIntoHash(String location) throws ArchivingException {
        File file = new File(location);
        Node rootNode;
        try {
            rootNode = XMLUtils.getRootNode(file);
        } catch (XMLWarning e) {
            throw XMLManager.generateFileArchivingException(location);
        }

        ViewConfiguration ret = loadViewConfigurationIntoHashFromRoot(rootNode);

        return ret;
    }

    public static ViewConfiguration loadViewConfigurationIntoHashFromRoot(Node rootNode) throws ArchivingException {
        ViewConfiguration viewConf = loadViewConfigurationData(rootNode);

        if (rootNode.hasChildNodes()) {
            NodeList childNodes = rootNode.getChildNodes();
            ViewConfigurationAttributes viewConfAttr = new ViewConfigurationAttributes();

            // as many loops as there are attributes in the VC
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node currentChild = childNodes.item(i);
                if (XMLUtils.isAFakeNode(currentChild)) {
                    continue;
                }

                String currentChildType = currentChild.getNodeName().trim();
                if (currentChildType.equals(ViewConfigurationAttribute.XML_TAG)) {
                    Map<String, String> attributeProperties = XMLUtils.loadAttributes(currentChild);
                    String attributeCompleteName = attributeProperties
                            .get(ViewConfigurationAttribute.COMPLETE_NAME_PROPERTY_XML_TAG);
                    Double attributeFactor;
                    String factorString = attributeProperties.get(ViewConfigurationAttribute.FACTOR_PROPERTY_XML_TAG);
                    if (factorString == null) {
                        factorString = "1";
                    }
                    attributeFactor = Double.valueOf(factorString);
                    if (attributeFactor == null) {
                        attributeFactor = Double.valueOf(1);
                    }
                    ViewConfigurationAttribute currentAttribute = new ViewConfigurationAttribute(attributeCompleteName);
                    currentAttribute.setFactor(attributeFactor.doubleValue());

                    currentAttribute = loadCurrentAttribute(currentAttribute, currentChild);

                    // currentAttribute.getProperties().getPlotProperties().getCurve()
                    // .setName(attributeCompleteName);

                    viewConfAttr.addAttribute(currentAttribute);
                } else if (currentChildType.equals(ViewConfigurationData.GENERIC_PLOT_PARAMETERS_XML_TAG)) {
                    ViewConfigurationData viewConfData = loadGenericPlotParameters(viewConf.getData(), currentChild);
                    viewConf.setData(viewConfData);
                } else if (currentChildType.equals(ViewConfigurationAttributes.COLOR_INDEX_XML_TAG)) {
                    Map<String, String> colorIndexProperties = XMLUtils.loadAttributes(currentChild);
                    String colorIndexStringValue = colorIndexProperties
                            .get(ViewConfigurationAttributes.COLOR_INDEX_XML_ATTRIBUTE);
                    try {
                        viewConfAttr.setColorIndex(Integer.parseInt(colorIndexStringValue));
                    } catch (NumberFormatException nfe) {
                        // could not get the colorIndex
                        // nothing to do : colorIndex automatically initialized
                        // with 0
                    }
                } else if (ExpressionAttribute.XML_TAG.equals(currentChildType)) {
                    // load expressions
                    ExpressionAttribute expressionAttribute = loadExpression(currentChild);
                    viewConf.addExpression(expressionAttribute);
                } else {
                    continue;
                }
            }
            viewConf.setAttributes(viewConfAttr);
        }
        return viewConf;
    }

}
