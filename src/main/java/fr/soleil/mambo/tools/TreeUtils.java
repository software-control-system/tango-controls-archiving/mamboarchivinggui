package fr.soleil.mambo.tools;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.archiving.IArchivingManagerApi;
import fr.soleil.mambo.datasources.db.DbConnectionManager;
import fr.soleil.mambo.models.AttributesTreeModel;

public class TreeUtils {

    /**
     * This method checks the complete name of an attribute according to a database.
     * 
     * @param tree The tree that may help to recover the real attribute name.
     * @param historic a {@link Boolean} to know to which database the name of the attribute should be asked
     * @param node the node that represents an attribute
     * @return The complete name of the attribute according to the database.
     */
    public static String getFullAttributeNameFromNode(final JTree tree, final Boolean historic,
            final DefaultMutableTreeNode node) {
        boolean facility = false;
        IArchivingManagerApi api = DbConnectionManager.getArchivingManagerApiInstance(historic);
        if (api != null) {
            facility = api.isFacility();
        }

        final TreeNode[] treeNode = node.getPath();
        final String treeName = (facility ? "//" + treeNode[0].toString() + TangoDeviceHelper.SLASH
                : ObjectUtils.EMPTY_STRING) + treeNode[1].toString() + TangoDeviceHelper.SLASH + treeNode[2].toString()
                + TangoDeviceHelper.SLASH + treeNode[3].toString() + TangoDeviceHelper.SLASH + treeNode[4].toString();
        String name = treeName;
        if (tree.getModel() instanceof AttributesTreeModel<?>) {
            // retrieve the real attribute name (there may be a case difference)
            final AttributesTreeModel<?> model = (AttributesTreeModel<?>) tree.getModel();
            final Attribute attribute = model.getAttribute(treeName);
            if ((attribute != null) && (attribute.getCompleteName() != null)) {
                name = attribute.getCompleteName();
            }
        }
        return name;
    }
}
