// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/lifecycle/LifeCycleManagerFactory.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  LifeCycleManagerFactory.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: LifeCycleManagerFactory.java,v $
// Revision 1.4  2007/10/30 16:12:38  ounsy
// change the name of the file which define the life cycle to use : mambo_conf.properties instead of mambo.properties.
//
// Revision 1.3  2007/09/13 09:56:54  ounsy
// DBA :
// Modification in LifecycleManagerFactory to manage multiple LifeCycleManager.
// In ArchivingManagerFactory, AttributeManagerFactory, ExtractingManagerFactory add setCurrentImpl methods.
//
// Revision 1.2  2005/11/29 18:28:26  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:32  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.lifecycle;

public class LifeCycleManagerFactory {
    public static final int DEFAULT_LIFE_CYCLE = 0;
    public static final String MAMBO_PROPERTIES = "mambo_conf.properties";
    public static final String LIFE_CYCLE_MANAGER_PROPERTY = "LIFE_CYCLE_MANAGER";

    private static LifeCycleManager currentImpl;

    /**
     * @return 28 juin 2005
     */
    public static LifeCycleManager getCurrentImpl() {
        return currentImpl;
    }

    /**
     * Set the current Impl for LifeCycle
     * 
     * @param currentImpl
     */
    public static void setCurrentImpl(LifeCycleManager currentImpl) {
        LifeCycleManagerFactory.currentImpl = currentImpl;
    }

}
