package fr.soleil.mambo.lifecycle;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.lib.project.swing.Splash;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.api.db.DbConnectionParameters;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.attributes.AttributeManagerFactory;
import fr.soleil.mambo.datasources.db.attributes.IAttributeManager;
import fr.soleil.mambo.datasources.db.extracting.ExtractingManagerFactory;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;
import fr.soleil.mambo.datasources.db.properties.PropertyManagerFactory;
import fr.soleil.mambo.datasources.file.ArchivingConfigurationManagerFactory;
import fr.soleil.mambo.datasources.file.ViewConfigurationManagerFactory;
import fr.soleil.mambo.datasources.tango.standard.TangoManagerFactory;
import fr.soleil.mambo.history.History;
import fr.soleil.mambo.history.manager.HistoryManagerFactory;
import fr.soleil.mambo.history.manager.IHistoryManager;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.manager.ACDefaultsManagerFactory;
import fr.soleil.mambo.options.manager.IACDefaultsManager;
import fr.soleil.mambo.options.manager.IOptionsManager;
import fr.soleil.mambo.options.manager.OptionsManagerFactory;
import fr.soleil.mambo.options.sub.ACOptions;
import fr.soleil.mambo.options.sub.SaveOptions;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.xmlhelpers.ac.ArchivingConfigurationXMLHelperFactory;

public class DefaultLifeCycleManager implements LifeCycleManager, ApiConstants {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultLifeCycleManager.class);

    private String historyPath;
    private String optionsPath;

    private boolean hasHistorySave;
    private boolean closing = false;

    public DefaultLifeCycleManager() {
        this(null);
    }

    public DefaultLifeCycleManager(final String vc) {
        super();
        hasHistorySave = vc == null;
        closing = false;
    }

    @Override
    public void applicationWillStart(final Splash splash) throws ArchivingException {
        startFactories();

        final Locale currentLocale = new Locale("en", "US"); // TODO use Locale.US?
        try {
            Messages.initResourceBundle(currentLocale);
        } catch (final Exception e) {
            throw ArchivingException.toArchivingException(e);
        }

        try {
            loadOptions(splash);
        } catch (final Exception e) {
            Mambo.treatError(e, Messages.getLogMessage("APPLICATION_WILL_START_LOAD_OPTIONS_KO"), splash);
        }

        if (hasHistorySave) {
            // treatError is already done in loadHistory
            loadHistory(splash);
        }

        connectToDatabase(splash);

        if (Mambo.hasACs()) {
            bufferTangoAttributes(splash);
        }

        bufferDBAttributes(splash);
    }

    private void bufferTangoAttributes(final Splash splash) {

        try {
            splash.progress(18);
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_TANGO_ATTRIBUTES"));
            splash.progress(19);
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_TANGO_ATTRIBUTES_OK"));
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_TANGO_ATTRIBUTES_KO");
            splash.progress(19);
            splash.setMessage(msg);
            LOGGER.error(msg, e);
        }
    }

    private void bufferDBAttributes(final Splash splash) {
        if (DbConnectionParameters.isHdbAvailable()) {
            try {
                splash.progress(20);
                splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_HDB_ATTRIBUTES"));

                final IAttributeManager source = AttributeManagerFactory.getCurrentImpl();
                source.loadDomains(true, false);

                splash.progress(21);
                splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_HDB_ATTRIBUTES_OK"));
            } catch (final Exception e) {
                LOGGER.warn("no connection to HDB");
            }
        }
        if (DbConnectionParameters.isTdbAvailable()) {
            try {
                splash.progress(22);
                splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_TDB_ATTRIBUTES"));

                final IAttributeManager source = AttributeManagerFactory.getCurrentImpl();
                source.loadDomains(false, false);

                splash.progress(23);
                splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_TDB_ATTRIBUTES_OK"));
            } catch (final Exception e) {
                LOGGER.error("error build TDB GUI", e);
                LOGGER.warn("no connection to TDB");
            }
        }
        if (DbConnectionParameters.isTtsAvailable()) {
            try {
                splash.progress(24);
                splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_TTS_ATTRIBUTES"));

                final IAttributeManager source = AttributeManagerFactory.getCurrentImpl();
                source.loadDomains(false, false);

                splash.progress(25);
                splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_TTS_ATTRIBUTES_OK"));
            } catch (final Exception e) {
                LOGGER.error("error build TTS GUI", e);
                LOGGER.warn("no connection to TTS");
            }
        }
    }

    private void connectToDatabase(final Splash splash) {
        try {
            splash.progress(16);
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_CONNECT_DB"));

            final IAttributeManager attributeSource = AttributeManagerFactory.getCurrentImpl();
            attributeSource.openConnection();

            final IExtractingManager extractingSource = ExtractingManagerFactory.getCurrentImpl();
            extractingSource.openConnection();

            splash.progress(17);
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_CONNECT_DB_OK"));
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_CONNECT_DB_KO");
            splash.progress(17);
            splash.setMessage(msg);
            // LOGGER.error(msg, e);
            Mambo.treatError(e, msg, splash);
        }
    }

    /**
     * 5 juil. 2005
     */
    private void loadOptions(final Splash splash) throws ArchivingException {
        try {

            splash.progress(3);
            LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_PREPARE_OPTIONS"));
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_PREPARE_OPTIONS"));
            optionsPath = Mambo.getPathToResources() + "/options";
            final File f = new File(optionsPath);
            if (!f.exists()) {
                f.mkdir();
            }
            optionsPath += "/options.xml";

            splash.progress(4);
            LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_INIT_OPTIONS"));
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_INIT_OPTIONS"));
            final IOptionsManager optionsManager = OptionsManagerFactory.getCurrentImpl();

            // Options options = optionsManager.loadOptions( optionsPath );
            splash.progress(5);
            LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_BASIC_OPTIONS"));
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_BASIC_OPTIONS"));
            final Options options = optionsManager.loadOptions(optionsPath);
            ACOptions acOptions = options.getAcOptions();
            Map<String, String> content = acOptions.getContent();
            boolean readyForTSS = false;
            for (String key : content.keySet()) {
                if (key.startsWith(TTS)) {
                    readyForTSS = true;
                    break;
                }
            }
            if (!readyForTSS) {
                Map<String, String> defaultContent = getACDefault().getContent();
                for (Entry<String, String> entry : defaultContent.entrySet()) {
                    if (entry.getKey().startsWith(TTS)) {
                        acOptions.putOption(entry.getKey(), entry.getValue());
                    }
                }
            }
            splash.progress(6);
            LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_BASIC_OPTIONS_OK"));
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_BASIC_OPTIONS_OK") + " - "
                    + Messages.getLogMessage("APPLICATION_WILL_START_APPLY_BASIC_OPTIONS"));

            Options.setOptionsInstance(options);
            splash.progress(8);
            LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_APPLY_BASIC_OPTIONS_OK"));
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_APPLY_BASIC_OPTIONS_OK") + " - "
                    + Messages.getLogMessage("APPLICATION_WILL_START_LOAD_HISTORY_OPTIONS"));
            // we have to call it now before the normal options.push () that occurs in applicationStarted
            // because otherwise the loaded desired log level won't be in effect before applicationStarted
            // is called, and we always get applicationWillStart DEBUG level logs.
            // same for history loading
            setHasToLoadHistory();
            splash.progress(9);
            LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_HISTORY_OPTIONS_OK"));
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_HISTORY_OPTIONS_OK"));

            LOGGER.debug(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_OPTIONS_OK"));
        } catch (final FileNotFoundException fnfe) {
            LOGGER.warn(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_OPTIONS_WARNING"), fnfe);
            splash.progress(8);
            LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_DEFAULT_OPTIONS"));
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_DEFAULT_OPTIONS"));
            loadACDefaults();
            splash.progress(9);
            LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_DEFAULT_OPTIONS_OK"));
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_DEFAULT_OPTIONS_OK"));
        } catch (final Exception e) {
            throw ArchivingException.toArchivingException(e);
        }
    }

    private ACOptions getACDefault() throws ArchivingException {
        final IACDefaultsManager manager = ACDefaultsManagerFactory.getCurrentImpl();
        manager.setDefault(true);
        manager.setSaveLocation(null);

        ACOptions defaultACOptions;
        try {
            defaultACOptions = manager.loadACDefaults();
        } catch (final Exception e) {
            throw ArchivingException.toArchivingException(e);
        }
        return defaultACOptions;
    }

    private void loadACDefaults() throws ArchivingException {
        try {
            ACOptions defaultACOptions = getACDefault();
            Options.getInstance().setAcOptions(defaultACOptions);
        } catch (final Exception e) {
            throw ArchivingException.toArchivingException(e);
        }
    }

    /**
     * 5 juil. 2005
     *
     * @throws ArchivingException
     */
    private void startFactories() throws ArchivingException {
        ArchivingManagerFactory.getImpl(ArchivingManagerFactory.REAL_IMPL_TYPE_GLOBAL_START_CALL);
        TangoManagerFactory.getImpl(TangoManagerFactory.BASIC_IMPL_TYPE);

        // TODO: real singletons
        ArchivingConfigurationXMLHelperFactory
                .getImpl(Mambo.canModifyExportPeriod() ? ArchivingConfigurationXMLHelperFactory.STANDARD_IMPL_TYPE
                        : ArchivingConfigurationXMLHelperFactory.FORCED_EXPORT_PERIOD_IMPL_TYPE);
        ArchivingConfigurationManagerFactory.getImpl(ArchivingConfigurationManagerFactory.XML_IMPL_TYPE);
        ViewConfigurationManagerFactory.getImpl(ViewConfigurationManagerFactory.XML_IMPL_TYPE);

        AttributeManagerFactory.getImpl(AttributeManagerFactory.BUFFERED_FORMATS_AND_DOMAINS);
        ExtractingManagerFactory.getImpl(ExtractingManagerFactory.BASIC);

        HistoryManagerFactory.getImpl(HistoryManagerFactory.XML_IMPL_TYPE);
        OptionsManagerFactory.getImpl(OptionsManagerFactory.XML_IMPL_TYPE);
        ACDefaultsManagerFactory.getImpl(ACDefaultsManagerFactory.PROPERTIES_IMPL_TYPE);

        PropertyManagerFactory.getImpl(PropertyManagerFactory.DEFAULT);
    }

    /**
     * 5 juil. 2005
     */
    private void loadHistory(final Splash splash) {
        try {
            splash.progress(10);
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_PREPARE_HISTORY"));
            historyPath = Mambo.getPathToResources() + "/history";
            final File f = new File(historyPath);
            if (!f.exists()) {
                // boolean b =
                f.mkdir();
            }
            historyPath += "/history.xml";

            splash.progress(11);
            LOGGER.info("start loading history file {} ", historyPath);
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_PREPARE_HISTORY"));
            final IHistoryManager historyManager = HistoryManagerFactory.getCurrentImpl();

            // History history = historyManager.loadHistory( historyPath );
            splash.progress(12);
            splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_LOAD_HISTORY"));
            final History history = historyManager.loadHistory(historyPath);
            LOGGER.info("history file loaded");
            if (history != null) {
                splash.progress(13);
                LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_APPLY_HISTORY"));
                splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_APPLY_HISTORY"));
                history.push();
                LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_APPLY_HISTORY_OK"));
                splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_APPLY_HISTORY_OK"));
                splash.progress(14);
                History.setCurrentHistory(history);
            }
            splash.progress(15);
            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_HISTORY_OK");
            splash.setMessage(msg);
            LOGGER.info(msg);
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_START_LOAD_HISTORY_KO");
            splash.progress(15);
            splash.setMessage(msg);
            Mambo.treatError(e, msg, splash);
        }

    }

    @Override
    public void applicationStarted() {
        final Options options = Options.getInstance();
        try {
            options.push();
        } catch (final Exception e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
        }
    }

    @Override
    public void applicationClosed() {
        if (!closing) {
            applicationWillClose();
        }
    }

    @Override
    public int applicationWillClose() {
        final AccountManager accountManager = Mambo.getAccountManager();
        try {
            if (!closing) {
                closing = true;

                // begin do stuff
                System.out.println("Mambo will close !");

                saveOptions();

                if (hasHistorySave) {
                    saveHistory();
                    hasHistorySave = false;
                }

                // end do stuff
                if (accountManager != null) {
                    accountManager.clearLock(accountManager.getSelectedAccountWorkingPath());
                }

                System.out.println("Mambo closed");

            }
            return 0;
        } catch (final Throwable t) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(t), t);
            if (accountManager != null) {
                accountManager.clearLock(accountManager.getSelectedAccountWorkingPath());
            }
            return 1;
        }
    }

    /**
     * 5 juil. 2005
     */
    private void saveOptions() throws ArchivingException {
        try {
            final Options optionsToSave = Options.getInstance();
            final IOptionsManager optionsManager = OptionsManagerFactory.getCurrentImpl();
            if ((optionsManager != null) && (optionsPath != null)) {
                optionsManager.saveOptions(optionsToSave, optionsPath);
                final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_OPTIONS_OK");
                LOGGER.debug(msg);
            }
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_OPTIONS_KO");
            LOGGER.error(msg, e);
        }
    }

    /**
     * 5 juil. 2005
     */
    private void saveHistory() throws ArchivingException {
        try {
            final History historyToSave = History.getCurrentHistory();

            final IHistoryManager historyManager = HistoryManagerFactory.getCurrentImpl();
            historyManager.saveHistory(historyToSave, historyPath);

            final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_HISTORY_OK");
            LOGGER.debug(msg);
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("APPLICATION_WILL_STOP_SAVE_HISTORY_KO");
            LOGGER.error(msg, e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see mambo.lifecycle.LifeCycleManager#setHasHistorySave(boolean)
     */
    @Override
    public void setHasHistorySave(final boolean b) {
        hasHistorySave = b;
    }

    /*
     * (non-Javadoc)
     *
     * @see mambo.lifecycle.LifeCycleManager#hasHistorySave()
     */
    @Override
    public boolean hasHistorySave() {
        return hasHistorySave;
    }

    /**
     * 20 juil. 2005
     */
    private void setHasToLoadHistory() {
        final Options options = Options.getOptionsInstance();
        final SaveOptions saveOptions = options.getSaveOptions();
        saveOptions.push();
    }

}
