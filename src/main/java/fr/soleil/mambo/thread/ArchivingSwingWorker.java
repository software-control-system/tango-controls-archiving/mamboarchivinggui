package fr.soleil.mambo.thread;

import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;

public abstract class ArchivingSwingWorker<U, V> extends SwingWorker<U, V> implements ICancelable {

    protected final Logger logger;
    protected final WeakReference<ViewConfigurationBean> vcBeanRef;

    public ArchivingSwingWorker(ViewConfigurationBean viewConfigurationBean) {
        super();
        vcBeanRef = (viewConfigurationBean == null ? null
                : new WeakReference<ViewConfigurationBean>(viewConfigurationBean));
        logger = LoggerFactory.getLogger(getClass());
    }

    protected ProgressDialog getWaitingDialog() {
        ProgressDialog dialog;
        ViewConfigurationBean vcBean = ObjectUtils.recoverObject(vcBeanRef);
        if (vcBean == null) {
            dialog = null;
        } else {
            dialog = vcBean.getWaitingDialog();
        }
        return dialog;
    }

    @Override
    protected final U doInBackground() throws Exception {
        U result;
        try {
            result = doTheJob();
        } catch (OutOfMemoryError oome) {
            result = null;
            treatMemoryError(oome);
        }
        return result;
    }

    @Override
    protected final void done() {
        U result = null;
        try {
            result = get();
        } catch (InterruptedException e) {
            // interrupted means canceled
            setCanceled(true);
        } catch (ExecutionException e) {
            Throwable reason = e.getCause();
            if (!isCanceled()) {
                if (reason instanceof OutOfMemoryError) {
                    treatMemoryError((OutOfMemoryError) reason);
                } else {
                    setCanceled(true);
                    logger.error("Extraction canceled due to unexpected error", reason);
                }
            }
        }
        afterJob(result);
        if (isCanceled()) {
            closeWaitingDialog();
        }
    }

    protected void closeWaitingDialog() {
        ViewConfigurationBean bean = ObjectUtils.recoverObject(vcBeanRef);
        if ((bean != null)) {
            ProgressDialog waitingDialog = bean.getWaitingDialog();
            if (waitingDialog != null) {
                waitingDialog.setVisible(false);
            }
        }
    }

    protected abstract U doTheJob() throws ArchivingException;

    protected abstract void afterJob(U jobResult);

    public abstract void prepareWaitingDialog();

    protected void treatMemoryError(OutOfMemoryError oome) {
        setCanceled(true);
        logger.error("Extraction canceled due to memory error", oome);
    }

}
