package fr.soleil.mambo.thread;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.view.VCAttributesRecapTree;
import fr.soleil.mambo.containers.view.AbstractViewCleanablePanel;
import fr.soleil.mambo.containers.view.ViewActionPanel;
import fr.soleil.mambo.containers.view.ViewAttributesGraphPanel;
import fr.soleil.mambo.containers.view.ViewAttributesPanel;
import fr.soleil.mambo.containers.view.ViewAttributesTreePanel;
import fr.soleil.mambo.containers.view.ViewNumberScalarPanel;
import fr.soleil.mambo.containers.view.ViewStringStateBooleanScalarPanel;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationData;
import fr.soleil.mambo.datasources.db.extracting.ExtractingManagerFactory;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;
import fr.soleil.mambo.thread.VCExtractionSwingWorker.TransmitableData;
import fr.soleil.mambo.tools.ChartPropertiesUtils;
import fr.soleil.mambo.tools.Messages;

/**
 * A {@link SwingWorker} that extracts data from database and updates associated
 * vc attributes panels
 * 
 * @author girardot
 */
public class VCExtractionSwingWorker extends ArchivingSwingWorker<Void, TransmitableData> {

    private static final String AUTOAVERAGING_METHOD = Messages.getLogMessage("EXTRACT_VC_AUTOSAMPLING_METHOD");
    private static final String AUTOAVERAGING_METHOD_REMINDER = Messages
            .getLogMessage("EXTRACT_VC_AUTOSAMPLING_METHOD_REMINDER");

    private final boolean wasModified;
    private volatile boolean canceled;
    private String autoAveragingType;

    public VCExtractionSwingWorker(ViewConfigurationBean vcBean, boolean modified) {
        super(vcBean);
        wasModified = modified;
        canceled = false;
        autoAveragingType = null;
    }

    @Override
    public void prepareWaitingDialog() {
        ViewConfigurationBean bean = ObjectUtils.recoverObject(vcBeanRef);
        if (bean != null) {
            ProgressDialog waitingDialog = bean.getWaitingDialog();
            if (waitingDialog != null) {
                waitingDialog.setCancelable((ICancelable) this);
                waitingDialog.setProgress(0, ObjectUtils.EMPTY_STRING);
                waitingDialog.setProgressIndeterminate(false);
                int max = 0;
                ViewAttributesPanel attrPanel = bean.getAttributesPanel();
                if (attrPanel != null) {
                    ViewAttributesGraphPanel graphPanel = attrPanel.getViewAttributesGraphPanel();
                    if (graphPanel != null) {
                        final AbstractViewCleanablePanel[] panels = graphPanel.getViewPanels();
                        if (panels != null) {
                            for (AbstractViewCleanablePanel panel : panels) {
                                max += panel.getAttributeCount() * panel.getAttributeWeight();
                            }
                        }
                    }
                }
                waitingDialog.setMaxProgress(max);
            }
        }
    }

    @Override
    public boolean isCanceled() {
        return canceled;
    }

    @Override
    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
        IExtractingManager manager = ExtractingManagerFactory.getCurrentImpl();
        if (manager != null) {
            ViewConfigurationBean bean = ObjectUtils.recoverObject(vcBeanRef);
            if (bean != null) {
                ViewConfiguration vc = bean.getViewConfiguration();
                if (vc != null) {
                    ViewConfigurationData data = vc.getData();
                    if (data != null) {
                        try {
                            manager.setCanceled(data.isHistoric(), canceled);
                        } catch (ArchivingException e) {
                            logger.error("Failed to " + (canceled ? "allow" : "canceled") + " extracting manager", e);
                        }
                    }
                }
            }
        }
    }

    @Override
    protected void process(List<TransmitableData> chunks) {
        try {
            ViewConfigurationBean bean = ObjectUtils.recoverObject(vcBeanRef);
            if (bean != null) {
                for (final TransmitableData toTransmit : chunks) {
                    if ((toTransmit != null) && !isCanceled()) {
                        AbstractViewCleanablePanel panel = toTransmit.getPanel();
                        if (panel == null) {
                            Map<String, DbData[]> expressionMap = toTransmit.getExpressionMap();
                            if (expressionMap != null) {
                                ViewConfiguration vc = bean.getViewConfiguration();
                                ViewAttributesPanel temp = bean.getAttributesPanel();
                                if ((vc != null) && (temp != null)) {
                                    ViewAttributesGraphPanel graphPanel = temp.getViewAttributesGraphPanel();
                                    if (graphPanel != null) {
                                        try {
                                            vc.setChartAttributeExpressionOfString(expressionMap,
                                                    graphPanel.getChart());
                                        } catch (Exception e) {
                                            logger.error(e.getMessage(), e);
                                        }
                                    }
                                }
                            }
                        } else {
                            panel.updateFromDataInEDT(toTransmit.getName());
                        }
                        ProgressDialog dialog = bean.getWaitingDialog();
                        if (dialog != null) {
                            dialog.setProgress(toTransmit.getProgress(), String.format(
                                    Messages.getMessage("VIEW_ATTRIBUTES_ATTRIBUTE_LOADED"), toTransmit.getName()));
                        }
                    }
                }
            }
        } catch (OutOfMemoryError oome) {
            treatMemoryError(oome);
        } finally {
            chunks.clear();
        }
    }

    @Override
    protected Void doTheJob() throws ArchivingException {
        autoAveragingType = null;
        ViewConfigurationBean vcBean = ObjectUtils.recoverObject(vcBeanRef);
        IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
        if ((vcBean != null) && (extractingManager != null) && !isCanceled()) {
            ViewAttributesPanel attrPanel = vcBean.getAttributesPanel();
            ViewConfiguration vc = vcBean.getViewConfiguration();
            if ((vc != null) && (vc.getData() != null) && (attrPanel != null) && !isCanceled()) {
                vc.computeCurrentId();
                ViewAttributesGraphPanel graphPanel = attrPanel.getViewAttributesGraphPanel();
                if (graphPanel != null) {
                    final Boolean historic = vc.getData().isHistoric();
                    SamplingType samplingType = vc.getData().getSamplingType();
                    // only listing: can be done outside of EDT
                    final AbstractViewCleanablePanel[] panels = graphPanel.getViewPanels();
                    if (panels != null) {
                        Timestamp start = vc.getData().getStartDate();
                        Timestamp end = vc.getData().getEndDate();
                        // TANGOARCH-842, TANGOARCH-889: auto averaging
                        if (vc.getData().isAutoAveraging()) {
                            long delta = Math.abs(end.getTime() - start.getTime());
                            long milliDay = 86400000, milliMonth = milliDay * 30;
                            long days = delta / milliDay;
                            short factor = 1;
                            if (days >= 1) {
                                if (days >= 7) {
                                    long months = delta / milliMonth;
                                    if (months >= 1) {
                                        if (months >= 3) {
                                            if (months >= 6) {
                                                long years = delta / (milliDay * 365);
                                                if (years >= 1) {
                                                    if (years >= 5) {
                                                        // delta >= 5 years: sampling by month
                                                        samplingType = SamplingType.getSamplingType(SamplingType.MONTH);
                                                        autoAveragingType = "1 month";
                                                    } else {
                                                        // 1 year <= delta < 5 years: 1 week
                                                        samplingType = SamplingType.getSamplingType(SamplingType.DAY);
                                                        factor = 7;
                                                        autoAveragingType = "1 week";
                                                    }
                                                } else {
                                                    // 6 months <= delta < 1 year: sampling by day
                                                    samplingType = SamplingType.getSamplingType(SamplingType.DAY);
                                                    autoAveragingType = "1 day";
                                                }
                                            } else {
                                                // 3 months <= delta < 6 months: sampling by hour
                                                samplingType = SamplingType.getSamplingType(SamplingType.HOUR);
                                                autoAveragingType = "1 hour";
                                            }
                                        } else {
                                            // 1 month <= delta < 3 months: sampling by 15 minutes
                                            samplingType = SamplingType.getSamplingType(SamplingType.MINUTE);
                                            factor = 15;
                                            autoAveragingType = "15 minutes";
                                        }
                                    } else {
                                        // 1 week <= delta < 1 month: sampling by minute
                                        samplingType = SamplingType.getSamplingType(SamplingType.MINUTE);
                                        autoAveragingType = "1 minute";
                                    }
                                } else {
                                    // 1 day <= delta < 1 week: sampling by second
                                    samplingType = SamplingType.getSamplingType(SamplingType.SECOND);
                                    autoAveragingType = "1 second";
                                }
                            } else {
                                // delta < 1 day: no sampling
                                samplingType = SamplingType.getSamplingType(SamplingType.ALL);
                                autoAveragingType = "no averaging";
                            }
                            if (samplingType.getType() != SamplingType.ALL) {
                                samplingType.setHasAdditionalFiltering(true);
                                samplingType.setAdditionalFilteringFactor(factor);
                            }
                            String samplingMessage = String.format(AUTOAVERAGING_METHOD, vc.getDisplayableTitle(),
                                    autoAveragingType);
                            logger.info(samplingMessage);
                            SwingUtilities.invokeLater(() -> {
                                ViewConfigurationBean bean = ObjectUtils.recoverObject(vcBeanRef);
                                if (bean != null) {
                                    ProgressDialog waitingDialog = bean.getWaitingDialog();
                                    if (waitingDialog != null) {
                                        waitingDialog.setOverallProgressMessage(samplingMessage);
                                        waitingDialog.pack();
                                    }
                                }
                            });
                        } // end if (vc.getData().isAutoAveraging())
                        String startDate = ObjectUtils.EMPTY_STRING;
                        String endDate = ObjectUtils.EMPTY_STRING;
                        try {
                            startDate = extractingManager.timeToDateSGBD(historic, start.getTime());
                            endDate = extractingManager.timeToDateSGBD(historic, end.getTime());
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                        int progress = 0;
                        Map<String, DbData[]> expressionMap = null;
                        boolean chartDataDone = false;
                        for (final AbstractViewCleanablePanel panel : panels) {
                            if (isCanceled()) {
                                break;
                            }
                            if (panel instanceof ViewNumberScalarPanel) {
                                chartDataDone = true;
                            }
                            final int toAdd = panel.getAttributeWeight();
                            if (panel instanceof ViewStringStateBooleanScalarPanel) {
                                expressionMap = new HashMap<>();
                            }
                            Collection<String> attributes = panel.getAssociatedAttributes();
                            if ((attributes != null) && (!attributes.isEmpty()) && !isCanceled()) {
                                for (String attribute : attributes) {
                                    attribute = attribute.toLowerCase();
                                    if (isCanceled()) {
                                        break;
                                    }
                                    DbData[] splitData;
                                    try {
                                        splitData = extractingManager.retrieveData(historic, samplingType, attribute,
                                                startDate, endDate);
                                    } catch (Exception e) {
                                        logger.error("Failed to extract DB data for " + attribute, e);
                                        splitData = null;
                                    }
                                    progress += toAdd;
                                    if (expressionMap != null) {
                                        if (splitData == null) {
                                            expressionMap.remove(attribute);
                                        } else {
                                            expressionMap.put(attribute, splitData);
                                        }
                                    }
                                    if (!isCanceled()) {
                                        panel.applyDataOutsideOfEDT(attribute, splitData);
                                        publish(new TransmitableData(attribute, panel, null, progress));
                                    }
                                } // end for (String attribute : attributes)
                            } // end if ((attributes != null) && (!attributes.isEmpty()) && !isCanceled())
                            if (chartDataDone && (expressionMap != null) && (!isCanceled())) {
                                publish(new TransmitableData(Messages.getMessage("EXPRESSION_STRING"), null,
                                        expressionMap, progress));
                            }
                        } // end for (final AbstractViewCleanablePanel panel : panels)
                    }
                }
            }
        }
        return null;
    }

    @Override
    protected void afterJob(Void jobResult) {
        ViewConfigurationBean vcBean = ObjectUtils.recoverObject(vcBeanRef);
        if (vcBean != null) {
            if (autoAveragingType != null) {
                ViewConfiguration vc = vcBean.getViewConfiguration();
                if ((vc != null) && (vc.getData() != null)) {
                    logger.info(
                            String.format(AUTOAVERAGING_METHOD_REMINDER, vc.getDisplayableTitle(), autoAveragingType));
                }
            }
            ViewAttributesPanel attrPanel = vcBean.getAttributesPanel();
            if (attrPanel != null) {
                ViewAttributesGraphPanel graphPanel = attrPanel.getViewAttributesGraphPanel();
                if (graphPanel != null) {
                    if (isCanceled()) {
                        graphPanel.cancel();
                    } else {
                        graphPanel.setLoaded();
                    }
                }
                ViewAttributesTreePanel treePanel = attrPanel.getViewAttributesTreePanel();
                if (treePanel != null) {
                    VCAttributesRecapTree tree = treePanel.getTree();
                    if (tree != null) {
                        tree.openExpandedPath();
                    }
                    ViewActionPanel actionPanel = treePanel.getViewActionPanel();
                    if (actionPanel != null) {
                        actionPanel.putRefreshButton();
                    }
                }
            }
        }
        ViewConfiguration vc = vcBean.getViewConfiguration();
        if (vc != null) {
            vc.setModified(wasModified);
        }
        ProgressDialog dialog = vcBean.getWaitingDialog();
        if (dialog != null) {
            dialog.setVisible(false);
        }
        if (vcBean != null) {
            vcBean.setEnableRefreshButton(true);
        }
        ChartPropertiesUtils.configurationListenerEnabled = true;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static class TransmitableData {
        private String name;
        private AbstractViewCleanablePanel panel;
        private int progress;
        private Map<String, DbData[]> expressionMap;

        public TransmitableData(String name, AbstractViewCleanablePanel panel, Map<String, DbData[]> expressionMap,
                int progress) {
            super();
            this.name = name;
            this.panel = panel;
            this.expressionMap = expressionMap;
            this.progress = progress;
        }

        public String getName() {
            return name;
        }

        public AbstractViewCleanablePanel getPanel() {
            return panel;
        }

        public Map<String, DbData[]> getExpressionMap() {
            return expressionMap;
        }

        public int getProgress() {
            return progress;
        }

        @Override
        protected void finalize() {
            name = null;
            panel = null;
            if (expressionMap != null) {
                expressionMap.clear();
            }
            expressionMap = null;
        }
    }

}
