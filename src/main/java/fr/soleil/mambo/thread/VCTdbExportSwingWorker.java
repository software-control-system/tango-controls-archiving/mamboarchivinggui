package fr.soleil.mambo.thread;

import java.util.Collection;
import java.util.List;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.containers.view.ViewAttributesPanel;
import fr.soleil.mambo.containers.view.ViewAttributesTreePanel;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttributes;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.tools.Messages;

public class VCTdbExportSwingWorker extends ArchivingSwingWorker<Void, String> {

    private final VCExtractionSwingWorker extractor;
    private ViewConfigurationAttributes attributes;

    public VCTdbExportSwingWorker(ViewConfigurationBean viewConfigurationBean, ViewConfigurationAttributes attributes) {
        super(viewConfigurationBean);
        this.attributes = attributes;
        boolean modified;
        if (viewConfigurationBean == null) {
            modified = false;
        } else {
            ViewConfiguration vc = viewConfigurationBean.getViewConfiguration();
            if (vc == null) {
                modified = false;
            } else {
                modified = vc.isModified();
            }
        }
        extractor = new VCExtractionSwingWorker(viewConfigurationBean, modified);
        // forces extracting manager to allow extraction
        setCanceled(false);
    }

    @Override
    public void setCanceled(boolean canceled) {
        extractor.setCanceled(canceled);
    }

    @Override
    public boolean isCanceled() {
        return extractor.isCanceled();
    }

    @Override
    protected void process(List<String> chunks) {
        if (chunks != null) {
            ProgressDialog dialog = getWaitingDialog();
            if (dialog != null) {
                for (final String message : chunks) {
                    dialog.setProgress(0, message);
                }
            }
            chunks.clear();
        }
    }

    @Override
    protected Void doTheJob() throws ArchivingException {
        if (!isCanceled()) {
            publish("Panels initialized. Preparing data export");
            ViewConfigurationBean vcBean = ObjectUtils.recoverObject(vcBeanRef);
            if ((vcBean != null) && (attributes != null)) {
                ViewAttributesPanel attrPanel = vcBean.getAttributesPanel();
                if (attrPanel != null) {
                    ViewAttributesTreePanel treePanel = attrPanel.getViewAttributesTreePanel();
                    if (treePanel != null) {
                        Collection<String> attributeNames = attributes.getAttributeNames();
                        if ((attributeNames != null) && !isCanceled()) {
                            for (final String attributeName : attributeNames) {
                                try {
                                    if (isCanceled()) {
                                        break;
                                    } else {
                                        ArchivingManagerFactory.getCurrentImpl().exportData2Tdb(attributeName,
                                                treePanel.getDateRangeBox().getEndDateField().getText());
                                        logger.info("export for " + attributeName + " done");
                                    }
                                } catch (final ArchivingException e) {
                                    final String msg = Messages.getMessage("VIEW_ACTION_EXPORT_WARNING")
                                            + attributeName;
                                    logger.warn(msg, e);
                                }
                            }
                        }
                    }
                }
            }
            publish("Data export done");
        }
        return null;
    }

    @Override
    protected void afterJob(Void jobResult) {
        ViewConfigurationBean vcBean = ObjectUtils.recoverObject(vcBeanRef);
        if (vcBean != null) {
            ViewAttributesPanel attrPanel = vcBean.getAttributesPanel();
            if (attrPanel != null) {
                attrPanel.revalidate();
                attrPanel.repaint();
            }
        }
        extractor.prepareWaitingDialog();
        extractor.execute();
    }

    @Override
    public void prepareWaitingDialog() {
        // nothing to be done here
    }

}
