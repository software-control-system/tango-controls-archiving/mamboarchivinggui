package fr.soleil.mambo.thread;

import java.util.List;
import java.util.Map;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.view.VCAttributesRecapTree;
import fr.soleil.mambo.containers.view.ViewAttributesPanel;
import fr.soleil.mambo.containers.view.ViewAttributesTreePanel;
import fr.soleil.mambo.containers.view.ViewSelectionPanel;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.data.view.ViewConfigurationAttributes;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.thread.VCRefreshSwingWorker.VCInfo;
import fr.soleil.mambo.tools.ChartPropertiesUtils;
import fr.soleil.mambo.tools.Messages;

public class VCRefreshSwingWorker extends ArchivingSwingWorker<VCInfo, String> {

    private ArchivingSwingWorker<?, ?> extractor;
    protected final boolean selectedAttributes;
    protected final boolean doForceTdbExport;
    private volatile boolean canceled;

    public VCRefreshSwingWorker(ViewConfigurationBean vcBean, boolean selectedAttributes) {
        super(vcBean);
        this.canceled = false;
        this.selectedAttributes = selectedAttributes;
        this.doForceTdbExport = Options.getInstance().getVcOptions().isDoForceTdbExport();
    }

    @Override
    public void prepareWaitingDialog() {
        // TODO use Messages
        ViewConfigurationBean bean = ObjectUtils.recoverObject(vcBeanRef);
        if (bean != null) {
            ViewConfiguration vc = bean.getViewConfiguration();
            ProgressDialog waitingDialog = bean.getWaitingDialog();
            if (waitingDialog != null) {
                waitingDialog.setMainPanelBackground(GUIUtilities.getViewColor());
                waitingDialog.setProgressBackground(GUIUtilities.getViewColor());
                waitingDialog.setProgressForeground(GUIUtilities.getViewCopyColor());
                waitingDialog.setCancelable((ICancelable) this);
                waitingDialog.setTitle("VC Refresh");
                waitingDialog.setMainMessage("Refreshing \"" + (vc == null ? "VC" : vc.getDisplayableTitle()) + "\"");
                waitingDialog.setProgressIndeterminate(true);
                waitingDialog.setMaxProgress(100);
                waitingDialog.setProgress(0, "Preparing UI...");
                waitingDialog.pack();
                waitingDialog.setLocationRelativeTo(waitingDialog.getOwner());
                waitingDialog.setVisible(true);
                waitingDialog.toFront();
            }
            ViewAttributesPanel attrPanel = bean.getAttributesPanel();
            if (attrPanel != null) {
                ViewAttributesTreePanel treePanel = attrPanel.getViewAttributesTreePanel();
                if (treePanel != null) {
                    // EDT: selects VC and expands tree
                    bean.refreshMainUI(false);
                    // EDT: cleans panels and sets as loading
                    bean.prepareComponentsReset();
                    // EDT
                    bean.setEnableRefreshButton(false);
                }
            }
        }
    }

    @Override
    protected void process(List<String> chunks) {
        if (chunks != null) {
            ProgressDialog dialog = getWaitingDialog();
            if (dialog != null) {
                for (final String message : chunks) {
                    dialog.setProgress(0, message);
                }
            }
            chunks.clear();
        }
    }

    @Override
    protected VCInfo doTheJob() throws ArchivingException {
        VCInfo result = null;
        ChartPropertiesUtils.configurationListenerEnabled = false;
        ViewConfigurationBean vcBean = ObjectUtils.recoverObject(vcBeanRef);
        boolean modified;
        if (vcBean == null) {
            modified = false;
        } else {
            ViewConfiguration vc = vcBean.getViewConfiguration();
            if (vc == null) {
                modified = false;
            } else {
                modified = vc.isModified();
            }
            ViewAttributesPanel attrPanel = vcBean.getAttributesPanel();
            if (attrPanel == null) {
                extractor = new VCExtractionSwingWorker(vcBean, modified);
                extractor.setCanceled(canceled);
            } else {
                ViewAttributesTreePanel treePanel = attrPanel.getViewAttributesTreePanel();
                if (treePanel == null) {
                    extractor = new VCExtractionSwingWorker(vcBean, modified);
                    extractor.setCanceled(canceled);
                } else {
                    final ViewConfiguration selectedVC = vcBean.getViewConfiguration();
                    VCAttributesRecapTree tree = treePanel.getTree();
                    // EDT (selected attributes) and not EDT, but can be done outside of EDT (only reading, not writing)
                    ViewConfigurationAttributes attributes = selectedAttributes ? selectedVC.getSelectedAttributes(tree)
                            : selectedVC.getAttributes();
                    try {
                        // Not EDT
                        Map<String, Object[]> splittedAttributes = vcBean.splitAttributes(attributes);
                        // Not EDT
                        int attrCount = attributes == null ? 0 : attributes.countAttributes();
                        // Not EDT
                        Map<ViewConfigurationAttribute, Integer> dataTypes = vcBean.extractDataTypeMap(attributes);
                        result = new VCInfo(splittedAttributes, dataTypes, attrCount);
                        // Not EDT
                        if (selectedVC != null) {
                            // recalculates VC start and end dates (in case of dynamic date range)
                            // can be done before previous EDT actions
                            selectedVC.refreshContent();
                        }
                    } finally {
                        // Not EDT
                        // try to force data export, when asked, in case of TDB
                        if ((selectedVC.getData() != null) && (selectedVC.getData().isHistoric() != null)
                                && (!selectedVC.getData().isHistoric().booleanValue())) {
                            if (doForceTdbExport) {
                                extractor = new VCTdbExportSwingWorker(vcBean, attributes);
                                extractor.setCanceled(canceled);
                            } else {
                                logger.info(Messages.getMessage("VIEW_ACTION_NO_EXPORT"));
                                extractor = new VCExtractionSwingWorker(vcBean, modified);
                                extractor.setCanceled(canceled);
                            }
                        } else {
                            extractor = new VCExtractionSwingWorker(vcBean, modified);
                            extractor.setCanceled(canceled);
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    protected void afterJob(VCInfo jobResult) {
        ViewConfigurationBean vcBean = ObjectUtils.recoverObject(vcBeanRef);
        if (vcBean != null) {
            ViewAttributesPanel attrPanel = vcBean.getAttributesPanel();
            ViewAttributesTreePanel treePanel = attrPanel.getViewAttributesTreePanel();
            if ((jobResult != null) && (treePanel != null) && !isCanceled()) {
                // EDT: creates/sets the panels according to known attributes
                vcBean.resetComponents(jobResult.getSplittedAttributes(), jobResult.getDataTypes(),
                        jobResult.getAttrCount());
                ProgressDialog dialog = getWaitingDialog();
                if (dialog != null) {
                    dialog.setProgress(0, "Panels initialized");
                }
                // EDT: updates the "force export" label text
                ViewSelectionPanel.getInstance().updateForceExport();
            }
            if (attrPanel != null) {
                attrPanel.revalidate();
                attrPanel.repaint();
            }
        }
        if (extractor != null) {
            extractor.prepareWaitingDialog();
            extractor.execute();
        }
    }

    @Override
    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
        if (extractor != null) {
            extractor.setCanceled(canceled);
        }
    }

    @Override
    public boolean isCanceled() {
        return canceled;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class VCInfo {

        Map<String, Object[]> splittedAttributes;
        Map<ViewConfigurationAttribute, Integer> dataTypes;
        int attrCount;

        public VCInfo(Map<String, Object[]> splittedAttributes, Map<ViewConfigurationAttribute, Integer> dataTypes,
                int attrCount) {
            this.splittedAttributes = splittedAttributes;
            this.dataTypes = dataTypes;
            this.attrCount = attrCount;
        }

        public Map<String, Object[]> getSplittedAttributes() {
            return splittedAttributes;
        }

        public Map<ViewConfigurationAttribute, Integer> getDataTypes() {
            return dataTypes;
        }

        public int getAttrCount() {
            return attrCount;
        }

        @Override
        protected void finalize() {
            splittedAttributes = null;
            dataTypes = null;
        }
    }
}
