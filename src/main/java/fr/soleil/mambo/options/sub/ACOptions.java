package fr.soleil.mambo.options.sub;

import java.util.Map.Entry;

import javax.swing.ButtonModel;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.components.MamboMenuBar;
import fr.soleil.mambo.components.MamboToolBar;
import fr.soleil.mambo.components.archiving.OpenedACComboBox;
import fr.soleil.mambo.containers.archiving.ArchivingActionPanel;
import fr.soleil.mambo.containers.archiving.dialogs.ACEditDialog;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesPropertiesPanel;
import fr.soleil.mambo.containers.sub.dialogs.options.ACTabDefaultPanel;
import fr.soleil.mambo.containers.sub.dialogs.options.OptionsACTab;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.options.PushPullOptionBook;
import fr.soleil.mambo.options.ReadWriteOptionBook;

public class ACOptions extends ReadWriteOptionBook implements PushPullOptionBook {

    public static final String SELECTION_MODE = "SELECTION_MODE";
    public static final int SELECTION_MODE_TREE = 0;
    public static final int SELECTION_MODE_TABLE = 1;

    public static final String STACK_DEPTH = "STACK_DEPTH";
    private static final int DEFAULT_STACK_DEPTH = 5;

    // --
    public static final String HDB_PERIODIC_MODE = "HDB_PERIODIC_MODE";
    public static final String HDB_ABSOLUTE_MODE = "HDB_ABSOLUTE_MODE";
    public static final String HDB_RELATIVE_MODE = "HDB_RELATIVE_MODE";
    public static final String HDB_THRESHOLD_MODE = "HDB_THRESHOLD_MODE";
    public static final String HDB_DIFFERENCE_MODE = "HDB_DIFFERENCE_MODE";
    // ----
    public static final String HDB_ABSOLUTE_MODE_PERIOD = "HDB_ABSOLUTE_MODE_PERIOD";
    public static final String HDB_ABSOLUTE_MODE_UPPER = "HDB_ABSOLUTE_MODE_UPPER";
    public static final String HDB_ABSOLUTE_MODE_LOWER = "HDB_ABSOLUTE_MODE_LOWER";
    public static final String HDB_ABSOLUTE_SLOW_DRIFT = "HDB_ABSOLUTE_SLOW_DRIFT";

    public static final String HDB_RELATIVE_MODE_PERIOD = "HDB_RELATIVE_MODE_PERIOD";
    public static final String HDB_RELATIVE_MODE_UPPER = "HDB_RELATIVE_MODE_UPPER";
    public static final String HDB_RELATIVE_MODE_LOWER = "HDB_RELATIVE_MODE_LOWER";
    public static final String HDB_RELATIVE_SLOW_DRIFT = "HDB_RELATIVE_SLOW_DRIFT";

    public static final String HDB_THRESHOLD_MODE_PERIOD = "HDB_THRESHOLD_MODE_PERIOD";
    public static final String HDB_THRESHOLD_MODE_UPPER = "HDB_THRESHOLD_MODE_UPPER";
    public static final String HDB_THRESHOLD_MODE_LOWER = "HDB_THRESHOLD_MODE_LOWER";

    public static final String HDB_DIFFERENCE_MODE_PERIOD = "HDB_DIFFERENCE_MODE_PERIOD";
    // --

    // --
    public static final String TTS_PERIODIC_MODE = "TTS_PERIODIC_MODE";
    public static final String TTS_ABSOLUTE_MODE = "TTS_ABSOLUTE_MODE";
    public static final String TTS_RELATIVE_MODE = "TTS_RELATIVE_MODE";
    public static final String TTS_THRESHOLD_MODE = "TTS_THRESHOLD_MODE";
    public static final String TTS_DIFFERENCE_MODE = "TTS_DIFFERENCE_MODE";
    // ----
    public static final String TTS_ABSOLUTE_MODE_PERIOD = "TTS_ABSOLUTE_MODE_PERIOD";
    public static final String TTS_ABSOLUTE_MODE_UPPER = "TTS_ABSOLUTE_MODE_UPPER";
    public static final String TTS_ABSOLUTE_MODE_LOWER = "TTS_ABSOLUTE_MODE_LOWER";
    public static final String TTS_ABSOLUTE_SLOW_DRIFT = "TTS_ABSOLUTE_SLOW_DRIFT";

    public static final String TTS_RELATIVE_MODE_PERIOD = "TTS_RELATIVE_MODE_PERIOD";
    public static final String TTS_RELATIVE_MODE_UPPER = "TTS_RELATIVE_MODE_UPPER";
    public static final String TTS_RELATIVE_MODE_LOWER = "TTS_RELATIVE_MODE_LOWER";
    public static final String TTS_RELATIVE_SLOW_DRIFT = "TTS_RELATIVE_SLOW_DRIFT";

    public static final String TTS_THRESHOLD_MODE_PERIOD = "TTS_THRESHOLD_MODE_PERIOD";
    public static final String TTS_THRESHOLD_MODE_UPPER = "TTS_THRESHOLD_MODE_UPPER";
    public static final String TTS_THRESHOLD_MODE_LOWER = "TTS_THRESHOLD_MODE_LOWER";

    public static final String TTS_DIFFERENCE_MODE_PERIOD = "TTS_DIFFERENCE_MODE_PERIOD";
    // --

    public static final String TDB_PERIODIC_MODE = "TDB_PERIODIC_MODE";
    public static final String TDB_ABSOLUTE_MODE = "TDB_ABSOLUTE_MODE";
    public static final String TDB_RELATIVE_MODE = "TDB_RELATIVE_MODE";
    public static final String TDB_THRESHOLD_MODE = "TDB_THRESHOLD_MODE";
    public static final String TDB_DIFFERENCE_MODE = "TDB_DIFFERENCE_MODE";
    // --

    public static final String TDB_ABSOLUTE_MODE_PERIOD = "TDB_ABSOLUTE_MODE_PERIOD";
    public static final String TDB_ABSOLUTE_MODE_UPPER = "TDB_ABSOLUTE_MODE_UPPER";
    public static final String TDB_ABSOLUTE_MODE_LOWER = "TDB_ABSOLUTE_MODE_LOWER";
    public static final String TDB_ABSOLUTE_SLOW_DRIFT = "TDB_ABSOLUTE_SLOW_DRIFT";

    public static final String TDB_RELATIVE_MODE_PERIOD = "TDB_RELATIVE_MODE_PERIOD";
    public static final String TDB_RELATIVE_MODE_UPPER = "TDB_RELATIVE_MODE_UPPER";
    public static final String TDB_RELATIVE_MODE_LOWER = "TDB_RELATIVE_MODE_LOWER";
    public static final String TDB_RELATIVE_SLOW_DRIFT = "TDB_RELATIVE_SLOW_DRIFT";

    public static final String TDB_THRESHOLD_MODE_PERIOD = "TDB_THRESHOLD_MODE_PERIOD";
    public static final String TDB_THRESHOLD_MODE_UPPER = "TDB_THRESHOLD_MODE_UPPER";
    public static final String TDB_THRESHOLD_MODE_LOWER = "TDB_THRESHOLD_MODE_LOWER";

    public static final String TDB_DIFFERENCE_MODE_PERIOD = "TDB_DIFFERENCE_MODE_PERIOD";
    // --
    // --

    // ----
    public static final String HDB_PERIOD = "HDB_PERIOD";
    public static final String TTS_PERIOD = "TTS_PERIOD";
    public static final String TDB_PERIOD = "TDB_PERIOD";
    public static final String TDB_EXPORT_PERIOD = "TDB_EXPORT_PERIOD";
    // ----

    public static final String KEY = "ACs"; // for XML save and load

    private boolean isAlternateSelectionMode;
    private int stackDepth = DEFAULT_STACK_DEPTH;

    /**
     *
     */
    public ACOptions() {
        super(KEY);
    }

    public ArchivingConfigurationAttributeProperties getDefaultACProperties() {
        final ArchivingConfigurationAttributeProperties ret = new ArchivingConfigurationAttributeProperties();
        String selection;
        String period;
        String lower;
        String upper;
        String slow_drift;

        // -------------HDB
        selection = content.get(HDB_PERIODIC_MODE);
        if (selection != null) {
            try {
                period = content.get(HDB_PERIOD);
                final int period_i = Integer.parseInt(period);

                final ModePeriode _mode = new ModePeriode(period_i);

                final Mode mode1 = new Mode();
                mode1.setModeP(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.TRUE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(HDB_ABSOLUTE_MODE);
        if (selection != null) {
            try {
                period = content.get(HDB_ABSOLUTE_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                lower = content.get(HDB_ABSOLUTE_MODE_LOWER);
                final double lower_d = Double.parseDouble(lower);
                upper = content.get(HDB_ABSOLUTE_MODE_UPPER);
                final double upper_d = Double.parseDouble(upper);
                slow_drift = content.get(HDB_ABSOLUTE_SLOW_DRIFT);
                final boolean sd = Boolean.parseBoolean(slow_drift);
                final ModeAbsolu _mode = new ModeAbsolu(period_i, lower_d, upper_d, sd);

                final Mode mode1 = new Mode();
                mode1.setModeA(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.TRUE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(HDB_RELATIVE_MODE);
        if (selection != null) {
            try {
                period = content.get(HDB_RELATIVE_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                lower = content.get(HDB_RELATIVE_MODE_LOWER);
                final double lower_d = Double.parseDouble(lower);
                upper = content.get(HDB_RELATIVE_MODE_UPPER);
                final double upper_d = Double.parseDouble(upper);
                slow_drift = content.get(HDB_RELATIVE_SLOW_DRIFT);
                final boolean sd = Boolean.parseBoolean(slow_drift);

                final ModeRelatif _mode = new ModeRelatif(period_i, lower_d, upper_d, sd);

                final Mode mode1 = new Mode();
                mode1.setModeR(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.TRUE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(HDB_THRESHOLD_MODE);
        if (selection != null) {
            try {
                period = content.get(HDB_THRESHOLD_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                lower = content.get(HDB_THRESHOLD_MODE_LOWER);
                final double lower_d = Double.parseDouble(lower);
                upper = content.get(HDB_THRESHOLD_MODE_UPPER);
                final double upper_d = Double.parseDouble(upper);

                final ModeSeuil _mode = new ModeSeuil(period_i, lower_d, upper_d);

                final Mode mode1 = new Mode();
                mode1.setModeT(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.TRUE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(HDB_DIFFERENCE_MODE);
        if (selection != null) {
            try {
                period = content.get(HDB_DIFFERENCE_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                final ModeDifference _mode = new ModeDifference(period_i);

                final Mode mode1 = new Mode();
                mode1.setModeD(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.TRUE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        // -------------TDB
        selection = content.get(TDB_PERIODIC_MODE);
        if (selection != null) {
            try {
                period = content.get(TDB_PERIOD);
                final int period_i = Integer.parseInt(period);
                final ModePeriode _mode = new ModePeriode(period_i);

                final Mode mode1 = new Mode();
                mode1.setModeP(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.FALSE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(TDB_ABSOLUTE_MODE);
        if (selection != null) {
            try {
                period = content.get(TDB_ABSOLUTE_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                lower = content.get(TDB_ABSOLUTE_MODE_LOWER);
                final double lower_d = Double.parseDouble(lower);
                upper = content.get(TDB_ABSOLUTE_MODE_UPPER);
                final double upper_d = Double.parseDouble(upper);
                slow_drift = content.get(TDB_ABSOLUTE_SLOW_DRIFT);
                final boolean sd = Boolean.parseBoolean(slow_drift);

                final ModeAbsolu _mode = new ModeAbsolu(period_i, lower_d, upper_d, sd);

                final Mode mode1 = new Mode();
                mode1.setModeA(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.FALSE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(TDB_RELATIVE_MODE);
        if (selection != null) {
            try {
                period = content.get(TDB_RELATIVE_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                lower = content.get(TDB_RELATIVE_MODE_LOWER);
                final double lower_d = Double.parseDouble(lower);
                upper = content.get(TDB_RELATIVE_MODE_UPPER);
                final double upper_d = Double.parseDouble(upper);
                slow_drift = content.get(TDB_RELATIVE_SLOW_DRIFT);
                final boolean sd = Boolean.parseBoolean(slow_drift);

                final ModeRelatif _mode = new ModeRelatif(period_i, lower_d, upper_d, sd);

                final Mode mode1 = new Mode();
                mode1.setModeR(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.FALSE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(TDB_THRESHOLD_MODE);
        if (selection != null) {
            try {
                period = content.get(TDB_THRESHOLD_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                lower = content.get(TDB_THRESHOLD_MODE_LOWER);
                final double lower_d = Double.parseDouble(lower);
                upper = content.get(TDB_THRESHOLD_MODE_UPPER);
                final double upper_d = Double.parseDouble(upper);

                final ModeSeuil _mode = new ModeSeuil(period_i, lower_d, upper_d);

                final Mode mode1 = new Mode();
                mode1.setModeT(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.FALSE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(TDB_DIFFERENCE_MODE);
        if (selection != null) {
            try {
                period = content.get(TDB_DIFFERENCE_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                final ModeDifference _mode = new ModeDifference(period_i);

                final Mode mode1 = new Mode();
                mode1.setModeD(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, Boolean.FALSE);
            } catch (final Exception e) {
                // do nothing
            }
        }

        // ----export period
        try {
            period = content.get(TDB_EXPORT_PERIOD);
            final long period_l = Long.parseLong(period);
            ret.getTDBProperties().setExportPeriod(period_l);
        } catch (final Exception e) {
            // do nothing
        }

        // -------------TTS
        selection = content.get(TTS_PERIODIC_MODE);
        if (selection != null) {
            try {
                period = content.get(TTS_PERIOD);
                final int period_i = Integer.parseInt(period);

                final ModePeriode _mode = new ModePeriode(period_i);

                final Mode mode1 = new Mode();
                mode1.setModeP(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, null);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(TTS_ABSOLUTE_MODE);
        if (selection != null) {
            try {
                period = content.get(TTS_ABSOLUTE_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                lower = content.get(TTS_ABSOLUTE_MODE_LOWER);
                final double lower_d = Double.parseDouble(lower);
                upper = content.get(TTS_ABSOLUTE_MODE_UPPER);
                final double upper_d = Double.parseDouble(upper);
                slow_drift = content.get(TTS_ABSOLUTE_SLOW_DRIFT);
                final boolean sd = Boolean.parseBoolean(slow_drift);
                final ModeAbsolu _mode = new ModeAbsolu(period_i, lower_d, upper_d, sd);

                final Mode mode1 = new Mode();
                mode1.setModeA(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, null);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(TTS_RELATIVE_MODE);
        if (selection != null) {
            try {
                period = content.get(TTS_RELATIVE_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                lower = content.get(TTS_RELATIVE_MODE_LOWER);
                final double lower_d = Double.parseDouble(lower);
                upper = content.get(TTS_RELATIVE_MODE_UPPER);
                final double upper_d = Double.parseDouble(upper);
                slow_drift = content.get(TTS_RELATIVE_SLOW_DRIFT);
                final boolean sd = Boolean.parseBoolean(slow_drift);

                final ModeRelatif _mode = new ModeRelatif(period_i, lower_d, upper_d, sd);

                final Mode mode1 = new Mode();
                mode1.setModeR(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, null);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(TTS_THRESHOLD_MODE);
        if (selection != null) {
            try {
                period = content.get(TTS_THRESHOLD_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                lower = content.get(TTS_THRESHOLD_MODE_LOWER);
                final double lower_d = Double.parseDouble(lower);
                upper = content.get(TTS_THRESHOLD_MODE_UPPER);
                final double upper_d = Double.parseDouble(upper);

                final ModeSeuil _mode = new ModeSeuil(period_i, lower_d, upper_d);

                final Mode mode1 = new Mode();
                mode1.setModeT(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, null);
            } catch (final Exception e) {
                // do nothing
            }
        }

        selection = content.get(TTS_DIFFERENCE_MODE);
        if (selection != null) {
            try {
                period = content.get(TTS_DIFFERENCE_MODE_PERIOD);
                final int period_i = Integer.parseInt(period);
                final ModeDifference _mode = new ModeDifference(period_i);

                final Mode mode1 = new Mode();
                mode1.setModeD(_mode);
                final ArchivingConfigurationMode acMode = new ArchivingConfigurationMode(mode1);

                ret.addMode(acMode, null);
            } catch (final Exception e) {
                // do nothing
            }
        }

        return ret;
    }

    //

    /*
     * (non-Javadoc)
     * 
     * @see bensikin.bensikin.options.PushPullOptionBook#fillFromOptionsDialog()
     */
    @Override
    public void fillFromOptionsDialog() {
        fillSelectionMode();
        fillStackDepth();

        fillDefaultModes();
        fillDefaultValues();
    }

    /**
     *
     */
    private void fillStackDepth() {
        final OptionsACTab acTab = OptionsACTab.getInstance();
        final String stackDepth_s = acTab.getStackDepth();
        try {
            stackDepth = Integer.parseInt(stackDepth_s);
            content.put(STACK_DEPTH, stackDepth_s);
        } catch (final Exception e) {
            // If the new value is bad, we keep the last good one.
            // We could also put the default value:
            // content.put( STACK_DEPTH , String.valueOf(this.getStackDepth())
            // );
        }
    }

    private void fillDefaultValues() {
        final ACTabDefaultPanel defaultPanel = ACTabDefaultPanel.getInstance();
        int value;
        String value_s;
        boolean value_b;
        long l;

        // ------HDB Periods
        value = defaultPanel.getHDBperiod();
        content.put(HDB_PERIOD, String.valueOf(value));

        value = defaultPanel.getHDBAbsolutePeriod();
        content.put(HDB_ABSOLUTE_MODE_PERIOD, String.valueOf(value));

        value = defaultPanel.getHDBRelativePeriod();
        content.put(HDB_RELATIVE_MODE_PERIOD, String.valueOf(value));

        value = defaultPanel.getHDBThresholdPeriod();
        content.put(HDB_THRESHOLD_MODE_PERIOD, String.valueOf(value));

        value = defaultPanel.getHDBDifferencePeriod();
        content.put(HDB_DIFFERENCE_MODE_PERIOD, String.valueOf(value));
        // ------

        // ------TDB Periods
        value = defaultPanel.getTDBperiod();
        content.put(TDB_PERIOD, String.valueOf(value));

        l = defaultPanel.getExportPeriod();
        content.put(TDB_EXPORT_PERIOD, String.valueOf(l));

        value = defaultPanel.getTDBAbsolutePeriod();
        content.put(TDB_ABSOLUTE_MODE_PERIOD, String.valueOf(value));

        value = defaultPanel.getTDBRelativePeriod();
        content.put(TDB_RELATIVE_MODE_PERIOD, String.valueOf(value));

        value = defaultPanel.getTDBThresholdPeriod();
        content.put(TDB_THRESHOLD_MODE_PERIOD, String.valueOf(value));

        value = defaultPanel.getTDBDifferencePeriod();
        content.put(TDB_DIFFERENCE_MODE_PERIOD, String.valueOf(value));
        // ------

        // ------TTS Periods
        value = defaultPanel.getTTSperiod();
        content.put(TTS_PERIOD, String.valueOf(value));

        value = defaultPanel.getTTSAbsolutePeriod();
        content.put(TTS_ABSOLUTE_MODE_PERIOD, String.valueOf(value));

        value = defaultPanel.getTTSRelativePeriod();
        content.put(TTS_RELATIVE_MODE_PERIOD, String.valueOf(value));

        value = defaultPanel.getTTSThresholdPeriod();
        content.put(TTS_THRESHOLD_MODE_PERIOD, String.valueOf(value));

        value = defaultPanel.getTTSDifferencePeriod();
        content.put(TTS_DIFFERENCE_MODE_PERIOD, String.valueOf(value));
        // ------

        // ------HDB upper/lower
        value_s = defaultPanel.getHDBAbsoluteUpper();
        content.put(HDB_ABSOLUTE_MODE_UPPER, value_s);

        value_s = defaultPanel.getHDBRelativeUpper();
        content.put(HDB_RELATIVE_MODE_UPPER, value_s);

        value_s = defaultPanel.getHDBThresholdUpper();
        content.put(HDB_THRESHOLD_MODE_UPPER, value_s);

        value_s = defaultPanel.getHDBAbsoluteLower();
        content.put(HDB_ABSOLUTE_MODE_LOWER, value_s);

        value_s = defaultPanel.getHDBRelativeLower();
        content.put(HDB_RELATIVE_MODE_LOWER, value_s);

        value_s = defaultPanel.getHDBThresholdLower();
        content.put(HDB_THRESHOLD_MODE_LOWER, value_s);

        // ------

        // ------TDB upper/lower
        value_s = defaultPanel.getTDBAbsoluteUpper();
        content.put(TDB_ABSOLUTE_MODE_UPPER, value_s);

        value_s = defaultPanel.getTDBRelativeUpper();
        content.put(TDB_RELATIVE_MODE_UPPER, value_s);

        value_s = defaultPanel.getTDBThresholdUpper();
        content.put(TDB_THRESHOLD_MODE_UPPER, value_s);

        value_s = defaultPanel.getTDBAbsoluteLower();
        content.put(TDB_ABSOLUTE_MODE_LOWER, value_s);

        value_s = defaultPanel.getTDBRelativeLower();
        content.put(TDB_RELATIVE_MODE_LOWER, value_s);

        value_s = defaultPanel.getTDBThresholdLower();
        content.put(TDB_THRESHOLD_MODE_LOWER, value_s);
        // ------

        // ------TTS upper/lower
        value_s = defaultPanel.getTTSAbsoluteUpper();
        content.put(TTS_ABSOLUTE_MODE_UPPER, value_s);

        value_s = defaultPanel.getTTSRelativeUpper();
        content.put(TTS_RELATIVE_MODE_UPPER, value_s);

        value_s = defaultPanel.getTTSThresholdUpper();
        content.put(TTS_THRESHOLD_MODE_UPPER, value_s);

        value_s = defaultPanel.getTTSAbsoluteLower();
        content.put(TTS_ABSOLUTE_MODE_LOWER, value_s);

        value_s = defaultPanel.getTTSRelativeLower();
        content.put(TTS_RELATIVE_MODE_LOWER, value_s);

        value_s = defaultPanel.getTTSThresholdLower();
        content.put(TTS_THRESHOLD_MODE_LOWER, value_s);

        // ------

        // ---HDB Slow Drift values
        value_b = defaultPanel.getHDBAbsoluteDLCheck();
        content.put(HDB_ABSOLUTE_SLOW_DRIFT, Boolean.toString(value_b));

        value_b = defaultPanel.getHDBRelativeDLCheck();
        content.put(HDB_RELATIVE_SLOW_DRIFT, Boolean.toString(value_b));

        // ---TDB Slow Drift values
        value_b = defaultPanel.getTDBAbsoluteDLCheck();
        content.put(TDB_ABSOLUTE_SLOW_DRIFT, Boolean.toString(value_b));

        value_b = defaultPanel.getTDBRelativeDLCheck();
        content.put(TDB_RELATIVE_SLOW_DRIFT, Boolean.toString(value_b));

        // ---TTS Slow Drift values
        value_b = defaultPanel.getTTSAbsoluteDLCheck();
        content.put(TTS_ABSOLUTE_SLOW_DRIFT, Boolean.toString(value_b));

        value_b = defaultPanel.getTTSRelativeDLCheck();
        content.put(TTS_RELATIVE_SLOW_DRIFT, Boolean.toString(value_b));

    }

    /**
     *
     */
    private void fillDefaultModes() {
        final ACTabDefaultPanel defaultPanel = ACTabDefaultPanel.getInstance();

        if (defaultPanel.hasModeHDB_P()) {
            content.put(HDB_PERIODIC_MODE, HDB_PERIODIC_MODE);
        } else {
            content.remove(HDB_PERIODIC_MODE);
        }

        if (defaultPanel.hasModeHDB_A()) {
            content.put(HDB_ABSOLUTE_MODE, HDB_ABSOLUTE_MODE);
        } else {
            content.remove(HDB_ABSOLUTE_MODE);
        }

        if (defaultPanel.hasModeHDB_R()) {
            content.put(HDB_RELATIVE_MODE, HDB_RELATIVE_MODE);
        } else {
            content.remove(HDB_RELATIVE_MODE);
        }

        if (defaultPanel.hasModeHDB_T()) {
            content.put(HDB_THRESHOLD_MODE, HDB_THRESHOLD_MODE);
        } else {
            content.remove(HDB_THRESHOLD_MODE);
        }

        if (defaultPanel.hasModeHDB_D()) {
            content.put(HDB_DIFFERENCE_MODE, HDB_DIFFERENCE_MODE);
        } else {
            content.remove(HDB_DIFFERENCE_MODE);
        }
        // ----
        if (defaultPanel.hasModeTDB_P()) {
            content.put(TDB_PERIODIC_MODE, TDB_PERIODIC_MODE);
        } else {
            content.remove(TDB_PERIODIC_MODE);
        }

        if (defaultPanel.hasModeTDB_A()) {
            content.put(TDB_ABSOLUTE_MODE, TDB_ABSOLUTE_MODE);
        } else {
            content.remove(TDB_ABSOLUTE_MODE);
        }

        if (defaultPanel.hasModeTDB_R()) {
            content.put(TDB_RELATIVE_MODE, TDB_RELATIVE_MODE);
        } else {
            content.remove(TDB_RELATIVE_MODE);
        }

        if (defaultPanel.hasModeTDB_T()) {
            content.put(TDB_THRESHOLD_MODE, TDB_THRESHOLD_MODE);
        } else {
            content.remove(TDB_THRESHOLD_MODE);
        }

        if (defaultPanel.hasModeTDB_D()) {
            content.put(TDB_DIFFERENCE_MODE, TDB_DIFFERENCE_MODE);
        } else {
            content.remove(TDB_DIFFERENCE_MODE);
        }

        if (defaultPanel.hasModeTTS_P()) {
            content.put(TTS_PERIODIC_MODE, TTS_PERIODIC_MODE);
        } else {
            content.remove(TTS_PERIODIC_MODE);
        }

        if (defaultPanel.hasModeTTS_A()) {
            content.put(TTS_ABSOLUTE_MODE, TTS_ABSOLUTE_MODE);
        } else {
            content.remove(TTS_ABSOLUTE_MODE);
        }

        if (defaultPanel.hasModeTTS_R()) {
            content.put(TTS_RELATIVE_MODE, TTS_RELATIVE_MODE);
        } else {
            content.remove(TTS_RELATIVE_MODE);
        }

        if (defaultPanel.hasModeTTS_T()) {
            content.put(TTS_THRESHOLD_MODE, TTS_THRESHOLD_MODE);
        } else {
            content.remove(TTS_THRESHOLD_MODE);
        }

        if (defaultPanel.hasModeTTS_D()) {
            content.put(TTS_DIFFERENCE_MODE, TTS_DIFFERENCE_MODE);
        } else {
            content.remove(TTS_DIFFERENCE_MODE);
        }
    }

    /**
     *
     */
    private void fillSelectionMode() {
        final OptionsACTab acTab = OptionsACTab.getInstance();

        final ButtonModel selectedModel = acTab.getButtonGroup().getSelection();
        final String selectedActionCommand = selectedModel.getActionCommand();
        content.put(SELECTION_MODE, selectedActionCommand);

        final int plaf = Integer.parseInt(selectedActionCommand);

        switch (plaf) {
            case SELECTION_MODE_TREE:
                isAlternateSelectionMode = false;
                break;

            case SELECTION_MODE_TABLE:
                isAlternateSelectionMode = true;
                break;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see bensikin.bensikin.options.PushPullOptionBook#push()
     */
    @Override
    public void push() {
        pushSelectionMode();
        pushStackDepth();

        pushDefaultModes();
        pushDefaultValues();
    }

    /**
     *
     */
    private void pushStackDepth() {
        final OptionsACTab acTab = OptionsACTab.getInstance();

        String stackDepth_s = content.get(STACK_DEPTH);
        try {
            stackDepth = Integer.parseInt(stackDepth_s);
        } catch (final Exception e) {
            stackDepth = DEFAULT_STACK_DEPTH;
            stackDepth_s = String.valueOf(stackDepth);
            content.put(STACK_DEPTH, stackDepth_s);
        }
        acTab.setStackDepth(stackDepth_s);
        final OpenedACComboBox openedACComboBox = OpenedACComboBox.getInstance();
        if (openedACComboBox != null) {
            openedACComboBox.setMaxSize(stackDepth);
        }
    }

    /**
     *
     */
    private void pushDefaultValues() {
        final ACTabDefaultPanel defaultPanel = ACTabDefaultPanel.getInstance();
        String s;
        boolean b;
        int i;
        long l;

        // ---HDB periods
        s = content.get(HDB_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setHDBPeriod(i);
        }

        s = content.get(HDB_ABSOLUTE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setHDBAbsolutePeriod(i);
        }

        s = content.get(HDB_RELATIVE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setHDBRelativePeriod(i);
        }

        s = content.get(HDB_THRESHOLD_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setHDBThresholdPeriod(i);
        }

        s = content.get(HDB_DIFFERENCE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setHDBDifferencePeriod(i);
        }

        // ---TDB periods
        s = content.get(TDB_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTDBPeriod(i);
        }

        s = content.get(TDB_EXPORT_PERIOD);
        if (s != null && !s.isEmpty()) {
            l = Long.parseLong(s);
            defaultPanel.setExportPeriod(l);
        }

        s = content.get(TDB_ABSOLUTE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTDBAbsolutePeriod(i);
        }

        s = content.get(TDB_RELATIVE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTDBRelativePeriod(i);
        }

        s = content.get(TDB_THRESHOLD_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTDBThresholdPeriod(i);
        }

        s = content.get(TDB_DIFFERENCE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTDBDifferencePeriod(i);
        }

        // ---TTS periods
        s = content.get(TTS_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTTSPeriod(i);
        }

        s = content.get(TTS_ABSOLUTE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTTSAbsolutePeriod(i);
        }

        s = content.get(TTS_RELATIVE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTTSRelativePeriod(i);
        }

        s = content.get(TTS_THRESHOLD_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTTSThresholdPeriod(i);
        }

        s = content.get(TTS_DIFFERENCE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            defaultPanel.setTTSDifferencePeriod(i);
        }

        // ---HDB values
        s = content.get(HDB_ABSOLUTE_MODE_UPPER);
        defaultPanel.setHDBAbsoluteUpper(s);

        s = content.get(HDB_RELATIVE_MODE_UPPER);
        defaultPanel.setHDBRelativeUpper(s);

        s = content.get(HDB_THRESHOLD_MODE_UPPER);
        defaultPanel.setHDBThresholdUpper(s);

        s = content.get(HDB_ABSOLUTE_MODE_LOWER);
        defaultPanel.setHDBAbsoluteLower(s);

        s = content.get(HDB_RELATIVE_MODE_LOWER);
        defaultPanel.setHDBRelativeLower(s);

        s = content.get(HDB_THRESHOLD_MODE_LOWER);
        defaultPanel.setHDBThresholdLower(s);

        // ---TDB values
        s = content.get(TDB_ABSOLUTE_MODE_UPPER);
        defaultPanel.setTDBAbsoluteUpper(s);

        s = content.get(TDB_RELATIVE_MODE_UPPER);
        defaultPanel.setTDBRelativeUpper(s);

        s = content.get(TDB_THRESHOLD_MODE_UPPER);
        defaultPanel.setTDBThresholdUpper(s);

        s = content.get(TDB_ABSOLUTE_MODE_LOWER);
        defaultPanel.setTDBAbsoluteLower(s);

        s = content.get(TDB_RELATIVE_MODE_LOWER);
        defaultPanel.setTDBRelativeLower(s);

        s = content.get(TDB_THRESHOLD_MODE_LOWER);
        defaultPanel.setTDBThresholdLower(s);

        // ---TTS values
        s = content.get(TTS_ABSOLUTE_MODE_UPPER);
        defaultPanel.setTTSAbsoluteUpper(s);

        s = content.get(TTS_RELATIVE_MODE_UPPER);
        defaultPanel.setTTSRelativeUpper(s);

        s = content.get(TTS_THRESHOLD_MODE_UPPER);
        defaultPanel.setTTSThresholdUpper(s);

        s = content.get(TTS_ABSOLUTE_MODE_LOWER);
        defaultPanel.setTTSAbsoluteLower(s);

        s = content.get(TTS_RELATIVE_MODE_LOWER);
        defaultPanel.setTTSRelativeLower(s);

        s = content.get(TTS_THRESHOLD_MODE_LOWER);
        defaultPanel.setTTSThresholdLower(s);

        // ---HDB Slow Drift values
        b = Boolean.parseBoolean(content.get(HDB_ABSOLUTE_SLOW_DRIFT));
        defaultPanel.setHDBAbsoluteDLCheck(b);

        b = Boolean.parseBoolean(content.get(HDB_RELATIVE_SLOW_DRIFT));
        defaultPanel.setHDBRelativeDLCheck(b);

        // ---TDB Slow Drift values
        b = Boolean.parseBoolean(content.get(TDB_ABSOLUTE_SLOW_DRIFT));
        defaultPanel.setTDBAbsoluteDLCheck(b);

        b = Boolean.parseBoolean(content.get(TDB_RELATIVE_SLOW_DRIFT));
        defaultPanel.setTDBRelativeDLCheck(b);

        // ---TTS Slow Drift values
        b = Boolean.parseBoolean(content.get(TTS_ABSOLUTE_SLOW_DRIFT));
        defaultPanel.setTTSAbsoluteDLCheck(b);

        b = Boolean.parseBoolean(content.get(TTS_RELATIVE_SLOW_DRIFT));
        defaultPanel.setTTSRelativeDLCheck(b);

    }

    /**
     *
     */
    private void pushDefaultModes() {
        final ACTabDefaultPanel defaultPanel = ACTabDefaultPanel.getInstance();
        String s;

        defaultPanel.doUnselectAllModes();

        s = content.get(HDB_PERIODIC_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_P);
        }

        s = content.get(HDB_ABSOLUTE_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_A);
        }

        s = content.get(HDB_RELATIVE_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_R);
        }

        s = content.get(HDB_THRESHOLD_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_T);
        }

        s = content.get(HDB_DIFFERENCE_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_D);
        }

        s = content.get(TDB_PERIODIC_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_P);
        }

        s = content.get(TDB_ABSOLUTE_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_A);
        }

        s = content.get(TDB_RELATIVE_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_R);
        }

        s = content.get(TDB_THRESHOLD_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_T);
        }

        s = content.get(TDB_DIFFERENCE_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_D);
        }

        s = content.get(TTS_PERIODIC_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(null, ArchivingConfigurationMode.TYPE_P);
        }

        s = content.get(TTS_ABSOLUTE_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(null, ArchivingConfigurationMode.TYPE_A);
        }

        s = content.get(TTS_RELATIVE_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(null, ArchivingConfigurationMode.TYPE_R);
        }

        s = content.get(TTS_THRESHOLD_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(null, ArchivingConfigurationMode.TYPE_T);
        }

        s = content.get(TTS_DIFFERENCE_MODE);
        if (s != null && !s.isEmpty()) {
            defaultPanel.doSelectMode(null, ArchivingConfigurationMode.TYPE_D);
        }
    }

    public void pushSelectionMode() {
        final String plaf_s = content.get(SELECTION_MODE);

        if (plaf_s != null) {
            final int plaf = Integer.parseInt(plaf_s);
            boolean isAlternate = false;

            switch (plaf) {
                case SELECTION_MODE_TREE:
                    isAlternate = false;
                    break;

                case SELECTION_MODE_TABLE:
                    isAlternate = true;
                    break;
            }

            isAlternateSelectionMode = isAlternate;
            ArchivingActionPanel.setAlternateSelectionMode(isAlternate);

            final ACEditDialog dialog = ACEditDialog.getInstance();
            if (dialog != null) {
                dialog.setAlternateSelectionMode(isAlternate);
            }

            if (MamboToolBar.getCurrentInstance() != null) {
                MamboToolBar.getCurrentInstance().setAlternateSelection(isAlternate);
            }
            if (MamboMenuBar.getCurrentInstance() != null) {
                MamboMenuBar.getCurrentInstance().setAlternateSelection(isAlternate);
            }

            final OptionsACTab acTab = OptionsACTab.getInstance();
            acTab.selectIsAlternate(isAlternate);
        }
    }

    public void applyValueDefaultsMain() {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        String s;
        int i;

        // ---HDB periods
        s = content.get(HDB_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setHDBPeriod(i);
        }

        // ---TDB periods
        s = content.get(TDB_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTDBPeriod(i);
        }

        s = content.get(TDB_EXPORT_PERIOD);
        if (s != null && !s.isEmpty()) {
            i = Integer.parseInt(s);
            panel.setExportPeriod(i);
        }

        // ---TTS periods
        s = content.get(TTS_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTTSPeriod(i);
        }

    }

    public void applyValueDefaultsSub() {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        String s;
        boolean b;
        int i;

        // ---HDB periods
        s = content.get(HDB_ABSOLUTE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setHDBAbsolutePeriod(i);
        }

        s = content.get(HDB_RELATIVE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setHDBRelativePeriod(i);
        }

        s = content.get(HDB_THRESHOLD_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setHDBThresholdPeriod(i);
        }

        s = content.get(HDB_DIFFERENCE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setHDBDifferencePeriod(i);
        }

        // ---TDB periods
        s = content.get(TDB_ABSOLUTE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTDBAbsolutePeriod(i);
        }

        s = content.get(TDB_RELATIVE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTDBRelativePeriod(i);
        }

        s = content.get(TDB_THRESHOLD_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTDBThresholdPeriod(i);
        }

        s = content.get(TDB_DIFFERENCE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTDBDifferencePeriod(i);
        }

        // ---TTS periods
        s = content.get(TTS_ABSOLUTE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTTSAbsolutePeriod(i);
        }

        s = content.get(TTS_RELATIVE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTTSRelativePeriod(i);
        }

        s = content.get(TTS_THRESHOLD_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTTSThresholdPeriod(i);
        }

        s = content.get(TTS_DIFFERENCE_MODE_PERIOD);
        if (s != null && !s.isEmpty()) {
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            panel.setTTSDifferencePeriod(i);
        }

        // ---HDB values
        s = content.get(HDB_ABSOLUTE_MODE_UPPER);
        panel.setHDBAbsoluteUpper(s);

        s = content.get(HDB_RELATIVE_MODE_UPPER);
        panel.setHDBRelativeUpper(s);

        s = content.get(HDB_THRESHOLD_MODE_UPPER);
        panel.setHDBThresholdUpper(s);

        s = content.get(HDB_ABSOLUTE_MODE_LOWER);
        panel.setHDBAbsoluteLower(s);

        s = content.get(HDB_RELATIVE_MODE_LOWER);
        panel.setHDBRelativeLower(s);

        s = content.get(HDB_THRESHOLD_MODE_LOWER);
        panel.setHDBThresholdLower(s);

        // ---TDB values
        s = content.get(TDB_ABSOLUTE_MODE_UPPER);
        panel.setTDBAbsoluteUpper(s);

        s = content.get(TDB_RELATIVE_MODE_UPPER);
        panel.setTDBRelativeUpper(s);

        s = content.get(TDB_THRESHOLD_MODE_UPPER);
        panel.setTDBThresholdUpper(s);

        s = content.get(TDB_ABSOLUTE_MODE_LOWER);
        panel.setTDBAbsoluteLower(s);

        s = content.get(TDB_RELATIVE_MODE_LOWER);
        panel.setTDBRelativeLower(s);

        s = content.get(TDB_THRESHOLD_MODE_LOWER);
        panel.setTDBThresholdLower(s);

        // ---TTS values
        s = content.get(TTS_ABSOLUTE_MODE_UPPER);
        panel.setTTSAbsoluteUpper(s);

        s = content.get(TTS_RELATIVE_MODE_UPPER);
        panel.setTTSRelativeUpper(s);

        s = content.get(TTS_THRESHOLD_MODE_UPPER);
        panel.setTTSThresholdUpper(s);

        s = content.get(TTS_ABSOLUTE_MODE_LOWER);
        panel.setTTSAbsoluteLower(s);

        s = content.get(TTS_RELATIVE_MODE_LOWER);
        panel.setTTSRelativeLower(s);

        s = content.get(TTS_THRESHOLD_MODE_LOWER);
        panel.setTTSThresholdLower(s);

        // ---HDB slow drift values
        b = Boolean.parseBoolean(content.get(HDB_ABSOLUTE_SLOW_DRIFT));
        panel.setHDBAbsoluteDLCheck(b);

        b = Boolean.parseBoolean(content.get(HDB_RELATIVE_SLOW_DRIFT));
        panel.setHDBRelativeDLCheck(b);

        // ---TDB slow drift values
        b = Boolean.parseBoolean(content.get(TDB_ABSOLUTE_SLOW_DRIFT));
        panel.setTDBAbsoluteDLCheck(b);

        b = Boolean.parseBoolean(content.get(TDB_RELATIVE_SLOW_DRIFT));
        panel.setTDBRelativeDLCheck(b);

        // ---TTS slow drift values
        b = Boolean.parseBoolean(content.get(TTS_ABSOLUTE_SLOW_DRIFT));
        panel.setTTSAbsoluteDLCheck(b);

        b = Boolean.parseBoolean(content.get(TTS_RELATIVE_SLOW_DRIFT));
        panel.setTTSRelativeDLCheck(b);

    }

    public void applyDefaults() {
        applySelectionDefaults();
        applyValueDefaultsMain();
        applyValueDefaultsSub();
    }

    public void applySelectionDefaults() {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        String s;

        panel.doUnselectAllModes();

        s = content.get(HDB_PERIODIC_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_P);
        }

        s = content.get(HDB_ABSOLUTE_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_A);
        }

        s = content.get(HDB_RELATIVE_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_R);
        }

        s = content.get(HDB_THRESHOLD_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_T);
        }

        s = content.get(HDB_DIFFERENCE_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.TRUE, ArchivingConfigurationMode.TYPE_D);
        }

        s = content.get(TDB_PERIODIC_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_P);
        }

        s = content.get(TDB_ABSOLUTE_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_A);
        }

        s = content.get(TDB_RELATIVE_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_R);
        }

        s = content.get(TDB_THRESHOLD_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_T);
        }

        s = content.get(TDB_DIFFERENCE_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(Boolean.FALSE, ArchivingConfigurationMode.TYPE_D);
        }

        s = content.get(TTS_PERIODIC_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(null, ArchivingConfigurationMode.TYPE_P);
        }

        s = content.get(TTS_ABSOLUTE_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(null, ArchivingConfigurationMode.TYPE_A);
        }

        s = content.get(TTS_RELATIVE_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(null, ArchivingConfigurationMode.TYPE_R);
        }

        s = content.get(TTS_THRESHOLD_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(null, ArchivingConfigurationMode.TYPE_T);
        }

        s = content.get(TTS_DIFFERENCE_MODE);
        if (s != null && !s.isEmpty()) {
            panel.doSelectMode(null, ArchivingConfigurationMode.TYPE_D);
        }

    }

    public String toPropertiesString() {
        String ret = ObjectUtils.EMPTY_STRING;

        for (Entry<String, String> entry : content.entrySet()) {
            final String nextOptionKey = entry.getKey();
            final String nextOptionValue = entry.getValue();
            if (SELECTION_MODE.equals(nextOptionKey)) {
                continue;
            }

            final String nextLine = nextOptionKey + " = " + nextOptionValue;

            ret += nextLine;
            ret += GUIUtilities.CRLF;
        }

        return ret;
    }

    /**
     * @return Returns the stackDepth.
     */
    public int getStackDepth() {
        return stackDepth;
    }

    /**
     * @param stackDepth
     *            The stackDepth to set.
     */
    public void setStackDepth(final int stackDepth) {
        this.stackDepth = stackDepth;
    }

    /**
     * @return
     */
    public boolean isAlternateSelectionMode() {
        return isAlternateSelectionMode;
    }
}
