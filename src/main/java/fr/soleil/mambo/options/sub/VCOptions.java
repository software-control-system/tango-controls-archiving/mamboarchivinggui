package fr.soleil.mambo.options.sub;

import java.util.HashMap;
import java.util.Map;

import javax.swing.ButtonModel;

import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.properties.xml.ErrorPropertiesXmlManager;
import fr.soleil.comete.definition.widget.properties.xml.OffsetPropertiesXmlManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.bean.manager.ViewConfigurationBeanManager;
import fr.soleil.mambo.components.view.OpenedVCComboBox;
import fr.soleil.mambo.containers.sub.dialogs.options.OptionsVCTab;
import fr.soleil.mambo.containers.view.ViewSelectionPanel;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.data.view.plot.Bar;
import fr.soleil.mambo.data.view.plot.Curve;
import fr.soleil.mambo.data.view.plot.Interpolation;
import fr.soleil.mambo.data.view.plot.Marker;
import fr.soleil.mambo.data.view.plot.MathPlot;
import fr.soleil.mambo.data.view.plot.Polynomial2OrderTransform;
import fr.soleil.mambo.data.view.plot.Smoothing;
import fr.soleil.mambo.datasources.db.extracting.ExtractingManagerFactory;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;
import fr.soleil.mambo.options.PushPullOptionBook;
import fr.soleil.mambo.options.ReadWriteOptionBook;

public class VCOptions extends ReadWriteOptionBook implements PushPullOptionBook {

    public static final String DISPLAY_WRITE = "DISPLAY_WRITE";
    public static final int DISPLAY_WRITE_NO = 0;
    public static final int DISPLAY_WRITE_YES = 1;

    public static final String DISPLAY_READ = "DISPLAY_READ";
    public static final int DISPLAY_READ_NO = 0;
    public static final int DISPLAY_READ_YES = 1;

    public static final String STACK_DEPTH = "STACK_DEPTH";
    public static final int DEFAULT_STACK_DEPTH = 5;

    public static final String MAX_DISPLAYED_VIEWS = "MAX_DISPLAYED_VIEWS";
    public static final int DEFAULT_MAX_DISPLAYED_VIEWS = 2;

    public static final String DATA_FORMAT = "DATA_FORMAT";
    public static final String DEFAULT_DATA_FORMAT = "%16.8f";

    public static final String FORCE_TDB_EXPORT = "FORCE_TDB_EXPORT";
    public static final int FORCE_TDB_EXPORT_NO = 0;
    public static final int FORCE_TDB_EXPORT_YES = 1;

    public static final String CHART_NO_VALUE_STRING = "CHART_NO_VALUE_STRING";

    public static final String SPECTRUM_VIEW_TYPE = "SPECTRUM_VIEW_TYPE";
    public static final int DEFAULT_SPECTRUM_VIEW_TYPE = ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX;

    public static final String VIEW_TYPE = "viewType";
    public static final String AXIS_CHOICE = "axisChoice";

    public static final String KEY = "VCs"; // for XML save and load

    private boolean doForceTdbExport = false;
    private int stackDepth = DEFAULT_STACK_DEPTH;
    private int maxDisplayedViews = DEFAULT_MAX_DISPLAYED_VIEWS;
    private String dataFormat = DEFAULT_DATA_FORMAT;
    private int spectrumViewType = DEFAULT_SPECTRUM_VIEW_TYPE;
    private String noValueString = "*";

    /**
     *
     */
    public VCOptions() {
        super(KEY);
    }

    @Override
    public void fillFromOptionsDialog() {
        OptionsVCTab vcTab = OptionsVCTab.getInstance();

        ButtonModel selectedModel = vcTab.getWriteButtonGroup().getSelection();
        String selectedActionCommand = selectedModel.getActionCommand();
        content.put(DISPLAY_WRITE, selectedActionCommand);

        selectedModel = vcTab.getReadButtonGroup().getSelection();
        selectedActionCommand = selectedModel.getActionCommand();
        content.put(DISPLAY_READ, selectedActionCommand);

        selectedModel = vcTab.getForceTdbExportButtonGroup().getSelection();
        selectedActionCommand = selectedModel.getActionCommand();
        content.put(FORCE_TDB_EXPORT, selectedActionCommand);

        String stackDepth_s = vcTab.getStackDepth();
        try {
            stackDepth = Integer.parseInt(stackDepth_s);
            content.put(STACK_DEPTH, stackDepth_s);
        } catch (NumberFormatException e) {
            // If the new value is bad, we keep the last good one.
            // We could also put the default value:
            // content.put( STACK_DEPTH , String.valueOf(this.getStackDepth())
            // );
        }

        String maxDisplayedViews_s = vcTab.getMaxDisplayedViews();
        try {
            maxDisplayedViews = Integer.parseInt(maxDisplayedViews_s);
            content.put(MAX_DISPLAYED_VIEWS, maxDisplayedViews_s);
        } catch (NumberFormatException e) {
            // If the new value is bad, we keep the last good one.
            // We could also put the default value:
            // content.put( MAX_DISPLAYED_VIEWS ,
            // String.valueOf(this.getMaxDisplayedViews())
            // );
        }
        if (maxDisplayedViews > stackDepth) {
            maxDisplayedViews = stackDepth;
            maxDisplayedViews_s = Integer.toString(maxDisplayedViews);
            content.put(MAX_DISPLAYED_VIEWS, maxDisplayedViews_s);
        }

        String dataFormat_s = vcTab.getDataFormat();
        dataFormat = dataFormat_s;
        content.put(DATA_FORMAT, dataFormat_s);

        noValueString = vcTab.getNoValueString();
        content.put(CHART_NO_VALUE_STRING, noValueString);

        spectrumViewType = vcTab.getSelectedSpectrumViewType();
        if (spectrumViewType < 0) {
            spectrumViewType = DEFAULT_SPECTRUM_VIEW_TYPE;
        }
        content.put(SPECTRUM_VIEW_TYPE, String.valueOf(spectrumViewType));

        fillDefaultPlotProperties(vcTab);
    }

    @Override
    public void push() {
        OptionsVCTab vcTab = OptionsVCTab.getInstance();
        IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();

        String plaf_s = content.get(DISPLAY_WRITE);
        if (plaf_s != null) {
            int plaf = Integer.parseInt(plaf_s);

            switch (plaf) {
                case DISPLAY_WRITE_YES:
                    extractingManager.setShowWrite(true);
                    break;

                case DISPLAY_WRITE_NO:
                    extractingManager.setShowWrite(false);
                    break;
            }

            vcTab.selectDisplayWrite(plaf);
        }

        plaf_s = content.get(DISPLAY_READ);
        if (plaf_s != null) {
            int plaf = Integer.parseInt(plaf_s);

            switch (plaf) {
                case DISPLAY_READ_YES:
                    extractingManager.setShowRead(true);
                    break;

                case DISPLAY_READ_NO:
                    extractingManager.setShowRead(false);
                    break;
            }

            vcTab.selectDisplayRead(plaf);
        }

        plaf_s = content.get(FORCE_TDB_EXPORT);
        if (plaf_s != null) {

            int plaf = Integer.parseInt(plaf_s);

            switch (plaf) {
                case FORCE_TDB_EXPORT_YES:
                    this.setDoForceTdbExport(true);
                    break;

                case FORCE_TDB_EXPORT_NO:
                    this.setDoForceTdbExport(false);
                    break;

                default:
                    this.setDoForceTdbExport(false);
                    break;
            }

            vcTab.selectForceTdbExport(plaf);
        } else {
            this.setDoForceTdbExport(false);
            vcTab.selectForceTdbExport(FORCE_TDB_EXPORT_NO);
        }

        String stackDepth_s = content.get(STACK_DEPTH);
        try {
            stackDepth = Integer.parseInt(stackDepth_s);
        } catch (NumberFormatException e) {
            stackDepth = DEFAULT_STACK_DEPTH;
            stackDepth_s = String.valueOf(stackDepth);
            content.put(STACK_DEPTH, stackDepth_s);
        }
        vcTab.setStackDepth(stackDepth_s);
        OpenedVCComboBox openedVCComboBox = OpenedVCComboBox.getInstance();
        openedVCComboBox.setMaxSize(stackDepth);

        String maxDisplayedViews_s = content.get(MAX_DISPLAYED_VIEWS);
        try {
            maxDisplayedViews = Integer.parseInt(maxDisplayedViews_s);
        } catch (NumberFormatException e) {
            maxDisplayedViews = DEFAULT_MAX_DISPLAYED_VIEWS;
            maxDisplayedViews_s = String.valueOf(maxDisplayedViews);
            content.put(MAX_DISPLAYED_VIEWS, maxDisplayedViews_s);
        }
        if (maxDisplayedViews > stackDepth) {
            maxDisplayedViews = stackDepth;
            maxDisplayedViews_s = Integer.toString(maxDisplayedViews);
            content.put(MAX_DISPLAYED_VIEWS, maxDisplayedViews_s);
        }
        vcTab.setMaxDisplayedViews(maxDisplayedViews_s);
        ViewConfigurationBeanManager.getInstance().setMaximumDisplayedViews(maxDisplayedViews);

        String dataFormat_s = content.get(DATA_FORMAT);
        dataFormat = dataFormat_s;
        vcTab.setDataFormat(dataFormat_s);

        noValueString = content.get(CHART_NO_VALUE_STRING);
        noValueString = (noValueString == null ? "*" : noValueString);
        vcTab.setNoValueString(noValueString);
        content.put(CHART_NO_VALUE_STRING, noValueString);

        String spectrumViewType_s = content.get(SPECTRUM_VIEW_TYPE);
        if (spectrumViewType_s == null || spectrumViewType_s.trim().isEmpty()) {
            spectrumViewType = DEFAULT_SPECTRUM_VIEW_TYPE;
        } else {
            try {
                spectrumViewType = Integer.parseInt(spectrumViewType_s);
            } catch (NumberFormatException e) {
                spectrumViewType = DEFAULT_SPECTRUM_VIEW_TYPE;
                spectrumViewType_s = String.valueOf(spectrumViewType);
                content.put(SPECTRUM_VIEW_TYPE, spectrumViewType_s);
            }
        }
        vcTab.setSelectedSpectrumViewType(spectrumViewType);
        ViewSelectionPanel.getInstance().updateForceExport();

        pushDefaultPlotProperties(vcTab);
    }

    /**
     * Register into options default plot properties
     * 
     * @param vcTab the dialog for vc configuration
     */
    private void fillDefaultPlotProperties(OptionsVCTab vcTab) {
        PlotProperties plotProperties = vcTab.getDefaultPlotProperties();
        Map<String, String> mapPlotProperties = new HashMap<String, String>();

        BarProperties bar = plotProperties.getBar();
        CurveProperties curve = plotProperties.getCurve();
        InterpolationProperties interpolation = plotProperties.getInterpolation();
        MarkerProperties marker = plotProperties.getMarker();
        MathProperties math = plotProperties.getMath();
        TransformationProperties polynom = plotProperties.getTransform();
        SmoothingProperties smoothing = plotProperties.getSmoothing();

        if (bar != null && bar instanceof Bar) {
            mapPlotProperties.putAll(((Bar) bar).getBarPropertiesList());
        }
        if (curve != null && curve instanceof Curve) {
            mapPlotProperties.putAll(((Curve) curve).getCurvePropertiesList());
        }
        if (interpolation != null && interpolation instanceof Interpolation) {
            mapPlotProperties.putAll(((Interpolation) interpolation).getInterpolationPropertiesList());
        }
        if (marker != null && marker instanceof Marker) {
            mapPlotProperties.putAll(((Marker) marker).getMarkerPropertiesList());
        }
        if (math != null && math instanceof MathPlot) {
            mapPlotProperties.putAll(((MathPlot) math).getMathPlotPropertiesList());
        }
        if (polynom != null && polynom instanceof Polynomial2OrderTransform) {
            mapPlotProperties
                    .putAll(((Polynomial2OrderTransform) polynom).getPolynomial2OrderTransformPropertiesList());
        }
        if (smoothing != null && smoothing instanceof Smoothing) {
            mapPlotProperties.putAll(((Smoothing) smoothing).getSmoothingPropertiesList());
        }

        for (String key : mapPlotProperties.keySet()) {
            content.put(key, mapPlotProperties.get(key));
        }
        content.put(VIEW_TYPE, String.valueOf(plotProperties.getViewType()));
        content.put(AXIS_CHOICE, String.valueOf(plotProperties.getAxisChoice()));
    }

    /**
     * Push default plot properties options to the VC menu
     * 
     * @param vcTab the dialog for vc configuration
     */
    private void pushDefaultPlotProperties(OptionsVCTab vcTab) {

        Object viewTypeObj = content.get(VIEW_TYPE);
        Object axisChoiceObj = content.get(AXIS_CHOICE);

        // If we don't have an axis choice or a view type for curve, we use
        // default options instead of this method
        if (viewTypeObj != null && axisChoiceObj != null) {

            int viewType = Integer.parseInt(content.get(VIEW_TYPE));
            int axisChoice = Integer.parseInt(content.get(AXIS_CHOICE));

            BarProperties bar = new Bar(content);
            CurveProperties curve = new Curve(content);
            InterpolationProperties interpolation = new Interpolation(content);
            MarkerProperties marker = new Marker(content);
            ErrorProperties error = ErrorPropertiesXmlManager.loadErrorFromXmlNodeAttributes(content);
            MathProperties math = new MathPlot(content);
            TransformationProperties polynom = new Polynomial2OrderTransform(content);
            OffsetProperties xOffset = OffsetPropertiesXmlManager.loadOffsetFromXmlNodeAttributes(content);
            SmoothingProperties smoothing = new Smoothing(content);

            PlotProperties properties = new PlotProperties(viewType, axisChoice, null, ObjectUtils.EMPTY_STRING, bar,
                    curve, marker, error, polynom, xOffset, interpolation, smoothing, math);

            vcTab.setDefaultPlotProperties(properties);
        }
    }

    /**
     * @return Returns the doForceTdbExport.
     */
    public boolean isDoForceTdbExport() {
        return doForceTdbExport;
    }

    private void setDoForceTdbExport(boolean b) {
        doForceTdbExport = b;
    }

    /**
     * @return Returns the stackDepth.
     */
    public int getStackDepth() {
        return stackDepth;
    }

    /**
     * @param stackDepth
     *            The stackDepth to set.
     */
    public void setStackDepth(int stackDepth) {
        this.stackDepth = stackDepth;
    }

    /**
     * @return Returns the maxDisplayedViews.
     */
    public int getMaxDisplayedViews() {
        return maxDisplayedViews;
    }

    /**
     * @param maxDisplayedViews
     *            The maxDisplayedViews to set.
     */
    public void setMaxDisplayedViews(int maxDisplayedViews) {
        this.maxDisplayedViews = maxDisplayedViews;
    }

    /**
     * @return Returns the dataFormat.
     */
    public String getDataFormat() {
        return dataFormat;
    }

    /**
     * @param dataFormat
     *            The dataFormat to set.
     */
    public void setDataFormat(String dataFormat) {
        this.dataFormat = dataFormat;
    }

    public int getSpectrumViewType() {
        return spectrumViewType;
    }

    public void setSpectrumViewType(int spectrumViewType) {
        this.spectrumViewType = spectrumViewType;
    }

    public String getNoValueString() {
        return noValueString;
    }

    public void setNoValueString(String noValueString) {
        this.noValueString = noValueString;
    }

    public static PlotProperties getDefaultPlotProperties() {
        OptionsVCTab vcTab = OptionsVCTab.getInstance();
        PlotProperties plotProp = vcTab.getDefaultPlotProperties();
        return plotProp;
    }

}
