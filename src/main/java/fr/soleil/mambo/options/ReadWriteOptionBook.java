package fr.soleil.mambo.options;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

public class ReadWriteOptionBook {

    protected Map<String, String> content;
    protected String tagName;

    public static final String OPTION_TAG = "option";
    public static final String NAME_TAG = "name";
    public static final String VALUE_TAG = "value";

    /**
     * @param tagName
     */
    protected ReadWriteOptionBook(String tagName) {
        content = new ConcurrentHashMap<>();
        this.tagName = tagName;
    }

    @Override
    public String toString() {
        return toStringBuilder(null).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder builder) {
        StringBuilder result = builder;
        if (result == null) {
            result = new StringBuilder();
        }
        XMLLine openingLine = new XMLLine(tagName, XMLLine.OPENING_TAG_CATEGORY);
        XMLLine closingLine = new XMLLine(tagName, XMLLine.CLOSING_TAG_CATEGORY);

        openingLine.toAppendable(result).append(GUIUtilities.CRLF);
        for (Entry<String, String> entry : content.entrySet()) {
            String nextOptionKey = entry.getKey();
            String nextOptionValue = entry.getValue();

            XMLLine nextLine = new XMLLine(OPTION_TAG, XMLLine.EMPTY_TAG_CATEGORY);
            nextLine.setAttribute(NAME_TAG, nextOptionKey);
            nextLine.setAttribute(VALUE_TAG, nextOptionValue);

            nextLine.toAppendable(result).append(GUIUtilities.CRLF);
        }
        closingLine.toAppendable(result);
        return result;
    }

    public void putOption(String key, String value) {
        content.put(key, value);
    }

    public String getOption(String key) {
        String ret = content.get(key);
        return ret;
    }

    public void build(List<Map<String, String>> options) {
        if (options != null) {
            for (Map<String, String> nextOption : options) {
                String nextName = nextOption.get(ReadWriteOptionBook.NAME_TAG);
                String nextValue = nextOption.get(ReadWriteOptionBook.VALUE_TAG);
                putOption(nextName, nextValue);
            }
        }
    }

    /**
     * @return Returns the content.
     */
    public Map<String, String> getContent() {
        return content;
    }

    /**
     * @param content
     *            The content to set.
     */
    public void setContent(Map<String, String> content) {
        this.content = content;
    }
}
