package fr.soleil.mambo.options.manager;

import fr.soleil.mambo.options.sub.ACOptions;

public class DummyACDefaultsManager implements IACDefaultsManager {

    public DummyACDefaultsManager() {
        super();
    }

    @Override
    public void saveACDefaults(ACOptions options) throws Exception {
    }

    @Override
    public ACOptions loadACDefaults() throws Exception {
        return null;
    }

    @Override
    public void setSaveLocation(String location) {
    }

    @Override
    public void setDefault(boolean _default) {
    }

    @Override
    public String getDefaultSaveLocation() {
        return null;
    }

    @Override
    public String getACDefaultsPropertyValue(String key) {
        return null;
    }

}
