package fr.soleil.mambo.options.manager;

import fr.soleil.mambo.options.Options;

public class DummyOptionsManager implements IOptionsManager {

    @Override
    public void saveOptions(Options options, String optionsResourceLocation) throws Exception {
    }

    @Override
    public Options loadOptions(String optionsResourceLocation) throws Exception {
        Options.getInstance();
        return null;
    }

}
