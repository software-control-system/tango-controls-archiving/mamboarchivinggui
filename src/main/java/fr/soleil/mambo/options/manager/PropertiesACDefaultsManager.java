package fr.soleil.mambo.options.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.options.sub.ACOptions;

public class PropertiesACDefaultsManager implements IACDefaultsManager {

    private String defaultSaveLocation;
    private String saveLocation;
    private PrintWriter writer;
    private boolean isDefault;

    private ResourceBundle defaultBundle;

    public PropertiesACDefaultsManager() {
        startUp();
    }

    public void startUp() throws IllegalStateException {
        String resourceLocation = null;
        boolean illegal = false;

        final String acPath = Mambo.getPathToResources() + "/acd";

        File f = null;
        try {
            f = new File(acPath);
            if (!f.canWrite()) {
                illegal = true;
            }
        } catch (final Exception e) {
            illegal = true;
        }

        if (illegal) {
            f.mkdir();
        }
        resourceLocation = acPath;
        defaultSaveLocation = resourceLocation;

        defaultBundle = ResourceBundle.getBundle("fr.soleil.mambo.resources.defaults.ac");
    }

    @Override
    public void saveACDefaults(final ACOptions options) throws Exception {
        final String _location = saveLocation == null ? defaultSaveLocation : saveLocation;
        writer = new PrintWriter(new FileWriter(_location, false));
        GUIUtilities.writeWithoutDate(writer, options.toPropertiesString(), true);
        writer.close();
    }

    @Override
    public ACOptions loadACDefaults() throws Exception {
        final ACOptions ret = new ACOptions();
        final Map<String, String> contentMap = new ConcurrentHashMap<>();
        if (isDefault) {
            final Enumeration<String> keys = defaultBundle.getKeys();
            while (keys.hasMoreElements()) {
                final String nextKey = keys.nextElement();
                final String nextVal = defaultBundle.getString(nextKey);
                contentMap.put(nextKey, nextVal);
            }
        } else {
            final Properties prop = new Properties();
            final String location = (saveLocation == null ? defaultSaveLocation : saveLocation);
            final FileInputStream inStream = new FileInputStream(location);
            prop.load(inStream);
            for (Entry<Object, Object> entry : prop.entrySet()) {
                Object key = entry.getKey();
                Object value = entry.getValue();
                if ((key != null) && (value != null)) {
                    contentMap.put(key.toString(), value.toString());
                }
            }
        }
        ret.setContent(contentMap);
        return ret;
    }

    /**
     * @param saveLocation
     *            The saveLocation to set.
     */
    @Override
    public void setSaveLocation(final String saveLocation) {
        this.saveLocation = saveLocation;
    }

    /**
     * @param isDefault
     *            The isDefault to set.
     */
    @Override
    public void setDefault(final boolean isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public String getACDefaultsPropertyValue(final String key) {
        return defaultBundle.getString(key);
    }

    /**
     * @return Returns the defaultSaveLocation.
     */
    @Override
    public String getDefaultSaveLocation() {
        return defaultSaveLocation;
    }
}
