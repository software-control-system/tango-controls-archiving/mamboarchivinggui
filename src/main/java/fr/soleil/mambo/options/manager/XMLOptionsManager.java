// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/options/manager/XMLOptionsManager.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  XMLOptionsManager.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: pierrejoseph $
//
// $Revision: 1.5 $
//
// $Log: XMLOptionsManager.java,v $
// Revision 1.5  2007/02/01 14:14:09  pierrejoseph
// XmlHelper reorg
//
// Revision 1.4  2006/05/19 15:05:29  ounsy
// minor changes
//
// Revision 1.3  2005/12/15 11:45:39  ounsy
// "copy table to clipboard" management
//
// Revision 1.2  2005/11/29 18:27:07  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:44  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.options.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.manager.XMLDataManager;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.sub.ACOptions;
import fr.soleil.mambo.options.sub.DisplayOptions;
import fr.soleil.mambo.options.sub.GeneralOptions;
import fr.soleil.mambo.options.sub.PrintOptions;
import fr.soleil.mambo.options.sub.SaveOptions;
import fr.soleil.mambo.options.sub.VCOptions;
import fr.soleil.mambo.options.sub.WordlistOptions;

public class XMLOptionsManager extends XMLDataManager<Options, Map<String, List<Map<String, String>>>>
        implements IOptionsManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLOptionsManager.class);

    @Override
    public void saveOptions(Options options, String optionsResourceLocation) throws ArchivingException {
        saveData(options, optionsResourceLocation);
    }

    @Override
    public Options loadOptions(String optionsResourceLocation) throws ArchivingException {
        Options ret = Options.getInstance();

        Map<String, List<Map<String, String>>> optionsHt = loadDataIntoHash(optionsResourceLocation);

        // START BUILDING SUB OPTIONS
        List<Map<String, String>> options;

        options = optionsHt.get(DisplayOptions.KEY);
        DisplayOptions displayOptions = new DisplayOptions();
        displayOptions.build(options);

        options = optionsHt.get(PrintOptions.KEY);
        PrintOptions printOptions = new PrintOptions();
        printOptions.build(options);

        options = optionsHt.get(SaveOptions.KEY);
        SaveOptions saveOptions = new SaveOptions();
        saveOptions.build(options);

        options = optionsHt.get(WordlistOptions.KEY);
        WordlistOptions wordlistOptions = new WordlistOptions();
        wordlistOptions.build(options);

        options = optionsHt.get(GeneralOptions.KEY);
        GeneralOptions generalOptions = new GeneralOptions();
        generalOptions.build(options);

        options = optionsHt.get(ACOptions.KEY);
        ACOptions acOptions = new ACOptions();
        if (options != null) {
            acOptions.build(options);
        }

        options = optionsHt.get(VCOptions.KEY);
        VCOptions vcOptions = new VCOptions();
        if (options != null) {
            vcOptions.build(options);
        }

        // END BUILDING SUB OPTIONS

        // START BUILDING OPTIONS
        ret.setDisplayOptions(displayOptions);
        ret.setPrintOptions(printOptions);
        ret.setSaveOptions(saveOptions);
        ret.setWordlistOptions(wordlistOptions);
        ret.setGeneralOptions(generalOptions);
        ret.setAcOptions(acOptions);
        ret.setVcOptions(vcOptions);
        // END BUILDING OPTIONS

        return ret;

    }

    /**
     * @param currentBookNode
     * @return 5 juil. 2005
     */
    private List<Map<String, String>> loadOptionBook(Node currentBookNode) {
        List<Map<String, String>> book = new ArrayList<>();
        if (currentBookNode.hasChildNodes()) {
            NodeList contextChapterNodes = currentBookNode.getChildNodes();
            // as many loops as there are options in the current options sub-block
            for (int i = 0; i < contextChapterNodes.getLength(); i++) {
                Node currentOptionChapterNode = contextChapterNodes.item(i);
                if (XMLUtils.isAFakeNode(currentOptionChapterNode)) {
                    continue;
                }
                Map<String, String> currentOptionChapter = null;
                try {
                    currentOptionChapter = XMLUtils.loadAttributes(currentOptionChapterNode);
                } catch (Exception e) {
                    LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                }
                book.add(currentOptionChapter);
            }
        }
        return book;
    }

    @Override
    protected Options getDefaultData() {
        return Options.getInstance();
    }

    @Override
    protected Map<String, List<Map<String, String>>> loadDataIntoHashFromRoot(Node rootNode) throws ArchivingException {
        Map<String, List<Map<String, String>>> options = null;

        if (rootNode.hasChildNodes()) {
            NodeList bookNodes = rootNode.getChildNodes();
            options = new ConcurrentHashMap<>();

            // As many loops as there are options sub-blocks in the saved file
            // (which is normally five: display, print, logs, save ,and wordlist)
            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentBookNode = bookNodes.item(i);
                if (XMLUtils.isAFakeNode(currentBookNode)) {
                    continue;
                }
                String currentBookType = currentBookNode.getNodeName().trim();
                List<Map<String, String>> currentBook = loadOptionBook(currentBookNode);
                options.put(currentBookType, currentBook);
            }
        }
        return options;
    }

}
