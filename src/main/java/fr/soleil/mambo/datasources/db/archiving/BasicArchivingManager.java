package fr.soleil.mambo.datasources.db.archiving;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.AttributesArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.api.ApiUtils;
import fr.soleil.mambo.api.archiving.ArchiverFactory;
import fr.soleil.mambo.api.archiving.ArchivingUtils;
import fr.soleil.mambo.api.archiving.IArchiver;
import fr.soleil.mambo.api.archiving.IArchivingManagerApi;
import fr.soleil.mambo.api.archiving.IBufferedArchivingManagerApi;
import fr.soleil.mambo.api.db.TDBDataBaseAccess;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributes;
import fr.soleil.mambo.datasources.db.DbConnectionManager;
import fr.soleil.mambo.tools.Messages;

public class BasicArchivingManager extends DbConnectionManager implements IArchivingManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicArchivingManager.class);
    protected static final String ERROR_SEPARATOR = "\n*----------------------------*\n";

    public BasicArchivingManager() throws ArchivingException {
        super();
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    public void startArchiving(ArchivingConfiguration ac, ArchivingConfigurationAttribute... attributes)
            throws ArchivingException {
    }

    @Override
    public void startArchiving(final ArchivingConfiguration AC) throws ArchivingException {
    }

    @Override
    public void stopArchiving(ArchivingConfiguration ac, ArchivingConfigurationAttribute... attributes)
            throws ArchivingException {
        if ((ac != null) && (attributes != null)) {
            final ArrayList<String> attributeListFinal = new ArrayList<String>();
            final AttributesArchivingException toThrow = new AttributesArchivingException();
            final Boolean historic = ac.isHistoric();
            IArchivingManagerApi manager;
            String dbName;
            if (historic == null) {
                manager = ttsManager;
                dbName = ApiConstants.TTS;
            } else if (historic.booleanValue()) {
                manager = hdbManager;
                dbName = ApiConstants.HDB;
            } else {
                manager = tdbManager;
                dbName = ApiConstants.TDB;
            }
            try {
                int exceptionCounter = 1;
                String exceptionMessage = ObjectUtils.EMPTY_STRING;
                openConnection();
                for (ArchivingConfigurationAttribute next : attributes) {
                    final String nextName = next.getCompleteName();
                    try {
                        if (manager != null && manager.isArchived(nextName)) {
                            attributeListFinal.add(nextName);
                        }
                    } catch (final ArchivingException e1) {
                        exceptionMessage = ERROR_SEPARATOR;
                        exceptionMessage += Messages.getMessage("LOGS_ERROR_NUMBER") + exceptionCounter++ + "\n";
                        exceptionMessage += Messages.getMessage("LOGS_ATTRIBUTE_IN_ERROR") + " " + nextName + "\n";
                        exceptionMessage += Messages.getMessage("LOGS_CONCERNED_DATABASE") + " " + dbName + "\n";
                        exceptionMessage += e1.getMessage();
                        toThrow.addStack(exceptionMessage, e1);
                        if (e1 instanceof AttributesArchivingException) {
                            AttributesArchivingException ae1 = (AttributesArchivingException) e1;
                            toThrow.getFaultingAttributes().addAll(ae1.getFaultingAttributes());
                        }
                    }
                } // end for (int i = 0; i < nbOfAttributes; i++)

                try {
                    if (manager != null && !attributeListFinal.isEmpty()) {
                        String[] attributeListFinal_s = new String[0];
                        attributeListFinal_s = attributeListFinal.toArray(attributeListFinal_s);
                        manager.archivingStop(attributeListFinal_s);
                    }
                } catch (final ArchivingException e1) {
                    exceptionMessage = ERROR_SEPARATOR;
                    exceptionMessage += Messages.getMessage("LOGS_ERROR_NUMBER") + exceptionCounter++ + "\n";
                    exceptionMessage += Messages.getMessage("LOGS_CONCERNED_DATABASE") + " " + dbName + "\n";
                    exceptionMessage += e1.getMessage();

                    toThrow.addStack(exceptionMessage, e1);
                    if (e1 instanceof AttributesArchivingException) {
                        AttributesArchivingException ae1 = (AttributesArchivingException) e1;
                        toThrow.getFaultingAttributes().addAll(ae1.getFaultingAttributes());
                    }
                }
                if (exceptionCounter > 1) {
                    throw toThrow;
                }
            } finally {
                // TANGOARCH-838: clean attributes buffer after stop, to force reading the information in database.
                if (manager instanceof IBufferedArchivingManagerApi) {
                    IBufferedArchivingManagerApi bufferedManager = (IBufferedArchivingManagerApi) manager;
                    bufferedManager.cleanBuffer(ApiUtils.getAttributeNames(EMPTY, attributes));
                }
            }
        } // end if ((ac != null) && (attributes != null))
    }

    @Override
    public void stopArchiving(final ArchivingConfiguration ac) throws ArchivingException {
        if (ac != null) {
            final ArchivingConfigurationAttributes acAttributes = ac.getAttributes();
            final ArchivingConfigurationAttribute[] attributes = acAttributes.getAttributesList();
            stopArchiving(ac, attributes);
        }
    }

    @Override
    public Mode getArchivingMode(final String completeName, final Boolean historic) throws ArchivingException {
        String[] modeStr = null;
        try {
            IArchivingManagerApi ref = getArchivingManagerApiInstance(historic);
            if ((ref != null) && (completeName != null)) {
                modeStr = ref.getArchivingMode(completeName);
            }
        } catch (final Exception e) {
            String msg = "Failed to get archiving mode for " + completeName;
            getLogger().debug(msg, e);
        }
        Mode mode;
        if (modeStr == null) {
            mode = null;
        } else {
            Map<String, String> accumulator = new HashMap<>();
            mode = ArchivingUtils.toMode(ApiUtils.split(modeStr, accumulator));
            accumulator.clear();
        }
        return mode;
    }

    @Override
    public boolean isArchived(final String completeName, final Boolean historic) throws ArchivingException {
        boolean ret = false;
        IArchivingManagerApi ref = getArchivingManagerApiInstance(historic);
        if ((ref != null) && (completeName != null)) {
            ret = ref.isArchived(completeName);
        }
        return ret;
    }

    @Override
    public IArchiver getCurrentArchiverForAttribute(final String completeName, final Boolean historic)
            throws ArchivingException {
        openConnection();
        final String archiverName = getDataBaseApi(historic).getArchiverForAttribute(completeName);
        return ArchiverFactory.getArchiver(archiverName, historic);
    }

    @Override
    public void exportData2Tdb(final String attributeName, final String endDate) throws ArchivingException {
        ((TDBDataBaseAccess) tdbManager.getDataBase()).getTdbExport().ExportData2Tdb(attributeName, endDate);
    }
}
