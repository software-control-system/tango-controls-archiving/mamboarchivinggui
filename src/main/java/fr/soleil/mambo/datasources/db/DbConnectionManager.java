package fr.soleil.mambo.datasources.db;

import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.utils.DbConnectionInfo;
import fr.soleil.archiving.hdbtdb.api.HdbTdbConnectionParameters;
import fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRefFactory;
import fr.soleil.archiving.hdbtdb.api.manager.HdbArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.manager.IArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.manager.TdbArchivingManagerApiRef;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.api.ApiUtils;
import fr.soleil.mambo.api.archiving.ArchivingUtils;
import fr.soleil.mambo.api.archiving.BufferedArchivingManagerApi;
import fr.soleil.mambo.api.archiving.IArchivingManagerApi;
import fr.soleil.mambo.api.archiving.LegacyArchivingManagerApi;
import fr.soleil.mambo.api.archiving.TTSArchivingManagerApi;
import fr.soleil.mambo.api.db.DbConnectionParameters;
import fr.soleil.mambo.api.db.HDBDataBaseAccess;
import fr.soleil.mambo.api.db.IDataBaseAcess;
import fr.soleil.mambo.api.db.TDBDataBaseAccess;
import fr.soleil.mambo.api.db.TTSDataBaseAccess;
import fr.soleil.mambo.data.IConfiguration;

public class DbConnectionManager implements ApiConstants {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbConnectionManager.class);

    protected static IArchivingManagerApi hdbManager, tdbManager, ttsManager;
    protected static IDataBaseAcess hdbAccess, tdbAccess, ttsAccess;
    protected static boolean isReady = false;

    public DbConnectionManager() throws ArchivingException {
        openConnection();
    }

    protected Logger getLogger() {
        return LOGGER;
    }

    private void logTTSConnectionError(Exception error) {
        Logger tmp = LoggerFactory.getLogger(HdbTdbConnectionParameters.class);
        String msg = "Failed to connect to TTS";
        if (tmp.isErrorEnabled() && !LOGGER.isErrorEnabled()) {
            if (error == null) {
                tmp.error(msg);
            } else {
                tmp.error(msg, error);
            }
        } else if (error == null) {
            LOGGER.error(msg);
        } else {
            LOGGER.error(msg, error);
        }
    }

    private void logTTSInformation(DataBaseParameters params) {
        StringBuilder builder = new StringBuilder(TTS);
        builder.append('\n').append(DbConnectionInfo.DB_HOST).append(IConfiguration.DISPAY_SEPARATOR)
                .append(params.getHost());
        builder.append('\n').append(PORT).append(IConfiguration.DISPAY_SEPARATOR)
                .append(ApiUtils.getProperty(TTS_PORT_PROPERTY, TTS_PORT_ENV, DEFAULT));
        builder.append('\n').append(DB_NAME).append(IConfiguration.DISPAY_SEPARATOR).append(params.getName());
        builder.append('\n').append(DbConnectionInfo.DB_USER).append(IConfiguration.DISPAY_SEPARATOR)
                .append(params.getUser());
        Logger tmp = LoggerFactory.getLogger(HdbTdbConnectionParameters.class);
        if (tmp.isInfoEnabled() && !LOGGER.isInfoEnabled()) {
            tmp.info(builder.toString());
        } else {
            LOGGER.info(builder.toString());
        }
        builder.delete(0, builder.length());
    }

    // centralized method for database connection
    private void connectDataBase(final Boolean historic) throws ArchivingException {
        IArchivingManagerApi manager = null;
        DataBaseParameters params;
        boolean bufferedArchivingInfo = ApiUtils.getBooleanProperty(BUFFERED_ARCHIVING_INFO_PROPERTY,
                BUFFERED_ARCHIVING_INFO_ENV, true); // activate buffer by default
        if (historic == null) {
            // TTS
            try {
                params = ApiUtils.getDataBaseParameters(historic);
                TTSDataBaseAccess ttsAccess = new TTSDataBaseAccess(params);
                DbConnectionManager.ttsAccess = ttsAccess;
                // TANGOARCH-838: buffer management.
                TTSArchivingManagerApi api = new TTSArchivingManagerApi(ttsAccess, params,
                        ArchivingUtils.TTS_DEVICE_CLASS);
                ttsManager = bufferedArchivingInfo ? new BufferedArchivingManagerApi(api) : api;
                manager = ttsManager;
                logTTSInformation(params);
            } catch (Exception e) {
                logTTSConnectionError(e);
                if (e instanceof ArchivingException) {
                    throw (ArchivingException) e;
                } else {
                    throw new ArchivingException(e);
                }
            }
        } else if (historic.booleanValue()) {
            // HDB
            params = ApiUtils.getDataBaseParameters(historic);
            final AbstractDataBaseConnector connector = ConnectionFactory.connect(params);
            IArchivingManagerApiRef managerRef = ArchivingManagerApiRefFactory.getInstance(true, connector);
            hdbAccess = new HDBDataBaseAccess((HdbArchivingManagerApiRef) managerRef, connector);
            // TANGOARCH-838: buffer management.
            LegacyArchivingManagerApi api = new LegacyArchivingManagerApi(hdbAccess, managerRef, true);
            hdbManager = bufferedArchivingInfo ? new BufferedArchivingManagerApi(api) : api;
            manager = hdbManager;
            HdbTdbConnectionParameters.printHDBConnectionInfoLog();
        } else {
            // TDB
            params = ApiUtils.getDataBaseParameters(historic);
            final AbstractDataBaseConnector connector = ConnectionFactory.connect(params);
            IArchivingManagerApiRef managerRef = ArchivingManagerApiRefFactory.getInstance(false, connector);
            tdbAccess = new TDBDataBaseAccess((TdbArchivingManagerApiRef) managerRef, connector);
            // TANGOARCH-838: buffer management.
            LegacyArchivingManagerApi api = new LegacyArchivingManagerApi(tdbAccess, managerRef, false);
            tdbManager = bufferedArchivingInfo ? new BufferedArchivingManagerApi(api) : api;
            manager = tdbManager;
            HdbTdbConnectionParameters.printTDBConnectionInfoLog();
        }
        if (manager == null) {
            String message = "Failed to initialize ArchivingManager API for ";
            if (historic == null) {
                message += ApiConstants.TTS;
            } else if (historic.booleanValue()) {
                message += ApiConstants.HDB;
            } else {
                message += ApiConstants.TDB;
            }
            LOGGER.error(message);
        } else {
            if (Mambo.canChooseArchivers()) {
                manager.archivingConfigure();
            } else {
                manager.archivingConfigureWithoutArchiverListInit();
            }
        }
    }

    public static IArchivingManagerApi getArchivingManagerApiInstance(final Boolean historic) {
        IArchivingManagerApi manager;
        if (historic == null) {
            manager = ttsManager;
        } else if (historic.booleanValue()) {
            manager = hdbManager;
        } else {
            manager = tdbManager;
        }
        return manager;
    }

    public IDataBaseAcess getDataBaseApi(final Boolean historic) throws ArchivingException {
        openConnection();
        IArchivingManagerApi api = getArchivingManagerApiInstance(historic);
        if (api == null) {
            String msg;
            if (historic == null) {
                msg = DbConnectionParameters.isTtsAvailable() ? "connection to TTS impossible" : null;
            } else if (historic.booleanValue()) {
                msg = DbConnectionParameters.isHdbAvailable() ? "connection to HDB impossible" : null;
            } else {
                msg = DbConnectionParameters.isTdbAvailable() ? "connection to TDB impossible" : null;
            }
            if (msg != null) {
                throw new ArchivingException(msg);
            }
        }
        return api == null ? null : api.getDataBase();
    }

    private ArchivingException connectDB(String name, Supplier<Boolean> method, Boolean historic) {
        ArchivingException failure = null;
        // If DB is not available it is not useful to connect to this DB
        try {
            // check TTS connection parameters
            if (method.get()) {
                connectDataBase(historic);
            }
        } catch (final ArchivingException ae) {
            getLogger().error("Failed to connect to " + name, ae);
            failure = ae;
            if (historic == null) {
                ttsManager = null;
            } else if (historic.booleanValue()) {
                hdbManager = null;
            } else {
                tdbManager = null;
            }
        } catch (final Exception e) {
            getLogger().error("Failed to connect to " + name, e);
        }
        return failure;
    }

    public void openConnection() throws ArchivingException {
        if (!isReady) {
            ArchivingException hdbFailure = null, tdbFailure = null, ttsFailure = null;

            // If TTS is not available it is not useful to connect to this DB
            ttsFailure = connectDB(ApiConstants.TTS, DbConnectionParameters::isTtsAvailable, null);

            // If HDB is not available it is not useful to connect to this DB
            hdbFailure = connectDB(ApiConstants.HDB, DbConnectionParameters::isHdbAvailable, Boolean.TRUE);

            // If TDB is not available it is not useful to connect to this DB
            tdbFailure = connectDB(ApiConstants.TDB, DbConnectionParameters::isTdbAvailable, Boolean.FALSE);

            if (ttsFailure != null) {
                throw ttsFailure;
            }

            if (hdbFailure != null) {
                throw hdbFailure;
            }

            if (tdbFailure != null) {
                throw tdbFailure;
            }

            isReady = true;
        }
    }

}
