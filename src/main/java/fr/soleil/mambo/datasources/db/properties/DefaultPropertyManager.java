package fr.soleil.mambo.datasources.db.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.GetArchivingConf;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;

/**
 * This is an {@link IPropertyManager} that really checks for degraded modes
 * 
 * @author awo
 */
public class DefaultPropertyManager implements IPropertyManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPropertyManager.class);

    private final static String DEGRAD_PROPERTY = "isDegradMode";

    private boolean hdbDegrad;
    private boolean tdbDegrad;

    public DefaultPropertyManager() {
        super();
        hdbDegrad = false;
        tdbDegrad = false;
        readProperties();
    }

    private void readProperties() {
        readHdbDegradFromDb();
        readTdbDegradFromDb();
    }

    private void readHdbDegradFromDb() {
        try {
            // check HdbArchiver class properties
            hdbDegrad = GetArchivingConf.readBooleanInDB(ConfigConst.HDB_CLASS_DEVICE, DEGRAD_PROPERTY);
        } catch (ArchivingException e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            hdbDegrad = false;
        }
    }

    private void readTdbDegradFromDb() {
        try {
            // check TdbArchiver class properties
            tdbDegrad = GetArchivingConf.readBooleanInDB(ConfigConst.TDB_CLASS_DEVICE, DEGRAD_PROPERTY);
        } catch (ArchivingException e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            tdbDegrad = false;
        }
    }

    @Override
    public boolean isHdbDegrad() {
        return hdbDegrad;
    }

    @Override
    public boolean isTdbDegrad() {
        return tdbDegrad;
    }
}
