/*
 * Synchrotron Soleil File : BasicExtractingManager.java Project : mambo
 * Description : Author : CLAISSE Original : 22 sept. 2006 Revision: Author:
 * Date: State: Log: BasicExtractingManager.java,v
 */
/*
 * Created on 22 sept. 2006 To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.mambo.datasources.db.extracting;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.ArchivingDateConstants;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.mambo.api.db.IDataBaseAcess;
import fr.soleil.mambo.api.extract.IAttributeExtractor;
import fr.soleil.mambo.datasources.db.DbConnectionManager;

public class BasicExtractingManager extends DbConnectionManager implements IExtractingManager, ArchivingDateConstants {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicExtractingManager.class);

    /**
     * Map that knows the created and used AttributeExtractor. Mandatory because
     * DataBaseManager.getExtractor() always creates a new AttributeExtractor
     */
    private final Set<IAttributeExtractor> knownExtractors = Collections.newSetFromMap(new ConcurrentHashMap<>());

    private boolean showRead = true;
    private boolean showWrite = true;
    private volatile boolean hdbCanceled = false, tdbCanceled = false, ttsCanceled = false;

    public BasicExtractingManager() throws ArchivingException {
        super();
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    public double[] getMinAndMax(final Boolean historic, final String... param) throws ArchivingException {
        final double[] minmax = new double[2];
        final IDataBaseAcess db = getDataBaseApi(historic);
        IAttributeExtractor minExtractor = getExtractor(db);
        IAttributeExtractor maxEtractor = getExtractor(db);
        try {
            minmax[0] = minExtractor.getAttDataMinBetweenDates(param);
            minmax[1] = maxEtractor.getAttDataMaxBetweenDates(param);
        } finally {
            cleanExtractor(minExtractor);
            cleanExtractor(maxEtractor);
        }
        return minmax;
    }

    private IAttributeExtractor[] getExtractorsToCancel() {
        return knownExtractors.toArray(new IAttributeExtractor[knownExtractors.size()]);
    }

    private IAttributeExtractor getExtractor(IDataBaseAcess db) throws ArchivingException {
        IAttributeExtractor result = null;
        if (db != null) {
            result = db.getExtractor();
            if (result != null) {
                knownExtractors.add(result);
                result.setCanceled(false);
            }
        }
        return result;
    }

    private void cleanExtractor(IAttributeExtractor extractor) {
        if (extractor != null) {
            knownExtractors.remove(extractor);
        }
    }

    @Override
    public DbData[] retrieveData(final Boolean historic, final SamplingType samplingType, final String... param) {
        DbData[] result = null;
        if (!isCanceled(historic)) {
            getLogger().info(new StringBuilder("extracting ").append(param[0]).append(" from DB...").toString());
            final long t1 = System.currentTimeMillis();
            result = getDbDataWithSampling(historic, samplingType, param);
            final long t2 = System.currentTimeMillis();
            getLogger().info(DateUtil
                    .elapsedTimeToStringBuilder(
                            new StringBuilder("DB extraction for ").append(param[0]).append(" took "), t2 - t1)
                    .toString());
        }
        return result;
    }

    @Override
    public Collection<ImageData> retrieveImageDatas(final Boolean historic, final SamplingType samplingType,
            final String... param) throws ArchivingException {
        final IDataBaseAcess database = getDataBaseApi(historic);
        final Collection<ImageData> ret;
        IAttributeExtractor extractor = getExtractor(database);
        try {
            ret = extractor.getAttImageDataBetweenDates(samplingType, param);
        } finally {
            cleanExtractor(extractor);
        }
        if ((historic != null) && (ret != null)) {
            for (ImageData data : ret) {
                if (data != null) {
                    data.setHistoric(historic.booleanValue());
                }
            }
        }
        return ret;
    }

    @Override
    public String timeToDateSGBD(final Boolean historic, final long milli) throws ArchivingException {
        String date;
        if (getDataBaseApi(historic).hasUSDateFormat()) {
            date = formatDate(milli, US_FORMAT);
        } else {
            date = formatDate(milli, EU_FORMAT);
        }
        return date;
    }

    private DbData[] getDbDataWithSampling(final Boolean historic, final SamplingType samplingType,
            final String... param) {
        DbData[] data;
        if (isCanceled(historic)) {
            data = null;
        } else {
            try {
                final IDataBaseAcess db = getDataBaseApi(historic);
                IAttributeExtractor extractor = getExtractor(db);
                try {
                    data = extractor.getAttDataBetweenDates(samplingType, param);
                } finally {
                    cleanExtractor(extractor);
                }
            } catch (final ArchivingException e) {
                getLogger().error("Failed to get db data with sampling", e);
                data = null;
            }
        }
        return data;
    }

    @Override
    public boolean isCanceled(Boolean historic) {
        boolean cancelled;
        if (historic == null) {
            cancelled = ttsCanceled;
        } else if (historic.booleanValue()) {
            cancelled = hdbCanceled;
        } else {
            cancelled = tdbCanceled;
        }
        return cancelled;
    }

    @Override
    public void setCanceled(Boolean historic, boolean canceled) throws ArchivingException {
        if (historic == null) {
            ttsCanceled = canceled;
        } else if (historic.booleanValue()) {
            hdbCanceled = canceled;
        } else {
            tdbCanceled = canceled;
        }
        IDataBaseAcess manager = getDataBaseApi(historic);
        if (manager != null) {
            IAttributeExtractor[] extractors = getExtractorsToCancel();
            if (extractors != null) {
                for (final IAttributeExtractor extractor : extractors) {
                    if (extractor != null) {
                        try {
                            extractor.setCanceled(canceled);
                        } catch (ArchivingException e) {
                            getLogger().error("failed to cancel sql statement", e);
                        }
                    }
                }
            }
        }
    }

    /**
     * @return Returns the showRead.
     */
    @Override
    public boolean isShowRead() {
        return showRead;
    }

    @Override
    public void setShowRead(final boolean showRead) {
        this.showRead = showRead;
    }

    /**
     * @return Returns the showWrite.
     */
    @Override
    public boolean isShowWrite() {
        return showWrite;
    }

    @Override
    public void setShowWrite(final boolean showWrite) {
        this.showWrite = showWrite;
    }

    private static String formatDate(final long timeInMillis, final DateTimeFormatter format) {
        String date = null;
        if (format != null) {
            date = format.format(Instant.ofEpochMilli(timeInMillis).atZone(ZoneId.systemDefault()).toLocalDateTime());
        }
        return date;
    }

}
