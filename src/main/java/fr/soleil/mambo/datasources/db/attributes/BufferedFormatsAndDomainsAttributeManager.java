/*
 * Synchrotron Soleil File : CompletelyBufferedAttrDBManagerImpl.java Project :
 * mambo Description : Author : CLAISSE Original : 10 mai 2006 Revision: Author:
 * Date: State: Log: CompletelyBufferedAttrDBManagerImpl.java,v
 */
/*
 * Created on 10 mai 2006 To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.mambo.datasources.db.attributes;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Condition;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.lib.project.ICancelable;

public class BufferedFormatsAndDomainsAttributeManager extends BufferedFormatsAttributeManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(BufferedFormatsAndDomainsAttributeManager.class);

    private static final String NULL_CRITERION_DESCRIPTION = "No Criterion";

    private static final Criterions NULL_CRITERION = new Criterions() {

        private static final long serialVersionUID = -5303818538374941282L;

        @Override
        public void addCondition(final Condition condition) {
            // nothing to do
        }

        @Override
        public Condition[] getConditions(final String columnName) {
            return null;
        }

        @Override
        public Map<String, Collection<Condition>> getConditionsHT() {
            return null;
        }

        @Override
        public String toString() {
            return NULL_CRITERION_DESCRIPTION;
        }
    };

    private final Map<Criterions, List<Domain>> hdbBuffer = new ConcurrentHashMap<>();
    private final Map<Criterions, Long> hdbTimeBuffer = new ConcurrentHashMap<>();
    private final Map<Criterions, List<Domain>> tdbBuffer = new ConcurrentHashMap<>();
    private final Map<Criterions, Long> tdbTimeBuffer = new ConcurrentHashMap<>();
    private final Map<Criterions, List<Domain>> ttsBuffer = new ConcurrentHashMap<>();
    private final Map<Criterions, Long> ttsTimeBuffer = new ConcurrentHashMap<>();

    public BufferedFormatsAndDomainsAttributeManager() throws ArchivingException {
        super();
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected AutoCleaner generateAutoCleaner() {
        return new DomainAutoCleaner();
    }

    @Override
    public List<Domain> loadDomains(final Criterions searchCriterions, final Boolean historic,
            final boolean forceReload, final ICancelable cancelable) throws ArchivingException {
        List<Domain> bufferisedResult;

        if (searchCriterions == null) {
            bufferisedResult = loadDomains(historic, forceReload, cancelable);
        } else {
            Long time = Long.valueOf(System.currentTimeMillis());
            final Map<Criterions, List<Domain>> domainsBuffer;
            if (historic == null) {
                ttsTimeBuffer.put(NULL_CRITERION, time);
                domainsBuffer = ttsBuffer;
            } else if (historic.booleanValue()) {
                hdbTimeBuffer.put(NULL_CRITERION, time);
                domainsBuffer = hdbBuffer;
            } else {
                tdbTimeBuffer.put(NULL_CRITERION, time);
                domainsBuffer = tdbBuffer;
            }
            final Map<String, Collection<Condition>> conditionsHT = searchCriterions.getConditionsHT();

            if (conditionsHT == null) {
                bufferisedResult = loadDomains(historic, forceReload, cancelable);
            } else {
                bufferisedResult = domainsBuffer.get(searchCriterions);
                if (bufferisedResult == null || forceReload) {
                    final List<Domain> newResult = super.loadDomains(searchCriterions, historic, forceReload,
                            cancelable);
                    if (newResult != null && (cancelable == null || !cancelable.isCanceled())) {
                        domainsBuffer.put(searchCriterions, newResult);
                    }
                    bufferisedResult = newResult;
                }
            }
        }
        return bufferisedResult;
    }

    @Override
    public List<Domain> loadDomains(final Boolean historic, final boolean forceReload, final ICancelable cancelable)
            throws ArchivingException {
        Long time = Long.valueOf(System.currentTimeMillis());
        final Map<Criterions, List<Domain>> domainsBuffer;
        if (historic == null) {
            ttsTimeBuffer.put(NULL_CRITERION, time);
            domainsBuffer = ttsBuffer;
        } else if (historic.booleanValue()) {
            hdbTimeBuffer.put(NULL_CRITERION, time);
            domainsBuffer = hdbBuffer;
        } else {
            tdbTimeBuffer.put(NULL_CRITERION, time);
            domainsBuffer = tdbBuffer;
        }
        List<Domain> bufferisedResult = domainsBuffer.get(NULL_CRITERION);

        if (bufferisedResult == null || forceReload) {
            final List<Domain> newResult = super.loadDomains(historic, forceReload, cancelable);
            if (newResult != null && (cancelable == null || !cancelable.isCanceled())) {
                domainsBuffer.put(NULL_CRITERION, newResult);
            }
            bufferisedResult = newResult;
        }
        return bufferisedResult;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * An {@link AutoCleaner} that also checks hdb and tdb domains buffers
     * 
     * @author girardot
     */
    protected class DomainAutoCleaner extends AutoCleaner {
        @Override
        protected long checkMaps() {
            long time = super.checkMaps();
            checkMaps(hdbBuffer, hdbTimeBuffer, time);
            checkMaps(tdbBuffer, tdbTimeBuffer, time);
            return time;
        }

        @Override
        protected boolean isRemovableKey(Object key) {
            // consider NULL_CRITERION as not removable
            return (!NULL_CRITERION.equals(key));
        }
    }

}
