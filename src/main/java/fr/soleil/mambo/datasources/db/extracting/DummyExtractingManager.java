/*
 * Synchrotron Soleil File : DummyExtractingManager.java Project : mambo
 * Description : Author : CLAISSE Original : 22 sept. 2006 Revision: Author:
 * Date: State: Log: DummyExtractingManager.java,v
 */
package fr.soleil.mambo.datasources.db.extracting;

import java.util.Collection;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;

public class DummyExtractingManager implements IExtractingManager {

    public DummyExtractingManager() {
        super();
    }

    @Override
    public double[] getMinAndMax(Boolean historic, String... param) {
        return null;
    }

    @Override
    public DbData[] retrieveData(Boolean historic, SamplingType samplingType, String... param) {
        return null;
    }

    @Override
    public Collection<ImageData> retrieveImageDatas(Boolean historic, SamplingType samplingType, String... param)
            throws ArchivingException {
        return null;
    }

    @Override
    public String timeToDateSGBD(Boolean historic, long milli) throws ArchivingException {
        return null;
    }

    @Override
    public void setCanceled(Boolean historic, boolean canceled) {
    }

    @Override
    public boolean isCanceled(Boolean historic) {
        return false;
    }

    @Override
    public void setShowWrite(boolean b) {
    }

    @Override
    public void setShowRead(boolean b) {
    }

    @Override
    public void openConnection() throws ArchivingException {
    }

    @Override
    public boolean isShowRead() {
        return false;
    }

    @Override
    public boolean isShowWrite() {
        return false;
    }
}
