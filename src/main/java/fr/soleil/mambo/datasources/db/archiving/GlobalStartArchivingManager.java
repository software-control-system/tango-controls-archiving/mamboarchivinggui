package fr.soleil.mambo.datasources.db.archiving;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.api.ApiUtils;
import fr.soleil.mambo.api.archiving.IArchivingManagerApi;
import fr.soleil.mambo.api.archiving.IBufferedArchivingManagerApi;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributes;
import fr.soleil.mambo.tools.Messages;

public class GlobalStartArchivingManager extends BasicArchivingManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalStartArchivingManager.class);

    public GlobalStartArchivingManager() throws ArchivingException {
        super();
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    public void startArchiving(ArchivingConfiguration ac, ArchivingConfigurationAttribute... attributes)
            throws ArchivingException {
        if ((ac != null) && (attributes != null)) {
            final Boolean historic = ac.isHistoric();
            IArchivingManagerApi manager;
            if (historic == null) {
                manager = ttsManager;
            } else if (historic.booleanValue()) {
                manager = hdbManager;
            } else {
                manager = tdbManager;
            }
            if (manager == null) {
                String format = Messages.getMessage("ARCHIVING_ACTION_START_ERROR");
                String errorMessage;
                if (historic == null) {
                    errorMessage = String.format(format, ApiConstants.TTS);
                } else if (historic.booleanValue()) {
                    errorMessage = String.format(format, ApiConstants.HDB);
                } else {
                    errorMessage = String.format(format, ApiConstants.TDB);
                }
                LOGGER.error(errorMessage);
                throw new ArchivingException(errorMessage);
            } else {
                try {
                    manager.archivingStart(attributes);
                } finally {
                    // TANGOARCH-838: clean attributes buffer after start, to force reading the information in database.
                    if (manager instanceof IBufferedArchivingManagerApi) {
                        IBufferedArchivingManagerApi bufferedManager = (IBufferedArchivingManagerApi) manager;
                        bufferedManager.cleanBuffer(ApiUtils.getAttributeNames(EMPTY, attributes));
                    }
                }
            }
        }
    }

    @Override
    public void startArchiving(final ArchivingConfiguration ac) throws ArchivingException {
        if (!isReady) {
            openConnection();
        }
        if (ac != null) {
            final ArchivingConfigurationAttributes ACAttributes = ac.getAttributes();
            final ArchivingConfigurationAttribute[] attributes = ACAttributes.getAttributesList();

            startArchiving(ac, attributes);
        } // end if (ac != null)
    }

}
