package fr.soleil.mambo.datasources.db.attributes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.TreeMap;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Condition;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Domains;
import fr.soleil.archiving.tango.entity.Family;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.db.IDataBaseAcess;
import fr.soleil.mambo.datasources.db.DbConnectionManager;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;

public class BasicAttributeManager extends DbConnectionManager implements IAttributeManager {

    public BasicAttributeManager() throws ArchivingException {
        super();
    }

    @Override
    public boolean isScalar(final String completeName, final Boolean historic) throws ArchivingException {
        final int format = getFormat(completeName, historic);
        return (format == AttrDataFormat._SCALAR);

    }

    @Override
    public boolean isSpectrum(final String completeName, final Boolean historic) throws ArchivingException {
        final int format = getFormat(completeName, historic);
        return (format == AttrDataFormat._SPECTRUM);
    }

    @Override
    public boolean isImage(final String completeName, final Boolean historic) throws ArchivingException {
        final int format = getFormat(completeName, historic);
        return (format == AttrDataFormat._IMAGE);
    }

    @Override
    public List<Domain> loadDomains(final Criterions searchCriterions, final Boolean historic,
            final boolean forceReload) throws ArchivingException {
        return loadDomains(searchCriterions, historic, forceReload, null);
    }

    @Override
    public List<Domain> loadDomains(final Criterions searchCriterions, final Boolean historic,
            final boolean forceReload, final ICancelable cancelable) throws ArchivingException {
        final List<Domain> ret = new ArrayList<>();

        String domainCriterions = GUIUtilities.TANGO_JOKER;
        String familyCriterions = GUIUtilities.TANGO_JOKER;
        String memberCriterions = GUIUtilities.TANGO_JOKER;
        String attributeCriterions = GUIUtilities.TANGO_JOKER;

        if (searchCriterions != null && (cancelable == null || !cancelable.isCanceled())) {
            final Condition[] domainConditions = searchCriterions
                    .getConditions(ITangoManager.FIELD_LOWER_ATTRIBUTE_DOMAIN);
            if (domainConditions != null) {
                final Condition domainCondition = domainConditions[0];
                domainCriterions = domainCondition.getValue();
            }

            final Condition[] familyConditions = searchCriterions
                    .getConditions(ITangoManager.FIELD_LOWER_ATTRIBUTE_FAMILY);
            if (familyConditions != null) {
                final Condition familyCondition = familyConditions[0];
                familyCriterions = familyCondition.getValue();
            }

            final Condition[] memberConditions = searchCriterions
                    .getConditions(ITangoManager.FIELD_LOWER_ATTRIBUTE_MEMBER);
            if (memberConditions != null) {
                final Condition memberCondition = memberConditions[0];
                memberCriterions = memberCondition.getValue();
            }

            final Condition[] attributeConditions = searchCriterions
                    .getConditions(ITangoManager.FIELD_LOWER_ATTRIBUTE_NAME);
            if (attributeConditions != null) {
                final Condition attributeCondition = attributeConditions[0];
                attributeCriterions = attributeCondition.getValue();
            }
        }
        if (cancelable == null || !cancelable.isCanceled()) {
            final IDataBaseAcess database = getDataBaseApi(historic);
            final String[] attributes = database.getAttributesByCriterion(domainCriterions, familyCriterions,
                    memberCriterions, attributeCriterions, cancelable);
            if (attributes != null) {
                Map<String, Domain> domains = new TreeMap<>();
                for (String attribute : attributes) {
                    if (attribute != null) {
                        String[] splitAttr = attribute.split(TangoDeviceHelper.SLASH);
                        if (splitAttr.length == 4) {
                            String domainStr = splitAttr[0];
                            String domainLower = domainStr.toLowerCase();
                            Domain domain = domains.get(domainLower);
                            if (domain == null) {
                                domain = new Domain(domainStr);
                                domains.put(domainLower, domain);
                            }
                            Family family = domain.getFamily(splitAttr[1]);
                            if (family == null) {
                                family = new Family(splitAttr[1]);
                                domain.addFamily(family);
                            }
                            Member member = family.getMember(splitAttr[2]);
                            if (member == null) {
                                member = new Member(splitAttr[2]);
                                family.addMember(member);
                            }
                            final Attribute currentAttribute = new Attribute();
                            currentAttribute.setName(splitAttr[3]);
                            currentAttribute.setDomain(splitAttr[0]);
                            currentAttribute.setFamily(splitAttr[1]);
                            currentAttribute.setMember(splitAttr[2]);
                            member.addAttribute(currentAttribute);
                        }
                    }
                }
                ret.addAll(domains.values());
                domains.clear();
            }
        }
        return ret;
    }

    @Override
    public List<Domain> loadDomains(final Boolean historic, final boolean forceReload) throws ArchivingException {
        return loadDomains(historic, forceReload, null);
    }

    @Override
    public List<Domain> loadDomains(final Boolean historic, final boolean forceReload, final ICancelable cancelable)
            throws ArchivingException {
        final List<Domain> ret;
        final Domains domains = new Domains();

        final IDataBaseAcess database = getDataBaseApi(historic);
        if (database == null) {
            ret = new ArrayList<>();
        } else {
            final String completeNames[] = database.getAttributesCompleteNames();
            if (completeNames == null || (cancelable != null && cancelable.isCanceled())) {
                ret = new ArrayList<>();
            } else {
                for (final String completeName : completeNames) {
                    if (cancelable != null && cancelable.isCanceled()) {
                        break;
                    }
                    final String[] dfma = parseCompleteNameIntoDFMA(completeName);

                    final String domainName = dfma[0];
                    final String familyName = dfma[1];
                    final String memberName = dfma[2];
                    final String attributeName = dfma[3];

                    domains.addAttribute(domainName, familyName, memberName, attributeName);
                }

                ret = domains.getList();
            }
        }
        return ret;
    }

    @Override
    public Collection<String> getAttributes(final String domain, final String family, final String member,
            final Boolean historic) throws DevFailed, ArchivingException {
        final IDataBaseAcess database = getDataBaseApi(historic);
        final String[] list = database.getAttributes(domain, family, member);
        final Collection<String> ret;
        if (list == null) {
            ret = null;
        } else {
            ret = new ArrayList<>(list.length);
            for (final String element : list) {
                ret.add(parseCompleteName(element));
            }
        }
        return ret;
    }

    /**
     * @param string
     * @return 26 août 2005
     */
    private String parseCompleteName(final String completeName) {
        final StringTokenizer st = new StringTokenizer(completeName, TangoDeviceHelper.SLASH);
        String ret = ObjectUtils.EMPTY_STRING;
        while (true) {
            try {
                ret = st.nextToken();
            } catch (final NoSuchElementException nsee) {
                break;
            }
        }
        return ret;
    }

    private String[] parseCompleteNameIntoDFMA(final String completeName) {
        final StringTokenizer st = new StringTokenizer(completeName, TangoDeviceHelper.SLASH);
        final String[] ret = new String[4];

        ret[0] = st.nextToken();
        ret[1] = st.nextToken();
        ret[2] = st.nextToken();
        ret[3] = st.nextToken();

        return ret;
    }

    @Override
    public int getDataType(final String completeName, final Boolean historic) throws ArchivingException {
        final IDataBaseAcess database = getDataBaseApi(historic);
        final int type = database == null ? -1 : database.getType(completeName);
        return type;
    }

    @Override
    public int getDataWritable(final String completeName, final Boolean historic) throws ArchivingException {
        final IDataBaseAcess database = getDataBaseApi(historic);
        final int writable = database.getWritable(completeName);
        return writable;
    }

    @Override
    public String getDisplayFormat(final String completeName, final Boolean historic) throws ArchivingException {
        final IDataBaseAcess database = getDataBaseApi(historic);
        return database.getFormat(completeName);
    }

    /**
     * @param name the name of the attribute
     * @param historic a boolean to know wheather you check HDB or TDB
     * @return an int representing the format of the attribute. -1 on error
     * @throws ArchivingException
     */
    @Override
    public int getFormat(final String name, final Boolean historic) throws ArchivingException {
        final IDataBaseAcess database = getDataBaseApi(historic);
        return database == null ? -1 : database.getDataFormat(name);
    }

}
