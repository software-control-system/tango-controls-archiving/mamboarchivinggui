/*
 * Synchrotron Soleil File : BufferedAttrDBManagerImpl.java Project : mambo
 * Description : Author : CLAISSE Original : 10 mai 2006 Revision: Author: Date:
 * State: Log: BufferedAttrDBManagerImpl.java,v
 */
/*
 * Created on 10 mai 2006 To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.mambo.datasources.db.attributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;

public class BufferedFormatsAttributeManager extends BasicAttributeManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(BufferedFormatsAttributeManager.class);

    public static final long DEFAULT_CHECK_PERIOD = 120000;// 2 minutes
    public static final long DEFAULT_TIME_TO_DEATH = 1800000;// 30 minutes
    private static final int MAX_BUFFER_SIZE_ATTRIBUTES_TYPES = 10000;

    private final Map<String, Integer> hdbDataFormatBuffer;
    private final Map<String, Long> hdbTimeBuffer;

    private final Map<String, Integer> tdbDataFormatBuffer;
    private final Map<String, Long> tdbTimeBuffer;

    private final Map<String, Integer> ttsDataFormatBuffer;
    private final Map<String, Long> ttsTimeBuffer;

    // Auto clean management
    protected final String cleanerName;
    protected long maximumLifeTime;

    public BufferedFormatsAttributeManager() throws ArchivingException {
        super();
        hdbDataFormatBuffer = new ConcurrentHashMap<>(MAX_BUFFER_SIZE_ATTRIBUTES_TYPES);
        hdbTimeBuffer = new ConcurrentHashMap<>();
        tdbDataFormatBuffer = new ConcurrentHashMap<>(MAX_BUFFER_SIZE_ATTRIBUTES_TYPES);
        tdbTimeBuffer = new ConcurrentHashMap<>();
        ttsDataFormatBuffer = new ConcurrentHashMap<>(MAX_BUFFER_SIZE_ATTRIBUTES_TYPES);
        ttsTimeBuffer = new ConcurrentHashMap<>();
        cleanerName = "Mambo " + getClass().getSimpleName() + " data auto cleaner";
        maximumLifeTime = DEFAULT_TIME_TO_DEATH;
        // start auto clean thread
        Executors.newScheduledThreadPool(1, new CleaningThreadFactory()).scheduleAtFixedRate(generateAutoCleaner(), 0,
                DEFAULT_CHECK_PERIOD, TimeUnit.MILLISECONDS);
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    protected AutoCleaner generateAutoCleaner() {
        return new AutoCleaner();
    }

    @Override
    public int getFormat(final String completeName, final Boolean historic) throws ArchivingException {
        Long time = Long.valueOf(System.currentTimeMillis());
        Map<String, Integer> formatMap;
        if (historic == null) {
            ttsTimeBuffer.put(completeName, time);
            formatMap = ttsDataFormatBuffer;
        } else if (historic.booleanValue()) {
            hdbTimeBuffer.put(completeName, time);
            formatMap = hdbDataFormatBuffer;
        } else {
            tdbTimeBuffer.put(completeName, time);
            formatMap = tdbDataFormatBuffer;
        }
        int dataFormat;
        Integer knownFormat = formatMap.get(completeName);
        if (knownFormat == null) {
            dataFormat = super.getFormat(completeName, historic);
            if (dataFormat > -1) {
                knownFormat = Integer.valueOf(dataFormat);
                formatMap.put(completeName, knownFormat);
            }
        } else {
            dataFormat = knownFormat.intValue();
        }
        return dataFormat;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link Runnable} that checks buffers and cleans unused elements
     * 
     * @author girardot
     */
    protected class AutoCleaner implements Runnable {

        @Override
        public void run() {
            checkMaps();
        }

        /**
         * checks the buffers
         * 
         * @return The time at which it started to check buffers
         */
        protected long checkMaps() {
            long time = System.currentTimeMillis();
            checkMaps(hdbDataFormatBuffer, hdbTimeBuffer, time);
            checkMaps(tdbDataFormatBuffer, tdbTimeBuffer, time);
            return time;
        }

        /**
         * Compares keys last check time with a given time, and if to old, removes these keys.
         * 
         * @param valueMap The {@link Map} that is used as a buffer
         * @param timeMap The {@link Map} that contains keys last check time
         * @param checkTime The reference time
         */
        protected final <K, V> void checkMaps(Map<K, V> valueMap, Map<K, Long> timeMap, long checkTime) {
            if ((valueMap != null) && (timeMap != null)) {
                try {
                    List<K> toRemove = new ArrayList<>();
                    for (final Entry<K, Long> entry : timeMap.entrySet()) {
                        Long time = entry.getValue();
                        if (isRemovableKey(entry.getKey()) && (time != null)
                                && (checkTime - time.longValue() >= maximumLifeTime)) {
                            toRemove.add(entry.getKey());
                        }
                    }
                    if (!toRemove.isEmpty()) {
                        for (final K key : toRemove) {
                            valueMap.remove(key);
                            timeMap.remove(key);
                        }
                        toRemove.clear();
                    }
                } catch (Exception e) {
                    // Do ignore exceptions in order never to die
                }
            }
        }

        /**
         * Returns whether a given key can be removed
         * 
         * @param key
         *            The key
         * @return A <code>boolean</code> value
         */
        protected boolean isRemovableKey(Object key) {
            return true;
        }
    }

    /**
     * A {@link ThreadFactory} that gives the expected "cleaning" name to its generated {@link Thread}s
     * 
     * @author girardot
     */
    protected class CleaningThreadFactory implements ThreadFactory {
        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, cleanerName);
        }
    }

}
