/*
 * Synchrotron Soleil File : IExtractingManager.java Project : mambo Description
 * : Author : CLAISSE Original : 22 sept. 2006 Revision: Author: Date: State:
 * Log: IExtractingManager.java,v
 */
/*
 * Created on 22 sept. 2006 To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.mambo.datasources.db.extracting;

import java.util.Collection;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;

public interface IExtractingManager {

    public void openConnection() throws ArchivingException;

    public double[] getMinAndMax(Boolean historic, String... param) throws ArchivingException;

    public DbData[] retrieveData(Boolean historic, SamplingType samplingType, String... param);

    public Collection<ImageData> retrieveImageDatas(Boolean historic, SamplingType samplingType, String... param)
            throws ArchivingException;

    public String timeToDateSGBD(Boolean historic, long milli) throws ArchivingException;

    public void setCanceled(Boolean historic, boolean canceled) throws ArchivingException;

    public boolean isCanceled(Boolean historic);

    public void setShowWrite(boolean b);

    public void setShowRead(boolean b);

    public boolean isShowRead();

    public boolean isShowWrite();
}
