package fr.soleil.mambo.datasources.db.attributes;

import java.util.ArrayList;
import java.util.List;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.api.db.IDataBaseAcess;

public class DummyAttributeManager implements IAttributeManager {

    @Override
    public void openConnection() throws ArchivingException {
    }

    @Override
    public List<Domain> loadDomains(Criterions searchCriterions, Boolean historic, boolean forceReload) {
        return new ArrayList<>();
    }

    @Override
    public List<Domain> loadDomains(Criterions searchCriterions, Boolean historic, boolean forceReload,
            ICancelable cancelable) {
        return loadDomains(searchCriterions, historic, forceReload);
    }

    @Override
    public ArrayList<String> getAttributes(String domain, String family, String member, Boolean historic)
            throws DevFailed, ArchivingException {
        return null;
    }

    @Override
    public boolean isScalar(String completeName, Boolean historic) {
        return true;
    }

    @Override
    public boolean isSpectrum(String completeName, Boolean historic) {
        return false;
    }

    @Override
    public int getDataType(String completeName, Boolean historic) {
        return -1;
    }

    @Override
    public int getDataWritable(String completeName, Boolean historic) {
        return -1;
    }

    @Override
    public boolean isImage(String completeName, Boolean historic) {
        return false;
    }

    @Override
    public String getDisplayFormat(String completeName, Boolean historic) {
        return null;
    }

    @Override
    public int getFormat(String name, Boolean historic) {
        return 0;
    }

    @Override
    public IDataBaseAcess getDataBaseApi(Boolean historic) throws ArchivingException {
        return null;
    }

    @Override
    public List<Domain> loadDomains(Boolean b, boolean c) {
        return new ArrayList<>();
    }

    @Override
    public List<Domain> loadDomains(Boolean b, boolean c, ICancelable cancelable) {
        return loadDomains(b, c);
    }

}
