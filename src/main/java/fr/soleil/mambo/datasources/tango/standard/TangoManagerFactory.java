package fr.soleil.mambo.datasources.tango.standard;

public class TangoManagerFactory {
    public static final int DUMMY_IMPL_TYPE = 1;
    public static final int BASIC_IMPL_TYPE = 2;

    private static ITangoManager currentImpl = null;

    /**
     * @param typeOfImpl
     * @return 8 juil. 2005
     */
    public static ITangoManager getImpl(final int typeOfImpl) {
        switch (typeOfImpl) {
            case DUMMY_IMPL_TYPE:
                currentImpl = new DummyTangoManagerImpl();
                break;

            case BASIC_IMPL_TYPE:
                currentImpl = new BasicTangoManagerImpl();
                break;

            default:
                throw new IllegalStateException(
                        "Expected either DUMMY_IMPL_TYPE (1) or REAL_IMPL_TYPE (2), got " + typeOfImpl + " instead.");
        }

        return currentImpl;
    }

    /**
     * @return 28 juin 2005
     */
    public static ITangoManager getCurrentImpl() {
        return currentImpl;
    }

}
