package fr.soleil.mambo.datasources.tango.standard;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Family;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.api.archiving.IArchiver;

public class DummyTangoManagerImpl implements ITangoManager {

    @Override
    public List<Domain> loadDomains(String searchCriterions, ICancelable cancelable) {
        final List<Domain> val = new ArrayList<Domain>();
        if ((cancelable != null) && (!cancelable.isCanceled())) {
            final Domain archiving = new Domain("archiving");
            final Domain dserver = new Domain("dserver");
            final Domain sys = new Domain("sys");
            final Domain tango = new Domain("tango");

            val.add(archiving);
            val.add(dserver);
            val.add(sys);
            val.add(tango);
            if ((cancelable != null) && (!cancelable.isCanceled())) {
                final Family tangotest = new Family("tangotest");
                tango.addFamily(tangotest);

                final Member n1 = new Member("1");
                tangotest.addMember(n1);
                if ((cancelable != null) && (!cancelable.isCanceled())) {
                    final Attribute float_scalar = new Attribute("float_scalar");
                    final Attribute short_scalar_ro = new Attribute("short_scalar_ro");
                    final Attribute short_scalar = new Attribute("short_scalar");
                    final Attribute long_scalar = new Attribute("long_scalar");
                    final Attribute double_scalar = new Attribute("double_scalar");
                    final Attribute string_scalar = new Attribute("string_scalar");
                    final Attribute float_spectrum = new Attribute("float_spectrum");
                    final Attribute short_spectrum = new Attribute("short_spectrum");
                    final Attribute short_image = new Attribute("short_image");
                    final Attribute float_image = new Attribute("float_image");

                    n1.addAttribute(float_scalar);
                    n1.addAttribute(short_scalar_ro);
                    n1.addAttribute(short_scalar);
                    n1.addAttribute(long_scalar);
                    n1.addAttribute(double_scalar);
                    n1.addAttribute(string_scalar);
                    n1.addAttribute(float_spectrum);
                    n1.addAttribute(short_spectrum);
                    n1.addAttribute(short_image);
                    n1.addAttribute(float_image);
                }
            }
        }
        return val;
    }

    @Override
    public List<Domain> loadDomains(final String searchCriterions) {
        return loadDomains(searchCriterions, null);
    }

    @Override
    public IArchiver[] findArchivers(final Boolean historic) {
        return null;
    }

    public void loadDServers() {
    }

}
