// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/datasources/tango/standard/ITangoManager.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ITangoManager.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.1 $
//
// $Log: ITangoManager.java,v $
// Revision 1.1  2006/09/20 12:47:45  ounsy
// moved from mambo.datasources.tango
//
// Revision 1.5  2006/09/14 11:29:29  ounsy
// complete buffering refactoring
//
// Revision 1.4  2006/07/18 10:29:25  ounsy
// possibility to reload tango attributes
//
// Revision 1.3  2006/06/15 15:41:49  ounsy
// added a findArchivers method
//
// Revision 1.2  2005/11/29 18:28:12  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:32  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.datasources.tango.standard;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Condition;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.api.archiving.IArchiver;

public interface ITangoManager {
    // Operators.
    public static final String OP_EQUALS = "=";
    public static final String OP_GREATER_THAN = ">=";
    public static final String OP_LOWER_THAN = "<=";
    public static final String OP_GREATER_THAN_STRICT = ">";
    public static final String OP_LOWER_THAN_STRICT = "<";
    public static final String OP_CONTAINS = "Contains";
    public static final String OP_STARTS_WITH = "Starts with";
    public static final String OP_ENDS_WITH = "Ends with";

    public static final String FIELD_ATTRIBUTE_DOMAIN = "domain";
    public static final String FIELD_ATTRIBUTE_FAMILY = "family";
    public static final String FIELD_ATTRIBUTE_MEMBER = "member";
    public static final String FIELD_ATTRIBUTE_NAME = "att_name";

    public static final String FIELD_LOWER_ATTRIBUTE_DOMAIN = "lower(domain)";
    public static final String FIELD_LOWER_ATTRIBUTE_FAMILY = "lower(family)";
    public static final String FIELD_LOWER_ATTRIBUTE_MEMBER = "lower(member)";
    public static final String FIELD_LOWER_ATTRIBUTE_NAME = "lower(att_name)";

    public List<Domain> loadDomains(String searchCriterions, ICancelable cancelable);

    public List<Domain> loadDomains(String searchCriterions);

    public IArchiver[] findArchivers(Boolean historic) throws ArchivingException;

    public static Criterions getAttributesSearchCriterions(final String pattern) {
        final Criterions ret;
        if (pattern == null) {
            ret = null;
        } else {
            ret = new Criterions();
            Condition cond;

            final StringTokenizer st = new StringTokenizer(pattern, GUIUtilities.TANGO_DELIM);
            /* int nbOfTokens = st.countTokens(); */

            String domain = GUIUtilities.TANGO_JOKER;
            String family = GUIUtilities.TANGO_JOKER;
            String member = GUIUtilities.TANGO_JOKER;
            String attribute = GUIUtilities.TANGO_JOKER;

            try {
                domain = st.nextToken();
                family = st.nextToken();
                member = st.nextToken();
                attribute = st.nextToken();
            } catch (final NoSuchElementException e) {
                // do nothing
            }

            cond = new Condition(ITangoManager.FIELD_LOWER_ATTRIBUTE_DOMAIN, ITangoManager.OP_EQUALS, domain);
            ret.addCondition(cond);

            cond = new Condition(ITangoManager.FIELD_LOWER_ATTRIBUTE_FAMILY, ITangoManager.OP_EQUALS, family);
            ret.addCondition(cond);

            cond = new Condition(ITangoManager.FIELD_LOWER_ATTRIBUTE_MEMBER, ITangoManager.OP_EQUALS, member);
            ret.addCondition(cond);

            cond = new Condition(ITangoManager.FIELD_LOWER_ATTRIBUTE_NAME, ITangoManager.OP_EQUALS, attribute);
            ret.addCondition(cond);
        }
        return ret;
    }
}
