package fr.soleil.mambo.datasources.tango.standard;

import java.util.List;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.tango.TangoLoaderUtil;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.api.archiving.ArchiverFactory;
import fr.soleil.mambo.api.archiving.IArchiver;
import fr.soleil.mambo.api.archiving.IArchivingManagerApi;
import fr.soleil.mambo.datasources.db.DbConnectionManager;

public class BasicTangoManagerImpl implements ITangoManager {

    public BasicTangoManagerImpl() {
        super();
    }

    @Override
    public List<Domain> loadDomains(String searchCriterions, ICancelable cancelable) {
        return TangoLoaderUtil.searchAttributes(searchCriterions, cancelable);
    }

    @Override
    public List<Domain> loadDomains(final String searchCriterions) {
        return TangoLoaderUtil.searchAttributes(searchCriterions);
    }

    @Override
    public IArchiver[] findArchivers(final Boolean historic) throws ArchivingException {
        final IArchiver[] ret;
        final String[] list;
        IArchivingManagerApi api;
        try {
            api = DbConnectionManager.getArchivingManagerApiInstance(historic);
        } catch (Exception e) {
            api = null;
        }
        list = api == null ? null : api.getExportedArchivers();
        if (list == null || list.length == 0) {
            ret = null;
        } else {
            ret = new IArchiver[list.length];
            int i = 0;
            for (String nextName : list) {
                ret[i++] = ArchiverFactory.getArchiver(nextName, historic);
            }
        }
        return ret;
    }

}
