package fr.soleil.mambo.datasources.file;

import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;

public class DummyArchivingConfigurationManagerImpl implements IArchivingConfigurationManager {

    @Override
    public void saveArchivingConfiguration(ArchivingConfiguration ac) throws Exception {
    }

    @Override
    public ArchivingConfiguration[] loadArchivingConfigurations(Criterions searchCriterions) throws Exception {
        return null;
    }

    @Override
    public ArchivingConfiguration[] findArchivingConfigurations(ArchivingConfiguration[] in,
            Criterions searchCriterions) throws Exception {
        return null;
    }

    @Override
    public void startUp() throws Exception {
    }

    @Override
    public void shutDown() throws Exception {
    }

    @Override
    public ArchivingConfiguration loadArchivingConfiguration() throws Exception {
        return null;
    }

    @Override
    public String getDefaultSaveLocation() {
        return null;
    }

    @Override
    public String getNonDefaultSaveLocation() {
        return null;
    }

    @Override
    public void setNonDefaultSaveLocation(String location) {
    }

    @Override
    public String getSaveLocation() {
        return null;
    }

}
