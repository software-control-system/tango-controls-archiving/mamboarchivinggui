package fr.soleil.mambo.datasources.file;

import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.mambo.data.view.ViewConfiguration;

public class DummyViewConfigurationManagerImpl implements IViewConfigurationManager {

    @Override
    public void saveViewConfiguration(ViewConfiguration ac) throws Exception {
    }

    @Override
    public ViewConfiguration[] loadViewConfigurations(Criterions searchCriterions) throws Exception {
        return null;
    }

    @Override
    public ViewConfiguration[] findViewConfigurations(ViewConfiguration[] in, Criterions searchCriterions)
            throws Exception {
        return null;
    }

    @Override
    public void startUp() throws Exception {
    }

    @Override
    public void shutDown() throws Exception {
    }

    @Override
    public ViewConfiguration loadViewConfiguration() throws Exception {
        return null;
    }

    @Override
    public String getDefaultSaveLocation() {
        return null;
    }

    @Override
    public String getNonDefaultSaveLocation() {
        return null;
    }

    @Override
    public void setNonDefaultSaveLocation(String location) {
    }

    @Override
    public String getSaveLocation() {
        return null;
    }

}
