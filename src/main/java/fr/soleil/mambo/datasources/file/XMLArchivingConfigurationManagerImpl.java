package fr.soleil.mambo.datasources.file;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.tools.xmlhelpers.ac.ArchivingConfigurationXMLHelperFactory;

public class XMLArchivingConfigurationManagerImpl implements IArchivingConfigurationManager {

    private String defaultSaveLocation;
    private String saveLocation;
    private PrintWriter writer;

    protected XMLArchivingConfigurationManagerImpl() {
        startUp();
    }

    @Override
    public void saveArchivingConfiguration(final ArchivingConfiguration ac) throws Exception {
        final String _location = getSaveLocation();

        writer = new PrintWriter(new FileWriter(_location, false));
        GUIUtilities.writeWithoutDate(writer, XMLUtils.XML_HEADER, true);
        GUIUtilities.writeWithoutDate(writer, ac.toString(), true);
        writer.close();
    }

    @Override
    public ArchivingConfiguration[] loadArchivingConfigurations(final Criterions searchCriterions) throws Exception {
        return null;
    }

    @Override
    public ArchivingConfiguration[] findArchivingConfigurations(final ArchivingConfiguration[] in,
            final Criterions searchCriterions) throws Exception {
        return null;
    }

    @Override
    public void startUp() throws IllegalStateException {
        String resourceLocation = null;
        boolean illegal = false;

        final String acPath = Mambo.getPathToResources() + "/ac";

        File f = null;
        try {
            f = new File(acPath);
            if (!f.canWrite()) {
                illegal = true;
            }
        } catch (final Exception e) {
            illegal = true;
        }

        if (illegal) {
            f.mkdir();
        }
        resourceLocation = acPath;
        defaultSaveLocation = resourceLocation;
    }

    @Override
    public void shutDown() throws Exception {

    }

    @Override
    public ArchivingConfiguration loadArchivingConfiguration( /* String location */) throws Exception {
        final String _location = getSaveLocation();
        final ArchivingConfiguration ac = ArchivingConfigurationXMLHelperFactory.getCurrentImpl()
                .loadArchivingConfigurationIntoHash(_location);
        return ac;
    }

    @Override
    public String getDefaultSaveLocation() {
        return defaultSaveLocation;
    }

    /**
     * @param defaultSaveLocation
     *            The defaultSaveLocation to set.
     */
    public void setDefaultSaveLocation(final String defaultSaveLocation) {
        this.defaultSaveLocation = defaultSaveLocation;
    }

    @Override
    public String getNonDefaultSaveLocation() {
        return saveLocation;
    }

    @Override
    public void setNonDefaultSaveLocation(final String saveLocation) {
        this.saveLocation = saveLocation;
    }

    @Override
    public String getSaveLocation() {
        final String _location = saveLocation == null ? defaultSaveLocation : saveLocation;
        return _location;
    }
}
