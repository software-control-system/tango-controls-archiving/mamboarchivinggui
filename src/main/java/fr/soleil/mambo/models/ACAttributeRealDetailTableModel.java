package fr.soleil.mambo.models;

import javax.swing.table.DefaultTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeHDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeRealDetailTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 2267857820996407673L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeRealDetailTableModel.class);

    private String[] columnsNames;
    private String isArchived;
    private String modes;

    private static ACAttributeRealDetailTableModel instance;
    private ArchivingConfigurationAttribute attribute;
    private ArchivingConfigurationAttributeHDBProperties HDBProperties;
    private ArchivingConfigurationAttributeTDBProperties TDBProperties;

    public static ACAttributeRealDetailTableModel getInstance() {
        if (instance == null) {
            String msgCol1 = "A";
            String msgCol2 = "B";
            String msgTDB = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TDB_LABEL");
            String msgHDB = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_HDB_LABEL");

            String[] columnNames = new String[4];

            columnNames[0] = msgCol1;
            columnNames[1] = msgCol2;
            columnNames[2] = msgTDB;
            columnNames[3] = msgHDB;

            instance = new ACAttributeRealDetailTableModel(columnNames, 8);
        }

        return instance;
    }

    private ACAttributeRealDetailTableModel(String[] _columnNames, int rowCount) {
        super(_columnNames, rowCount);
        this.columnsNames = _columnNames;
        isArchived = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_IS_ARCHIVED_LABEL");
        modes = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODE_LABEL");
    }

    public void load(ArchivingConfigurationAttribute attribute) {
        this.attribute = attribute;

        ArchivingConfigurationAttributeProperties attrProperties = attribute.getProperties();
        HDBProperties = attrProperties.getHDBProperties();
        TDBProperties = attrProperties.getTDBProperties();

        this.fireTableDataChanged();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public int getRowCount() {
        return 9;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return getFirstColumnValue(rowIndex);

            case 1:
                return getSecondColumnValue(rowIndex);

            case 2:
                return getTDBColumnValue(rowIndex);

            case 3:
                return getHDBColumnValue(rowIndex);

            default:
                return null;
            // we have a bug
        }
    }

    private Object getHDBColumnValue(int rowIndex) {
        Boolean ret = Boolean.valueOf(false);

        if (rowIndex == 0) {
            return columnsNames[3];
        } else if (rowIndex == 1) {
            if (attribute == null) {
                return ret;
            }

            try {
                ret = Boolean.valueOf(
                        ArchivingManagerFactory.getCurrentImpl().isArchived(attribute.getCompleteName(), true));
            } catch (Exception e) {
                LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            }
        } else {
            if (HDBProperties == null || HDBProperties.isEmpty()) {
                return ret;
            }
            IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
            try {
                Mode mode = manager.getArchivingMode(attribute.getCompleteName(), true);
                ret = this.hasMode(mode, rowIndex);
            } catch (Exception e) {
                LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            }
        }

        return ret;
    }

    private Boolean hasMode(Mode mode, int rowIndex) {
        Boolean ret = Boolean.FALSE;

        switch (rowIndex) {
            case 2:
                ret = Boolean.valueOf(mode.getModeP() != null);
                break;

            case 3:
                ret = Boolean.valueOf(mode.getModeA() != null);
                break;

            case 4:
                ret = Boolean.valueOf(mode.getModeR() != null);
                break;

            case 5:
                ret = Boolean.valueOf(mode.getModeT() != null);
                break;

            case 6:
                ret = Boolean.valueOf(mode.getModeD() != null);
                break;

            case 7:
                ret = Boolean.valueOf(mode.getModeC() != null);
                break;

            case 8:
                ret = Boolean.valueOf(mode.getModeA() != null);
                break;
        }
        return ret;
    }

    private Object getTDBColumnValue(int rowIndex) {
        Boolean ret = Boolean.valueOf(false);
        if (rowIndex == 0) {
            return columnsNames[2];
        } else if (rowIndex == 1) {
            if (attribute == null) {
                return ret;
            }

            try {
                ret = Boolean.valueOf(
                        ArchivingManagerFactory.getCurrentImpl().isArchived(attribute.getCompleteName(), false));
            } catch (Exception e) {
                LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            }
        } else {
            if (TDBProperties == null || TDBProperties.isEmpty()) {
                return ret;
            }
            IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
            try {
                Mode mode = manager.getArchivingMode(attribute.getCompleteName(), false);
                ret = this.hasMode(mode, rowIndex);
            } catch (Exception e) {
                LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            }
        }
        return ret;
    }

    private Object getSecondColumnValue(int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;

        switch (rowIndex) {
            case 2:
                ret = ArchivingConfigurationMode.TYPE_P_NAME;
                break;

            case 3:
                ret = ArchivingConfigurationMode.TYPE_A_NAME;
                break;

            case 4:
                ret = ArchivingConfigurationMode.TYPE_R_NAME;
                break;

            case 5:
                ret = ArchivingConfigurationMode.TYPE_T_NAME;
                break;

            case 6:
                ret = ArchivingConfigurationMode.TYPE_D_NAME;
                break;

            case 7:
                ret = ArchivingConfigurationMode.TYPE_C_NAME;
                break;

            case 8:
                ret = ArchivingConfigurationMode.TYPE_E_NAME;
                break;
        }

        return ret;
    }

    private Object getFirstColumnValue(int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;

        switch (rowIndex) {
            case 1:
                ret = isArchived;
                break;

            case 2:
                ret = modes;
                break;
        }

        return ret;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columnsNames[columnIndex];
    }

    public void setColumnName(int columnIndex, String columnName) {
        if (columnIndex < getColumnCount()) {
            columnsNames[columnIndex] = columnName;
        }
    }

}
