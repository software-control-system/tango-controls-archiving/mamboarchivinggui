package fr.soleil.mambo.models;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Family;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.archiving.tango.entity.ui.comparator.EntitiesComparator;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.ObjectUtils;

public abstract class AttributesTreeModel<A extends Attribute> extends DefaultTreeModel {

    private static final long serialVersionUID = -6525796701421757733L;

    protected List<Domain> domains;
    protected Map<String, A> attributesHT = new TreeMap<>(Collator.getInstance());

    private static String DEFAULT_ROOT_NAME = "ROOT";
    public static final int CONTEXT_TREE_DEPTH = 5;

    /**
     * @return 8 juil. 2005
     */
    public static DefaultMutableTreeNode findRootInSystemProperties() {
        String rootName = System.getProperty("TANGO_HOST");

        if (rootName == null) {
            rootName = DEFAULT_ROOT_NAME;
        }

        return new DefaultMutableTreeNode(rootName);
    }

    /**
     * @param requestDomains
     * @param toTest
     * @return
     */
    private static Domain getDomain(final Collection<Domain> requestDomains, final String toTest) {
        Domain result = null;
        for (final Domain domain : requestDomains) {
            if (domain.getName().equalsIgnoreCase(toTest)) {
                result = domain;
                break;
            }
        }
        return result;
    }

    public static List<Domain> intersectDomains(final List<Domain> before, final Collection<Domain> requestDomains) {
        final List<Domain> after = new ArrayList<>(before);
        final Iterator<Domain> enumD = after.iterator();
        while (enumD.hasNext()) {
            final Domain nextD = enumD.next();
            final Domain requestDomain = getDomain(requestDomains, nextD.getName());
            if (requestDomain == null) {
                enumD.remove();
            } else {
                List<Family> fams = nextD.getFamilies();
                for (final Family nextF : fams) {
                    final Family requestFamily = requestDomain.getFamily(nextF.getName());
                    if (requestFamily == null) {
                        nextD.removeFamily(nextF.getName());
                    } else {
                        final List<Member> mems = nextF.getMembers();
                        for (final Member nextM : mems) {
                            final Member requestMember = requestFamily.getMember(nextM.getName());
                            if (requestMember == null) {
                                nextF.removeMember(nextM.getName());
                            } else {
                                List<Attribute> attributes = nextM.getAttributes();
                                for (final Attribute nextA : attributes) {
                                    final Attribute requesteAttribute = requestMember.getAttribute(nextA.getName());
                                    if (requesteAttribute == null) {
                                        nextM.removeAttribute(nextA.getName());
                                    }
                                }
                                attributes.clear();
                            }
                        }
                    }
                }
            }
        }
        return after;
    }

    /**
     * 
     */
    public AttributesTreeModel() {
        super(findRootInSystemProperties());
    }

    public boolean hasAttributes() {
        Map<String, A> attrMap = this.attributesHT;
        return attrMap != null && !attrMap.isEmpty();
    }

    /**
     * @param domains
     *            8 juil. 2005
     */
    public void build(final List<Domain> domains) {
        if (attributesHT == null) {
            attributesHT = new TreeMap<>(Collator.getInstance());
        } else {
            attributesHT.clear();
        }
        this.domains = domains;
        if (domains != null) {
            Collections.sort(domains, new EntitiesComparator());
            for (final Domain domain : this.domains) {
                // START CURRENT DOMAIN
                final int currentDomainNr = ((DefaultMutableTreeNode) getRoot()).getChildCount();
                DefaultMutableTreeNode domainNode = null;
                for (int i = 0; i < currentDomainNr; i++) {
                    final DefaultMutableTreeNode createdDomain = (DefaultMutableTreeNode) ((DefaultMutableTreeNode) getRoot())
                            .getChildAt(i);
                    final String nodeName = (String) createdDomain.getUserObject();
                    if (nodeName.equalsIgnoreCase(domain.getName())) {
                        domainNode = createdDomain;
                        break;
                    }
                }
                if (domainNode == null) {
                    domainNode = new DefaultMutableTreeNode(domain.getName());
                    ((DefaultMutableTreeNode) getRoot()).add(domainNode);
                }
                final List<Family> familiesToSort = domain.getFamilies();

                Collections.sort(familiesToSort, new EntitiesComparator());

                for (final Family family : familiesToSort) {
                    // START CURRENT FAMILY
                    final int currentFamilyNr = domainNode.getChildCount();
                    DefaultMutableTreeNode familyNode = null;
                    for (int i = 0; i < currentFamilyNr; i++) {
                        final DefaultMutableTreeNode createdFamily = (DefaultMutableTreeNode) domainNode.getChildAt(i);
                        final String nodeName = (String) createdFamily.getUserObject();
                        if (nodeName.equalsIgnoreCase(family.getName())) {
                            familyNode = createdFamily;
                            break;
                        }
                    }
                    if (familyNode == null) {
                        familyNode = new DefaultMutableTreeNode(family.getName());
                        domainNode.add(familyNode);
                    }

                    final List<Member> membersToSort = family.getMembers();
                    Collections.sort(membersToSort, new EntitiesComparator());

                    for (final Member member : membersToSort) {
                        // START CURRENT MEMBER
                        final int currentMemberNr = familyNode.getChildCount();
                        DefaultMutableTreeNode memberNode = null;
                        for (int i = 0; i < currentMemberNr; i++) {
                            final DefaultMutableTreeNode createdMember = (DefaultMutableTreeNode) familyNode
                                    .getChildAt(i);
                            final String nodeName = (String) createdMember.getUserObject();
                            if (nodeName.equalsIgnoreCase(member.getName())) {
                                memberNode = createdMember;
                                break;
                            }
                        }
                        if (memberNode == null) {
                            memberNode = new DefaultMutableTreeNode(member.getName());
                            familyNode.add(memberNode);
                        }

                        final Collection<Attribute> coll = member.getAttributes();
                        final List<A> attributesToSort = new ArrayList<>();
                        final Iterator<Attribute> it = coll.iterator();
                        while (it.hasNext()) {
                            attributesToSort.add(getValidAttributeFor(it.next()));
                        }
                        coll.clear();
                        Collections.sort(attributesToSort, new EntitiesComparator());
                        for (int attrIndex = 0; attrIndex < attributesToSort.size(); attrIndex++) {
                            // START CURRENT ATTRIBUTE
                            final A attribute = attributesToSort.get(attrIndex);
                            // --
                            attribute.setCompleteName(domain.getName() + TangoDeviceHelper.SLASH + family.getName()
                                    + TangoDeviceHelper.SLASH + member.getName() + TangoDeviceHelper.SLASH
                                    + attribute.getName());
                            attribute.setDomain(domain.getName());
                            attribute.setFamily(family.getName());
                            attribute.setMember(member.getName());
                            attribute.setDevice(member.getName());
                            // --
                            final DefaultMutableTreeNode attributeNode = new DefaultMutableTreeNode(
                                    attribute.getName());
                            memberNode.add(attributeNode);
                            addAttribute(attributeNode.getPath(), attribute);
                            // END CURRENT ATTRIBUTE
                        }
                        attributesToSort.clear();
                        // END CURRENT MEMBER
                    }
                    membersToSort.clear();
                    // END CURRENT FAMILY
                }
                familiesToSort.clear();
                // END CURRENT DOMAIN
            }
        }
    }

    /**
     * @param attribute
     *            29 juin 2005
     */
    public void addAttribute(final TreeNode[] path, final A attribute) {
        attributesHT.put(translatePathIntoKey(path), attribute);
    }

    public Map<String, A> getAttributesCopy() {
        return (attributesHT == null ? null : new LinkedHashMap<>(attributesHT));
    }

    public A getAttribute(String key) {
        return attributesHT.get(key);
    }

    /**
     * @return 8 juil. 2005
     */
    public Collection<A> getTreeAttributes() {
        return attributesHT.values();
    }

    /**
     * @param path
     * @return 8 juil. 2005
     */
    public static String translatePathIntoKey(final TreeNode[] path) {
        String ret = ObjectUtils.EMPTY_STRING;

        for (int i = 1; i < path.length; i++) {
            ret += path[i].toString();
            if (i < path.length - 1) {
                ret += TangoDeviceHelper.SLASH;
            }
        }

        return ret;
    }

    public void removeAll() {
        final DefaultMutableTreeNode root = (DefaultMutableTreeNode) getRoot();
        final String name = (String) root.getUserObject();
        attributesHT.clear();
        setRoot(new DefaultMutableTreeNode(name));
    }

    /**
     * @param string
     *            14 sept. 2005
     */
    public void removeFromDomains(final String string) {
        if ((string != null) && (!string.isEmpty())) {
            try {
                final StringTokenizer st = new StringTokenizer(string, TangoDeviceHelper.SLASH);
                final String dom_s = st.nextToken();
                final String fam_s = st.nextToken();
                final String mem_s = st.nextToken();
                final String attr_s = st.nextToken();

                final Domain dom = Domain.hasDomain(this.domains, dom_s);
                final Family fam = dom.getFamily(fam_s);
                final Member mem = fam.getMember(mem_s);
                mem.removeAttribute(attr_s);
            } catch (final Exception e) {
                // do nothing
            }
        }
    }

    /**
     * @param attribute
     *            14 sept. 2005
     */
    public void addToDomains(final Attribute attribute) {
        final String dom_s = attribute.getDomainName();
        final String fam_s = attribute.getFamilyName();
        final String mem_s = attribute.getMemberName();
        final String attr_s = attribute.getName();

        Domain dom = Domain.hasDomain(this.domains, dom_s);
        if (dom == null) {
            dom = new Domain(dom_s);
            final Family fam = new Family(fam_s);
            final Member mem = new Member(mem_s);

            mem.addAttribute(attribute);
            fam.addMember(mem);
            dom.addFamily(fam);

            this.domains.add(dom);
        } else {
            Family fam = dom.getFamily(fam_s);
            if (fam == null) {
                fam = new Family(fam_s);
                final Member mem = new Member(mem_s);

                mem.addAttribute(attribute);
                fam.addMember(mem);
                dom.addFamily(fam);
            } else {
                Member mem = fam.getMember(mem_s);
                if (mem == null) {
                    mem = new Member(mem_s);

                    mem.addAttribute(attribute);
                    fam.addMember(mem);
                } else {
                    final Attribute att = mem.getAttribute(attr_s);
                    if (att == null) {
                        mem.addAttribute(attribute);
                    }
                }
            }
        }
    }

    public abstract A getValidAttributeFor(Attribute attr);

}
