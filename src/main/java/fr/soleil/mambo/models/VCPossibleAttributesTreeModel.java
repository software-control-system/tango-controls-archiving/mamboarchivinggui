// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/models/VCPossibleAttributesTreeModel.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class VCPossibleAttributesTreeModel.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: VCPossibleAttributesTreeModel.java,v $
// Revision 1.5 2006/11/09 14:23:42 ounsy
// domain/family/member/attribute refactoring
//
// Revision 1.4 2006/09/22 09:34:41 ounsy
// refactoring du package mambo.datasources.db
//
// Revision 1.3 2006/06/28 12:32:12 ounsy
// db attributes buffering
//
// Revision 1.2 2005/11/29 18:27:08 chinkumo
// no message
//
// Revision 1.1.2.4 2005/09/26 07:52:25 chinkumo
// Miscellaneous changes...
//
// Revision 1.1.2.3 2005/09/15 10:30:05 chinkumo
// Third commit !
//
// Revision 1.1.2.2 2005/09/14 15:41:44 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.models;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.datasources.db.attributes.AttributeManagerFactory;
import fr.soleil.mambo.datasources.db.attributes.IAttributeManager;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;

public class VCPossibleAttributesTreeModel extends VCTreeModel {

    private static final long serialVersionUID = -4781579937975464506L;

    private static final Logger LOGGER = LoggerFactory.getLogger(VCPossibleAttributesTreeModel.class);

    public VCPossibleAttributesTreeModel() {
        this(true);
    }

    public VCPossibleAttributesTreeModel(final Boolean historic) {
        super();
        setHistoric(historic);
    }

    public void build(final IAttributeManager source, final String pattern) {
        final Criterions searchCriterions = ITangoManager.getAttributesSearchCriterions(pattern);
        List<Domain> domains = null;
        setRootName(historic);
        try {
            domains = source.loadDomains(searchCriterions, historic, false);
        } catch (final DevFailed | ArchivingException e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
        }
        applyDomains(domains);
    }

    public void applyDomains(List<Domain> domains) {
        removeAll();
        super.build(domains);
    }

    public void setHistoric(final Boolean historic) {
        this.historic = historic;
        build(AttributeManagerFactory.getCurrentImpl(), null);
        setRootName(historic);
        reload();
    }

}
