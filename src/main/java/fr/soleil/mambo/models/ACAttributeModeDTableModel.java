package fr.soleil.mambo.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeModeDTableModel extends AACAttributeModeTableModel<ModeDifference> {

    private static final long serialVersionUID = -6020867456292427599L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeModeDTableModel.class);

    private static final String MODED_NAME = "ARCHIVING_ATTRIBUTES_DETAIL_MODED_NAME";
    private static final String MODED_PERIOD = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODED_PERIOD");

    private static ACAttributeModeDTableModel hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeDTableModel(Boolean historic) {
        super(historic);
    }

    public static ACAttributeModeDTableModel getInstance(Boolean historic) {
        ACAttributeModeDTableModel instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeDTableModel.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeDTableModel.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeDTableModel.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

    @Override
    protected String getModeColumnValue(ModeDifference mode, int rowIndex) {
        String ret;
        if ((mode != null) && (rowIndex == 0)) {
            ret = getPeriodAsString(mode.getPeriod());
        } else {
            ret = ObjectUtils.EMPTY_STRING;
        }
        return ret;
    }

    @Override
    protected int getModeType() {
        return ArchivingConfigurationMode.TYPE_D;
    }

    @Override
    protected ModeDifference getSubMode(Mode mode) {
        return mode == null ? null : mode.getModeD();
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    protected String getSecondColumnValue(int rowIndex) {
        String ret;
        if (rowIndex == 0) {
            ret = MODED_PERIOD;
        } else {
            ret = ObjectUtils.EMPTY_STRING;
        }
        return ret;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getTitleKey() {
        return MODED_NAME;
    }

}
