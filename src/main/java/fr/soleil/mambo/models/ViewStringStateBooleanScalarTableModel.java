/*
 * Synchrotron Soleil File : ViewStringStateBooleanScalarTableModel.java Project
 * : Mambo_CVS Description : Author : SOLEIL Original : 7 mars 2006 Revision:
 * Author: Date: State: Log: ViewStringStateBooleanScalarTableModel.java,v
 */
package fr.soleil.mambo.models;

import java.lang.reflect.Array;
import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.swing.table.DefaultTableModel;

import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.tools.Messages;

public class ViewStringStateBooleanScalarTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -1455103243358102874L;

    private DbData readReference;
    private DbData writeReference;
    private final SimpleDateFormat genFormatUS;

    public ViewStringStateBooleanScalarTableModel() {
        super();
        genFormatUS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        readReference = null;
        writeReference = null;
    }

    public void setDataOutsideOfEDT(final DbData... splitData) {
        if (splitData == null || splitData.length != 2) {
            readReference = null;
            writeReference = null;
        } else {
            readReference = splitData[0];
            writeReference = splitData[1];
        }
    }

    public void setData(final DbData... splitData) {
        setDataOutsideOfEDT(splitData);
        fireTableStructureChanged();
    }

    protected boolean isValid(final DbData reference) {
        boolean result = false;
        if (reference != null && reference.getTimedData() != null && reference.getTimedData().length > 0) {
            final NullableTimedData dataElement = reference.getTimedData()[0];
            result = dataElement.getValue() != null && Array.getLength(dataElement.getValue()) > 0;
        }
        return result;
    }

    @Override
    public int getRowCount() {
        int length = 0;
        final DbData readReference = this.readReference, writeReference = this.writeReference;
        if (readReference != null && readReference.getTimedData() != null) {
            length = readReference.getTimedData().length;
        }
        if (writeReference != null && writeReference.getTimedData() != null
                && writeReference.getTimedData().length > length) {
            length = writeReference.getTimedData().length;
        }
        return length;
    }

    @Override
    public int getColumnCount() {
        int col;
        final DbData readReference = this.readReference, writeReference = this.writeReference;
        if (isValid(readReference) && isValid(writeReference)) {
            col = 3;
        } else {
            col = 2;
        }
        return col;
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        Object value = null;
        switch (columnIndex) {
            case 0:
                if (isValid(readReference)) {
                    value = genFormatUS.format(new Date(readReference.getTimedData()[rowIndex].getTime()));
                } else if (isValid(writeReference)) {
                    value = genFormatUS.format(new Date(writeReference.getTimedData()[rowIndex].getTime()));
                }
                break;
            case 1:
                if (isValid(readReference)) {
                    if (!isValueNull(readReference, rowIndex)) {
                        value = Array.get(readReference.getTimedData()[rowIndex].getValue(), 0);
                    }
                } else if (isValid(writeReference)) {
                    if (!isValueNull(writeReference, rowIndex)) {
                        value = Array.get(writeReference.getTimedData()[rowIndex].getValue(), 0);
                    }
                }
                break;
            case 2:
                if (isValid(writeReference)) {
                    if (!isValueNull(writeReference, rowIndex)) {
                        value = Array.get(writeReference.getTimedData()[rowIndex].getValue(), 0);
                    }
                }
                break;
            default:
                value = null;
                break;
        }

        return value;
    }

    private boolean isValueNull(final DbData data, final int rowIndex) {
        boolean isNull = false;
        if (Array.getLength(data.getTimedData()[rowIndex].getNullElements()) > 0) {
            final boolean[] nullElements = (boolean[]) data.getTimedData()[rowIndex].getNullElements();
            for (int i = 0; i < nullElements.length; i++) {
                if (nullElements[i]) {
                    isNull = true;
                    break;
                }
            }
        }
        return isNull;
    }

    @Override
    public String getColumnName(final int column) {
        String name;
        final DbData readReference = this.readReference, writeReference = this.writeReference;
        switch (column) {
            case 0:
                name = Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_DATE");
                break;
            case 1:
                if (isValid(readReference)) {
                    name = Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_READ");
                } else if (isValid(writeReference)) {
                    name = Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_WRITE");
                } else {
                    name = Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_VALUE");
                }
                break;
            case 2:
                if (isValid(writeReference)) {
                    name = Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_WRITE");
                } else {
                    name = ObjectUtils.EMPTY_STRING;
                }
                break;
            default:
                name = ObjectUtils.EMPTY_STRING;
                break;
        }
        return name;
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        return false;
    }

    public void clearData() {
        setData((DbData[]) null);
    }

}
