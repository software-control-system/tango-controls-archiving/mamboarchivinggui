package fr.soleil.mambo.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeModesTitleTableModel extends AACAttributeDetailsTableModel {

    private static final long serialVersionUID = -7802396446029587008L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeModesTitleTableModel.class);

    private static final String TABLE_MODE = "ARCHIVING_ATTRIBUTES_DETAIL_TITLE_TABLE_MODE";
    private static final String TABLE_CARACTERISTIC = Messages
            .getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TITLE_TABLE_CARACTERISTIC");
    private static final String TABLE_AC = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TITLE_TABLE_AC");
    private static final String TABLE_DB = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TITLE_TABLE_DB");

    private static ACAttributeModesTitleTableModel hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModesTitleTableModel(Boolean historic) {
        super(historic);
    }

    public static ACAttributeModesTitleTableModel getInstance(Boolean historic) {
        ACAttributeModesTitleTableModel instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModesTitleTableModel.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModesTitleTableModel.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModesTitleTableModel.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    protected String getSecondColumnValue(int rowIndex) {
        return TABLE_CARACTERISTIC;
    }

    @Override
    protected String getACColumnValue(int rowIndex) {
        return TABLE_AC;
    }

    @Override
    protected Object getDBColumnValue(int rowIndex) throws ArchivingException {
        return TABLE_DB;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getTitleKey() {
        return TABLE_MODE;
    }

    @Override
    public boolean toPaint() {
        return true;
    }

}
