package fr.soleil.mambo.models;

import java.awt.Color;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeHDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeDetailTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -9015155466672058672L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeDetailTableModel.class);

    private final String[] columnsNames;
    private final String isArchived;
    private final String modes;

    private static ACAttributeDetailTableModel instance;
    private ArchivingConfigurationAttribute attribute;
    private ArchivingConfigurationAttributeHDBProperties HDBProperties;
    private ArchivingConfigurationAttributeTDBProperties TDBProperties;

    private final JTextArea hdbArea, tdbArea;
    private final JScrollPane hdbScroll, tdbScroll;

    /**
     * @param snapshot
     * @return 8 juil. 2005
     */
    public static ACAttributeDetailTableModel getInstance() {
        if (instance == null) {
            final String msgCol1 = "A";
            final String msgCol2 = "B";
            final String msgTDB = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TDB_LABEL");
            final String msgHDB = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_HDB_LABEL");

            final String[] columnNames = new String[4];

            columnNames[0] = msgCol1;
            columnNames[1] = msgCol2;
            columnNames[2] = msgTDB;
            columnNames[3] = msgHDB;

            instance = new ACAttributeDetailTableModel(columnNames, 8);
        }

        return instance;
    }

    private ACAttributeDetailTableModel(final String[] _columnNames, final int rowCount) {
        super(_columnNames, rowCount);
        columnsNames = _columnNames;
        isArchived = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_IS_ARCHIVED_LABEL");
        modes = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODE_LABEL");
        hdbArea = new JTextArea(ObjectUtils.EMPTY_STRING);
        tdbArea = new JTextArea(ObjectUtils.EMPTY_STRING);
        hdbArea.setBackground(new Color(210, 190, 190));
        tdbArea.setBackground(new Color(210, 190, 190));
        hdbScroll = new JScrollPane(hdbArea);
        tdbScroll = new JScrollPane(tdbArea);
        hdbScroll.setBackground(new Color(210, 190, 190));
        tdbScroll.setBackground(new Color(210, 190, 190));
    }

    /**
     * 17 juin 2005
     * 
     * @throws SnapshotingException
     */
    /**
     * @param snapshot
     *            8 juil. 2005
     */
    public void load(final ArchivingConfigurationAttribute attribute) {
        hdbArea.setText(ObjectUtils.EMPTY_STRING);
        tdbArea.setText(ObjectUtils.EMPTY_STRING);
        this.attribute = attribute;

        final ArchivingConfigurationAttributeProperties attrProperties = attribute.getProperties();
        HDBProperties = attrProperties.getHDBProperties();
        TDBProperties = attrProperties.getTDBProperties();

        if (TDBProperties != null) {
            fillArea(TDBProperties, tdbArea);
            if (TDBProperties.getExportPeriod() >= 0) {
                tdbArea.setText(tdbArea.getText() + GUIUtilities.CRLF + "Export Period : "
                        + TDBProperties.getExportPeriod() + "ms");
            }
        }
        if (HDBProperties != null) {
            fillArea(HDBProperties, hdbArea);
        }

        fireTableDataChanged();
    }

    private void fillArea(final ArchivingConfigurationAttributeDBProperties properties, final JTextArea textArea) {
        final ArchivingConfigurationMode ACPMode = properties.getMode(ArchivingConfigurationMode.TYPE_P);
        if (ACPMode != null) {
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Periodic Mode Period : "
                    + ACPMode.getMode().getModeP().getPeriod() + "ms");
        }
        final ArchivingConfigurationMode ACAMode = properties.getMode(ArchivingConfigurationMode.TYPE_A);
        if (ACAMode != null) {
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Absolute Mode Period : "
                    + ACAMode.getMode().getModeA().getPeriod() + "ms");
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Absolute Mode Upper Limit : "
                    + ACAMode.getMode().getModeA().getValSup());
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Absolute Mode Lower Limit : "
                    + ACAMode.getMode().getModeA().getValInf());
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Absolute Mode Slow Drift : "
                    + ACAMode.getMode().getModeA().isSlow_drift());
        }
        final ArchivingConfigurationMode ACRMode = properties.getMode(ArchivingConfigurationMode.TYPE_R);
        if (ACRMode != null) {
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Relative Mode Period : "
                    + ACRMode.getMode().getModeR().getPeriod() + "ms");
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Relative Mode Upper Percent Limit : "
                    + ACRMode.getMode().getModeR().getPercentSup() + "%");
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Relative Mode Lower Percent Limit : "
                    + ACRMode.getMode().getModeR().getPercentInf() + "%");
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Relative Mode Slow Drift : "
                    + ACRMode.getMode().getModeR().isSlow_drift());
        }
        final ArchivingConfigurationMode ACTMode = properties.getMode(ArchivingConfigurationMode.TYPE_T);
        if (ACTMode != null) {
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Threshold Mode Period : "
                    + ACTMode.getMode().getModeT().getPeriod() + "ms");
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Threshold Mode Upper Limit : "
                    + ACTMode.getMode().getModeT().getThresholdSup());
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Threshold Mode Lower Limit : "
                    + ACTMode.getMode().getModeT().getThresholdInf());
        }
        final ArchivingConfigurationMode ACDMode = properties.getMode(ArchivingConfigurationMode.TYPE_D);
        if (ACDMode != null) {
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "On Difference Mode Period : "
                    + ACDMode.getMode().getModeD().getPeriod() + "ms");
        }
        final ArchivingConfigurationMode ACCMode = properties.getMode(ArchivingConfigurationMode.TYPE_C);
        if (ACCMode != null) {
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Calculation Mode Period : "
                    + ACPMode.getMode().getModeC().getPeriod() + "ms");
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Calculation Mode Range : "
                    + ACPMode.getMode().getModeC().getRange());
            textArea.setText(textArea.getText() + GUIUtilities.CRLF + "Calculation Mode Type : "
                    + ACPMode.getMode().getModeC().getTypeCalcul());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return 4;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        return 10;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        switch (columnIndex) {
            case 0:
                return getFirstColumnValue(rowIndex);

            case 1:
                return getSecondColumnValue(rowIndex);

            case 2:
                return getTDBColumnValue(rowIndex);

            case 3:
                return getHDBColumnValue(rowIndex);

            default:
                return null;
            // we have a bug
        }
    }

    /**
     * @param rowIndex
     * @return 27 juil. 2005
     */
    private Object getHDBColumnValue(final int rowIndex) {
        Object ret = Boolean.FALSE;
        if (rowIndex == 0) {
            ret = columnsNames[3];
        } else if (rowIndex == 1) {
            if (attribute != null) {
                try {
                    ret = Boolean.valueOf(
                            ArchivingManagerFactory.getCurrentImpl().isArchived(attribute.getCompleteName(), true));
                } catch (final Exception e) {
                    LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                }
            }
        } else if (rowIndex == 9) {
            ret = hdbScroll;
        } else if ((HDBProperties != null) && !HDBProperties.isEmpty()) {
            ret = Boolean.valueOf(hasMode(HDBProperties, rowIndex));
        }
        return ret;
    }

    /**
     * @param properties
     * @param rowIndex
     * @return 27 juil. 2005
     */
    private boolean hasMode(final ArchivingConfigurationAttributeDBProperties properties, final int rowIndex) {
        boolean ret = false;
        switch (rowIndex) {
            case 2:
                ret = properties.hasMode(ArchivingConfigurationMode.TYPE_P);
                if (ret) {
                    if (properties == HDBProperties) {

                    }
                }
                break;

            case 3:
                ret = properties.hasMode(ArchivingConfigurationMode.TYPE_A);
                break;

            case 4:
                ret = properties.hasMode(ArchivingConfigurationMode.TYPE_R);
                break;

            case 5:
                ret = properties.hasMode(ArchivingConfigurationMode.TYPE_T);
                break;

            case 6:
                ret = properties.hasMode(ArchivingConfigurationMode.TYPE_D);
                break;

            case 7:
                ret = properties.hasMode(ArchivingConfigurationMode.TYPE_C);
                break;

            case 8:
                ret = properties.hasMode(ArchivingConfigurationMode.TYPE_E);
                break;
        }

        return ret;
    }

    /**
     * @param rowIndex
     * @return 27 juil. 2005
     */
    private Object getTDBColumnValue(final int rowIndex) {
        Boolean ret = new Boolean(false);
        if (rowIndex == 0) {
            return columnsNames[2];
        } else if (rowIndex == 1) {
            if (attribute == null) {
                return ret;
            }

            try {
                ret = new Boolean(
                        ArchivingManagerFactory.getCurrentImpl().isArchived(attribute.getCompleteName(), false));
            } catch (final Exception e) {
                LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            }
        } else if (rowIndex == 9) {
            return tdbScroll;
        } else {
            if (TDBProperties == null || TDBProperties.isEmpty()) {
                return ret;
            }

            ret = hasMode(TDBProperties, rowIndex);
        }

        return ret;
    }

    /**
     * @param rowIndex
     * @return 27 juil. 2005
     */
    private Object getSecondColumnValue(final int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;

        switch (rowIndex) {
            case 2:
                ret = ArchivingConfigurationMode.TYPE_P_NAME;
                break;

            case 3:
                ret = ArchivingConfigurationMode.TYPE_A_NAME;
                break;

            case 4:
                ret = ArchivingConfigurationMode.TYPE_R_NAME;
                break;

            case 5:
                ret = ArchivingConfigurationMode.TYPE_T_NAME;
                break;

            case 6:
                ret = ArchivingConfigurationMode.TYPE_D_NAME;
                break;

            case 7:
                ret = ArchivingConfigurationMode.TYPE_C_NAME;
                break;

            case 8:
                ret = ArchivingConfigurationMode.TYPE_E_NAME;
                break;
        }

        return ret;
    }

    /**
     * @param rowIndex
     * @return 27 juil. 2005
     */
    private Object getFirstColumnValue(final int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;

        switch (rowIndex) {
            case 1:
                ret = isArchived;
                break;

            case 2:
                ret = modes;
                break;
        }

        return ret;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getColumnName(int)
     */
    @Override
    public String getColumnName(final int columnIndex) {
        return columnsNames[columnIndex];
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#setColumnName(int, String)
     */
    public void setColumnName(final int columnIndex, final String columnName) {
        if (columnIndex < getColumnCount()) {
            columnsNames[columnIndex] = columnName;
        }
    }

}
