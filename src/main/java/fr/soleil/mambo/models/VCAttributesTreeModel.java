package fr.soleil.mambo.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.comparators.MamboToStringObjectComparator;
import fr.soleil.mambo.components.view.VCAttributesSelectTree;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.datasources.db.attributes.IAttributeManager;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;
import fr.soleil.mambo.tools.MamboDbUtils;

public class VCAttributesTreeModel extends VCTreeModel {

    private static final long serialVersionUID = -1886153176720299009L;

    private static final Logger LOGGER = LoggerFactory.getLogger(VCAttributesTreeModel.class);

    private VCAttributesSelectTree treeInstance = null;
    private final VCPossibleAttributesTreeModel possibleAttributesTreeModel;
    private List<Domain> savedDomains;

    public VCAttributesTreeModel(final VCPossibleAttributesTreeModel possibleAttributesTreeModel) {
        super();
        this.possibleAttributesTreeModel = possibleAttributesTreeModel;
        setHistoric(MamboDbUtils.isHistoricAccordingToAvailableDb(), false);
    }

    /**
     * @param tree
     *            8 juil. 2005
     */
    public void setTree(final VCAttributesSelectTree tree) {
        treeInstance = tree;
    }

    public void addSelectedAttibutes(final Collection<TreePath> validSelected) {
        final Map<String, ViewConfigurationAttribute> htAttr = attributesHT;

        for (TreePath aPath : validSelected) {
            // START CURRENT SELECTED PATH
            DefaultMutableTreeNode currentTreeObject = null;
            for (int depth = 0; depth < CONTEXT_TREE_DEPTH; depth++) {
                final Object currentPathObject = aPath.getPathComponent(depth);

                if (depth == 0) {
                    currentTreeObject = (DefaultMutableTreeNode) getRoot();
                    continue;
                }

                final int numOfChildren = getChildCount(currentTreeObject);
                final List<String> vect = new ArrayList<String>();
                for (int childIndex = 0; childIndex < numOfChildren; childIndex++) {
                    final Object childAt = getChild(currentTreeObject, childIndex);
                    vect.add(childAt.toString());
                }

                if (vect.contains(currentPathObject.toString())) {
                    final int pos = vect.indexOf(currentPathObject.toString());
                    currentTreeObject = (DefaultMutableTreeNode) getChild(currentTreeObject, pos);
                } else {
                    vect.add(currentPathObject.toString());
                    Collections.sort(vect, new MamboToStringObjectComparator());
                    final DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(currentPathObject.toString());
                    // int index = 0;
                    final int index = vect.indexOf(currentPathObject.toString());
                    insertNodeInto(newChild, currentTreeObject, index);

                    if (depth == CONTEXT_TREE_DEPTH - 1) {
                        final TreeNode[] path = newChild.getPath();
                        // ----------------
                        final TreePath treePath = new TreePath(path);
                        treeInstance.makeVisible(treePath);
                        // ----------------

                        final ViewConfigurationAttribute attribute = possibleAttributesTreeModel
                                .getAttribute(translatePathIntoKey(path));

                        if (attribute != null) {
                            htAttr.put(translatePathIntoKey(path), attribute);
                            super.addToDomains(attribute);
                        }
                    }

                    currentTreeObject = newChild;
                }
            }
            // END CURRENT SELECTED PATH
        }
    }

    public void removeSelectedAttributes(final Collection<TreePath> validSelected) {
        for (TreePath aPath : validSelected) {
            final Map<String, ViewConfigurationAttribute> htAttr = attributesHT;
            final String dom_s = aPath.getPathComponent(1).toString();
            final String fam_s = aPath.getPathComponent(2).toString();
            final String mem_s = aPath.getPathComponent(3).toString();
            final String att_s = aPath.getPathComponent(4).toString();
            final String path = dom_s + GUIUtilities.TANGO_DELIM + fam_s + GUIUtilities.TANGO_DELIM + mem_s
                    + GUIUtilities.TANGO_DELIM + att_s;
            htAttr.remove(path);
            removeFromDomains(path);
            final MutableTreeNode node = (MutableTreeNode) aPath.getLastPathComponent();
            try {
                if (node.getParent() != null) {
                    removeNodeFromParent(node);
                }
            } catch (final Exception e) {
                // do nothing
                LOGGER.debug("Failed to remove node", e);
            }
        }
    }

    @Override
    public void setRootName(final Boolean historic) {
        this.historic = historic;
        super.setRootName(historic);
    }

    public void setHistoric(final Boolean historic, final boolean reloadDomains) {
        this.historic = historic;
        removeAll();
        setRootName(historic);
        if (reloadDomains) {
            build(domains);
        }
        reload();
    }

    /**
     * @param manager
     * @param searchCriterions
     * @param _historic
     */
    public void match(final IAttributeManager manager, String pattern) {
        final Criterions searchCriterions = ITangoManager.getAttributesSearchCriterions(pattern);
        Collection<Domain> requestDomains = null;
        setRootName(historic);
        try {
            requestDomains = manager.loadDomains(searchCriterions, historic, false);

        } catch (final DevFailed | ArchivingException e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
        }

        // don't build immediately, first do the intersect with the current model
        prepareSavedDomains();
        applyDomains(intersectDomains(savedDomains, requestDomains));

    }

    protected void prepareSavedDomains() {
        if (savedDomains == null) {
            savedDomains = domains;
        }
    }

    public List<Domain> prepareAndGetSavedDomains() {
        prepareSavedDomains();
        return savedDomains == null ? null : new ArrayList<>(savedDomains);
    }

    public void applyDomains(List<Domain> domains) {
        removeAll();
        build(domains);
    }

}
