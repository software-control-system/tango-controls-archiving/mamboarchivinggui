package fr.soleil.mambo.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeModePTableModel extends AACAttributeModeTableModel<ModePeriode> {

    private static final long serialVersionUID = 610233888427647059L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeModePTableModel.class);

    private static final String MODEP_NAME = "ARCHIVING_ATTRIBUTES_DETAIL_MODEP_NAME";
    private static final String MODEP_PERIOD = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEP_PERIOD");

    private static ACAttributeModePTableModel hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModePTableModel(Boolean historic) {
        super(historic);
    }

    public static ACAttributeModePTableModel getInstance(Boolean historic) {
        ACAttributeModePTableModel instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModePTableModel.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModePTableModel.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModePTableModel.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

    @Override
    protected String getModeColumnValue(ModePeriode mode, int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;
        if ((mode != null) && rowIndex == 0) {
            ret = getPeriodAsString(mode.getPeriod());
        }
        return ret;
    }

    @Override
    protected int getModeType() {
        return ArchivingConfigurationMode.TYPE_P;
    }

    @Override
    protected ModePeriode getSubMode(Mode mode) {
        return mode == null ? null : mode.getModeP();
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    protected String getSecondColumnValue(int rowIndex) {
        return MODEP_PERIOD;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getTitleKey() {
        return MODEP_NAME;
    }

}
