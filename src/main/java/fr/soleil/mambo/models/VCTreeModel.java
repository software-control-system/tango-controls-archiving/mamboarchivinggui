package fr.soleil.mambo.models;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;

public abstract class VCTreeModel extends AttributesTreeModel<ViewConfigurationAttribute> {

    private static final long serialVersionUID = -7611013843925229243L;

    protected Boolean historic;

    public VCTreeModel() {
        super();
    }

    @Override
    public ViewConfigurationAttribute getValidAttributeFor(Attribute attr) {
        if (attr == null) {
            return null;
        } else if (attr instanceof ViewConfigurationAttribute) {
            return (ViewConfigurationAttribute) attr;
        } else {
            return new ViewConfigurationAttribute(attr);
        }
    }

    public void setRootName(final Boolean historic) {
        final DefaultMutableTreeNode root = (DefaultMutableTreeNode) getRoot();
        if (historic == null) {
            root.setUserObject(ApiConstants.TTS);
        } else if (historic.booleanValue()) {
            root.setUserObject(ApiConstants.HDB);
        } else {
            root.setUserObject(ApiConstants.TDB);
        }
    }

    public Boolean isHistoric() {
        return historic;
    }

}
