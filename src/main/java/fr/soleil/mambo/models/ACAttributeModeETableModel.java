package fr.soleil.mambo.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.hdbtdb.api.tools.mode.EventMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeModeETableModel extends AACAttributeModeTableModel<EventMode> {

    private static final long serialVersionUID = -3289916590559280395L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeModeETableModel.class);

    private static final String MODEE_NAME = "ARCHIVING_ATTRIBUTES_DETAIL_MODEE_NAME";
    private static final String MODEE_ARCHIVED = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEE_ARCHIVED");

    private static ACAttributeModeETableModel hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeETableModel(Boolean historic) {
        super(historic);
    }

    public static ACAttributeModeETableModel getInstance(Boolean historic) {
        ACAttributeModeETableModel instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeETableModel.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeETableModel.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeETableModel.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

    @Override
    protected Object getDefaultValue() {
        return Boolean.FALSE;
    }

    @Override
    protected Object getModeColumnValue(EventMode mode, int rowIndex) {
        return mode == null ? Boolean.FALSE : Boolean.TRUE;
    }

    @Override
    protected int getModeType() {
        return ArchivingConfigurationMode.TYPE_E;
    }

    @Override
    protected EventMode getSubMode(Mode mode) {
        return mode == null ? null : mode.getEventMode();
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    protected String getSecondColumnValue(int rowIndex) {
        String ret;
        if (rowIndex == 0) {
            ret = MODEE_ARCHIVED;
        } else {
            ret = ObjectUtils.EMPTY_STRING;
        }
        return ret;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getTitleKey() {
        return MODEE_NAME;
    }

}
