/*
 * Synchrotron Soleil File : ViewSpectrumTableModel.java Project : Mambo_CVS
 * Description : Author : SOLEIL Original : 31 janv. 2006 Revision: Author:
 * Date: State: Log: ViewSpectrumTableModel.java,v
 */
package fr.soleil.mambo.models;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.table.DefaultTableModel;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.tools.Messages;

/**
 * @author SOLEIL
 */
public class ViewSpectrumTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -2469862174097438012L;

    public static final int R = 0;
    public static final int W = 1;
    public static final int RW = 2;
    public static final int UNKNOWN = 3;

    private boolean[] selectedRead;
    private boolean[] selectedWrite;
    private int viewSpectrumType;
    private String[] readNames;
    private String[] writeNames;

    public ViewSpectrumTableModel() {
        super();
        setNames(null, null);
    }

    public void setNames(String[] readNames, String[] writeNames) {
        this.readNames = readNames;
        this.writeNames = writeNames;
        int readLength = Math.max(0, computeReadLength());
        int writeLength = Math.max(0, computeWriteLength());
        selectedRead = new boolean[readLength];
        selectedWrite = new boolean[writeLength];
        for (int i = 0; i < selectedRead.length; i++) {
            selectedRead[i] = true;
        }
    }

    private int computeLength(String[] names) {
        int length;
        if (names == null) {
            length = -1;
        } else {
            length = names.length;
        }
        return length;
    }

    private int computeReadLength() {
        return computeLength(readNames);
    }

    private int computeWriteLength() {
        return computeLength(writeNames);
    }

    @Override
    public int getRowCount() {
        int readLength = Math.max(0, computeReadLength());
        int writeLength = Math.max(0, computeWriteLength());
        return Math.max(readLength, writeLength);
    }

    public int getSelectedReadRowCount() {
        int count = selectedRead.length;
        for (int i = 0; i < selectedRead.length; i++) {
            if (!selectedRead[i]) {
                count--;
            }
        }
        return count;
    }

    public int getSelectedWriteRowCount() {
        int count = selectedWrite.length;
        for (int i = 0; i < selectedWrite.length; i++) {
            if (!selectedWrite[i]) {
                count--;
            }
        }
        return count;
    }

    @Override
    public int getColumnCount() {
        if (computeReadLength() > -1 && computeWriteLength() > -1) {
            return 4;
        }
        return 2;
    }

    protected Boolean getBoolean(boolean[] array, int index) {
        Boolean value;
        if ((array != null) && (index > -1) && (index < array.length)) {
            value = Boolean.valueOf(array[index]);
        } else {
            value = null;
        }
        return value;
    }

    protected void setBoolean(boolean[] array, Boolean value, int index) {
        if ((value != null) && (array != null) && (index > -1) && (index < array.length)) {
            array[index] = value.booleanValue();
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (aValue instanceof Boolean) {
            if (columnIndex == 0) {
                if (selectedRead.length > 0) {
                    setBoolean(selectedRead, (Boolean) aValue, rowIndex);
                } else {
                    setBoolean(selectedWrite, (Boolean) aValue, rowIndex);
                }
            } else if (columnIndex == 3) {
                setBoolean(selectedWrite, (Boolean) aValue, rowIndex);
            }
            fireTableCellUpdated(rowIndex, columnIndex);
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        switch (columnIndex) {
            case 0:
                if (selectedRead.length > 0) {
                    value = getBoolean(selectedRead, rowIndex);
                } else if (selectedWrite.length > 0) {
                    value = getBoolean(selectedWrite, rowIndex);
                }
                break;
            case 1:
                if (selectedRead.length > 0) {
                    if (rowIndex < readNames.length) {
                        value = readNames[rowIndex];
                    } else {
                        value = Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA");
                    }
                } else if (selectedWrite.length > 0) {
                    if (rowIndex < writeNames.length) {
                        value = writeNames[rowIndex];
                    } else {
                        value = Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA");
                    }
                } else {
                    value = Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA");
                }
                break;
            case 2:
                if (rowIndex < writeNames.length) {
                    value = writeNames[rowIndex];
                } else {
                    value = Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA");
                }
                break;
            case 3:
                if (rowIndex < selectedWrite.length) {
                    value = new Boolean(selectedWrite[rowIndex]);
                }
                break;
            default:
                value = null;
        }
        return value;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean editable = false;
        if (columnIndex == 0) {
            if (selectedRead.length > 0) {
                if (rowIndex < selectedRead.length) {
                    editable = true;
                }
            } else if (selectedWrite.length > 0) {
                if (rowIndex < selectedWrite.length) {
                    editable = true;
                }
            }
        } else if (columnIndex == 3) {
            if (rowIndex < selectedWrite.length) {
                editable = true;
            }
        }
        return editable;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnIndex == 0 || columnIndex == 3 ? Boolean.class : super.getColumnClass(columnIndex);
    }

    @Override
    public String getColumnName(int column) {
        String ret = ObjectUtils.EMPTY_STRING;
        switch (column) {
            case 0:
                ret = Messages.getMessage("VIEW_SPECTRUM_FILTER_SELECT");
                ret = appendWritableToString(ret);
                break;
            case 1:
                if (viewSpectrumType == ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX) {
                    ret = Messages.getMessage("VIEW_SPECTRUM_SUBPART");
                } else if (viewSpectrumType == ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME) {
                    ret = Messages.getMessage("VIEW_SPECTRUM_SUBPART_TIME");
                }
                ret = appendWritableToString(ret);
                break;
            case 2:
                if (viewSpectrumType == ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX) {
                    ret = Messages.getMessage("VIEW_SPECTRUM_SUBPART");
                } else if (viewSpectrumType == ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME) {
                    ret = Messages.getMessage("VIEW_SPECTRUM_SUBPART_TIME");
                }
                ret += " " + Messages.getMessage("VIEW_SPECTRUM_WRITE");
                break;
            case 3:
                ret = Messages.getMessage("VIEW_SPECTRUM_FILTER_SELECT") + " "
                        + Messages.getMessage("VIEW_SPECTRUM_WRITE");
                break;
        }
        return ret;
    }

    private String appendWritableToString(String ret) {
        StringBuilder result = new StringBuilder(ret);
        if (selectedRead.length > 0) {
            result.append(" ").append(Messages.getMessage("VIEW_SPECTRUM_READ"));
        } else if (selectedWrite.length > 0) {
            result.append(" ").append(Messages.getMessage("VIEW_SPECTRUM_WRITE"));
        }
        return result.toString();
    }

    public int getReadLength() {
        return selectedRead.length;
    }

    public int getWriteLength() {
        return selectedWrite.length;
    }

    public int getViewSpectrumType() {
        return viewSpectrumType;
    }

    public void setViewSpectrumType(int viewSpectrumType) {
        this.viewSpectrumType = viewSpectrumType;
    }

    public Collection<Integer> getSelectedRead() {
        return getSelected(selectedRead);
    }

    public Collection<Integer> getSelectedWrite() {
        return getSelected(selectedWrite);
    }

    private Collection<Integer> getSelected(boolean[] ref) {
        Collection<Integer> result = new ArrayList<Integer>();
        if (ref != null && ref.length > 0) {
            result = new ArrayList<Integer>();
            int i = 0;
            for (boolean toTest : ref) {
                if (toTest) {
                    result.add(i);
                }
                i++;
            }
        }
        return result;
    }

}
