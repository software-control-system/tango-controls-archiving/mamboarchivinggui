package fr.soleil.mambo.models;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.table.DefaultTableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;
import fr.soleil.mambo.tools.Messages;

public class ACRecapTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -6209151662228853834L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACRecapTableModel.class);

    public static final int TITLE_OR_GENERAL = -1;
    public static final int MODE_P = 0;
    public static final int MODE_A = 1;
    public static final int MODE_R = 2;
    public static final int MODE_T = 3;
    public static final int MODE_D = 4;
    public static final int MODE_C = 5;
    public static final int MODE_E = 6;

    private int mode;
    private Boolean historic;
    private ArchivingConfigurationAttribute attribute;
    private ArchivingConfigurationAttributeDBProperties DBProperties;

    // --CLA
    private Map<String, Mode> TDBmodesBuffer;
    private Map<String, Mode> HDBmodesBuffer;
    private Map<String, Mode> TTSmodesBuffer;

    // --CLA

    public ACRecapTableModel() {
        this(false);
    }

    public ACRecapTableModel(final Boolean historic) {
        super();

        mode = TITLE_OR_GENERAL;
        this.historic = historic;
        attribute = null;
        DBProperties = null;

        HDBmodesBuffer = new ConcurrentHashMap<>();
        TDBmodesBuffer = new ConcurrentHashMap<>();
        TTSmodesBuffer = new ConcurrentHashMap<>();
    }

    public void load(final ArchivingConfigurationAttribute attribute, final Boolean historic, final int mode) {
        this.attribute = attribute;
        this.historic = historic;
        this.mode = mode;
        if (attribute != null) {
            final ArchivingConfigurationAttributeProperties attrProperties = attribute.getProperties();
            if (historic == null) {
                DBProperties = attrProperties.getTTSProperties();
            } else if (historic.booleanValue()) {
                DBProperties = attrProperties.getHDBProperties();
            } else {
                DBProperties = attrProperties.getTDBProperties();
            }
        }
        // fireTableStructureChanged();
        fireTableDataChanged();
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return 5;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getRowCount()
     */

    @Override
    public int getRowCount() {
        Mode mode = null;

        if (HDBmodesBuffer == null) {
            HDBmodesBuffer = new ConcurrentHashMap<>();
        }

        if (TDBmodesBuffer == null) {
            TDBmodesBuffer = new ConcurrentHashMap<>();
        }

        if (TTSmodesBuffer == null) {
            TTSmodesBuffer = new ConcurrentHashMap<>();
        }
        final Map<String, Mode> modesBuffer;
        if (historic == null) {
            modesBuffer = TTSmodesBuffer;
        } else if (historic.booleanValue()) {
            modesBuffer = HDBmodesBuffer;
        } else {
            modesBuffer = TDBmodesBuffer;
        }

        String completeName = null;
        if (attribute != null) {
            completeName = attribute.getCompleteName();
        }

        if (completeName != null) {
            mode = modesBuffer.get(completeName);

            if (mode == null && completeName != null) {
                try {
                    mode = ArchivingManagerFactory.getCurrentImpl().getArchivingMode(completeName, historic);
                } catch (final Exception e) {
                    LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                    return 0;
                }

                if (completeName != null && mode != null) {
                    modesBuffer.put(completeName, mode);
                }
            }
        }
        // -CLA

        return getRowCount2(mode);
    }

    public int getRowCount2(final Mode mode) {
        switch (this.mode) {
            case MODE_P:
                if (DBProperties != null && DBProperties.getMode(ArchivingConfigurationMode.TYPE_P) != null) {
                    return 1;
                } else {
                    if (mode != null && mode.getModeP() != null) {
                        return 1;
                    }
                    return 0;
                }
            case MODE_A:
                if (DBProperties != null && DBProperties.getMode(ArchivingConfigurationMode.TYPE_A) != null) {
                    return 4;
                } else {
                    if (mode != null && mode.getModeA() != null) {
                        return 4;
                    }
                    return 0;
                }
            case MODE_R:
                if (DBProperties != null && DBProperties.getMode(ArchivingConfigurationMode.TYPE_R) != null) {
                    return 4;
                } else {
                    if (mode != null && mode.getModeR() != null) {
                        return 4;
                    }
                    return 0;
                }
            case MODE_T:
                if (DBProperties != null && DBProperties.getMode(ArchivingConfigurationMode.TYPE_T) != null) {
                    return 3;
                } else {
                    if (mode != null && mode.getModeT() != null) {
                        return 3;
                    }
                    return 0;
                }
            case MODE_D:
                if (DBProperties != null && DBProperties.getMode(ArchivingConfigurationMode.TYPE_D) != null) {
                    return 1;
                } else {
                    if (mode != null && mode.getModeD() != null) {
                        return 1;
                    }
                    return 0;
                }
            case MODE_C:
                if (DBProperties != null && DBProperties.getMode(ArchivingConfigurationMode.TYPE_C) != null) {
                    return 3;
                } else {
                    if (mode != null && mode.getModeC() != null) {
                        return 3;
                    }
                    return 0;
                }
            case MODE_E:
                if (DBProperties != null && DBProperties.getMode(ArchivingConfigurationMode.TYPE_E) != null) {
                    return 1;
                } else {
                    if (mode != null && mode.getEventMode() != null) {
                        return 1;
                    }
                    return 0;
                }
            case TITLE_OR_GENERAL:
                if (attribute == null) {
                    return 1;
                }
                if (DBProperties != null) {
                    if (DBProperties.getModes().length > 0) {
                        return 1;
                    } else {
                        if (mode != null && mode.getTdbSpec() != null) {
                            return 1;
                        }
                    }
                }
                return 0;
            default:
                return 0;
        } // end switch (this.mode)
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        switch (columnIndex) {
            case 0:
                return getFirstColumnValue(rowIndex);
            case 1:
                return getSecondColumnValue(rowIndex);
            case 2:
                return getThirdColumnValue(rowIndex);
            case 3:
                return getACColumnValue(rowIndex);
            case 4:
                return getDBColumnValue(rowIndex);
            default:
                return null;
        }
    }

    private Object getThirdColumnValue(final int rowIndex) {
        if (attribute == null) {
            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TITLE_TABLE_CARACTERISTIC");
        } else {
            switch (rowIndex) {
                case 0:
                    switch (mode) {
                        case MODE_P:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEP_PERIOD");
                        case MODE_A:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_PERIOD");
                        case MODE_R:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_PERIOD");
                        case MODE_T:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODET_PERIOD");
                        case MODE_D:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODED_PERIOD");
                        case MODE_C:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEC_PERIOD");
                        case MODE_E:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEE_ARCHIVED");
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                case 1:
                    switch (mode) {
                        case MODE_A:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_UPPER");
                        case MODE_R:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_UPPER");
                        case MODE_T:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODET_UPPER");
                        case MODE_C:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEC_RANGE");
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                case 2:
                    switch (mode) {
                        case MODE_A:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_LOWER");
                        case MODE_R:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_LOWER");
                        case MODE_T:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODET_LOWER");
                        case MODE_C:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEC_TYPE");
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                case 3:
                    switch (mode) {
                        case MODE_A:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_SLOW_DRIFT");
                        case MODE_R:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_SLOW_DRIFT");
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                default:
                    return ObjectUtils.EMPTY_STRING;
            } // end switch (rowIndex)
        } // end if (attribute == null) ... else
    } // end getThirdColumnValue(int rowIndex)

    private Object getDBColumnValue(final int rowIndex) {
        if (attribute == null) {
            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TITLE_TABLE_DB");
        } else {
            final IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
            Mode mode;
            try {
                mode = manager.getArchivingMode(attribute.getCompleteName(), historic);
            } catch (final Exception e) {
                mode = null;
            }
            if (mode == null) {
                return ObjectUtils.EMPTY_STRING;
            }
            switch (rowIndex) {
                case 0:
                    switch (this.mode) {
                        case MODE_P:
                            if (mode.getModeP() != null) {
                                if (historic == null) {
                                    return mode.getModeP().getPeriod() + "ms";
                                } else if (historic.booleanValue()) {
                                    return mode.getModeP().getPeriod() / 1000 + "s";
                                }
                                return mode.getModeP().getPeriod() + "ms";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_A:
                            if (mode.getModeA() != null) {
                                if (historic == null) {
                                    return mode.getModeA().getPeriod() + "ms";
                                } else if (historic.booleanValue()) {
                                    return mode.getModeA().getPeriod() / 1000 + "s";
                                }
                                return mode.getModeA().getPeriod() + "ms";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_R:
                            if (mode.getModeR() != null) {
                                if (historic == null) {
                                    return mode.getModeR().getPeriod() + "ms";
                                } else if (historic.booleanValue()) {
                                    return mode.getModeR().getPeriod() / 1000 + "s";
                                }
                                return mode.getModeR().getPeriod() + "ms";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_T:
                            if (mode.getModeT() != null) {
                                if (historic == null) {
                                    return mode.getModeT().getPeriod() + "ms";
                                } else if (historic.booleanValue()) {
                                    return mode.getModeT().getPeriod() / 1000 + "s";
                                }
                                return mode.getModeT().getPeriod() + "ms";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_D:
                            if (mode.getModeD() != null) {
                                if (historic == null) {
                                    return mode.getModeD().getPeriod() + "ms";
                                } else if (historic.booleanValue()) {
                                    return mode.getModeD().getPeriod() / 1000 + "s";
                                }
                                return mode.getModeD().getPeriod() + "ms";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_C:
                            if (mode.getModeC() != null) {
                                if (historic == null) {
                                    return mode.getModeC().getPeriod() + "ms";
                                } else if (historic.booleanValue()) {
                                    return mode.getModeC().getPeriod() / 1000 + "s";
                                }
                                return mode.getModeC().getPeriod() + "ms";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_E:
                            if (mode.getEventMode() != null) {
                                return new Boolean(true);
                            }
                            return new Boolean(false);
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                case 1:
                    switch (this.mode) {
                        case MODE_A:
                            if (mode.getModeA() != null) {
                                return mode.getModeA().getValSup() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_R:
                            if (mode.getModeR() != null) {
                                return mode.getModeR().getPercentSup() + "%";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_T:
                            if (mode.getModeT() != null) {
                                return mode.getModeT().getThresholdSup() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_C:
                            if (mode.getModeC() != null) {
                                return mode.getModeC().getRange() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                case 2:
                    switch (this.mode) {
                        case MODE_A:
                            if (mode.getModeA() != null) {
                                return mode.getModeA().getValInf() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_R:
                            if (mode.getModeR() != null) {
                                return mode.getModeR().getPercentInf() + "%";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_T:
                            if (mode.getModeT() != null) {
                                return mode.getModeT().getThresholdInf() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_C:
                            if (mode.getModeC() != null) {
                                return mode.getModeC().getTypeCalcul() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                case 3:
                    switch (this.mode) {
                        case MODE_A:
                            if (mode.getModeA() != null) {
                                return Boolean.toString(mode.getModeA().isSlow_drift()) + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_R:
                            if (mode.getModeR() != null) {
                                return Boolean.toString(mode.getModeR().isSlow_drift()) + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }

                default:
                    return ObjectUtils.EMPTY_STRING;
            } // end switch (rowIndex)
        } // end if (attribute == null) ... else
    } // end getDBColumnValue(int rowIndex)

    private Object getACColumnValue(final int rowIndex) {
        if (attribute == null) {
            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TITLE_TABLE_AC");
        } else {
            if (DBProperties == null) {
                return ObjectUtils.EMPTY_STRING;
            }
            ArchivingConfigurationMode[] modes = null;
            modes = DBProperties.getModes();
            if (modes == null) {
                modes = new ArchivingConfigurationMode[0];
            }
            switch (rowIndex) {
                case 0:
                    switch (this.mode) {
                        case MODE_P:
                            final ArchivingConfigurationMode ACPMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_P);
                            if (ACPMode != null) {
                                if (DBProperties instanceof ArchivingConfigurationAttributeTDBProperties) {
                                    return ACPMode.getMode().getModeP().getPeriod() + "ms";
                                }
                                return ACPMode.getMode().getModeP().getPeriod() / 1000 + "s";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_A:
                            final ArchivingConfigurationMode ACAMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_A);
                            if (ACAMode != null) {
                                if (DBProperties instanceof ArchivingConfigurationAttributeTDBProperties) {
                                    return ACAMode.getMode().getModeA().getPeriod() + "ms";
                                }
                                return ACAMode.getMode().getModeA().getPeriod() / 1000 + "s";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_R:
                            final ArchivingConfigurationMode ACRMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_R);
                            if (ACRMode != null) {
                                if (DBProperties instanceof ArchivingConfigurationAttributeTDBProperties) {
                                    return ACRMode.getMode().getModeR().getPeriod() + "ms";
                                }
                                return ACRMode.getMode().getModeR().getPeriod() / 1000 + "s";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_T:
                            final ArchivingConfigurationMode ACTMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_T);
                            if (ACTMode != null) {
                                if (DBProperties instanceof ArchivingConfigurationAttributeTDBProperties) {
                                    return ACTMode.getMode().getModeT().getPeriod() + "ms";
                                }
                                return ACTMode.getMode().getModeT().getPeriod() / 1000 + "s";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_D:
                            final ArchivingConfigurationMode ACDMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_D);
                            if (ACDMode != null) {
                                if (DBProperties instanceof ArchivingConfigurationAttributeTDBProperties) {
                                    return ACDMode.getMode().getModeD().getPeriod() + "ms";
                                }
                                return ACDMode.getMode().getModeD().getPeriod() / 1000 + "s";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_C:
                            final ArchivingConfigurationMode ACCMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_C);
                            if (ACCMode != null) {
                                if (DBProperties instanceof ArchivingConfigurationAttributeTDBProperties) {
                                    return ACCMode.getMode().getModeC().getPeriod() + "ms";
                                }
                                return ACCMode.getMode().getModeC().getPeriod() / 1000 + "s";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_E:
                            final ArchivingConfigurationMode ACEMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_E);
                            if (ACEMode != null) {
                                return new Boolean(true);
                            }
                            return new Boolean(false);
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                case 1:
                    switch (this.mode) {
                        case MODE_A:
                            final ArchivingConfigurationMode ACAMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_A);
                            if (ACAMode != null) {
                                return ACAMode.getMode().getModeA().getValSup() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_R:
                            final ArchivingConfigurationMode ACRMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_R);
                            if (ACRMode != null) {
                                return ACRMode.getMode().getModeR().getPercentSup() + "%";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_T:
                            final ArchivingConfigurationMode ACTMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_T);
                            if (ACTMode != null) {
                                return ACTMode.getMode().getModeT().getThresholdSup() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_C:
                            final ArchivingConfigurationMode ACCMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_C);
                            if (ACCMode != null) {
                                return ACCMode.getMode().getModeC().getRange() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                case 2:
                    switch (this.mode) {
                        case MODE_A:
                            final ArchivingConfigurationMode ACAMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_A);
                            if (ACAMode != null) {
                                return ACAMode.getMode().getModeA().getValInf() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_R:
                            final ArchivingConfigurationMode ACRMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_R);
                            if (ACRMode != null) {
                                return ACRMode.getMode().getModeR().getPercentInf() + "%";
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_T:
                            final ArchivingConfigurationMode ACTMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_T);
                            if (ACTMode != null) {
                                return ACTMode.getMode().getModeT().getThresholdInf() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_C:
                            final ArchivingConfigurationMode ACCMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_C);
                            if (ACCMode != null) {
                                return ACCMode.getMode().getModeC().getTypeCalcul() + ObjectUtils.EMPTY_STRING;
                            }
                            return ObjectUtils.EMPTY_STRING;
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                case 3:
                    switch (this.mode) {
                        case MODE_A:
                            final ArchivingConfigurationMode ACAMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_A);
                            if (ACAMode != null) {
                                return Boolean.toString(ACAMode.getMode().getModeA().isSlow_drift());
                            }
                            return ObjectUtils.EMPTY_STRING;
                        case MODE_R:
                            final ArchivingConfigurationMode ACRMode = DBProperties
                                    .getMode(ArchivingConfigurationMode.TYPE_R);
                            if (ACRMode != null) {
                                return Boolean.toString(ACRMode.getMode().getModeR().isSlow_drift());
                            }
                            return ObjectUtils.EMPTY_STRING;
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }

                default:
                    return ObjectUtils.EMPTY_STRING;
            } // end switch (rowIndex)
        } // end if (attribute == null) ... else
    } // end getACColumnValue(int rowIndex)

    private Object getSecondColumnValue(final int rowIndex) {
        if (attribute == null) {
            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TITLE_TABLE_MODE");
        } else {
            switch (rowIndex) {
                case 0:
                    switch (this.mode) {
                        case MODE_P:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEP_NAME");
                        case MODE_A:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_NAME");
                        case MODE_R:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_NAME");
                        case MODE_T:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODET_NAME");
                        case MODE_D:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODED_NAME");
                        case MODE_C:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEC_NAME");
                        case MODE_E:
                            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEE_NAME");
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                default:
                    return ObjectUtils.EMPTY_STRING;
            }
        }
    }

    private Object getFirstColumnValue(final int rowIndex) {
        if (attribute == null) {
            return Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_TITLE_TABLE_NAME");
        } else {
            switch (rowIndex) {
                case 0:
                    switch (mode) {
                        case TITLE_OR_GENERAL:
                            return attribute.getCompleteName();
                        default:
                            return ObjectUtils.EMPTY_STRING;
                    }
                default:
                    return ObjectUtils.EMPTY_STRING;
            }
        }
    }

}
