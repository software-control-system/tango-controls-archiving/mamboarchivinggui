package fr.soleil.mambo.models;

import java.text.Collator;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import fr.soleil.mambo.data.view.ExpressionAttribute;
import fr.soleil.mambo.tools.Messages;

public class ExpressionTreeModel extends DefaultTreeModel {

    private static final long serialVersionUID = -5198087716791647528L;

    protected Map<String, ExpressionAttribute> expressionAttributes;
    protected DefaultMutableTreeNode rootNode;

    public ExpressionTreeModel() {
        super(new DefaultMutableTreeNode(Messages.getMessage("EXPRESSION_ATTRIBUTES")));
        rootNode = (DefaultMutableTreeNode) this.getRoot();
        setRoot(rootNode);
        expressionAttributes = new TreeMap<String, ExpressionAttribute>(Collator.getInstance());
    }

    public boolean addAttribute(ExpressionAttribute attribute) {
        if (expressionAttributes.containsKey(attribute.getName())) {
            return false;
        } else {
            expressionAttributes.put(attribute.getName(), attribute);
            this.build();
            return true;
        }
    }

    public ExpressionAttribute removeAttribute(String attributeName) {
        DefaultMutableTreeNode node = null;
        for (int i = 0; i < rootNode.getChildCount(); i++) {
            if (attributeName.equals(((DefaultMutableTreeNode) rootNode.getChildAt(i)).getUserObject())) {
                node = (DefaultMutableTreeNode) rootNode.getChildAt(i);
                break;
            }
        }
        if (node != null) {
            removeNodeFromParent(node);
            reload();
        }
        return expressionAttributes.remove(attributeName);
    }

    public boolean updateAttribute(String formerName, ExpressionAttribute attribute) {
        boolean updated;
        if (expressionAttributes.containsKey(formerName)) {
            if (attribute.getName().equals(formerName) || !expressionAttributes.containsKey(attribute.getName())) {
                removeAttribute(formerName);
                updated = addAttribute(attribute);
            } else {
                updated = false;
            }
        } else {
            updated = false;
        }
        return updated;
    }

    public Map<String, ExpressionAttribute> getExpressionAttributes() {
        return expressionAttributes;
    }

    public void setExpressionAttributes(TreeMap<String, ExpressionAttribute> expressionAttributes) {
        this.expressionAttributes = expressionAttributes;
        this.build();
    }

    public ExpressionAttribute getExpressionAttribute(String attributeName) {
        return expressionAttributes.get(attributeName);
    }

    protected void build() {
        rootNode.removeAllChildren();
        int index = 0;
        for (String key : expressionAttributes.keySet()) {
            insertNodeInto(new DefaultMutableTreeNode(key), rootNode, index++);
        }
        reload();
    }
}
