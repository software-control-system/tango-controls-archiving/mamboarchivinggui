package fr.soleil.mambo.models;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.swing.table.DefaultTableModel;

import org.slf4j.Logger;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.tools.Messages;

/**
 * A {@link DefaultTableModel} that describes an attribute archiving mode.
 * 
 * @author GIRARDOT
 */
public abstract class AACAttributeDetailsTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -8057619059631763513L;

    protected static final String CLOSE_PAR_SEPARATION = "): ";
    protected static final String MILLISECONDS = "ms";
    protected static final String SECONDS = "s";

    protected final Boolean historic;
    protected final String title;

    public AACAttributeDetailsTableModel(Boolean historic) {
        super();
        this.historic = historic;
        this.title = Messages.getMessage(getTitleKey());
    }

    protected static <T extends AACAttributeDetailsTableModel> T checkInstance(T instance, Class<T> modelClass,
            Boolean historic) {
        T model = instance;
        if (model == null) {
            Constructor<T> constructor;
            try {
                constructor = modelClass.getDeclaredConstructor(Boolean.class);
                if (constructor == null) {
                    model = null;
                } else {
                    model = constructor.newInstance(historic);
                }
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException e) {
                model = null;
            }
        }
        return model;
    }

    @Override
    public abstract int getRowCount();

    @Override
    public final int getColumnCount() {
        return 4;
    }

    @Override
    public final Object getValueAt(int rowIndex, int columnIndex) {
        Object value;
        try {
            switch (columnIndex) {
                case 0:
                    value = getFirstColumnValue(rowIndex);
                    break;
                case 1:
                    value = getSecondColumnValue(rowIndex);
                    break;
                case 2:
                    value = getACColumnValue(rowIndex);
                    break;
                case 3:
                    value = getDBColumnValue(rowIndex);
                    break;
                default:
                    value = null;
                    break;
            }
        } catch (Exception e) {
            value = null;
            getLogger().error(getClass().getSimpleName() + CometeConstants.OPEN_PAR + historic + CLOSE_PAR_SEPARATION
                    + TangoExceptionHelper.getErrorMessage(e), e);
        }
        return value;
    }

    @Override
    public final void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        // not supported
    }

    protected final ArchivingConfigurationAttributeDBProperties getDBProperties(
            ArchivingConfigurationAttributeProperties attrProperties) {
        ArchivingConfigurationAttributeDBProperties dbProperties;
        if (attrProperties == null) {
            dbProperties = null;
        } else if (historic == null) {
            dbProperties = attrProperties.getTTSProperties();
        } else if (historic.booleanValue()) {
            dbProperties = attrProperties.getHDBProperties();
        } else {
            dbProperties = attrProperties.getTDBProperties();
        }
        return dbProperties;
    }

    public void load(ArchivingConfigurationAttribute attribute) {
        fireTableDataChanged();
    }

    protected String getPeriodAsString(int period) {
        String str;
        if ((historic == null) || !historic.booleanValue()) {
            str = period + MILLISECONDS;
        } else {
            str = (period / 1000) + SECONDS;
        }
        return str;
    }

    protected Object getDefaultValue() {
        return ObjectUtils.EMPTY_STRING;
    }

    protected String getFirstColumnValue(int rowIndex) {
        String ret;
        if (rowIndex == 0) {
            ret = title;
        } else {
            ret = ObjectUtils.EMPTY_STRING;
        }
        return ret;
    }

    protected abstract String getSecondColumnValue(int rowIndex);

    protected abstract Object getACColumnValue(int rowIndex);

    protected abstract Object getDBColumnValue(int rowIndex) throws ArchivingException;

    protected abstract Logger getLogger();

    protected abstract String getTitleKey();

    public abstract boolean toPaint();

}
