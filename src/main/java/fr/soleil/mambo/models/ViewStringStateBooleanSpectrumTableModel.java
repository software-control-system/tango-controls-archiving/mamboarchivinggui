package fr.soleil.mambo.models;

import java.lang.reflect.Array;
import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.swing.table.DefaultTableModel;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.mambo.tools.Messages;

public class ViewStringStateBooleanSpectrumTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 522103456937181071L;

    private DbData data;
    private final SimpleDateFormat genFormatUS;
    private int dataType;

    public ViewStringStateBooleanSpectrumTableModel() {
        super();
        this.genFormatUS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        data = null;
    }

    public void setData(DbData data, int dataType) {
        this.dataType = dataType;
        this.data = data;
        fireTableStructureChanged();
    }

    @Override
    public int getColumnCount() {
        int col = 0;
        DbData data = this.data;
        if (data != null) {
            if (data.getMaxX() > col) {
                col = data.getMaxX() + 1;
            }
        }
        return col;
    }

    @Override
    public int getRowCount() {
        int length = 0;
        DbData data = this.data;
        if ((data != null) && (data.getTimedData() != null)) {
            length = data.getTimedData().length;
        }
        return length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        DbData data = this.data;
        Object value;
        if (columnIndex == 0) {
            if ((data != null) && (data.getTimedData() != null)) {
                value = genFormatUS.format(new Date(data.getTimedData()[rowIndex].getTime()));
            } else {
                value = null;
            }
        } else {
            if (data != null) {
                switch (dataType) {
                case TangoConst.Tango_DEV_STRING:
                case TangoConst.Tango_DEV_STATE:
                case TangoConst.Tango_DEV_BOOLEAN:
                    if ((data.getTimedData() != null) && (data.getTimedData()[rowIndex].getValue() != null)
                            && columnIndex - 1 < Array.getLength(data.getTimedData()[rowIndex].getValue())) {
                        value = Array.get(data.getTimedData()[rowIndex].getValue(), columnIndex - 1);
                    } else {
                        value = null;
                    }
                    break;
                default:
                    value = null;
                }
            } else {
                switch (dataType) {
                case TangoConst.Tango_DEV_STRING:
                case TangoConst.Tango_DEV_STATE:
                case TangoConst.Tango_DEV_BOOLEAN:
                default:
                    value = null;
                }
            }
        }
        return value;
    }

    @Override
    public String getColumnName(int column) {
        String name;
        if (column == 0) {
            name = Messages.getMessage("VIEW_SPECTRUM_DATE");
        } else {
            name = Integer.toString(column);
        }
        return name;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void clearData() {
        setData(null, -1);
    }

}
