package fr.soleil.mambo.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeModeATableModel extends AACAttributeModeTableModel<ModeAbsolu> {

    private static final long serialVersionUID = 8390522488068417053L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeModeATableModel.class);

    private static final String MODEA_NAME = "ARCHIVING_ATTRIBUTES_DETAIL_MODEA_NAME";
    private static final String MODEA_PERIOD = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_PERIOD");
    private static final String MODEA_UPPER = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_UPPER");
    private static final String MODEA_LOWER = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_LOWER");
    private static final String MODEA_SLOW_DRIFT = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_SLOW_DRIFT");

    private static ACAttributeModeATableModel hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeATableModel(Boolean historic) {
        super(historic);
    }

    public static ACAttributeModeATableModel getInstance(Boolean historic) {
        ACAttributeModeATableModel instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeATableModel.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeATableModel.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeATableModel.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

    @Override
    protected String getModeColumnValue(ModeAbsolu mode, int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;
        if (mode != null) {
            switch (rowIndex) {
                case 0:
                    ret = getPeriodAsString(mode.getPeriod());
                    break;
                case 1:
                    ret = Double.toString(mode.getValSup());
                    break;
                case 2:
                    ret = Double.toString(mode.getValInf());
                    break;
                case 3:
                    ret = Boolean.toString(mode.isSlow_drift());
                    break;
            }
        }
        return ret;
    }

    @Override
    protected int getModeType() {
        return ArchivingConfigurationMode.TYPE_A;
    }

    @Override
    protected ModeAbsolu getSubMode(Mode mode) {
        return mode == null ? null : mode.getModeA();
    }

    @Override
    public int getRowCount() {
        return 4;
    }

    @Override
    protected String getSecondColumnValue(int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;
        switch (rowIndex) {
            case 0:
                ret = MODEA_PERIOD;
                break;
            case 1:
                ret = MODEA_UPPER;
                break;
            case 2:
                ret = MODEA_LOWER;
                break;
            case 3:
                ret = MODEA_SLOW_DRIFT;
                break;
        }
        return ret;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getTitleKey() {
        return MODEA_NAME;
    }

}
