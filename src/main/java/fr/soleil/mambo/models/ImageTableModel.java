//+======================================================================
// $Source$
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ImageTableModel.
//                      (GIRARDOT Rapha�l) - 10 juil. 2006
//
// $Author$
//
// $Revision: 
//
// copyleft :   Synchrotron SOLEIL
//                  L'Orme des Merisiers
//                  Saint-Aubin - BP 48
//                  91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.mambo.models;

import javax.swing.table.DefaultTableModel;

import fr.esrf.TangoDs.TangoConst;

/**
 * @author GIRARDOT
 * 
 */
public class ImageTableModel extends DefaultTableModel {

    private static final long serialVersionUID = 1453772645150344156L;

    protected Object value;
    protected int data_type;

    public ImageTableModel(Object value, int data_type) {
        super();
        this.value = value;
        this.data_type = data_type;
    }

    @Override
    public int getColumnCount() {
        if (value != null) {
            switch (data_type) {
                case TangoConst.Tango_DEV_CHAR:
                    if (((byte[][]) value).length != 0 && ((byte[][]) value)[0].length != 0) {
                        return ((byte[][]) value).length;
                    }
                    break;
                case TangoConst.Tango_DEV_UCHAR:
                case TangoConst.Tango_DEV_SHORT:
                    if (((short[][]) value).length != 0 && ((short[][]) value)[0].length != 0) {
                        return ((short[][]) value).length;
                    }
                    break;
                case TangoConst.Tango_DEV_USHORT:
                case TangoConst.Tango_DEV_LONG:
                    if (((int[][]) value).length != 0 && ((int[][]) value)[0].length != 0) {
                        return ((int[][]) value).length;
                    }
                    break;
                case TangoConst.Tango_DEV_ULONG:
                case TangoConst.Tango_DEV_LONG64:
                case TangoConst.Tango_DEV_ULONG64:
                    if (((long[][]) value).length != 0 && ((long[][]) value)[0].length != 0) {
                        return ((long[][]) value).length;
                    }
                    break;
                case TangoConst.Tango_DEV_FLOAT:
                    if (((float[][]) value).length != 0 && ((float[][]) value)[0].length != 0) {
                        return ((float[][]) value).length;
                    }
                    break;
                case TangoConst.Tango_DEV_DOUBLE:
                    if (((double[][]) value).length != 0 && ((double[][]) value)[0].length != 0) {
                        return ((double[][]) value).length;
                    }
                    break;
                default: // nothing to do
            }
        }
        return 0;
    }

    @Override
    public int getRowCount() {
        if (value != null) {
            switch (data_type) {
                case TangoConst.Tango_DEV_CHAR:
                case TangoConst.Tango_DEV_UCHAR:
                    if (((byte[][]) value).length != 0 && ((byte[][]) value)[0].length != 0) {
                        return ((byte[][]) value)[0].length;
                    }
                    break;
                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                    if (((short[][]) value).length != 0 && ((short[][]) value)[0].length != 0) {
                        return ((short[][]) value)[0].length;
                    }
                    break;
                case TangoConst.Tango_DEV_LONG:
                case TangoConst.Tango_DEV_ULONG:
                    if (((int[][]) value).length != 0 && ((int[][]) value)[0].length != 0) {
                        return ((int[][]) value)[0].length;
                    }
                    break;
                case TangoConst.Tango_DEV_FLOAT:
                    if (((float[][]) value).length != 0 && ((float[][]) value)[0].length != 0) {
                        return ((float[][]) value)[0].length;
                    }
                    break;
                case TangoConst.Tango_DEV_DOUBLE:
                    if (((double[][]) value).length != 0 && ((double[][]) value)[0].length != 0) {
                        return ((double[][]) value)[0].length;
                    }
                    break;
                default: // nothing to do
            }
        }
        return 0;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return Integer.toString(columnIndex + 1);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (value != null) {
            switch (data_type) {
                case TangoConst.Tango_DEV_CHAR:
                case TangoConst.Tango_DEV_UCHAR:
                    return new Byte(((byte[][]) value)[columnIndex][rowIndex]);
                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                    return new Short(((short[][]) value)[columnIndex][rowIndex]);
                case TangoConst.Tango_DEV_LONG:
                case TangoConst.Tango_DEV_ULONG:
                    return new Integer(((int[][]) value)[columnIndex][rowIndex]);
                case TangoConst.Tango_DEV_FLOAT:
                    return new Float(((float[][]) value)[columnIndex][rowIndex]);
                case TangoConst.Tango_DEV_DOUBLE:
                    return new Double(((double[][]) value)[columnIndex][rowIndex]);
                default: // nothing to do
            }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

}
