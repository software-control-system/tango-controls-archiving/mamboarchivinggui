package fr.soleil.mambo.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeModeTTableModel extends AACAttributeModeTableModel<ModeSeuil> {

    private static final long serialVersionUID = 3186213912634736993L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeModeTTableModel.class);

    private static final String MODET_NAME = "ARCHIVING_ATTRIBUTES_DETAIL_MODET_NAME";
    private static final String MODET_PERIOD = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODET_PERIOD");
    private static final String MODET_UPPER = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODET_UPPER");
    private static final String MODET_LOWER = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODET_LOWER");

    private static ACAttributeModeTTableModel hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeTTableModel(Boolean historic) {
        super(historic);
    }

    public static ACAttributeModeTTableModel getInstance(Boolean historic) {
        ACAttributeModeTTableModel instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeTTableModel.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeTTableModel.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeTTableModel.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

    @Override
    protected Object getModeColumnValue(ModeSeuil mode, int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;
        if (mode != null) {
            switch (rowIndex) {
                case 0:
                    ret = getPeriodAsString(mode.getPeriod());
                    break;
                case 1:
                    ret = Double.toString(mode.getThresholdSup());
                    break;
                case 2:
                    ret = Double.toString(mode.getThresholdInf());
                    break;
            }
        }
        return ret;
    }

    @Override
    protected int getModeType() {
        return ArchivingConfigurationMode.TYPE_T;
    }

    @Override
    protected ModeSeuil getSubMode(Mode mode) {
        return mode == null ? null : mode.getModeT();
    }

    @Override
    public int getRowCount() {
        return 3;
    }

    @Override
    protected String getSecondColumnValue(int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;
        switch (rowIndex) {
            case 0:
                ret = MODET_PERIOD;
                break;
            case 1:
                ret = MODET_UPPER;
                break;
            case 2:
                ret = MODET_LOWER;
                break;
        }
        return ret;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getTitleKey() {
        return MODET_NAME;
    }

}
