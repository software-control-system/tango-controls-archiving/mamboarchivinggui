package fr.soleil.mambo.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeModeRTableModel extends AACAttributeModeTableModel<ModeRelatif> {

    private static final long serialVersionUID = -6119772902734197075L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeModeRTableModel.class);

    private static final String PERCENT = "%";
    private static final String MODER_NAME = "ARCHIVING_ATTRIBUTES_DETAIL_MODER_NAME";
    private static final String MODER_PERIOD = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_PERIOD");
    private static final String MODER_UPPER = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_UPPER");
    private static final String MODER_LOWER = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_LOWER");
    private static final String MODER_SLOW_DRIFT = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_SLOW_DRIFT");

    private static ACAttributeModeRTableModel hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeRTableModel(Boolean historic) {
        super(historic);
    }

    public static ACAttributeModeRTableModel getInstance(Boolean historic) {
        ACAttributeModeRTableModel instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeRTableModel.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeRTableModel.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeRTableModel.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

    @Override
    protected String getModeColumnValue(ModeRelatif mode, int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;
        if (mode != null) {
            switch (rowIndex) {
                case 0:
                    ret = getPeriodAsString(mode.getPeriod());
                    break;
                case 1:
                    ret = mode.getPercentSup() + PERCENT;
                    break;
                case 2:
                    ret = mode.getPercentInf() + PERCENT;
                    break;
                case 3:
                    ret = Boolean.toString(mode.isSlow_drift());
                    break;

            }
        }
        return ret;
    }

    @Override
    protected int getModeType() {
        return ArchivingConfigurationMode.TYPE_R;
    }

    @Override
    protected ModeRelatif getSubMode(Mode mode) {
        return mode == null ? null : mode.getModeR();
    }

    @Override
    public int getRowCount() {
        return 4;
    }

    @Override
    protected String getSecondColumnValue(int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;
        switch (rowIndex) {
            case 0:
                ret = MODER_PERIOD;
                break;
            case 1:
                ret = MODER_UPPER;
                break;
            case 2:
                ret = MODER_LOWER;
                break;
            case 3:
                ret = MODER_SLOW_DRIFT;
                break;
        }
        return ret;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getTitleKey() {
        return MODER_NAME;
    }

}
