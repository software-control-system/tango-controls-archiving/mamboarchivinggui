package fr.soleil.mambo.models;

import javax.swing.table.DefaultTableModel;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRoot;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;

/**
 * A {@link DefaultTableModel} that describes an attribute archiving mode.
 * 
 * @author GIRARDOT
 */
public abstract class AACAttributeModeTableModel<M extends ModeRoot> extends AACAttributeDetailsTableModel {

    private static final long serialVersionUID = -2612279659225605131L;

    protected ArchivingConfigurationAttribute attribute;
    protected ArchivingConfigurationAttributeDBProperties dbProperties;

    public AACAttributeModeTableModel(Boolean historic) {
        super(historic);
    }

    @Override
    public void load(ArchivingConfigurationAttribute attribute) {
        this.attribute = attribute;
        ArchivingConfigurationAttributeProperties attrProperties = attribute.getProperties();
        dbProperties = getDBProperties(attrProperties);
        fireTableDataChanged();
    }

    protected M getModeFromProperties() {
        M mode;
        if (dbProperties == null) {
            mode = null;
        } else {
            ArchivingConfigurationMode acMode = dbProperties.getMode(getModeType());
            if (acMode == null) {
                mode = null;
            } else {
                mode = getSubMode(acMode.getMode());
            }
        }
        return mode;
    }

    @Override
    protected Object getDefaultValue() {
        return ObjectUtils.EMPTY_STRING;
    }

    @Override
    protected final Object getDBColumnValue(int rowIndex) throws ArchivingException {
        Object ret = getDefaultValue();
        IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
        M mode = manager == null ? null : getSubMode(manager.getArchivingMode(attribute.getCompleteName(), historic));
        if (mode != null) {
            ret = getModeColumnValue(mode, rowIndex);
        }
        return ret;
    }

    @Override
    protected Object getACColumnValue(int rowIndex) {
        Object ret = getDefaultValue();
        M mode = getModeFromProperties();
        if (mode != null) {
            ret = getModeColumnValue(mode, rowIndex);
        }
        return ret;
    }

    protected abstract Object getModeColumnValue(M mode, int rowIndex);

    protected abstract int getModeType();

    protected abstract M getSubMode(Mode mode);

    @Override
    public boolean toPaint() {
        boolean toPaint;
        if (dbProperties == null) {
            toPaint = false;
        } else {
            if (dbProperties.getMode(getModeType()) != null) {
                toPaint = true;
            } else if (attribute != null && attribute.getCompleteName() != null) {
                try {
                    Mode mode = ArchivingManagerFactory.getCurrentImpl().getArchivingMode(attribute.getCompleteName(),
                            historic);
                    if ((mode != null) && (getSubMode(mode) != null)) {
                        toPaint = true;
                    } else {
                        toPaint = false;
                    }
                } catch (Exception e) {
                    toPaint = false;
                }
            } else {
                toPaint = false;
            }
        }
        return toPaint;
    }

}
