package fr.soleil.mambo.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Family;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.mambo.comparators.MamboToStringObjectComparator;
import fr.soleil.mambo.containers.archiving.ACAttributeModesPanel;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;

public class ACAttributesTreeModel extends ACTreeModel {

    private static final long serialVersionUID = 7513715003556210362L;

    private static ACAttributesTreeModel instance = null;

    /**
     * @param root
     */
    private ACAttributesTreeModel() {
        super();
    }

    public static ACAttributesTreeModel getInstance() {
        if (instance == null) {
            instance = new ACAttributesTreeModel();
        }

        return instance;
    }

    /**
     * @param validSelected
     *            27 juin 2005
     */
    // moved from daughter ACAttr
    public void addSelectedAttibutes(final List<TreePath> validSelected) {
        final Map<String, ArchivingConfigurationAttribute> htAttr = attributesHT;
        for (final TreePath aPath : validSelected) {
            // START CURRENT SELECTED PATH
            DefaultMutableTreeNode currentTreeObject = null;
            for (int depth = 0; depth < CONTEXT_TREE_DEPTH; depth++) {
                final Object currentPathObject = aPath.getPathComponent(depth);

                if (depth == 0) {
                    currentTreeObject = (DefaultMutableTreeNode) getRoot();
                    continue;
                }

                final int numOfChildren = getChildCount(currentTreeObject);
                final List<String> vect = new ArrayList<>();
                for (int childIndex = 0; childIndex < numOfChildren; childIndex++) {
                    final Object childAt = getChild(currentTreeObject, childIndex);
                    vect.add(childAt.toString());
                }

                if (vect.contains(currentPathObject.toString())) {
                    final int pos = vect.indexOf(currentPathObject.toString());
                    currentTreeObject = (DefaultMutableTreeNode) getChild(currentTreeObject, pos);
                } else {
                    vect.add(currentPathObject.toString());
                    Collections.sort(vect, new MamboToStringObjectComparator());
                    final DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(currentPathObject.toString());
                    final int index = vect.indexOf(currentPathObject.toString());
                    insertNodeInto(newChild, currentTreeObject, index);

                    if (depth == CONTEXT_TREE_DEPTH - 1) {
                        final TreeNode[] path = newChild.getPath();

                        Attribute attribute = ACPossibleAttributesTreeModel.getInstance()
                                .getAttribute(translatePathIntoKey(path));
                        if (attribute == null) {
                            attribute = new Attribute(path);
                        }

                        final ArchivingConfigurationAttribute acattribute = new ArchivingConfigurationAttribute(
                                attribute);
                        htAttr.put(translatePathIntoKey(path), acattribute);
                    }

                    currentTreeObject = newChild;
                }
            }
            // END CURRENT SELECTED PATH
        }
    }

    /**
     * @param validSelected
     *            29 juin 2005
     */
    public void removeSelectedAttributes(final List<TreePath> validSelected) {
        for (final TreePath treePath : validSelected) {
            final Map<String, ArchivingConfigurationAttribute> htAttr = attributesHT;
            final String dom_s = treePath.getPathComponent(1).toString();
            final String fam_s = treePath.getPathComponent(2).toString();
            final String mem_s = treePath.getPathComponent(3).toString();
            final String att_s = treePath.getPathComponent(4).toString();
            final String path = dom_s + GUIUtilities.TANGO_DELIM + fam_s + GUIUtilities.TANGO_DELIM + mem_s
                    + GUIUtilities.TANGO_DELIM + att_s;
            htAttr.remove(path);
            removeFromDomains(path);
            final MutableTreeNode node = (MutableTreeNode) treePath.getLastPathComponent();
            try {
                removeNodeFromParent(node);
            } catch (final Exception e) {
                // do nothing
            }
        }
    }

    /**
     * @param validSelected
     *            29 juin 2005
     */
    public void removeSelectedAttributes2(final List<TreePath> validSelected) {
        for (final TreePath aPath : validSelected) {
            DefaultMutableTreeNode currentTreeObject = null;
            for (int depth = 0; depth < CONTEXT_TREE_DEPTH; depth++) {
                final String userObjectRef = (String) aPath.getPathComponent(depth);

                if (depth == 0) {
                    currentTreeObject = (DefaultMutableTreeNode) getRoot();
                    continue;
                }

                final int numOfChildren = getChildCount(currentTreeObject);
                for (int childIndex = 0; childIndex < numOfChildren; childIndex++) {
                    final DefaultMutableTreeNode childAt = (DefaultMutableTreeNode) getChild(currentTreeObject,
                            childIndex);
                    final String userObjectAt = (String) childAt.getUserObject();

                    if (userObjectRef.equals(userObjectAt)) {
                        currentTreeObject = childAt;
                        break;
                    }
                }
            }

            final TreeNode[] path = currentTreeObject.getPath();
            removeNodeFromParent(currentTreeObject);

            attributesHT.remove(translatePathIntoKey(path));
        }
    }

    @Override
    public void reload() {
        super.reload();
        ACAttributeModesPanel.getInstance(Boolean.TRUE).load(new ArchivingConfigurationAttribute());
        ACAttributeModesPanel.getInstance(Boolean.FALSE).load(new ArchivingConfigurationAttribute());
        ACAttributeModesPanel.getInstance(null).load(new ArchivingConfigurationAttribute());
    }

    @Override
    public void removeAll() {
        super.removeAll();
        ACAttributeModesPanel.getInstance(Boolean.TRUE).load(new ArchivingConfigurationAttribute());
        ACAttributeModesPanel.getInstance(Boolean.FALSE).load(new ArchivingConfigurationAttribute());
        ACAttributeModesPanel.getInstance(null).load(new ArchivingConfigurationAttribute());
    }

    public void match(final ITangoManager source, final String searchCriterions) {
        final List<Domain> domains = source.loadDomains(searchCriterions);
        applyDomains(domains);
    }

    public void applyDomains(final List<Domain> domains) {
        final Enumeration<?> enumeration = ((DefaultMutableTreeNode) root).preorderEnumeration();
        final List<TreePath> attributesToAdd = new ArrayList<>();
        while (enumeration.hasMoreElements()) {
            final DefaultMutableTreeNode currentTraversedNode = (DefaultMutableTreeNode) enumeration.nextElement();
            if (currentTraversedNode.getLevel() == AttributesTreeModel.CONTEXT_TREE_DEPTH - 1) {
                if (isWanted(domains, currentTraversedNode.getUserObjectPath())) {
                    final TreeNode[] obj = currentTraversedNode.getPath();
                    final TreePath path = new TreePath(obj);
                    attributesToAdd.add(path);
                }
            }
        }
        removeAll();
        addSelectedAttibutes(attributesToAdd);
    }

    /**
     * @param domains
     * @param currentPath
     * @return 25 août 2005
     */
    private boolean isWanted(final List<Domain> domains, final Object[] currentPath) {
        boolean wanted = false;
        final String domainName = (String) currentPath[1];
        final String familyName = (String) currentPath[2];
        final String memberName = (String) currentPath[3];
        final String attributeName = currentPath.length == 5 && (currentPath[4] != null) ? currentPath[4].toString()
                : null;

        for (final Domain domain : domains) {
            final String dName = domain.getName();
            if (!dName.equalsIgnoreCase(domainName)) {
                continue;
            }

            final Family family = domain.getFamily(familyName);
            if (family != null && family.getName().equalsIgnoreCase(familyName)) {
                final Member member = family.getMember(memberName);
                if (member != null && member.getName().equalsIgnoreCase(memberName)) {
                    if (attributeName == null) {
                        wanted = true;
                    } else {
                        wanted = (member.getAttribute(attributeName) != null);
                    }
                }
            }
            break;
        }

        return wanted;
    }

    public void setAttributes(final TreeMap<String, ArchivingConfigurationAttribute> in) {
        attributesHT = in;
    }

}
