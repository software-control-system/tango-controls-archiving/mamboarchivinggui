package fr.soleil.mambo.models;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;

public abstract class ACTreeModel extends AttributesTreeModel<ArchivingConfigurationAttribute> {

    private static final long serialVersionUID = -4880484716401816418L;

    public ACTreeModel() {
        super();
    }

    @Override
    public ArchivingConfigurationAttribute getValidAttributeFor(Attribute attr) {
        if (attr == null) {
            return null;
        } else if (attr instanceof ArchivingConfigurationAttribute) {
            return (ArchivingConfigurationAttribute) attr;
        } else {
            return new ArchivingConfigurationAttribute(attr);
        }
    }

}
