package fr.soleil.mambo.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;

public class ACAttributeModeCTableModel extends AACAttributeModeTableModel<ModeCalcul> {

    private static final long serialVersionUID = -490449314725755464L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributeModeCTableModel.class);

    private static final String MODEC_NAME = "ARCHIVING_ATTRIBUTES_DETAIL_MODEC_NAME";
    private static final String MODEC_PERIOD = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEC_PERIOD");
    private static final String MODEC_RANGE = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEC_RANGE");
    private static final String MODEC_TYPE = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEC_TYPE");

    private static ACAttributeModeCTableModel hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeCTableModel(Boolean historic) {
        super(historic);
    }

    public static ACAttributeModeCTableModel getInstance(Boolean historic) {
        ACAttributeModeCTableModel instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeCTableModel.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeCTableModel.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeCTableModel.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

    @Override
    protected String getModeColumnValue(ModeCalcul mode, int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;
        if (mode != null) {
            switch (rowIndex) {
                case 0:
                    ret = getPeriodAsString(mode.getPeriod());
                    break;
                case 1:
                    ret = Integer.toString(mode.getRange());
                    break;
                case 2:
                    ret = Integer.toString(mode.getTypeCalcul());
                    break;
            }

        }
        return ret;
    }

    @Override
    protected int getModeType() {
        return ArchivingConfigurationMode.TYPE_C;
    }

    @Override
    protected ModeCalcul getSubMode(Mode mode) {
        return mode == null ? null : mode.getModeC();
    }

    @Override
    public int getRowCount() {
        return 3;
    }

    @Override
    protected String getSecondColumnValue(int rowIndex) {
        String ret = ObjectUtils.EMPTY_STRING;
        switch (rowIndex) {
            case 0:
                ret = MODEC_PERIOD;
                break;
            case 1:
                ret = MODEC_RANGE;
                break;
            case 2:
                ret = MODEC_TYPE;
                break;
        }
        return ret;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getTitleKey() {
        return MODEC_NAME;
    }

}
