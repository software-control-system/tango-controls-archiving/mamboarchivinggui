package fr.soleil.mambo.actions;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SwingWorker;
import javax.swing.tree.TreeModel;

import org.slf4j.Logger;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.mambo.containers.MamboFrame;

public abstract class AMatchAttributesAction<T extends JTree, M extends TreeModel, A extends JPanel>
        extends AbstractAction implements ICancelable {

    private static final long serialVersionUID = 4186385304039284082L;

    private volatile boolean canceled;
    private volatile ATreeBuilder treeBuilder;
    private ProgressDialog progressDialog;

    public AMatchAttributesAction(String name) {
        super(name);
        setEnabled(true);
    }

    protected abstract Logger getLogger();

    protected abstract String getSearchRunningMessage();

    protected abstract String getSearchCancelledMessage();

    protected abstract String getSearchInterruptedMessage();

    protected abstract String getSearchErrorMessage();

    protected abstract String getSearchOkMessage();

    protected abstract A getAttributesTab();

    protected abstract String getPatternFromAttributesTab(A attributesTab);

    protected abstract T recorverTree(A attributesTab);

    protected abstract M recoverModel(T tree);

    protected abstract void prepareTreeModel(M model);

    protected abstract List<Domain> loadDomains(String pattern, ICancelable cancelable)
            throws ArchivingException, DevFailed;

    protected abstract void applyDomains(List<Domain> domains, T tree, M model);

    protected abstract void setEnabled(A attributesTab, boolean enabled);

    protected abstract ATreeBuilder generateTreeBuilder(T tree, M model, String pattern);

    @Override
    public boolean isCanceled() {
        return canceled;
    }

    @Override
    public void setCanceled(boolean canceled) {
        if (canceled != this.canceled) {
            this.canceled = canceled;
            ATreeBuilder builder = treeBuilder;
            if (canceled) {
                if (builder != null) {
                    builder.setCanceled(canceled);
                }
                setEnabled(getAttributesTab(), true);
                getLogger().debug(getSearchCancelledMessage());
            }
        }
    }

    @Override
    public final void actionPerformed(ActionEvent evt) {
        canceled = false;
        final A attributesTab = getAttributesTab();
        final String pattern = getPatternFromAttributesTab(attributesTab);
        setEnabled(attributesTab, false);
        try {
            T tree = recorverTree(attributesTab);
            M model = recoverModel(tree);

            Window parent = WindowSwingUtils.getWindowForComponent(tree);
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(parent == null ? MamboFrame.getInstance() : parent);
                progressDialog.setTitle(getSearchRunningMessage());
                progressDialog.setMainMessage(progressDialog.getTitle());
                progressDialog.setProgressIndeterminate(true);
            }
            progressDialog.pack();
            progressDialog.setLocationRelativeTo(parent);
            progressDialog.setCancelable(this);
            progressDialog.setVisible(true);

            prepareTreeModel(model);
            ATreeBuilder former = treeBuilder;
            if (former != null) {
                former.setCanceled(true);
            }
            ATreeBuilder builder = generateTreeBuilder(tree, model, pattern);
            treeBuilder = builder;
            builder.execute();
        } catch (final Exception e) {
            getLogger().error(getSearchErrorMessage(), e);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected abstract class ATreeBuilder extends SwingWorker<List<Domain>, Void> implements ICancelable {

        private T tree;
        private M model;
        private String pattern;
        private volatile boolean canceled;

        public ATreeBuilder(T tree, M model, String pattern) {
            super();
            this.tree = tree;
            this.model = model;
            this.pattern = pattern;
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @Override
        protected List<Domain> doInBackground() throws Exception {
            return loadDomains(pattern, this);
        }

        @Override
        protected void done() {
            try {
                List<Domain> domains = get();
                if (!isCanceled()) {
                    applyDomains(domains, tree, model);
                    getLogger().debug(getSearchOkMessage());
                }
            } catch (InterruptedException e) {
                getLogger().debug(getSearchInterruptedMessage());
            } catch (ExecutionException e) {
                getLogger().error(getSearchErrorMessage(), e.getCause());
            } finally {
                setEnabled(getAttributesTab(), true);
                if (progressDialog != null) {
                    progressDialog.setVisible(false);
                }
            }
        }

    }

}
