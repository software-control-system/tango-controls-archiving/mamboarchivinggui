package fr.soleil.mambo.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.mambo.containers.MamboFrame;

public class ExitAction extends AbstractAction {

    private static final long serialVersionUID = 2510197742127430255L;

    /**
     * @param name
     */
    public ExitAction(String name) {
        super(name);
        this.putValue(Action.NAME, name);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        MamboFrame.getInstance().close();
    }
}
