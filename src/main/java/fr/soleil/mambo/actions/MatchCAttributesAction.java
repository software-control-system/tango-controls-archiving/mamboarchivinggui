//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/actions/MatchCAttributesAction.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  MatchCAttributesAction.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: MatchCAttributesAction.java,v $
// Revision 1.4  2006/09/20 12:49:11  ounsy
// minor changes
//
// Revision 1.3  2006/05/19 14:57:17  ounsy
// minor changes
//
// Revision 1.2  2005/11/29 18:27:45  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:20  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.actions;

import javax.swing.AbstractAction;
import javax.swing.Action;

public abstract class MatchCAttributesAction extends AbstractAction {

    private static final long serialVersionUID = 2864272109980670287L;

    public MatchCAttributesAction(final String name) {
        putValue(Action.NAME, name);
        setEnabled(true);
    }

}
