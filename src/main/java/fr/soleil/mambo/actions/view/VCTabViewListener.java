package fr.soleil.mambo.actions.view;

import fr.soleil.docking.event.ViewEvent;
import fr.soleil.docking.infonode.view.InfoNodeView;
import fr.soleil.docking.listener.IViewListener;
import fr.soleil.mambo.components.view.OpenedVCComboBox;
import fr.soleil.mambo.data.view.ViewConfiguration;

public class VCTabViewListener implements IViewListener {

    @Override
    public void viewChanged(ViewEvent event) {
        if ((event != null) && (event.getSource() instanceof InfoNodeView)
                && (event.getReason() == ViewEvent.FOCUS_GAINED)) {
            InfoNodeView view = (InfoNodeView) event.getSource();
            if (view.getId() instanceof ViewConfiguration) {
                OpenedVCComboBox.getInstance().selectElement((ViewConfiguration) view.getId());
            }
        }
    }

}
