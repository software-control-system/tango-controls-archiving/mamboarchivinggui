/*
 * Synchrotron Soleil
 * 
 * File : ViewEditCopyAction.java
 * 
 * Project : Mambo_CVS
 * 
 * Description :
 * 
 * Author : SOLEIL
 * 
 * Original : 8 mars 2006
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: ViewEditCopyAction.java,v
 *
 */
package fr.soleil.mambo.actions.view;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.containers.view.AbstractViewCleanablePanel;
import fr.soleil.mambo.containers.view.ViewStringStateBooleanScalarPanel;
import fr.soleil.mambo.containers.view.ViewStringStateBooleanSpectrumPanel;
import fr.soleil.mambo.containers.view.dialogs.ViewEditCopyDialog;
import fr.soleil.mambo.tools.Messages;

public class ViewEditCopyAction extends AbstractAction {

    private static final long serialVersionUID = 3377629743721232717L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewEditCopyAction.class);

    private AbstractViewCleanablePanel sourcePanel;
    private int reference;

    /**
     * @param name
     */
    public ViewEditCopyAction(String name, AbstractViewCleanablePanel sourcePanel, int reference) {
        super.putValue(Action.NAME, name);
        super.putValue(Action.SHORT_DESCRIPTION, name);
        this.sourcePanel = sourcePanel;
        this.reference = reference;
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        WaitingDialog.changeFirstMessage(Messages.getMessage("DIALOGS_WAITING_COPY_TABLE_TITLE"));
        WaitingDialog.changeSecondMessage(Messages.getMessage("DIALOGS_WAITING_WAIT_TABLE_TITLE"));
        WaitingDialog.openInstance();
        try {
            String copyText;
            if (sourcePanel instanceof ViewStringStateBooleanSpectrumPanel) {
                switch (reference) {
                    case ViewStringStateBooleanSpectrumPanel.READ_REFERENCE:
                        copyText = ((ViewStringStateBooleanSpectrumPanel) sourcePanel).getReadValue();
                        break;
                    case ViewStringStateBooleanSpectrumPanel.WRITE_REFERENCE:
                        copyText = ((ViewStringStateBooleanSpectrumPanel) sourcePanel).getWriteValue();
                        break;
                    default:
                        copyText = ObjectUtils.EMPTY_STRING;
                }
            } else if (sourcePanel instanceof ViewStringStateBooleanScalarPanel) {
                copyText = ((ViewStringStateBooleanScalarPanel) sourcePanel).getSelectedToString(reference);
            } else {
                copyText = ObjectUtils.EMPTY_STRING;
            }
            ViewEditCopyDialog dialog = new ViewEditCopyDialog(MamboFrame.getInstance(),
                    (String) this.getValue(Action.SHORT_DESCRIPTION), copyText);
            dialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowOpened(WindowEvent e) {
                    WaitingDialog.closeInstance();
                }
            });
            if (sourcePanel != null) {
                dialog.setLocationRelativeTo(sourcePanel);
            }
            dialog.setVisible(true);
        } catch (Throwable t) {
            if (t instanceof OutOfMemoryError) {
                sourcePanel.outOfMemoryErrorManagement();
            } else {
                LOGGER.error(TangoExceptionHelper.getErrorMessage(t), t);
            }
        }
        WaitingDialog.closeInstance();
    }
}
