/*
 * Synchrotron Soleil
 * 
 * File : ViewEditCopyValidateAction.java
 * 
 * Project : Mambo_CVS
 * 
 * Description :
 * 
 * Author : SOLEIL
 * 
 * Original : 8 mars 2006
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: ViewEditCopyValidateAction.java,v
 *
 */
package fr.soleil.mambo.actions.view;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.tools.Messages;

public class ViewEditCopyValidateAction extends AbstractAction {

    private static final long serialVersionUID = 7383510483827640392L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewEditCopyValidateAction.class);

    private final JTextArea text;
    private final JDialog dialog;

    /**
     * @param name
     */
    public ViewEditCopyValidateAction(final String name, final JTextArea copyText, final JDialog _dialog) {
        super.putValue(Action.NAME, name);
        super.putValue(Action.SHORT_DESCRIPTION, name);
        text = copyText;
        dialog = _dialog;
    }

    @Override
    public void actionPerformed(final ActionEvent arg0) {
        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        final Clipboard clipboard = toolkit.getSystemClipboard();
        try {
            final StringSelection stringSelection = new StringSelection(
                    text.getText().replace("\n", GUIUtilities.CRLF));
            clipboard.setContents(stringSelection, stringSelection);
        } catch (Throwable t) {
            if (t instanceof OutOfMemoryError) {
                JOptionPane.showMessageDialog(MamboFrame.getInstance(), "Memory Error",
                        Messages.getMessage("DIALOGS_VIEW_MEMORY_ERROR_TITLE"), JOptionPane.ERROR_MESSAGE);
            }
            LOGGER.error(TangoExceptionHelper.getErrorMessage(t), t);
        }
        dialog.setVisible(false);
    }
}
