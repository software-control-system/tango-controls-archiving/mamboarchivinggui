// +======================================================================
// $Source:
// /cvsroot/tango-cs/archiving/tool/hdbtdb/mamboArchivingGUI/src/main/java/fr/soleil/mambo/actions/view/VCRefreshAction.java,v
// $

// Project: Tango Archiving Service

// Description: Java source code for the class VCViewAction.
// (Claisse Laurent) - 5 juil. 2005

// $Author$

// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX

// -======================================================================
package fr.soleil.mambo.actions.view;

import java.lang.ref.WeakReference;

import javax.swing.ImageIcon;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.icons.DecorableIcon;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.thread.VCRefreshSwingWorker;

public class VCRefreshAction extends AbstractVCRefreshAction {

    private static final long serialVersionUID = 9131955657365579954L;

    private static final DecorableIcon SELECTED_ICON = new DecorableIcon(Mambo.class.getResource("icons/View.gif"));
    static {
        SELECTED_ICON.setDecoration(new ImageIcon(Mambo.class.getResource("icons/bullet_check.png")));
        SELECTED_ICON.setDecorationHorizontalAlignment(DecorableIcon.RIGHT);
        SELECTED_ICON.setDecorationVerticalAlignment(DecorableIcon.BOTTOM);
        SELECTED_ICON.setDecorated(true);
    }

    private final WeakReference<ViewConfigurationBean> vcBeanRef;
    private final boolean selectedAttributes;

    /**
     * @param name
     */
    public VCRefreshAction(String name, boolean selectedAttributes, ViewConfigurationBean viewConfigurationBean) {
        super(name, selectedAttributes ? SELECTED_ICON : ICON);
        this.selectedAttributes = selectedAttributes;
        vcBeanRef = (viewConfigurationBean == null ? null
                : new WeakReference<ViewConfigurationBean>(viewConfigurationBean));
    }

    @Override
    protected VCRefreshSwingWorker buildSwingWorker() {
        return new VCRefreshSwingWorker(ObjectUtils.recoverObject(vcBeanRef), selectedAttributes);
    }

}
