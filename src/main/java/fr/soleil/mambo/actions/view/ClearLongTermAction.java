package fr.soleil.mambo.actions.view;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.soleil.mambo.containers.view.dialogs.DateRangeBox;

public final class ClearLongTermAction extends AbstractAction {

    private static final long serialVersionUID = 6403880119061787699L;

    private final DateRangeBox dateRangeBox;

    public ClearLongTermAction(final String name, final DateRangeBox dateRangeBox) {
        super(name);
        this.dateRangeBox = dateRangeBox;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        this.dateRangeBox.setLongTerm(false);
        this.dateRangeBox.setEnabledFields(true);
    }

}
