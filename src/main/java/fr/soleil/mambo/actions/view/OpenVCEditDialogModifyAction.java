package fr.soleil.mambo.actions.view;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.mambo.bean.view.ViewConfigurationBean;

public class OpenVCEditDialogModifyAction extends AbstractAction {

    private static final long serialVersionUID = 6626469510900400687L;

    private final ViewConfigurationBean viewConfigurationBean;

    /**
     * @param name
     */
    public OpenVCEditDialogModifyAction(final String name, final ViewConfigurationBean viewConfigurationBean) {
        super();
        putValue(Action.NAME, name);
        putValue(Action.SHORT_DESCRIPTION, name);
        this.viewConfigurationBean = viewConfigurationBean;
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        viewConfigurationBean.editViewConfiguration();
    }

}
