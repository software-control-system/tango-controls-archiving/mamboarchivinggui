package fr.soleil.mambo.actions.view;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.thread.VCRefreshSwingWorker;

public abstract class AbstractVCRefreshAction extends AbstractAction {

    private static final long serialVersionUID = -1830236711122775033L;

    protected static final ImageIcon ICON = new ImageIcon(Mambo.class.getResource("icons/View.gif"));

    public AbstractVCRefreshAction(String name, Icon icon) {
        super();
        putValue(Action.NAME, name);
        putValue(Action.SHORT_DESCRIPTION, name);
        putValue(VCRefreshAction.SMALL_ICON, icon);
    }

    protected abstract VCRefreshSwingWorker buildSwingWorker();

    @Override
    public final void actionPerformed(ActionEvent e) {
        final VCRefreshSwingWorker worker = buildSwingWorker();
        SwingUtilities.invokeLater(() -> {
            worker.prepareWaitingDialog();
            worker.execute();
        });
    }
}
