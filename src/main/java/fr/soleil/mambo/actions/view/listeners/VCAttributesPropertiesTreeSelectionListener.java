// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/actions/view/listeners/VCAttributesPropertiesTreeSelectionListener.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class
// VCAttributesPropertiesTreeSelectionListener.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: VCAttributesPropertiesTreeSelectionListener.java,v $
// Revision 1.4 2007/01/11 14:05:46 ounsy
// Math Expressions Management (warning ! requires atk 2.7.0 or greater)
//
// Revision 1.3 2006/05/19 15:03:24 ounsy
// minor changes
//
// Revision 1.2 2005/11/29 18:27:07 chinkumo
// no message
//
// Revision 1.1.2.2 2005/09/14 15:41:20 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.actions.view.listeners;

import java.util.Collection;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.actions.view.dialogs.listeners.PlotPropertyComboListener;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.view.VCAttributesPropertiesTree;
import fr.soleil.mambo.containers.view.dialogs.AttributesPlotPropertiesPanel;
import fr.soleil.mambo.containers.view.dialogs.VCEditDialog;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.data.view.ViewConfigurationAttributeProperties;
import fr.soleil.mambo.data.view.ViewConfigurationAttributes;
import fr.soleil.mambo.models.AttributesTreeModel;

public class VCAttributesPropertiesTreeSelectionListener implements TreeSelectionListener {

    private ViewConfigurationBean viewConfigurationBean;
    private VCEditDialog editDialog;

    public VCAttributesPropertiesTreeSelectionListener(ViewConfigurationBean viewConfigurationBean,
            VCEditDialog editDialog) {
        super();
        this.viewConfigurationBean = viewConfigurationBean;
        this.editDialog = editDialog;
    }

    @Override
    public void valueChanged(TreeSelectionEvent event) {
        editDialog.getAttributesPlotPropertiesTab().getVcAttributesPropertiesTree().saveLastSelectionPath();
        treeSelectionAttributeSave();
        treeSelectionAttributesPush();
    }

    public void treeSelectionAttributeSave() {

        VCAttributesPropertiesTree tree = editDialog.getAttributesPlotPropertiesTab().getVcAttributesPropertiesTree();
        Collection<ViewConfigurationAttribute> attributes = tree.getLastListOfAttributesToSet();
        if ((attributes != null) && (attributes.size() < 2)) {

            AttributesPlotPropertiesPanel panel = editDialog.getAttributesPlotPropertiesTab().getPropertiesPanel();
            PlotProperties plotProperties = panel.getPlotProperties();
            boolean hidden = panel.isHidden();

            // the VC before modification
            ViewConfiguration currentVC = viewConfigurationBean.getEditingViewConfiguration();
            if (currentVC != null) {
                ViewConfigurationAttributes currentVCAttributes = currentVC.getAttributes();

                for (ViewConfigurationAttribute attribute : attributes) {
                    // This element (next) has neither factor nor axis values so the
                    // current values must be retrieved to avoid their loss
                    ViewConfigurationAttribute currArribute = currentVCAttributes
                            .getAttribute(attribute.getCompleteName());

                    // the properties to set
                    ViewConfigurationAttributeProperties currentProperties = new ViewConfigurationAttributeProperties();
                    ViewConfigurationAttributePlotProperties currentPlotProperties = currentProperties
                            .getPlotProperties();
                    if (!(plotProperties.getViewType() == currentPlotProperties.getViewType()))
                        currentPlotProperties.setViewType(plotProperties.getViewType());
                    if (!plotProperties.getBar().equals(currentPlotProperties.getBar()))
                        currentPlotProperties.setBar(plotProperties.getBar());
                    if (!plotProperties.getCurve().equals(currentPlotProperties.getCurve()))
                        currentPlotProperties.setCurve(plotProperties.getCurve());
                    if (!plotProperties.getMarker().equals(currentPlotProperties.getMarker()))
                        currentPlotProperties.setMarker(plotProperties.getMarker());
                    if (!plotProperties.getTransform().equals(currentPlotProperties.getTransform()))
                        currentPlotProperties.setTransform(plotProperties.getTransform());
                    if (!plotProperties.getInterpolation().equals(currentPlotProperties.getInterpolation()))
                        currentPlotProperties.setInterpolation(plotProperties.getInterpolation());
                    if (!plotProperties.getSmoothing().equals(currentPlotProperties.getSmoothing()))
                        currentPlotProperties.setSmoothing(plotProperties.getSmoothing());
                    if (!plotProperties.getMath().equals(currentPlotProperties.getMath()))
                        currentPlotProperties.setMath(plotProperties.getMath());
                    if (!(hidden == currentPlotProperties.isHidden()))
                        currentPlotProperties.setHidden(hidden);

                    attribute.setProperties(currentProperties);

                    // Retrieve the factor value
                    attribute.setFactor(currArribute.getFactor());
                    attribute.getProperties().getPlotProperties()
                            .setAxisChoice(currArribute.getProperties().getPlotProperties().getAxisChoice());
                    attribute.getProperties().getPlotProperties().setSpectrumViewType(
                            currArribute.getProperties().getPlotProperties().getSpectrumViewType());
                    currentVCAttributes.addAttribute(attribute);
                }
            }

            ViewConfigurationAttributeProperties.resetCurrentProperties();
        }
    }

    public void treeSelectionAttributesPush() {

        VCAttributesPropertiesTree tree = editDialog.getAttributesPlotPropertiesTab().getVcAttributesPropertiesTree();
        Collection<ViewConfigurationAttribute> attributes = tree.getListOfAttributesToSet();

        if ((attributes != null) && (attributes.size() > 0)) {

            AttributesPlotPropertiesPanel panel = editDialog.getAttributesPlotPropertiesTab().getPropertiesPanel();
            panel.setEnabled(!(attributes.size() >= 2));

            DefaultComboBoxModel<String> axisChoiceComboModel = panel.getAxisChoiceComboModel();
            JComboBox<String> axisChoiceCombo = panel.getAxisChoiceCombo();
            PlotPropertyComboListener plotPropertyComboListener = panel.getPlotPropertyChoiceComboListener();

            DefaultComboBoxModel<String> spectrumViewTypeComboModel = panel.getSpectrumViewTypeComboModel();
            JComboBox<String> spectrumViewTypeCombo = panel.getSpectrumViewTypeCombo();

            JTextField factorField = panel.getFactorField();

            if (attributes.size() == 1) {// Select one attribut

                // les listeners des combobox sont enleves pour qu'ils ne se
                // declenchent pas, et ils sont rajoutes juste apres
                axisChoiceCombo.removeActionListener(plotPropertyComboListener);
                if (axisChoiceComboModel.getSize() == 4)
                    axisChoiceComboModel.removeElement("---");
                axisChoiceCombo.addActionListener(plotPropertyComboListener);

                spectrumViewTypeCombo.removeActionListener(plotPropertyComboListener);
                if (spectrumViewTypeComboModel.getSize() == 4)
                    spectrumViewTypeComboModel.removeElement("---");
                spectrumViewTypeCombo.addActionListener(plotPropertyComboListener);

                DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

                TreeNode[] path = node.getPath();
                String completeName = AttributesTreeModel.translatePathIntoKey(path);

                ViewConfiguration currentViewConfiguration = viewConfigurationBean.getEditingViewConfiguration();
                if (currentViewConfiguration != null) {
                    ViewConfigurationAttribute selectedAttribute = currentViewConfiguration.getAttributes()
                            .getAttribute(completeName);

                    ViewConfigurationAttributePlotProperties plotProperties = new ViewConfigurationAttributePlotProperties();

                    if (selectedAttribute != null) {
                        ViewConfigurationAttributeProperties properties = selectedAttribute.getProperties();

                        plotProperties = properties.getPlotProperties();
                        editDialog.getAttributesPlotPropertiesTab().getPropertiesPanel()
                                .setFactor(selectedAttribute.getFactor());
                    }
                    viewConfigurationBean.pushAttributesPlotProperties(plotProperties);
                }
            }

            if (attributes.size() >= 2) {// Multi-Selection

                // les listeners des combobox sont enleves pour qu'ils ne se
                // declenchent pas, et ils sont rajoutes juste apres
                axisChoiceCombo.removeActionListener(plotPropertyComboListener);
                if (axisChoiceComboModel.getSize() == 3)
                    axisChoiceComboModel.insertElementAt("---", 0);
                axisChoiceComboModel.setSelectedItem("---");
                axisChoiceCombo.addActionListener(plotPropertyComboListener);

                spectrumViewTypeCombo.removeActionListener(plotPropertyComboListener);
                if (spectrumViewTypeComboModel.getSize() == 3)
                    spectrumViewTypeComboModel.insertElementAt("---", 0);
                spectrumViewTypeComboModel.setSelectedItem("---");
                spectrumViewTypeCombo.addActionListener(plotPropertyComboListener);

                factorField.setText(ObjectUtils.EMPTY_STRING);
            }
        }
    }
}
