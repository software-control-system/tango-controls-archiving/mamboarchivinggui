// +======================================================================
// $Source:
// /cvsroot/tango-cs/archiving/tool/hdbtdb/mamboArchivingGUI/src/main/java/fr/soleil/mambo/actions/view/VCRefreshAction.java,v
// $

// Project: Tango Archiving Service

// Description: Java source code for the class VCViewAction.
// (Claisse Laurent) - 5 juil. 2005

// $Author$

// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX

// -======================================================================
package fr.soleil.mambo.actions.view;

import fr.soleil.mambo.bean.manager.ViewConfigurationBeanManager;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.thread.VCRefreshSwingWorker;

public class SelectedVCRefreshAction extends AbstractVCRefreshAction {

    private static final long serialVersionUID = 3772883244475421126L;

    private static SelectedVCRefreshAction instance = null;

    public static SelectedVCRefreshAction getInstance(String name) {
        if (instance == null) {
            instance = new SelectedVCRefreshAction(name);
        }
        return instance;
    }

    public static SelectedVCRefreshAction getInstance() {
        return instance;
    }

    /**
     * @param name
     */
    public SelectedVCRefreshAction(String name) {
        super(name, ICON);
    }

    @Override
    protected VCRefreshSwingWorker buildSwingWorker() {
        ViewConfigurationBean viewConfigurationBean = ViewConfigurationBeanManager.getInstance()
                .getBeanFor(ViewConfigurationBeanManager.getInstance().getSelectedConfiguration());
        return new VCRefreshSwingWorker(viewConfigurationBean, false);
    }

}
