package fr.soleil.mambo.actions.view;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.lang.ref.WeakReference;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.containers.view.ViewSpectrumStackPanel;
import fr.soleil.mambo.containers.view.dialogs.ViewSpectrumStackChartDialog;
import fr.soleil.mambo.tools.Messages;

/**
 * @author awo
 */
public class VCViewSpectrumStackChartAction extends AbstractAction {

    private static final long serialVersionUID = 4994275452649219569L;

    private final WeakReference<ViewSpectrumStackPanel> specPanelRef;
    private ViewSpectrumStackChartDialog viewSpectrumChartDialog;
    private final WeakReference<ViewConfigurationBean> viewReference;

    public VCViewSpectrumStackChartAction(ViewSpectrumStackPanel panel, ViewConfigurationBean viewConfigurationBean) {
        super();
        specPanelRef = (panel == null ? null : new WeakReference<ViewSpectrumStackPanel>(panel));
        viewReference = (viewConfigurationBean == null ? null
                : new WeakReference<ViewConfigurationBean>(viewConfigurationBean));
        viewSpectrumChartDialog = null;
    }

    protected ViewSpectrumStackPanel getSpecPanel() {
        return ObjectUtils.recoverObject(specPanelRef);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        ViewSpectrumStackPanel specPanel = getSpecPanel();
        if ((evt != null) && (specPanel != null)) {
            if (evt.getSource().getClass() == JCheckBox.class) {
                if (!specPanel.getReadCheckBox() && !specPanel.getWriteCheckBox()) {
                    specPanel.enableViewButton(false, true);
                } else {
                    specPanel.enableViewButton(true, true);
                }
            } else if (evt.getSource().getClass() == JButton.class) {
                specPanel.enableViewButton(false, false);
                String firstMessage = WaitingDialog.getFirstMessage();
                boolean wasOpen = WaitingDialog.isInstanceVisible();
                try {
                    WaitingDialog.changeFirstMessage(Messages.getMessage("DIALOGS_WAITING_PREPARING_VIEW_TITLE"));
                    WaitingDialog.openInstance();
                    ViewConfigurationBean viewConfigurationBean = ObjectUtils.recoverObject(viewReference);
                    try {
                        if (viewConfigurationBean != null) {
                            Window parent = null;
                            if (evt.getSource() instanceof Component) {
                                parent = WindowSwingUtils.getWindowForComponent((Component) evt.getSource());
                            }
                            if (viewSpectrumChartDialog == null) {
                                viewSpectrumChartDialog = new ViewSpectrumStackChartDialog(parent, specPanel,
                                        viewConfigurationBean);
                            }
                            viewSpectrumChartDialog.setLocationRelativeTo(parent);
                            viewSpectrumChartDialog.updateContent();
                        }
                    } finally {
                        specPanel.enableViewButton(true, false);
                        if (WaitingDialog.isInstanceVisible()) {
                            if (wasOpen) {
                                WaitingDialog.changeFirstMessage(firstMessage);
                            } else {
                                WaitingDialog.closeInstance();
                            }
                        }
                    }
                    viewSpectrumChartDialog.setVisible(true);
                    viewSpectrumChartDialog.clean();
                } catch (OutOfMemoryError oome) {
                    specPanel.outOfMemoryErrorManagement();
                }
            }
        }
    }

}
