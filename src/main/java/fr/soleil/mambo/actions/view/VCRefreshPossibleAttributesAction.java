package fr.soleil.mambo.actions.view;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.containers.view.dialogs.VCEditDialog;
import fr.soleil.mambo.datasources.db.attributes.AttributeManagerFactory;
import fr.soleil.mambo.datasources.db.attributes.IAttributeManager;
import fr.soleil.mambo.tools.Messages;

/**
 * @author operateur
 */
public class VCRefreshPossibleAttributesAction extends AbstractAction {

    private static final long serialVersionUID = 5430025852299032299L;

    private static final Logger LOGGER = LoggerFactory.getLogger(VCRefreshPossibleAttributesAction.class);

    private Boolean historic;

    private final static String title = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_UPDATE_KO");
    private String msg;
    private final VCEditDialog editDialog;

    public VCRefreshPossibleAttributesAction(final VCEditDialog editDialog) {
        super();
        super.putValue(Action.NAME, Messages.getMessage("VIEW_ACTION_REFRESH_POSSIBLE_BUTTON"));
        this.editDialog = editDialog;
    }

    @Override
    public void actionPerformed(final ActionEvent arg0) {
        historic = editDialog.getAttributesTab().getPossibleAttributesTree().getModel().isHistoric();
        WaitingDialog.openInstance();
        boolean closeDialog = true;
        try {
            final IAttributeManager source = AttributeManagerFactory.getCurrentImpl();
            source.loadDomains(null, historic, true);
        } catch (final Throwable e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            if (historic == null) {
                LOGGER.warn("no connection to TTS");
            } else if (historic.booleanValue()) {
                LOGGER.warn("no connection to HDB");
            } else {
                LOGGER.warn("no connection to TDB");
            }

            JOptionPane.showMessageDialog(editDialog, msg, title, JOptionPane.ERROR_MESSAGE);
            msg = null;
            WaitingDialog.closeInstance();
            closeDialog = false;
        }
        if (closeDialog) {
            editDialog.getAttributesTab().getPossibleAttributesTree().getModel().setHistoric(historic);
            editDialog.getAttributesTab().getPossibleAttributesTree().repaint();
            WaitingDialog.closeInstance();
        }
    }
}
