package fr.soleil.mambo.actions.view;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.lang.ref.WeakReference;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.mambo.containers.view.dialogs.ViewImageDialog;
import fr.soleil.mambo.tools.Messages;

public class ViewImageAction extends AbstractAction {

    private static final long serialVersionUID = 4739861802780030594L;

    private ViewImageDialog dialog;
    private String name;

    private WeakReference<double[][]> value;
    private String displayFormat;

    protected double[][] getValue() {
        return ObjectUtils.recoverObject(value);
    }

    public void setData(double[][] value, String displayFormat) {
        this.displayFormat = displayFormat;
        this.value = (value == null ? null : new WeakReference<double[][]>(value));
        if (dialog != null) {
            dialog.setVisible(false);
        }
    }

    public ViewImageAction(String name) {
        super();
        this.putValue(Action.NAME, Messages.getMessage("VIEW_IMAGE_VIEW"));
        dialog = null;
        this.name = name;
    }

    protected ViewImageDialog getDialog(Window parent) {
        if (dialog == null) {
            dialog = new ViewImageDialog(parent);
        }
        return dialog;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Window parent;
        if (e == null) {
            parent = null;
        } else if (e.getSource() instanceof Component) {
            parent = WindowSwingUtils.getWindowForComponent((Component) e.getSource());
        } else {
            parent = null;
        }
        getDialog(parent).setImageName(name);
        getDialog(parent).setData(getValue(), displayFormat);
        getDialog(parent).setVisible(true);
    }

}
