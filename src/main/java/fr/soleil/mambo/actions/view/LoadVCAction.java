// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/actions/view/LoadVCAction.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class LoadVCAction.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.3 $
//
// $Log: LoadVCAction.java,v $
// Revision 1.3 2006/05/05 14:39:43 ounsy
// added the same optimisation on loading VCs as for ACs
//
// Revision 1.2 2005/11/29 18:27:07 chinkumo
// no message
//
// Revision 1.1.2.2 2005/09/14 15:41:20 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.actions.view;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Timestamp;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.mambo.bean.manager.ViewConfigurationBeanManager;
import fr.soleil.mambo.components.ConfigurationFileFilter;
import fr.soleil.mambo.components.view.OpenedVCComboBox;
import fr.soleil.mambo.components.view.VCFileFilter;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.datasources.file.IViewConfigurationManager;
import fr.soleil.mambo.datasources.file.ViewConfigurationManagerFactory;
import fr.soleil.mambo.tools.Messages;

public class LoadVCAction extends AbstractAction {

    private static final long serialVersionUID = 107505901499291358L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LoadVCAction.class);

    private final boolean isDefault;

    /**
     * @param name
     */
    public LoadVCAction(final String name, final boolean _isDefault) {
        super.putValue(Action.NAME, name);
        super.putValue(Action.SHORT_DESCRIPTION, name);

        isDefault = _isDefault;
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        boolean treatVC = true;
        final IViewConfigurationManager manager = ViewConfigurationManagerFactory.getCurrentImpl();
        if (!isDefault) {
            // open file chooser
            final JFileChooser chooser = new JFileChooser();
            final VCFileFilter VCfilter = new VCFileFilter();
            chooser.addChoosableFileFilter(VCfilter);
            chooser.setCurrentDirectory(new File(manager.getDefaultSaveLocation()));

            final int returnVal = chooser.showOpenDialog(MamboFrame.getInstance());
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                final File f = chooser.getSelectedFile();
                String path = f.getAbsolutePath();

                if (f != null) {
                    final String extension = ConfigurationFileFilter.getExtension(f);
                    final String expectedExtension = VCfilter.getExtension();

                    if (extension == null || !extension.equalsIgnoreCase(expectedExtension)) {
                        path += ".";
                        path += expectedExtension;
                    }
                }
                manager.setNonDefaultSaveLocation(path);
            } else {
                treatVC = false;
            }
        } else {
            manager.setNonDefaultSaveLocation(null);
        }
        if (treatVC) {
            ViewConfiguration existing = null;
            final String path = manager.getNonDefaultSaveLocation();
            for (final ViewConfiguration vc : OpenedVCComboBox.getInstance().getVCElements()) {
                if (path != null && path.equals(vc.getPath())) {
                    existing = vc;
                    break;
                }
            }
            if (existing == null) {
                loadVCOnceThePathIsSet(false);
            } else {
                // If the VC is already open, we ask to duplicate it or not
                final int ok = JOptionPane.showConfirmDialog(MamboFrame.getInstance(),
                        Messages.getMessage("DIALOGS_FILE_ALREADY_OPENED"),
                        Messages.getMessage("DIALOGS_FILE_ALREADY_OPENED_TITLE"), JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                if (ok == JOptionPane.YES_OPTION) {
                    loadVCOnceThePathIsSet(true);
                } else {
                    ViewConfigurationBeanManager.getInstance().setSelectedConfiguration(existing);
                }
            }
        }
    }

    public static void loadVCOnceThePathIsSet(final boolean modified) {
        final IViewConfigurationManager manager = ViewConfigurationManagerFactory.getCurrentImpl();
        try {
            final ViewConfiguration selectedViewConfiguration = manager.loadViewConfiguration();
            selectedViewConfiguration.setModified(modified);
            if (modified) {
                selectedViewConfiguration.setPath(null);
            } else {
                selectedViewConfiguration.setPath(manager.getSaveLocation());
            }
            // We set the last update date as now, in order to duplicate the vc
            // if we open it twice
            if (selectedViewConfiguration.getData() != null) {
                selectedViewConfiguration.getData().setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
            }

            // selectedViewConfiguration.push();
            final OpenedVCComboBox openedVCComboBox = OpenedVCComboBox.getInstance();
            if (openedVCComboBox != null) {
                openedVCComboBox.selectElement(selectedViewConfiguration);
            }

            final String msg = Messages.getLogMessage("LOAD_VIEW_CONFIGURATION_ACTION_OK");
            LOGGER.debug(msg);
        } catch (final FileNotFoundException fnfe) {
            final String msg = Messages.getLogMessage("LOAD_VIEW_CONFIGURATION_ACTION_WARNING");
            LOGGER.warn(msg, fnfe);
        } catch (final Exception e) {
            final String msg = Messages.getLogMessage("LOAD_VIEW_CONFIGURATION_ACTION_KO");
            LOGGER.error(msg, e);
        }
    }

}
