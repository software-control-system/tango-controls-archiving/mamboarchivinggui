/*
 * Synchrotron Soleil
 * 
 * File : ViewCopyAction.java
 * 
 * Project : Mambo_CVS
 * 
 * Description :
 * 
 * Author : SOLEIL
 * 
 * Original : 8 mars 2006
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: ViewCopyAction.java,v
 *
 */
package fr.soleil.mambo.actions.view;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.lang.ref.WeakReference;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.containers.view.AbstractViewCleanablePanel;
import fr.soleil.mambo.containers.view.ViewStringStateBooleanScalarPanel;
import fr.soleil.mambo.containers.view.ViewStringStateBooleanSpectrumPanel;
import fr.soleil.mambo.tools.Messages;

public class ViewCopyAction extends AbstractAction {

    private static final long serialVersionUID = 8891451802181509872L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewCopyAction.class);

    private WeakReference<AbstractViewCleanablePanel> sourcePanelRef;
    private int reference;

    /**
     * @param name
     */
    public ViewCopyAction(String name, AbstractViewCleanablePanel sourcePanel, int reference) {
        super.putValue(Action.NAME, name);
        super.putValue(Action.SHORT_DESCRIPTION, name);
        sourcePanelRef = (sourcePanel == null ? null : new WeakReference<AbstractViewCleanablePanel>(sourcePanel));
        this.reference = reference;
    }

    protected AbstractViewCleanablePanel getSourcePanel() {
        return ObjectUtils.recoverObject(sourcePanelRef);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        AbstractViewCleanablePanel sourcePanel = getSourcePanel();
        if (sourcePanel != null) {
            WaitingDialog.changeFirstMessage(Messages.getMessage("DIALOGS_WAITING_COPY_TABLE_TITLE"));
            WaitingDialog.changeSecondMessage(Messages.getMessage("DIALOGS_WAITING_WAIT_TABLE_TITLE"));
            WaitingDialog.openInstance();
            try {
                Toolkit toolkit = Toolkit.getDefaultToolkit();
                Clipboard clipboard = toolkit.getSystemClipboard();
                StringSelection stringSelection;
                if (sourcePanel instanceof ViewStringStateBooleanSpectrumPanel) {
                    switch (reference) {
                        case ViewStringStateBooleanSpectrumPanel.READ_REFERENCE:
                            stringSelection = new StringSelection(
                                    ((ViewStringStateBooleanSpectrumPanel) sourcePanel).getReadValue());
                            break;
                        case ViewStringStateBooleanSpectrumPanel.WRITE_REFERENCE:
                            stringSelection = new StringSelection(
                                    ((ViewStringStateBooleanSpectrumPanel) sourcePanel).getWriteValue());
                            break;
                        default:
                            stringSelection = new StringSelection(ObjectUtils.EMPTY_STRING);
                    }
                } else if (sourcePanel instanceof ViewStringStateBooleanScalarPanel) {
                    stringSelection = new StringSelection(
                            ((ViewStringStateBooleanScalarPanel) sourcePanel).getSelectedToString(reference));
                } else {
                    stringSelection = new StringSelection(ObjectUtils.EMPTY_STRING);
                }
                clipboard.setContents(stringSelection, stringSelection);
            } catch (Throwable t) {
                if (t instanceof OutOfMemoryError) {
                    sourcePanel.outOfMemoryErrorManagement();
                } else {
                    LOGGER.error(TangoExceptionHelper.getErrorMessage(t), t);
                }
            }
            WaitingDialog.closeInstance();
        }
    }
}
