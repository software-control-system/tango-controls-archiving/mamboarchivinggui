package fr.soleil.mambo.actions.view.listeners;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import fr.soleil.mambo.components.view.OpenedVCComboBox;
import fr.soleil.mambo.data.view.ViewConfiguration;

public class OpenedVCItemListener implements ItemListener {

    public OpenedVCItemListener() {
        super();
    }

    @Override
    public void itemStateChanged(ItemEvent event) {
        if ((event != null) && (event.getStateChange() == ItemEvent.SELECTED)
                && (event.getItem() instanceof ViewConfiguration)) {
            OpenedVCComboBox.getInstance().selectElement((ViewConfiguration) event.getItem());
        }
    }

}
