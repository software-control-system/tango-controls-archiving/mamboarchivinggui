// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/actions/view/MatchVCAttributesAction.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class MatchVCAttributesAction.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: MatchVCAttributesAction.java,v $
// Revision 1.6 2006/09/22 09:34:41 ounsy
// refactoring du package mambo.datasources.db
//
// Revision 1.5 2006/08/07 13:03:07 ounsy
// trees and lists sort
//
// Revision 1.4 2006/07/18 10:23:16 ounsy
// Less time consuming by setting tree expanding on demand only
//
// Revision 1.3 2006/05/19 15:03:05 ounsy
// minor changes
//
// Revision 1.2 2005/11/29 18:27:07 chinkumo
// no message
//
// Revision 1.1.2.3 2005/09/15 10:30:05 chinkumo
// Third commit !
//
// Revision 1.1.2.2 2005/09/14 15:41:20 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.actions.view;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.actions.AMatchAttributesAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.view.VCAttributesSelectTree;
import fr.soleil.mambo.containers.view.dialogs.AttributesTab;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.datasources.db.attributes.AttributeManagerFactory;
import fr.soleil.mambo.datasources.db.attributes.IAttributeManager;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;
import fr.soleil.mambo.models.AttributesTreeModel;
import fr.soleil.mambo.models.VCAttributesTreeModel;
import fr.soleil.mambo.tools.Messages;

public class MatchVCAttributesAction
        extends AMatchAttributesAction<VCAttributesSelectTree, VCAttributesTreeModel, AttributesTab> {

    private static final long serialVersionUID = -421393877593868169L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchVCAttributesAction.class);

    private static final String VC_ATTRIBUTES_SEARCH_RUNNING = Messages
            .getMessage("VIEW_ACTION_REFRESH_SELECT_BUTTON_SEARCHING");
    private static final String VC_ATTRIBUTES_SEARCH_CANCELLED = Messages.getLogMessage("LOAD_VC_ATTRIBUTES_CANCELLED");
    private static final String VC_ATTRIBUTES_SEARCH_OK = Messages.getLogMessage("LOAD_VC_ATTRIBUTES_OK");
    private static final String VC_ATTRIBUTES_SEARCH_INTERRUPTED = Messages
            .getLogMessage("LOAD_VC_ATTRIBUTES_INTERRUPTED");
    private static final String VC_ATTRIBUTES_SEARCH_KO = Messages.getLogMessage("LOAD_VC_ATTRIBUTES_KO");

    private final ViewConfigurationBean viewConfigurationBean;
    private final AttributesTab attributesTab;

    /**
     * @param name
     */
    public MatchVCAttributesAction(final String name, final ViewConfigurationBean viewConfigurationBean,
            final AttributesTab attributesTab) {
        super(name);
        this.viewConfigurationBean = viewConfigurationBean;
        this.attributesTab = attributesTab;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getSearchRunningMessage() {
        return VC_ATTRIBUTES_SEARCH_RUNNING;
    }

    @Override
    protected String getSearchCancelledMessage() {
        return VC_ATTRIBUTES_SEARCH_CANCELLED;
    }

    @Override
    protected String getSearchInterruptedMessage() {
        return VC_ATTRIBUTES_SEARCH_INTERRUPTED;
    }

    @Override
    protected String getSearchErrorMessage() {
        return VC_ATTRIBUTES_SEARCH_KO;
    }

    @Override
    protected String getSearchOkMessage() {
        return VC_ATTRIBUTES_SEARCH_OK;
    }

    @Override
    protected AttributesTab getAttributesTab() {
        return attributesTab;
    }

    @Override
    protected String getPatternFromAttributesTab(AttributesTab attributesTab) {
        return attributesTab.getRightRegexp();
    }

    @Override
    protected VCAttributesSelectTree recorverTree(AttributesTab attributesTab) {
        return attributesTab.getSelectedAttributesTree();
    }

    @Override
    protected VCAttributesTreeModel recoverModel(VCAttributesSelectTree tree) {
        return tree.getModel();
    }

    @Override
    protected void prepareTreeModel(VCAttributesTreeModel model) {
        model.setRootName(model.isHistoric());
    }

    @Override
    protected List<Domain> loadDomains(String pattern, ICancelable cancelable) throws ArchivingException, DevFailed {
        final Criterions searchCriterions = ITangoManager.getAttributesSearchCriterions(pattern);
        final IAttributeManager manager = AttributeManagerFactory.getCurrentImpl();
        VCAttributesTreeModel model = attributesTab.getSelectedAttributesTree().getModel();
        List<Domain> domains = manager.loadDomains(searchCriterions, model.isHistoric(), false, cancelable);
        List<Domain> savedDomains = model.prepareAndGetSavedDomains();
        return AttributesTreeModel.intersectDomains(savedDomains, domains);
    }

    @Override
    protected void applyDomains(List<Domain> domains, VCAttributesSelectTree tree, VCAttributesTreeModel model) {
        model.applyDomains(domains);
        final Map<String, ViewConfigurationAttribute> attrs = model.getAttributesCopy();
        if (attrs != null) {
            final ViewConfiguration currentViewConfiguration = viewConfigurationBean.getEditingViewConfiguration();
            if (currentViewConfiguration != null) {
                currentViewConfiguration.getAttributes().removeAttributesNotInList(attrs.keySet());
                currentViewConfiguration.getAttributes().addAttributes(attrs);
            }
            attrs.clear();
        }
        viewConfigurationBean.refreshEditingUI();
    }

    @Override
    protected void setEnabled(AttributesTab attributesTab, boolean enabled) {
        attributesTab.setVCAttributesSearchEnabled(enabled);
    }

    @Override
    protected VCTreeBuilder generateTreeBuilder(VCAttributesSelectTree tree, VCAttributesTreeModel model,
            String pattern) {
        return new VCTreeBuilder(tree, model, pattern);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class VCTreeBuilder extends ATreeBuilder {

        public VCTreeBuilder(VCAttributesSelectTree tree, VCAttributesTreeModel model, String pattern) {
            super(tree, model, pattern);
        }

    }
}
