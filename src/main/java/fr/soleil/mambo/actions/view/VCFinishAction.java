package fr.soleil.mambo.actions.view;

import java.awt.event.ActionEvent;
import java.sql.Timestamp;
import java.util.Collection;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.view.VCCustomTabbedPane;
import fr.soleil.mambo.containers.view.dialogs.ChartGeneralTabbedPane;
import fr.soleil.mambo.containers.view.dialogs.DateRangeBox;
import fr.soleil.mambo.containers.view.dialogs.GeneralTab;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.data.view.ViewConfigurationData;
import fr.soleil.mambo.models.VCAttributesTreeModel;
import fr.soleil.mambo.tools.Messages;

public class VCFinishAction extends AbstractAction {

    private static final long serialVersionUID = -322136353267965103L;

    private final ViewConfigurationBean viewConfigurationBean;

    /**
     * @param name
     */
    public VCFinishAction(final String name, final ViewConfigurationBean viewConfigurationBean) {
        super();
        this.viewConfigurationBean = viewConfigurationBean;
        putValue(Action.NAME, name);
        putValue(Action.SHORT_DESCRIPTION, name);
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        // Save parameters of last attribut
        final VCCustomTabbedPane tabbedPane = viewConfigurationBean.getEditDialog().getVcCustomTabbedPane();
        final int oldValue = tabbedPane.getSelectedIndex();
        if (oldValue == 2) {
            viewConfigurationBean.getEditDialog().getAttributesPlotPropertiesTab().getVcAttributesPropertiesTree()
                    .saveLastSelectionPath();
            viewConfigurationBean.getEditDialog().getAttributesPlotPropertiesTab().getVcAttributesPropertiesTree()
                    .getVcAttributesPropertiesTreeSelectionListener().treeSelectionAttributeSave();
        }
        if (oldValue == 3) {
            viewConfigurationBean.getEditDialog().getExpressionTab().getExpressionTree().saveCurrentSelection();
            viewConfigurationBean.getEditDialog().getExpressionTab().getExpressionTree().getExpressionTreeListener()
                    .treeSelectionSave();
        }

        if (verifyVC(viewConfigurationBean.getEditDialog().getGeneralTab().getDateRangeBox())) {
            // --setting the generic plot data
            final ChartGeneralTabbedPane generalTabbedPane = viewConfigurationBean.getEditDialog().getGeneralTab()
                    .getChartGeneralTabbedPane();
            final ChartProperties chartProperties = generalTabbedPane.getProperties();

            final ViewConfigurationData newData = new ViewConfigurationData(chartProperties);

            final ViewConfiguration currentVC = viewConfigurationBean.getEditingViewConfiguration();
            if (currentVC != null) {
                final ViewConfigurationData oldData = currentVC.getData();

                // --setting the edit dates
                if (currentVC.isNew()) {
                    newData.setCreationDate(GUIUtilities.now());
                    newData.setPath(null);
                } else {
                    newData.setCreationDate(oldData.getCreationDate());
                    newData.setPath(oldData.getPath());
                }

                newData.setLastUpdateDate(GUIUtilities.now());

                // --setting the start and end dates and historic parameter
                final boolean dynamic = viewConfigurationBean.getEditDialog().getGeneralTab().getDateRangeBox()
                        .isDynamicDateRange();
                final boolean longTerm = viewConfigurationBean.getEditDialog().getGeneralTab().getDateRangeBox()
                        .isLongTerm();
                if (dynamic) {
                    final Timestamp[] range = viewConfigurationBean.getEditDialog().getGeneralTab().getDateRangeBox()
                            .getDynamicStartAndEndDates();
                    newData.setStartDate(range[0]);
                    newData.setEndDate(range[1]);
                    // viewConfigurationBean.getEditDialog().getGeneralTab().getDateRangeBox()
                    // .setEnabledFields(true);
                } else {
                    newData.setStartDate(
                            viewConfigurationBean.getEditDialog().getGeneralTab().getDateRangeBox().getStartDate());
                    newData.setEndDate(
                            viewConfigurationBean.getEditDialog().getGeneralTab().getDateRangeBox().getEndDate());
                }
                newData.setLongTerm(longTerm);
                newData.setDynamicDateRange(dynamic);
                newData.setDateRange(
                        viewConfigurationBean.getEditDialog().getGeneralTab().getDateRangeBox().getDateRange());
                // --setting the start and end dates
                final GeneralTab generalTab = viewConfigurationBean.getEditDialog().getGeneralTab();
                newData.setHistoric(generalTab.isHistoric());
                newData.setName(generalTab.getName());
                newData.setSamplingType(generalTab.getSamplingType());
                newData.setAutoAveraging(generalTab.isAutoAveraging());

                currentVC.setData(newData);
                currentVC.setModified(true);

                viewConfigurationBean.validateEdition();
            }
        }
    }

    /**
     * @return
     */
    private boolean containsNonSetAttributes() {
        boolean containsUnset = false;
        final VCAttributesTreeModel model = viewConfigurationBean.getEditDialog().getAttributesTab()
                .getSelectedAttributesTree().getModel();
        if (model != null) {
            Collection<ViewConfigurationAttribute> attributes = model.getTreeAttributes();
            if (attributes != null) {
                for (ViewConfigurationAttribute nextValue : model.getTreeAttributes()) {
                    if (nextValue != null) {
                        final String name = nextValue.getCompleteName();
                        final ViewConfiguration currentVC = viewConfigurationBean.getEditingViewConfiguration();
                        if (!currentVC.containsAttribute(name)) {
                            containsUnset = true;
                            break;
                        }
                        ViewConfigurationAttribute attribute = currentVC.getAttribute(name);
                        if ((attribute != null) && attribute.isEmpty()) {
                            containsUnset = true;
                            break;
                        }
                    }
                }
            }
        }
        return containsUnset;
    }

    private boolean verifyAttributesAreSet() {
        if (containsNonSetAttributes()) {
            final String msgTitle = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_NON_SET_CONFIRM_TITLE");
            String msgConfirm = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_NON_SET_CONFIRM_LABEL_1");
            msgConfirm += GUIUtilities.CRLF;
            msgConfirm += Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_NON_SET_CONFIRM_LABEL_2");
            final String msgCancel = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_NON_SET_CONFIRM_CANCEL");
            final String msgValidate = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_NON_SET_CONFIRM_VALIDATE");
            final Object[] options = { msgValidate, msgCancel };

            final int confirm = JOptionPane.showOptionDialog(null, msgConfirm, msgTitle, JOptionPane.DEFAULT_OPTION,
                    JOptionPane.WARNING_MESSAGE, null, options, options[0]);
            return confirm == JOptionPane.OK_OPTION;
        } else {
            return true;
        }
    }

    /**
     * @return
     */
    public boolean verifyVC(final DateRangeBox dateRangeBox) {
        return ViewConfigurationData.verifyDates(dateRangeBox) && verifyAttributesAreSet();
    }

}
