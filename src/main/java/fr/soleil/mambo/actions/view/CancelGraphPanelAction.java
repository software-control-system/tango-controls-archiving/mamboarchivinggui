package fr.soleil.mambo.actions.view;

import java.awt.event.ActionEvent;
import java.lang.ref.WeakReference;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;

public class CancelGraphPanelAction extends AbstractAction {

    private static final long serialVersionUID = 4684137310796925235L;

    private final WeakReference<ViewConfigurationBean> vcBeanRef;

    public CancelGraphPanelAction(String name, ViewConfigurationBean vcBean) {
        super();
        this.putValue(Action.NAME, name);
        vcBeanRef = (vcBean == null ? null : new WeakReference<ViewConfigurationBean>(vcBean));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ViewConfigurationBean vcBean = ObjectUtils.recoverObject(vcBeanRef);
        if (vcBean != null) {
            ProgressDialog waitingDialog = vcBean.getWaitingDialog();
            if (waitingDialog != null) {
                WindowSwingUtils.closeWindow(waitingDialog);
            }
        }
    }

}
