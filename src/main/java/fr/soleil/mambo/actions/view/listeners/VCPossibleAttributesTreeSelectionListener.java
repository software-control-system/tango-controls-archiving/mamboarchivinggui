package fr.soleil.mambo.actions.view.listeners;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.components.AttributesTree;
import fr.soleil.mambo.models.VCPossibleAttributesTreeModel;

public class VCPossibleAttributesTreeSelectionListener implements TreeSelectionListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(VCPossibleAttributesTreeSelectionListener.class);

    private static VCPossibleAttributesTreeModel model;

    @Override
    public void valueChanged(final TreeSelectionEvent event) {
        final AttributesTree tree = (AttributesTree) event.getSource();
        final DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        model = (VCPossibleAttributesTreeModel) tree.getModel();
        if ((node != null) && node.isLeaf()) {
            try {
                query(node.toString(), node.getLevel(), model.isHistoric());
            } catch (final DevFailed | ArchivingException e) {
                LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            }
        }
    }

    // Called whenever each time a node needs to be rebuilt.
    // (A query is made to the database to the current node children)
    /**
     * @param st
     * @param level
     *            8 juil. 2005
     * @throws ArchivingException
     * @throws DevFailed
     */
    public void query(final String st, final int level, final Boolean historic) throws DevFailed, ArchivingException {
//        final boolean loadCompletely = true;
//        if (loadCompletely) {
//            return;
//        }
//
//        // my statement
//        if (level == 3) {
//            final DefaultMutableTreeNode parent = (DefaultMutableTreeNode) currentNode.getParent();
//            final DefaultMutableTreeNode grdparent = (DefaultMutableTreeNode) parent.getParent();
//
//            final String domain = grdparent.toString();
//            final String family = parent.toString();
//            final String member = currentNode.toString();
//
//            final IAttributeManager source = AttributeManagerFactory.getCurrentImpl();
//            // ArrayList attribute_list = getAttributes(device_name);
//            final Collection<String> attribute_list = source.getAttributes(domain, family, member, historic);
//
//            if (attribute_list != null) {
//                for (final String attributeName : attribute_list) {
//                    final DefaultMutableTreeNode attributeNode = new DefaultMutableTreeNode(attributeName);
//                    currentNode.add(attributeNode);
//
//                    final TreeNode[] path = new TreeNode[5];
//                    path[0] = (DefaultMutableTreeNode) model.getRoot();
//                    path[1] = grdparent;
//                    path[2] = parent;
//                    path[3] = currentNode;
//                    path[4] = attributeNode;
//                    final Attribute attribute = new Attribute(attributeName);
//                    //
//                    model.addAttribute(path, new ViewConfigurationAttribute(attribute));
//                }
//            }
//        }
    }
}
