// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/actions/view/MatchPossibleVCAttributesAction.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class MatchPossibleVCAttributesAction.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: MatchPossibleVCAttributesAction.java,v $
// Revision 1.5 2006/09/22 09:34:41 ounsy
// refactoring du package mambo.datasources.db
//
// Revision 1.4 2006/07/18 10:23:16 ounsy
// Less time consuming by setting tree expanding on demand only
//
// Revision 1.3 2006/05/19 15:03:05 ounsy
// minor changes
//
// Revision 1.2 2005/11/29 18:27:07 chinkumo
// no message
//
// Revision 1.1.2.2 2005/09/15 10:30:05 chinkumo
// Third commit !
//
// Revision 1.1.2.1 2005/09/14 15:40:16 chinkumo
// First commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.actions.view;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.actions.AMatchAttributesAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.view.VCPossibleAttributesTree;
import fr.soleil.mambo.containers.view.dialogs.AttributesTab;
import fr.soleil.mambo.datasources.db.attributes.AttributeManagerFactory;
import fr.soleil.mambo.datasources.db.attributes.IAttributeManager;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;
import fr.soleil.mambo.models.VCPossibleAttributesTreeModel;
import fr.soleil.mambo.tools.Messages;

public class MatchPossibleVCAttributesAction
        extends AMatchAttributesAction<VCPossibleAttributesTree, VCPossibleAttributesTreeModel, AttributesTab> {

    private static final long serialVersionUID = 2136748466969496424L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchPossibleVCAttributesAction.class);

    private static final String POSSIBLE_ATTRIBUTES_SEARCH_RUNNING = Messages
            .getMessage("VIEW_ACTION_REFRESH_POSSIBLE_BUTTON_SEARCHING");
    private static final String POSSIBLE_ATTRIBUTES_SEARCH_CANCELLED = Messages
            .getLogMessage("LOAD_POSSIBLE_VC_ATTRIBUTES_CANCELLED");
    private static final String POSSIBLE_ATTRIBUTES_SEARCH_OK = Messages
            .getLogMessage("LOAD_POSSIBLE_VC_ATTRIBUTES_OK");
    private static final String POSSIBLE_ATTRIBUTES_SEARCH_INTERRUPTED = Messages
            .getLogMessage("LOAD_POSSIBLE_ATTRIBUTES_INTERRUPTED");
    private static final String POSSIBLE_ATTRIBUTES_SEARCH_KO = Messages
            .getLogMessage("LOAD_POSSIBLE_VC_ATTRIBUTES_KO");

    private final ViewConfigurationBean viewConfigurationBean;
    private final AttributesTab attributesTab;

    /**
     * @param name
     */
    public MatchPossibleVCAttributesAction(String name, ViewConfigurationBean viewConfigurationBean,
            AttributesTab attributesTab) {
        super(name);
        this.viewConfigurationBean = viewConfigurationBean;
        this.attributesTab = attributesTab;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getSearchRunningMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_RUNNING;
    }

    @Override
    protected String getSearchCancelledMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_CANCELLED;
    }

    @Override
    protected String getSearchInterruptedMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_INTERRUPTED;
    }

    @Override
    protected String getSearchErrorMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_KO;
    }

    @Override
    protected String getSearchOkMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_OK;
    }

    @Override
    protected AttributesTab getAttributesTab() {
        return attributesTab;
    }

    @Override
    protected String getPatternFromAttributesTab(AttributesTab attributesTab) {
        return attributesTab.getLeftRegexp();
    }

    @Override
    protected VCPossibleAttributesTree recorverTree(AttributesTab attributesTab) {
        return attributesTab.getPossibleAttributesTree();
    }

    @Override
    protected VCPossibleAttributesTreeModel recoverModel(VCPossibleAttributesTree tree) {
        return viewConfigurationBean.getVcPossibleAttributesTreeModel();
    }

    @Override
    protected void prepareTreeModel(VCPossibleAttributesTreeModel model) {
        model.setRootName(model.isHistoric());
    }

    @Override
    protected List<Domain> loadDomains(String pattern, ICancelable cancelable) throws ArchivingException, DevFailed {
        Criterions criterions = ITangoManager.getAttributesSearchCriterions(pattern);
        IAttributeManager manager = AttributeManagerFactory.getCurrentImpl();
        return manager.loadDomains(criterions, viewConfigurationBean.getVcPossibleAttributesTreeModel().isHistoric(),
                false, cancelable);
    }

    @Override
    protected void applyDomains(List<Domain> domains, VCPossibleAttributesTree tree,
            VCPossibleAttributesTreeModel model) {
        model.applyDomains(domains);
        if (model.hasAttributes()) {
            // Fully expand tree if pattern was for attributes
            tree.expandAll(true);
        } else {
            // Expand 1 level
            tree.expandAll1Level(true);
        }
    }

    @Override
    protected void setEnabled(AttributesTab attributesTab, boolean enabled) {
        attributesTab.setPossibleAttributesSearchEnabled(enabled);
    }

    @Override
    protected TreeBuilder generateTreeBuilder(VCPossibleAttributesTree tree, VCPossibleAttributesTreeModel model,
            String pattern) {
        return new TreeBuilder(tree, model, pattern);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class TreeBuilder extends ATreeBuilder {

        public TreeBuilder(VCPossibleAttributesTree tree, VCPossibleAttributesTreeModel model, String pattern) {
            super(tree, model, pattern);
        }

    }

}
