package fr.soleil.mambo.actions.view.listeners;

import java.util.Collection;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.actions.view.dialogs.listeners.PlotPropertyComboListener;
import fr.soleil.mambo.components.view.ExpressionTree;
import fr.soleil.mambo.containers.view.dialogs.ExpressionTab;
import fr.soleil.mambo.data.view.ExpressionAttribute;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;

public class ExpressionTreeListener implements TreeSelectionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExpressionTreeListener.class);
    private final ExpressionTab expressionTab;

    public ExpressionTreeListener(final ExpressionTab expressionTab) {
        super();
        this.expressionTab = expressionTab;
    }

    @Override
    public void valueChanged(final TreeSelectionEvent event) {
        expressionTab.getExpressionTree().saveCurrentSelection();
        treeSelectionSave();
        try {
            treeSelectionChange();
        } catch (final ArchivingException e) {
            LOGGER.error("error", e);
        }
    }

    public void treeSelectionSave() {
        final ExpressionTree tree = expressionTab.getExpressionTree();
        final Collection<ExpressionAttribute> attributes = tree.getLastListOfAttributesToSet();
        if ((attributes != null) && (attributes.size() < 2)) {
            // AttributesPlotPropertiesPanel panel = expressionTab
            // .getPropertiesPanel();
            // PlotProperties plotProperties = panel.getPlotProperties();
            final PlotProperties plotProperties = expressionTab.getPlotProperties();
            final boolean hidden = expressionTab.isHidden();
            for (ExpressionAttribute attribute : attributes) {
                ViewConfigurationAttributePlotProperties properties = attribute.getProperties();
                properties.setViewType(plotProperties.getViewType());
                properties.setBar(plotProperties.getBar());
                properties.setCurve(plotProperties.getCurve());
                properties.setMarker(plotProperties.getMarker());
                properties.setTransform(plotProperties.getTransform());
                properties.setInterpolation(plotProperties.getInterpolation());
                properties.setSmoothing(plotProperties.getSmoothing());
                properties.setMath(plotProperties.getMath());
                properties.setHidden(hidden);
                attribute.setName(plotProperties.getCurve().getName());
            }
        }
    }

    public void treeSelectionChange() throws ArchivingException {

        final ExpressionTree tree = expressionTab.getExpressionTree();
        final Collection<ExpressionAttribute> attributes = tree.getSelectedAttributes();

        if ((attributes == null) || (attributes.size() == 0)) {
            expressionTab.setEnabledPropertiesPanel(false);
            expressionTab.getApplyAction().setEnabled(false);
            expressionTab.setEnableAddButton(true);
            expressionTab.setEnabledEditing(true);
        } else {

            expressionTab.setEnabledPropertiesPanel(!(attributes.size() >= 2));

            final DefaultComboBoxModel<String> axisChoiceComboModel = expressionTab.getAxisChoiceComboModel();
            final JComboBox<String> axisChoiceCombo = expressionTab.getAxisChoiceCombo();
            final PlotPropertyComboListener axisChoiceComboListener = expressionTab
                    .getPlotPropertyChoiceComboListener();
            final JTextField factorField = expressionTab.getFactorField();

            if (attributes.size() == 1) {// Select one attribut
                expressionTab.setEnabledEditing(true);
                axisChoiceCombo.removeActionListener(axisChoiceComboListener);
                if (axisChoiceComboModel.getSize() == 4) {
                    axisChoiceComboModel.removeElement("---");
                }
                axisChoiceCombo.addActionListener(axisChoiceComboListener);
                expressionTab.setParameters(expressionTab.getExpressionTree().getSelectedAttribute());
            }

            if (attributes.size() >= 2) {// Multi-Selection
                axisChoiceCombo.removeActionListener(axisChoiceComboListener);

                if (axisChoiceComboModel.getSize() == 3) {
                    axisChoiceComboModel.insertElementAt("---", 0);
                }
                axisChoiceComboModel.setSelectedItem("---");
                axisChoiceCombo.addActionListener(axisChoiceComboListener);

                factorField.setText(ObjectUtils.EMPTY_STRING);
                expressionTab.setEnabledPropertiesPanel(false);
                expressionTab.setEnabledEditing(false);
            }
        }
    }
}
