package fr.soleil.mambo.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.bean.manager.ViewConfigurationBeanManager;
import fr.soleil.mambo.components.DTPrinter;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.tools.Messages;

public class PrintAction extends AbstractAction {

    private static final long serialVersionUID = 5653644795266233576L;

    private static final Logger LOGGER = LoggerFactory.getLogger(PrintAction.class);
    public static final int NO_PRESET_TYPE = -1;
    public static final int AC_TYPE = 0;
    public static final int VC_TYPE = 1;

    private int type = NO_PRESET_TYPE;

    /**
     * @param name
     */
    public PrintAction(final String name, final Icon icon, final int _type) {
        super(name, icon);
        putValue(Action.NAME, name);
        type = _type;
    }

    @Override
    public void actionPerformed(final ActionEvent arg0) {
        int printType = -1;
        boolean treatEvent = true;
        if (type == NO_PRESET_TYPE) {
            final String printLabel = Messages.getMessage("MENU_PRINT");
            final String chooseLabel = Messages.getMessage("ACTION_PRINT_CHOOSE");
            final String currentACLabel = Messages.getMessage("ACTION_PRINT_CURRENT_AC");
            final String currentVCLabel = Messages.getMessage("ACTION_PRINT_CURRENT_VC");

            final String[] possibleValues = { currentACLabel, currentVCLabel };
            final String selectedValue = (String) JOptionPane.showInputDialog(null, chooseLabel, printLabel,
                    JOptionPane.INFORMATION_MESSAGE, null, possibleValues, possibleValues[0]);

            if (selectedValue == null) {
                treatEvent = false;
            } else {

                if (selectedValue.equals(possibleValues[0])) {
                    printType = AC_TYPE;
                } else if (selectedValue.equals(possibleValues[1])) {
                    printType = VC_TYPE;
                } else {
                    throw new IllegalStateException();
                }
            }
        } else {
            printType = type;
        }
        if (treatEvent) {
            String content = ObjectUtils.EMPTY_STRING;
            String title = ObjectUtils.EMPTY_STRING;
            String msg = ObjectUtils.EMPTY_STRING;

            try {
                switch (printType) {
                    case AC_TYPE:
                        final ArchivingConfiguration selectedArchivingConfiguration = ArchivingConfiguration
                                .getSelectedArchivingConfiguration();
                        if (selectedArchivingConfiguration != null) {
                            content = selectedArchivingConfiguration.toString();
                            title = Messages.getMessage("ACTION_PRINT_CURRENT_AC");
                        } else {
                            msg = Messages.getLogMessage("PRINT_ARCHIVING_CONFIGURATION_ACTION_NULL");
                            LOGGER.warn(msg);
                            treatEvent = false;
                        }

                        msg = Messages.getLogMessage("PRINT_ARCHIVING_CONFIGURATION_ACTION_OK");
                        break;

                    case VC_TYPE:
                        final ViewConfiguration selectedViewConfiguration = ViewConfigurationBeanManager.getInstance()
                                .getSelectedConfiguration();
                        if (selectedViewConfiguration != null) {
                            content = selectedViewConfiguration.toString();
                            title = Messages.getMessage("ACTION_PRINT_CURRENT_VC");
                        } else {
                            msg = Messages.getLogMessage("PRINT_VIEW_CONFIGURATION_ACTION_NULL");
                            LOGGER.warn(msg);
                            treatEvent = false;
                        }

                        msg = Messages.getLogMessage("PRINT_VIEW_CONFIGURATION_ACTION_OK");
                        break;
                }
                if (treatEvent) {
                    DTPrinter.printText(title, content);
                }
                LOGGER.debug(msg);
            } catch (final Exception e) {
                switch (printType) {
                    case AC_TYPE:
                        msg = Messages.getLogMessage("PRINT_ARCHIVING_CONFIGURATION_ACTION_KO");
                        break;

                    case VC_TYPE:
                        msg = Messages.getLogMessage("PRINT_VIEW_CONFIGURATION_ACTION_KO");
                        break;
                }

                LOGGER.error(msg, e);
            }
        }
    }
}
