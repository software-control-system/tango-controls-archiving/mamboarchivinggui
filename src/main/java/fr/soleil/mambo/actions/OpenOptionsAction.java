package fr.soleil.mambo.actions;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.mambo.containers.sub.dialogs.options.OptionsDialog;

public class OpenOptionsAction extends AbstractAction {

    private static final long serialVersionUID = -6533758230733770460L;

    public OpenOptionsAction(String name) {
        this.putValue(Action.NAME, name);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        OptionsDialog menuDialog = OptionsDialog.getInstance();
        if (!menuDialog.isVisible()) {
            menuDialog.pack();
            Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
            menuDialog.setSize(Math.min(menuDialog.getWidth() + 20, size.width - 50),
                    Math.min(menuDialog.getHeight(), size.height - 50));
            menuDialog.setLocationRelativeTo(menuDialog.getOwner());
            menuDialog.setVisible(true);
        }
    }

}
