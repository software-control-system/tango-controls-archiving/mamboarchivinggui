package fr.soleil.mambo.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;

import fr.soleil.mambo.containers.archiving.dialogs.ACEditDialog;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;

public class CancelAction extends AbstractAction {

    private static final long serialVersionUID = 1384495445229664451L;

    private final JDialog toClose;

    /**
     * @param name
     */
    public CancelAction(final String name, final JDialog toClose) {
        super(name);
        putValue(Action.NAME, name);
        this.toClose = toClose;
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        CancelAction.performCancel(toClose);
    }

    public static void performCancel(final JDialog toClose) {
        // nothing to do in case of VCEditDialog : everything is kept clear by ViewConfigurationBean
        if (toClose instanceof ACEditDialog) {
            ArchivingConfiguration selectedAC = ArchivingConfiguration.getSelectedArchivingConfiguration();
            if (selectedAC == null) {
                selectedAC = new ArchivingConfiguration();
            }
            selectedAC.pushLight();
        }

        toClose.setVisible(false);
    }
}
