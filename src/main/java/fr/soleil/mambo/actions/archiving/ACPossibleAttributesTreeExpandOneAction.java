/*
 * Created on Jun 28, 2006
 *
 */
package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.mambo.components.archiving.ACPossibleAttributesTree;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.tools.Messages;

/**
 * @author GIRARDOT
 */
public class ACPossibleAttributesTreeExpandOneAction extends AbstractAction {

    private static final long serialVersionUID = 6584158717483778763L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACPossibleAttributesTreeExpandOneAction.class);

    public ACPossibleAttributesTreeExpandOneAction() {
        super();
        this.putValue(Action.NAME, Messages.getMessage("ARCHIVING_ACTION_EXPAND_FIRST_POSSIBLE_BUTTON"));
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        WaitingDialog.openInstance();
        try {
            ACPossibleAttributesTree.getInstance().expandAll1Level(true);
        } catch (Throwable t) {
            LOGGER.error("Failed to expand tree", t);
        }
        WaitingDialog.closeInstance();
    }

}
