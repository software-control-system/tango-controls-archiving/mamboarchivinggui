//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/actions/archiving/LoadACAction.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  LoadACAction.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.3 $
//
// $Log: LoadACAction.java,v $
// Revision 1.3  2006/04/10 08:58:20  ounsy
// optimisation on loading an AC
//
// Revision 1.2  2005/11/29 18:27:07  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:20  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.actions.archiving;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.ExecutionException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.SwingWorker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.mambo.components.ConfigurationFileFilter;
import fr.soleil.mambo.components.archiving.ACFileFilter;
import fr.soleil.mambo.components.archiving.OpenedACComboBox;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.datasources.file.ArchivingConfigurationManagerFactory;
import fr.soleil.mambo.datasources.file.IArchivingConfigurationManager;
import fr.soleil.mambo.tools.Messages;

public class LoadACAction extends AbstractAction {

    private static final long serialVersionUID = 1760537800968225359L;

    private static final Logger LOGGER = LoggerFactory.getLogger(LoadACAction.class);

    private final boolean isDefault;

    /**
     * @param name
     */
    public LoadACAction(final String name, final boolean _isDefault) {
        super.putValue(Action.NAME, name);
        super.putValue(Action.SHORT_DESCRIPTION, name);

        isDefault = _isDefault;
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        final IArchivingConfigurationManager manager = ArchivingConfigurationManagerFactory.getCurrentImpl();
        String path = null;
        int returnVal = JFileChooser.APPROVE_OPTION;
        if (!isDefault) {
            // open file chooser
            final JFileChooser chooser = new JFileChooser();
            final ACFileFilter ACfilter = new ACFileFilter();
            chooser.addChoosableFileFilter(ACfilter);
            chooser.setCurrentDirectory(new File(manager.getDefaultSaveLocation()));
            returnVal = chooser.showOpenDialog(MamboFrame.getInstance());
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                final File f = chooser.getSelectedFile();
                if (f == null) {
                    returnVal = JFileChooser.CANCEL_OPTION;
                } else {
                    path = f.getAbsolutePath();
                    final String extension = ConfigurationFileFilter.getExtension(f);
                    final String expectedExtension = ACfilter.getExtension();
                    if (extension == null || !extension.equalsIgnoreCase(expectedExtension)) {
                        StringBuilder pathBuilder = new StringBuilder(path);
                        pathBuilder.append('.').append(expectedExtension);
                        path = pathBuilder.toString();
                        pathBuilder.delete(0, pathBuilder.length());
                    }
                }
            }
        }
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            final ProgressDialog dialog = new ProgressDialog(MamboFrame.getInstance());
            dialog.setTitle(Messages.getMessage("DIALOGS_WAITING_LOADING_TITLE"));
            dialog.setProgressIndeterminate(true);
            dialog.setProgress(0, dialog.getTitle(), Messages.getMessage("DIALOGS_WAITING_WAIT_TITLE"));
            dialog.pack();
            dialog.setLocationRelativeTo((Component) actionEvent.getSource());
            LoadWorker worker = new LoadWorker(manager, path, dialog);
            dialog.setCancelable((ICancelable) worker);
            dialog.setVisible(true);
            worker.execute();
        }
    }

    private void selectedLoadedAC(ArchivingConfiguration selectedArchivingConfiguration) {
        if (selectedArchivingConfiguration != null) {
            final OpenedACComboBox openedACComboBox = OpenedACComboBox.getInstance();
            if (openedACComboBox != null) {
                openedACComboBox.selectElement(selectedArchivingConfiguration);
            }
            final String msg = Messages.getLogMessage("LOAD_ARCHIVING_CONFIGURATION_ACTION_OK");
            LOGGER.debug(msg);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class LoadWorker extends SwingWorker<ArchivingConfiguration, Void> implements ICancelable {
        private volatile boolean canceled;
        private IArchivingConfigurationManager manager;
        private String path;
        private ProgressDialog dialog;

        public LoadWorker(final IArchivingConfigurationManager manager, final String path,
                final ProgressDialog dialog) {
            canceled = false;
            this.manager = manager;
            this.path = path;
            this.dialog = dialog;
        }

        private ArchivingConfiguration prepareACOnceThePathIsSet() {
            ArchivingConfiguration selectedArchivingConfiguration = null;
            if (!isCanceled()) {
                try {
                    selectedArchivingConfiguration = manager.loadArchivingConfiguration();
                    if (!isCanceled()) {
                        selectedArchivingConfiguration.setModified(false);
                        selectedArchivingConfiguration.setPath(manager.getSaveLocation());
                    }
                } catch (final FileNotFoundException fnfe) {
                    final String msg = Messages.getLogMessage("LOAD_ARCHIVING_CONFIGURATION_ACTION_WARNING");
                    LOGGER.warn(msg, fnfe);
                } catch (final Exception e) {
                    final String msg = Messages.getLogMessage("LOAD_ARCHIVING_CONFIGURATION_ACTION_KO");
                    LOGGER.error(msg, e);
                }
            }
            return selectedArchivingConfiguration;
        }

        @Override
        public void setCanceled(boolean canceled) {
            this.canceled = canceled;
        }

        @Override
        public boolean isCanceled() {
            return canceled;
        }

        @Override
        protected ArchivingConfiguration doInBackground() throws Exception {
            manager.setNonDefaultSaveLocation(path);
            ArchivingConfiguration ac = prepareACOnceThePathIsSet();
            return ac;
        }

        @Override
        protected void done() {
            try {
                if (!isCanceled()) {
                    ArchivingConfiguration ac = get();
                    if (!isCanceled()) {
                        selectedLoadedAC(ac);
                    }
                }
            } catch (InterruptedException e) {
                // nothing to do;
            } catch (ExecutionException e) {
                final String msg = Messages.getLogMessage("LOAD_ARCHIVING_CONFIGURATION_ACTION_KO");
                LOGGER.error(msg, e, e.getCause());
            } finally {
                dialog.setVisible(false);
            }
        }

        @Override
        protected void finalize() {
            manager = null;
            path = null;
            dialog = null;
        }
    }

}
