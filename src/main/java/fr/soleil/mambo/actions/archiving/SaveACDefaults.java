package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.mambo.components.ConfigurationFileFilter;
import fr.soleil.mambo.components.archiving.ACDefaultsFileFilter;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.options.manager.ACDefaultsManagerFactory;
import fr.soleil.mambo.options.manager.IACDefaultsManager;
import fr.soleil.mambo.options.sub.ACOptions;
import fr.soleil.mambo.tools.Messages;

public class SaveACDefaults extends AbstractAction {

    private static final long serialVersionUID = -7763441372091811121L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SaveACDefaults.class);

    public SaveACDefaults(final String name) {
        super.putValue(Action.NAME, name);
        super.putValue(Action.SHORT_DESCRIPTION, name);
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        boolean canSave = true;
        final IACDefaultsManager manager = ACDefaultsManagerFactory.getCurrentImpl();

        // open file chooser
        final JFileChooser chooser = new JFileChooser();
        final ACDefaultsFileFilter filter = new ACDefaultsFileFilter();
        chooser.addChoosableFileFilter(filter);
        chooser.setCurrentDirectory(new File(manager.getDefaultSaveLocation()));

        final int returnVal = chooser.showSaveDialog(MamboFrame.getInstance());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            final File f = chooser.getSelectedFile();
            String path = f.getAbsolutePath();

            if (f != null) {
                final String extension = ConfigurationFileFilter.getExtension(f);
                final String expectedExtension = filter.getExtension();

                if (extension == null || !extension.equalsIgnoreCase(expectedExtension)) {
                    path += ".";
                    path += expectedExtension;
                }
            }
            manager.setSaveLocation(path);
        } else {
            canSave = false;
        }
        if (canSave) {
            try {
                final ACOptions selectedACOptions = new ACOptions();
                selectedACOptions.fillFromOptionsDialog();
                manager.saveACDefaults(selectedACOptions);

                final String msg = Messages.getLogMessage("SAVE_ARCHIVING_CONFIGURATION_DEFAULTS_ACTION_OK");
                LOGGER.debug(msg);
            } catch (final FileNotFoundException fnfe) {
                final String msg = Messages.getLogMessage("SAVE_ARCHIVING_CONFIGURATION_DEFAULTS_ACTION_WARNING");
                LOGGER.warn(msg, fnfe);
            } catch (final Exception e) {
                final String msg = Messages.getLogMessage("SAVE_ARCHIVING_CONFIGURATION_DEFAULTS_ACTION_KO");
                LOGGER.error(msg, e);
            }
        }
    }
}
