/*
 * Created on Jun 28, 2006
 *
 */
package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.mambo.components.archiving.ACPossibleAttributesTree;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.tools.Messages;

/**
 * @author GIRARDOT
 */
public class ACPossibleAttributesTreeExpandSelectedAction extends AbstractAction {

    private static final long serialVersionUID = 6332603335822213634L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACPossibleAttributesTreeExpandSelectedAction.class);

    public ACPossibleAttributesTreeExpandSelectedAction() {
        super();
        this.putValue(Action.NAME, Messages.getMessage("ARCHIVING_ACTION_EXPAND_SELECTED_POSSIBLE_BUTTON"));
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        TreePath path = ACPossibleAttributesTree.getInstance().getSelectionPath();
        if (path != null) {
            WaitingDialog.openInstance();
            try {
                ACPossibleAttributesTree.getInstance().expandAll(path, true);
            } catch (Throwable t) {
                LOGGER.error("Failed to expand tree", t);
            }
            WaitingDialog.closeInstance();
        }
    }

}
