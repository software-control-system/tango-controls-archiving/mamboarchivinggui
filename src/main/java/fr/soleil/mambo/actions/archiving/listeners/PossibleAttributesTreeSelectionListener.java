package fr.soleil.mambo.actions.archiving.listeners;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.mambo.components.AttributesTree;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.models.ACPossibleAttributesTreeModel;

public class PossibleAttributesTreeSelectionListener implements TreeSelectionListener {

    private static DefaultMutableTreeNode currentNode;
    private static ACPossibleAttributesTreeModel model;

    @Override
    public void valueChanged(TreeSelectionEvent event) {
        AttributesTree tree = (AttributesTree) event.getSource();
        model = (ACPossibleAttributesTreeModel) tree.getModel();

        TreePath[] paths = event.getPaths();
        for (TreePath treePath : paths) {
            if (event.isAddedPath(treePath)) {
                DefaultMutableTreeNode node = ((DefaultMutableTreeNode) treePath.getLastPathComponent());
                if (node.isLeaf()) {
                    currentNode = node;
                    query(node.toString(), node.getLevel());
                }
            }
        }

    }

    // Called whenever each time a node needs to be rebuilt.
    // (A query is made to the database to the current node children)
    /**
     * @param st
     * @param level
     *            8 juil. 2005
     */
    private void query(String st, int level) {
        // my statement

        if (level == 3) {
            DefaultMutableTreeNode parent = ((DefaultMutableTreeNode) currentNode.getParent());
            DefaultMutableTreeNode grdparent = (DefaultMutableTreeNode) parent.getParent();

            String device_name = grdparent.toString() + TangoDeviceHelper.SLASH + parent.toString()
                    + TangoDeviceHelper.SLASH + currentNode.toString();

            ArrayList<String> attribute_list = dbGetAttributeList(device_name);
            if (attribute_list != null) {
                for (int i = 0; i < attribute_list.size(); i++) {
                    DefaultMutableTreeNode attributeNode = new DefaultMutableTreeNode(attribute_list.get(i));
                    currentNode.add(attributeNode);

                    TreeNode[] path = new TreeNode[5];
                    path[0] = (DefaultMutableTreeNode) model.getRoot();
                    path[1] = grdparent;
                    path[2] = parent;
                    path[3] = currentNode;
                    path[4] = attributeNode;
                    Attribute attribute = new Attribute(attribute_list.get(i));
                    //
                    model.addAttribute(path, new ArchivingConfigurationAttribute(attribute));
                }
            }
        }
    }

    /**
     * @param device_name
     * @return 8 juil. 2005
     */
    private ArrayList<String> dbGetAttributeList(String device_name) {
        ArrayList<String> liste_att = new ArrayList<>(32);

        try {
            DeviceProxy deviceProxy = new DeviceProxy(device_name);
            deviceProxy.ping();
            DeviceData device_data_argin = new DeviceData();
            String[] device_data_argout;
            device_data_argin.insert("*" + device_name + "*");
            device_data_argout = deviceProxy.get_attribute_list();

            for (int i = 0; i < device_data_argout.length; i++) {
                liste_att.add(device_data_argout[i]);
            }

            liste_att.trimToSize();
            Collections.sort(liste_att, Collator.getInstance());
            return liste_att;
        } catch (DevFailed e) {
            return null;
        }
    }
}
