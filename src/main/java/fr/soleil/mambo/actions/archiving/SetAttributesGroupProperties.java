package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;
import java.util.Collection;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.mambo.components.archiving.ACAttributesPropertiesTree;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesPropertiesPanel;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeHDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTTSProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributes;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;

public class SetAttributesGroupProperties extends AbstractAction {

    private static final long serialVersionUID = 7617727735791573522L;

    private static final Logger LOGGER = LoggerFactory.getLogger(SetAttributesGroupProperties.class);

    public SetAttributesGroupProperties(final String name) {
        putValue(Action.NAME, name);
        putValue(Action.SHORT_DESCRIPTION, name);
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        final ArchivingConfiguration currentAC = ArchivingConfiguration.getCurrentArchivingConfiguration();
        if (panel.verifyValues()) {
            boolean canSet = true;
            try {
                ArchivingConfiguration.updateCurrentModes(currentAC.isHistoric());
            } catch (final Exception e) {
                final String title = Messages
                        .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_PERIODIC_MODE_TITLE");
                final String msg = Messages
                        .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_PERIODIC_MODE_MESSAGE");
                JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);
            }

            long exportPeriod = 0;
            int hdbPeriod = 0, tdbPeriod = 0, ttsPeriod = 0;
            final String dedicatedArchiver = panel.getDedicatedArchiver();

            exportPeriod = panel.getExportPeriod();

            try {
                if (panel.isHistoric() == null) {
                    ttsPeriod = panel.getTTSperiod();
                }
            } catch (final Exception e) {
                final String msg = Messages.getLogMessage("SET_ATTRIBUTES_GROUP_INVALID_VALUES");
                LOGGER.warn(msg, e);

                panel.setTTSPeriod(Integer.parseInt(AttributesPropertiesPanel.DEFAULT_TTS_PERIOD_VALUE));

                canSet = false;
            }
            if (canSet) {
                try {
                    if ((panel.isHistoric() != null) && panel.isHistoric().booleanValue()) {
                        hdbPeriod = panel.getHDBperiod();
                    }
                } catch (final Exception e) {
                    final String msg = Messages.getLogMessage("SET_ATTRIBUTES_GROUP_INVALID_VALUES");
                    LOGGER.warn(msg, e);

                    panel.setHDBPeriod(Integer.parseInt(AttributesPropertiesPanel.DEFAULT_HDB_PERIOD_VALUE));

                    canSet = false;
                }
                if (canSet) {
                    try {
                        if ((panel.isHistoric() != null) && !panel.isHistoric().booleanValue()) {
                            tdbPeriod = panel.getTDBperiod();
                        }
                    } catch (final Exception e) {
                        final String msg = Messages.getLogMessage("SET_ATTRIBUTES_GROUP_INVALID_VALUES");
                        LOGGER.warn(msg, e);

                        panel.setTDBPeriod(Integer.parseInt(AttributesPropertiesPanel.DEFAULT_TDB_PERIOD_VALUE));

                        canSet = false;
                    }
                    if (canSet) {
                        final ArchivingConfigurationAttributeProperties currentProperties = ArchivingConfigurationAttributeProperties
                                .getCurrentProperties();

                        final ArchivingConfigurationAttributeHDBProperties currentHDBProperties = currentProperties
                                .getHDBProperties();
                        final ArchivingConfigurationAttributeTDBProperties currentTDBProperties = currentProperties
                                .getTDBProperties();
                        currentTDBProperties.setExportPeriod(exportPeriod);
                        final ArchivingConfigurationAttributeTTSProperties currentTTSProperties = currentProperties
                                .getTTSProperties();

                        final ACAttributesPropertiesTree tree = ACAttributesPropertiesTree.getInstance();
                        final Collection<ArchivingConfigurationAttribute> attributes = tree.getListOfAttributesToSet();
                        if (attributes != null) {
                            final ArchivingConfigurationAttributes currentACAttributes = currentAC.getAttributes();
                            for (ArchivingConfigurationAttribute attribute : attributes) {

                                final String name = attribute.getCompleteName();
                                attribute = currentACAttributes.getAttribute(name);
                                if (attribute == null) {
                                    attribute = new ArchivingConfigurationAttribute(currentACAttributes);
                                    attribute.setCompleteName(name);
                                }

                                if ((!currentHDBProperties.isEmpty()) || (!currentTDBProperties.isEmpty())
                                        || (!currentTTSProperties.isEmpty())) {
                                    if (currentAC.isHistoric() == null) {
                                        currentProperties
                                                .setHDBProperties(new ArchivingConfigurationAttributeHDBProperties());
                                        currentProperties
                                                .setTDBProperties(new ArchivingConfigurationAttributeTDBProperties());
                                    } else if (currentAC.isHistoric().booleanValue()) {
                                        currentProperties
                                                .setTTSProperties(new ArchivingConfigurationAttributeTTSProperties());
                                        currentProperties
                                                .setTDBProperties(new ArchivingConfigurationAttributeTDBProperties());
                                    } else {
                                        currentProperties
                                                .setTTSProperties(new ArchivingConfigurationAttributeTTSProperties());
                                        currentProperties
                                                .setHDBProperties(new ArchivingConfigurationAttributeHDBProperties());
                                    }
                                    ArchivingConfigurationAttributeProperties clone = currentProperties.clone();
                                    clone.setAttribute(attribute);
                                    attribute.addProperties(clone);
                                }

                                attribute.setHDBPeriod(hdbPeriod);
                                attribute.setTDBPeriod(tdbPeriod);
                                attribute.setTTSPeriod(ttsPeriod);
                                attribute.setExportPeriod(exportPeriod);
                                attribute.setDedicatedArchiver(currentAC.isHistoric(), dedicatedArchiver);

                                removeUnwantedProperties(attribute);
                                currentACAttributes.addAttribute(attribute);
                            } // end for (ArchivingConfigurationAttribute attribute : attributes)
                            tree.repaint();

                            ArchivingConfigurationAttributeProperties.resetCurrentProperties();
                        } // end if (attributes != null)
                    } // end if (canSet)
                } // end if (canSet)
            } // end if (canSet)
        } // end if (panel.verifyValues())
    }

    /**
     * @param next
     *            7 sept. 2005
     */
    private void removeUnwantedProperties(final ArchivingConfigurationAttribute next) {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();

        // HDB
        final boolean hasModeHDB_A = panel.hasModeHDB_A();
        next.removeProperty(hasModeHDB_A, ArchivingConfigurationMode.TYPE_A, Boolean.TRUE);
        final boolean hasModeHDB_R = panel.hasModeHDB_R();
        next.removeProperty(hasModeHDB_R, ArchivingConfigurationMode.TYPE_R, Boolean.TRUE);
        final boolean hasModeHDB_T = panel.hasModeHDB_T();
        next.removeProperty(hasModeHDB_T, ArchivingConfigurationMode.TYPE_T, Boolean.TRUE);
        final boolean hasModeHDB_D = panel.hasModeHDB_D();
        next.removeProperty(hasModeHDB_D, ArchivingConfigurationMode.TYPE_D, Boolean.TRUE);
        final boolean hasModeHDB_P = panel.hasModeHDB_P();
        next.removeProperty(hasModeHDB_P, ArchivingConfigurationMode.TYPE_P, Boolean.TRUE);

        // TDB
        final boolean hasModeTDB_A = panel.hasModeTDB_A();
        next.removeProperty(hasModeTDB_A, ArchivingConfigurationMode.TYPE_A, Boolean.FALSE);
        final boolean hasModeTDB_R = panel.hasModeTDB_R();
        next.removeProperty(hasModeTDB_R, ArchivingConfigurationMode.TYPE_R, Boolean.FALSE);
        final boolean hasModeTDB_T = panel.hasModeTDB_T();
        next.removeProperty(hasModeTDB_T, ArchivingConfigurationMode.TYPE_T, Boolean.FALSE);
        final boolean hasModeTDB_D = panel.hasModeTDB_D();
        next.removeProperty(hasModeTDB_D, ArchivingConfigurationMode.TYPE_D, Boolean.FALSE);
        final boolean hasModeTDB_P = panel.hasModeTDB_P();
        next.removeProperty(hasModeTDB_P, ArchivingConfigurationMode.TYPE_P, Boolean.FALSE);

        // TTS
        final boolean hasModeTTS_A = panel.hasModeTTS_A();
        next.removeProperty(hasModeTTS_A, ArchivingConfigurationMode.TYPE_A, null);
        final boolean hasModeTTS_R = panel.hasModeTTS_R();
        next.removeProperty(hasModeTTS_R, ArchivingConfigurationMode.TYPE_R, null);
        final boolean hasModeTTS_T = panel.hasModeTTS_T();
        next.removeProperty(hasModeTTS_T, ArchivingConfigurationMode.TYPE_T, null);
        final boolean hasModeTTS_D = panel.hasModeTTS_D();
        next.removeProperty(hasModeTTS_D, ArchivingConfigurationMode.TYPE_D, null);
        final boolean hasModeTTS_P = panel.hasModeTTS_P();
        next.removeProperty(hasModeTTS_P, ArchivingConfigurationMode.TYPE_P, null);
    }

}
