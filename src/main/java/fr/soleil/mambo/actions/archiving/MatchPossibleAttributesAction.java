//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/actions/archiving/MatchPossibleAttributesAction.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  MatchPossibleAttributesAction.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: MatchPossibleAttributesAction.java,v $
// Revision 1.5  2006/09/22 14:52:23  ounsy
// minor changes
//
// Revision 1.4  2006/09/20 12:49:40  ounsy
// changed imports
//
// Revision 1.3  2006/07/18 10:21:55  ounsy
// Less time consuming by setting tree expanding on demand only
//
// Revision 1.2  2005/11/29 18:27:07  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:20  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.actions.archiving;

import java.util.ArrayList;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.actions.AMatchAttributesAction;
import fr.soleil.mambo.components.archiving.ACPossibleAttributesTree;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesTab;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;
import fr.soleil.mambo.datasources.tango.standard.TangoManagerFactory;
import fr.soleil.mambo.models.ACPossibleAttributesTreeModel;
import fr.soleil.mambo.tools.Messages;

public class MatchPossibleAttributesAction
        extends AMatchAttributesAction<ACPossibleAttributesTree, ACPossibleAttributesTreeModel, AttributesTab> {

    private static final long serialVersionUID = 4200141450846576089L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchPossibleAttributesAction.class);

    private static final String POSSIBLE_ATTRIBUTES_SEARCH_RUNNING = Messages
            .getMessage("ARCHIVING_ACTION_REFRESH_POSSIBLE_BUTTON_SEARCHING");
    private static final String POSSIBLE_ATTRIBUTES_SEARCH_CANCELLED = Messages
            .getLogMessage("LOAD_POSSIBLE_ATTRIBUTES_CANCELLED");
    private static final String POSSIBLE_ATTRIBUTES_SEARCH_OK = Messages.getLogMessage("LOAD_POSSIBLE_ATTRIBUTES_OK");
    private static final String POSSIBLE_ATTRIBUTES_SEARCH_INTERRUPTED = Messages
            .getLogMessage("LOAD_POSSIBLE_ATTRIBUTES_INTERRUPTED");
    private static final String POSSIBLE_ATTRIBUTES_SEARCH_KO = Messages.getLogMessage("LOAD_POSSIBLE_ATTRIBUTES_KO");

    public MatchPossibleAttributesAction(final String name) {
        super(name);
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getSearchRunningMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_RUNNING;
    }

    @Override
    protected String getSearchCancelledMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_CANCELLED;
    }

    @Override
    protected String getSearchInterruptedMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_INTERRUPTED;
    }

    @Override
    protected String getSearchErrorMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_KO;
    }

    @Override
    protected String getSearchOkMessage() {
        return POSSIBLE_ATTRIBUTES_SEARCH_OK;
    }

    @Override
    protected AttributesTab getAttributesTab() {
        return AttributesTab.getInstance();
    }

    @Override
    protected String getPatternFromAttributesTab(AttributesTab attributesTab) {
        return attributesTab.getLeftRegexp();
    }

    @Override
    protected ACPossibleAttributesTree recorverTree(AttributesTab attributesTab) {
        return ACPossibleAttributesTree.getInstance();
    }

    @Override
    protected ACPossibleAttributesTreeModel recoverModel(ACPossibleAttributesTree tree) {
        return (ACPossibleAttributesTreeModel) tree.getModel();
    }

    @Override
    protected void prepareTreeModel(ACPossibleAttributesTreeModel model) {
        final DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        root.removeAllChildren();
        model.setRoot(root);
    }

    @Override
    protected List<Domain> loadDomains(String pattern, ICancelable cancelable) {
        final ITangoManager source = TangoManagerFactory.getCurrentImpl();
        List<Domain> domains;
        if (isCanceled()) {
            domains = new ArrayList<>();
        } else {
            domains = source.loadDomains(pattern, cancelable);
        }
        return domains;
    }

    @Override
    protected void applyDomains(List<Domain> domains, ACPossibleAttributesTree tree,
            ACPossibleAttributesTreeModel model) {
        model.build(domains);

        // Notify that the model has been reset
        model.reload((DefaultMutableTreeNode) model.getRoot());
        tree.setModel(model);

        if (model.hasAttributes()) {
            // Fully expand tree if pattern was for attributes
            tree.expandAll(true);
        } else {
            // Expand 1 level
            tree.expandAll1Level(true);
        }
    }

    @Override
    protected void setEnabled(AttributesTab attributesTab, boolean enabled) {
        attributesTab.setPossibleAttributesSearchEnabled(enabled);
    }

    @Override
    protected TreeBuilder generateTreeBuilder(ACPossibleAttributesTree tree, ACPossibleAttributesTreeModel model,
            String pattern) {
        return new TreeBuilder(tree, model, pattern);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class TreeBuilder extends ATreeBuilder {

        public TreeBuilder(ACPossibleAttributesTree tree, ACPossibleAttributesTreeModel model, String pattern) {
            super(tree, model, pattern);
        }

    }

}
