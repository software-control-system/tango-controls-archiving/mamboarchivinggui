/*
 * Created on Jun 28, 2006
 *
 */
package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.mambo.components.archiving.ACAttributesRecapTree;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.tools.Messages;

/**
 * @author GIRARDOT
 */
public class ACAttributesRecapTreeExpandAllAction extends AbstractAction {

    private static final long serialVersionUID = 5174486315068515842L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributesRecapTreeExpandAllAction.class);

    public ACAttributesRecapTreeExpandAllAction() {
        super();
        this.putValue(Action.NAME, Messages.getMessage("ARCHIVING_ACTION_EXPAND_ALL_RECAP_BUTTON"));
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        WaitingDialog.openInstance();
        try {
            ACAttributesRecapTree.getInstance().expandAll(true);
        } catch (Throwable t) {
            LOGGER.error("Failed to expand tree", t);
        }
        WaitingDialog.closeInstance();
    }

}
