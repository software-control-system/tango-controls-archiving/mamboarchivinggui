package fr.soleil.mambo.actions.archiving.listeners;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import fr.soleil.mambo.components.archiving.ACAttributesRecapTree;
import fr.soleil.mambo.containers.archiving.ACAttributeModesPanel;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributes;
import fr.soleil.mambo.models.AttributesTreeModel;

public class ACAttributesRecapTreeSelectionListener implements TreeSelectionListener {

    @Override
    public void valueChanged(TreeSelectionEvent event) {
        ACAttributesRecapTree tree = (ACAttributesRecapTree) event.getSource();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        if ((node != null) && (node.getLevel() == 4)) {
            TreeNode[] path = node.getPath();
            String completeName = AttributesTreeModel.translatePathIntoKey(path);
            ArchivingConfiguration selectedArchivingConfiguration = ArchivingConfiguration
                    .getSelectedArchivingConfiguration();
            if (selectedArchivingConfiguration != null) {
                ArchivingConfigurationAttributes attributes = selectedArchivingConfiguration.getAttributes();
                if (attributes != null) {
                    ArchivingConfigurationAttribute selectedAttribute = attributes.getAttribute(completeName);
                    if (selectedAttribute != null) {
                        // No need to refresh undisplayed panels
                        if (ACAttributeModesPanel.getInstance(Boolean.TRUE).isShowing()) {
                            ACAttributeModesPanel.getInstance(Boolean.TRUE).load(selectedAttribute);
                        }
                        if (ACAttributeModesPanel.getInstance(Boolean.FALSE).isShowing()) {
                            ACAttributeModesPanel.getInstance(Boolean.FALSE).load(selectedAttribute);
                        }
                        if (ACAttributeModesPanel.getInstance(null).isShowing()) {
                            ACAttributeModesPanel.getInstance(null).load(selectedAttribute);
                        }
                    }
                }
            }
        }
    }

}
