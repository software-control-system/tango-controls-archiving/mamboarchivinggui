/*	Synchrotron Soleil 
 *  
 *   File          :  ACRecapEditCopyAction.java
 *  
 *   Project       :  mambo
 *  
 *   Description   :  
 *  
 *   Author        :  SOLEIL
 *  
 *   Original      :  8 d�c. 2005 
 *  
 *   Revision:  					Author:  
 *   Date: 							State:  
 *  
 *   Log: ACRecapEditCopyAction.java,v 
 *
 */
package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.mambo.containers.archiving.dialogs.ACRecapEditCopyDialog;

/**
 * 
 * @author SOLEIL
 */
public class ACRecapEditCopyAction extends AbstractAction {

    private static final long serialVersionUID = 8873922225143475735L;

    private static ACRecapEditCopyAction instance = null;

    public static ACRecapEditCopyAction getInstance(String name) {
        if (instance == null) {
            instance = new ACRecapEditCopyAction(name);
        }

        return instance;
    }

    public static ACRecapEditCopyAction getInstance() {
        return instance;
    }

    private ACRecapEditCopyAction(String name) {
        super.putValue(Action.NAME, name);
        super.putValue(Action.SHORT_DESCRIPTION, name);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        new ACRecapEditCopyDialog().setVisible(true);
    }
}
