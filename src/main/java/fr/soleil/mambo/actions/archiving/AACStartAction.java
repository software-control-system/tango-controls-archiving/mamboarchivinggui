package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JOptionPane;

import org.slf4j.Logger;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.components.archiving.ACAttributesRecapTree;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.containers.view.ViewPanel;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationException;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.archiving.ACActionHelper;

/**
 * {@link AbstractAction} able to start some data in an {@link ArchivingConfiguration}.
 *
 * @param <S> The type of data to start.
 * 
 * @author GIRARDOT
 */
public abstract class AACStartAction<S> extends AbstractAction {

    private static final long serialVersionUID = -5260676816736217914L;

    protected AACStartAction(String name) {
        super(name);
    }

    protected AACStartAction(String name, Icon icon) {
        super(name, icon);
    }

    /**
     * Returns the {@link Logger} used by this {@link AACStartAction}.
     * 
     * @return A {@link Logger}.
     */
    protected abstract Logger getLogger();

    /**
     * Returns the data that can be started by this {@link AACStartAction}, knowing given
     * {@link ArchivingConfiguration}.
     * 
     * @param selectedArchivingConfiguration The {@link ArchivingConfiguration}.
     * @return The data to check and start.
     */
    protected abstract S getStartableData(ArchivingConfiguration selectedArchivingConfiguration);

    /**
     * Checks some data before starting it.
     * 
     * @param startable The data to check.
     * @throws ArchivingConfigurationException If a problem occurred.
     */
    protected abstract void checkData(S startable) throws ArchivingConfigurationException;

    /**
     * Starts some {@link ArchivingConfiguration} data, knowing given {@link IArchivingManager}.
     * 
     * @param manager The {@link IArchivingManager}.
     * @param selectedArchivingConfiguration The {@link ArchivingConfiguration} to start.
     * @param startableData The data to start.
     * @throws ArchivingException If a problem occurred.
     */
    protected abstract void startConfiguration(IArchivingManager manager,
            ArchivingConfiguration selectedArchivingConfiguration, S startableData) throws ArchivingException;

    @Override
    public void actionPerformed(ActionEvent e) {
        WaitingDialog.changeFirstMessage(Messages.getMessage("DIALOGS_WAITING_STARTING_TITLE"));
        WaitingDialog.openInstance();
        final IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
        final ArchivingConfiguration selectedArchivingConfiguration = ArchivingConfiguration
                .getSelectedArchivingConfiguration();
        S toControl = getStartableData(selectedArchivingConfiguration);
        boolean closeDialog = true;
        try {
            checkData(toControl);
        } catch (final ArchivingConfigurationException ace) {
            String msg = Messages.getLogMessage("START_ARCHIVING_ACTION_BAD_AC");

            final int cod = ace.getCode();
            final String attr = ace.getAttributeName();

            String errorCase = ObjectUtils.EMPTY_STRING;
            switch (cod) {
                case ArchivingConfigurationMode.PERIOD_TOO_LOW:
                    errorCase = Messages.getLogMessage("START_ARCHIVING_ACTION_PERIOD_TOO_LOW");
                    break;

                case ArchivingConfigurationAttributeTDBProperties.EXPORT_PERIOD_TOO_LOW:
                    errorCase = Messages.getLogMessage("START_ARCHIVING_ACTION_EXPORT_PERIOD_TOO_LOW");
                    break;

                case ArchivingConfigurationMode.MODE_A_VAL_INF_BIGGER_THAN_VAL_SUP:
                    errorCase = Messages.getLogMessage("START_ARCHIVING_ACTION_MODE_A_VAL_INF_BIGGER_THAN_VAL_SUP");
                    break;

                case ArchivingConfigurationMode.MODE_R_PERCENT_INF_BIGGER_THAN_PERCENT_SUP:
                    errorCase = Messages
                            .getLogMessage("START_ARCHIVING_ACTION_MODE_R_PERCENT_INF_BIGGER_THAN_PERCENT_SUP");
                    break;
            }

            msg += errorCase;
            if (attr != null) {
                msg += " ";
                msg += Messages.getLogMessage("START_ARCHIVING_ACTION_BAD_AC_ON_ATTRIBUTES");
                msg += attr;
            }

            getLogger().warn(msg, ace);
            WaitingDialog.closeInstance();
            JOptionPane.showMessageDialog(ACActionHelper.getJOptionPaneComponent(e), msg,
                    Messages.getMessage("STANDARD_MESSAGES_WARNING"), JOptionPane.WARNING_MESSAGE);
            closeDialog = false;
        }
        if (closeDialog) {
            try {
                startConfiguration(manager, selectedArchivingConfiguration, toControl);
                final String msg = Messages.getLogMessage("START_ARCHIVING_ACTION_OK");
                getLogger().debug(msg);
            } catch (final ArchivingException ae) {
                String fullMessage = ae.toString();
                String msg = ae.getMessage();
                if ((msg == null) || msg.trim().isEmpty()) {
                    msg = fullMessage;
                }
                getLogger().error(fullMessage, ae);
                WaitingDialog.closeInstance();
                JOptionPane.showMessageDialog(ACActionHelper.getJOptionPaneComponent(e), "Error at AC start:\n" + msg,
                        Messages.getMessage("STANDARD_MESSAGES_ERROR"), JOptionPane.ERROR_MESSAGE);
                closeDialog = false;
            } catch (final Throwable t) {
                final String msg = Messages.getLogMessage("START_ARCHIVING_ACTION_KO");
                getLogger().error(msg, t);
                WaitingDialog.closeInstance();
                JOptionPane.showMessageDialog(ACActionHelper.getJOptionPaneComponent(e),
                        "Unexpected error:\n" + t.getClass().getName() + ": " + t.getMessage() + "\n" + msg,
                        Messages.getMessage("STANDARD_MESSAGES_ERROR"), JOptionPane.ERROR_MESSAGE);
                closeDialog = false;
            } finally {
                if (closeDialog) {
                    if (ACAttributesRecapTree.getInstance() != null) {
                        ACAttributesRecapTree.getInstance().revalidate();
                        ACAttributesRecapTree.getInstance().repaint();
                    }
                    ViewPanel.getInstance().repaint();
                    WaitingDialog.closeInstance();
                }
                MamboFrame.getInstance().repaint();
            }
        } else {
            MamboFrame.getInstance().repaint();
        }
    }

}
