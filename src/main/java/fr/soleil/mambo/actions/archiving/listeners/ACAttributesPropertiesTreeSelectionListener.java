package fr.soleil.mambo.actions.archiving.listeners;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.archiving.IArchiver;
import fr.soleil.mambo.components.archiving.ACAttributesPropertiesTree;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesPropertiesPanel;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeHDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTTSProperties;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;
import fr.soleil.mambo.models.AttributesTreeModel;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.sub.ACOptions;

public class ACAttributesPropertiesTreeSelectionListener implements TreeSelectionListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ACAttributesPropertiesTreeSelectionListener.class);

    @Override
    public void valueChanged(TreeSelectionEvent event) {
        ACAttributesPropertiesTree tree = (ACAttributesPropertiesTree) event.getSource();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        if (node != null) {
            if (node.isLeaf()) {
                TreeNode[] path = node.getPath();
                String completeName = AttributesTreeModel.translatePathIntoKey(path);
                ArchivingConfiguration currentArchivingConfiguration = ArchivingConfiguration
                        .getCurrentArchivingConfiguration();
                ArchivingConfigurationAttribute selectedAttribute = currentArchivingConfiguration.getAttributes()
                        .getAttribute(completeName);

                ArchivingConfigurationAttributeHDBProperties HDBProperties = new ArchivingConfigurationAttributeHDBProperties();
                ArchivingConfigurationAttributeTDBProperties TDBProperties = new ArchivingConfigurationAttributeTDBProperties();
                ArchivingConfigurationAttributeTTSProperties TTSProperties = new ArchivingConfigurationAttributeTTSProperties();

                boolean theSelectedAttributeBelongsToTheCurrentAC = false;
                if (selectedAttribute == null) {
                    theSelectedAttributeBelongsToTheCurrentAC = true;
                } else {
                    String selectedAttributeName = selectedAttribute.getCompleteName();
                    if (!currentArchivingConfiguration.containsAttribute(selectedAttributeName)) {
                        theSelectedAttributeBelongsToTheCurrentAC = true;
                    } else if (currentArchivingConfiguration.getAttributes().getAttribute(selectedAttributeName)
                            .isEmpty()) {
                        theSelectedAttributeBelongsToTheCurrentAC = true;
                    }
                }

                if (theSelectedAttributeBelongsToTheCurrentAC) {
                    // we reset the fields to defaults
                    applyDefaults(completeName, true);
                } else {
                    ArchivingConfigurationAttributeProperties properties = selectedAttribute.getProperties();
                    Boolean historic = currentArchivingConfiguration.isHistoric();
                    if (historic == null) {
                        TTSProperties = properties.getTTSProperties();
                        TTSProperties.push();
                    } else if (historic.booleanValue()) {
                        HDBProperties = properties.getHDBProperties();
                        HDBProperties.push();
                    } else {
                        TDBProperties = properties.getTDBProperties();
                        TDBProperties.push();
                    }
                }
            } else {
                applyDefaults(ObjectUtils.EMPTY_STRING, false);
            }
        }
    }

    /**
     * @param completeName
     * @param isLeafNode
     * 
     */
    private void applyDefaults(String completeName, boolean isLeafNode) {
        Options options = Options.getInstance();
        ACOptions acOptions = options.getAcOptions();
        try {
            acOptions.applyDefaults();
        } catch (Exception e) {
            // if there is a problem, we push empty properties (fields are reset)
            ArchivingConfigurationAttributeHDBProperties hdbProperties = new ArchivingConfigurationAttributeHDBProperties();
            ArchivingConfigurationAttributeTDBProperties tdbProperties = new ArchivingConfigurationAttributeTDBProperties();
            ArchivingConfigurationAttributeTTSProperties ttsProperties = new ArchivingConfigurationAttributeTTSProperties();

            hdbProperties.push();
            tdbProperties.push();
            ttsProperties.push();
        }

        AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        panel.setCurrentlySelectedAttribute(completeName);

        if (isLeafNode) {
            try {
                IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
                Boolean historic = panel.isHistoric();
                IArchiver currentArchiver = manager.getCurrentArchiverForAttribute(completeName, historic);
                panel.pushCurrentArchiver(currentArchiver, completeName);
                panel.pushDedicatedArchiver(completeName);
            } catch (Exception e) {
                LOGGER.error("Failed to apply archiver to " + completeName, e);
            }
        }
    }

}
