//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/actions/archiving/RemoveSelectedACAttributesAction.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  RemoveSelectedACAttributesAction.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: RemoveSelectedACAttributesAction.java,v $
// Revision 1.4  2006/08/23 10:01:32  ounsy
// some optimizations with less tree model reloading
//
// Revision 1.3  2006/08/07 13:03:07  ounsy
// trees and lists sort
//
// Revision 1.2  2005/11/29 18:27:07  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:20  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.tree.TreePath;

import fr.soleil.mambo.components.archiving.ACAttributesPropertiesTree;
import fr.soleil.mambo.components.archiving.ACAttributesRecapTree;
import fr.soleil.mambo.components.archiving.ACAttributesSelectTree;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.models.ACAttributesTreeModel;

public class RemoveSelectedACAttributesAction extends AbstractAction {

    private static final long serialVersionUID = 8107504952660243887L;

    /**
     * @param name
     */
    public RemoveSelectedACAttributesAction(final String name) {
        putValue(Action.NAME, name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(final ActionEvent event) {
        final ACAttributesSelectTree rightTree = ACAttributesSelectTree.getInstance();
        final List<TreePath> listToRemove = rightTree.getListOfAttributesTreePathUnderSelectedNodes(true);
        if (listToRemove != null) {
            final ACAttributesTreeModel model = (ACAttributesTreeModel) rightTree.getModel();
            if (listToRemove.size() != 0) {
                model.removeSelectedAttributes(listToRemove);

                rightTree.revalidate();
                rightTree.updateUI();
                rightTree.repaint();

                final Map<String, ArchivingConfigurationAttribute> attrs = model.getAttributesCopy();
                if (attrs != null) {
                    final ArchivingConfiguration currentArchivingConfiguration = ArchivingConfiguration
                            .getCurrentArchivingConfiguration();
                    currentArchivingConfiguration.getAttributes().removeAttributesNotInList(attrs);
                    attrs.clear();
                }

                final ACAttributesPropertiesTree propTree = ACAttributesPropertiesTree.getInstance();
                if (propTree != null) {
                    propTree.revalidate();
                    propTree.updateUI();
                    propTree.repaint();
                }
                final ACAttributesRecapTree recapTree = ACAttributesRecapTree.getInstance();
                if (recapTree != null) {
                    recapTree.revalidate();
                    recapTree.updateUI();
                    recapTree.repaint();
                }
            }
        }
    }

}
