package fr.soleil.mambo.actions.archiving.listeners;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import fr.soleil.mambo.data.archiving.ArchivingConfiguration;

/**
 * 
 *  
 */
public class OpenedACItemListener implements ItemListener {

    public OpenedACItemListener() {

    }

    @Override
    public void itemStateChanged(ItemEvent event) {
        ArchivingConfiguration selected = (ArchivingConfiguration) event.getItem();
        if (selected != null) {
            selected.push();
        }
    }

}
