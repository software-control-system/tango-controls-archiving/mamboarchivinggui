package fr.soleil.mambo.actions.archiving;

import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.lib.project.swing.icons.DecorableIcon;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationException;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.archiving.ACActionHelper;

public class StartSelectedACAttributesAction extends AACStartAction<ArchivingConfigurationAttribute[]> {

    private static final long serialVersionUID = 9215039671139352982L;

    protected static final DecorableIcon ICON = new DecorableIcon(Mambo.class.getResource("icons/Play.gif"));
    private static final Logger LOGGER = LoggerFactory.getLogger(StartSelectedACAttributesAction.class);
    private static final StartSelectedACAttributesAction INSTANCE = new StartSelectedACAttributesAction();

    static {
        ICON.setDecoration(new ImageIcon(Mambo.class.getResource("icons/bullet_check.png")));
        ICON.setDecorationHorizontalAlignment(DecorableIcon.RIGHT);
        ICON.setDecorationVerticalAlignment(DecorableIcon.BOTTOM);
        ICON.setDecorated(true);
    }

    public static StartSelectedACAttributesAction getInstance() {
        return INSTANCE;
    }

    private StartSelectedACAttributesAction() {
        super(Messages.getMessage("ARCHIVING_ACTION_START_SELECTED_BUTTON"), ICON);
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected ArchivingConfigurationAttribute[] getStartableData(
            ArchivingConfiguration selectedArchivingConfiguration) {
        return ACActionHelper.getSelectedAttributs(selectedArchivingConfiguration);
    }

    @Override
    protected void checkData(ArchivingConfigurationAttribute[] attributes) throws ArchivingConfigurationException {
        if (attributes != null) {
            for (ArchivingConfigurationAttribute attribute : attributes) {
                if (attribute != null) {
                    attribute.controlValues();
                }
            }
        }
    }

    @Override
    protected void startConfiguration(IArchivingManager manager, ArchivingConfiguration selectedArchivingConfiguration,
            ArchivingConfigurationAttribute[] attributes) throws ArchivingException {
        manager.startArchiving(selectedArchivingConfiguration, attributes);
    }

}
