package fr.soleil.mambo.actions.archiving;

import javax.swing.ImageIcon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.lib.project.swing.icons.DecorableIcon;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.archiving.ACActionHelper;

public class StopSelectedACAttributesAction extends AACStopAction<ArchivingConfigurationAttribute[]> {

    private static final long serialVersionUID = -6771759685602438300L;

    private static final DecorableIcon ICON = new DecorableIcon(Mambo.class.getResource("icons/Stop.gif"));
    private static final Logger LOGGER = LoggerFactory.getLogger(StopSelectedACAttributesAction.class);
    private static final StopSelectedACAttributesAction INSTANCE = new StopSelectedACAttributesAction();

    static {
        ICON.setDecoration(new ImageIcon(Mambo.class.getResource("icons/bullet_check.png")));
        ICON.setDecorationHorizontalAlignment(DecorableIcon.RIGHT);
        ICON.setDecorationVerticalAlignment(DecorableIcon.BOTTOM);
        ICON.setDecorated(true);
    }

    public static StopSelectedACAttributesAction getInstance() {
        return INSTANCE;
    }

    private StopSelectedACAttributesAction() {
        super(Messages.getMessage("ARCHIVING_ACTION_STOP_SELECTED_BUTTON"), ICON);
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected ArchivingConfigurationAttribute[] getStoppableData(
            ArchivingConfiguration selectedArchivingConfiguration) {
        return ACActionHelper.getSelectedAttributs(selectedArchivingConfiguration);
    }

    @Override
    protected void stopConfiguration(IArchivingManager manager, ArchivingConfiguration selectedArchivingConfiguration,
            ArchivingConfigurationAttribute[] attributes) throws ArchivingException {
        manager.stopArchiving(selectedArchivingConfiguration, attributes);
    }

}
