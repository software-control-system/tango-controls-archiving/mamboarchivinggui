//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/actions/archiving/ACFinishAction.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ValidateACEditAction.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: ACFinishAction.java,v $
// Revision 1.5  2006/08/07 13:03:07  ounsy
// trees and lists sort
//
// Revision 1.4  2006/05/19 14:58:40  ounsy
// minor changes
//
// Revision 1.3  2006/02/24 12:15:44  ounsy
// small modifications
//
// Revision 1.2  2005/12/15 10:40:45  ounsy
// avoiding a null pointer exception
//
// Revision 1.1  2005/11/29 18:27:07  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:20  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.containers.archiving.dialogs.ACEditDialog;
import fr.soleil.mambo.containers.archiving.dialogs.GeneralTab;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.models.ACAttributesTreeModel;
import fr.soleil.mambo.tools.Messages;

public class ACFinishAction extends AbstractAction {

    private static final long serialVersionUID = -3563636954207911910L;

    private static ACFinishAction instance = null;

    public static ACFinishAction getInstance(final String name) {
        if (instance == null) {
            instance = new ACFinishAction(name);
        }

        return instance;
    }

    public static ACFinishAction getInstance() {
        return instance;
    }

    private ACFinishAction(final String name) {
        putValue(Action.NAME, name);
        putValue(Action.SHORT_DESCRIPTION, name);
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        if (verifyAC()) {
            final ACEditDialog dialog = ACEditDialog.getInstance();
            dialog.setVisible(false);

            final ArchivingConfiguration currentAC = ArchivingConfiguration.getCurrentArchivingConfiguration();
            if (currentAC.isNew()) {
                currentAC.setCreationDate(GUIUtilities.now());
            }

            currentAC.setLastUpdateDate(GUIUtilities.now());

            final GeneralTab generalTab = GeneralTab.getInstance();
            currentAC.setName(generalTab.getName());
            currentAC.setModified(true);

            currentAC.push();
        }
    }

    /**
     * @return
     */
    public boolean verifyAC() {
        return verifyAttributesAreSet();
    }

    /**
     * @return
     */
    private boolean verifyAttributesAreSet() {
        if (containsNonSetAttributes()) {
            final String msgTitle = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_NON_SET_CONFIRM_TITLE");
            String msgConfirm = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_NON_SET_CONFIRM_LABEL_1");
            msgConfirm += GUIUtilities.CRLF;
            msgConfirm += Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_NON_SET_CONFIRM_LABEL_2");
            final String msgCancel = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_NON_SET_CONFIRM_CANCEL");
            final String msgValidate = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_NON_SET_CONFIRM_VALIDATE");
            final Object[] options = { msgValidate, msgCancel };

            final int confirm = JOptionPane.showOptionDialog(null, msgConfirm, msgTitle, JOptionPane.DEFAULT_OPTION,
                    JOptionPane.WARNING_MESSAGE, null, options, options[0]);
            return confirm == JOptionPane.OK_OPTION;
        } else {
            return true;
        }
    }

    private boolean containsNonSetAttributes() {
        boolean containsUnset = false;
        final ACAttributesTreeModel model = ACAttributesTreeModel.getInstance();
        final Map<String, ArchivingConfigurationAttribute> htAttr = model.getAttributesCopy();
        if (htAttr != null) {
            for (Entry<String, ArchivingConfigurationAttribute> entry : htAttr.entrySet()) {
                String nextKey = entry.getKey();
                ArchivingConfigurationAttribute nextValue = entry.getValue();

                final ArchivingConfiguration currentAC = ArchivingConfiguration.getCurrentArchivingConfiguration();
                if (!currentAC.containsAttribute(nextKey)) {
                    containsUnset = true;
                    break;
                }
                nextValue = currentAC.getAttribute(nextKey);

                if (nextValue.isEmpty()) {
                    containsUnset = true;
                    break;
                }
            }
            htAttr.clear();
        }
        return containsUnset;
    }

}
