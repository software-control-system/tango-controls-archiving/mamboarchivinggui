package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JOptionPane;

import org.slf4j.Logger;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.AttributesArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.FaultingAttribute;
import fr.soleil.mambo.components.archiving.ACAttributesRecapTree;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.containers.view.ViewPanel;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.archiving.ACActionHelper;

/**
 * {@link AbstractAction} able to stop some data in an {@link ArchivingConfiguration}.
 *
 * @param <S> The particular data to stop in {@link ArchivingConfiguration}.*
 * 
 * @author GIRARDOT
 */
public abstract class AACStopAction<S> extends AbstractAction {

    private static final long serialVersionUID = -3516584434654259484L;

    protected AACStopAction(String name) {
        super(name);
    }

    protected AACStopAction(String name, Icon icon) {
        super(name, icon);
    }

    /**
     * Returns the {@link Logger} used by this {@link AACStartAction}.
     * 
     * @return A {@link Logger}.
     */
    protected abstract Logger getLogger();

    /**
     * Returns the data to stop knowing given {@link ArchivingConfiguration}.
     * 
     * @param selectedArchivingConfiguration The {@link ArchivingConfiguration}.
     * @return The data to stop.
     */
    protected abstract S getStoppableData(ArchivingConfiguration selectedArchivingConfiguration);

    /**
     * Stops some data in {@link ArchivingConfiguration}, knowing an {@link IArchivingManager}.
     * 
     * @param manager The {@link IArchivingManager}.
     * @param selectedArchivingConfiguration The {@link ArchivingConfiguration}.
     * @param dataToStop The data to stop.
     * @throws ArchivingException If a problem occurred.
     */
    protected abstract void stopConfiguration(IArchivingManager manager,
            ArchivingConfiguration selectedArchivingConfiguration, S dataToStop) throws ArchivingException;

    @Override
    public void actionPerformed(ActionEvent e) {
        WaitingDialog.changeFirstMessage(Messages.getMessage("DIALOGS_WAITING_STOPPING_TITLE"));
        WaitingDialog.openInstance();
        final IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
        final ArchivingConfiguration selectedArchivingConfiguration = ArchivingConfiguration
                .getSelectedArchivingConfiguration();
        S dataToStop = getStoppableData(selectedArchivingConfiguration);
        boolean closeDialog = true;
        try {
            stopConfiguration(manager, selectedArchivingConfiguration, dataToStop);
            final String msg = Messages.getLogMessage("STOP_ARCHIVING_ACTION_OK");
            getLogger().debug(msg);
        } catch (final ArchivingException ae) {
            String msg;

            msg = Messages.getLogMessage("STOP_ARCHIVING_ACTION_KO");
            getLogger().error(msg, ae);

            if (ae instanceof AttributesArchivingException) {
                final AttributesArchivingException aae = (AttributesArchivingException) ae;
                int i = 0;
                for (final FaultingAttribute faultingAttribute : aae.getFaultingAttributes()) {
                    final StringBuilder buffer = new StringBuilder("\t");
                    buffer.append(Messages.getLogMessage("STOP_ARCHIVING_ACTION_EXCEPTION_TRACE_MESSAGE_ATTRIBUTE"));
                    buffer.append(++i).append(":");
                    buffer.append("\n\t\t").append(
                            Messages.getLogMessage("STOP_ARCHIVING_ACTION_EXCEPTION_TRACE_MESSAGE_ATTRIBUTE_NAME"));
                    buffer.append(faultingAttribute.getName());
                    buffer.append("\n\t\t").append(
                            Messages.getLogMessage("STOP_ARCHIVING_ACTION_EXCEPTION_TRACE_MESSAGE_ATTRIBUTE_REASON"));
                    buffer.append(faultingAttribute.getCause());
                    getLogger().error(buffer.toString());
                }
            }
            WaitingDialog.closeInstance();
            JOptionPane.showMessageDialog(ACActionHelper.getJOptionPaneComponent(e), msg,
                    Messages.getMessage("STANDARD_MESSAGES_ERROR"), JOptionPane.ERROR_MESSAGE);
            closeDialog = false;
        } finally {
            if (closeDialog) {
                if (ACAttributesRecapTree.getInstance() != null) {
                    ACAttributesRecapTree.getInstance().revalidate();
                    ACAttributesRecapTree.getInstance().repaint();
                }

                ViewPanel.getInstance().repaint();
                WaitingDialog.closeInstance();
            }
            MamboFrame.getInstance().repaint();
        }
    }

}
