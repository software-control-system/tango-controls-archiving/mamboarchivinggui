//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/actions/archiving/MatchACAttributesAction.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  MatchACAttributesAction.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: MatchACAttributesAction.java,v $
// Revision 1.5  2006/09/20 12:49:40  ounsy
// changed imports
//
// Revision 1.4  2006/08/07 13:03:07  ounsy
// trees and lists sort
//
// Revision 1.3  2006/07/18 10:21:55  ounsy
// Less time consuming by setting tree expanding on demand only
//
// Revision 1.2  2005/11/29 18:27:07  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:20  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.actions.archiving;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.actions.AMatchAttributesAction;
import fr.soleil.mambo.components.archiving.ACAttributesSelectTree;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesTab;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;
import fr.soleil.mambo.datasources.tango.standard.TangoManagerFactory;
import fr.soleil.mambo.models.ACAttributesTreeModel;
import fr.soleil.mambo.tools.Messages;

public class MatchACAttributesAction
        extends AMatchAttributesAction<ACAttributesSelectTree, ACAttributesTreeModel, AttributesTab> {

    private static final long serialVersionUID = 1473829832704177781L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchACAttributesAction.class);

    private static final String AC_ATTRIBUTES_SEARCH_RUNNING = Messages
            .getMessage("ARCHIVING_ACTION_REFRESH_SELECT_BUTTON_SEARCHING");
    private static final String AC_ATTRIBUTES_SEARCH_CANCELLED = Messages
            .getLogMessage("LOAD_ARCHIVING_ATTRIBUTES_CANCELLED");
    private static final String AC_ATTRIBUTES_SEARCH_OK = Messages.getLogMessage("LOAD_ARCHIVING_ATTRIBUTES_OK");
    private static final String AC_ATTRIBUTES_SEARCH_INTERRUPTED = Messages
            .getLogMessage("LOAD_ARCHIVING_ATTRIBUTES_INTERRUPTED");
    private static final String AC_ATTRIBUTES_SEARCH_KO = Messages.getLogMessage("LOAD_ARCHIVING_ATTRIBUTES_KO");

    private final AttributesTab attributesTab;

    /**
     * @param name
     */
    public MatchACAttributesAction(final String name, final AttributesTab attributesTab) {
        super(name);
        this.attributesTab = attributesTab;
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @Override
    protected String getSearchRunningMessage() {
        return AC_ATTRIBUTES_SEARCH_RUNNING;
    }

    @Override
    protected String getSearchCancelledMessage() {
        return AC_ATTRIBUTES_SEARCH_CANCELLED;
    }

    @Override
    protected String getSearchInterruptedMessage() {
        return AC_ATTRIBUTES_SEARCH_INTERRUPTED;
    }

    @Override
    protected String getSearchErrorMessage() {
        return AC_ATTRIBUTES_SEARCH_KO;
    }

    @Override
    protected String getSearchOkMessage() {
        return AC_ATTRIBUTES_SEARCH_OK;
    }

    @Override
    protected AttributesTab getAttributesTab() {
        return attributesTab;
    }

    @Override
    protected String getPatternFromAttributesTab(AttributesTab attributesTab) {
        return attributesTab.getRightRegexp();
    }

    @Override
    protected ACAttributesSelectTree recorverTree(AttributesTab attributesTab) {
        return ACAttributesSelectTree.getInstance();
    }

    @Override
    protected ACAttributesTreeModel recoverModel(ACAttributesSelectTree tree) {
        return ACAttributesTreeModel.getInstance();
    }

    @Override
    protected void prepareTreeModel(ACAttributesTreeModel model) {
        // nothing to do
    }

    @Override
    protected List<Domain> loadDomains(String pattern, ICancelable cancelable) {
        final ITangoManager manager = TangoManagerFactory.getCurrentImpl();
        return manager.loadDomains(pattern, cancelable);
    }

    @Override
    protected void applyDomains(List<Domain> domains, ACAttributesSelectTree tree, ACAttributesTreeModel model) {
        model.applyDomains(domains);
        final Map<String, ArchivingConfigurationAttribute> attrs = model.getAttributesCopy();
        if (attrs != null) {
            final ArchivingConfiguration currentArchivingConfiguration = ArchivingConfiguration
                    .getCurrentArchivingConfiguration();
            currentArchivingConfiguration.getAttributes().removeAttributesNotInList(attrs);
            attrs.clear();
        }
        tree.expandAll1Level(true);
    }

    @Override
    protected void setEnabled(AttributesTab attributesTab, boolean enabled) {
        attributesTab.setACAttributesSearchEnabled(enabled);
    }

    @Override
    protected ACTreeBuilder generateTreeBuilder(ACAttributesSelectTree tree, ACAttributesTreeModel model,
            String pattern) {
        return new ACTreeBuilder(tree, model, pattern);
    }

//    @Override
//    public void actionPerformed(ActionEvent evt) {
//        final String pattern = attributesTab.getRightRegexp();
//
//        final ACAttributesTreeModel model = ACAttributesTreeModel.getInstance();
//        final ITangoManager manager = TangoManagerFactory.getCurrentImpl();
//
//        model.match(manager, pattern);
//
//        // We expand the results from 1 level of depth
//        ACAttributesSelectTree tree = ACAttributesSelectTree.getInstance();
//        tree.expandAll1Level(true);
//    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ACTreeBuilder extends ATreeBuilder {

        public ACTreeBuilder(ACAttributesSelectTree tree, ACAttributesTreeModel model, String pattern) {
            super(tree, model, pattern);
        }

    }

}
