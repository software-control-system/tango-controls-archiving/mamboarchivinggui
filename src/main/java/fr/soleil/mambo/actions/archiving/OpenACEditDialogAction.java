package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.mambo.components.archiving.OpenedACComboBox;
import fr.soleil.mambo.containers.archiving.dialogs.ACEditChooseDialog;
import fr.soleil.mambo.containers.archiving.dialogs.ACEditDialog;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesPropertiesPanel;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.sub.ACOptions;

public class OpenACEditDialogAction extends AbstractAction {

    private static final long serialVersionUID = -4994209550093611623L;

    private static final Logger LOGGER = LoggerFactory.getLogger(OpenACEditDialogAction.class);
    private final boolean isNew;
    private boolean alternateSelectionMode;
    private Boolean historic;

    /**
     * @param name
     */
    public OpenACEditDialogAction(final String name, final boolean isNew, final boolean alternateSelectionMode) {
        super.putValue(Action.NAME, name);
        super.putValue(Action.SHORT_DESCRIPTION, name);

        this.isNew = isNew;
        this.alternateSelectionMode = alternateSelectionMode;
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        final ArchivingConfiguration selectedArchivingConfiguration = ArchivingConfiguration
                .getSelectedArchivingConfiguration();
        final boolean needsNewAc = isNew || selectedArchivingConfiguration == null;
        boolean needsStackCleaning = false;

        WaitingDialog.openInstance();
        boolean treatEvent = true;
        // 1st, test whether it is an historical AC
        if (needsNewAc) {
            ACEditChooseDialog dialog = ACEditChooseDialog.getInstance();
            if (dialog.hasOnlyOneChoice()) {
                // Avoid opening a database selection dialog when there is only 1 database available.
                treatEvent = true;
            } else {
                dialog.pack();
                dialog.setSize(Math.max(ACEditChooseDialog.WIDTH, dialog.getWidth()),
                        Math.max(ACEditChooseDialog.HEIGHT, dialog.getHeight()));
                dialog.setVisible(true);
                if (!dialog.isValidated()) {
                    WaitingDialog.closeInstance();
                    treatEvent = false;
                }
            }
            if (treatEvent) {
                historic = dialog.isHistoric();
            }
        } else {
            historic = selectedArchivingConfiguration.isHistoric();
        }
        if (treatEvent) {
            ACEditDialog dialog;
            try {
                dialog = ACEditDialog.getInstance(alternateSelectionMode, historic);
                dialog.setNewAC(needsNewAc);

                if (needsNewAc) {
                    needsStackCleaning = OpenedACComboBox.getInstance().getStackSize() == OpenedACComboBox.getInstance()
                            .getMaxSize();
                    if (needsStackCleaning) {
                        OpenedACComboBox.getInstance().setMaxSize(OpenedACComboBox.getInstance().getMaxSize() + 1);
                    }

                    final ArchivingConfiguration newAC = new ArchivingConfiguration();
                    newAC.setHistoric(historic);
                    newAC.pushLight();

                    final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance(historic);
                    panel.doUnselectAllModes();

                    final Options options = Options.getInstance();
                    final ACOptions ACoptions = options.getAcOptions();
                    ACoptions.applyDefaults();
                } else {
                    selectedArchivingConfiguration.pushLight();
                }

                dialog.resetTabbedPane();
                WaitingDialog.closeInstance();
                dialog.setVisible(true);
            } catch (final ArchivingException e) {
                LOGGER.error("error", e);
            }
            if (needsStackCleaning) {
                OpenedACComboBox.getInstance().setMaxSize(OpenedACComboBox.getInstance().getMaxSize() - 1);
            }
        }
    }

    /**
     * @return Returns the isAlternateSelectionMode.
     */
    public boolean isAlternateSelectionMode() {
        return alternateSelectionMode;
    }

    /**
     * @param isAlternateSelectionMode
     *            The isAlternateSelectionMode to set.
     */
    public void setAlternateSelectionMode(final boolean isAlternateSelectionMode) {
        this.alternateSelectionMode = isAlternateSelectionMode;
    }
}
