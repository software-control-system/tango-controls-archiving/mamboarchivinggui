package fr.soleil.mambo.actions.archiving;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.mambo.components.archiving.ACPossibleAttributesTree;
import fr.soleil.mambo.containers.archiving.dialogs.ACEditDialog;
import fr.soleil.mambo.containers.sub.dialogs.WaitingDialog;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;
import fr.soleil.mambo.datasources.tango.standard.TangoManagerFactory;
import fr.soleil.mambo.models.ACPossibleAttributesTreeModel;
import fr.soleil.mambo.tools.Messages;

/**
 * @author operateur
 */
public class ACRefreshPossibleAttributesAction extends AbstractAction {

    private static final long serialVersionUID = -4362619951953117117L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACRefreshPossibleAttributesAction.class);
    private static final String TITLE = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_UPDATE_KO");

    private static ACRefreshPossibleAttributesAction instance;

    private String msgLog;
    private String msg;

    public static ACRefreshPossibleAttributesAction getInstance() {
        if (instance == null) {
            instance = new ACRefreshPossibleAttributesAction();
        }
        return instance;
    }

    private ACRefreshPossibleAttributesAction() {
        super();
        super.putValue(Action.NAME, Messages.getMessage("ARCHIVING_ACTION_REFRESH_POSSIBLE_BUTTON"));
    }

    @Override
    public void actionPerformed(final ActionEvent arg0) {
        WaitingDialog.openInstance();
        boolean sourceLoaded = true;
        try {
            final ITangoManager source = TangoManagerFactory.getCurrentImpl();
            source.loadDomains("*");
        } catch (final Exception e) {
            msgLog = Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_TANGO_ATTRIBUTES_KO");
            msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_UPDATE_KO");
            LOGGER.error(msgLog, e);
            JOptionPane.showMessageDialog(ACEditDialog.getInstance(), msg, TITLE, JOptionPane.ERROR_MESSAGE);
            msg = null;
            msgLog = null;
            WaitingDialog.closeInstance();
            sourceLoaded = false;
        } catch (final Throwable t) {
            msgLog = Messages.getLogMessage("APPLICATION_WILL_START_BUFFERING_TANGO_ATTRIBUTES_KO");
            msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_UPDATE_KO");
            LOGGER.error(msgLog, t);
            WaitingDialog.closeInstance();
            sourceLoaded = false;
        }
        if (sourceLoaded) {
            try {
                ACPossibleAttributesTree.getInstance().setModel(ACPossibleAttributesTreeModel.forceGetInstance());
                ACPossibleAttributesTree.getInstance().repaint();
            } catch (final Throwable t) {
                LOGGER.error("Failed to apply ACPossibleAttributesTreeModel", t);
            }
            WaitingDialog.closeInstance();
        }
    }

}
