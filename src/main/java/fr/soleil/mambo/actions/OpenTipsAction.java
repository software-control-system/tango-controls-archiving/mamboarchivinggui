//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/actions/OpenTipsAction.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  OpenTipsAction.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: chinkumo $
//
// $Revision: 1.2 $
//
// $Log: OpenTipsAction.java,v $
// Revision 1.2  2005/11/29 18:27:45  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:20  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

public class OpenTipsAction extends AbstractAction {

    private static final long serialVersionUID = 3052051580232476033L;

    public OpenTipsAction(String name) {
        this.putValue(Action.NAME, name);
        this.setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

}
