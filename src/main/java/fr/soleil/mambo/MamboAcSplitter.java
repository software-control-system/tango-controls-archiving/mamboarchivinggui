package fr.soleil.mambo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeHDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTTSProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributes;
import fr.soleil.mambo.datasources.file.ArchivingConfigurationManagerFactory;
import fr.soleil.mambo.datasources.file.IArchivingConfigurationManager;
import fr.soleil.mambo.tools.xmlhelpers.ac.ArchivingConfigurationXMLHelperFactory;

public class MamboAcSplitter {

    private static final Logger LOGGER = LoggerFactory.getLogger(MamboAcSplitter.class);

    /**
     * @param args
     *            8 juil. 2005
     */
    public static void main(String[] args) {
        if (args == null || args.length != 1) {
            LOGGER.info("Incorrect arguments. Correct syntax: java MamboAcSplitter pathToMixedAC");
            System.exit(1);
        }
        String pathToMixedAC = args[0];
        LOGGER.info("pathToMixedAC/" + pathToMixedAC);

        try {
            ArchivingConfigurationXMLHelperFactory.getImpl(ArchivingConfigurationXMLHelperFactory.STANDARD_IMPL_TYPE);
            ArchivingConfiguration ac = ArchivingConfigurationXMLHelperFactory.getCurrentImpl()
                    .loadArchivingConfigurationIntoHash(pathToMixedAC);
            String pathToAcHdb = getSplitPath(pathToMixedAC, Boolean.TRUE);
            String pathToAcTdb = getSplitPath(pathToMixedAC, Boolean.FALSE);
            String pathToAcTts = getSplitPath(pathToMixedAC, null);
            LOGGER.info("pathToAcHdb|" + pathToAcHdb + "|");
            LOGGER.info("pathToAcTdb|" + pathToAcTdb + "|");
            LOGGER.info("pathToAcTts|" + pathToAcTts + "|");

            ArchivingConfiguration[] split = split(ac);
            ArchivingConfiguration ac_hdb = split[0];
            ac_hdb.setHistoric(Boolean.TRUE);
            ArchivingConfiguration ac_tdb = split[1];
            ac_tdb.setHistoric(Boolean.FALSE);
            ArchivingConfiguration ac_tts = split[2];
            ac_tts.setHistoric(null);

            IArchivingConfigurationManager manager = ArchivingConfigurationManagerFactory
                    .getImpl(ArchivingConfigurationManagerFactory.XML_IMPL_TYPE);

            manager.setNonDefaultSaveLocation(pathToAcHdb);
            manager.saveArchivingConfiguration(ac_hdb);

            manager.setNonDefaultSaveLocation(pathToAcTdb);
            manager.saveArchivingConfiguration(ac_tdb);

            manager.setNonDefaultSaveLocation(pathToAcTts);
            manager.saveArchivingConfiguration(ac_tts);

        } catch (Throwable t) {
            LOGGER.info("Throwable!!!/" + t.getMessage());
            LOGGER.error(TangoExceptionHelper.getErrorMessage(t), t);
        }

    }

    /**
     * @param ac
     * @return
     */
    private static ArchivingConfiguration[] split(ArchivingConfiguration ac) {
        ArchivingConfiguration[] ret = new ArchivingConfiguration[3];
        ArchivingConfiguration hdbAc = new ArchivingConfiguration();
        ArchivingConfiguration tdbAc = new ArchivingConfiguration();
        ArchivingConfiguration ttsAc = new ArchivingConfiguration();
        ArchivingConfigurationAttributes hdbAcAttrs = new ArchivingConfigurationAttributes();
        ArchivingConfigurationAttributes tdbAcAttrs = new ArchivingConfigurationAttributes();
        ArchivingConfigurationAttributes ttsAcAttrs = new ArchivingConfigurationAttributes();

        hdbAc.setHistoric(Boolean.TRUE);
        hdbAc.setData(ac.getData());
        hdbAc.setHistoric(ac.isHistoric());
        hdbAc.setModified(ac.isModified());

        tdbAc.setHistoric(Boolean.FALSE);
        tdbAc.setData(ac.getData());
        tdbAc.setHistoric(ac.isHistoric());
        tdbAc.setModified(ac.isModified());

        ttsAc.setHistoric(null);
        ttsAc.setData(ac.getData());
        ttsAc.setHistoric(ac.isHistoric());
        ttsAc.setModified(ac.isModified());

        ArchivingConfigurationAttribute[] attrs = ac.getAttributes().getAttributesList();
        int numberOfAttrs = attrs.length;
        for (int i = 0; i < numberOfAttrs; i++) {
            ArchivingConfigurationAttribute currentAttr = attrs[i];
            ArchivingConfigurationAttributeHDBProperties HDBProperties = currentAttr.getProperties().getHDBProperties();
            ArchivingConfigurationAttributeTDBProperties TDBProperties = currentAttr.getProperties().getTDBProperties();
            ArchivingConfigurationAttributeTTSProperties TTSProperties = currentAttr.getProperties().getTTSProperties();
            if (!HDBProperties.isEmpty()) {
                ArchivingConfigurationAttribute hdbAttr = new ArchivingConfigurationAttribute();
                ArchivingConfigurationAttributeProperties propOfHdbAttr = new ArchivingConfigurationAttributeProperties();
                hdbAttr.setCompleteName(currentAttr.getCompleteName());
                propOfHdbAttr.setHDBProperties(HDBProperties);
                hdbAttr.setProperties(propOfHdbAttr);
                hdbAcAttrs.addAttribute(hdbAttr);
            }
            if (!TDBProperties.isEmpty()) {
                ArchivingConfigurationAttribute tdbAttr = new ArchivingConfigurationAttribute();
                ArchivingConfigurationAttributeProperties propOfTdbAttr = new ArchivingConfigurationAttributeProperties();
                tdbAttr.setCompleteName(currentAttr.getCompleteName());
                propOfTdbAttr.setTDBProperties(TDBProperties);
                tdbAttr.setProperties(propOfTdbAttr);
                tdbAcAttrs.addAttribute(tdbAttr);
            }
            if (!TTSProperties.isEmpty()) {
                ArchivingConfigurationAttribute ttsAttr = new ArchivingConfigurationAttribute();
                ArchivingConfigurationAttributeProperties propOfTtsAttr = new ArchivingConfigurationAttributeProperties();
                ttsAttr.setCompleteName(currentAttr.getCompleteName());
                propOfTtsAttr.setTTSProperties(TTSProperties);
                ttsAttr.setProperties(propOfTtsAttr);
                ttsAcAttrs.addAttribute(ttsAttr);
            }
        }

        hdbAc.setAttributes(hdbAcAttrs);
        tdbAc.setAttributes(tdbAcAttrs);
        ttsAc.setAttributes(ttsAcAttrs);

        ret[0] = hdbAc;
        ret[1] = tdbAc;
        ret[2] = ttsAc;
        return ret;
    }

    /**
     * @param pathToMixedAC
     * @param b
     * @return
     */
    private static String getSplitPath(String pathToMixedAC, Boolean historic) {
        int posOfFileExtension = pathToMixedAC.lastIndexOf(".");
        String beforeFileExtension = pathToMixedAC.substring(0, posOfFileExtension);
        // LOGGER.info( "beforeFileExtension/"+beforeFileExtension+TangoDeviceHelper.SLASH );
        String afterFileExtension = pathToMixedAC.substring(posOfFileExtension, pathToMixedAC.length());
        // LOGGER.info( "afterFileExtension/"+afterFileExtension+TangoDeviceHelper.SLASH );
        String suffix;
        if (historic == null) {
            suffix = "_TTS";
        } else if (historic.booleanValue()) {
            suffix = "_HDB";
        } else {
            suffix = "_TDB";
        }
        return beforeFileExtension + suffix + afterFileExtension;
    }

}
