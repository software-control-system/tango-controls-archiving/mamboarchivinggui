package fr.soleil.mambo.history.archiving;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.components.archiving.LimitedACStack;
import fr.soleil.mambo.components.archiving.OpenedACComboBox;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;

public class ACHistory {
    private ArchivingConfiguration selectedAC;
    private LimitedACStack openedACs;

    public static final String AC_KEY = "archivingConfigurations";
    public static final String SELECTED_AC_KEY = "selectedAC";
    public static final String OPENED_AC_KEY = "openedAC";
    public static final String OPENED_ACS_KEY = "openedACs";

    public ACHistory() {
        this(null, null);
    }

    public ACHistory(ArchivingConfiguration selectedAC) {
        this(selectedAC, null);
    }

    /**
     * @param selectedAc2
     * @param openedAc
     */
    public ACHistory(ArchivingConfiguration selectedAC, LimitedACStack openedACs) {
        this.selectedAC = selectedAC;
        this.openedACs = openedACs == null ? new LimitedACStack() : openedACs;
    }

    /**
     * 2 sept. 2005
     */
    public void push() {
        if (openedACs == null) {
            openedACs = new LimitedACStack();
        }
        OpenedACComboBox openedACComboBox = OpenedACComboBox.getInstance(openedACs);
        openedACComboBox.setElements(openedACs);

        if (openedACComboBox.getSelectedAC() != null) {
            openedACComboBox.getSelectedAC().push();
        }

    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        ret += new XMLLine(ACHistory.AC_KEY, XMLLine.OPENING_TAG_CATEGORY);
        ret += GUIUtilities.CRLF;

        if (selectedAC != null) {
            ret += new XMLLine(ACHistory.SELECTED_AC_KEY, XMLLine.OPENING_TAG_CATEGORY);
            ret += GUIUtilities.CRLF;

            ret += selectedAC.toString();
            ret += GUIUtilities.CRLF;

            ret += new XMLLine(ACHistory.SELECTED_AC_KEY, XMLLine.CLOSING_TAG_CATEGORY);
            ret += GUIUtilities.CRLF;
        }
        if (openedACs != null) {
            ret += new XMLLine(ACHistory.OPENED_ACS_KEY, XMLLine.OPENING_TAG_CATEGORY);
            ret += GUIUtilities.CRLF;

            ret += openedACs.toString();
            ret += GUIUtilities.CRLF;

            ret += new XMLLine(ACHistory.OPENED_ACS_KEY, XMLLine.CLOSING_TAG_CATEGORY);
            ret += GUIUtilities.CRLF;
        }

        ret += new XMLLine(ACHistory.AC_KEY, XMLLine.CLOSING_TAG_CATEGORY);
        ret += GUIUtilities.CRLF;

        return ret;
    }

    /**
     * @return Returns the openedACs.
     */
    public LimitedACStack getOpenedACs() {
        return openedACs;
    }

    /**
     * @param openedACs
     *            The openedACs to set.
     */
    public void setOpenedACs(LimitedACStack openedACs) {
        this.openedACs = openedACs;
    }
}
