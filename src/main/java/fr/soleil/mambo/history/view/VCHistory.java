package fr.soleil.mambo.history.view;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.components.view.LimitedVCStack;
import fr.soleil.mambo.components.view.OpenedVCComboBox;
import fr.soleil.mambo.data.view.ViewConfiguration;

public class VCHistory {

    // MULTI-CONF
    private ViewConfiguration selectedVC;
    private LimitedVCStack openedVCs;

    public static final String VC_KEY = "viewConfigurations";
    public static final String SELECTED_VC_KEY = "selectedVC";
    public static final String OPENED_VCS_KEY = "openedVCs";
    public static final String OPENED_VC_KEY = "openedVC";

    public static int STACK_DEPTH = 3;

    public VCHistory() {
        this(null, null);
    }

    public VCHistory(ViewConfiguration selectedVC) {
        this(selectedVC, null);
    }

    /**
     * @param selectedVc2
     * @param openedVc
     */
    public VCHistory(ViewConfiguration selectedVC, LimitedVCStack openedVCs) {
        this.selectedVC = selectedVC;
        this.openedVCs = openedVCs == null ? new LimitedVCStack() : openedVCs;
    }

    /**
     * 2 sept. 2005
     */
    public void push() {
        OpenedVCComboBox openedVCComboBox = OpenedVCComboBox.getInstance(this.openedVCs);
        openedVCComboBox.setElements(this.openedVCs);
        openedVCComboBox.selectElement(selectedVC);
    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        ret += new XMLLine(VCHistory.VC_KEY, XMLLine.OPENING_TAG_CATEGORY);
        ret += GUIUtilities.CRLF;

        if (selectedVC != null) {
            ret += new XMLLine(VCHistory.SELECTED_VC_KEY, XMLLine.OPENING_TAG_CATEGORY);
            ret += GUIUtilities.CRLF;

            ret += selectedVC.toString();
            ret += GUIUtilities.CRLF;

            ret += new XMLLine(VCHistory.SELECTED_VC_KEY, XMLLine.CLOSING_TAG_CATEGORY);
            ret += GUIUtilities.CRLF;
        }

        if (openedVCs != null) {
            ret += new XMLLine(VCHistory.OPENED_VCS_KEY, XMLLine.OPENING_TAG_CATEGORY);
            ret += GUIUtilities.CRLF;

            ret += openedVCs.toString();
            ret += GUIUtilities.CRLF;

            ret += new XMLLine(VCHistory.OPENED_VCS_KEY, XMLLine.CLOSING_TAG_CATEGORY);
            ret += GUIUtilities.CRLF;
        }

        ret += new XMLLine(VCHistory.VC_KEY, XMLLine.CLOSING_TAG_CATEGORY);
        ret += GUIUtilities.CRLF;

        return ret;
    }

    /**
     * @return Returns the openedVCs.
     */
    public LimitedVCStack getOpenedVCs() {
        return openedVCs;
    }

    /**
     * @param openedVCs The openedVCs to set.
     */
    public void setOpenedVCs(LimitedVCStack openedVCs) {
        this.openedVCs = openedVCs;
    }
}
