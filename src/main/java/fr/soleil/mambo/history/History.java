package fr.soleil.mambo.history;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.bean.manager.ViewConfigurationBeanManager;
import fr.soleil.mambo.components.archiving.LimitedACStack;
import fr.soleil.mambo.components.archiving.OpenedACComboBox;
import fr.soleil.mambo.components.view.LimitedVCStack;
import fr.soleil.mambo.components.view.OpenedVCComboBox;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.history.archiving.ACHistory;
import fr.soleil.mambo.history.view.VCHistory;

public class History {

    // MULTI-CONF
    private ACHistory acHistory;
    private VCHistory vcHistory;

    private static History currentHistory;
    public static final String XML_KEY = "history";
    public static final String ID_KEY = "id";

    public History(ACHistory acHistory, VCHistory vcHistory) {
        this.acHistory = acHistory;
        this.vcHistory = vcHistory;
    }

    /**
     * @return 8 juil. 2005
     */
    public static History getCurrentHistory() {
        // build an history object representing the current state of the application
        ArchivingConfiguration selectedAc = ArchivingConfiguration.getSelectedArchivingConfiguration();
        ViewConfiguration selectedVc = ViewConfigurationBeanManager.getInstance().getSelectedConfiguration();

        ACHistory acHistory = new ACHistory(selectedAc);
        VCHistory vcHistory = new VCHistory(selectedVc);

        OpenedVCComboBox openedVCComboBox = OpenedVCComboBox.getInstance();
        if (openedVCComboBox != null) {
            LimitedVCStack vcStack = openedVCComboBox.getVCElements();
            vcHistory.setOpenedVCs(vcStack);
        }

        OpenedACComboBox openedACComboBox = OpenedACComboBox.getInstance();
        if (openedACComboBox != null) {
            LimitedACStack acStack = openedACComboBox.getACElements();
            acHistory.setOpenedACs(acStack);
        }

        currentHistory = new History(acHistory, vcHistory);
        return currentHistory;
    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        ret += new XMLLine(XML_KEY, XMLLine.OPENING_TAG_CATEGORY);
        ret += GUIUtilities.CRLF;

        if (acHistory != null) {
            ret += acHistory.toString();
            ret += GUIUtilities.CRLF;
        }

        if (vcHistory != null) {
            ret += vcHistory.toString();
            ret += GUIUtilities.CRLF;
        }

        ret += new XMLLine(XML_KEY, XMLLine.CLOSING_TAG_CATEGORY);

        return ret;
    }

    /**
     * 8 juil. 2005
     */
    public void push() {
        if (acHistory != null) {
            acHistory.push();
        }
        if (vcHistory != null) {
            vcHistory.push();
        }
    }

    /**
     * @param currentHistory
     *            The currentHistory to set.
     */
    public static void setCurrentHistory(History currentHistory) {
        History.currentHistory = currentHistory;
    }
}
