package fr.soleil.mambo.history.manager;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.manager.XMLDataManager;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.mambo.components.archiving.LimitedACStack;
import fr.soleil.mambo.components.view.LimitedVCStack;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationData;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.history.History;
import fr.soleil.mambo.history.archiving.ACHistory;
import fr.soleil.mambo.history.view.VCHistory;
import fr.soleil.mambo.tools.xmlhelpers.ac.ArchivingConfigurationXMLHelperFactory;
import fr.soleil.mambo.tools.xmlhelpers.vc.ViewConfigurationXMLHelper;

public class XMLHistoryManager extends XMLDataManager<History, Map<String, Map<String, Object>>>
        implements IHistoryManager {

    /*
     * (non-Javadoc)
     * 
     * @see
     * bensikin.bensikin.history.manager.IHistoryManager#saveHistory(bensikin
     * .bensikin.history.History, java.lang.String)
     */
    @Override
    public void saveHistory(History history, String historyResourceLocation) throws ArchivingException {
        saveData(history, historyResourceLocation);
    }

    @Override
    public History loadHistory(String historyResourceLocation) throws ArchivingException {
        History result = null;
        try {
            Map<String, Map<String, Object>> historyHt = loadDataIntoHash(historyResourceLocation);
            // BEGIN AC
            Map<String, Object> acBook = historyHt.get(ACHistory.AC_KEY);

            if (acBook == null) {
                result = new History(new ACHistory(), new VCHistory());
            } else {
                ArchivingConfiguration selectedAc = (ArchivingConfiguration) acBook.get(ACHistory.SELECTED_AC_KEY);
                LimitedACStack openedAc = (LimitedACStack) acBook.get(ACHistory.OPENED_ACS_KEY);

                // END AC

                // BEGIN VC
                Map<String, Object> vcBook = historyHt.get(VCHistory.VC_KEY);

                if (vcBook == null) {
                    result = new History(new ACHistory(), new VCHistory());
                } else {
                    ViewConfiguration selectedVc = (ViewConfiguration) vcBook.get(VCHistory.SELECTED_VC_KEY);
                    LimitedVCStack openedVc = (LimitedVCStack) vcBook.get(VCHistory.OPENED_VCS_KEY);
                    ACHistory acHistory = new ACHistory(selectedAc, openedAc);
                    VCHistory vcHistory = new VCHistory(selectedVc, openedVc);
                    result = new History(acHistory, vcHistory);
                }
                // END VC
            }
        } catch (Exception e) {
            throw ArchivingException.toArchivingException(e);
        }
        return result;
    }

    /**
     * @param currentBookNode
     * @return 2 sept. 2005
     * @throws ArchivingException
     */
    private static Map<String, Object> loadVCBook(Node currentBookNode) throws ArchivingException {
        Map<String, Object> ret = new ConcurrentHashMap<String, Object>();

        if (currentBookNode.hasChildNodes()) {
            NodeList bookNodes = currentBookNode.getChildNodes();

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentSubNode = bookNodes.item(i);

                if (XMLUtils.isAFakeNode(currentSubNode)) {
                    continue;
                }

                String currentBookType = currentSubNode.getNodeName().trim();
                if (VCHistory.SELECTED_VC_KEY.equals(currentBookType)) {
                    ViewConfiguration selectedVC = loadSelectedVCBook(currentSubNode);
                    ret.put(VCHistory.SELECTED_VC_KEY, selectedVC);
                }
                if (VCHistory.OPENED_VCS_KEY.equals(currentBookType)) {
                    LimitedVCStack openedVCs = loadOpenedVCBook(currentSubNode);
                    ret.put(VCHistory.OPENED_VCS_KEY, openedVCs);
                }
            }
        }
        return ret;
    }

    /**
     * @param currentSubNode
     * @return
     * @throws ArchivingException
     */
    private static LimitedVCStack loadOpenedVCBook(Node currentSubNode) throws ArchivingException {
        LimitedVCStack openedVCs = new LimitedVCStack();

        if (currentSubNode.hasChildNodes()) {
            NodeList bookNodes = currentSubNode.getChildNodes();

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentSubSubNode = bookNodes.item(i);

                if (XMLUtils.isAFakeNode(currentSubSubNode)) {
                    continue;
                }

                String currentBookType = currentSubSubNode.getNodeName().trim();
                if (ViewConfiguration.XML_TAG.equals(currentBookType)) {

                    ViewConfiguration openedVC = ViewConfigurationXMLHelper
                            .loadViewConfigurationIntoHashFromRoot(currentSubSubNode);
                    openedVCs.push(openedVC);
                }
            }
        }

        Collections.reverse(openedVCs);
        return openedVCs;
    }

    /**
     * @param currentSubNode
     * @return
     * @throws ArchivingException
     */
    private static LimitedACStack loadOpenedACBook(Node currentSubNode) throws ArchivingException {
        LimitedACStack openedACs = new LimitedACStack();

        if (currentSubNode.hasChildNodes()) {
            NodeList bookNodes = currentSubNode.getChildNodes();

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentSubSubNode = bookNodes.item(i);

                if (XMLUtils.isAFakeNode(currentSubSubNode)) {
                    continue;
                }

                String currentBookType = currentSubSubNode.getNodeName().trim();
                if (ArchivingConfiguration.XML_TAG.equals(currentBookType)) {
                    ArchivingConfiguration openedAC = ArchivingConfigurationXMLHelperFactory.getCurrentImpl()
                            .loadArchivingConfigurationIntoHashFromRoot(currentSubSubNode);
                    openedACs.push(openedAC);
                }
            }
        }

        Collections.reverse(openedACs);
        return openedACs;
    }

    /**
     * @param currentSubNode
     * @return 2 sept. 2005
     * @throws ArchivingException
     */
    private static ViewConfiguration loadSelectedVCBook(Node currentSubNode) throws ArchivingException {
        if (currentSubNode.hasChildNodes()) {
            NodeList bookNodes = currentSubNode.getChildNodes();

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentSubSubNode = bookNodes.item(i);

                if (XMLUtils.isAFakeNode(currentSubSubNode)) {
                    continue;
                }

                String currentBookType = currentSubSubNode.getNodeName().trim();
                if (ViewConfiguration.XML_TAG.equals(currentBookType)) {
                    ViewConfiguration selectedVC = ViewConfigurationXMLHelper
                            .loadViewConfigurationIntoHashFromRoot(currentSubSubNode);
                    return selectedVC;
                }
            }
        }
        return null;
    }

    /**
     * @param currentBookNode
     * @return 2 sept. 2005
     * @throws ArchivingException
     */
    private static Map<String, Object> loadACBook(Node currentBookNode) throws ArchivingException {
        Map<String, Object> ret = new ConcurrentHashMap<String, Object>();

        if (currentBookNode.hasChildNodes()) {
            NodeList bookNodes = currentBookNode.getChildNodes();

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentSubNode = bookNodes.item(i);

                if (XMLUtils.isAFakeNode(currentSubNode)) {
                    continue;
                }

                String currentBookType = currentSubNode.getNodeName().trim();
                if (ACHistory.SELECTED_AC_KEY.equals(currentBookType)) {
                    ArchivingConfiguration selectedAC = loadSelectedACBook(currentSubNode);
                    ret.put(ACHistory.SELECTED_AC_KEY, selectedAC);
                }
                if (ACHistory.OPENED_ACS_KEY.equals(currentBookType)) {
                    LimitedACStack openedACs = loadOpenedACBook(currentSubNode);
                    ret.put(ACHistory.OPENED_ACS_KEY, openedACs);
                }
            }
        }
        return ret;
    }

    /**
     * @param currentSubNode
     * @return 2 sept. 2005
     * @throws ArchivingException
     */
    private static ArchivingConfiguration loadSelectedACBook(Node currentSubNode) throws ArchivingException {
        if (currentSubNode.hasChildNodes()) {
            NodeList bookNodes = currentSubNode.getChildNodes();

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentSubSubNode = bookNodes.item(i);

                if (XMLUtils.isAFakeNode(currentSubSubNode)) {
                    continue;
                }

                String currentBookType = currentSubSubNode.getNodeName().trim();
                if (ArchivingConfigurationData.XML_TAG.equals(currentBookType)) {
                    ArchivingConfiguration selectedAC = ArchivingConfigurationXMLHelperFactory.getCurrentImpl()
                            .loadArchivingConfigurationIntoHashFromRoot(currentSubSubNode);
                    return selectedAC;
                }
            }
        }
        return null;
    }

    @Override
    protected History getDefaultData() {
        return History.getCurrentHistory();
    }

    @Override
    protected Map<String, Map<String, Object>> loadDataIntoHashFromRoot(Node rootNode) throws ArchivingException {
        Map<String, Map<String, Object>> history = null;
        if (rootNode.hasChildNodes()) {
            NodeList bookNodes = rootNode.getChildNodes();
            history = new ConcurrentHashMap<String, Map<String, Object>>();
            // As many loops as there are history parts in the saved file
            // (which is normally two: AC and VC)
            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node currentBookNode = bookNodes.item(i);
                if (XMLUtils.isAFakeNode(currentBookNode)) {
                    continue;
                } else {
                    String currentBookType = currentBookNode.getNodeName().trim();
                    Map<String, Object> currentBook;
                    if (ACHistory.AC_KEY.equals(currentBookType)) {
                        currentBook = loadACBook(currentBookNode);
                        history.put(ACHistory.AC_KEY, currentBook);
                    } else if (VCHistory.VC_KEY.equals(currentBookType)) {
                        currentBook = loadVCBook(currentBookNode);
                        history.put(VCHistory.VC_KEY, currentBook);
                    }
                }
            }
        }
        return history;
    }

}
