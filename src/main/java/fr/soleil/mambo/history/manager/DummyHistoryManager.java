package fr.soleil.mambo.history.manager;

import fr.soleil.mambo.history.History;

public class DummyHistoryManager implements IHistoryManager {

    @Override
    public void saveHistory(History history, String historyResourceLocation) {
    }

    @Override
    public History loadHistory(String historyResourceLocation) {
        return null;
    }

}
