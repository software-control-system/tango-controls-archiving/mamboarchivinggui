package fr.soleil.mambo.data;

public interface IConfiguration {

    public static final String DISPAY_SEPARATOR = " : ";

    public boolean isModified();

    public StringBuilder displayableTitleToStringBuilder(StringBuilder builder);

    public String getDisplayableTitle();

    public boolean containsAttribute(String attributeName);

}
