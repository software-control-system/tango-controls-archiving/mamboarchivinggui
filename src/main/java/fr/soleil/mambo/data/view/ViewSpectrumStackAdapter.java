package fr.soleil.mambo.data.view;

import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.comete.definition.event.PlayerEvent;
import fr.soleil.comete.definition.listener.IPlayerListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.mambo.containers.view.AbstractViewSpectrumPanel;
import fr.soleil.mambo.tools.Messages;

public class ViewSpectrumStackAdapter extends AbstractViewAdapter implements IPlayerListener {

    private final List<Map<String, Object>> stack;
    private final WeakReference<IPlayer> playerRef;

    public ViewSpectrumStackAdapter(IChartViewer chart, IPlayer player) {
        super(chart);
        stack = new ArrayList<Map<String, Object>>();
        playerRef = (player == null ? null : new WeakReference<IPlayer>(player));
    }

    protected IPlayer getPlayer() {
        return ObjectUtils.recoverObject(playerRef);
    }

    @Override
    public void clean() {
        IPlayer player = getPlayer();
        if (player != null) {
            player.removePlayerListener(this);
            player.stop();
            player.gotoFirst();
        }
        stack.clear();
    }

    private void extractReadData(DbData[] splitedData) {
        if (splitedData[0] != null) {
            int readSize = splitedData[0].getTimedData().length;
            for (int i = 0; i < readSize; i++) {
                Map<String, Object> dataMap = new HashMap<String, Object>();
                extractDataArray(dataMap, i, splitedData[0], Messages.getMessage("VIEW_SPECTRUM_READ"));
                stack.add(dataMap);
            }
        }
    }

    private void extractWriteData(DbData[] splitedData) {
        int writeSize = splitedData[1].getTimedData().length;
        for (int i = 0; i < writeSize; i++) {
            Map<String, Object> dataMap = new HashMap<String, Object>();
            extractDataArray(dataMap, i, splitedData[1], Messages.getMessage("VIEW_SPECTRUM_WRITE"));
            stack.add(dataMap);
        }
    }

    private void extractDataArray(Map<String, Object> dataMap, int timeIndex, DbData data, String idSuffix) {
        if ((dataMap != null) && (data != null) && (data.getTimedData() != null) && (timeIndex > -1)
                && (timeIndex < data.getTimedData().length)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(data.getTimedData()[timeIndex].getTime());
            StringBuilder idBuilder = new StringBuilder(
                    AbstractViewSpectrumPanel.DATE_FORMAT.format(calendar.getTime()));
            if ((idSuffix != null) && (!idSuffix.trim().isEmpty())) {
                idBuilder.append(' ').append(idSuffix);
            }
            double[] array = new double[2 * data.getMaxX()];
            for (int index = 0; index < data.getMaxX(); index++) {
                double y;
                if ((data.getTimedData()[timeIndex].getValue() != null)
                        && (index < Array.getLength(data.getTimedData()[timeIndex].getValue()))) {
                    y = Array.getDouble(data.getTimedData()[timeIndex].getValue(), index);
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                array[2 * index] = index;
                array[2 * index + 1] = y;
            }
            dataMap.put(idBuilder.toString(), array);
        }
    }

    private void setNameAndColor(String id) {
        if (id != null) {
            IChartViewer chart = getChart();
            if (chart != null) {
                if (id.contains(Messages.getMessage("VIEW_SPECTRUM_READ"))) {
                    // read curves
                    chart.setDataViewCometeColor(id, CometeColor.RED);
                } else if (id.contains(Messages.getMessage("VIEW_SPECTRUM_WRITE"))) {
                    // write curves
                    chart.setDataViewCometeColor(id, CometeColor.BLUE);
                } else {
                    // should never happen
                    chart.setDataViewCometeColor(id, CometeColor.BLACK);
                }
            }
        }
    }

    @Override
    public void indexChanged(PlayerEvent event) {
        IPlayer player = event.getSource();
        if (player != null) {
            int index = player.getIndex();
            IChartViewer chart = getChart();
            if (chart != null) {
                Map<String, Object> data = null;
                synchronized (stack) {
                    if (index >= 0 && index < stack.size()) {
                        // convert data
                        data = stack.get(index);
                        if (data != null) {
                            for (String id : data.keySet()) {
                                setNameAndColor(id);
                            }
                        }
                    }
                }
                chart.setData(data);
            }
        }
    }

    public void setData(DbData[] splitedData, int checkBoxSelected) {
        clean();
        IPlayer player = getPlayer();
        if (player != null) {
            player.addPlayerListener(this);
            switch (checkBoxSelected) {
            case 1:
                extractReadData(splitedData);
                break;
            case 2:
                extractWriteData(splitedData);
                break;
            case 3:
                extractReadData(splitedData);
                extractWriteData(splitedData);
                break;
            default:
                // nothing to do
                break;
            }
            player.initSlider(stack.size() - 1);
            player.setIndex(0);
        }
    }

}
