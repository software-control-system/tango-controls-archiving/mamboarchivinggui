package fr.soleil.mambo.data.view;

import java.lang.ref.WeakReference;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.lib.project.ObjectUtils;

public abstract class AbstractViewAdapter implements IViewAdapter {

    private final WeakReference<IChartViewer> chartRef;

    protected AbstractViewAdapter(IChartViewer chart) {
        chartRef = (chart == null ? null : new WeakReference<>(chart));
    }

    protected final IChartViewer getChart() {
        return ObjectUtils.recoverObject(chartRef);
    }

}
