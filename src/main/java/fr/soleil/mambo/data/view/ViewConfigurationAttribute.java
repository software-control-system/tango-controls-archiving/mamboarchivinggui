package fr.soleil.mambo.data.view;

import java.text.ParseException;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.Chart;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.datasources.db.attributes.AttributeManagerFactory;
import fr.soleil.mambo.datasources.db.attributes.IAttributeManager;
import fr.soleil.mambo.datasources.db.extracting.ExtractingManagerFactory;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;

public class ViewConfigurationAttribute extends Attribute implements DBExtractionConst {

    private static final long serialVersionUID = -8697942719962141249L;

    public static final String XML_TAG = "attribute";
    public static final String COMPLETE_NAME_PROPERTY_XML_TAG = "completeName";
    public static final String FACTOR_PROPERTY_XML_TAG = "factor";

    public static final String SUFFIX_READ = "/read";
    public static final String SUFFIX_WRITE = "/write";

    private ViewConfigurationAttributeProperties properties;

    private double factor;

    public ViewConfigurationAttribute(String completeName) {
        super();
        factor = 1;
        properties = new ViewConfigurationAttributeProperties();
        setCompleteName(completeName);
    }

    /**
     * @param _attribute
     */
    public ViewConfigurationAttribute(final Attribute _attribute) {
        super();
        factor = 1;
        properties = new ViewConfigurationAttributeProperties();

        setCompleteName(_attribute.getCompleteName());
        setDevice(_attribute.getDeviceName());
        setDomain(_attribute.getDomainName());
        setFamily(_attribute.getFamilyName());
        this.setMember(_attribute.getMemberName());
        setName(_attribute.getName());
    }

    /**
     * @return Returns the properties.
     */
    public ViewConfigurationAttributeProperties getProperties() {
        return properties;
    }

    /**
     * @param properties
     *            The properties to set.
     */
    public void setProperties(ViewConfigurationAttributeProperties properties) {
        if (properties == null) {
            properties = new ViewConfigurationAttributeProperties();
        }
        String name = properties.getPlotProperties().getCurve().getName();
        if ((name == null) || (name.trim().isEmpty())) {
            properties.getPlotProperties().getCurve().setName(getCompleteName());
        }
        this.properties = properties;
    }

    public boolean isEmpty() {
        final ViewConfigurationAttributePlotProperties plotProperties = properties.getPlotProperties();

        final boolean ret = plotProperties.isEmpty();

        return ret;
    }

    public void addToChart(final Chart chart, final String startDate, final String endDate, final Boolean historic,
            final SamplingType samplingType, final String idViewSelected) throws DevFailed, ParseException {
        final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
        DbData[] recoveredData = extractingManager.retrieveData(historic, samplingType, getCompleteName(), startDate,
                endDate);

        if (recoveredData != null) {
            for (DbData data : recoveredData) {
                if (data != null) {
                    data.setName(getCompleteName());
                }
            }
            if (idViewSelected != null) {
                MamboViewAdapterFactory daoFactory = MamboViewAdapterFactory.INSTANCE;
                ViewScalarAdapter dao = daoFactory.getDAOScalar(idViewSelected, chart);
                if (dao != null) {
                    prepareView(chart, recoveredData);
                    dao.addData(extractingManager.isShowRead(), extractingManager.isShowWrite(), recoveredData);
                }
            }
        }
    }

    public final void prepareView(final Chart chart, final DbData... recoveredData) {
        boolean hasRead = false;
        boolean hasWrite = false;
        if ((recoveredData != null) && (recoveredData.length == 2)
                && ((recoveredData[0] != null) || (recoveredData[1] != null))) {
            hasRead = recoveredData[READ_INDEX] != null;
            hasWrite = recoveredData[WRITE_INDEX] != null;
            String nameId = hasRead ? recoveredData[READ_INDEX].getName() : recoveredData[WRITE_INDEX].getName();
            String name = nameId;
            nameId = nameId.toLowerCase();
            final ViewConfigurationAttributePlotProperties plotProperties = properties.getPlotProperties();
            String displayName = plotProperties.getCurve().getName();
            if ((displayName == null) || (displayName.trim().isEmpty())) {
                displayName = name;
            }
            if (hasWrite) {
                setAttributeDataView(chart, displayName, nameId, true, hasRead);
            }
            if (hasRead) {
                setAttributeDataView(chart, displayName, nameId, false, hasWrite);
            }
        }
    }

    private String checkName(String name, String suffix) {
        String dvName = name;
        if (!getCompleteName().endsWith(suffix)) {
            while (dvName.endsWith(suffix)) {
                dvName = dvName.substring(0, dvName.length() - suffix.length());
            }
        }
        return dvName;
    }

    private void setAttributeDataView(final Chart chart, String displayName, String nameId, final boolean hasWrite,
            final boolean hasWriteRead) {
        final ViewConfigurationAttributePlotProperties vcPlotProperties = properties.getPlotProperties();

        String dvName = displayName;

        String suffix = hasWrite ? SUFFIX_WRITE : SUFFIX_READ;

        if (dvName == null) {
            dvName = getCompleteName();
        } else {
            dvName = checkName(checkName(dvName, SUFFIX_WRITE), SUFFIX_READ);
        }
        if (dvName.trim().isEmpty()) {
            dvName = getCompleteName();
        }

        dvName = dvName + suffix;
        nameId = nameId + suffix;

        if (vcPlotProperties != null) {
            PlotProperties plotProperties = vcPlotProperties.clone();
            plotProperties.getCurve().setName(dvName);
            chart.setDataViewPlotProperties(nameId, plotProperties);
            if (hasWrite && hasWriteRead) {
                chart.setDataViewLineStyle(nameId, IChartViewer.STYLE_LONG_DASH);
            }
            if (vcPlotProperties.isHidden()) {
                chart.setDataViewLineWidth(nameId, 0);
                chart.setDataViewMarkerStyle(nameId, IChartViewer.MARKER_NONE);
                chart.setDataViewBarWidth(nameId, 0);
                chart.setDataViewFillStyle(nameId, IChartViewer.FILL_STYLE_NONE);
                chart.setDataViewLabelVisible(nameId, false);
                chart.setDataViewClickable(nameId, false);
            }
        }
    }

    /**
     * @return 2 sept. 2005
     * @throws ArchivingException
     * @throws DevFailed
     */
    public boolean isScalar(final Boolean historic) throws ArchivingException {
        final IAttributeManager manager = AttributeManagerFactory.getCurrentImpl();

        return manager.isScalar(getCompleteName(), historic);
    }

    /**
     * @return 2 sept. 2005
     * @throws ArchivingException
     * @throws DevFailed
     */
    public boolean isSpectrum(final Boolean historic) throws ArchivingException {
        final IAttributeManager manager = AttributeManagerFactory.getCurrentImpl();

        return manager.isSpectrum(getCompleteName(), historic);
    }

    /**
     * @return 2 sept. 2005
     * @throws ArchivingException
     * @throws DevFailed
     */
    public int getDataType(final Boolean historic) throws ArchivingException {
        final IAttributeManager manager = AttributeManagerFactory.getCurrentImpl();

        return manager.getDataType(getCompleteName(), historic);
    }

    public int getDataWritable(final Boolean historic) throws ArchivingException {
        final IAttributeManager manager = AttributeManagerFactory.getCurrentImpl();

        return manager.getDataWritable(getCompleteName(), historic);
    }

    /**
     * @param _historic
     * @return
     * @throws ArchivingException
     */
    public boolean isImage(final Boolean historic) throws ArchivingException {
        final IAttributeManager manager = AttributeManagerFactory.getCurrentImpl();

        return manager.isImage(getCompleteName(), historic);
    }

    public String getDisplayFormat(final Boolean historic) throws ArchivingException {
        final IAttributeManager manager = AttributeManagerFactory.getCurrentImpl();
        return manager.getDisplayFormat(getCompleteName(), historic);
    }

    public double getFactor() {
        return factor;
    }

    public void setFactor(final double _factor) {
        factor = _factor;
    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        final XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.OPENING_TAG_CATEGORY);
        openingLine.setAttribute(COMPLETE_NAME_PROPERTY_XML_TAG, getCompleteName());
        openingLine.setAttribute(FACTOR_PROPERTY_XML_TAG, getFactor() + ObjectUtils.EMPTY_STRING);
        final XMLLine closingLine = new XMLLine(XML_TAG, XMLLine.CLOSING_TAG_CATEGORY);

        ret += openingLine.toString();
        ret += GUIUtilities.CRLF;

        if (properties != null) {
            final ViewConfigurationAttributePlotProperties plotProperties = properties.getPlotProperties();
            ret += plotProperties.toString();
            ret += GUIUtilities.CRLF;
        }
        ret += closingLine.toString();

        return ret;
    }

}
