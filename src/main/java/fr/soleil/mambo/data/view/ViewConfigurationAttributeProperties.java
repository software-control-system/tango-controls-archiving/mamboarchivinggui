package fr.soleil.mambo.data.view;

public class ViewConfigurationAttributeProperties {

    private ViewConfigurationAttributePlotProperties plotProperties;

    private static ViewConfigurationAttributeProperties currentProperties;

    public static ViewConfigurationAttributeProperties getCurrentProperties() {
        return currentProperties;
    }

    public ViewConfigurationAttributeProperties() {
        this.plotProperties = new ViewConfigurationAttributePlotProperties();
    }

    /**
     * @return Returns the plotProperties.
     */
    public ViewConfigurationAttributePlotProperties getPlotProperties() {
        return plotProperties;
    }

    /**
     * @param plotProperties
     *            The plotProperties to set.
     */
    public void setPlotProperties(final ViewConfigurationAttributePlotProperties _plotProperties) {
        this.plotProperties = _plotProperties;
    }

    public static void resetCurrentProperties() {
        currentProperties = null;
        currentProperties = new ViewConfigurationAttributeProperties();
    }

    /**
     * @param currentProperties2
     */
    public static void setCurrentProperties(final ViewConfigurationAttributeProperties _currentProperties) {
        currentProperties = _currentProperties;
    }

}
