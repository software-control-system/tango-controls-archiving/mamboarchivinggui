package fr.soleil.mambo.data.view;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.lib.project.math.MathConst;

public class ViewScalarAdapter extends AbstractViewAdapter implements DBExtractionConst {

    private static final String READ_DATA_VIEW_KEY = "read";
    private static final String WRITE_DATA_VIEW_KEY = "write";

    private boolean subtractMean;

    public ViewScalarAdapter(IChartViewer chart) {
        super(chart);
        subtractMean = false;
        if (chart != null) {
            chart.setData(null);
        }
    }

    public boolean isSubtractMean() {
        return subtractMean;
    }

    public void setSubtractMean(final boolean subtractMean) {
        if (this.subtractMean != subtractMean) {
            this.subtractMean = subtractMean;
            IChartViewer chart = getChart();
            if (chart != null) {
                chart.setData(null);
            }
        }
    }

    public Map<String, Object> computeChartData(final boolean showRead, final boolean showWrite,
            final DbData... splitData) {
        Map<String, Object> result = null;
        DbData refData = DbData.getFirstDbData(splitData);
        if ((refData != null) && (splitData.length == 2)) {
            String name = refData.getName().toLowerCase();
            int format = refData.getDataFormat();
            int writable = refData.getWritable();
            int type = refData.getDataType();
            if (format == AttrDataFormat._SCALAR) {
                switch (writable) {
                    case AttrWriteType._READ:
                        if (showRead) {
                            result = buildDataMap(extractScalarDataView(splitData[READ_INDEX], type,
                                    name + TangoDeviceHelper.SLASH + READ_DATA_VIEW_KEY));
                        }
                        break;
                    case AttrWriteType._WRITE:
                        if (showWrite) {
                            result = buildDataMap(extractScalarDataView(splitData[WRITE_INDEX], type,
                                    name + TangoDeviceHelper.SLASH + WRITE_DATA_VIEW_KEY));
                        }
                        break;
                    case AttrWriteType._READ_WITH_WRITE:
                    case AttrWriteType._READ_WRITE:
                        if (showRead) {
                            result = buildDataMap(extractScalarDataView(splitData[READ_INDEX], type,
                                    name + TangoDeviceHelper.SLASH + READ_DATA_VIEW_KEY));
                        }
                        if (showWrite) {
                            Map<String, Object> temp = buildDataMap(extractScalarDataView(splitData[WRITE_INDEX], type,
                                    name + TangoDeviceHelper.SLASH + WRITE_DATA_VIEW_KEY));
                            if (result == null) {
                                result = temp;
                            } else {
                                result.putAll(temp);
                                temp.clear();
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        } // end if (splitData != null)
        return result;
    }

    public void addData(final boolean showRead, final boolean showWrite, final DbData... splitData) {
        IChartViewer chart = getChart();
        if (chart != null) {
            chart.addData(computeChartData(showRead, showWrite, splitData));
        }
    }

    public void addData(final DataBuilder data) {
        IChartViewer chart = getChart();
        if (chart != null) {
            chart.addData(buildDataMap(data));
        }
    }

    @Override
    public void clean() {
        // nothing to do
    }

    private DataBuilder extractScalarDataView(final DbData data, final int dataType, final String id) {
        DataBuilder result = null;
        if ((data != null) && (data.getTimedData() != null)) {
            NullableTimedData[] timedAttrData = data.getTimedData();
            switch (dataType) {
                case TangoConst.Tango_DEV_UCHAR:
                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                case TangoConst.Tango_DEV_LONG:
                case TangoConst.Tango_DEV_ULONG:
                case TangoConst.Tango_DEV_ULONG64:
                case TangoConst.Tango_DEV_LONG64:
                case TangoConst.Tango_DEV_FLOAT:
                case TangoConst.Tango_DEV_DOUBLE:
                    result = new DataBuilder(id);
                    if (subtractMean) {
                        // compute the sum for this scalar
                        double nbPoints = data.size();
                        double sum = 0;
                        for (int i = 0; i < nbPoints; i++) {
                            NullableTimedData timedData = timedAttrData[i];
                            Object dataValue = timedData.getValue();
                            boolean[] nullElements = (boolean[]) timedData.getNullElements();
                            if (dataValue != null) {
                                int length = Array.getLength(dataValue);
                                for (int j = 0; j < length; j++) {
                                    double value;
                                    if ((nullElements == null) || (!nullElements[j])) {
                                        value = Array.getDouble(dataValue, j);
                                    } else {
                                        value = MathConst.NAN_FOR_NULL;
                                    }
                                    sum += value;
                                }
                            }
                        }

                        // divide by number of values
                        double mean = 0;
                        if (nbPoints != 0) {
                            mean = sum / nbPoints;
                        }

                        // subtract the mean from all points
                        for (int i = 0; i < timedAttrData.length; i++) {
                            NullableTimedData timedData = timedAttrData[i];
                            Object dataValue = timedData.getValue();
                            boolean[] nullElements = (boolean[]) timedData.getNullElements();
                            long sec_l = timedData.getTime();
                            if (dataValue == null) {
                                result.add(sec_l, MathConst.NAN_FOR_NULL);
                            } else {
                                int length = Array.getLength(dataValue);
                                for (int j = 0; j < length; j++) {
                                    double value;
                                    if ((nullElements == null) || (!nullElements[j])) {
                                        value = Array.getDouble(dataValue, j) - mean;
                                    } else {
                                        value = MathConst.NAN_FOR_NULL;
                                    }
                                    result.add(sec_l, value);
                                }
                            }
                        }
                    } else {
                        for (int i = 0; i < timedAttrData.length; i++) {
                            NullableTimedData timedData = timedAttrData[i];
                            Object dataValue = timedData.getValue();
                            boolean[] nullElements = (boolean[]) timedData.getNullElements();
                            long sec_l = timedData.getTime();
                            if (dataValue == null) {
                                result.add(sec_l, MathConst.NAN_FOR_NULL);
                            } else {
                                int length = Array.getLength(dataValue);
                                for (int j = 0; j < length; j++) {
                                    double value;
                                    if ((nullElements == null) || (!nullElements[j])) {
                                        value = Array.getDouble(dataValue, j);
                                    } else {
                                        value = MathConst.NAN_FOR_NULL;
                                    }
                                    result.add(sec_l, value);
                                }
                            }
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_BOOLEAN:
                    result = new DataBuilder(id);
                    for (int i = 0; i < timedAttrData.length; i++) {
                        NullableTimedData timedData = timedAttrData[i];
                        Object dataValue = timedData.getValue();
                        boolean[] nullElements = (boolean[]) timedData.getNullElements();
                        long sec_l = timedData.getTime();
                        if (dataValue == null) {
                            result.add(sec_l, MathConst.NAN_FOR_NULL);
                        } else {
                            int length = Array.getLength(dataValue);
                            for (int j = 0; j < length; j++) {
                                double value;
                                if ((nullElements == null) || (!nullElements[j])) {
                                    value = Array.getBoolean(dataValue, j) ? 1 : 0;
                                } else {
                                    value = MathConst.NAN_FOR_NULL;
                                }
                                result.add(sec_l, value);
                            }
                        }
                    }
                    break;
            }
        }
        return result;
    }

    public Map<String, Object> buildDataMap(DataBuilder dataArray) {
        Map<String, Object> dataMap = new HashMap<>();
        if ((dataArray != null) && (dataArray.getId() != null) && (dataArray.getData() != null)) {
            dataMap.put(dataArray.getId(), dataArray.getData());
        }
        return dataMap;
    }

}
