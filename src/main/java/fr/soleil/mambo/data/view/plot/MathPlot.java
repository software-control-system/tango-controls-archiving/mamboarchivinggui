package fr.soleil.mambo.data.view.plot;

import java.util.HashMap;
import java.util.Map;

import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.tools.BasicObjectFormatter;

public class MathPlot extends MathProperties {

    private static final long serialVersionUID = 2213677218487771428L;

    public static final String XML_TAG = "math";
    public static final String FUNCTION_PROPERTY_XML_TAG = "function";

    public MathPlot() {
        super();
    }

    public MathPlot(int function) {
        super(function);
    }

    public MathPlot(MathProperties math) {
        super();
        if (math != null) {
            MathProperties clone = math.clone();
            setFunction(clone.getFunction());
        }
    }

    public MathPlot(final Map<String, String> properties) {

        if (properties != null) {
            String function = properties.get(XML_TAG + "_" + FUNCTION_PROPERTY_XML_TAG);

            if (function != null) {
                setFunction(BasicObjectFormatter.getInt(function));
            }
        }
    }

    public Map<String, String> getMathPlotPropertiesList() {
        Map<String, String> result = new HashMap<String, String>();

        result.put(XML_TAG + "_" + FUNCTION_PROPERTY_XML_TAG, String.valueOf(getFunction()));

        return result;
    }

    @Override
    public String toString() {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);

        openingLine.setAttribute(FUNCTION_PROPERTY_XML_TAG, String.valueOf(getFunction()));

        return openingLine.toString();
    }
}
