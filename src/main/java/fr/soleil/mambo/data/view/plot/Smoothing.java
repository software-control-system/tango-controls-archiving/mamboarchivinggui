package fr.soleil.mambo.data.view.plot;

import java.util.HashMap;
import java.util.Map;

import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.tools.BasicObjectFormatter;

public class Smoothing extends SmoothingProperties {

    private static final long serialVersionUID = 1376258076934988130L;

    public static final String XML_TAG = "smoothing";
    public static final String METHOD_PROPERTY_XML_TAG = "method";
    public static final String NEIGHBORS_PROPERTY_XML_TAG = "neighbors";
    public static final String GAUSS_SIGMA_PROPERTY_XML_TAG = "gaussSigma";
    public static final String EXTRAPOLATION_PROPERTY_XML_TAG = "extrapolation";

    public Smoothing() {
        super();
    }

    public Smoothing(int method, int neighbors, double gaussSigma, int extrapolation) {
        super(method, neighbors, gaussSigma, extrapolation);
    }

    public Smoothing(SmoothingProperties smoothing) {
        super();
        if (smoothing != null) {
            SmoothingProperties clone = smoothing.clone();
            setExtrapolation(clone.getExtrapolation());
            setGaussSigma(clone.getGaussSigma());
            setMethod(clone.getMethod());
            setNeighbors(clone.getNeighbors());
        }
    }

    public Smoothing(final Map<String, String> properties) {

        if (properties != null) {
            String method = properties.get(XML_TAG + "_" + METHOD_PROPERTY_XML_TAG);
            String neighbors = properties.get(XML_TAG + "_" + NEIGHBORS_PROPERTY_XML_TAG);
            String gaussSigma = properties.get(XML_TAG + "_" + GAUSS_SIGMA_PROPERTY_XML_TAG);
            String extrapolation = properties.get(XML_TAG + "_" + EXTRAPOLATION_PROPERTY_XML_TAG);

            if (method != null) {
                setMethod(BasicObjectFormatter.getInt(method));
            }
            if (neighbors != null) {
                setNeighbors(BasicObjectFormatter.getInt(neighbors));
            }
            if (gaussSigma != null) {
                setGaussSigma(BasicObjectFormatter.getDouble(gaussSigma));
            }
            if (extrapolation != null) {
                setExtrapolation(BasicObjectFormatter.getInt(extrapolation));
            }
        }
    }

    public Map<String, String> getSmoothingPropertiesList() {
        Map<String, String> result = new HashMap<String, String>();

        result.put(XML_TAG + "_" + METHOD_PROPERTY_XML_TAG, String.valueOf(getMethod()));
        result.put(XML_TAG + "_" + NEIGHBORS_PROPERTY_XML_TAG, String.valueOf(getNeighbors()));
        result.put(XML_TAG + "_" + GAUSS_SIGMA_PROPERTY_XML_TAG, String.valueOf(getGaussSigma()));
        result.put(XML_TAG + "_" + EXTRAPOLATION_PROPERTY_XML_TAG, String.valueOf(getExtrapolation()));

        return result;
    }

    @Override
    public String toString() {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);

        openingLine.setAttribute(METHOD_PROPERTY_XML_TAG, String.valueOf(getMethod()));
        openingLine.setAttribute(NEIGHBORS_PROPERTY_XML_TAG, String.valueOf(getNeighbors()));
        openingLine.setAttribute(GAUSS_SIGMA_PROPERTY_XML_TAG, String.valueOf(getGaussSigma()));
        openingLine.setAttribute(EXTRAPOLATION_PROPERTY_XML_TAG, String.valueOf(getExtrapolation()));

        return openingLine.toString();
    }
}
