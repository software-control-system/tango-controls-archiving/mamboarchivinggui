package fr.soleil.mambo.data.view;

import java.io.File;
import java.sql.Timestamp;
import java.text.Collator;
import java.text.ParseException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.components.ConfigurationFileFilter;
import fr.soleil.mambo.components.view.OpenedVCComboBox;
import fr.soleil.mambo.components.view.VCAttributesRecapTree;
import fr.soleil.mambo.components.view.VCFileFilter;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.data.IConfiguration;
import fr.soleil.mambo.datasources.db.extracting.ExtractingManagerFactory;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;
import fr.soleil.mambo.datasources.file.IViewConfigurationManager;
import fr.soleil.mambo.tools.ChartPropertiesUtils;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.TreeUtils;

public class ViewConfiguration implements IConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewConfiguration.class);

    public static final String XML_TAG = "viewConfiguration";
    private ViewConfigurationAttributes attributes;
    private String currentId = null;

    // MULTI-CONF
    private ViewConfigurationData data;

    private TreeMap<String, ExpressionAttribute> expressions;

    private volatile boolean isModified = true;
    private final ChartListener chartListener;

    public ViewConfiguration() {
        super();
        chartListener = new ChartListener();
        setData(new ViewConfigurationData());
        setAttributes(new ViewConfigurationAttributes());
        setExpressions(new TreeMap<>(Collator.getInstance()));
        setModified(true);
    }

    /**
     * Adds a new expression to the ViewConfiguration. Useful when loading a VC
     * from XML.
     * 
     * @param expression
     *            the expression to add
     */
    public void addExpression(final ExpressionAttribute expression) {
        expressions.put(expression.getName(), expression);
    }

    public void clearExpressions() {
        expressions.clear();
    }

    public int getNotNumberExpressionsCount() {
        return attributes.getNotNumberExpressionsCount();
    }

    /**
     * @return
     */
    public ViewConfiguration cloneVC() {
        final ViewConfiguration ret = new ViewConfiguration();

        ret.setModified(isModified());
        ret.setPath(getPath() == null ? null : new String(getPath()));
        ret.setData(getData().cloneData());
        ret.setAttributes(getAttributes().cloneAttrs());
        ret.setExpressions(getExpressionsDuplicata());

        return ret;
    }

    public void computeCurrentId() {
        currentId = null;
        final String selectedViewName = getDisplayableTitle();
        String lastUpdate = null;
        if ((getData() != null) && getData().getLastUpdateDate() != null) {
            lastUpdate = Long.toString(getData().getLastUpdateDate().getTime());
            currentId = selectedViewName + " - " + lastUpdate;
        }
    }

    @Override
    public boolean containsAttribute(final String completeName) {
        return attributes.containsAttribute(completeName);
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals;
        if (obj instanceof ViewConfiguration) {
            ViewConfiguration this2 = (ViewConfiguration) obj;
            boolean dataEquals;
            boolean attrEquals;
            boolean pathEquals;

            final String path1 = data.getPath() == null ? ObjectUtils.EMPTY_STRING : data.getPath();
            final String path2 = this2.data.getPath() == null ? ObjectUtils.EMPTY_STRING : this2.data.getPath();
            pathEquals = path1.equals(path2);

            final ViewConfigurationData data1 = getData();
            final ViewConfigurationData data2 = this2.getData();
            if (data1 == null) {
                if (data2 == null) {
                    dataEquals = true;
                } else {
                    dataEquals = false;
                }
            } else {
                if (data2 == null) {
                    dataEquals = false;
                } else {
                    dataEquals = data1.equals(data2);
                }
            }
            final ViewConfigurationAttributes attrs1 = getAttributes();
            final ViewConfigurationAttributes attrs2 = this2.getAttributes();
            if (attrs1 == null) {
                if (attrs2 == null) {
                    attrEquals = true;
                } else {
                    attrEquals = false;
                }
            } else {
                if (attrs2 == null) {
                    attrEquals = false;
                } else {
                    attrEquals = attrs1.equals(attrs2);
                }
            }

            equals = dataEquals && attrEquals && pathEquals;
        } else {
            equals = false;
        }
        return equals;
    }

    public ViewConfigurationAttribute getAttribute(final String completeName) {
        return attributes.getAttribute(completeName);
    }

    /**
     * @return Returns the attributes.
     */
    public ViewConfigurationAttributes getAttributes() {
        return attributes;
    }

    public ViewConfigurationAttributes getSelectedAttributes(VCAttributesRecapTree tree) {
        ViewConfigurationAttributes attributes = new ViewConfigurationAttributes();
        if (tree != null) {
            TreePath[] selectionPaths = tree.getSelectionPaths();
            Collection<DefaultMutableTreeNode> nodes = new LinkedHashSet<>();
            if (selectionPaths != null) {
                for (TreePath path : selectionPaths) {
                    if (path != null) {
                        Object comp = path.getLastPathComponent();
                        if (comp instanceof DefaultMutableTreeNode) {
                            addAttributeNode((DefaultMutableTreeNode) comp, nodes);
                        }
                    }
                }
            }
            ViewConfigurationData data = getData();
            if (data != null) {
                Boolean historic = data.isHistoric();
                for (DefaultMutableTreeNode node : nodes) {
                    ViewConfigurationAttribute attribute = getAttribute(
                            TreeUtils.getFullAttributeNameFromNode(tree, historic, node));
                    if (attribute != null) {
                        attributes.addAttribute(attribute);
                    }
                }
            }
            nodes.clear();
        }
        return attributes;
    }

    protected void addAttributeNode(DefaultMutableTreeNode node, Collection<DefaultMutableTreeNode> nodes) {
        if (node != null) {
            // According to VCTreeRenderer, a node is associated to an attribute when it is of level 4
            if (node.getLevel() == 4) {
                nodes.add(node);
            }
            for (int i = 0; i < node.getChildCount(); i++) {
                TreeNode child = node.getChildAt(i);
                if (child instanceof DefaultMutableTreeNode) {
                    addAttributeNode((DefaultMutableTreeNode) child, nodes);
                }
            }
        }
    }

    public Chart getChartLight() {
        final Chart chart = data.generateAdaptedChart();
        chart.addChartViewerListener(chartListener);
        // bug no21498 : uncomment the line below to disable X axis scale
        // properties inside graph panel
        //
        // chart.setAxisScaleEditionEnabled(false, IChartViewer.X);

        chart.setDataDirectory(Mambo.getPathToResources());
        return chart;
    }

    public void cleanChart(Chart chart) {
        if (chart != null) {
            chart.removeChartViewerListener(chartListener);
            chart.resetAll();
        }
    }

    protected boolean isOnX(PlotProperties properties) {
        return (properties != null && properties.getAxisChoice() == IChartViewer.X);
    }

    protected boolean isOnX(ViewConfigurationAttributeProperties attrProperties) {
        boolean hasX = false;
        if (attrProperties != null) {
            hasX = isOnX(attrProperties.getPlotProperties());
        }
        return hasX;
    }

    public void configureChart(Chart chart) {
        if (chart != null) {
            cleanChart(chart);
            data.configureChart(chart);
            chart.setDataDirectory(Mambo.getPathToResources());
            // Chart is configured.
            // Now check whether there is some data on X (TANGOARCH-938).
            ViewConfigurationAttributes attributes = this.attributes;
            Map<String, ExpressionAttribute> expressions = this.expressions;
            boolean hasX = false;
            if (attributes != null) {
                Collection<ViewConfigurationAttribute> vcAttributes = attributes.getAttributeList();
                if (vcAttributes != null) {
                    for (ViewConfigurationAttribute attribute : vcAttributes) {
                        if (attribute != null) {
                            hasX = isOnX(attribute.getProperties());
                            if (hasX) {
                                break;
                            }
                        }
                    }
                }
            }
            if ((!hasX) && (expressions != null)) {
                for (ExpressionAttribute expr : expressions.values()) {
                    if (expr != null) {
                        hasX = isOnX(expr.getProperties());
                        if (hasX) {
                            break;
                        }
                    }
                }
            }
            // If there is some data on X, restore X autoscale (TANGOARCH-938).
            if (hasX) {
                ChartProperties chartProperties = data.getChartProperties();
                if (chartProperties != null) {
                    AxisProperties xProperties = chartProperties.getXAxisProperties();
                    if (xProperties != null && xProperties.isAutoScale()) {
                        if (!chart.isAutoScale(IChartViewer.X)) {
                            chart.setAutoScale(true, IChartViewer.X);
                        }
                    }
                }
            }
            chart.addChartViewerListener(chartListener);
        }
    }

    public String getCurrentId() {
        return currentId;
    }

    /**
     * @return Returns the data.
     */
    public ViewConfigurationData getData() {
        return data;
    }

    @Override
    public StringBuilder displayableTitleToStringBuilder(StringBuilder builder) {
        StringBuilder textBuffer = builder;
        if (textBuffer == null) {
            textBuffer = new StringBuilder();
        }
        if (data == null) {
            textBuffer.append(String.valueOf(data));
        } else {
            textBuffer.append(data.getName());
            final Timestamp lastUpdateDate = data.getLastUpdateDate();
            if (lastUpdateDate != null) {
                textBuffer.append(DISPAY_SEPARATOR).append(lastUpdateDate);
            }
        }
        return textBuffer;
    }

    @Override
    public String getDisplayableTitle() {
        return displayableTitleToStringBuilder(null).toString();
    }

    public TreeMap<String, ExpressionAttribute> getExpressions() {
        return expressions;
    }

    public TreeMap<String, ExpressionAttribute> getExpressionsDuplicata() {
        if (expressions == null) {
            return null;
        }
        final TreeMap<String, ExpressionAttribute> duplicata = new TreeMap<>();
        final Set<String> keySet = expressions.keySet();
        final Iterator<String> keyIterator = keySet.iterator();
        while (keyIterator.hasNext()) {
            final String key = keyIterator.next();
            duplicata.put(new String(key), expressions.get(key).duplicate());
        }
        return duplicata;
    }

    public String getPath() {
        if (data != null) {
            return data.getPath();
        } else {
            return null;
        }
    }

    /**
     * @return
     */
    private String getPathToUse(final IViewConfigurationManager manager, final boolean saveAs) {
        String path;
        final String pathToUse = getPath();
        final JFileChooser chooser = new JFileChooser();
        final VCFileFilter VCfilter = new VCFileFilter();
        chooser.addChoosableFileFilter(VCfilter);

        if (pathToUse != null) {
            if (saveAs) {
                chooser.setCurrentDirectory(new File(pathToUse));
            } else {
                return pathToUse;
            }
        } else {
            chooser.setCurrentDirectory(new File(manager.getDefaultSaveLocation()));
        }

        // ----show the user what he's saving
        final OpenedVCComboBox openedVCComboBox = OpenedVCComboBox.getInstance();
        openedVCComboBox.selectElement(this);
        // ----

        chooser.setDialogTitle(String.format(Messages.getMessage("DIALOGS_FILE_CHOOSER_SAVE_CONFIGURARTION_TITLE"),
                getDisplayableTitle()));
        final int returnVal = chooser.showSaveDialog(MamboFrame.getInstance());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            final File f = chooser.getSelectedFile();
            path = f.getAbsolutePath();

            if (f != null) {
                final String extension = ConfigurationFileFilter.getExtension(f);
                final String expectedExtension = VCfilter.getExtension();

                if (extension == null || !extension.equalsIgnoreCase(expectedExtension)) {
                    path += ".";
                    path += expectedExtension;
                }
            }
            if (f.exists()) {
                final int choice = JOptionPane.showConfirmDialog(MamboFrame.getInstance(),
                        Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS"),
                        Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS_TITLE"), JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                if (choice != JOptionPane.OK_OPTION) {
                    path = null;
                }
            }
            if (path != null) {
                manager.setNonDefaultSaveLocation(path);
            }
        } else {
            path = null;
        }
        return path;

    }

    /**
     * @return Returns the isModified.
     */
    @Override
    public boolean isModified() {
        return isModified;
    }

    public boolean isNew() {
        return data.getCreationDate() == null;
    }

    public void loadAttributes(Chart chart) throws ArchivingException {
        final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
        final Boolean historic = data.isHistoric();
        if (!extractingManager.isCanceled(historic)) {
            try {
                attributes.setIdViewSelected(currentId);
                if (data.isDynamicDateRange()) {
                    final Timestamp[] range = data.getDynamicStartAndEndDates();
                    chart = attributes.setChartAttributes(chart, range[0].toString(), range[1].toString(), historic,
                            data.getSamplingType());
                } else {
                    chart = attributes.setChartAttributes(chart, data.getStartDate().toString(),
                            data.getEndDate().toString(), historic, data.getSamplingType());
                }
            } catch (final DevFailed | ParseException e) {
                LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            }
        }
    }

    public void refreshContent() {
        data.refreshContent();
    }

    public void save(final IViewConfigurationManager manager, final boolean saveAs) {
        final String pathToUse = getPathToUse(manager, saveAs);
        if (pathToUse != null) {
            manager.setNonDefaultSaveLocation(pathToUse);
            try {
                setPath(pathToUse);
                setModified(false);
                manager.saveViewConfiguration(this);

                final String msg = Messages.getLogMessage("SAVE_VIEW_CONFIGURATION_ACTION_OK");
                LOGGER.debug(msg);
            } catch (final Exception e) {
                final String msg = Messages.getLogMessage("SAVE_VIEW_CONFIGURATION_ACTION_KO");
                LOGGER.error(msg, e);

                setPath(null);
                setModified(true);
            }
        }
    }

    public Map<ViewConfigurationAttribute, Integer> extractDataTypeMap(ViewConfigurationAttributes attributes)
            throws ArchivingException {
        Map<ViewConfigurationAttribute, Integer> dataTypes = new LinkedHashMap<>();
        if (attributes != null) {
            Collection<ViewConfigurationAttribute> attributeList = attributes.getAttributeList();
            if (attributeList != null) {
                for (ViewConfigurationAttribute attribute : attributeList) {
                    if (attribute != null) {
                        dataTypes.put(attribute, Integer.valueOf(attribute.getDataType(getData().isHistoric())));
                    }
                }
            }
        }
        return dataTypes;
    }

    /**
     * @param attributes
     *            The attributes to set.
     */
    public void setAttributes(final ViewConfigurationAttributes attributes) {
        this.attributes = attributes;
        if (attributes != null) {
            attributes.setViewConfiguration(this);
        }
    }

    public void prepareExpressions(final Chart chart, Map<ViewConfigurationAttribute, Integer> dataTypes)
            throws ArchivingException {
        attributes.prepareExpressions(chart, dataTypes);
    }

    public void setChartAttributeExpressionOfString(final Map<String, DbData[]> dataMap, final Chart chart)
            throws ArchivingException {
        attributes.setChartAttributeExpressionOfString(dataMap, data.isHistoric(), chart, currentId);
    }

    /**
     * @param data
     *            The data to set.
     */
    public void setData(final ViewConfigurationData data) {
        this.data = data;
    }

    public void setExpressions(final TreeMap<String, ExpressionAttribute> expressions) {
        this.expressions = expressions;
    }

    /**
     * @param isModified
     *            The isModified to set.
     */
    public void setModified(final boolean isModified) {
        this.isModified = isModified;
        OpenedVCComboBox.refresh();
    }

    /**
     * @param saveLocation
     */
    public void setPath(final String path) {
        if (data != null) {
            data.setPath(path);
        }
    }

    @Override
    public String toString() {
        final StringBuilder ret = new StringBuilder();

        final XMLLine openingLine = new XMLLine(ViewConfiguration.XML_TAG, XMLLine.OPENING_TAG_CATEGORY);

        final Timestamp creationDate = data.getCreationDate();
        final Timestamp lastUpdateDate = data.getLastUpdateDate();
        final Timestamp startDate = data.getStartDate();
        final Timestamp endDate = data.getEndDate();
        final Boolean historic = data.isHistoric();
        final boolean isLongTerm = data.isLongTerm();
        final boolean dynamicDateRange = data.isDynamicDateRange();
        final boolean autoAveraging = data.isAutoAveraging();
        final String dateRange = data.getDateRange();
        final String name = data.getName();
        final String path = data.getPath();
        final SamplingType samplingType = data.getSamplingType();

        openingLine.setAttribute(ViewConfigurationData.IS_MODIFIED_PROPERTY_XML_TAG,
                isModified + ObjectUtils.EMPTY_STRING);
        if (creationDate != null) {
            openingLine.setAttribute(ViewConfigurationData.CREATION_DATE_PROPERTY_XML_TAG, creationDate.toString());
        }
        if (lastUpdateDate != null) {
            openingLine.setAttribute(ViewConfigurationData.LAST_UPDATE_DATE_PROPERTY_XML_TAG,
                    lastUpdateDate.toString());
        }
        if (startDate != null) {
            openingLine.setAttribute(ViewConfigurationData.START_DATE_PROPERTY_XML_TAG, startDate.toString());
        }
        if (endDate != null) {
            openingLine.setAttribute(ViewConfigurationData.END_DATE_PROPERTY_XML_TAG, endDate.toString());
        }
        openingLine.setAttribute(ViewConfigurationData.HISTORIC_PROPERTY_XML_TAG, String.valueOf(historic));
        openingLine.setAttribute(ViewConfigurationData.LONG_TERM_PROPERTY_XML_TAG, String.valueOf(isLongTerm));

        openingLine.setAttribute(ViewConfigurationData.DYNAMIC_DATE_RANGE_PROPERTY_XML_TAG,
                String.valueOf(dynamicDateRange));
        openingLine.setAttribute(ViewConfigurationData.AUTOMATIC_AVERAGING_PROPERTY_XML_TAG,
                String.valueOf(autoAveraging));
        openingLine.setAttribute(ViewConfigurationData.DATE_RANGE_PROPERTY_XML_TAG, dateRange);

        if (name != null) {
            openingLine.setAttribute(ViewConfigurationData.NAME_PROPERTY_XML_TAG, name);
        }
        if (path != null) {
            openingLine.setAttribute(ViewConfigurationData.PATH_PROPERTY_XML_TAG, path);
        }
        if (samplingType != null) {
            openingLine.setAttribute(ViewConfigurationData.SAMPLING_TYPE_PROPERTY_XML_TAG, samplingType.toString());
            openingLine.setAttribute(ViewConfigurationData.SAMPLING_FACTOR_PROPERTY_XML_TAG,
                    ObjectUtils.EMPTY_STRING + samplingType.getAdditionalFilteringFactor());
        }

        final XMLLine closingLine = new XMLLine(ViewConfiguration.XML_TAG, XMLLine.CLOSING_TAG_CATEGORY);

        ret.append(openingLine.toString());
        ret.append(GUIUtilities.CRLF);

        if (data != null) {
            ret.append(data.toString());
            ret.append(GUIUtilities.CRLF);
        }

        // parameters specific to groups of attributes
        if (attributes != null) {
            ret.append(attributes.toString());
        }

        if (expressions != null) {
            for (final ExpressionAttribute expression : expressions.values()) {
                ret.append(expression.toString());
            }
        }

        ret.append(closingLine.toString());

        return ret.toString();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class ChartListener implements IChartViewerListener {
        @Override
        public void chartViewerChanged(ChartViewerEvent event) {
            if (event.getReason() == ChartViewerEvent.Reason.CONFIGURATION
                    && ChartPropertiesUtils.configurationListenerEnabled) {
                IChartViewer viewer = event.getSource();
                ChartProperties properties = viewer.getChartProperties();
                ChartPropertiesUtils.saveChartConfiguration(properties, ViewConfiguration.this);
            }
        }

    }

}
