package fr.soleil.mambo.data.view;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.swing.JOptionPane;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.swing.Chart;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.expression.jep.JEPExpressionParser;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.containers.view.dialogs.DateRangeBox;
import fr.soleil.mambo.tools.AxisPropertiesUtils;
import fr.soleil.mambo.tools.ChartPropertiesUtils;
import fr.soleil.mambo.tools.MamboDbUtils;
import fr.soleil.mambo.tools.Messages;

public class ViewConfigurationData {

    public static final boolean DEFAULT_DYNAMIC_DATE_RANGE = false;
    public static final boolean DEFAULT_AUTO_AVERAGING = true;

    private static final long HOUR = 3600000, DAY = 86400000;

    public static final String CREATION_DATE_PROPERTY_XML_TAG = "creationDate";
    public static final String DATE_RANGE_PROPERTY_XML_TAG = "dateRange";
    public static final String DYNAMIC_DATE_RANGE_PROPERTY_XML_TAG = "dynamicDateRange";
    public static final String AUTOMATIC_AVERAGING_PROPERTY_XML_TAG = "autoAveraging";
    public static final String END_DATE_PROPERTY_XML_TAG = "endDate";

    public static final String GENERIC_PLOT_PARAMETERS_XML_TAG = "genericPlotParameters";
    public static final String HISTORIC_PROPERTY_XML_TAG = "historic";
    public static final String IS_MODIFIED_PROPERTY_XML_TAG = "isModified";
    public static final String LAST_UPDATE_DATE_PROPERTY_XML_TAG = "lastUpdateDate";

    public static final String LONG_TERM_PROPERTY_XML_TAG = "longTerm";
    public static final String NAME_PROPERTY_XML_TAG = "name";
    public static final String PATH_PROPERTY_XML_TAG = "path";
    public static final String SAMPLING_FACTOR_PROPERTY_XML_TAG = "samplingFactor";

    public static final String SAMPLING_TYPE_PROPERTY_XML_TAG = "samplingType";

    public static final String START_DATE_PROPERTY_XML_TAG = "startDate";

    private ChartProperties chartProperties;
    private Timestamp creationDate;
    private String dateRange;
    private boolean dynamicDateRange, autoAveraging;

    private Timestamp endDate;
    // MULTI-CONF
    private Boolean historic;
    private Timestamp lastUpdateDate;
    private boolean longTerm;
    private String name;
    private String path;

    private SamplingType samplingType;

    private Timestamp startDate;

    public ViewConfigurationData() {
        this(null);
    }

    public ViewConfigurationData(ChartProperties chartProperties) {
        super();
        creationDate = GUIUtilities.now();
        dateRange = ObjectUtils.EMPTY_STRING;
        dynamicDateRange = DEFAULT_AUTO_AVERAGING;
        autoAveraging = DEFAULT_AUTO_AVERAGING;
        setChartProperties(chartProperties);
        samplingType = SamplingType.getSamplingType(SamplingType.ALL);
        historic = MamboDbUtils.isHistoricAccordingToAvailableDb();
        longTerm = false;
    }

    public static boolean verifyDates(DateRangeBox dateRangeBox) {
        boolean ok = true;
        String msg = null;
        long startDate_l = 0;
        long endDate_l = 0;
        Timestamp[] range = dateRangeBox.getDynamicStartAndEndDates();

        try {
            Timestamp startDate;
            if (dateRangeBox.isDynamicDateRange()) {
                startDate = range[0];
            } else {
                startDate = dateRangeBox.getStartDate();
            }
            startDate_l = startDate.getTime();
        } catch (IllegalArgumentException iae) {
            ok = false;
            msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_CONTROLS_INVALID_START_DATE");
        }

        try {
            Timestamp endDate;
            if (dateRangeBox.isDynamicDateRange()) {
                endDate = range[1];
            } else {
                endDate = dateRangeBox.getEndDate();
            }
            endDate_l = endDate.getTime();
        } catch (IllegalArgumentException iae) {
            ok = false;
            msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_CONTROLS_INVALID_END_DATE");
        }

        if (ok) {
            if (endDate_l <= startDate_l) {
                ok = false;
                msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_CONTROLS_INVALID_DATE_RANGE");
            }
        }

        if (!ok) {
            String title = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_CONTROLS_INVALID_TITLE");
            JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);
        }

        return ok;
    }

    public static Timestamp[] getDynamicStartAndEndDates(String range) {
        Timestamp[] startAndEnd = new Timestamp[2];
        long now = System.currentTimeMillis();
        Timestamp end = new Timestamp(now);
        startAndEnd[1] = end;
        long delta = 0;
        if (Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DATES_RANGE_LAST_1H").equals(range)) {
            delta = 1 * HOUR;
        } else if (Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DATES_RANGE_LAST_4H").equals(range)) {
            delta = 4 * HOUR;
        } else if (Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DATES_RANGE_LAST_8H").equals(range)) {
            delta = 8 * HOUR;
        } else if (Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DATES_RANGE_LAST_1D").equals(range)) {
            delta = 1 * DAY;
        } else if (Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DATES_RANGE_LAST_3D").equals(range)) {
            delta = 3 * DAY;
        } else if (Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DATES_RANGE_LAST_7D").equals(range)) {
            delta = 7 * DAY;
        } else if (Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DATES_RANGE_LAST_30D").equals(range)) {
            delta = 30 * DAY;
        }
        Timestamp start = new Timestamp(now - delta);
        startAndEnd[0] = start;
        return startAndEnd;
    }

    protected static void updateDates(final Timestamp[] startAndEnd, final long now, final long delta) {
        Calendar calendar = Calendar.getInstance();
        synchronized (calendar) {
            calendar.setTimeInMillis(now - delta);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            startAndEnd[0] = new Timestamp(calendar.getTimeInMillis());
            calendar.setTimeInMillis(now + DAY);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            startAndEnd[1] = new Timestamp(calendar.getTimeInMillis());
        }
    }

    public Chart generateAdaptedChart() {
        Chart chart = new Chart();
        configureChart(chart);
        return chart;
    }

    public void configureChart(Chart chart) {
        if (chart != null) {
            chart.setExpressionParser(new JEPExpressionParser());
            ChartPropertiesUtils.configureChartWithProperties(chart, chartProperties);
            double startDate_d;
            double endDate_d;
            if (isDynamicDateRange()) {
                Timestamp[] range = getDynamicStartAndEndDates();
                startDate_d = range[0].getTime();
                endDate_d = range[1].getTime();
            } else {
                startDate_d = startDate.getTime();
                endDate_d = endDate.getTime();
            }
            chart.setAutoScale(false, IChartViewer.X);
            chart.setAxisMinimum(startDate_d, IChartViewer.X);
            chart.setAxisMaximum(endDate_d, IChartViewer.X);
            StringBuilder title = new StringBuilder();
            if (chart.getHeader() != null) {
                title.append(chart.getHeader()).append(" - ");
            }
            title.append(Messages.getMessage("VIEW_ATTRIBUTES_START_DATE")).append(getStartDate()).append(", ")
                    .append(Messages.getMessage("VIEW_ATTRIBUTES_END_DATE")).append(getEndDate());
            chart.setHeader(title.toString());
            chart.setPreferDialogForTable(true, false);
            chart.setDataViewRemovingEnabled(true);
            chart.setCleanDataViewConfigurationOnRemoving(true);
        }
    }

    /**
     * @return Returns the ChartProperties.
     */
    public ChartProperties getChartProperties() {
        return chartProperties;
    }

    /**
     * @param chartProperties The ChartProperties to set.
     */
    public void setChartProperties(ChartProperties chartProperties) {
        if (chartProperties == null) {
            chartProperties = new ChartProperties();
            chartProperties.setAutoHighlightOnLegend(true);
        }
        this.chartProperties = chartProperties;
    }

    /**
     * @return Returns the creationDate.
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate The creationDate to set.
     */
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return returns dateRange
     */
    public String getDateRange() {
        return dateRange;
    }

    /**
     * @param range the value to set to dateRange
     */
    public void setDateRange(String range) {
        dateRange = range;
    }

    public Timestamp[] getDynamicStartAndEndDates() {
        return getDynamicStartAndEndDates(getDateRange());
    }

    /**
     * @return Returns the endDate.
     */
    public Timestamp getEndDate() {
        return endDate;
    }

    /**
     * @param endDate The endDate to set.
     */
    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    /**
     * @return Returns the lastUpdateDate.
     */
    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * @param lastUpdateDate The lastUpdateDate to set.
     */
    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the path.
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path The path to set.
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return Returns the samplingType.
     */
    public SamplingType getSamplingType() {
        return samplingType;
    }

    public void setSamplingType(SamplingType _samplingType) {
        samplingType = _samplingType;
    }

    /**
     * @return Returns the startDate.
     */
    public Timestamp getStartDate() {
        return startDate;
    }

    /**
     * @param startDate The startDate to set.
     */
    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    /**
     * @return returns the dynamicDateRange
     */
    public boolean isDynamicDateRange() {
        return dynamicDateRange;
    }

    /**
     * @param dynamic the value to set to dynamicDateRange
     */
    public void setDynamicDateRange(boolean dynamic) {
        dynamicDateRange = dynamic;
    }

    /**
     * Returns whether automatic averaging is used.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isAutoAveraging() {
        return autoAveraging;
    }

    /**
     * Sets whether automatic averaging should be used.
     * 
     * @param autoAveraging Whether automatic averaging should be used.
     */
    public void setAutoAveraging(boolean autoAveraging) {
        this.autoAveraging = autoAveraging;
    }

    /**
     * @return Returns the isHistoric.
     */
    public Boolean isHistoric() {
        return historic;
    }

    /**
     * @param historic The historic to set.
     */
    public void setHistoric(Boolean historic) {
        this.historic = historic;
    }

    public boolean isLongTerm() {
        return longTerm;
    }

    public void setLongTerm(boolean longTerm) {
        this.longTerm = longTerm;
    }

    public void refreshContent() {
        if (isDynamicDateRange()) {
            Timestamp[] range = getDynamicStartAndEndDates();
            setStartDate(range[0]);
            setEndDate(range[1]);
        }
    }

    public void updateDateRangeBox(DateRangeBox dateRangeBox) {
        boolean dynamic = dateRangeBox.isDynamicDateRange();
        if (dynamic) {
            Timestamp[] range = dateRangeBox.getDynamicStartAndEndDates();
            setStartDate(range[0]);
            setEndDate(range[1]);
        } else {
            setStartDate(dateRangeBox.getStartDate());
            setEndDate(dateRangeBox.getEndDate());
        }
        setDynamicDateRange(dynamic);
        setDateRange(dateRangeBox.getDateRange());
    }

    public ViewConfigurationData cloneData() {
        ViewConfigurationData ret = new ViewConfigurationData();
        ret.setCreationDate(getCreationDate() == null ? null : (Timestamp) getCreationDate().clone());
        ret.setDateRange(getDateRange() == null ? null : new String(getDateRange()));
        ret.setDynamicDateRange(isDynamicDateRange());
        ret.setEndDate(getEndDate() == null ? null : (Timestamp) getEndDate().clone());
        ret.setChartProperties(getChartProperties() == null ? null : (ChartProperties) getChartProperties().clone());
        ret.setHistoric(historic);
        ret.setLastUpdateDate(getLastUpdateDate() == null ? null : (Timestamp) getLastUpdateDate().clone());
        ret.setName(getName() == null ? null : new String(getName()));
        ret.setPath(getPath() == null ? null : new String(getPath()));
        ret.setSamplingType(getSamplingType() == null ? null : getSamplingType().cloneSamplingType());
        ret.setAutoAveraging(isAutoAveraging());
        ret.setStartDate(getStartDate() == null ? null : (Timestamp) getStartDate().clone());
        ret.setLongTerm(isLongTerm());
        return ret;
    }

    public boolean equals(ViewConfigurationData data) {
        boolean pathEquals, nameEquals, creationDateEquals, lastUpdateDateEquals, samplingEquals, tdbEquals;
        String path1 = getPath() == null ? ObjectUtils.EMPTY_STRING : getPath();
        String path2 = data.getPath() == null ? ObjectUtils.EMPTY_STRING : data.getPath();
        pathEquals = path1.equals(path2);

        String name1 = getName() == null ? ObjectUtils.EMPTY_STRING : getName();
        String name2 = data.getName() == null ? ObjectUtils.EMPTY_STRING : data.getName();
        nameEquals = name1.equals(name2);

        Timestamp creationDate1 = getCreationDate();
        Timestamp creationDate2 = data.getCreationDate();
        creationDateEquals = true;
        if (creationDate1 == null) {
            if (creationDate2 != null) {
                creationDateEquals = false;
            }
        } else {
            if (creationDate2 == null) {
                creationDateEquals = false;
            } else {
                if (!creationDate1.equals(creationDate2)) {
                    creationDateEquals = false;
                }
            }
        }
        tdbEquals = isLongTerm() == data.isLongTerm();

        Timestamp lastUpdateDate1 = getLastUpdateDate();
        Timestamp lastUpdateDate2 = data.getLastUpdateDate();
        lastUpdateDateEquals = true;
        if (lastUpdateDate1 == null) {
            if (lastUpdateDate2 != null) {
                lastUpdateDateEquals = false;
            }
        } else {
            if (lastUpdateDate2 == null) {
                lastUpdateDateEquals = false;
            } else {
                if (!lastUpdateDate1.equals(lastUpdateDate2)) {
                    lastUpdateDateEquals = false;
                }
            }
        }

        SamplingType samplingType1 = getSamplingType();
        SamplingType samplingType2 = data.getSamplingType();
        samplingEquals = true;
        if (samplingType1 == null) {
            if (samplingType2 != null) {
                samplingEquals = false;
            }
        } else {
            if (samplingType2 == null) {
                samplingEquals = false;
            } else {
                if (samplingType1.getType() != samplingType2.getType()) {
                    samplingEquals = false;
                }
            }
        }

        return pathEquals && nameEquals && creationDateEquals && lastUpdateDateEquals && samplingEquals && tdbEquals;
    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        XMLLine openingLine = new XMLLine(GENERIC_PLOT_PARAMETERS_XML_TAG, XMLLine.OPENING_TAG_CATEGORY);
        XMLLine closingLine = new XMLLine(GENERIC_PLOT_PARAMETERS_XML_TAG, XMLLine.CLOSING_TAG_CATEGORY);

        ret += openingLine.toString();
        ret += GUIUtilities.CRLF;

        if (chartProperties != null) {
            ret += ChartPropertiesUtils.toXMLLine(chartProperties);
            ret += GUIUtilities.CRLF;
        }

        AxisProperties y1 = chartProperties.getY1AxisProperties();
        if (y1 != null) {
            ret += AxisPropertiesUtils.toXMLLine(AxisPropertiesUtils.XML_TAG_1, y1);
            ret += GUIUtilities.CRLF;
        }
        AxisProperties y2 = chartProperties.getY2AxisProperties();
        if (y2 != null) {
            ret += AxisPropertiesUtils.toXMLLine(AxisPropertiesUtils.XML_TAG_2, y2);
            ret += GUIUtilities.CRLF;
        }
        AxisProperties x = chartProperties.getXAxisProperties();
        if (x != null) {
            ret += AxisPropertiesUtils.toXMLLine(AxisPropertiesUtils.XML_TAG_3, x);
            ret += GUIUtilities.CRLF;
        }

        ret += closingLine.toString();

        return ret;
    }

}
