package fr.soleil.mambo.data.view;

import java.lang.reflect.Array;
import java.text.Collator;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.Chart;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.expression.IExpressionParser;
import fr.soleil.lib.project.expression.exception.ExpressionEvaluationException;
import fr.soleil.lib.project.expression.jep.JEPExpressionParser;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.containers.view.dialogs.AttributesPlotPropertiesPanel;
import fr.soleil.mambo.data.view.plot.Bar;
import fr.soleil.mambo.data.view.plot.Curve;
import fr.soleil.mambo.data.view.plot.Interpolation;
import fr.soleil.mambo.data.view.plot.Marker;
import fr.soleil.mambo.data.view.plot.MathPlot;
import fr.soleil.mambo.data.view.plot.Smoothing;
import fr.soleil.mambo.datasources.db.extracting.ExtractingManagerFactory;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.sub.VCOptions;

public class ViewConfigurationAttributes {

    public static final String COLOR_INDEX_XML_TAG = "MamboVCAttributesColorIndex";
    public static final String COLOR_INDEX_XML_ATTRIBUTE = "VALUE";

    private TreeMap<String, ViewConfigurationAttribute> attributes;
    private int colorIndex;
    private ViewConfiguration viewConfiguration;
    private final List<ExpressionAttribute> notNumberExpression;
    private String idViewSelected;

    public ViewConfigurationAttributes() {
        attributes = new TreeMap<>(Collator.getInstance());
        colorIndex = 0;
        notNumberExpression = new ArrayList<>();
    }

    public void addAttribute(final ViewConfigurationAttribute attribute) {
        attributes.put(attribute.getCompleteName().toLowerCase(), attribute);
    }

    public ViewConfigurationAttribute getAttribute(final String completeName) {
        return completeName == null ? null : attributes.get(completeName.toLowerCase());
    }

    public boolean containsAttribute(String completeName) {
        return completeName == null ? false : attributes.containsKey(completeName.toLowerCase());
    }

    public int countAttributes() {
        return attributes == null ? 0 : attributes.size();
    }

    public int getNotNumberExpressionsCount() {
        return notNumberExpression.size();
    }

    public Collection<ViewConfigurationAttribute> getAttributeList() {
        return attributes == null ? null : attributes.values();
    }

    public Collection<String> getAttributeNames() {
        return attributes == null ? null : attributes.keySet();
    }

    public Map<String, ViewConfigurationAttribute> getAttributesCopy() {
        return attributes == null ? null : new LinkedHashMap<>(attributes);
    }

    public void clearAttributes() {
        if (attributes != null) {
            attributes.clear();
        }
    }

    /**
     * Returns the spectrum attributes
     * 
     * @param historic to know in which database you want to check if the attributes are spectrum or not.
     * @return The spectrum attributes
     * @throws ArchivingException
     */
    public TreeMap<String, ViewConfigurationAttribute> getSpectrumAttributes(final Boolean historic)
            throws ArchivingException {
        final TreeMap<String, ViewConfigurationAttribute> spectra = new TreeMap<>(Collator.getInstance());
        if (attributes != null) {
            for (Entry<String, ViewConfigurationAttribute> entry : attributes.entrySet()) {
                final String nextKey = entry.getKey();
                final ViewConfigurationAttribute nextValue = entry.getValue();
                if (nextValue.isSpectrum(historic)) {
                    spectra.put(nextKey, nextValue);
                }
            }
        }
        return spectra;
    }

    /**
     * Returns the string and state scalar attributes
     * 
     * @param historic to know in which database you want to check the attributes
     * @return The string scalar attributes
     * @throws ArchivingException
     */
    public TreeMap<String, ViewConfigurationAttribute> getStringStateScalarAttributes(final Boolean historic)
            throws ArchivingException {
        final TreeMap<String, ViewConfigurationAttribute> strings = new TreeMap<>(Collator.getInstance());
        if (attributes != null) {
            for (Entry<String, ViewConfigurationAttribute> entry : attributes.entrySet()) {
                final String nextKey = entry.getKey();
                final ViewConfigurationAttribute nextValue = entry.getValue();
                if (nextValue.isScalar(historic)) {
                    if (nextValue.getDataType(historic) == TangoConst.Tango_DEV_STRING
                            || nextValue.getDataType(historic) == TangoConst.Tango_DEV_STATE) {
                        strings.put(nextKey, nextValue);
                    }
                }
            }
        }
        return strings;
    }

    /**
     * Returns the number and boolean scalar attributes complete names
     * 
     * @param historic to know in which database you want to check the attributes
     * @return The number and boolean scalar attributes complete names
     * @throws ArchivingException
     */
    public String[] getNumberAndBooleanScalarAttributeNames(final Boolean historic) throws ArchivingException {
        Collection<String> nameList = new ArrayList<>();
        if (attributes != null) {
            for (Entry<String, ViewConfigurationAttribute> entry : attributes.entrySet()) {
                final String name = entry.getKey();
                final ViewConfigurationAttribute attr = entry.getValue();
                if (attr.isScalar(historic)) {
                    switch (attr.getDataType(historic)) {
                        case TangoConst.Tango_DEV_BOOLEAN:
                        case TangoConst.Tango_DEV_CHAR:
                        case TangoConst.Tango_DEV_UCHAR:
                        case TangoConst.Tango_DEV_LONG:
                        case TangoConst.Tango_DEV_ULONG:
                        case TangoConst.Tango_DEV_LONG64:
                        case TangoConst.Tango_DEV_ULONG64:
                        case TangoConst.Tango_DEV_SHORT:
                        case TangoConst.Tango_DEV_USHORT:
                        case TangoConst.Tango_DEV_FLOAT:
                        case TangoConst.Tango_DEV_DOUBLE:
                            nameList.add(name);
                            break;
                    }
                }
            }
        }
        String[] names = nameList.toArray(new String[nameList.size()]);
        nameList.clear();
        return names;
    }

    /**
     * Returns the image attributes
     * 
     * @param historic to know in which database you want to check the attributes
     * @return The image attributes
     * @throws ArchivingException
     */
    public TreeMap<String, ViewConfigurationAttribute> getImageAttributes(final Boolean historic)
            throws ArchivingException {
        final TreeMap<String, ViewConfigurationAttribute> images = new TreeMap<>(Collator.getInstance());
        if (attributes != null) {
            for (Entry<String, ViewConfigurationAttribute> entry : attributes.entrySet()) {
                final String nextKey = entry.getKey();
                final ViewConfigurationAttribute nextValue = entry.getValue();
                if (nextValue.isImage(historic)) {
                    images.put(nextKey, nextValue);
                }
            }
        }
        return images;
    }

    /**
     * @param attributes
     *            The attributes to set.
     */
    public void setAttributes(final TreeMap<String, ViewConfigurationAttribute> attributes) {
        this.attributes = attributes;
    }

    public void prepareExpressions(final Chart chart, Map<ViewConfigurationAttribute, Integer> dataTypes)
            throws ArchivingException {
        notNumberExpression.clear();
        if ((viewConfiguration != null) && (dataTypes != null)) {
            final Map<String, ExpressionAttribute> expressions = viewConfiguration.getExpressions();
            for (final ExpressionAttribute attrExpression : expressions.values()) {
                final String[] variables = attrExpression.getVariables();
                if (variables != null) {
                    boolean allVariablesAreNumber = true;
                    for (final String variable : variables) {
                        String attributName = ObjectUtils.EMPTY_STRING;
                        if (variable.contains(ViewConfigurationAttribute.SUFFIX_WRITE)) {
                            final int index = variable.indexOf(ViewConfigurationAttribute.SUFFIX_WRITE);
                            attributName = variable.substring(0, index);
                        }
                        if (variable.contains(ViewConfigurationAttribute.SUFFIX_READ)) {
                            final int index = variable.indexOf(ViewConfigurationAttribute.SUFFIX_READ);
                            attributName = variable.substring(0, index);
                        }
                        final ViewConfigurationAttribute attr = attributes.get(attributName.toLowerCase());
                        if (attr != null) {
                            final Integer dataType = dataTypes.get(attr);
                            if (dataType != null) {
                                final int attrType = dataType.intValue();
                                if (attrType != TangoConst.Tango_DEV_BOOLEAN && attrType != TangoConst.Tango_DEV_CHAR
                                        && attrType != TangoConst.Tango_DEV_UCHAR
                                        && attrType != TangoConst.Tango_DEV_LONG
                                        && attrType != TangoConst.Tango_DEV_ULONG
                                        && attrType != TangoConst.Tango_DEV_SHORT
                                        && attrType != TangoConst.Tango_DEV_USHORT
                                        && attrType != TangoConst.Tango_DEV_FLOAT
                                        && attrType != TangoConst.Tango_DEV_DOUBLE) {
                                    allVariablesAreNumber = false;
                                    break;
                                }
                            }
                        }
                    } // end for (final String variable : variables)
                    if (allVariablesAreNumber) {
                        int axis = -1;
                        switch (attrExpression.getProperties().getAxisChoice()) {
                            case ViewConfigurationAttributePlotProperties.AXIS_CHOICE_Y1:
                                axis = IChartViewer.Y1;
                                break;
                            case ViewConfigurationAttributePlotProperties.AXIS_CHOICE_Y2:
                                axis = IChartViewer.Y2;
                                break;
                            case ViewConfigurationAttributePlotProperties.AXIS_CHOICE_X:
                                axis = IChartViewer.X;
                                break;
                        }
                        attrExpression.prepareView(chart);
                        chart.addExpression(attrExpression.getId(), attrExpression.getName(),
                                attrExpression.getExpression(), axis, attrExpression.getVariables(),
                                attrExpression.isX());
                    } else {
                        notNumberExpression.add(attrExpression);
                    }
                } // end if (variables != null
            } // end for (final ExpressionAttribute attrExpression : expressions.values())
        } // end if (viewConfiguration != null)
    }

    public Chart setChartAttributes(final Chart chart, final String startDate, final String endDate,
            final Boolean historic, final SamplingType samplingType)
            throws DevFailed, ParseException, ArchivingException {
        final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();

        if (!extractingManager.isCanceled(historic)) {
            if (attributes != null) {
                for (final ViewConfigurationAttribute nextValue : attributes.values()) {
                    if (extractingManager.isCanceled(historic)) {
                        break;
                    }
                    if (nextValue.isScalar(historic)) {
                        switch (nextValue.getDataType(historic)) {
                            case TangoConst.Tango_DEV_BOOLEAN:
                            case TangoConst.Tango_DEV_CHAR:
                            case TangoConst.Tango_DEV_UCHAR:
                            case TangoConst.Tango_DEV_LONG:
                            case TangoConst.Tango_DEV_ULONG:
                            case TangoConst.Tango_DEV_LONG64:
                            case TangoConst.Tango_DEV_ULONG64:
                            case TangoConst.Tango_DEV_SHORT:
                            case TangoConst.Tango_DEV_USHORT:
                            case TangoConst.Tango_DEV_FLOAT:
                            case TangoConst.Tango_DEV_DOUBLE:
                                nextValue.addToChart(chart, startDate, endDate, historic, samplingType, idViewSelected);
                                break;
                        }
                    }
                }
            } // end if (attributes != null)
        }
        return chart;
    }

    public void setChartAttributeExpressionOfString(final Map<String, DbData[]> dataMap, final Boolean historic,
            final Chart chart, final String currentViewId) throws ArchivingException {
        if (dataMap != null) {
            try {
                final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
                if (!extractingManager.isCanceled(historic)) {
                    for (final ExpressionAttribute attributeExpression : notNumberExpression) {
                        if (extractingManager.isCanceled(historic)) {
                            break;
                        }
                        final String[] variablesNames = attributeExpression.getVariables();
                        DbData[] variablesDatas = new DbData[variablesNames.length];
                        if (variablesNames != null) {
                            for (int j = 0; j < variablesNames.length; j++) {
                                String attributName = ObjectUtils.EMPTY_STRING;
                                boolean isRead = true;
                                if (variablesNames[j].contains(ViewConfigurationAttribute.SUFFIX_WRITE)) {
                                    final int index = variablesNames[j]
                                            .indexOf(ViewConfigurationAttribute.SUFFIX_WRITE);
                                    attributName = variablesNames[j].substring(0, index);
                                    isRead = false;
                                }
                                if (variablesNames[j].contains(ViewConfigurationAttribute.SUFFIX_READ)) {
                                    final int index = variablesNames[j].indexOf(ViewConfigurationAttribute.SUFFIX_READ);
                                    attributName = variablesNames[j].substring(0, index);
                                    isRead = true;
                                }
                                final ViewConfigurationAttribute attr = attributes.get(attributName.toLowerCase());
                                if (attr != null) {
                                    switch (attr.getDataType(historic)) {
                                        case TangoConst.Tango_DEV_BOOLEAN:
                                        case TangoConst.Tango_DEV_CHAR:
                                        case TangoConst.Tango_DEV_UCHAR:
                                        case TangoConst.Tango_DEV_LONG:
                                        case TangoConst.Tango_DEV_ULONG:
                                        case TangoConst.Tango_DEV_SHORT:
                                        case TangoConst.Tango_DEV_USHORT:
                                        case TangoConst.Tango_DEV_FLOAT:
                                        case TangoConst.Tango_DEV_DOUBLE:
                                        case TangoConst.Tango_DEV_LONG64:
                                        case TangoConst.Tango_DEV_ULONG64:
                                            if (chart != null) {
                                                Map<String, Object> chartData = chart.getData();
                                                if (chartData != null) {
                                                    for (Map.Entry<String, Object> dataAttribute : chartData
                                                            .entrySet()) {
                                                        if (dataAttribute.getKey()
                                                                .equalsIgnoreCase(variablesNames[j])) {
                                                            Object points = dataAttribute.getValue();
                                                            int[] shape = ArrayUtils.recoverShape(points);
                                                            NullableTimedData[] timedDatas = null;
                                                            if (shape.length == 1) {
                                                                int nbPoints = shape[0] / 2;
                                                                timedDatas = new NullableTimedData[nbPoints];

                                                                // TODO this part must be replaced with a proper
                                                                // management, due to uses of DataArray2 using Double
                                                                // objects instead of double primitives

                                                                // TODO check whether it still works with single element
                                                                // arrays

                                                                double[] listPoints = null;
                                                                Double[] listPointsObject = null;
                                                                if (points instanceof double[]) {
                                                                    listPoints = (double[]) points;
                                                                } else if (points instanceof Double[]) {
                                                                    listPointsObject = (Double[]) points;
                                                                }

                                                                for (int i = 0; i < nbPoints; i++) {
                                                                    final NullableTimedData timedData = new NullableTimedData();
                                                                    if (listPoints != null) {
                                                                        timedData.setTime((long) listPoints[2 * i]);
                                                                        timedData.setValue(
                                                                                new double[] { listPoints[2 * i + 1] },
                                                                                null);
                                                                    } else if (listPointsObject != null) {
                                                                        timedData.setTime(
                                                                                listPointsObject[2 * i].longValue());
                                                                        timedData.setValue(new double[] {
                                                                                listPointsObject[2 * i + 1]
                                                                                        .doubleValue() },
                                                                                null);
                                                                    }
                                                                    timedDatas[i] = timedData;
                                                                }
                                                            } else if (shape.length == 2) {
                                                                int nbPoints = shape[1];
                                                                timedDatas = new NullableTimedData[nbPoints];
                                                                double[][] listPoints = (double[][]) points;
                                                                for (int i = 0; i < nbPoints; i++) {
                                                                    final NullableTimedData timedData = new NullableTimedData();
                                                                    timedData.setTime((long) listPoints[0][i]);
                                                                    timedData.setValue(
                                                                            new double[] { listPoints[1][i] }, null);
                                                                    timedDatas[i] = timedData;
                                                                }
                                                            }
                                                            final DbData dat = new DbData(attributName);
                                                            dat.setTimedData(timedDatas);
                                                            variablesDatas[j] = dat;
                                                            break;
                                                        } // end if (dataAttribute.getKey().equals(variablesNames[j]))
                                                    } // end for (Map.Entry<String, Object> dataAttribute :
                                                      // chartData.entrySet())
                                                    break;
                                                } // end if (chart != null)
                                            } // end if (chart != null)

                                        case TangoConst.Tango_DEV_STATE:
                                            final DbData[] splitedStateData = dataMap.get(attributName.toLowerCase());
                                            if (splitedStateData != null) {
                                                DbData data = null;
                                                if (isRead) {
                                                    data = splitedStateData[0];
                                                } else {
                                                    data = splitedStateData[1];
                                                }
                                                if (data != null) {
                                                    final NullableTimedData[] timedDatas = new NullableTimedData[data
                                                            .getTimedData().length];
                                                    for (int k = 0; k < data.getTimedData().length; k++) {
                                                        final NullableTimedData timedData = new NullableTimedData();
                                                        if (data.getTimedData()[k] != null) {
                                                            Object value = Array.get(data.getTimedData()[k].getValue(),
                                                                    0);
                                                            if (value != null) {
                                                                final String state = TangoConst.Tango_DevStateName[(Integer) value];
                                                                timedData.setValue(new String[] { state }, null);
                                                                timedData.setTime(data.getTimedData()[k].getTime());
                                                                timedDatas[k] = timedData;
                                                            }
                                                        }
                                                    }
                                                    final DbData dat = new DbData(attributName);
                                                    dat.setTimedData(timedDatas);
                                                    variablesDatas[j] = dat;
                                                    break;
                                                }
                                            }
                                        case TangoConst.Tango_DEV_STRING:
                                            final DbData[] splitedStringData = dataMap.get(attributName.toLowerCase());
                                            if (splitedStringData != null) {
                                                if (isRead) {
                                                    variablesDatas[j] = splitedStringData[0];
                                                } else {
                                                    variablesDatas[j] = splitedStringData[1];
                                                }
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            } // end for (int j = 0; j < variables.length; j++)

                            if (chart != null) {
                                final int timePrecision = chart.getTimePrecision();
                                // calculating curve data
                                DataBuilder expressionDataArray = constructCurveForStringExpression(attributeExpression,
                                        variablesDatas, timePrecision, historic);

                                // add expression to chart
                                if (expressionDataArray != null) {
                                    final MamboViewAdapterFactory daoFactory = MamboViewAdapterFactory.INSTANCE;
                                    if (currentViewId != null) {
                                        final ViewScalarAdapter dao = daoFactory.getDAOScalar(currentViewId, chart);
                                        if (dao != null) {
                                            attributeExpression.prepareView(chart);
                                            dao.addData(expressionDataArray);
                                        }
                                    }
                                }
                            }
                        }
                    } // end for (ExpressionAttribute attributeExpression : notNumberExpression)
                }
            } finally {
                notNumberExpression.clear();
            }
        }
    }

    private DataBuilder constructCurveForStringExpression(final ExpressionAttribute expressionAttribute,
            final DbData[] datas, final int timePrecision, final Boolean historic) {
        DataBuilder result = null;
        if (datas != null) {
            final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
            if (!extractingManager.isCanceled(historic)) {
                final IExpressionParser parser = new JEPExpressionParser();
                result = new DataBuilder(expressionAttribute.getId());
                final int[] currentIndex = new int[datas.length];

                boolean hasNextValue = true;
                // init index
                for (int i = 0; i < currentIndex.length; i++) {
                    currentIndex[i] = 0;
                    if (datas[i] != null) {
                        if (datas[i].getTimedData() != null) {
                            if (datas[i].getTimedData().length == 0) {
                                hasNextValue = false;
                            }
                        }
                    }
                }

                while (hasNextValue) {
                    if (extractingManager.isCanceled(historic)) {
                        break;
                    }
                    double minTime = Double.MAX_VALUE;
                    boolean allIndexAtEnd = true;
                    for (int i = 0; i < datas.length; i++) {
                        if (datas[i] != null) {
                            if (datas[i].getTimedData() != null) {
                                if (datas[i].getTimedData().length > currentIndex[i] + 1) {
                                    allIndexAtEnd = false;
                                    // Find min time value
                                    if (datas[i].getTimedData()[currentIndex[i]] != null) {
                                        if (minTime > datas[i].getTimedData()[currentIndex[i]].getTime()) {
                                            minTime = datas[i].getTimedData()[currentIndex[i]].getTime();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (minTime == Double.MAX_VALUE || allIndexAtEnd) {
                        hasNextValue = false;
                    }

                    for (int i = 0; (i < datas.length) && (!extractingManager.isCanceled(historic)); i++) {
                        if (datas[i] != null) {
                            if (datas[i].getTimedData()[currentIndex[i]] != null) {
                                if ((datas[i].getTimedData()[currentIndex[i]].getTime() >= minTime - timePrecision)
                                        && (datas[i].getTimedData()[currentIndex[i]].getTime() <= minTime
                                                + timePrecision)) {
                                    StringBuilder nameBuilder = new StringBuilder("x");
                                    if (!expressionAttribute.isX()) {
                                        nameBuilder.append(i + 1);
                                    }
                                    String variableName = nameBuilder.toString();
                                    parser.removeVariable(variableName);
                                    parser.addVariable(variableName,
                                            Array.get(datas[i].getTimedData()[currentIndex[i]].getValue(), 0));
                                    if (currentIndex[i] + 1 < datas[i].getTimedData().length) {
                                        currentIndex[i] = currentIndex[i] + 1;
                                    }
                                }
                            } else {
                                // if no data at current point
                                if (currentIndex[i] + 1 < datas[i].getTimedData().length) {
                                    currentIndex[i] = currentIndex[i] + 1;
                                }
                            }
                        }
                    } // end for (int i = 0; i < datas.length; i++)
                    if (extractingManager.isCanceled(historic)) {
                        break;
                    } else {
                        double value;
                        try {
                            value = parser.parseExpression(expressionAttribute.getExpression());
                        } catch (ExpressionEvaluationException e) {
                            value = Double.NaN;
                        }
                        if (Double.isNaN(value)) {
                            value = MathConst.NAN_FOR_NULL;
                        }
                        // do not consider minTime when not found
                        if (minTime != Double.MAX_VALUE) {
                            result.add(minTime, value);
                        }
                    }
                }
            } // end while (hasNextValue)
        }
        return result;
    }

    public void removeAttributesNotInList(final Collection<String> attrs) {
        Collection<String> toKeep = new ArrayList<>(attrs.size()), toRemove = new ArrayList<>(attributes.size());
        // Step 1: keep a list of lower cased attribute names to keep
        for (final String next : attrs) {
            if (next != null) {
                toKeep.add(next.toLowerCase());
            }
        }
        // Step 2: keep a list of attributes to remove
        for (String attribute : attributes.keySet()) {
            if (!toKeep.contains(attribute)) {
                toRemove.add(attribute);
            }
        }
        toKeep.clear();
        // Step 3: remove attributes
        for (String attribute : toRemove) {
            attributes.remove(attribute);
        }
        toRemove.clear();
    }

    public void addAttributes(final Map<String, ViewConfigurationAttribute> attrs) {
        for (Entry<String, ViewConfigurationAttribute> entry : attrs.entrySet()) {
            final String nextKey = entry.getKey().toLowerCase();
            final ViewConfigurationAttribute nextValue = entry.getValue();

            boolean needsColor = false;
            if (!attributes.containsKey(nextKey)) {
                attributes.put(nextKey, nextValue);

                needsColor = true;
                final ViewConfigurationAttributeProperties currentProperties = new ViewConfigurationAttributeProperties();
                nextValue.setProperties(currentProperties);
            } else {
                final ViewConfigurationAttributePlotProperties plotProperties = nextValue.getProperties()
                        .getPlotProperties();
                if (plotProperties.getCurve() == null) {
                    needsColor = true;
                }
            }
            if (needsColor) {
                int viewType = 0;
                int axisChoice = 0;
                final int spectrumViewType = Options.getInstance().getVcOptions().getSpectrumViewType();

                final CometeColor color = ColorTool
                        .getCometeColor(AttributesPlotPropertiesPanel.getColorFor(colorIndex));
                colorIndex = AttributesPlotPropertiesPanel.getDefaultNextColorIndex(colorIndex);

                PlotProperties properties = VCOptions.getDefaultPlotProperties();

                viewType = properties.getViewType();
                axisChoice = properties.getAxisChoice();

                Bar bar = new Bar(properties.getBar());
                bar.setFillColor(color);
                Curve curve = new Curve(properties.getCurve());
                curve.setColor(color);
                curve.setName(nextValue.getCompleteName());
                Marker marker = new Marker(properties.getMarker());
                marker.setColor(color);
                ErrorProperties error = properties.getError().clone();
                error.setColor(color);
                TransformationProperties transform = properties.getTransform().clone();
                OffsetProperties xOffset = properties.getXOffset().clone();
                Interpolation interpolation = new Interpolation(properties.getInterpolation());
                Smoothing smoothing = new Smoothing(properties.getSmoothing());
                MathPlot math = new MathPlot(properties.getMath());

                final boolean hidden = false;
                ViewConfigurationAttributeProperties currentProperties = nextValue.getProperties();
                ViewConfigurationAttributePlotProperties currentPlotProperties = new ViewConfigurationAttributePlotProperties(
                        viewType, axisChoice, null, ObjectUtils.EMPTY_STRING, bar, curve, marker, error, transform,
                        xOffset, interpolation, smoothing, math, spectrumViewType, hidden);
                currentProperties.setPlotProperties(currentPlotProperties);
                nextValue.setProperties(currentProperties);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public ViewConfigurationAttributes cloneAttrs() {
        final ViewConfigurationAttributes ret = new ViewConfigurationAttributes();
        ret.setAttributes((TreeMap<String, ViewConfigurationAttribute>) attributes.clone());
        ret.setColorIndex(colorIndex);
        return ret;
    }

    public int getColorIndex() {
        return colorIndex;
    }

    public void setColorIndex(final int colorIndex) {
        this.colorIndex = colorIndex;
    }

    public void setViewConfiguration(final ViewConfiguration viewConfiguration) {
        this.viewConfiguration = viewConfiguration;
    }

    public void setIdViewSelected(final String idViewSelected) {
        this.idViewSelected = idViewSelected;
    }

    public boolean equals(final ViewConfigurationAttributes vca) {
        boolean equals;
        if (attributes.size() == vca.attributes.size()) {
            equals = true;
            for (String key : attributes.keySet()) {
                if (!vca.containsAttribute(key)) {
                    equals = false;
                    break;
                }
            }
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        final XMLLine colorIndexEmptyLine = new XMLLine(COLOR_INDEX_XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);
        colorIndexEmptyLine.setAttribute(COLOR_INDEX_XML_ATTRIBUTE, Integer.toString(colorIndex));
        ret.append(colorIndexEmptyLine.toString());
        if (attributes != null) {
            int i = 0;
            for (final ViewConfigurationAttribute nextValue : attributes.values()) {
                ret.append(nextValue.toString());
                if (i < attributes.size() - 1) {
                    ret.append(GUIUtilities.CRLF);
                }
                i++;
            }
        }
        return ret.toString();
    }

}
