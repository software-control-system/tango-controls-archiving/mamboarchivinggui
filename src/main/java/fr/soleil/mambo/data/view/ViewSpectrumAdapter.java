package fr.soleil.mambo.data.view;

import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.mambo.datasources.db.extracting.ExtractingManagerFactory;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;
import fr.soleil.mambo.models.ViewSpectrumTableModel;

public class ViewSpectrumAdapter extends AbstractViewAdapter {

    private static final String COMPARISON_SEPARATOR = " - ";

    private WeakReference<ViewSpectrumTableModel> modelRef;

    public ViewSpectrumAdapter(IChartViewer chart) {
        super(chart);
    }

    public void setModel(ViewSpectrumTableModel model) {
        if (!ObjectUtils.sameObject(model, getModel())) {
            modelRef = (model == null ? null : new WeakReference<ViewSpectrumTableModel>(model));
            clean();
        }
    }

    protected ViewSpectrumTableModel getModel() {
        return ObjectUtils.recoverObject(modelRef);
    }

    @Override
    public void clean() {
        setData(null, -1, false, false);
    }

    private DataBuilder compare(Collection<Integer> selectedRows, int nameColumn, int booleanColumn,
            int spectrumViewType, DbData data) {
        DataBuilder result = null;
        for (Integer row : selectedRows) {
            DataBuilder dataArray = extractDataArray(row, nameColumn, booleanColumn, spectrumViewType, data);
            if (dataArray != null) {
                if (result == null) {
                    // this is the first array
                    result = dataArray;
                } else {
                    // this is the second array
                    StringBuilder idBuilder = new StringBuilder(result.getId());
                    idBuilder.append(COMPARISON_SEPARATOR).append(dataArray.getId());
                    result.setId(idBuilder.toString());
                    // compute subtraction
                    result.subtract(dataArray);
                }
            }
        }
        return result;
    }

    /**
     * Subtract mean from a DataArray. Incoming dataArray is modified in place.
     * 
     * @param dataArray
     *            the dataArray to modify
     */
    private void subtractMean(DataBuilder dataArray) {
        // compute the mean of this spectrum
        double sum = 0;
        for (double y : dataArray.getData()) {
            sum += y;
        }
        double size = dataArray.size();
        double mean = sum / size;

        // remove the mean from each point
        dataArray.subtract(mean);
    }

    private DataBuilder extractDataArray(int row, int nameColumn, int booleanColumn, int viewType, DbData data) {
        DataBuilder dataArray = null;
        ViewSpectrumTableModel model = getModel();
        if ((model != null) && (data != null) && (data.getTimedData() != null) && (row > -1)
                && (row < model.getRowCount()) && (nameColumn > -1) && (nameColumn < model.getColumnCount())
                && (booleanColumn > -1) && (booleanColumn < model.getColumnCount())) {
            Boolean selected = null;
            if (model.getValueAt(row, booleanColumn) instanceof Boolean) {
                selected = (Boolean) model.getValueAt(row, booleanColumn);
            }
            if (selected == null) {
                selected = Boolean.valueOf(false);
            }
            if (selected.booleanValue()) {
                switch (viewType) {
                case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX:
                    // Index spectrum : each DataArray is the history of an
                    // index in the spectrum
                    dataArray = new DataBuilder(String.valueOf(model.getValueAt(row, nameColumn)));
                    double[] value = new double[data.getTimedData().length * 2];
                    for (int timeIndex = 0; timeIndex < data.getTimedData().length; timeIndex++) {
                        NullableTimedData timedData = data.getTimedData()[timeIndex];
                        if (timedData.getTime() == 0) {
                            value[2 * timeIndex] = MathConst.NAN_FOR_NULL;
                        } else {
                            value[2 * timeIndex] = timedData.getTime();
                        }
                        boolean[] nullElements;
                        if (timedData.getNullElements() instanceof boolean[]) {
                            nullElements = (boolean[]) timedData.getNullElements();
                        } else {
                            nullElements = null;
                        }
                        if ((timedData.getValue() != null) && (row < Array.getLength(timedData.getValue()))) {
                            if ((nullElements == null) || (!nullElements[row])) {
                                value[2 * timeIndex + 1] = Array.getDouble(timedData.getValue(), row);
                            } else {
                                value[2 * timeIndex + 1] = MathConst.NAN_FOR_NULL;
                            }
                        } else {
                            value[2 * timeIndex + 1] = MathConst.NAN_FOR_NULL;
                        }
                    }
                    dataArray.setData(value);
                    break;
                case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME:
                case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME_STACK:
                    // Time spectrum : each DataArray is a full spectrum at a
                    // particular time
                    dataArray = new DataBuilder(String.valueOf(model.getValueAt(row, nameColumn)));
                    if (row < data.getTimedData().length) {
                        NullableTimedData timedData = data.getTimedData()[row];
                        boolean[] nullElements;
                        if (timedData.getNullElements() instanceof boolean[]) {
                            nullElements = (boolean[]) timedData.getNullElements();
                        } else {
                            nullElements = null;
                        }
                        for (int index = 0; index < data.getMaxX(); index++) {
                            double y;
                            if ((timedData.getValue() != null) && (index < Array.getLength(timedData.getValue()))
                                    && ((nullElements == null) || (!nullElements[index]))) {
                                y = Array.getDouble(timedData.getValue(), index);
                            } else {
                                y = MathConst.NAN_FOR_NULL;
                            }
                            dataArray.add(index, y);
                        }
                    }
                    break;
                default:
                    // Invalid view type
                    dataArray = null;
                    break;
                }
            } else {
                // Value is not selected
                dataArray = null;
            }
        }
        return dataArray;
    }

    public void setData(DbData[] splitedData, int spectrumViewType, boolean subtractMean, boolean compareMode) {
        List<DataBuilder> dataList = new ArrayList<DataBuilder>();
        ViewSpectrumTableModel model = getModel();
        if ((splitedData != null) && (model != null)) {
            int dataType = TangoConst.Tango_DEV_VOID;
            int readBooleanColumn = -1;
            int readNameColumn = -1;
            int writeBooleanColumn = -1;
            int writeNameColumn = -1;
            if (splitedData[0] == null) {
                dataType = splitedData[1].getDataType();
                writeBooleanColumn = 0;
                writeNameColumn = 1;
            } else {
                dataType = splitedData[0].getDataType();
                readBooleanColumn = 0;
                readNameColumn = 1;
                writeBooleanColumn = 3;
                writeNameColumn = 2;
            }
            switch (dataType) {
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_LONG64:
            case TangoConst.Tango_DEV_ULONG64:
            case TangoConst.Tango_DEV_FLOAT:
            case TangoConst.Tango_DEV_DOUBLE:
            case TangoConst.Tango_DEV_UCHAR:
                IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
                if (extractingManager != null) {
                    if (compareMode) {
                        Collection<Integer> selectedRead = model.getSelectedRead();
                        if (!selectedRead.isEmpty()) {
                            DataBuilder result = compare(selectedRead, readNameColumn, readBooleanColumn,
                                    spectrumViewType, splitedData[0]);
                            dataList.add(result);
                        }

                        Collection<Integer> selectedWrite = model.getSelectedWrite();
                        if (!selectedWrite.isEmpty()) {
                            DataBuilder result = compare(selectedWrite, writeNameColumn, writeBooleanColumn,
                                    spectrumViewType, splitedData[1]);
                            dataList.add(result);
                        }
                    } else {
                        for (int row = 0; row < model.getRowCount(); row++) {
                            if (extractingManager.isShowRead()) {
                                DataBuilder readDataArray = extractDataArray(row, readNameColumn, readBooleanColumn,
                                        spectrumViewType, splitedData[0]);

                                if (readDataArray != null) {
                                    if (subtractMean) {
                                        subtractMean(readDataArray);
                                    }
                                    dataList.add(readDataArray);
                                }
                            }

                            if (extractingManager.isShowWrite()) {
                                DataBuilder writeDataArray = extractDataArray(row, writeNameColumn, writeBooleanColumn,
                                        spectrumViewType, splitedData[1]);

                                if (writeDataArray != null) {
                                    if (subtractMean) {
                                        subtractMean(writeDataArray);
                                    }
                                    dataList.add(writeDataArray);
                                }
                            }
                        }
                    }
                }
            }
        }
        IChartViewer chart = getChart();
        // convert data
        if (chart != null) {
            Map<String, Object> data = new LinkedHashMap<String, Object>(dataList.size());
            for (DataBuilder item : dataList) {
                data.put(item.getId(), item.getData());
            }
            chart.setData(data);
        }
        dataList.clear();
    }

}
