// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/data/view/plot/Polynomial2OrderTransform.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class Polynomial2OrderTransform.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: Polynomial2OrderTransform.java,v $
// Revision 1.5 2007/02/27 10:06:32 ounsy
// corrected the transformation property bug
//
// Revision 1.4 2007/02/01 14:21:45 pierrejoseph
// XmlHelper reorg
//
// Revision 1.3 2007/01/11 14:05:45 ounsy
// Math Expressions Management (warning ! requires atk 2.7.0 or greater)
//
// Revision 1.2 2005/11/29 18:28:12 chinkumo
// no message
//
// Revision 1.1.2.2 2005/09/14 15:41:32 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.data.view.plot;

import java.util.HashMap;
import java.util.Map;

import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.tools.BasicObjectFormatter;

public class Polynomial2OrderTransform extends TransformationProperties {

    private static final long serialVersionUID = 7242378620916037569L;

    public static final String XML_TAG = "transform";
    public static final String A0_PROPERTY_XML_TAG = "a0";
    public static final String A1_PROPERTY_XML_TAG = "a1";
    public static final String A2_PROPERTY_XML_TAG = "a2";

    public Polynomial2OrderTransform() {
        super();
    }

    public Polynomial2OrderTransform(double _a0, double _a1, double _a2) {
        super(_a0, _a1, _a2);
    }

    public Polynomial2OrderTransform(TransformationProperties transformation) {
        super();
        if (transformation != null) {
            TransformationProperties clone = transformation.clone();
            setA0(clone.getA0());
            setA1(clone.getA1());
            setA2(clone.getA2());
        }
    }

    public Polynomial2OrderTransform(final Map<String, String> properties) {

        if (properties != null) {
            String a0 = properties.get(XML_TAG + "_" + A0_PROPERTY_XML_TAG);
            String a1 = properties.get(XML_TAG + "_" + A1_PROPERTY_XML_TAG);
            String a2 = properties.get(XML_TAG + "_" + A2_PROPERTY_XML_TAG);

            if (a0 != null) {
                setA0(BasicObjectFormatter.getDouble(a0));
            }
            if (a1 != null) {
                setA1(BasicObjectFormatter.getDouble(a1));
            }
            if (a2 != null) {
                setA2(BasicObjectFormatter.getDouble(a2));
            }
        }
    }

    public Map<String, String> getPolynomial2OrderTransformPropertiesList() {
        Map<String, String> result = new HashMap<String, String>();

        result.put(XML_TAG + "_" + A0_PROPERTY_XML_TAG, String.valueOf(getA0()));
        result.put(XML_TAG + "_" + A1_PROPERTY_XML_TAG, String.valueOf(getA1()));
        result.put(XML_TAG + "_" + A2_PROPERTY_XML_TAG, String.valueOf(getA2()));

        return result;
    }

    @Override
    public String toString() {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);

        openingLine.setAttribute(A0_PROPERTY_XML_TAG, String.valueOf(getA0()));
        openingLine.setAttribute(A1_PROPERTY_XML_TAG, String.valueOf(getA1()));
        openingLine.setAttribute(A2_PROPERTY_XML_TAG, String.valueOf(getA2()));

        return openingLine.toString();
    }
}
