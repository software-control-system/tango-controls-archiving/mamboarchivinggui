// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/data/view/ViewConfigurationAttributePlotProperties.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class
// ViewConfigurationAttributePlotProperties.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.8 $
//
// $Log: ViewConfigurationAttributePlotProperties.java,v $
// Revision 1.8 2007/03/20 14:15:09 ounsy
// added the possibility to hide a dataview
//
// Revision 1.7 2007/02/27 10:05:36 ounsy
// minor changes
//
// Revision 1.6 2007/02/01 14:21:46 pierrejoseph
// XmlHelper reorg
//
// Revision 1.5 2007/01/11 14:05:47 ounsy
// Math Expressions Management (warning ! requires atk 2.7.0 or greater)
//
// Revision 1.4 2006/09/27 07:00:16 chinkumo
// correction for Mantis 2230
//
// Revision 1.3 2006/09/20 13:03:29 chinkumo
// Mantis 2230: Separated Set addition in the "Attributes plot properties" tab
// of the New/Modify VC View
//
// Revision 1.2 2005/11/29 18:27:07 chinkumo
// no message
//
// Revision 1.1.2.2 2005/09/14 15:41:32 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.data.view;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.OffsetProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.data.view.plot.Bar;
import fr.soleil.mambo.data.view.plot.Curve;
import fr.soleil.mambo.data.view.plot.Interpolation;
import fr.soleil.mambo.data.view.plot.Marker;
import fr.soleil.mambo.data.view.plot.MathPlot;
import fr.soleil.mambo.data.view.plot.Polynomial2OrderTransform;
import fr.soleil.mambo.data.view.plot.Smoothing;

public class ViewConfigurationAttributePlotProperties extends PlotProperties {

    private static final long serialVersionUID = 762126186643956310L;

    private int spectrumViewType;
    private boolean hidden;
    private boolean isEmpty;

    public static final String XML_TAG = "plotParams";
    public static final String VIEW_TYPE_PROPERTY_XML_TAG = "viewType";
    public static final String AXIS_CHOICE_PROPERTY_XML_TAG = "axisChoice";
    public static final String SPECTRUM_VIEW_TYPE_PROPERTY_XML_TAG = "spectrumViewType";
    public static final String HIDDEN_PROPERTY_XML_TAG = "hidden";

    public static final int VIEW_TYPE_LINE = 0;
    public static final int VIEW_TYPE_BAR = 1;
    public static final int AXIS_CHOICE_Y1 = 0;
    public static final int AXIS_CHOICE_Y2 = 1;
    public static final int AXIS_CHOICE_X = 2;

    public static final int SPECTRUM_VIEW_TYPE_INDEX = 0;
    public static final int SPECTRUM_VIEW_TYPE_TIME = 1;
    public static final int SPECTRUM_VIEW_TYPE_TIME_STACK = 2;
    public static final int SPECTRUM_VIEW_TYPE_IMAGE = 3;

    public ViewConfigurationAttributePlotProperties() {
        super();
        isEmpty = true;
    }

    public ViewConfigurationAttributePlotProperties(int viewType, int axisChoice, String format, String unit,
            BarProperties bar, CurveProperties curve, MarkerProperties marker, ErrorProperties errorProperties,
            TransformationProperties transform, OffsetProperties xTransform, InterpolationProperties interpolation,
            SmoothingProperties smoothing, MathProperties math, int spectrumViewType, boolean hidden) {
        super(viewType, axisChoice, format, unit, bar, curve, marker, errorProperties, transform, xTransform,
                interpolation, smoothing, math);
        this.hidden = hidden;
        this.spectrumViewType = spectrumViewType;
        isEmpty = false;
    }

    public ViewConfigurationAttributePlotProperties(int viewType, int axisChoice, PlotProperties plotProperties,
            int spectrumViewType, boolean hidden) {
        super();
        if (plotProperties != null) {
            PlotProperties clone = plotProperties.clone();
            setViewType(viewType);
            setAxisChoice(axisChoice);
            setBar(clone.getBar());
            setCurve(clone.getCurve());
            setHidden(hidden);
            setInterpolation(clone.getInterpolation());
            setMarker(clone.getMarker());
            setMath(clone.getMath());
            setSmoothing(clone.getSmoothing());
            setSpectrumViewType(spectrumViewType);
            setTransform(clone.getTransform());
            setSpectrumViewType(spectrumViewType);
        }
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();

        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.OPENING_TAG_CATEGORY);
        openingLine.setAttribute(VIEW_TYPE_PROPERTY_XML_TAG, String.valueOf(getViewType()));
        openingLine.setAttribute(AXIS_CHOICE_PROPERTY_XML_TAG, String.valueOf(this.getAxisChoice()));
        openingLine.setAttribute(SPECTRUM_VIEW_TYPE_PROPERTY_XML_TAG, String.valueOf(this.spectrumViewType));
        openingLine.setAttribute(HIDDEN_PROPERTY_XML_TAG, String.valueOf(this.hidden));
        XMLLine closingLine = new XMLLine(XML_TAG, XMLLine.CLOSING_TAG_CATEGORY);

        openingLine.toAppendable(ret);
        ret.append(GUIUtilities.CRLF);

        if (getBar() != null) {
            ret.append(new Bar(getBar()).toString());
            ret.append(GUIUtilities.CRLF);
        }
        if (getCurve() != null) {
            ret.append(new Curve(getCurve()).toString());
            ret.append(GUIUtilities.CRLF);
        }
        if (getMarker() != null) {
            ret.append(new Marker(getMarker()).toString());
            ret.append(GUIUtilities.CRLF);
        }
        if (getTransform() != null) {
            ret.append(new Polynomial2OrderTransform(getTransform()).toString());
            ret.append(GUIUtilities.CRLF);
        }
        if (getInterpolation() != null) {
            ret.append(new Interpolation(getInterpolation()).toString());
            ret.append(GUIUtilities.CRLF);
        }
        if (getSmoothing() != null) {
            ret.append(new Smoothing(getSmoothing()).toString());
            ret.append(GUIUtilities.CRLF);
        }
        if (getMath() != null) {
            ret.append(new MathPlot(getMath()).toString());
            ret.append(GUIUtilities.CRLF);
        }

        closingLine.toAppendable(ret);

        return ret.toString();
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        isEmpty = false;
        this.hidden = hidden;
    }

    public int getSpectrumViewType() {
        return spectrumViewType;
    }

    public void setSpectrumViewType(int spectrumViewType) {
        isEmpty = false;
        this.spectrumViewType = spectrumViewType;
    }

    public void setPlotProperties(PlotProperties plotProterties) {
        if (plotProterties != null) {
            PlotProperties clone = plotProterties.clone();
            setViewType(clone.getViewType());
            setAxisChoice(clone.getAxisChoice());
            setBar(clone.getBar());
            setCurve(clone.getCurve());
            setInterpolation(clone.getInterpolation());
            setMarker(clone.getMarker());
            setMath(clone.getMath());
            setSmoothing(clone.getSmoothing());
            setTransform(clone.getTransform());
        }
    }
}
