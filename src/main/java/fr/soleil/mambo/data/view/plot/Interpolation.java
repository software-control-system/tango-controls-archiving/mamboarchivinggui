package fr.soleil.mambo.data.view.plot;

import java.util.HashMap;
import java.util.Map;

import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.tools.BasicObjectFormatter;

public class Interpolation extends InterpolationProperties {

    private static final long serialVersionUID = 7992754504099599845L;

    public static final String XML_TAG = "interpolation";
    public static final String INTERPOLATION_METHOD_PROPERTY_XML_TAG = "interpolationMethod";
    public static final String INTERPOLATION_STEP_PROPERTY_XML_TAG = "interpolationStep";
    public static final String HERMITE_BIAS_PROPERTY_XML_TAG = "hermiteBias";
    public static final String HERMITE_TENSION_PROPERTY_XML_TAG = "hermiteTension";

    public Interpolation() {
        super();
    }

    public Interpolation(int interpolationMethod, int interpolationStep, double hermiteBias, double hermiteTension) {
        super(interpolationMethod, interpolationStep, hermiteBias, hermiteTension);
    }

    public Interpolation(InterpolationProperties interpolation) {
        super();
        if (interpolation != null) {
            InterpolationProperties clone = interpolation.clone();
            setHermiteBias(clone.getHermiteBias());
            setHermiteTension(clone.getHermiteTension());
            setInterpolationMethod(clone.getInterpolationMethod());
            setInterpolationStep(clone.getInterpolationStep());
        }
    }

    public Interpolation(final Map<String, String> properties) {

        if (properties != null) {
            String interpolationMethod = properties.get(XML_TAG + "_" + INTERPOLATION_METHOD_PROPERTY_XML_TAG);
            String interpolationStep = properties.get(XML_TAG + "_" + INTERPOLATION_STEP_PROPERTY_XML_TAG);
            String hermiteBias = properties.get(XML_TAG + "_" + HERMITE_BIAS_PROPERTY_XML_TAG);
            String hermiteTension = properties.get(XML_TAG + "_" + HERMITE_TENSION_PROPERTY_XML_TAG);

            if (interpolationMethod != null) {
                setInterpolationMethod(BasicObjectFormatter.getInt(interpolationMethod));
            }
            if (interpolationStep != null) {
                setInterpolationStep(BasicObjectFormatter.getInt(interpolationStep));
            }
            if (hermiteBias != null) {
                setHermiteBias(BasicObjectFormatter.getDouble(hermiteBias));
            }
            if (hermiteTension != null) {
                setHermiteTension(BasicObjectFormatter.getDouble(hermiteTension));
            }
        }
    }

    public Map<String, String> getInterpolationPropertiesList() {
        Map<String, String> result = new HashMap<String, String>();

        result.put(XML_TAG + "_" + INTERPOLATION_METHOD_PROPERTY_XML_TAG, String.valueOf(getInterpolationMethod()));
        result.put(XML_TAG + "_" + INTERPOLATION_STEP_PROPERTY_XML_TAG, String.valueOf(getInterpolationStep()));
        result.put(XML_TAG + "_" + HERMITE_BIAS_PROPERTY_XML_TAG, String.valueOf(getHermiteBias()));
        result.put(XML_TAG + "_" + HERMITE_TENSION_PROPERTY_XML_TAG, String.valueOf(getHermiteTension()));

        return result;
    }

    @Override
    public String toString() {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);

        openingLine.setAttribute(INTERPOLATION_METHOD_PROPERTY_XML_TAG, String.valueOf(getInterpolationMethod()));
        openingLine.setAttribute(INTERPOLATION_STEP_PROPERTY_XML_TAG, String.valueOf(getInterpolationStep()));
        openingLine.setAttribute(HERMITE_BIAS_PROPERTY_XML_TAG, String.valueOf(getHermiteBias()));
        openingLine.setAttribute(HERMITE_TENSION_PROPERTY_XML_TAG, String.valueOf(getHermiteTension()));

        return openingLine.toString();
    }
}
