package fr.soleil.mambo.data.view;

import org.w3c.dom.Node;

import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.data.exception.KeyCompatibilityException;
import fr.soleil.data.service.AbstractKey;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.models.ViewSpectrumTableModel;

public class ViewAdapterKey extends AbstractKey {

    public final static int TYPE_NUMBER_SCALAR = 0;
//    public final static int TYPE_BOOLEAN_SCALAR = 1;
//    public final static int TYPE_STRING_SCALAR = 2;
//    public final static int TYPE_STATE_SCALAR = 3;

    public final static int TYPE_NUMBER_SPECTRUM = 4;
//    public final static int TYPE_BOOLEAN_SPECTRUM = 5;
//    public final static int TYPE_STRING_SPECTRUM = 6;
//    public final static int TYPE_STATE_SPECTRUM = 7;

    public ViewAdapterKey(int typeData, String idViewSelected) {
	super(MamboViewAdapterFactory.class.getName());
	registerProperty("viewSelectedId", idViewSelected);
	registerProperty("dataType", Integer.valueOf(typeData));
    }

    /**
     * Constructor for chart spectrum view
     *
     * @param typeData
     * @param splitedData
     * @param model
     * @param spectrumViewType
     * @param subtractMean
     * @param compareMode
     */
    public ViewAdapterKey(int typeData, DbData[] splitedData, ViewSpectrumTableModel model, Integer spectrumViewType,
	    boolean subtractMean, boolean compareMode) {
	super(MamboViewAdapterFactory.class.getName());
	registerProperty("dataType", Integer.valueOf(typeData));
	registerProperty("splitedData", splitedData);
	if (model != null) {
	    registerProperty("model", model);
	}
	if (spectrumViewType != null) {
	    registerProperty("spectrumType", spectrumViewType);
	}
	registerProperty("subtractMean", Boolean.valueOf(subtractMean));
	registerProperty("compareMode", Boolean.valueOf(compareMode));
    }

    /**
     * Constructor for stack chart view
     *
     * @param splitedData
     * @param checkBoxSelected
     */
    public ViewAdapterKey(DbData[] splitedData, int checkBoxSelected) {
	super(MamboViewAdapterFactory.class.getName());
	registerProperty("splitedData", splitedData);
	registerProperty("checkBoxSelected", checkBoxSelected);
    }

    @Override
    public String getInformationKey() {
	return null;
    }

    @Override
    public boolean isValid() {
	return true;
    }

    @Override
    public XMLLine[] toXMLLines(String tag) {
	return null;
    }

    @Override
    public void parseXML(Node xmlNode) throws KeyCompatibilityException {
    }

}
