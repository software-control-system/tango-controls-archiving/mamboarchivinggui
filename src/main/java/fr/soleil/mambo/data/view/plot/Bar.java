// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/data/view/plot/Bar.java,v $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class Bar.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: pierrejoseph $
//
// $Revision: 1.4 $
//
// $Log: Bar.java,v $
// Revision 1.4 2007/02/01 14:21:46 pierrejoseph
// XmlHelper reorg
//
// Revision 1.3 2007/01/11 14:05:45 ounsy
// Math Expressions Management (warning ! requires atk 2.7.0 or greater)
//
// Revision 1.2 2005/11/29 18:28:12 chinkumo
// no message
//
// Revision 1.1.2.2 2005/09/14 15:41:32 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.data.view.plot;

import java.util.HashMap;
import java.util.Map;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.tools.BasicObjectFormatter;

public class Bar extends BarProperties {

    private static final long serialVersionUID = -5275323686991379930L;

    public static final String XML_TAG = "bar";
    public static final String COLOR_PROPERTY_XML_TAG = "color";
    public static final String WIDTH_PROPERTY_XML_TAG = "width";
    public static final String FILL_STYLE_PROPERTY_XML_TAG = "fillStyle";
    public static final String FILL_METHOD_PROPERTY_XML_TAG = "fillMethod";

    public Bar() {
        super();
    }

    public Bar(CometeColor color, int width, int fillStyle, int fillingMethod) {
        super(color, width, fillStyle, fillingMethod);
    }

    public Bar(BarProperties bar) {
        super();
        if (bar != null) {
            BarProperties clone = bar.clone();
            setFillColor(clone.getFillColor());
            setFillingMethod(clone.getFillingMethod());
            setFillStyle(clone.getFillStyle());
        }
    }

    public Bar(final Map<String, String> properties) {

        if (properties != null) {
            String color = properties.get(XML_TAG + "_" + COLOR_PROPERTY_XML_TAG);
            String width = properties.get(XML_TAG + "_" + WIDTH_PROPERTY_XML_TAG);
            String fillStyle = properties.get(XML_TAG + "_" + FILL_STYLE_PROPERTY_XML_TAG);
            String fillMethod = properties.get(XML_TAG + "_" + FILL_METHOD_PROPERTY_XML_TAG);

            if (color != null) {
                setFillColor(BasicObjectFormatter.getCometeColor(color.split(",")));
            }
            if (width != null) {
                setWidth(BasicObjectFormatter.getInt(width));
            }
            if (fillStyle != null) {
                setFillStyle(BasicObjectFormatter.getInt(fillStyle));
            }
            if (fillMethod != null) {
                setFillingMethod(BasicObjectFormatter.getInt(fillMethod));
            }
        }
    }

    public Map<String, String> getBarPropertiesList() {
        Map<String, String> result = new HashMap<String, String>();

        result.put(XML_TAG + "_" + COLOR_PROPERTY_XML_TAG, BasicObjectFormatter.cometeColor(this.getFillColor()));
        result.put(XML_TAG + "_" + WIDTH_PROPERTY_XML_TAG, String.valueOf(getWidth()));
        result.put(XML_TAG + "_" + FILL_STYLE_PROPERTY_XML_TAG, String.valueOf(getFillStyle()));
        result.put(XML_TAG + "_" + FILL_METHOD_PROPERTY_XML_TAG, String.valueOf(getFillingMethod()));

        return result;
    }

    public boolean isEmpty() {
        return false;
    }

    @Override
    public String toString() {
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);

        openingLine
                .setAttribute(COLOR_PROPERTY_XML_TAG, GUIUtilities.colorToString(ColorTool.getColor(getFillColor())));
        openingLine.setAttribute(WIDTH_PROPERTY_XML_TAG, String.valueOf(getWidth()));
        openingLine.setAttribute(FILL_STYLE_PROPERTY_XML_TAG, String.valueOf(getFillStyle()));
        openingLine.setAttribute(FILL_METHOD_PROPERTY_XML_TAG, String.valueOf(getFillingMethod()));

        return openingLine.toString();
    }

}
