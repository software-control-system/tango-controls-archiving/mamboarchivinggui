package fr.soleil.mambo.data.view;

import java.util.Map;
import java.util.WeakHashMap;

import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.data.service.AbstractKey;
import fr.soleil.mambo.models.ViewSpectrumTableModel;

public enum MamboViewAdapterFactory {

    INSTANCE;

    private Map<String, ViewScalarAdapter> viewSelectedAdapter = new WeakHashMap<>();
    private Map<IChartViewer, AbstractKey> keyForChart = new WeakHashMap<>();

    // TODO table qui memorise le dao pour chaque chart, pour faire un clean par la suite

    public void setKeyForChart(IChartViewer chart, AbstractKey key) {
        if (chart != null) {
            if (key == null) {
                keyForChart.remove(chart);
            } else {
                keyForChart.put(chart, key);
            }
        }
    }

    public IViewAdapter createNumberDataArrayDAO(IChartViewer chart) {
        IViewAdapter adapter = null;
        AbstractKey key = keyForChart.get(chart);

        if (key != null) {
            Object typeData = key.getPropertyValue("dataType");
            if (typeData != null) {
                if (typeData instanceof Integer) {
                    switch ((Integer) typeData) {
                        // case ViewAdapterKey.TYPE_BOOLEAN_SPECTRUM:
                        case ViewAdapterKey.TYPE_NUMBER_SPECTRUM:
                            // case ViewAdapterKey.TYPE_STATE_SPECTRUM:
                            // case ViewAdapterKey.TYPE_STRING_SPECTRUM:
                            adapter = createNumberDataArrayDAOForSpectrum(key, chart);
                            break;
                        // case ViewAdapterKey.TYPE_BOOLEAN_SCALAR:
                        case ViewAdapterKey.TYPE_NUMBER_SCALAR:
                            // case ViewAdapterKey.TYPE_STATE_SCALAR:
                            // case ViewAdapterKey.TYPE_STRING_SCALAR:
                            adapter = createNumberDataArrayDAOForScalar(key, chart);
                            break;
                        default:
                            adapter = null;
                            break;
                    }
                }
            }
        }
        return adapter;
    }

    public IViewAdapter createStackNumberDataArrayDAO(AbstractKey key, IChartViewer chart, IPlayer player) {
        ViewSpectrumStackAdapter dao = null;
        if (key != null) {
            Object splitedData = key.getPropertyValue("splitedData");
            if (splitedData != null) {
                if (splitedData instanceof DbData[]) {
                    Object checkBoxSelected = key.getPropertyValue("checkBoxSelected");
                    if (checkBoxSelected != null) {
                        if (checkBoxSelected instanceof Integer) {
                            dao = new ViewSpectrumStackAdapter(chart, player);
                            dao.setData((DbData[]) splitedData, (Integer) checkBoxSelected);
                        }
                    }
                }
            }
        }
        return dao;
    }

    private IViewAdapter createNumberDataArrayDAOForSpectrum(AbstractKey key, IChartViewer chart) {
        ViewSpectrumAdapter dao = null;
        Object spectrumType = key.getPropertyValue("spectrumType");
        if (spectrumType != null && spectrumType instanceof Integer) {
            Object model = key.getPropertyValue("model");
            if (model != null && model instanceof ViewSpectrumTableModel) {
                Object splitedData = key.getPropertyValue("splitedData");
                if (splitedData != null && splitedData instanceof DbData[]) {
                    Object subtractMean = key.getPropertyValue("subtractMean");
                    if (subtractMean != null && subtractMean instanceof Boolean) {
                        Object compareMode = key.getPropertyValue("compareMode");
                        if (compareMode != null && compareMode instanceof Boolean) {
                            dao = new ViewSpectrumAdapter(chart);
                            dao.setModel((ViewSpectrumTableModel) model);
                            dao.setData((DbData[]) splitedData, (Integer) spectrumType, (Boolean) subtractMean,
                                    (Boolean) compareMode);
                        }
                    }
                }
            }
        }
        return dao;
    }

    private ViewScalarAdapter createNumberDataArrayDAOForScalar(AbstractKey key, IChartViewer chart) {
        ViewScalarAdapter daoScalar = null;
        Object idViewSelected = key.getPropertyValue("viewSelectedId");
        if (idViewSelected != null) {
            if (idViewSelected instanceof String) {
                daoScalar = getDAOScalar((String) idViewSelected, chart);
            }
        }
        return daoScalar;
    }

    public ViewScalarAdapter getDAOScalar(String idViewSelected, IChartViewer chart) {
        ViewScalarAdapter result = viewSelectedAdapter.get(idViewSelected);
        if (result == null) {
            result = new ViewScalarAdapter(chart);
            viewSelectedAdapter.put(idViewSelected, result);
        }
        return result;
    }

    public void removeKeyForDaoScalar(String idViewSelected) {
        ViewScalarAdapter previous = viewSelectedAdapter.remove(idViewSelected);
        if (previous != null) {
            previous.clean();
        }
    }

}
