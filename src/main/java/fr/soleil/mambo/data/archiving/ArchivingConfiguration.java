package fr.soleil.mambo.data.archiving;

import java.io.File;
import java.sql.Timestamp;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.Criterions;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.components.ConfigurationFileFilter;
import fr.soleil.mambo.components.archiving.ACFileFilter;
import fr.soleil.mambo.components.archiving.OpenedACComboBox;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.containers.archiving.ArchivingAttributesDetailPanel;
import fr.soleil.mambo.containers.archiving.ArchivingGeneralPanel;
import fr.soleil.mambo.containers.archiving.dialogs.ACEditDialog;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesPropertiesPanel;
import fr.soleil.mambo.containers.archiving.dialogs.GeneralTab;
import fr.soleil.mambo.data.IConfiguration;
import fr.soleil.mambo.datasources.file.ArchivingConfigurationManagerFactory;
import fr.soleil.mambo.datasources.file.IArchivingConfigurationManager;
import fr.soleil.mambo.models.ACAttributesTreeModel;
import fr.soleil.mambo.tools.MamboDbUtils;
import fr.soleil.mambo.tools.Messages;

public class ArchivingConfiguration implements IConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivingConfiguration.class);

    private static ArchivingConfiguration selectedArchivingConfiguration;
    private static ArchivingConfiguration[] openedArchivingConfigurations;

    private static ArchivingConfiguration currentArchivingConfiguration = null;

    private ArchivingConfigurationData data;
    private ArchivingConfigurationAttributes attributes;
    public static final String XML_TAG = "archivingConfiguration";

    private boolean isModified = true;
    private Boolean historic = MamboDbUtils.isHistoricAccordingToAvailableDb();

    public static final int EMPTY_ATTRIBUTES = 1;

    public ArchivingConfiguration() {
        data = new ArchivingConfigurationData();
        attributes = new ArchivingConfigurationAttributes();

        attributes.setArchivingConfiguration(this);
    }

    public ArchivingConfiguration(final ArchivingConfigurationData _data,
            final ArchivingConfigurationAttributes _attributes) {
        data = _data;
        attributes = _attributes;

        data.setArchivingConfiguration(this);
        attributes.setArchivingConfiguration(this);
    }

    /**
     * @param _contextData
     */
    public ArchivingConfiguration(final ArchivingConfigurationData _data) {
        data = _data;
    }

    @Override
    public boolean containsAttribute(final String completeName) {
        return attributes.getAttributesMap().containsKey(completeName);
    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        final XMLLine openingLine = new XMLLine(ArchivingConfiguration.XML_TAG, XMLLine.OPENING_TAG_CATEGORY);

        openingLine.setAttribute(ArchivingConfigurationData.IS_HISTORIC_PROPERTY_XML_TAG,
                historic + ObjectUtils.EMPTY_STRING);
        openingLine.setAttribute(ArchivingConfigurationData.IS_MODIFIED_PROPERTY_XML_TAG,
                isModified + ObjectUtils.EMPTY_STRING);

        if (getCreationDate() != null) {
            openingLine.setAttribute(ArchivingConfigurationData.CREATION_DATE_PROPERTY_XML_TAG,
                    getCreationDate().toString());
        }
        if (getLastUpdateDate() != null) {
            openingLine.setAttribute(ArchivingConfigurationData.LAST_UPDATE_DATE_PROPERTY_XML_TAG,
                    getLastUpdateDate().toString());
        }
        if (getName() != null) {
            openingLine.setAttribute(ArchivingConfigurationData.NAME_PROPERTY_XML_TAG, getName());
        }
        if (getPath() != null) {
            openingLine.setAttribute(ArchivingConfigurationData.PATH_PROPERTY_XML_TAG, getPath());
        }

        final XMLLine closingLine = new XMLLine(ArchivingConfiguration.XML_TAG, XMLLine.CLOSING_TAG_CATEGORY);

        ret += openingLine.toString();
        ret += GUIUtilities.CRLF;

        if (attributes != null) {
            ret += attributes.toString();
        }

        ret += closingLine.toString();

        // ret = XMLUtils.replaceXMLChars ( ret );

        return ret;
    }

    public static ArchivingConfiguration[] findArchivingConfigurations(final Criterions searchCriterions,
            final boolean forceGlobalReload) throws Exception {
        final IArchivingConfigurationManager manager = ArchivingConfigurationManagerFactory.getCurrentImpl();
        ArchivingConfiguration[] ret;

        if (openedArchivingConfigurations == null || forceGlobalReload) {
            ret = manager.loadArchivingConfigurations(searchCriterions);
        } else {
            ret = manager.findArchivingConfigurations(openedArchivingConfigurations, searchCriterions);
        }

        return ret;
    }

    /**
     * @return Returns the selectedArchivingConfiguration.
     */
    public static ArchivingConfiguration getSelectedArchivingConfiguration() {
        return selectedArchivingConfiguration;
    }

    /**
     * @param selectedArchivingConfiguration
     *            The selectedArchivingConfiguration to set.
     */
    public static void setSelectedArchivingConfiguration(final ArchivingConfiguration selectedArchivingConfiguration) {
        ArchivingConfiguration.selectedArchivingConfiguration = selectedArchivingConfiguration;
    }

    /**
     * @return Returns the attributes.
     */
    public ArchivingConfigurationAttributes getAttributes() {
        return attributes;
    }

    /**
     * @param attributes
     *            The attributes to set.
     */
    public void setAttributes(final ArchivingConfigurationAttributes attributes) {
        this.attributes = attributes;
    }

    /**
     * @return Returns the data.
     */
    public ArchivingConfigurationData getData() {
        return data;
    }

    /**
     * @param data
     *            The data to set.
     */
    public void setData(final ArchivingConfigurationData data) {
        this.data = data;
    }

    /**
     * @return Returns the currentArchivingConfiguration.
     */
    public static ArchivingConfiguration getCurrentArchivingConfiguration() {
        return currentArchivingConfiguration;
    }

    /**
     * @param currentArchivingConfiguration
     *            The currentArchivingConfiguration to set.
     */
    public static void setCurrentArchivingConfiguration(final ArchivingConfiguration currentArchivingConfiguration) {
        ArchivingConfiguration.currentArchivingConfiguration = currentArchivingConfiguration;
    }

    public String getName() {
        if (data == null) {
            return null;
        }

        return data.getName();
    }

    /**
     * @return Returns the creationDate.
     */
    public Timestamp getCreationDate() {
        if (data == null) {
            return null;
        }

        return data.getCreationDate();
    }

    /**
     * @param creationDate
     *            The creationDate to set.
     */
    public void setCreationDate(final Timestamp creationDate) {
        if (data != null) {
            data.setCreationDate(creationDate);
        }
    }

    /**
     * @return Returns the lastUpdateDate.
     */
    public Timestamp getLastUpdateDate() {
        if (data == null) {
            return null;
        }

        return data.getLastUpdateDate();
    }

    /**
     * @param lastUpdateDate
     *            The lastUpdateDate to set.
     */
    public void setLastUpdateDate(final Timestamp lastUpdateDate) {
        if (data != null) {
            data.setLastUpdateDate(lastUpdateDate);
        }
    }

    /**
     * @return 27 juil. 2005
     */
    public boolean isNew() {
        if (data == null) {
            return true;
        }

        return getCreationDate() == null;
    }

    public void push() {
        ArchivingConfiguration.setCurrentArchivingConfiguration(cloneAC());
        ArchivingConfiguration.setSelectedArchivingConfiguration(this);

        if (Mambo.hasACs()) {
            ArchivingAttributesDetailPanel.getInstance(historic).refresh(historic);

            final ACAttributesTreeModel model = ACAttributesTreeModel.getInstance();
            model.removeAll();
            if (ACEditDialog.getInstance() != null
                    && ACEditDialog.getInstance().getAttributeTableSelectionBean() != null) {
                ACEditDialog.getInstance().getAttributeTableSelectionBean().getSelectionPanel()
                        .getAttributesSelectTable().getModel().reset();
            }

            if (attributes != null) {
                attributes.push();
            }

            final ArchivingGeneralPanel archivingGeneralPanel = ArchivingGeneralPanel.getInstance();
            archivingGeneralPanel.setLastUpdateDate(getLastUpdateDate());
            archivingGeneralPanel.setCreationDate(getCreationDate());
            archivingGeneralPanel.setName(getName());
            archivingGeneralPanel.setPath(getPath());

            final GeneralTab generalTab = GeneralTab.getInstance();
            generalTab.setLastUpdateDate(getLastUpdateDate());
            generalTab.setCreationDate(getCreationDate());
            generalTab.setName(getName());
            generalTab.setHistoric(historic);

            final OpenedACComboBox openedACComboBox = OpenedACComboBox.getInstance();
            if (openedACComboBox != null) {
                openedACComboBox.selectElement(this);
            }
        }
    }

    public void pushLight() {
        ArchivingConfiguration.setCurrentArchivingConfiguration(cloneAC());

        if (Mambo.hasACs()) {
            final ACAttributesTreeModel model = ACAttributesTreeModel.getInstance();
            model.removeAll();
            if (ACEditDialog.getInstance() != null
                    && ACEditDialog.getInstance().getAttributeTableSelectionBean() != null) {
                ACEditDialog.getInstance().getAttributeTableSelectionBean().getSelectionPanel()
                        .getAttributesSelectTable().getModel().reset();
            }

            if (attributes != null) {
                attributes.push();
            }

            final ArchivingGeneralPanel archivingGeneralPanel = ArchivingGeneralPanel.getInstance();
            archivingGeneralPanel.setLastUpdateDate(getLastUpdateDate());
            archivingGeneralPanel.setCreationDate(getCreationDate());
            archivingGeneralPanel.setName(getName());
            archivingGeneralPanel.setPath(getPath());

            final GeneralTab generalTab = GeneralTab.getInstance();
            generalTab.setLastUpdateDate(getLastUpdateDate());
            generalTab.setCreationDate(getCreationDate());
            generalTab.setName(getName());
            generalTab.setHistoric(historic);
        }
    }

    private ArchivingConfiguration cloneAC() {
        final ArchivingConfiguration ret = new ArchivingConfiguration();

        ret.setModified(isModified());
        ret.setHistoric(isHistoric());
        ret.setPath(getPath());
        ret.setData(getData());
        ret.setAttributes(getAttributes().cloneAttrs());

        return ret;
    }

    /**
     * @return 9 sept. 2005
     */
    public void controlValues() throws ArchivingConfigurationException {
        if (attributes == null) {
            throw new ArchivingConfigurationException(null, EMPTY_ATTRIBUTES);
        } else {
            attributes.controlValues();
        }
    }

    /**
     * @throws ArchivingException
     */
    public static void updateCurrentModes(final Boolean currentACIsHistoric) throws ArchivingException {
        final ArchivingConfigurationAttributeProperties currentProperties = ArchivingConfigurationAttributeProperties
                .getCurrentProperties();
        updateCurrentAbsoluteMode(currentACIsHistoric);
        updateCurrentRelativeMode(currentACIsHistoric);
        updateCurrentThresholdMode(currentACIsHistoric);
        updateCurrentPeriodicMode(currentACIsHistoric);
        updateCurrentDifferenceMode(currentACIsHistoric);
        if (currentACIsHistoric == null) {
            currentProperties.setHDBProperties(new ArchivingConfigurationAttributeHDBProperties());
            currentProperties.setTDBProperties(new ArchivingConfigurationAttributeTDBProperties());
        } else if (currentACIsHistoric.booleanValue()) {
            currentProperties.setTDBProperties(new ArchivingConfigurationAttributeTDBProperties());
            currentProperties.setTTSProperties(new ArchivingConfigurationAttributeTTSProperties());
        } else {
            currentProperties.setHDBProperties(new ArchivingConfigurationAttributeHDBProperties());
            currentProperties.setTTSProperties(new ArchivingConfigurationAttributeTTSProperties());
        }

        ArchivingConfigurationAttributeProperties.setCurrentProperties(currentProperties);
    }

    /**
     * @param b
     * @throws ArchivingException
     */
    private static void updateCurrentAbsoluteMode(final Boolean historic) {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        final boolean selected;
        if (historic == null) {
            selected = panel.hasModeTTS_A();
        } else if (historic.booleanValue()) {
            selected = panel.hasModeHDB_A();
        } else {
            selected = panel.hasModeTDB_A();
        }

        final Mode mode = new Mode();
        final ArchivingConfigurationMode currentMode = new ArchivingConfigurationMode(mode);
        currentMode.setType(ArchivingConfigurationMode.TYPE_A);
        ArchivingConfigurationMode.setCurrentMode(currentMode);
        final ArchivingConfigurationAttributeProperties currentProperties = ArchivingConfigurationAttributeProperties
                .getCurrentProperties();

        if (!selected) {
            currentMode.getMode().setModeA(null);

            if (historic == null) {
                final ArchivingConfigurationAttributeTTSProperties TTSProperties = currentProperties.getTTSProperties();
                TTSProperties.removeMode(ArchivingConfigurationMode.TYPE_A);
                currentProperties.setTTSProperties(TTSProperties);
            } else if (historic.booleanValue()) {
                final ArchivingConfigurationAttributeHDBProperties HDBProperties = currentProperties.getHDBProperties();
                HDBProperties.removeMode(ArchivingConfigurationMode.TYPE_A);
                currentProperties.setHDBProperties(HDBProperties);
            } else {
                final ArchivingConfigurationAttributeTDBProperties TDBProperties = currentProperties.getTDBProperties();
                TDBProperties.removeMode(ArchivingConfigurationMode.TYPE_A);
                currentProperties.setTDBProperties(TDBProperties);
            }
        } else {
            final int absolutePeriod;
            double upperLimit, lowerLimit;
            final boolean slowDrift;
            if (historic == null) {
                absolutePeriod = panel.getTTSAbsolutePeriod();
                upperLimit = Double.parseDouble(panel.getTTSAbsoluteUpper());
                lowerLimit = Double.parseDouble(panel.getTTSAbsoluteLower());
                slowDrift = panel.isTTSAbsoluteDLCheck();
            } else if (historic.booleanValue()) {
                absolutePeriod = panel.getHDBAbsolutePeriod();
                upperLimit = Double.parseDouble(panel.getHDBAbsoluteUpper());
                lowerLimit = Double.parseDouble(panel.getHDBAbsoluteLower());
                slowDrift = panel.isHDBAbsoluteDLCheck();
            } else {
                absolutePeriod = panel.getTDBAbsolutePeriod();
                upperLimit = Double.parseDouble(panel.getTDBAbsoluteUpper());
                lowerLimit = Double.parseDouble(panel.getTDBAbsoluteLower());
                slowDrift = panel.isTDBAbsoluteDLCheck();
            }
            upperLimit = Math.abs(upperLimit);
            lowerLimit = Math.abs(lowerLimit);

            final ModeAbsolu ModeA = new ModeAbsolu(absolutePeriod, lowerLimit, upperLimit, slowDrift);

            currentMode.getMode().setModeA(ModeA);
            currentProperties.addMode(currentMode, historic);
        }

        ArchivingConfigurationAttributeProperties.setCurrentProperties(currentProperties);
    }

    /**
     * @param b
     * @throws ArchivingException
     */
    private static void updateCurrentRelativeMode(final Boolean historic) throws ArchivingException {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        final boolean selected;
        if (historic == null) {
            selected = panel.hasModeTTS_R();
        } else if (historic.booleanValue()) {
            selected = panel.hasModeHDB_R();
        } else {
            selected = panel.hasModeTDB_R();
        }

        final Mode mode = new Mode();
        final ArchivingConfigurationMode currentMode = new ArchivingConfigurationMode(mode);
        currentMode.setType(ArchivingConfigurationMode.TYPE_R);
        ArchivingConfigurationMode.setCurrentMode(currentMode);
        final ArchivingConfigurationAttributeProperties currentProperties = ArchivingConfigurationAttributeProperties
                .getCurrentProperties();

        if (selected) {
            final int relativePeriod;
            double upperLimit;
            double lowerLimit;
            final boolean slowDrift;
            if (historic == null) {
                relativePeriod = panel.getTTSRelativePeriod();
                upperLimit = Double.parseDouble(panel.getTTSRelativeUpper());
                lowerLimit = Double.parseDouble(panel.getTTSRelativeLower());
                slowDrift = panel.isTTSRelativeDLCheck();
            } else if (historic.booleanValue()) {
                relativePeriod = panel.getHDBRelativePeriod();
                upperLimit = Double.parseDouble(panel.getHDBRelativeUpper());
                lowerLimit = Double.parseDouble(panel.getHDBRelativeLower());
                slowDrift = panel.isHDBRelativeDLCheck();
            } else {
                relativePeriod = panel.getTDBRelativePeriod();
                upperLimit = Double.parseDouble(panel.getTDBRelativeUpper());
                lowerLimit = Double.parseDouble(panel.getTDBRelativeLower());
                slowDrift = panel.isTDBRelativeDLCheck();
            }
            upperLimit = Math.abs(upperLimit);
            lowerLimit = Math.abs(lowerLimit);

            final ModeRelatif ModeR = new ModeRelatif(relativePeriod, lowerLimit, upperLimit, slowDrift);

            currentMode.getMode().setModeR(ModeR);
            currentProperties.addMode(currentMode, historic);
        } else {
            currentMode.getMode().setModeR(null);

            if (historic == null) {
                final ArchivingConfigurationAttributeTTSProperties TTSProperties = currentProperties.getTTSProperties();
                TTSProperties.removeMode(ArchivingConfigurationMode.TYPE_R);
                currentProperties.setTTSProperties(TTSProperties);
            } else if (historic.booleanValue()) {
                final ArchivingConfigurationAttributeHDBProperties HDBProperties = currentProperties.getHDBProperties();
                HDBProperties.removeMode(ArchivingConfigurationMode.TYPE_R);
                currentProperties.setHDBProperties(HDBProperties);
            } else {
                final ArchivingConfigurationAttributeTDBProperties TDBProperties = currentProperties.getTDBProperties();
                TDBProperties.removeMode(ArchivingConfigurationMode.TYPE_R);
                currentProperties.setTDBProperties(TDBProperties);
            }
        }

        ArchivingConfigurationAttributeProperties.setCurrentProperties(currentProperties);
    }

    private static void updateCurrentThresholdMode(final Boolean historic) throws ArchivingException {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        final boolean selected;
        if (historic == null) {
            selected = panel.hasModeTTS_T();
        } else if (historic.booleanValue()) {
            selected = panel.hasModeHDB_T();
        } else {
            selected = panel.hasModeTDB_T();
        }

        final Mode mode = new Mode();
        final ArchivingConfigurationMode currentMode = new ArchivingConfigurationMode(mode);
        currentMode.setType(ArchivingConfigurationMode.TYPE_T);
        ArchivingConfigurationMode.setCurrentMode(currentMode);
        final ArchivingConfigurationAttributeProperties currentProperties = ArchivingConfigurationAttributeProperties
                .getCurrentProperties();

        if (selected) {
            final int thresholdPeriod;
            final double upperLimit;
            final double lowerLimit;
            if (historic == null) {
                thresholdPeriod = panel.getTTSThresholdPeriod();
                upperLimit = Double.parseDouble(panel.getTTSThresholdUpper());
                lowerLimit = Double.parseDouble(panel.getTTSThresholdLower());
            } else if (historic.booleanValue()) {
                thresholdPeriod = panel.getHDBThresholdPeriod();
                upperLimit = Double.parseDouble(panel.getHDBThresholdUpper());
                lowerLimit = Double.parseDouble(panel.getHDBThresholdLower());
            } else {
                thresholdPeriod = panel.getTDBThresholdPeriod();
                upperLimit = Double.parseDouble(panel.getTDBThresholdUpper());
                lowerLimit = Double.parseDouble(panel.getTDBThresholdLower());
            }
            final ModeSeuil ModeT = new ModeSeuil(thresholdPeriod, lowerLimit, upperLimit);

            currentMode.getMode().setModeT(ModeT);
            currentProperties.addMode(currentMode, historic);
        } else {
            currentMode.getMode().setModeT(null);
            if (historic == null) {
                final ArchivingConfigurationAttributeTTSProperties TTSProperties = currentProperties.getTTSProperties();
                TTSProperties.removeMode(ArchivingConfigurationMode.TYPE_T);
                currentProperties.setTTSProperties(TTSProperties);
            } else if (historic.booleanValue()) {
                final ArchivingConfigurationAttributeHDBProperties HDBProperties = currentProperties.getHDBProperties();
                HDBProperties.removeMode(ArchivingConfigurationMode.TYPE_T);
                currentProperties.setHDBProperties(HDBProperties);
            } else {
                final ArchivingConfigurationAttributeTDBProperties TDBProperties = currentProperties.getTDBProperties();
                TDBProperties.removeMode(ArchivingConfigurationMode.TYPE_T);
                currentProperties.setTDBProperties(TDBProperties);
            }
        }

        ArchivingConfigurationAttributeProperties.setCurrentProperties(currentProperties);
    }

    private static void updateCurrentPeriodicMode(final Boolean historic) throws ArchivingException {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        final boolean selected;
        if (historic == null) {
            selected = panel.hasModeTTS_P();
        } else if (historic.booleanValue()) {
            selected = panel.hasModeHDB_P();
        } else {
            selected = panel.hasModeTDB_P();
        }

        final Mode mode = new Mode();
        final ArchivingConfigurationMode currentMode = new ArchivingConfigurationMode(mode);
        currentMode.setType(ArchivingConfigurationMode.TYPE_P);
        ArchivingConfigurationMode.setCurrentMode(currentMode);
        final ArchivingConfigurationAttributeProperties currentProperties = ArchivingConfigurationAttributeProperties
                .getCurrentProperties();

        if (selected) {
            final int periodicPeriod;
            if (historic == null) {
                periodicPeriod = panel.getTTSperiod();
            } else if (historic.booleanValue()) {
                periodicPeriod = panel.getHDBperiod();
            } else {
                periodicPeriod = panel.getTDBperiod();
            }
            final ModePeriode ModeP = new ModePeriode(periodicPeriod);

            currentMode.getMode().setModeP(ModeP);
            currentProperties.addMode(currentMode, historic);
        } else {
            currentMode.getMode().setModeP(null);
            if (historic == null) {
                final ArchivingConfigurationAttributeTTSProperties TTSProperties = currentProperties.getTTSProperties();
                TTSProperties.removeMode(ArchivingConfigurationMode.TYPE_P);
                currentProperties.setTTSProperties(TTSProperties);
            } else if (historic.booleanValue()) {
                final ArchivingConfigurationAttributeHDBProperties HDBProperties = currentProperties.getHDBProperties();
                HDBProperties.removeMode(ArchivingConfigurationMode.TYPE_P);
                currentProperties.setHDBProperties(HDBProperties);
            } else {
                final ArchivingConfigurationAttributeTDBProperties TDBProperties = currentProperties.getTDBProperties();
                TDBProperties.removeMode(ArchivingConfigurationMode.TYPE_P);
                currentProperties.setTDBProperties(TDBProperties);
            }
        }

        ArchivingConfigurationAttributeProperties.setCurrentProperties(currentProperties);
    }

    private static void updateCurrentDifferenceMode(final Boolean historic) throws ArchivingException {
        final AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        final boolean selected;
        if (historic == null) {
            selected = panel.hasModeTTS_D();
        } else if (historic.booleanValue()) {
            selected = panel.hasModeHDB_D();
        } else {
            selected = panel.hasModeTDB_D();
        }

        final Mode mode = new Mode();
        final ArchivingConfigurationMode currentMode = new ArchivingConfigurationMode(mode);
        currentMode.setType(ArchivingConfigurationMode.TYPE_D);
        ArchivingConfigurationMode.setCurrentMode(currentMode);
        final ArchivingConfigurationAttributeProperties currentProperties = ArchivingConfigurationAttributeProperties
                .getCurrentProperties();

        if (selected) {
            final int differencePeriod;
            if (historic == null) {
                differencePeriod = panel.getTTSDifferencePeriod();
            } else if (historic.booleanValue()) {
                differencePeriod = panel.getHDBDifferencePeriod();
            } else {
                differencePeriod = panel.getTDBDifferencePeriod();
            }
            final ModeDifference ModeD = new ModeDifference(differencePeriod);

            currentMode.getMode().setModeD(ModeD);
            currentProperties.addMode(currentMode, historic);
        } else {
            currentMode.getMode().setModeD(null);
            if (historic == null) {
                final ArchivingConfigurationAttributeTTSProperties TTSProperties = currentProperties.getTTSProperties();
                TTSProperties.removeMode(ArchivingConfigurationMode.TYPE_D);
                currentProperties.setTTSProperties(TTSProperties);
            } else if (historic.booleanValue()) {
                final ArchivingConfigurationAttributeHDBProperties HDBProperties = currentProperties.getHDBProperties();
                HDBProperties.removeMode(ArchivingConfigurationMode.TYPE_D);
                currentProperties.setHDBProperties(HDBProperties);
            } else {
                final ArchivingConfigurationAttributeTDBProperties TDBProperties = currentProperties.getTDBProperties();
                TDBProperties.removeMode(ArchivingConfigurationMode.TYPE_D);
                currentProperties.setTDBProperties(TDBProperties);
            }
        }

        ArchivingConfigurationAttributeProperties.setCurrentProperties(currentProperties);
    }

    /**
     * @param name
     * @return
     */
    public ArchivingConfigurationAttribute getAttribute(final String name) {
        return attributes.getAttributesMap().get(name);
    }

    /**
     * @return Returns the isModified.
     */
    @Override
    public boolean isModified() {
        return isModified;
    }

    /**
     * @param isModified
     *            The isModified to set.
     */
    public void setModified(final boolean isModified) {
        this.isModified = isModified;
        OpenedACComboBox.refresh();
    }

    public boolean equals(final ArchivingConfiguration ac) {
        boolean ret;
        boolean dataEquals;
        boolean attrEquals;

        final String path1 = data.getPath() == null ? ObjectUtils.EMPTY_STRING : data.getPath();
        final String path2 = ac.data.getPath() == null ? ObjectUtils.EMPTY_STRING : ac.data.getPath();
        if ((!path1.isEmpty()) && path1.equals(path2)) {
            ret = true;
        } else {
            final ArchivingConfigurationData data1 = getData();
            final ArchivingConfigurationData data2 = ac.getData();
            if (data1 == null) {
                if (data2 == null) {
                    dataEquals = true;
                } else {
                    dataEquals = false;
                }
            } else {
                if (data2 == null) {
                    dataEquals = false;
                } else {
                    dataEquals = data1.equals(data2);
                }
            }

            final ArchivingConfigurationAttributes attrs1 = getAttributes();
            final ArchivingConfigurationAttributes attrs2 = ac.getAttributes();
            if (attrs1 == null) {
                if (attrs2 == null) {
                    attrEquals = true;
                } else {
                    attrEquals = false;
                }
            } else {
                if (attrs2 == null) {
                    attrEquals = false;
                } else {
                    attrEquals = attrs1.equals(attrs2);
                }
            }

            ret = dataEquals && attrEquals && ObjectUtils.sameObject(historic, ac.historic);
        }
        return ret;
    }

    /**
     * @param saveLocation
     */
    public void setPath(final String path) {
        if (data != null) {
            data.setPath(path);
        }

    }

    public String getPath() {
        return data == null ? null : data.getPath();
    }

    /**
     * @param name
     */
    public void setName(final String name) {
        if (data != null) {
            data.setName(name);
        }
    }

    public void save(final IArchivingConfigurationManager manager, final boolean saveAs) {
        final String pathToUse = getPathToUse(manager, saveAs);
        if (pathToUse != null) {
            boolean doSave = true;
            manager.setNonDefaultSaveLocation(pathToUse);
            try {
                setPath(pathToUse);
                setModified(false);
                manager.saveArchivingConfiguration(this);

                final String msg = Messages.getLogMessage("SAVE_ARCHIVING_CONFIGURATION_ACTION_OK");
                LOGGER.debug(msg);
            } catch (final Exception e) {
                final String msg = Messages.getLogMessage("SAVE_ARCHIVING_CONFIGURATION_ACTION_KO");
                LOGGER.error(msg, e);

                setPath(null);
                setModified(true);

                doSave = false;
            }
            if (doSave) {
                // ----WARNING!!!!!!!!!!!!!!!!!!!!!
                // this.push ();
                final ArchivingGeneralPanel panel = ArchivingGeneralPanel.getInstance();
                panel.setPath(getPath());
                // ----WARNING!!!!!!!!!!!!!!!!!!!!!
            }
        }
    }

    /**
     * @return
     */
    private String getPathToUse(final IArchivingConfigurationManager manager, final boolean saveAs) {
        final String pathToUse = getPath();
        final JFileChooser chooser = new JFileChooser();
        final ACFileFilter ACfilter = new ACFileFilter();
        chooser.addChoosableFileFilter(ACfilter);

        if (pathToUse != null) {
            if (saveAs) {
                chooser.setCurrentDirectory(new File(pathToUse));
            } else {
                return pathToUse;
            }
        } else {
            chooser.setCurrentDirectory(new File(manager.getDefaultSaveLocation()));
        }

        // ----show the user what he's saving
        final OpenedACComboBox openedACComboBox = OpenedACComboBox.getInstance();
        openedACComboBox.selectElement(this);
        // ----

        chooser.setDialogTitle(String.format(Messages.getMessage("DIALOGS_FILE_CHOOSER_SAVE_CONFIGURARTION_TITLE"),
                getDisplayableTitle()));
        final int returnVal = chooser.showSaveDialog(MamboFrame.getInstance());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            final File f = chooser.getSelectedFile();
            String path = f.getAbsolutePath();

            if (f != null) {
                final String extension = ConfigurationFileFilter.getExtension(f);
                final String expectedExtension = ACfilter.getExtension();

                if (extension == null || !extension.equalsIgnoreCase(expectedExtension)) {
                    path += ".";
                    path += expectedExtension;
                }
            }
            if (f.exists()) {
                final int choice = JOptionPane.showConfirmDialog(MamboFrame.getInstance(),
                        Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS"),
                        Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS_TITLE"), JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                if (choice != JOptionPane.OK_OPTION) {
                    return null;
                }
            }
            manager.setNonDefaultSaveLocation(path);
            return path;
        } else {
            return null;
        }

    }

    /**
     * @return Returns the isHistoric.
     */
    public Boolean isHistoric() {
        return historic;
    }

    /**
     * @param historic
     *            The isHistoric to set.
     */
    public void setHistoric(final Boolean historic) {
        this.historic = historic;
    }

    @Override
    public StringBuilder displayableTitleToStringBuilder(StringBuilder builder) {
        StringBuilder textBuffer = builder;
        if (textBuffer == null) {
            textBuffer = new StringBuilder();
        }
        textBuffer.append(data.getName());

        Timestamp lastUpdateDate = data.getLastUpdateDate();
        if (lastUpdateDate != null) {
            textBuffer.append(DISPAY_SEPARATOR).append(lastUpdateDate);
        }
        return textBuffer;
    }

    @Override
    public String getDisplayableTitle() {
        return displayableTitleToStringBuilder(null).toString();
    }
}
