package fr.soleil.mambo.data.archiving;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.sub.ACOptions;

public class ArchivingConfigurationAttributeProperties implements Cloneable {
    private ArchivingConfigurationAttributeHDBProperties hdbProperties;
    private ArchivingConfigurationAttributeTDBProperties tdbProperties;
    private ArchivingConfigurationAttributeTTSProperties ttsProperties;
    private ArchivingConfigurationAttribute attribute;

    private static ArchivingConfigurationAttributeProperties currentProperties = null;

    public ArchivingConfigurationAttributeProperties(ArchivingConfigurationAttributeHDBProperties hdbProperties,
            ArchivingConfigurationAttributeTDBProperties tdbProperties,
            ArchivingConfigurationAttributeTTSProperties ttsProperties) {
        this.hdbProperties = hdbProperties;
        this.tdbProperties = tdbProperties;
        this.ttsProperties = ttsProperties;

        this.hdbProperties.setProperties(this);
        this.tdbProperties.setProperties(this);
        this.ttsProperties.setProperties(this);
    }

    public ArchivingConfigurationAttributeProperties() {
        hdbProperties = new ArchivingConfigurationAttributeHDBProperties();
        tdbProperties = new ArchivingConfigurationAttributeTDBProperties();
        ttsProperties = new ArchivingConfigurationAttributeTTSProperties();

        hdbProperties.setProperties(this);
        tdbProperties.setProperties(this);
        ttsProperties.setProperties(this);
    }

    public ArchivingConfigurationAttributeHDBProperties getHDBProperties() {
        return hdbProperties;
    }

    public void setHDBProperties(ArchivingConfigurationAttributeHDBProperties properties) {
        hdbProperties = properties;
        if (hdbProperties != null) {
            hdbProperties.setProperties(this);
        }
    }

    public ArchivingConfigurationAttributeTDBProperties getTDBProperties() {
        return tdbProperties;
    }

    public void setTDBProperties(ArchivingConfigurationAttributeTDBProperties properties) {
        tdbProperties = properties;
        if (tdbProperties != null) {
            tdbProperties.setProperties(this);
        }
    }

    public ArchivingConfigurationAttributeTTSProperties getTTSProperties() {
        return ttsProperties;
    }

    public void setTTSProperties(ArchivingConfigurationAttributeTTSProperties properties) {
        ttsProperties = properties;
        if (ttsProperties != null) {
            ttsProperties.setProperties(this);
        }
    }

    /**
     * @return Returns the attribute.
     */
    public ArchivingConfigurationAttribute getAttribute() {
        return attribute;
    }

    /**
     * @param attribute
     *            The attribute to set.
     */
    public void setAttribute(ArchivingConfigurationAttribute attribute) {
        this.attribute = attribute;
    }

    /**
     * @return Returns the currentProperties.
     */
    public static ArchivingConfigurationAttributeProperties getCurrentProperties() {
        return currentProperties;
    }

    /**
     * @param currentProperties
     *            The currentProperties to set.
     */
    public static void setCurrentProperties(ArchivingConfigurationAttributeProperties currentProperties) {
        ArchivingConfigurationAttributeProperties.currentProperties = currentProperties;
    }

    /**
     * @param currentMode
     * @param historic
     *            25 juil. 2005
     */
    public void addMode(ArchivingConfigurationMode currentMode, Boolean historic) {
        if (historic == null) {
            ttsProperties.addMode(currentMode);
        } else if (historic.booleanValue()) {
            hdbProperties.addMode(currentMode);
        } else {
            tdbProperties.addMode(currentMode);
        }
    }

    /**
     * 26 juil. 2005
     */
    public void reset() {
        hdbProperties = new ArchivingConfigurationAttributeHDBProperties();
        tdbProperties = new ArchivingConfigurationAttributeTDBProperties();
        ttsProperties = new ArchivingConfigurationAttributeTTSProperties();

        hdbProperties.setProperties(this);
        tdbProperties.setProperties(this);
        ttsProperties.setProperties(this);
    }

    /**
     * 26 juil. 2005
     */
    public static void resetCurrentProperties() {
        // currentProperties = new ArchivingConfigurationAttributeProperties();
        Options options = Options.getInstance();
        ACOptions acOptions = options.getAcOptions();
        currentProperties = acOptions.getDefaultACProperties();

    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        ret += hdbProperties.toString();
        ret += GUIUtilities.CRLF;

        ret += tdbProperties.toString();
        ret += GUIUtilities.CRLF;

        return ret;
    }

    @Override
    public ArchivingConfigurationAttributeProperties clone() {
        ArchivingConfigurationAttributeProperties clone;
        try {
            clone = (ArchivingConfigurationAttributeProperties) super.clone();
            clone.attribute = null;
            if (hdbProperties != null) {
                clone.hdbProperties = hdbProperties.clone();
            }
            if (tdbProperties != null) {
                clone.tdbProperties = tdbProperties.clone();
            }
            if (ttsProperties != null) {
                clone.ttsProperties = ttsProperties.clone();
            }
        } catch (CloneNotSupportedException e) {
            // Will not happen as ArchivingConfigurationAttributeProperties
            clone = null;
        }
        return clone;
    }

}
