package fr.soleil.mambo.data.archiving;

import java.util.Collection;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesPropertiesPanel;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;

public class ArchivingConfigurationAttributeHDBProperties extends ArchivingConfigurationAttributeDBProperties {
    public static final String XML_TAG = "HDBModes";
    private int defaultPeriod = -1;

    public ArchivingConfigurationAttributeHDBProperties() {

    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.OPENING_TAG_CATEGORY);
        openingLine.setAttribute(ArchivingConfigurationAttributeDBProperties.DEDICATED_ARCHIVER_PROPERTY_XML_TAG,
                super.getDedicatedArchiver());
        XMLLine closingLine = new XMLLine(XML_TAG, XMLLine.CLOSING_TAG_CATEGORY);

        ret += openingLine.toString();
        ret += GUIUtilities.CRLF;

        ret += super.toString();
        ret += GUIUtilities.CRLF;

        ret += closingLine.toString();

        return ret;
    }

    @Override
    public void push() {
        super.push();

        Collection<Integer> modesTypes = super.getModesTypes();

        AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        panel.setSelectedModes(Boolean.TRUE, modesTypes);

        // MODE P START
        ArchivingConfigurationMode periodicalMode = super.getMode(ArchivingConfigurationMode.TYPE_P);
        if (periodicalMode != null && periodicalMode.getMode() != null && periodicalMode.getMode().getModeP() != null) {
            int period = periodicalMode.getMode().getModeP().getPeriod();
            panel.setHDBPeriod(period);
        } else if (defaultPeriod != -1) {
            panel.setHDBPeriod(defaultPeriod);
        } else {
            panel.setHDBPeriod(Integer.parseInt(AttributesPropertiesPanel.DEFAULT_HDB_PERIOD_VALUE));
        }
        // MODE P END

        // MODE A START
        ArchivingConfigurationMode absoluteMode = super.getMode(ArchivingConfigurationMode.TYPE_A);
        if (absoluteMode != null && absoluteMode.getMode() != null && absoluteMode.getMode().getModeA() != null) {
            ModeAbsolu modeA = absoluteMode.getMode().getModeA();

            int period = modeA.getPeriod();
            double lower = modeA.getValInf();
            double upper = modeA.getValSup();
            boolean slow_drift = modeA.isSlow_drift();
            panel.setHDBAbsolutePeriod(period);
            panel.setHDBAbsoluteLower(String.valueOf(lower));
            panel.setHDBAbsoluteUpper(String.valueOf(upper));
            panel.setHDBAbsoluteDLCheck(slow_drift);
        } else {
            panel.setHDBAbsolutePeriod(-1);
            panel.setHDBAbsoluteLower(ObjectUtils.EMPTY_STRING);
            panel.setHDBAbsoluteUpper(ObjectUtils.EMPTY_STRING);
            panel.setHDBAbsoluteDLCheck(false);
        }
        // MODE A END

        // MODE R START
        ArchivingConfigurationMode relativeMode = super.getMode(ArchivingConfigurationMode.TYPE_R);
        if (relativeMode != null && relativeMode.getMode() != null && relativeMode.getMode().getModeR() != null) {
            ModeRelatif modeR = relativeMode.getMode().getModeR();

            int period = modeR.getPeriod();
            double lower = modeR.getPercentInf();
            double upper = modeR.getPercentSup();
            boolean slow_drift = modeR.isSlow_drift();

            panel.setHDBRelativePeriod(period);
            panel.setHDBRelativeLower(String.valueOf(lower));
            panel.setHDBRelativeUpper(String.valueOf(upper));
            panel.setHDBRelativeDLCheck(slow_drift);
        } else {
            panel.setHDBRelativePeriod(-1);
            panel.setHDBRelativeLower(ObjectUtils.EMPTY_STRING);
            panel.setHDBRelativeUpper(ObjectUtils.EMPTY_STRING);
            panel.setHDBRelativeDLCheck(false);
        }
        // MODE R END

        // MODE T START
        ArchivingConfigurationMode thresholdMode = super.getMode(ArchivingConfigurationMode.TYPE_T);
        if (thresholdMode != null && thresholdMode.getMode() != null && thresholdMode.getMode().getModeT() != null) {
            ModeSeuil modeT = thresholdMode.getMode().getModeT();

            int period = modeT.getPeriod();
            double lower = modeT.getThresholdInf();
            double upper = modeT.getThresholdSup();

            panel.setHDBThresholdPeriod(period);
            panel.setHDBThresholdLower(String.valueOf(lower));
            panel.setHDBThresholdUpper(String.valueOf(upper));
        } else {
            panel.setHDBThresholdPeriod(-1);
            panel.setHDBThresholdLower(ObjectUtils.EMPTY_STRING);
            panel.setHDBThresholdUpper(ObjectUtils.EMPTY_STRING);
        }
        // MODE T END

        // MODE D START
        ArchivingConfigurationMode differenceMode = super.getMode(ArchivingConfigurationMode.TYPE_D);
        if (differenceMode != null && differenceMode.getMode() != null && differenceMode.getMode().getModeD() != null) {
            ModeDifference modeD = differenceMode.getMode().getModeD();

            int period = modeD.getPeriod();
            panel.setHDBDifferencePeriod(period);
        } else {
            panel.setHDBDifferencePeriod(-1);
        }
        // MODE D END
    }

    public void setDefaultPeriod(int period) {
        this.defaultPeriod = period;
        ArchivingConfigurationMode ACPMode = this.getMode(ArchivingConfigurationMode.TYPE_P);
        if (ACPMode != null) {
            ACPMode.getMode().getModeP().setPeriod(period);
        }

    }

    public static ArchivingConfigurationAttributeHDBProperties loadHDBProperties(String completeName) throws Exception {
        IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
        Mode mode = manager.getArchivingMode(completeName, Boolean.TRUE);
        ArchivingConfigurationMode[] ACModes = ArchivingConfigurationMode.buildModesList(mode);
        ArchivingConfigurationAttributeHDBProperties ret = new ArchivingConfigurationAttributeHDBProperties();
        ret.setModes(ACModes);
        return ret;
    }

    @Override
    public void controlValues() throws ArchivingConfigurationException {
        if (!isEmpty()) {
            super.controlValues(1000);
        }
    }

    @Override
    public ArchivingConfigurationAttributeHDBProperties clone() {
        return (ArchivingConfigurationAttributeHDBProperties) super.clone();
    }

}
