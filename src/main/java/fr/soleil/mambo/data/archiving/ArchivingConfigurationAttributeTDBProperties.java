package fr.soleil.mambo.data.archiving;

import java.util.Collection;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesPropertiesPanel;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;

public class ArchivingConfigurationAttributeTDBProperties extends ArchivingConfigurationAttributeDBProperties {
    private long exportPeriod = -1;

    public static final String XML_TAG = "TDBModes";
    public static final String TDB_SPEC_EXPORT_PERIOD_XML_TAG = "exportPeriod";

    private int defaultPeriod;
    private static final long EXPORT_PERIOD_MIN = 1000;
    public static final int EXPORT_PERIOD_TOO_LOW = 2;

    public ArchivingConfigurationAttributeTDBProperties() {
        super();
    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;
        XMLLine openingLine = new XMLLine(XML_TAG, XMLLine.OPENING_TAG_CATEGORY);
        openingLine.setAttribute(TDB_SPEC_EXPORT_PERIOD_XML_TAG, String.valueOf(exportPeriod));
        openingLine.setAttribute(ArchivingConfigurationAttributeDBProperties.DEDICATED_ARCHIVER_PROPERTY_XML_TAG,
                super.getDedicatedArchiver());
        XMLLine closingLine = new XMLLine(XML_TAG, XMLLine.CLOSING_TAG_CATEGORY);

        ret += openingLine.toString();
        ret += GUIUtilities.CRLF;

        ret += super.toString();
        ret += GUIUtilities.CRLF;

        ret += closingLine.toString();

        return ret;
    }

    /**
     * @return Returns the exportPeriod.
     */
    public long getExportPeriod() {
        return exportPeriod;
    }

    /**
     * @param exportPeriod
     *            The exportPeriod to set.
     */
    public void setExportPeriod(long exportPeriod) {
        this.exportPeriod = exportPeriod;
    }

    /**
     * 25 juil. 2005
     */
    @Override
    public void push() {
        super.push();

        Collection<Integer> modesTypes = super.getModesTypes();
        AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();
        panel.setSelectedModes(Boolean.FALSE, modesTypes);

        ArchivingConfigurationMode periodicalMode = super.getMode(ArchivingConfigurationMode.TYPE_P);

        if (periodicalMode != null && periodicalMode.getMode() != null && periodicalMode.getMode().getModeP() != null) {
            int period = periodicalMode.getMode().getModeP().getPeriod();
            panel.setTDBPeriod(period);
        } else {
            panel.setTDBPeriod(Integer.parseInt(AttributesPropertiesPanel.DEFAULT_TDB_PERIOD_VALUE));
        }

        panel.setExportPeriod(this.exportPeriod);

        // MODE A START
        ArchivingConfigurationMode absoluteMode = super.getMode(ArchivingConfigurationMode.TYPE_A);
        if (absoluteMode != null && absoluteMode.getMode() != null && absoluteMode.getMode().getModeA() != null) {
            ModeAbsolu modeA = absoluteMode.getMode().getModeA();

            int period = modeA.getPeriod();
            double lower = modeA.getValInf();
            double upper = modeA.getValSup();
            boolean slow_drift = modeA.isSlow_drift();
            panel.setTDBAbsolutePeriod(period);
            panel.setTDBAbsoluteLower(String.valueOf(lower));
            panel.setTDBAbsoluteUpper(String.valueOf(upper));
            panel.setTDBAbsoluteDLCheck(slow_drift);
        } else {
            panel.setTDBAbsolutePeriod(-1);
            panel.setTDBAbsoluteLower(ObjectUtils.EMPTY_STRING);
            panel.setTDBAbsoluteUpper(ObjectUtils.EMPTY_STRING);
            panel.setTDBAbsoluteDLCheck(false);
        }
        // MODE A END

        // MODE R START
        ArchivingConfigurationMode relativeMode = super.getMode(ArchivingConfigurationMode.TYPE_R);
        if (relativeMode != null && relativeMode.getMode() != null && relativeMode.getMode().getModeR() != null) {
            ModeRelatif modeR = relativeMode.getMode().getModeR();

            int period = modeR.getPeriod();
            double lower = modeR.getPercentInf();
            double upper = modeR.getPercentSup();
            boolean slow_drift = modeR.isSlow_drift();

            panel.setTDBRelativePeriod(period);
            panel.setTDBRelativeLower(String.valueOf(lower));
            panel.setTDBRelativeUpper(String.valueOf(upper));
            panel.setTDBRelativeDLCheck(slow_drift);
        } else {
            panel.setTDBRelativePeriod(-1);
            panel.setTDBRelativeLower(ObjectUtils.EMPTY_STRING);
            panel.setTDBRelativeUpper(ObjectUtils.EMPTY_STRING);
            panel.setTDBRelativeDLCheck(false);
        }
        // MODE R END

        // MODE T START
        ArchivingConfigurationMode thresholdMode = super.getMode(ArchivingConfigurationMode.TYPE_T);
        if (thresholdMode != null && thresholdMode.getMode() != null && thresholdMode.getMode().getModeT() != null) {
            ModeSeuil modeT = thresholdMode.getMode().getModeT();

            int period = modeT.getPeriod();
            double lower = modeT.getThresholdInf();
            double upper = modeT.getThresholdSup();

            panel.setTDBThresholdPeriod(period);
            panel.setTDBThresholdLower(String.valueOf(lower));
            panel.setTDBThresholdUpper(String.valueOf(upper));
        } else {
            panel.setTDBThresholdPeriod(-1);
            panel.setTDBThresholdLower(ObjectUtils.EMPTY_STRING);
            panel.setTDBThresholdUpper(ObjectUtils.EMPTY_STRING);
        }
        // MODE T END

        // MODE D START
        ArchivingConfigurationMode differenceMode = super.getMode(ArchivingConfigurationMode.TYPE_D);
        if (differenceMode != null && differenceMode.getMode() != null && differenceMode.getMode().getModeD() != null) {
            ModeDifference modeD = differenceMode.getMode().getModeD();

            int period = modeD.getPeriod();
            panel.setTDBDifferencePeriod(period);
        } else {
            panel.setTDBDifferencePeriod(-1);
        }
        // MODE D END
    }

    public int getDefaultPeriod() {
        return defaultPeriod;
    }

    /**
     * @param period
     *            26 juil. 2005
     */
    public void setDefaultPeriod(int period) {
        this.defaultPeriod = period / 1000;
        ArchivingConfigurationMode ACPMode = this.getMode(ArchivingConfigurationMode.TYPE_P);
        if (ACPMode != null) {
            ACPMode.getMode().getModeP().setPeriod(period);
        }
    }

    /**
     * @param completeName
     * @return 28 juil. 2005
     * @throws Exception
     */
    public static ArchivingConfigurationAttributeTDBProperties loadTDBProperties(String completeName) throws Exception {
        IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
        Mode mode = manager.getArchivingMode(completeName, Boolean.FALSE);
        ArchivingConfigurationMode[] ACModes = ArchivingConfigurationMode.buildModesList(mode);
        ArchivingConfigurationAttributeTDBProperties ret = new ArchivingConfigurationAttributeTDBProperties();
        ret.setModes(ACModes);
        return ret;
    }

    /**
     * 9 sept. 2005
     * 
     * @throws ArchivingConfigurationException
     * 
     */
    @Override
    public void controlValues() throws ArchivingConfigurationException {
        if (!isEmpty()) {
            if (exportPeriod < EXPORT_PERIOD_MIN) {
                throw new ArchivingConfigurationException(null, EXPORT_PERIOD_TOO_LOW);
            }
            super.controlValues(100);
        }
    }

    @Override
    public ArchivingConfigurationAttributeTDBProperties clone() {
        return (ArchivingConfigurationAttributeTDBProperties) super.clone();
    }

}
