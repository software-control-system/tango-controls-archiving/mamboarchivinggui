package fr.soleil.mambo.data.archiving;

import java.text.Collator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Attributes;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Family;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.containers.archiving.dialogs.ACEditDialog;
import fr.soleil.mambo.models.ACAttributesTreeModel;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.sub.ACOptions;

public class ArchivingConfigurationAttributes extends Attributes {

    private static final long serialVersionUID = 4595279946933199320L;

    private Map<String, ArchivingConfigurationAttribute> attributes = new TreeMap<>(Collator.getInstance());
    private ArchivingConfiguration archivingConfiguration;

    public ArchivingConfigurationAttributes() {

    }

    /**
     * @param attributes
     */
    public ArchivingConfigurationAttributes(final ArchivingConfigurationAttribute[] attributes) {
        super(attributes);

        if (attributes != null) {
            for (final ArchivingConfigurationAttribute attribute : attributes) {
                if (attribute != null) {
                    attribute.setGroup(this);
                }
            }
        }
    }

    public boolean equals(final ArchivingConfigurationAttributes attrs) {
        boolean equals;
        if (attributes.size() == attrs.attributes.size()) {
            equals = true;
            for (String key : attributes.keySet()) {
                if (!attrs.attributes.containsKey(key)) {
                    equals = false;
                    break;
                }
            }
        } else {
            equals = false;
        }
        return equals;
    }

    public void addAttribute(final ArchivingConfigurationAttribute attribute) {
        attribute.setGroup(this);
        String completeName = attribute.getCompleteName();
        if (completeName.endsWith(TangoDeviceHelper.SLASH)) {
            completeName = completeName.substring(0, completeName.length() - 1);
        }
        attributes.put(completeName, attribute);
    }

    public ArchivingConfigurationAttribute getAttribute(final String completeName) {
        return attributes.get(completeName);
    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        if (attributes != null) {
            final Set<String> keySet = attributes.keySet();
            final Iterator<String> keyIterator = keySet.iterator();
            int i = 0;
            while (keyIterator.hasNext()) {
                final String nextKey = keyIterator.next();
                final ArchivingConfigurationAttribute nextValue = attributes.get(nextKey);

                ret += nextValue.toString();
                if (i < attributes.size() - 1) {
                    ret += GUIUtilities.CRLF;
                }

                i++;
            }
        }

        return ret;
    }

    /**
     * @return Returns the archivingConfiguration.
     */
    public ArchivingConfiguration getArchivingConfiguration() {
        return archivingConfiguration;
    }

    /**
     * @param archivingConfiguration
     *            The archivingConfiguration to set.
     */
    public void setArchivingConfiguration(final ArchivingConfiguration archivingConfiguration) {
        this.archivingConfiguration = archivingConfiguration;
    }

    /**
     * @return Returns the attributes.
     */
    public Map<String, ArchivingConfigurationAttribute> getAttributesMap() {
        return attributes;
    }

    public ArchivingConfigurationAttribute[] getAttributesList() {
        final ArchivingConfigurationAttribute[] ret = new ArchivingConfigurationAttribute[attributes.size()];
        final Set<String> keySet = attributes.keySet();
        final Iterator<String> keyIterator = keySet.iterator();
        int i = 0;
        while (keyIterator.hasNext()) {
            final String nextKey = keyIterator.next();
            final ArchivingConfigurationAttribute nextValue = attributes.get(nextKey);
            ret[i] = nextValue;
            i++;
        }
        return ret;
    }

    public void push() {
        if (getAttributesMap() != null) {
            final Options options = Options.getInstance();
            final ACOptions acOptions = options.getAcOptions();
            if (acOptions.isAlternateSelectionMode()) {
                final ArchivingConfigurationAttribute[] list = getListCLA(getAttributesMap());
                reloadTableModelCLA(list);
            }
            final List<Domain> domains = getDomainsCLA(getAttributesMap());
            reloadTreeModelCLA(domains);
        }
    }

    private void reloadTableModelCLA(final ArchivingConfigurationAttribute[] list) {
        if (ACEditDialog.getInstance() != null && ACEditDialog.getInstance().getAttributeTableSelectionBean() != null) {
            ACEditDialog.getInstance().getAttributeTableSelectionBean().getSelectionPanel().getAttributesSelectTable()
                    .getModel().setRows(list);
        }
    }

    private void reloadTreeModelCLA(final List<Domain> _domains) {
        final ACAttributesTreeModel model = ACAttributesTreeModel.getInstance();
        model.build(_domains);
        model.reload();
    }

    private List<Domain> getDomainsCLA(final Map<String, ArchivingConfigurationAttribute> attributesHash) {
        final Set<String> keySet = attributesHash.keySet();
        final Iterator<String> keyIterator = keySet.iterator();
        List<Domain> _domains = new ArrayList<Domain>();
        final ArchivingConfigurationAttribute[] _list = new ArchivingConfigurationAttribute[attributesHash.size()];
        int i = 0;

        while (keyIterator.hasNext()) {
            final String completeName = keyIterator.next();
            final ArchivingConfigurationAttribute next = attributesHash.get(completeName);
            _list[i] = next;
            i++;

            final String domain_s = next.getDomainName();
            final String family_s = next.getFamilyName();
            final String member_s = next.getMemberName();

            final Domain domain = new Domain(domain_s);
            final Family family = new Family(family_s);
            final Member member = new Member(member_s);

            final Domain dom = Domain.hasDomain(_domains, domain_s);
            if (dom != null) {
                final Family fam = dom.getFamily(family_s);
                if (fam != null) {
                    final Member mem = fam.getMember(member_s);
                    if (mem != null) {
                        mem.addAttribute(next);
                        fam.addMember(mem);
                        dom.addFamily(fam);
                        _domains = Domain.addDomain(_domains, dom);
                    } else {
                        member.addAttribute(next);
                        fam.addMember(member);
                        dom.addFamily(fam);
                        _domains = Domain.addDomain(_domains, dom);
                    }
                } else {
                    member.addAttribute(next);
                    family.addMember(member);
                    dom.addFamily(family);
                    _domains = Domain.addDomain(_domains, dom);
                }
            } else {
                member.addAttribute(next);
                family.addMember(member);
                domain.addFamily(family);
                _domains = Domain.addDomain(_domains, domain);
            }
        }
        return _domains;
    }

    /**
     * @return
     */
    private ArchivingConfigurationAttribute[] getListCLA(
            final Map<String, ArchivingConfigurationAttribute> attributesHash) {
        final ArchivingConfigurationAttribute[] list = new ArchivingConfigurationAttribute[attributesHash.size()];
        int i = 0;
        final Set<String> keySet = attributesHash.keySet();
        final Iterator<String> keyIterator = keySet.iterator();
        while (keyIterator.hasNext()) {
            final String completeName = keyIterator.next();
            final ArchivingConfigurationAttribute next = attributesHash.get(completeName);
            list[i] = next;

            i++;
        }
        return list;
    }

    public void setAttributes(final Map<String, ArchivingConfigurationAttribute> attributes) {
        this.attributes = attributes;
    }

    public void removeAttributesNotInList(final Map<String, ArchivingConfigurationAttribute> attrs) {
        final Set<String> keySet = attributes.keySet();
        final Set<String> toRemoveSet = new HashSet<String>();
        final Iterator<String> keyIterator = keySet.iterator();
        while (keyIterator.hasNext()) {
            final String next = keyIterator.next();
            if (!attrs.containsKey(next)) {
                toRemoveSet.add(next);
            }
        }
        final Iterator<String> toRemoveIterator = toRemoveSet.iterator();
        while (toRemoveIterator.hasNext()) {
            attributes.remove(toRemoveIterator.next());
        }
    }

    public void controlValues() throws ArchivingConfigurationException {
        final ArchivingConfigurationAttribute[] list = getAttributesList();
        if (list == null || list.length == 0) {
            throw new ArchivingConfigurationException(null, ArchivingConfiguration.EMPTY_ATTRIBUTES);
        }
        final int lg = list.length;
        for (int i = 0; i < lg; i++) {
            final ArchivingConfigurationAttribute attr = list[i];
            attr.controlValues();
        }
    }

    public ArchivingConfigurationAttributes cloneAttrs() {
        final ArchivingConfigurationAttributes ret = new ArchivingConfigurationAttributes();
        Map<String, ArchivingConfigurationAttribute> attrs = new TreeMap<>();
        for (Entry<String, ArchivingConfigurationAttribute> entry : attributes.entrySet()) {
            String key = entry.getKey();
            ArchivingConfigurationAttribute attribute = entry.getValue();
            if (attribute != null) {
                attribute = attribute.clone();
                attribute.setGroup(ret);
            }
            attrs.put(key, attribute);
        }
        ret.setAttributes(attrs);
        return ret;
    }
}
