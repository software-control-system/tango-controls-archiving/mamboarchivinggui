package fr.soleil.mambo.data.archiving;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.api.archiving.IArchiver;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesPropertiesPanel;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.datasources.db.archiving.IArchivingManager;

public abstract class ArchivingConfigurationAttributeDBProperties implements Cloneable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivingConfigurationAttributeDBProperties.class);

    public static final String DEDICATED_ARCHIVER_PROPERTY_XML_TAG = "dedicatedArchiver";

    private String status;
    private Map<Integer, ArchivingConfigurationMode> modes;
    private boolean isEmpty;
    private String dedicatedArchiver;

    private ArchivingConfigurationAttributeProperties properties;

    public ArchivingConfigurationAttributeDBProperties() {
        modes = new ConcurrentHashMap<>();
        isEmpty = true;
        dedicatedArchiver = ObjectUtils.EMPTY_STRING;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (modes != null) {
            for (ArchivingConfigurationMode nextValue : modes.values()) {
                builder.append(nextValue.toString());
                builder.append(GUIUtilities.CRLF);
            }
        }
        return builder.toString();
    }

    /**
     * @return Returns the properties.
     */
    public ArchivingConfigurationAttributeProperties getProperties() {
        return properties;
    }

    /**
     * @param properties
     *            The properties to set.
     */
    public void setProperties(ArchivingConfigurationAttributeProperties properties) {
        this.properties = properties;
    }

    /**
     * @return Returns the status.
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     *            The status to set.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return Returns the modes.
     */
    public ArchivingConfigurationMode[] getModes() {
        ArchivingConfigurationMode[] modeArray;
        if (modes == null) {
            modeArray = null;
        } else {
            Collection<ArchivingConfigurationMode> values = modes.values();
            modeArray = values.toArray(new ArchivingConfigurationMode[values.size()]);
        }
        return modeArray;
    }

    public Collection<Integer> getModesTypes() {
        Collection<Integer> ret;
        if (modes == null) {
            ret = null;
        } else {
            ret = new ArrayList<>(7);
            ret.addAll(modes.keySet());
        }
        return ret;
    }

    /**
     * @param modes
     *            The modes to set.
     */
    public void setModes(ArchivingConfigurationMode[] modes) {
        if (modes == null) {
            this.modes = new ConcurrentHashMap<>();
        } else {
            for (ArchivingConfigurationMode currentMode : modes) {
                Integer type = Integer.valueOf(currentMode.getType());
                this.modes.put(type, currentMode);
                isEmpty = false;
            }
        }
    }

    public void addMode(ArchivingConfigurationMode mode) {
        modes.put(Integer.valueOf(mode.getType()), mode);
        isEmpty = false;
    }

    public void removeMode(int modeToRemove) {
        modes.remove(Integer.valueOf(modeToRemove));
        isEmpty = modes.isEmpty();
    }

    /**
     * @param type_p
     * @return 25 juil. 2005
     */
    public ArchivingConfigurationMode getMode(int type) {
        return modes.get(Integer.valueOf(type));
    }

    public boolean hasMode(int type) {
        return modes == null ? Boolean.FALSE : modes.containsKey(Integer.valueOf(type));
    }

    public abstract void controlValues() throws ArchivingConfigurationException;

    protected void controlValues(int minPeriod) throws ArchivingConfigurationException {
        ArchivingConfigurationMode[] modes = getModes();
        if ((modes != null) && (modes.length > 0)) {
            for (ArchivingConfigurationMode mode : modes) {
                mode.controlValues(minPeriod);
            }
        }
    }

    public void setDedicatedArchiver(String dedicatedArchiver) {
        this.dedicatedArchiver = dedicatedArchiver;
    }

    /**
     * @return Returns the dedicatedArchiver.
     */
    public String getDedicatedArchiver() {
        return dedicatedArchiver;
    }

    public void push() {
        if (Mambo.canChooseArchivers()) {
            AttributesPropertiesPanel panel = AttributesPropertiesPanel.getInstance();

            if (panel != null) {
                panel.pushDefaultArchiver(getDedicatedArchiver());

                IArchivingManager manager = ArchivingManagerFactory.getCurrentImpl();
                Boolean historic = panel.isHistoric();

                try {
                    String completeName = properties.getAttribute().getCompleteName();

                    IArchiver currentArchiver = manager.getCurrentArchiverForAttribute(completeName, historic);
                    panel.pushCurrentArchiver(currentArchiver, completeName);
                    panel.pushDedicatedArchiver(completeName);
                    panel.pushChoiceArchiverColor(completeName);
                } catch (ArchivingException e) {
                    LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                } catch (Exception e) {
                    LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                }
            }
        }
    }

    @Override
    public ArchivingConfigurationAttributeDBProperties clone() {
        ArchivingConfigurationAttributeDBProperties clone;
        try {
            clone = (ArchivingConfigurationAttributeDBProperties) super.clone();
            if (modes != null) {
                clone.modes = new ConcurrentHashMap<>(modes);
            }
        } catch (CloneNotSupportedException e) {
            // Will not happen as ArchivingConfigurationAttributeDBProperties
            clone = null;
        }
        return clone;
    }

}
