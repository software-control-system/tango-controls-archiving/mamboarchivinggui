package fr.soleil.mambo.data.archiving;

import fr.soleil.archiving.hdbtdb.api.tools.mode.EventMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.archiving.hdbtdb.api.tools.mode.TdbSpec;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;

public class ArchivingConfigurationMode {
    public static ArchivingConfigurationMode currentMode;

    private Mode mode;
    private int type = -1;
    private boolean isTDB = false;

    // General tags START
    public static final String XML_TAG = "mode";
    public static final String TYPE_PROPERTY_XML_TAG = "type";

    // General tags END
    // modeRoot tags START
    public static final String PERIOD_PROPERTY_XML_TAG = "period";
    // modeRoot tags END

    // modeAbsolu tags START
    public static final String VAL_INF_PROPERTY_XML_TAG = "val_inf";
    public static final String VAL_SUP_PROPERTY_XML_TAG = "val_sup";
    public static final String VAL_ABS_SLOW_DRIFT_XML_TAG = "abs_slow_drift";
    // modeAbsolu tags END

    // modeCalcul tags START
    public static final String RANGE_PROPERTY_XML_TAG = "range";
    public static final String CALC_TYPE_PROPERTY_XML_TAG = "calcul_type";
    // modeCalcul tags END

    // modeDifference tags START
    public static final String PRECISION_PROPERTY_XML_TAG = "precision";
    // modeDifference tags END

    // modeRoot tags START
    public static final String DESCRIPTION_PROPERTY_XML_TAG = "description";
    // modeRoot tags END

    // modeRelatif tags START
    public static final String PERCENT_INF_PROPERTY_XML_TAG = "percent_inf";
    public static final String PERCENT_SUP_PROPERTY_XML_TAG = "percent_sup";
    public static final String VAL_REL_SLOW_DRIFT_XML_TAG = "rel_slow_drift";
    // modeRelatif tags END

    // modeSeuil tags START
    public static final String THRESHOLD_INF_PROPERTY_XML_TAG = "threshold_inf";
    public static final String THRESHOLD_SUP_PROPERTY_XML_TAG = "threshold_sup";
    // modeSeuil tags END

    public static final int TYPE_HDB = 0;
    public static final int TYPE_TDB = 1;

    public static final int TYPE_A = 0;
    public static final int TYPE_C = 1;
    public static final int TYPE_D = 2;
    public static final int TYPE_E = 3;
    public static final int TYPE_P = 4;
    public static final int TYPE_R = 5;
    public static final int TYPE_T = 6;

    public static String TYPE_A_NAME = "Absolute";
    public static String TYPE_C_NAME = "Calculation";
    public static String TYPE_D_NAME = "Difference";
    public static String TYPE_E_NAME = "External";
    public static String TYPE_P_NAME = "Periodical";
    public static String TYPE_R_NAME = "Relative";
    public static String TYPE_T_NAME = "Threshold";

    public static final int PERIOD_TOO_LOW = 100;
    public static final int MODE_A_VAL_INF_BIGGER_THAN_VAL_SUP = 101;
    public static final int MODE_R_PERCENT_INF_BIGGER_THAN_PERCENT_SUP = 102;

    public static String getLabel(int _type) {
        String ret = null;

        switch (_type) {
            case TYPE_A:
                ret = TYPE_A_NAME;
                break;

            case TYPE_C:
                ret = TYPE_C_NAME;
                break;

            case TYPE_D:
                ret = TYPE_D_NAME;
                break;

            case TYPE_E:
                ret = TYPE_E_NAME;
                break;

            case TYPE_P:
                ret = TYPE_P_NAME;
                break;

            case TYPE_R:
                ret = TYPE_R_NAME;
                break;

            case TYPE_T:
                ret = TYPE_T_NAME;
                break;

            default:
                throw new IllegalStateException();
        }

        return ret;
    }

    public static ArchivingConfigurationMode[] buildModesList(Mode _mode) {
        ModeAbsolu modeA = _mode.getModeA();
        ModeRelatif modeR = _mode.getModeR();
        ModeSeuil modeT = _mode.getModeT();
        ModePeriode modeP = _mode.getModeP();
        ModeDifference modeD = _mode.getModeD();
        EventMode modeE = _mode.getEventMode();
        ModeCalcul modeC = _mode.getModeC();

        int size = 0;
        if (modeA != null)
            size++;
        if (modeR != null)
            size++;
        if (modeT != null)
            size++;
        if (modeD != null)
            size++;
        if (modeE != null)
            size++;
        if (modeC != null)
            size++;
        if (modeP != null)
            size++;

        if (size == 0) {
            return null;
        }

        ArchivingConfigurationMode[] ret = new ArchivingConfigurationMode[size];
        int i = 0;
        if (modeA != null) {
            Mode mode1 = new Mode();
            mode1.setModeA(modeA);
            ret[i] = new ArchivingConfigurationMode(mode1);
            i++;
        }
        if (modeR != null) {
            Mode mode1 = new Mode();
            mode1.setModeR(modeR);
            ret[i] = new ArchivingConfigurationMode(mode1);
            i++;
        }
        if (modeT != null) {
            Mode mode1 = new Mode();
            mode1.setModeT(modeT);
            ret[i] = new ArchivingConfigurationMode(mode1);
            i++;
        }
        if (modeD != null) {
            Mode mode1 = new Mode();
            mode1.setModeD(modeD);
            ret[i] = new ArchivingConfigurationMode(mode1);
            i++;
        }
        if (modeE != null) {
            Mode mode1 = new Mode();
            mode1.setEventMode(modeE);
            ret[i] = new ArchivingConfigurationMode(mode1);
            i++;
        }
        if (modeC != null) {
            Mode mode1 = new Mode();
            mode1.setModeC(modeC);
            ret[i] = new ArchivingConfigurationMode(mode1);
            i++;
        }
        if (modeP != null) {
            Mode mode1 = new Mode();
            mode1.setModeP(modeP);
            ret[i] = new ArchivingConfigurationMode(mode1);
            i++;
        }

        return ret;
    }

    public ArchivingConfigurationMode(Mode _mode) {
        this.mode = _mode;

        if (_mode.getModeA() != null) {
            type = TYPE_A;
        } else if (_mode.getModeC() != null) {
            type = TYPE_C;
        } else if (_mode.getModeD() != null) {
            type = TYPE_D;
        } else if (_mode.getEventMode() != null) {
            type = TYPE_E;
        } else if (_mode.getModeP() != null) {
            type = TYPE_P;
        } else if (_mode.getModeR() != null) {
            type = TYPE_R;
        } else if (_mode.getModeT() != null) {
            type = TYPE_T;
        }

        if (_mode.getTdbSpec() != null) {
            isTDB = true;
        }

        // initLabels ();

    }

    @Override
    public String toString() {
        String ret = ObjectUtils.EMPTY_STRING;

        XMLLine modeLine = new XMLLine(XML_TAG, XMLLine.EMPTY_TAG_CATEGORY);

        String type_s = String.valueOf(type);
        modeLine.setAttribute(TYPE_PROPERTY_XML_TAG, type_s);

        switch (this.type) {
            case TYPE_A:
                ModeAbsolu modeA = this.mode.getModeA();
                modeLine.setAttribute(VAL_INF_PROPERTY_XML_TAG, String.valueOf(modeA.getValInf()));
                modeLine.setAttribute(VAL_SUP_PROPERTY_XML_TAG, String.valueOf(modeA.getValSup()));
                modeLine.setAttribute(PERIOD_PROPERTY_XML_TAG, String.valueOf(modeA.getPeriod()));
                modeLine.setAttribute(VAL_ABS_SLOW_DRIFT_XML_TAG, String.valueOf(modeA.isSlow_drift()));
                break;

            case TYPE_C:
                ModeCalcul modeC = this.mode.getModeC();
                modeLine.setAttribute(RANGE_PROPERTY_XML_TAG, String.valueOf(modeC.getRange()));
                modeLine.setAttribute(CALC_TYPE_PROPERTY_XML_TAG, String.valueOf(modeC.getTypeCalcul()));
                modeLine.setAttribute(PERIOD_PROPERTY_XML_TAG, String.valueOf(modeC.getPeriod()));
                break;

            case TYPE_D:
                ModeDifference modeD = this.mode.getModeD();
                modeLine.setAttribute(PERIOD_PROPERTY_XML_TAG, String.valueOf(modeD.getPeriod()));
                break;

            case TYPE_E:
                EventMode modeE = this.mode.getEventMode();
                modeLine.setAttribute(DESCRIPTION_PROPERTY_XML_TAG, String.valueOf(modeE.getDescription()));
                break;

            case TYPE_P:
                ModePeriode modeP = this.mode.getModeP();
                modeLine.setAttribute(PERIOD_PROPERTY_XML_TAG, String.valueOf(modeP.getPeriod()));
                break;

            case TYPE_R:
                ModeRelatif modeR = this.mode.getModeR();
                modeLine.setAttribute(PERCENT_INF_PROPERTY_XML_TAG, String.valueOf(modeR.getPercentInf()));
                modeLine.setAttribute(PERCENT_SUP_PROPERTY_XML_TAG, String.valueOf(modeR.getPercentSup()));
                modeLine.setAttribute(PERIOD_PROPERTY_XML_TAG, String.valueOf(modeR.getPeriod()));
                modeLine.setAttribute(VAL_REL_SLOW_DRIFT_XML_TAG, String.valueOf(modeR.isSlow_drift()));
                break;

            case TYPE_T:
                ModeSeuil modeT = this.mode.getModeT();
                modeLine.setAttribute(THRESHOLD_INF_PROPERTY_XML_TAG, String.valueOf(modeT.getThresholdInf()));
                modeLine.setAttribute(THRESHOLD_SUP_PROPERTY_XML_TAG, String.valueOf(modeT.getThresholdSup()));
                modeLine.setAttribute(PERIOD_PROPERTY_XML_TAG, String.valueOf(modeT.getPeriod()));
                break;
        }

        ret += modeLine.toString();

        return ret;
    }

    /**
     * @return Returns the currentMode.
     */
    public static ArchivingConfigurationMode getCurrentMode() {
        return currentMode;
    }

    /**
     * @param currentMode
     *            The currentMode to set.
     */
    public static void setCurrentMode(ArchivingConfigurationMode currentMode) {
        ArchivingConfigurationMode.currentMode = currentMode;
    }

    /**
     * @return Returns the isTDB.
     */
    public boolean isTDB() {
        return isTDB;
    }

    /**
     * @param isTDB
     *            The isTDB to set.
     */
    public void setTDB(boolean isTDB) {
        this.isTDB = isTDB;
    }

    /**
     * @return Returns the mode.
     */
    public Mode getMode() {
        return mode;
    }

    /**
     * @param mode
     *            The mode to set.
     */
    public void setMode(Mode mode) {
        this.mode = mode;
    }

    /**
     * @return Returns the type.
     */
    public int getType() {
        return type;
    }

    /**
     * @param type
     *            The type to set.
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @param modes
     * @return 27 juil. 2005
     */
    public static Mode buildCompleteHDBMode(ArchivingConfigurationMode[] modes) {
        return buildCompleteDBMode(modes);
    }

    /**
     * @param modes
     * @return 27 juil. 2005
     */
    public static Mode buildCompleteTDBMode(ArchivingConfigurationMode[] modes, long exportPeriod, long keepingPeriod) {
        Mode ret = buildCompleteDBMode(modes);

        TdbSpec tdbSpec = new TdbSpec(exportPeriod, keepingPeriod);
        ret.setTdbSpec(tdbSpec);

        return ret;
    }

    public static Mode buildCompleteTTSMode(ArchivingConfigurationMode[] modes) {
        return buildCompleteDBMode(modes);
    }

    /**
     * @param modes
     * @return 27 juil. 2005
     */
    public static Mode buildCompleteDBMode(ArchivingConfigurationMode[] modes) {
        Mode ret = new Mode();
        if ((modes != null) && (modes.length > 0)) {
            for (ArchivingConfigurationMode next : modes) {
                if (next != null) {
                    int type = next.getType();
                    Mode mode = next.getMode();
                    if (mode != null) {
                        switch (type) {
                            case TYPE_A:
                                ret.setModeA(mode.getModeA());
                                break;
                            case TYPE_C:
                                ret.setModeC(mode.getModeC());
                                break;
                            case TYPE_D:
                                ret.setModeD(mode.getModeD());
                                break;
                            case TYPE_E:
                                ret.setEventMode(mode.getEventMode());
                                break;
                            case TYPE_P:
                                ret.setModeP(mode.getModeP());
                                break;
                            case TYPE_R:
                                ret.setModeR(mode.getModeR());
                                break;
                            case TYPE_T:
                                ret.setModeT(mode.getModeT());
                                break;
                        }
                    } // end if (mode != null)
                } // end if (next != null)
            } // end for (ArchivingConfigurationMode next : modes)
        } // end if ((modes != null) && (modes.length > 0))
        return ret;
    }

    /**
     * Checks whether mode parameters are valid.
     * 
     * @param minPeriod The known minimum archiving period allowed according to database.
     * @throws ArchivingConfigurationException If any parameter is invalid.
     */
    public void controlValues(int minPeriod) throws ArchivingConfigurationException {
        switch (this.type) {
            case TYPE_A:
                ModeAbsolu modeA = this.mode.getModeA();
                if (modeA.getValInf() >= modeA.getValSup()) {
                    throw new ArchivingConfigurationException(null, MODE_A_VAL_INF_BIGGER_THAN_VAL_SUP);
                }
                if (modeA.getPeriod() < minPeriod) {
                    throw new ArchivingConfigurationException(null, PERIOD_TOO_LOW);
                }
                break;
            case TYPE_C:
                ModeCalcul modeC = this.mode.getModeC();
                if (modeC.getPeriod() < minPeriod) {
                    throw new ArchivingConfigurationException(null, PERIOD_TOO_LOW);
                }
                break;
            case TYPE_D:
                ModeDifference modeD = this.mode.getModeD();
                if (modeD.getPeriod() < minPeriod) {
                    throw new ArchivingConfigurationException(null, PERIOD_TOO_LOW);
                }
                break;
            case TYPE_E:
                break;
            case TYPE_P:
                ModePeriode modeP = this.mode.getModeP();
                if (modeP.getPeriod() < minPeriod) {
                    throw new ArchivingConfigurationException(null, PERIOD_TOO_LOW);
                }
                break;
            case TYPE_R:
                ModeRelatif modeR = this.mode.getModeR();
                if (modeR.getPercentInf() >= modeR.getPercentSup()) {
                    throw new ArchivingConfigurationException(null, MODE_R_PERCENT_INF_BIGGER_THAN_PERCENT_SUP);
                }
                if (modeR.getPeriod() < minPeriod) {
                    throw new ArchivingConfigurationException(null, PERIOD_TOO_LOW);
                }
                break;
            case TYPE_T:
                ModeSeuil modeT = this.mode.getModeT();
                if (modeT.getThresholdInf() >= modeT.getThresholdSup()) {
                    throw new ArchivingConfigurationException(null, MODE_R_PERCENT_INF_BIGGER_THAN_PERCENT_SUP);
                }
                if (modeT.getPeriod() < minPeriod) {
                    throw new ArchivingConfigurationException(null, PERIOD_TOO_LOW);
                }
                break;
        }
    }

}
