package fr.soleil.mambo.data.archiving;

import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.Attributes;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.xmlhelpers.XMLLine;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;

public class ArchivingConfigurationAttribute extends Attribute implements Cloneable {

    private static final long serialVersionUID = 8197101500878817023L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivingConfigurationAttribute.class);

    private ArchivingConfigurationAttributes group;
    private ArchivingConfigurationAttributeProperties properties;

    public static final String XML_TAG = "attribute";
    public static final String COMPLETE_NAME_PROPERTY_XML_TAG = "completeName";

    private boolean isSelected = true;
    private boolean isNew = false;

    private String deviceClass;

    public ArchivingConfigurationAttribute(ArchivingConfigurationAttributeProperties properties,
            Attributes contextAttributes) {
        super(contextAttributes);
        this.properties = properties;
        this.properties.setAttribute(this);
    }

    /**
     * @param contextAttributes
     */
    public ArchivingConfigurationAttribute(Attributes contextAttributes) {
        super(contextAttributes);
        properties = new ArchivingConfigurationAttributeProperties();
        properties.setAttribute(this);
    }

    public ArchivingConfigurationAttribute() {
        super();
        properties = new ArchivingConfigurationAttributeProperties();
        properties.setAttribute(this);
    }

    /**
     * @param attribute
     */
    public ArchivingConfigurationAttribute(Attribute attribute) {
        super();
        properties = new ArchivingConfigurationAttributeProperties();
        setCompleteName(attribute.getCompleteName());
        setDevice(attribute.getDeviceName());
        setDeviceClass(attribute.getDeviceClass());
        setDomain(attribute.getDomainName());
        setFamily(attribute.getFamilyName());
        setMember(attribute.getMemberName());
        setName(attribute.getName());
    }

    @Override
    public String toString() {
        setNew(false);

        String ret = ObjectUtils.EMPTY_STRING;

        XMLLine openingLine = new XMLLine(ArchivingConfigurationAttribute.XML_TAG, XMLLine.OPENING_TAG_CATEGORY);
        openingLine.setAttribute(ArchivingConfigurationAttribute.COMPLETE_NAME_PROPERTY_XML_TAG,
                super.getCompleteName());
        XMLLine closingLine = new XMLLine(ArchivingConfigurationAttribute.XML_TAG, XMLLine.CLOSING_TAG_CATEGORY);

        ret += openingLine;
        ret += GUIUtilities.CRLF;

        if (properties != null) {
            ArchivingConfigurationAttributeHDBProperties HDBProperties = properties.getHDBProperties();
            if (!HDBProperties.isEmpty()) {
                ret += HDBProperties.toString();
                ret += GUIUtilities.CRLF;
            }

            ArchivingConfigurationAttributeTDBProperties TDBProperties = properties.getTDBProperties();
            if (!TDBProperties.isEmpty()) {
                ret += TDBProperties.toString();
                ret += GUIUtilities.CRLF;
            }

            ArchivingConfigurationAttributeTTSProperties TTSProperties = properties.getTTSProperties();
            if (!TTSProperties.isEmpty()) {
                ret += TTSProperties.toString();
                ret += GUIUtilities.CRLF;
            }
        }

        ret += closingLine;

        return ret;
    }

    public void loadFromCurrentArchivingState() throws Exception {
        boolean iah, iat, iatts;
        try {
            iah = ArchivingManagerFactory.getCurrentImpl().isArchived(getCompleteName(), Boolean.TRUE);
        } catch (Exception e) {
            iah = false;
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
        }
        try {
            iat = ArchivingManagerFactory.getCurrentImpl().isArchived(getCompleteName(), Boolean.FALSE);
        } catch (Exception e) {
            iat = false;
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
        }
        try {
            iatts = ArchivingManagerFactory.getCurrentImpl().isArchived(getCompleteName(), null);
        } catch (Exception e) {
            iatts = false;
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
        }
        if (iah) {
            ArchivingConfigurationAttributeHDBProperties HDBProperties = ArchivingConfigurationAttributeHDBProperties
                    .loadHDBProperties(getCompleteName());
            if (HDBProperties != null) {
                properties.setHDBProperties(HDBProperties);
            }
        }
        if (iat) {
            ArchivingConfigurationAttributeTDBProperties TDBProperties = ArchivingConfigurationAttributeTDBProperties
                    .loadTDBProperties(getCompleteName());
            if (TDBProperties != null) {
                properties.setTDBProperties(TDBProperties);
            }
        }
        if (iatts) {
            ArchivingConfigurationAttributeTTSProperties TTSProperties = ArchivingConfigurationAttributeTTSProperties
                    .loadTTSProperties(getCompleteName());
            if (TTSProperties != null) {
                properties.setTTSProperties(TTSProperties);
            }
        }
    }

    /**
     * @return Returns the group.
     */
    public ArchivingConfigurationAttributes getGroup() {
        return group;
    }

    /**
     * @param group
     *            The group to set.
     */
    public void setGroup(ArchivingConfigurationAttributes group) {
        this.group = group;
    }

    /**
     * @return Returns the properties.
     */
    public ArchivingConfigurationAttributeProperties getProperties() {
        return properties;
    }

    /**
     * @param properties
     *            The properties to set.
     */
    public void setProperties(ArchivingConfigurationAttributeProperties properties) {
        this.properties = properties;
    }

    /**
     * @param properties
     *            The properties to set.
     */
    public void addProperties(ArchivingConfigurationAttributeProperties properties) {
        ArchivingConfigurationAttributeHDBProperties HDBPropertiesToAdd = properties.getHDBProperties();
        ArchivingConfigurationAttributeTDBProperties TDBPropertiesToAdd = properties.getTDBProperties();
        ArchivingConfigurationAttributeTTSProperties TTSPropertiesToAdd = properties.getTTSProperties();

        if (!HDBPropertiesToAdd.isEmpty()) {
            ArchivingConfigurationMode[] HDBModesToAdd = HDBPropertiesToAdd.getModes();
            ArchivingConfigurationAttributeHDBProperties HDBProperties = getProperties().getHDBProperties();

            for (int i = 0; i < HDBModesToAdd.length; i++) {
                HDBProperties.addMode(HDBModesToAdd[i]);
            }

            properties.setHDBProperties(HDBProperties);
        }

        if (!TDBPropertiesToAdd.isEmpty()) {
            ArchivingConfigurationMode[] TDBModesToAdd = TDBPropertiesToAdd.getModes();
            ArchivingConfigurationAttributeTDBProperties TDBProperties = getProperties().getTDBProperties();

            for (int i = 0; i < TDBModesToAdd.length; i++) {
                TDBProperties.addMode(TDBModesToAdd[i]);
            }

            properties.setTDBProperties(TDBProperties);
        }

        if (!TTSPropertiesToAdd.isEmpty()) {
            ArchivingConfigurationMode[] TTSModesToAdd = TTSPropertiesToAdd.getModes();
            ArchivingConfigurationAttributeTTSProperties TTSProperties = getProperties().getTTSProperties();

            for (int i = 0; i < TTSModesToAdd.length; i++) {
                TTSProperties.addMode(TTSModesToAdd[i]);
            }

            properties.setTTSProperties(TTSProperties);
        }

    }

    public void removeProperty(boolean hasMode, int type, Boolean historic) {
        if (!hasMode) {
            ArchivingConfigurationAttributeHDBProperties HDBProperties = properties.getHDBProperties();
            ArchivingConfigurationAttributeTDBProperties TDBProperties = properties.getTDBProperties();
            ArchivingConfigurationAttributeTTSProperties TTSProperties = properties.getTTSProperties();

            if (historic == null) {
                TTSProperties.removeMode(type);
            } else if (historic.booleanValue()) {
                HDBProperties.removeMode(type);
            } else {
                TDBProperties.removeMode(type);
            }
        }
    }

    /**
     * @param period
     *            26 juil. 2005
     */
    public void setHDBPeriod(int period) {
        ArchivingConfigurationAttributeHDBProperties HDBProperties = getProperties().getHDBProperties();
        HDBProperties.setDefaultPeriod(period);
    }

    /**
     * @return 26 juil. 2005
     */
    public boolean isEmpty() {
        ArchivingConfigurationAttributeHDBProperties HDBProperties = properties.getHDBProperties();
        ArchivingConfigurationAttributeTDBProperties TDBProperties = properties.getTDBProperties();
        ArchivingConfigurationAttributeTTSProperties TTSProperties = properties.getTTSProperties();

        return HDBProperties.isEmpty() && TDBProperties.isEmpty() && TTSProperties.isEmpty();
    }

    /**
     * @param period
     *            26 juil. 2005
     */
    public void setTDBPeriod(int period) {
        ArchivingConfigurationAttributeTDBProperties TDBProperties = getProperties().getTDBProperties();
        TDBProperties.setDefaultPeriod(period);
    }

    /**
     * @param exportPeriod
     *            26 juil. 2005
     */
    public void setExportPeriod(long exportPeriod) {
        ArchivingConfigurationAttributeTDBProperties TDBProperties = getProperties().getTDBProperties();
        TDBProperties.setExportPeriod(exportPeriod);
    }

    /**
     * @param period
     *            26 juil. 2005
     */
    public void setTTSPeriod(int period) {
        ArchivingConfigurationAttributeTTSProperties TTSProperties = getProperties().getTTSProperties();
        TTSProperties.setDefaultPeriod(period);
    }

    /**
     * @param i
     * @return 9 sept. 2005
     */
    public void controlValues() throws ArchivingConfigurationException {
        if (properties != null) {
            ArchivingConfigurationAttributeHDBProperties HDBProperties = properties.getHDBProperties();
            ArchivingConfigurationAttributeTDBProperties TDBProperties = properties.getTDBProperties();
            ArchivingConfigurationAttributeTTSProperties TTSProperties = properties.getTTSProperties();

            try {
                if (HDBProperties != null) {
                    HDBProperties.controlValues();
                }
                if (TDBProperties != null) {
                    TDBProperties.controlValues();
                }
                if (TTSProperties != null) {
                    TTSProperties.controlValues();
                }
            } catch (ArchivingConfigurationException ace) {
                ace.setAttributeName(getCompleteName());
                throw ace;
            }
        }
    }

    /**
     * @return
     */
    @Override
    public boolean isSelected() {
        return isSelected;
    }

    /**
     * @param isSelected
     *            The isSelected to set.
     */
    @Override
    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    /**
     * @return
     */
    public TreePath getTreePath() {

        String[] pathRef = new String[5];
        pathRef[0] = ObjectUtils.EMPTY_STRING;
        pathRef[1] = getDomainName();
        pathRef[2] = getFamilyName();
        pathRef[3] = getMemberName();
        pathRef[4] = getName();

        TreePath aPath = new TreePath(pathRef);
        return aPath;
    }

    /**
     * 
     */
    @Override
    public void reverseSelection() {
        isSelected = !isSelected;
    }

    /**
     * @return Returns the isNew.
     */
    @Override
    public boolean isNew() {
        return isNew;
    }

    /**
     * @param isNew
     *            The isNew to set.
     */
    @Override
    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public void setDedicatedArchiver(Boolean historic, String dedicatedArchiver) {
        ArchivingConfigurationAttributeDBProperties properties;
        if (historic == null) {
            properties = getProperties().getTTSProperties();
        } else if (historic.booleanValue()) {
            properties = getProperties().getHDBProperties();
        } else {
            properties = getProperties().getTDBProperties();
        }
        properties.setDedicatedArchiver(dedicatedArchiver);
    }

    @Override
    public String getDeviceClass() {
        return deviceClass;
    }

    /**
     * @param deviceClass
     *            The deviceClass to set.
     */
    @Override
    public void setDeviceClass(String deviceClass) {
        this.deviceClass = deviceClass;
    }

    @Override
    protected ArchivingConfigurationAttribute clone() {
        ArchivingConfigurationAttribute clone;
        try {
            clone = (ArchivingConfigurationAttribute) super.clone();
            clone.group = null;
            if (properties != null) {
                clone.properties = properties.clone();
            }
        } catch (CloneNotSupportedException e) {
            // Will not happen as ArchivingConfigurationAttribute implements Cloneable
            clone = null;
        }
        return clone;
    }

}
