package fr.soleil.mambo.containers;

import java.awt.BorderLayout;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;

import fr.soleil.lib.project.application.logging.LogManager;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.components.MamboToolBar;
import fr.soleil.mambo.containers.archiving.ArchivingPanel;
import fr.soleil.mambo.containers.view.ViewPanel;

public class MamboMainPanel extends JPanel {

    private static final long serialVersionUID = 4997110828909551765L;

    private static final int MESSAGE_SPLIT_DIVIDER_SIZE = 8;
    private static final double MESSAGE_SPLIT_RESIZE_WEIGHT = 1.0;
    private static final double MESSAGE_SPLIT_POSITION = 0.8;
    private static final int VIEW_SPLIT_DIVIDER_SIZE = MESSAGE_SPLIT_DIVIDER_SIZE;
    private static final double VIEW_SPLIT_RESIZE_WEIGHT = 0.5;
    private static final double VIEW_SPLIT_POSITION = VIEW_SPLIT_RESIZE_WEIGHT;

    private static MamboMainPanel instance = null;
    private final LogViewer logViewer;

    /**
     * @return 8 juil. 2005
     */
    public static MamboMainPanel getInstance() {
        if (instance == null) {
            instance = new MamboMainPanel();
        }

        return instance;
    }

    private MamboMainPanel() {
        super();

        // START HORIZONTAL SPLIT
        JComponent topComponent;
        final JSplitPane archivingSplit, messageSplit;
        if (Mambo.hasACs()) {
            archivingSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, false);
            archivingSplit.setDividerSize(VIEW_SPLIT_DIVIDER_SIZE);
            archivingSplit.setResizeWeight(VIEW_SPLIT_RESIZE_WEIGHT);
            archivingSplit.setOneTouchExpandable(true);
            archivingSplit.setDividerLocation(VIEW_SPLIT_POSITION);

            final JScrollPane archivingScrollPane = new JScrollPane(ArchivingPanel.getInstance());
            final JScrollPane viewScrollPane = new JScrollPane(ViewPanel.getInstance());
            archivingSplit.setLeftComponent(archivingScrollPane);
            archivingSplit.setRightComponent(viewScrollPane);

            topComponent = archivingSplit;
        } else {
            archivingSplit = null;
            final JScrollPane viewScrollPane = new JScrollPane(ViewPanel.getInstance());

            topComponent = viewScrollPane;
        }
        // END HORIZONTAL SPLIT

        // START VERTICAL SPLIT
        messageSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true);
        messageSplit.setDividerSize(MESSAGE_SPLIT_DIVIDER_SIZE);
        messageSplit.setResizeWeight(MESSAGE_SPLIT_RESIZE_WEIGHT);
        messageSplit.setOneTouchExpandable(true);
        messageSplit.setDividerLocation(MESSAGE_SPLIT_POSITION);
        messageSplit.setTopComponent(topComponent);
        logViewer = LogManager.getLogViewerInstance(Mambo.DEFAULT_LOG_ID, true);
        logViewer.setAutoResizeColumns(true);
        messageSplit.setBottomComponent(logViewer);
        // END VERTICAL SPLIT

        // Ensure dividers location when displaying mambo frame
        addHierarchyListener(new HierarchyListener() {
            @Override
            public void hierarchyChanged(HierarchyEvent e) {
                if (isShowing()) {
                    removeHierarchyListener(this);
                    SwingUtilities.invokeLater(() -> {
                        if (archivingSplit != null) {
                            archivingSplit.setDividerLocation(VIEW_SPLIT_POSITION);
                        }
                        if (messageSplit != null) {
                            messageSplit.setDividerLocation(MESSAGE_SPLIT_POSITION);
                        }
                    });
                }
            }
        });

        // START MAIN PART
        setLayout(new BorderLayout());

        add(MamboToolBar.getInstance(), BorderLayout.NORTH);
        add(messageSplit, BorderLayout.CENTER);
    }

}
