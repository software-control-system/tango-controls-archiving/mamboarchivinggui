package fr.soleil.mambo.containers.sub.dialogs.options;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.text.DocumentNumber;
import fr.soleil.mambo.actions.archiving.LoadACDefaults;
import fr.soleil.mambo.actions.archiving.SaveACDefaults;
import fr.soleil.mambo.components.archiving.ACTdbExportPeriodComboBox;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.SpringUtilities;

public class ACTabDefaultPanel extends JPanel {

    private static final long serialVersionUID = 268367484206833233L;

    private static ACTabDefaultPanel instance = null;

    // HDB objects
    private JPanel HDBPanel;
    private JLabel HDBperiodLabel;
    private JTextField HDBperiodField;
    private JLabel HDBmodesLabel;
    private JCheckBox HDBPeriodicalCheck;
    private JCheckBox HDBAbsoluteCheck;
    private JCheckBox HDBRelativeCheck;
    private JCheckBox HDBThresholdCheck;
    private JCheckBox HDBDifferenceCheck;
    private JCheckBox HDBAbsoluteDLCheck;
    private JCheckBox HDBRelativeDLCheck;
    private JLabel HDBPeriodicalLabel;
    private JLabel HDBAbsoluteLabel;
    private JLabel HDBRelativeLabel;
    private JLabel HDBThresholdLabel;
    private JLabel HDBDifferenceLabel;
    private JTextField HDBabsolutePeriodField;
    private JTextField HDBabsoluteLowerField;
    private JTextField HDBabsoluteUpperField;
    private JTextField HDBrelativePeriodField;
    private JTextField HDBrelativeLowerField;
    private JTextField HDBrelativeUpperField;
    private JTextField HDBthresholdPeriodField;
    private JTextField HDBthresholdLowerField;
    private JTextField HDBthresholdUpperField;
    private JTextField HDBdifferencePeriodField;
    private JLabel HDBabsolutePeriodLabel;
    private JLabel HDBabsoluteLowerLabel;
    private JLabel HDBabsoluteUpperLabel;
    private JLabel HDBrelativePeriodLabel;
    private JLabel HDBrelativeLowerLabel;
    private JLabel HDBrelativeUpperLabel;
    private JLabel HDBthresholdPeriodLabel;
    private JLabel HDBthresholdLowerLabel;
    private JLabel HDBthresholdUpperLabel;
    private JLabel HDBdifferencePeriodLabel;
    // HDB objects
    // /////////////////////////

    // TDB objects
    private JPanel TDBPanel;
    private JLabel TDBperiodLabel;
    private JTextField TDBperiodField;
    private JLabel TDBexportPeriodLabel;
    private ACTdbExportPeriodComboBox TDBexportPeriodComboBox;
    private JLabel TDBmodesLabel;
    private JCheckBox TDBPeriodicalCheck;
    private JCheckBox TDBAbsoluteCheck;
    private JCheckBox TDBRelativeCheck;
    private JCheckBox TDBThresholdCheck;
    private JCheckBox TDBDifferenceCheck;
    private JCheckBox TDBAbsoluteDLCheck;
    private JCheckBox TDBRelativeDLCheck;
    private JLabel TDBPeriodicalLabel;
    private JLabel TDBAbsoluteLabel;
    private JLabel TDBRelativeLabel;
    private JLabel TDBThresholdLabel;
    private JLabel TDBDifferenceLabel;
    private JTextField TDBabsolutePeriodField;
    private JTextField TDBabsoluteLowerField;
    private JTextField TDBabsoluteUpperField;
    private JTextField TDBrelativePeriodField;
    private JTextField TDBrelativeLowerField;
    private JTextField TDBrelativeUpperField;
    private JTextField TDBthresholdPeriodField;
    private JTextField TDBthresholdLowerField;
    private JTextField TDBthresholdUpperField;
    private JTextField TDBdifferencePeriodField;
    private JLabel TDBabsolutePeriodLabel;
    private JLabel TDBabsoluteLowerLabel;
    private JLabel TDBabsoluteUpperLabel;
    private JLabel TDBrelativePeriodLabel;
    private JLabel TDBrelativeLowerLabel;
    private JLabel TDBrelativeUpperLabel;
    private JLabel TDBthresholdPeriodLabel;
    private JLabel TDBthresholdLowerLabel;
    private JLabel TDBthresholdUpperLabel;
    private JLabel TDBdifferencePeriodLabel;
    // TDB objects
    // /////////////////////////

    // TTS objects
    private JPanel TTSPanel;
    private JLabel TTSperiodLabel;
    private JTextField TTSperiodField;
    private JLabel TTSmodesLabel;
    private JCheckBox TTSPeriodicalCheck;
    private JCheckBox TTSAbsoluteCheck;
    private JCheckBox TTSRelativeCheck;
    private JCheckBox TTSThresholdCheck;
    private JCheckBox TTSDifferenceCheck;
    private JCheckBox TTSAbsoluteDLCheck;
    private JCheckBox TTSRelativeDLCheck;
    private JLabel TTSPeriodicalLabel;
    private JLabel TTSAbsoluteLabel;
    private JLabel TTSRelativeLabel;
    private JLabel TTSThresholdLabel;
    private JLabel TTSDifferenceLabel;
    private JTextField TTSabsolutePeriodField;
    private JTextField TTSabsoluteLowerField;
    private JTextField TTSabsoluteUpperField;
    private JTextField TTSrelativePeriodField;
    private JTextField TTSrelativeLowerField;
    private JTextField TTSrelativeUpperField;
    private JTextField TTSthresholdPeriodField;
    private JTextField TTSthresholdLowerField;
    private JTextField TTSthresholdUpperField;
    private JTextField TTSdifferencePeriodField;
    private JLabel TTSabsolutePeriodLabel;
    private JLabel TTSabsoluteLowerLabel;
    private JLabel TTSabsoluteUpperLabel;
    private JLabel TTSrelativePeriodLabel;
    private JLabel TTSrelativeLowerLabel;
    private JLabel TTSrelativeUpperLabel;
    private JLabel TTSthresholdPeriodLabel;
    private JLabel TTSthresholdLowerLabel;
    private JLabel TTSthresholdUpperLabel;
    private JLabel TTSdifferencePeriodLabel;
    // TTS objects
    // /////////////////////////

    // general objects
    private JButton saveDefaultsButton;
    private JButton loadDefaultsButton;
    private JButton restoreDefaultsButton;
    private JPanel centerPanel;
    private JScrollPane centerScrollPane;
    private Box buttonsBox;
    // general objects

    public static final String DEFAULT_HDB_PERIOD_VALUE = "60000";
    public static final String DEFAULT_TDB_PERIOD_VALUE = "1000";
    public static final String DEFAULT_TTS_PERIOD_VALUE = "1000";

    public static ACTabDefaultPanel getInstance() {
        if (instance == null) {
            instance = new ACTabDefaultPanel();
        }

        return instance;
    }

    public static void resetInstance() {
        if (instance != null) {
            instance.repaint();
        }
        instance = null;
    }

    private ACTabDefaultPanel() {
        initComponents();
        initLayout();
        addComponents();

        ArchivingConfigurationAttributeProperties currentProperties = new ArchivingConfigurationAttributeProperties();
        ArchivingConfigurationAttributeProperties.setCurrentProperties(currentProperties);
    }

    private void initLayout() {
        setLayout(new BorderLayout());
    }

    private void addComponents() {
        add(centerScrollPane, BorderLayout.CENTER);
        add(buttonsBox, BorderLayout.PAGE_END);
    }

    protected JTextField generateTextField(int columns) {
        JTextField textField = new JTextField(columns);
        DocumentNumber nb = new DocumentNumber();
        nb.setAllowFloatValues(false);
        nb.setAllowNegativeValues(false);
        textField.setDocument(nb);
        nb.addDocumentListener(new DocumentListener() {

            protected void treatEvent(DocumentEvent e) {
                SwingUtilities.invokeLater(() -> {
                    String text = textField.getText();
                    textField.setToolTipText(text == null || text.trim().isEmpty() ? null : text.trim());
                });
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                treatEvent(e);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                treatEvent(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // not managed
            }
        });
        return textField;
    }

    private void initComponents() {
        initButtonsBox();

        String msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_MODES");
        HDBmodesLabel = new JLabel(msg);
        HDBmodesLabel.setFont(HDBmodesLabel.getFont().deriveFont(Font.ITALIC));
        TDBmodesLabel = new JLabel(msg);
        TDBmodesLabel.setFont(TDBmodesLabel.getFont().deriveFont(Font.ITALIC));
        TTSmodesLabel = new JLabel(msg);
        TTSmodesLabel.setFont(TTSmodesLabel.getFont().deriveFont(Font.ITALIC));

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIODICAL_MODE");
        HDBPeriodicalLabel = new JLabel(msg);
        TDBPeriodicalLabel = new JLabel(msg);
        TTSPeriodicalLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_ABSOLUTE_MODE");
        HDBAbsoluteLabel = new JLabel(msg);
        TDBAbsoluteLabel = new JLabel(msg);
        TTSAbsoluteLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_RELATIVE_MODE");
        HDBRelativeLabel = new JLabel(msg);
        TDBRelativeLabel = new JLabel(msg);
        TTSRelativeLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_THRESHOLD_MODE");
        HDBThresholdLabel = new JLabel(msg);
        TDBThresholdLabel = new JLabel(msg);
        TTSThresholdLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_DIFFERENCE_MODE");
        HDBDifferenceLabel = new JLabel(msg);
        TDBDifferenceLabel = new JLabel(msg);
        TTSDifferenceLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_HDB");
        HDBperiodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_TDB");
        TDBperiodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_TTS");
        TTSperiodLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD");
        TDBexportPeriodLabel = new JLabel(msg);

        TDBexportPeriodComboBox = new ACTdbExportPeriodComboBox();
        // set this parameter not visible, it is inserted as TdbArchiver class property
        TDBexportPeriodLabel.setVisible(false);
        TDBexportPeriodComboBox.setVisible(false);

        int periodColumns = 5;

        HDBperiodField = generateTextField(periodColumns);
        TDBperiodField = generateTextField(periodColumns);
        TTSperiodField = generateTextField(periodColumns);

        setHDBPeriod(Integer.parseInt(DEFAULT_HDB_PERIOD_VALUE));
        TDBperiodField.setText(DEFAULT_TDB_PERIOD_VALUE);
        TTSperiodField.setText(DEFAULT_TTS_PERIOD_VALUE);

        HDBPeriodicalCheck = new JCheckBox();
        HDBPeriodicalCheck.setEnabled(false);
        HDBAbsoluteCheck = new JCheckBox();
        HDBRelativeCheck = new JCheckBox();
        HDBThresholdCheck = new JCheckBox();
        HDBDifferenceCheck = new JCheckBox();

        TDBPeriodicalCheck = new JCheckBox();
        TDBPeriodicalCheck.setEnabled(false);
        TDBAbsoluteCheck = new JCheckBox();
        TDBRelativeCheck = new JCheckBox();
        TDBThresholdCheck = new JCheckBox();
        TDBDifferenceCheck = new JCheckBox();

        TTSPeriodicalCheck = new JCheckBox();
        TTSPeriodicalCheck.setEnabled(false);
        TTSAbsoluteCheck = new JCheckBox();
        TTSRelativeCheck = new JCheckBox();
        TTSThresholdCheck = new JCheckBox();
        TTSDifferenceCheck = new JCheckBox();

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_PERIOD_HDB");
        HDBabsolutePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_PERIOD_TDB");
        TDBabsolutePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_PERIOD_TTS");
        TTSabsolutePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_LOWER");
        HDBabsoluteLowerLabel = new JLabel(msg);
        TDBabsoluteLowerLabel = new JLabel(msg);
        TTSabsoluteLowerLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_UPPER");
        HDBabsoluteUpperLabel = new JLabel(msg);
        TDBabsoluteUpperLabel = new JLabel(msg);
        TTSabsoluteUpperLabel = new JLabel(msg);

        int columns = 3;

        HDBabsolutePeriodField = generateTextField(columns);
        TDBabsolutePeriodField = generateTextField(columns);
        TTSabsolutePeriodField = generateTextField(columns);
        HDBabsoluteLowerField = generateTextField(columns);
        TDBabsoluteLowerField = generateTextField(columns);
        TTSabsoluteLowerField = generateTextField(columns);
        HDBabsoluteUpperField = generateTextField(columns);
        TDBabsoluteUpperField = generateTextField(columns);
        TTSabsoluteUpperField = generateTextField(columns);
        msg = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_SLOW_DRIFT");
        HDBAbsoluteDLCheck = new JCheckBox(msg);
        TDBAbsoluteDLCheck = new JCheckBox(msg);
        TTSAbsoluteDLCheck = new JCheckBox(msg);

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_PERIOD_HDB");
        HDBrelativePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_PERIOD_TDB");
        TDBrelativePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_PERIOD_TTS");
        TTSrelativePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_LOWER");
        HDBrelativeLowerLabel = new JLabel(msg);
        TDBrelativeLowerLabel = new JLabel(msg);
        TTSrelativeLowerLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_UPPER");
        HDBrelativeUpperLabel = new JLabel(msg);
        TDBrelativeUpperLabel = new JLabel(msg);
        TTSrelativeUpperLabel = new JLabel(msg);

        HDBrelativePeriodField = generateTextField(columns);
        TDBrelativePeriodField = generateTextField(columns);
        TTSrelativePeriodField = generateTextField(columns);
        HDBrelativeLowerField = generateTextField(columns);
        TDBrelativeLowerField = generateTextField(columns);
        TTSrelativeLowerField = generateTextField(columns);
        HDBrelativeUpperField = generateTextField(columns);
        TDBrelativeUpperField = generateTextField(columns);
        TTSrelativeUpperField = generateTextField(columns);
        // adding slow drift check box
        msg = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_SLOW_DRIFT");
        HDBRelativeDLCheck = new JCheckBox(msg);
        TDBRelativeDLCheck = new JCheckBox(msg);
        TTSRelativeDLCheck = new JCheckBox(msg);

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_PERIOD_HDB");
        HDBthresholdPeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_PERIOD_TDB");
        TDBthresholdPeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_PERIOD_TTS");
        TTSthresholdPeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_LOWER");
        HDBthresholdLowerLabel = new JLabel(msg);
        TDBthresholdLowerLabel = new JLabel(msg);
        TTSthresholdLowerLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_UPPER");
        HDBthresholdUpperLabel = new JLabel(msg);
        TDBthresholdUpperLabel = new JLabel(msg);
        TTSthresholdUpperLabel = new JLabel(msg);

        HDBthresholdPeriodField = generateTextField(columns);
        TDBthresholdPeriodField = generateTextField(columns);
        TTSthresholdPeriodField = generateTextField(columns);
        HDBthresholdLowerField = generateTextField(columns);
        TDBthresholdLowerField = generateTextField(columns);
        TTSthresholdLowerField = generateTextField(columns);
        HDBthresholdUpperField = generateTextField(columns);
        TDBthresholdUpperField = generateTextField(columns);
        TTSthresholdUpperField = generateTextField(columns);

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_DIFFERENCE_PERIOD_HDB");
        HDBdifferencePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_DIFFERENCE_PERIOD_TDB");
        TDBdifferencePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_DIFFERENCE_PERIOD_TTS");
        TTSdifferencePeriodLabel = new JLabel(msg);

        HDBdifferencePeriodField = generateTextField(columns);
        TDBdifferencePeriodField = generateTextField(columns);
        TTSdifferencePeriodField = generateTextField(columns);

        initCenterPanel();
    }

    private void initButtonsBox() {
        String msg;

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ACTIONS_SAVE");
        saveDefaultsButton = new JButton(new SaveACDefaults(msg));

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ACTIONS_LOAD");
        loadDefaultsButton = new JButton(new LoadACDefaults(msg, false));

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ACTIONS_RESTORE");
        restoreDefaultsButton = new JButton(new LoadACDefaults(msg, true));

        buttonsBox = new Box(BoxLayout.X_AXIS);
        buttonsBox.add(Box.createHorizontalGlue());
        buttonsBox.add(saveDefaultsButton);
        buttonsBox.add(Box.createHorizontalStrut(5));
        buttonsBox.add(loadDefaultsButton);
        buttonsBox.add(Box.createHorizontalStrut(5));
        buttonsBox.add(restoreDefaultsButton);
        buttonsBox.add(Box.createHorizontalGlue());
    }

    protected void addComponents(JPanel panel, int y, JComponent... components) {
        int x = 0, index = 0;
        double weight = 1.0d / 3.0d;
        for (JComponent component : components) {
            GridBagConstraints constraints = new GridBagConstraints();
            if (index > 2 && index % 2 != 0) {
                constraints.fill = GridBagConstraints.HORIZONTAL;
                constraints.weightx = weight;
            } else {
                constraints.fill = GridBagConstraints.NONE;
                constraints.weightx = 0;
            }
            constraints.gridx = x++;
            constraints.gridy = y;
            constraints.weighty = 0;
            constraints.insets = new Insets(y > 0 ? 5 : 0, index != 0 && index % 2 == 0 ? 10 : 0, 0, 0);
            constraints.anchor = GridBagConstraints.WEST;
            panel.add(component, constraints);
            index++;
        }
    }

    private void initCenterPanel() {
        centerPanel = new JPanel();
        HDBPanel = new JPanel(new GridBagLayout());
        TDBPanel = new JPanel(new GridBagLayout());
        TTSPanel = new JPanel(new GridBagLayout());
        Font titleFont = new Font(Font.DIALOG, Font.BOLD, 18);
        Color hdbColor = new Color(239, 155, 0);
        Color tdbColor = new Color(140, 140, 60);
        Color ttsColor = Color.BLUE;

        int y = 0;
        HDBPanel.setBorder(new TitledBorder(new LineBorder(hdbColor),
                Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_HDB"), TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, titleFont, hdbColor));
        addComponents(HDBPanel, y++, HDBperiodLabel, HDBperiodField);
        addComponents(HDBPanel, y++, HDBmodesLabel);
        addComponents(HDBPanel, y++, HDBPeriodicalLabel, HDBPeriodicalCheck);
        addComponents(HDBPanel, y++, HDBAbsoluteLabel, HDBAbsoluteCheck, HDBabsolutePeriodLabel, HDBabsolutePeriodField,
                HDBabsoluteLowerLabel, HDBabsoluteLowerField, HDBabsoluteUpperLabel, HDBabsoluteUpperField,
                HDBAbsoluteDLCheck);
        addComponents(HDBPanel, y++, HDBRelativeLabel, HDBRelativeCheck, HDBrelativePeriodLabel, HDBrelativePeriodField,
                HDBrelativeLowerLabel, HDBrelativeLowerField, HDBrelativeUpperLabel, HDBrelativeUpperField,
                HDBRelativeDLCheck);
        addComponents(HDBPanel, y++, HDBThresholdLabel, HDBThresholdCheck, HDBthresholdPeriodLabel,
                HDBthresholdPeriodField, HDBthresholdLowerLabel, HDBthresholdLowerField, HDBthresholdUpperLabel,
                HDBthresholdUpperField);
        addComponents(HDBPanel, y++, HDBDifferenceLabel, HDBDifferenceCheck, HDBdifferencePeriodLabel,
                HDBdifferencePeriodField);

        // ---------------------------
        y = 0;
        TDBPanel.setBorder(new TitledBorder(new LineBorder(tdbColor),
                Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_TDB"), TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, titleFont, tdbColor));
        addComponents(TDBPanel, y++, TDBperiodLabel, TDBperiodField);
        addComponents(TDBPanel, y++, TDBexportPeriodLabel, TDBexportPeriodComboBox);
        addComponents(TDBPanel, y++, TDBmodesLabel);
        addComponents(TDBPanel, y++, TDBPeriodicalLabel, TDBPeriodicalCheck);
        addComponents(TDBPanel, y++, TDBAbsoluteLabel, TDBAbsoluteCheck, TDBabsolutePeriodLabel, TDBabsolutePeriodField,
                TDBabsoluteLowerLabel, TDBabsoluteLowerField, TDBabsoluteUpperLabel, TDBabsoluteUpperField,
                TDBAbsoluteDLCheck);
        addComponents(TDBPanel, y++, TDBRelativeLabel, TDBRelativeCheck, TDBrelativePeriodLabel, TDBrelativePeriodField,
                TDBrelativeLowerLabel, TDBrelativeLowerField, TDBrelativeUpperLabel, TDBrelativeUpperField,
                TDBRelativeDLCheck);
        addComponents(TDBPanel, y++, TDBThresholdLabel, TDBThresholdCheck, TDBthresholdPeriodLabel,
                TDBthresholdPeriodField, TDBthresholdLowerLabel, TDBthresholdLowerField, TDBthresholdUpperLabel,
                TDBthresholdUpperField);
        addComponents(TDBPanel, y++, TDBDifferenceLabel, TDBDifferenceCheck, TDBdifferencePeriodLabel,
                TDBdifferencePeriodField);
        // -------------------------

        // ---------------------------
        y = 0;
        TTSPanel.setBorder(new TitledBorder(new LineBorder(ttsColor),
                Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_TTS"), TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, titleFont, ttsColor));
        addComponents(TTSPanel, y++, TTSperiodLabel, TTSperiodField);
        addComponents(TTSPanel, y++, TTSmodesLabel);
        addComponents(TTSPanel, y++, TTSPeriodicalLabel, TTSPeriodicalCheck);
        addComponents(TTSPanel, y++, TTSAbsoluteLabel, TTSAbsoluteCheck, TTSabsolutePeriodLabel, TTSabsolutePeriodField,
                TTSabsoluteLowerLabel, TTSabsoluteLowerField, TTSabsoluteUpperLabel, TTSabsoluteUpperField,
                TTSAbsoluteDLCheck);
        addComponents(TTSPanel, y++, TTSRelativeLabel, TTSRelativeCheck, TTSrelativePeriodLabel, TTSrelativePeriodField,
                TTSrelativeLowerLabel, TTSrelativeLowerField, TTSrelativeUpperLabel, TTSrelativeUpperField,
                TTSRelativeDLCheck);
        addComponents(TTSPanel, y++, TTSThresholdLabel, TTSThresholdCheck, TTSthresholdPeriodLabel,
                TTSthresholdPeriodField, TTSthresholdLowerLabel, TTSthresholdLowerField, TTSthresholdUpperLabel,
                TTSthresholdUpperField);
        addComponents(TTSPanel, y++, TTSDifferenceLabel, TTSDifferenceCheck, TTSdifferencePeriodLabel,
                TTSdifferencePeriodField);
        // ----------------------------

        centerPanel.add(HDBPanel);

        JPanel warningPanel = new JPanel(new SpringLayout());
        String warning = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_WARNING1");
        warning += Integer.MAX_VALUE + "ms (=" + Integer.MAX_VALUE / 1000 + "s). ";
        warning += Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_WARNING2");
        JLabel warningLabel = new JLabel(warning);
//        Font font = new Font("Arial", Font.BOLD, 10);
        warningLabel.setForeground(Color.RED);
        JLabel label = new JLabel("*");
//        label.setFont(font);
        warningPanel.add(label);
        warningPanel.add(warningLabel);
        SpringUtilities.makeCompactGrid(warningPanel, 1, 2, // rows, cols
                6, 6, // initX, initY
                6, 6, true); // xPad, yPad

        centerPanel.add(TDBPanel);

        centerPanel.add(TTSPanel);

        centerPanel.setLayout(new SpringLayout());
        // Lay out the panel.
        SpringUtilities.makeCompactGrid(centerPanel, 3, 1, // rows, cols
                0, 0, // initX, initY
                0, 0, true); // xPad, yPad

        centerPanel.setBorder(new EmptyBorder(0, 0, 0, 5));

        centerScrollPane = new JScrollPane(centerPanel);
        centerScrollPane.setBorder(new EmptyBorder(0, 0, 5, 0));
        centerScrollPane.setColumnHeaderView(warningPanel);
    }

    /**
     * @return 22 juil. 2005
     */
    public long getExportPeriod() {
        return TDBexportPeriodComboBox.getExportPeriod();
    }

    /**
     * @param historic
     * @param type_a
     *            22 juil. 2005
     */
    public void doSelectMode(Boolean historic, int type) {
        JCheckBox checkBoxToSelect = null;

        switch (type) {
            case ArchivingConfigurationMode.TYPE_A:
                if (historic == null) {
                    checkBoxToSelect = TTSAbsoluteCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = HDBAbsoluteCheck;
                } else {
                    checkBoxToSelect = TDBAbsoluteCheck;
                }
                break;

            case ArchivingConfigurationMode.TYPE_D:
                if (historic == null) {
                    checkBoxToSelect = TTSDifferenceCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = HDBDifferenceCheck;
                } else {
                    checkBoxToSelect = TDBDifferenceCheck;
                }
                break;

            case ArchivingConfigurationMode.TYPE_P:
                if (historic == null) {
                    checkBoxToSelect = TTSPeriodicalCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = HDBPeriodicalCheck;
                } else {
                    checkBoxToSelect = TDBPeriodicalCheck;
                }
                break;

            case ArchivingConfigurationMode.TYPE_T:
                if (historic == null) {
                    checkBoxToSelect = TTSThresholdCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = HDBThresholdCheck;
                } else {
                    checkBoxToSelect = TDBThresholdCheck;
                }
                break;

            case ArchivingConfigurationMode.TYPE_R:
                if (historic == null) {
                    checkBoxToSelect = TTSRelativeCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = HDBRelativeCheck;
                } else {
                    checkBoxToSelect = TDBRelativeCheck;
                }
                break;
        }

        checkBoxToSelect.setSelected(true);
    }

    /**
     * @param modesTypes
     *            25 juil. 2005
     */
    public void setSelectedModes(Boolean historic, List<Integer> modesTypes) {
        if (historic == null) {
            TTSRelativeCheck.setSelected(false);
            TTSPeriodicalCheck.setSelected(false);
            TTSAbsoluteCheck.setSelected(false);
            TTSDifferenceCheck.setSelected(false);
            TTSThresholdCheck.setSelected(false);
        } else if (historic.booleanValue()) {
            HDBRelativeCheck.setSelected(false);
            HDBPeriodicalCheck.setSelected(false);
            HDBAbsoluteCheck.setSelected(false);
            HDBDifferenceCheck.setSelected(false);
            HDBThresholdCheck.setSelected(false);
        } else {
            TDBRelativeCheck.setSelected(false);
            TDBPeriodicalCheck.setSelected(false);
            TDBAbsoluteCheck.setSelected(false);
            TDBDifferenceCheck.setSelected(false);
            TDBThresholdCheck.setSelected(false);
        }

        if (modesTypes != null) {
            for (Integer _nextModeType : modesTypes) {
                int nextModeType = _nextModeType.intValue();
                doSelectMode(historic, nextModeType);
            }
        }
    }

    // ------------getters/setter

    // *************** HDB ***************
    public String getHDBAbsoluteUpper() {
        return HDBabsoluteUpperField.getText();
    }

    public String getHDBRelativeUpper() {
        return HDBrelativeUpperField.getText();
    }

    public String getHDBThresholdUpper() {
        return HDBthresholdUpperField.getText();
    }

    public String getHDBAbsoluteLower() {
        return HDBabsoluteLowerField.getText();
    }

    public String getHDBRelativeLower() {
        return HDBrelativeLowerField.getText();
    }

    public String getHDBThresholdLower() {
        return HDBthresholdLowerField.getText();
    }

    public void setHDBAbsoluteUpper(String val) {
        HDBabsoluteUpperField.setText(val);
    }

    public void setHDBRelativeUpper(String val) {
        HDBrelativeUpperField.setText(val);
    }

    public void setHDBThresholdUpper(String val) {
        HDBthresholdUpperField.setText(val);
    }

    public void setHDBAbsoluteLower(String val) {
        HDBabsoluteLowerField.setText(val);
    }

    public void setHDBRelativeLower(String val) {
        HDBrelativeLowerField.setText(val);
    }

    public void setHDBThresholdLower(String val) {
        HDBthresholdLowerField.setText(val);
    }

    /**
     * @param period
     *            25 juil. 2005
     */
    public void setHDBPeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        HDBperiodField.setText(text);
    }

    public int getHDBperiod() {
        try {
            String s = HDBperiodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getHDBAbsolutePeriod() {
        try {
            String s = HDBabsolutePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getHDBRelativePeriod() {
        try {
            String s = HDBrelativePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getHDBThresholdPeriod() {
        try {
            String s = HDBthresholdPeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getHDBDifferencePeriod() {
        try {
            String s = HDBdifferencePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public void setHDBAbsolutePeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        HDBabsolutePeriodField.setText(text);
    }

    public void setHDBRelativePeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        HDBrelativePeriodField.setText(text);
    }

    public void setHDBThresholdPeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        HDBthresholdPeriodField.setText(text);
    }

    public void setHDBDifferencePeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        HDBdifferencePeriodField.setText(text);
    }

    public boolean hasModeHDB_A() {
        return HDBAbsoluteCheck.isSelected();
    }

    public boolean hasModeHDB_P() {
        return HDBPeriodicalCheck.isSelected();
    }

    public boolean hasModeHDB_R() {
        return HDBRelativeCheck.isSelected();
    }

    public boolean hasModeHDB_T() {
        return HDBThresholdCheck.isSelected();
    }

    public boolean hasModeHDB_D() {
        return HDBDifferenceCheck.isSelected();
    }

    public boolean getHDBAbsoluteDLCheck() {
        return HDBAbsoluteDLCheck.isSelected();
    }

    public boolean getHDBRelativeDLCheck() {
        return HDBRelativeDLCheck.isSelected();
    }

    public void setHDBAbsoluteDLCheck(boolean b) {
        HDBAbsoluteDLCheck.setSelected(b);
    }

    public void setHDBRelativeDLCheck(boolean b) {
        HDBRelativeDLCheck.setSelected(b);
    }
    // ***********************************

    // *************** TDB ***************
    public String getTDBAbsoluteUpper() {
        return TDBabsoluteUpperField.getText();
    }

    public String getTDBRelativeUpper() {
        return TDBrelativeUpperField.getText();
    }

    public String getTDBThresholdUpper() {
        return TDBthresholdUpperField.getText();
    }

    public String getTDBAbsoluteLower() {
        return TDBabsoluteLowerField.getText();
    }

    public String getTDBRelativeLower() {
        return TDBrelativeLowerField.getText();
    }

    public String getTDBThresholdLower() {
        return TDBthresholdLowerField.getText();
    }

    public void setTDBAbsoluteUpper(String val) {
        TDBabsoluteUpperField.setText(val);
    }

    public void setTDBRelativeUpper(String val) {
        TDBrelativeUpperField.setText(val);
    }

    public void setTDBThresholdUpper(String val) {
        TDBthresholdUpperField.setText(val);
    }

    public void setTDBAbsoluteLower(String val) {
        TDBabsoluteLowerField.setText(val);
    }

    public void setTDBRelativeLower(String val) {
        TDBrelativeLowerField.setText(val);
    }

    public void setTDBThresholdLower(String val) {
        TDBthresholdLowerField.setText(val);
    }

    public int getTDBperiod() {
        try {
            String s = TDBperiodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getTDBAbsolutePeriod() {
        try {
            String s = TDBabsolutePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getTDBRelativePeriod() {
        try {
            String s = TDBrelativePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getTDBThresholdPeriod() {
        try {
            String s = TDBthresholdPeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getTDBDifferencePeriod() {
        try {
            String s = TDBdifferencePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public void setTDBAbsolutePeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TDBabsolutePeriodField.setText(text);
    }

    public void setTDBRelativePeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TDBrelativePeriodField.setText(text);
    }

    public void setTDBThresholdPeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TDBthresholdPeriodField.setText(text);
    }

    public void setTDBDifferencePeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TDBdifferencePeriodField.setText(text);
    }

    /**
     * @param period
     *            25 juil. 2005
     */
    public void setTDBPeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TDBperiodField.setText(text);
    }

    /**
     * @param period
     *            25 juil. 2005
     */
    public void setExportPeriod(long period) {
        TDBexportPeriodComboBox.setExportPeriod(period);
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_A() {
        return TDBAbsoluteCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_P() {
        return TDBPeriodicalCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_R() {
        return TDBRelativeCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_T() {
        return TDBThresholdCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_D() {
        return TDBDifferenceCheck.isSelected();
    }

    public boolean getTDBAbsoluteDLCheck() {
        return TDBAbsoluteDLCheck.isSelected();
    }

    public boolean getTDBRelativeDLCheck() {
        return TDBRelativeDLCheck.isSelected();
    }

    public void setTDBAbsoluteDLCheck(boolean b) {
        TDBAbsoluteDLCheck.setSelected(b);
    }

    public void setTDBRelativeDLCheck(boolean b) {
        TDBRelativeDLCheck.setSelected(b);
    }
    // ***********************************

    // *************** TTS ***************
    public String getTTSAbsoluteUpper() {
        return TTSabsoluteUpperField.getText();
    }

    public String getTTSRelativeUpper() {
        return TTSrelativeUpperField.getText();
    }

    public String getTTSThresholdUpper() {
        return TTSthresholdUpperField.getText();
    }

    public String getTTSAbsoluteLower() {
        return TTSabsoluteLowerField.getText();
    }

    public String getTTSRelativeLower() {
        return TTSrelativeLowerField.getText();
    }

    public String getTTSThresholdLower() {
        return TTSthresholdLowerField.getText();
    }

    public void setTTSAbsoluteUpper(String val) {
        TTSabsoluteUpperField.setText(val);
    }

    public void setTTSRelativeUpper(String val) {
        TTSrelativeUpperField.setText(val);
    }

    public void setTTSThresholdUpper(String val) {
        TTSthresholdUpperField.setText(val);
    }

    public void setTTSAbsoluteLower(String val) {
        TTSabsoluteLowerField.setText(val);
    }

    public void setTTSRelativeLower(String val) {
        TTSrelativeLowerField.setText(val);
    }

    public void setTTSThresholdLower(String val) {
        TTSthresholdLowerField.setText(val);
    }

    public int getTTSperiod() {
        try {
            String s = TTSperiodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getTTSAbsolutePeriod() {
        try {
            String s = TTSabsolutePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getTTSRelativePeriod() {
        try {
            String s = TTSrelativePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getTTSThresholdPeriod() {
        try {
            String s = TTSthresholdPeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public int getTTSDifferencePeriod() {
        try {
            String s = TTSdifferencePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (Exception e) {
            return -1;
        }
    }

    public void setTTSAbsolutePeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TTSabsolutePeriodField.setText(text);
    }

    public void setTTSRelativePeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TTSrelativePeriodField.setText(text);
    }

    public void setTTSThresholdPeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TTSthresholdPeriodField.setText(text);
    }

    public void setTTSDifferencePeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TTSdifferencePeriodField.setText(text);
    }

    /**
     * @param period
     *            25 juil. 2005
     */
    public void setTTSPeriod(int period) {
        String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        TTSperiodField.setText(text);
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_A() {
        return TTSAbsoluteCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_P() {
        return TTSPeriodicalCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_R() {
        return TTSRelativeCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_T() {
        return TTSThresholdCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_D() {
        return TTSDifferenceCheck.isSelected();
    }

    public boolean getTTSAbsoluteDLCheck() {
        return TTSAbsoluteDLCheck.isSelected();
    }

    public boolean getTTSRelativeDLCheck() {
        return TTSRelativeDLCheck.isSelected();
    }

    public void setTTSAbsoluteDLCheck(boolean b) {
        TTSAbsoluteDLCheck.setSelected(b);
    }

    public void setTTSRelativeDLCheck(boolean b) {
        TTSRelativeDLCheck.setSelected(b);
    }
    // ***********************************

    /**
     * 13 sept. 2005
     */
    public void doUnselectAllModes() {
        TDBPeriodicalCheck.setSelected(false);
        TDBAbsoluteCheck.setSelected(false);
        TDBRelativeCheck.setSelected(false);
        TDBThresholdCheck.setSelected(false);
        TDBDifferenceCheck.setSelected(false);

        TTSPeriodicalCheck.setSelected(false);
        TTSAbsoluteCheck.setSelected(false);
        TTSRelativeCheck.setSelected(false);
        TTSThresholdCheck.setSelected(false);
        TTSDifferenceCheck.setSelected(false);

        HDBPeriodicalCheck.setSelected(false);
        HDBAbsoluteCheck.setSelected(false);
        HDBRelativeCheck.setSelected(false);
        HDBThresholdCheck.setSelected(false);
        HDBDifferenceCheck.setSelected(false);
    }

}
