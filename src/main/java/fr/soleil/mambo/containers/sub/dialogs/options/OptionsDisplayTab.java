package fr.soleil.mambo.containers.sub.dialogs.options;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SpringLayout;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.options.sub.DisplayOptions;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.SpringUtilities;

public class OptionsDisplayTab extends JPanel {

    private static final long serialVersionUID = -5210394820545486196L;

    private static OptionsDisplayTab instance = null;

    private JRadioButton winPLAF;
    private JRadioButton javaPLAF;
    private ButtonGroup buttonGroup;

    private Box emptyBox;
    private Box plafPanel;

    /**
     * @return 8 juil. 2005
     */
    public static OptionsDisplayTab getInstance() {
        if (instance == null) {
            instance = new OptionsDisplayTab();
        }

        return instance;
    }

    public static void resetInstance() {
        if (instance != null) {
            instance.repaint();
        }

        instance = null;
    }

    private OptionsDisplayTab() {
        this.initComponents();
        this.addComponents();
        this.initLayout();
    }

    private void initLayout() {
        this.setLayout(new SpringLayout());

        SpringUtilities.makeCompactGrid(this, 2, 1, // rows, cols
                6, 6, // initX, initY
                6, 6, // xPad, yPad
                true);

    }

    private void addComponents() {
        this.add(plafPanel);
        this.add(emptyBox);
    }

    private void initComponents() {
        buttonGroup = new ButtonGroup();

        String msg = Messages.getMessage("DIALOGS_OPTIONS_DISPLAY_WINDOWS");
        winPLAF = new JRadioButton(msg, true);
        winPLAF.setActionCommand(String.valueOf(DisplayOptions.PLAF_WIN));
        msg = Messages.getMessage("DIALOGS_OPTIONS_DISPLAY_JAVA");
        javaPLAF = new JRadioButton(msg, false);
        javaPLAF.setActionCommand(String.valueOf(DisplayOptions.PLAF_JAVA));
        buttonGroup.add(winPLAF);
        buttonGroup.add(javaPLAF);

        emptyBox = new Box(BoxLayout.Y_AXIS);
        emptyBox.add(Box.createVerticalGlue());

        plafPanel = new Box(BoxLayout.Y_AXIS);
        plafPanel.add(javaPLAF);
        plafPanel.add(Box.createVerticalStrut(10));
        plafPanel.add(winPLAF);

        msg = Messages.getMessage("DIALOGS_OPTIONS_DISPLAY_BORDER");
        TitledBorder tb3 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg,
                TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP, GUIUtilities.getTitleFont());
        plafPanel.setBorder(tb3);
    }

    /**
     * @return 8 juil. 2005
     */
    public ButtonGroup getButtonGroup() {
        return buttonGroup;
    }

    /**
     * @return 8 juil. 2005
     */
    public JRadioButton getJavaPLAF() {
        return javaPLAF;
    }

    /**
     * @return 8 juil. 2005
     */
    public JRadioButton getWinPLAF() {
        return winPLAF;
    }

    /**
     * 20 juil. 2005
     * 
     * @param plaf
     */
    public void selectPlafButton(int plaf) {
        switch (plaf) {
            case DisplayOptions.PLAF_WIN:
                winPLAF.setSelected(true);
                break;

            case DisplayOptions.PLAF_JAVA:
                javaPLAF.setSelected(true);
                break;
        }
    }
}
