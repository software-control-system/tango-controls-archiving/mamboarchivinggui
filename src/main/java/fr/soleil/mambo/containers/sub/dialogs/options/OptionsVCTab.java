package fr.soleil.mambo.containers.sub.dialogs.options;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.chart.DataViewOption;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.data.view.plot.Bar;
import fr.soleil.mambo.data.view.plot.Curve;
import fr.soleil.mambo.data.view.plot.Interpolation;
import fr.soleil.mambo.data.view.plot.Marker;
import fr.soleil.mambo.data.view.plot.MathPlot;
import fr.soleil.mambo.data.view.plot.Polynomial2OrderTransform;
import fr.soleil.mambo.data.view.plot.Smoothing;
import fr.soleil.mambo.options.sub.VCOptions;
import fr.soleil.mambo.tools.Messages;

public class OptionsVCTab extends JPanel {

    private static final long serialVersionUID = 8779779786006710175L;

    private static OptionsVCTab instance = null;

    private JRadioButton displayWriteYes;
    private JRadioButton displayWriteNo;
    private ButtonGroup writeButtonGroup;

    private JRadioButton displayReadYes;
    private JRadioButton displayReadNo;
    private ButtonGroup readButtonGroup;

    private JRadioButton forceTdbExportYes;
    private JRadioButton forceTdbExportNo;
    private ButtonGroup forceTdbExportButtonGroup;

    private JLabel stackDepthLabel;
    private JTextField stackDepthField;

    private JLabel maxDisplayedViewsLabel;
    private JTextField maxDisplayedViewsField;
    private JLabel maxDisplayedViewsWarningLabel;

    private JLabel dataFormatLabel;
    private JTextField dataFormatField;

    private JLabel noValueLabel;
    private JComboBox<String> noValueCombo;
    protected final static String[] noValueStringChoice = { "*", ".", "-", "+", "#", "X", "!" };

    private JLabel spectrumViewTypeLabel;
    private JComboBox<String> spectrumViewTypeCombo;

    private DataViewOption defaultDataViewOption;

    /**
     * @return 8 juil. 2005
     */
    public static OptionsVCTab getInstance() {
        if (instance == null) {
            instance = new OptionsVCTab();
        }

        return instance;
    }

    public static void resetInstance() {
        if (instance != null) {
            instance.repaint();
        }

        instance = null;
    }

    private void initLayout() {
        Box buttonBox = new Box(BoxLayout.Y_AXIS);
        buttonBox.add(displayReadYes);
        buttonBox.add(displayReadNo);

        Box displayReadBox = new Box(BoxLayout.X_AXIS);
        displayReadBox.add(buttonBox);
        displayReadBox.add(Box.createHorizontalGlue());

        String msg1 = Messages.getMessage("DIALOGS_OPTIONS_VC_DISPLAY_READ_BORDER");
        TitledBorder tb1 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                msg1, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP, GUIUtilities.getTitleFont());
        CompoundBorder cb1 = BorderFactory.createCompoundBorder(tb1, BorderFactory.createEmptyBorder(2, 4, 4, 4));
        displayReadBox.setBorder(cb1);

        // ---

        buttonBox = new Box(BoxLayout.Y_AXIS);
        buttonBox.add(displayWriteYes);
        buttonBox.add(displayWriteNo);

        Box displayWriteBox = new Box(BoxLayout.X_AXIS);
        displayWriteBox.add(buttonBox);
        displayWriteBox.add(Box.createHorizontalGlue());

        String msg2 = Messages.getMessage("DIALOGS_OPTIONS_VC_DISPLAY_WRITE_BORDER");
        TitledBorder tb2 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                msg2, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP, GUIUtilities.getTitleFont());
        CompoundBorder cb2 = BorderFactory.createCompoundBorder(tb2, BorderFactory.createEmptyBorder(2, 4, 4, 4));
        displayWriteBox.setBorder(cb2);

        // ---

        buttonBox = new Box(BoxLayout.Y_AXIS);
        buttonBox.add(forceTdbExportYes);
        buttonBox.add(forceTdbExportNo);

        Box forceTdbExportBox = new Box(BoxLayout.X_AXIS);
        forceTdbExportBox.add(buttonBox);
        forceTdbExportBox.add(Box.createHorizontalGlue());

        String msg3 = Messages.getMessage("DIALOGS_OPTIONS_VC_FORCE_TDB_EXPORT_BORDER");
        TitledBorder tb3 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                msg3, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP, GUIUtilities.getTitleFont());
        CompoundBorder cb3 = BorderFactory.createCompoundBorder(tb3, BorderFactory.createEmptyBorder(2, 4, 4, 4));
        forceTdbExportBox.setBorder(cb3);

        // ---

        Box miscBox = new Box(BoxLayout.X_AXIS);
        miscBox.add(stackDepthLabel);
        miscBox.add(Box.createRigidArea(new Dimension(5, 0)));
        miscBox.add(stackDepthField);
        miscBox.add(Box.createRigidArea(new Dimension(20, 0)));
        miscBox.add(maxDisplayedViewsLabel);
        miscBox.add(Box.createRigidArea(new Dimension(5, 0)));
        miscBox.add(maxDisplayedViewsField);
        miscBox.add(maxDisplayedViewsWarningLabel);
        miscBox.add(Box.createRigidArea(new Dimension(20, 0)));
        miscBox.add(dataFormatLabel);
        miscBox.add(Box.createRigidArea(new Dimension(5, 0)));
        miscBox.add(dataFormatField);
        miscBox.add(Box.createHorizontalGlue());

        stackDepthField.setPreferredSize(new Dimension(50, stackDepthField.getPreferredSize().height));
        stackDepthField.setMaximumSize(new Dimension(Integer.MAX_VALUE, stackDepthField.getPreferredSize().height));

        maxDisplayedViewsField.setPreferredSize(new Dimension(50, maxDisplayedViewsField.getPreferredSize().height));
        maxDisplayedViewsField
                .setMaximumSize(new Dimension(Integer.MAX_VALUE, maxDisplayedViewsField.getPreferredSize().height));

        dataFormatField.setPreferredSize(new Dimension(50, dataFormatField.getPreferredSize().height));
        dataFormatField.setMaximumSize(new Dimension(Integer.MAX_VALUE, dataFormatField.getPreferredSize().height));

        String msg4 = Messages.getMessage("DIALOGS_OPTIONS_VC_MISC_BORDER");
        TitledBorder tb4 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                msg4, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP);
        CompoundBorder cb4 = BorderFactory.createCompoundBorder(tb4, BorderFactory.createEmptyBorder(2, 4, 4, 4));
        miscBox.setBorder(cb4);

        // ---

        Box chartPropertiesBox = new Box(BoxLayout.X_AXIS);
        chartPropertiesBox.add(noValueLabel);
        chartPropertiesBox.add(Box.createRigidArea(new Dimension(5, 0)));
        chartPropertiesBox.add(noValueCombo);
        chartPropertiesBox.add(Box.createHorizontalGlue());

        noValueCombo.setPreferredSize(new Dimension(40, noValueCombo.getPreferredSize().height));
        noValueCombo.setMaximumSize(new Dimension(Integer.MAX_VALUE, noValueCombo.getPreferredSize().height));

        String msg5 = Messages.getMessage("DIALOGS_OPTIONS_VC_CHART_BORDER");
        TitledBorder tb5 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                msg5, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP);
        CompoundBorder cb5 = BorderFactory.createCompoundBorder(tb5, BorderFactory.createEmptyBorder(2, 4, 4, 4));
        chartPropertiesBox.setBorder(cb5);

        // ---

        Box spectrumViewBox = new Box(BoxLayout.X_AXIS);
        spectrumViewBox.add(spectrumViewTypeLabel);
        spectrumViewBox.add(Box.createRigidArea(new Dimension(5, 0)));
        spectrumViewBox.add(spectrumViewTypeCombo);
        spectrumViewBox.add(Box.createHorizontalGlue());

        spectrumViewTypeCombo.setPreferredSize(new Dimension(spectrumViewTypeCombo.getPreferredSize().width,
                spectrumViewTypeCombo.getPreferredSize().height));
        spectrumViewTypeCombo
                .setMaximumSize(new Dimension(Integer.MAX_VALUE, spectrumViewTypeCombo.getPreferredSize().height));

        String msg6 = Messages.getMessage("DIALOGS_OPTIONS_VC_SPECTRUM_VIEW_TYPE_BORDER");
        TitledBorder tb6 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                msg6, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP);
        CompoundBorder cb6 = BorderFactory.createCompoundBorder(tb6, BorderFactory.createEmptyBorder(2, 4, 4, 4));
        spectrumViewBox.setBorder(cb6);

        Box defaultDataViewOptionBox = new Box(BoxLayout.X_AXIS);
        defaultDataViewOptionBox.add(Box.createRigidArea(new Dimension(200, 0)));
        defaultDataViewOptionBox.add(defaultDataViewOption);
        defaultDataViewOptionBox.add(Box.createHorizontalGlue());

        defaultDataViewOption.setPreferredSize(new Dimension(defaultDataViewOption.getPreferredSize().width,
                defaultDataViewOption.getPreferredSize().height));
        defaultDataViewOption
                .setMaximumSize(new Dimension(Integer.MAX_VALUE, defaultDataViewOption.getPreferredSize().height));

        String msg7 = Messages.getMessage("DIALOGS_OPTIONS_VC_DEFAULT_DATA_VIEW_OPTIONS_BORDER");
        TitledBorder tb7 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                msg7, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP);
        CompoundBorder cb7 = BorderFactory.createCompoundBorder(tb7, BorderFactory.createEmptyBorder(2, 4, 4, 4));
        defaultDataViewOptionBox.setBorder(cb7);

        // ---

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        this.add(displayReadBox);
        this.add(Box.createVerticalStrut(4));
        this.add(displayWriteBox);
        this.add(Box.createVerticalStrut(4));
        this.add(forceTdbExportBox);
        this.add(Box.createVerticalStrut(4));
        this.add(miscBox);
        this.add(Box.createVerticalStrut(4));
        this.add(chartPropertiesBox);
        this.add(Box.createVerticalStrut(4));
        this.add(spectrumViewBox);
        this.add(Box.createVerticalStrut(4));
        this.add(defaultDataViewOptionBox);
        this.add(Box.createVerticalGlue());
    }

    /**
     *
     */
    private OptionsVCTab() {
        this.initComponents();
        this.initLayout();
    }

    /**
     *
     */
    private void initComponents() {
        String msg = Messages.getMessage("DIALOGS_OPTIONS_VC_DISPLAY_READ_YES");
        displayReadYes = new JRadioButton(msg, true);
        displayReadYes.setActionCommand(String.valueOf(VCOptions.DISPLAY_READ_YES));

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_DISPLAY_READ_NO");
        displayReadNo = new JRadioButton(msg, false);
        displayReadNo.setActionCommand(String.valueOf(VCOptions.DISPLAY_READ_NO));

        readButtonGroup = new ButtonGroup();
        readButtonGroup.add(displayReadYes);
        readButtonGroup.add(displayReadNo);

        // ---

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_DISPLAY_WRITE_YES");
        displayWriteYes = new JRadioButton(msg, true);
        displayWriteYes.setActionCommand(String.valueOf(VCOptions.DISPLAY_WRITE_YES));

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_DISPLAY_WRITE_NO");
        displayWriteNo = new JRadioButton(msg, false);
        displayWriteNo.setActionCommand(String.valueOf(VCOptions.DISPLAY_WRITE_NO));

        writeButtonGroup = new ButtonGroup();
        writeButtonGroup.add(displayWriteYes);
        writeButtonGroup.add(displayWriteNo);

        // ---

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_FORCE_TDB_EXPORT_YES");
        forceTdbExportYes = new JRadioButton(msg, true);
        forceTdbExportYes.setActionCommand(String.valueOf(VCOptions.FORCE_TDB_EXPORT_YES));

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_FORCE_TDB_EXPORT_NO");
        forceTdbExportNo = new JRadioButton(msg, false);
        forceTdbExportNo.setActionCommand(String.valueOf(VCOptions.FORCE_TDB_EXPORT_NO));
        forceTdbExportNo.setSelected(true);

        forceTdbExportButtonGroup = new ButtonGroup();
        forceTdbExportButtonGroup.add(forceTdbExportYes);
        forceTdbExportButtonGroup.add(forceTdbExportNo);

        // ---

        stackDepthField = new JTextField();

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_MISC_STACK_DEPTH");
        stackDepthLabel = new JLabel(msg);
        stackDepthLabel.setLabelFor(stackDepthField);

        maxDisplayedViewsField = new JTextField();

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_MISC_MAX_DISPLAYED_VIEWS");
        maxDisplayedViewsLabel = new JLabel(msg);
        maxDisplayedViewsLabel.setLabelFor(maxDisplayedViewsField);

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_MISC_MAX_DISPLAYED_VIEWS_WARNING");
        maxDisplayedViewsWarningLabel = new JLabel(msg);
        maxDisplayedViewsWarningLabel.setLabelFor(maxDisplayedViewsField);
        maxDisplayedViewsWarningLabel.setFont(GUIUtilities.labeliFont);

        dataFormatField = new JTextField();

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_DATA_FORMAT");
        dataFormatLabel = new JLabel(msg);
        dataFormatLabel.setLabelFor(dataFormatField);

        // ---

        noValueCombo = new JComboBox<>();
        for (String choice : noValueStringChoice) {
            noValueCombo.addItem(choice);
        }

        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_CHART_NO_VALUE_STRING");
        noValueLabel = new JLabel(msg);
        noValueLabel.setLabelFor(noValueCombo);

        // ---

        String[] msgListItem = new String[4];
        msgListItem[0] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_CHART_VIEW_TYPE_INDICE");
        msgListItem[1] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_CHART_VIEW_TYPE_TIME");
        msgListItem[2] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_CHART_VIEW_TYPE_TIME_STACK");
        msgListItem[3] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_CHART_VIEW_TYPE_TIME_IMAGE");
        spectrumViewTypeCombo = new JComboBox<>(new DefaultComboBoxModel<>(msgListItem));
        msg = Messages.getMessage("DIALOGS_OPTIONS_VC_SPECTRUM_VIEW_TYPE_STRING");
        spectrumViewTypeLabel = new JLabel(msg);
        spectrumViewTypeLabel.setLabelFor(spectrumViewTypeCombo);

        defaultDataViewOption = new DataViewOption() {

            private static final long serialVersionUID = 4784429523723349976L;

            @Override
            protected void initComponents() {
                super.initComponents();
                lineNameLabel.setEnabled(false);
                lineNameText.setEnabled(false);
                lineColorBtn.setEnabled(false);
                lineColorLabel.setEnabled(false);
                lineColorView.setEnabled(false);
                fillColorBtn.setEnabled(false);
                fillColorLabel.setEnabled(false);
                fillColorView.setEnabled(false);
                setAllButton.setEnabled(false);
                closeButton.setVisible(false);
                lineColorView.setBackground(Color.GRAY);
            }
        };
        defaultDataViewOption.setPlotProperties(initDefaultPlotProperties());

    }

    /**
     * @return 8 juil. 2005
     */
    public ButtonGroup getWriteButtonGroup() {
        return writeButtonGroup;
    }

    /**
     * 20 juil. 2005
     * 
     * @param plaf
     */
    public void selectDisplayWrite(int displayCase) {
        switch (displayCase) {
            case VCOptions.DISPLAY_WRITE_YES:
                displayWriteYes.setSelected(true);
                break;

            case VCOptions.DISPLAY_WRITE_NO:
                displayWriteNo.setSelected(true);
                break;
        }
    }

    /**
     * @return Returns the displayWriteNo.
     */
    public JRadioButton getDisplayWriteNo() {
        return displayWriteNo;
    }

    /**
     * @return Returns the displayWriteYes.
     */
    public JRadioButton getDisplayWriteYes() {
        return displayWriteYes;
    }

    /**
     * @return 8 juil. 2005
     */
    public ButtonGroup getReadButtonGroup() {
        return readButtonGroup;
    }

    /**
     * 20 juil. 2005
     * 
     * @param plaf
     */
    public void selectDisplayRead(int displayCase) {
        switch (displayCase) {
            case VCOptions.DISPLAY_READ_YES:
                displayReadYes.setSelected(true);
                break;

            case VCOptions.DISPLAY_READ_NO:
                displayReadNo.setSelected(true);
                break;
        }
    }

    public void selectForceTdbExport(int displayCase) {
        switch (displayCase) {
            case VCOptions.FORCE_TDB_EXPORT_YES:
                forceTdbExportYes.setSelected(true);
                break;

            case VCOptions.FORCE_TDB_EXPORT_NO:
                forceTdbExportNo.setSelected(true);
                break;

            default:
                forceTdbExportNo.setSelected(true);
                break;
        }
    }

    /**
     * @return Returns the displayReadNo.
     */
    public JRadioButton getDisplayReadNo() {
        return displayReadNo;
    }

    /**
     * @return Returns the displayReadYes.
     */
    public JRadioButton getDisplayReadYes() {
        return displayReadYes;
    }

    /**
     * @return
     */
    public String getStackDepth() {
        return stackDepthField.getText();
    }

    /**
     * @param stackDepth_s
     */
    public void setStackDepth(String stackDepth_s) {
        stackDepthField.setText(stackDepth_s);
    }

    /**
     * @return
     */
    public String getMaxDisplayedViews() {
        return maxDisplayedViewsField.getText();
    }

    /**
     * @param maxDisplayedViews_s
     */
    public void setMaxDisplayedViews(String maxDisplayedViews_s) {
        maxDisplayedViewsField.setText(maxDisplayedViews_s);
    }

    /**
     * @return
     */
    public String getDataFormat() {
        return dataFormatField.getText();
    }

    /**
     * @param dataFormat_s
     */
    public void setDataFormat(String dataFormat_s) {
        dataFormatField.setText(dataFormat_s);
    }

    /**
     * @return Returns the forceTdbExportButtonGroup.
     */
    public ButtonGroup getForceTdbExportButtonGroup() {
        return forceTdbExportButtonGroup;
    }

    public void setNoValueString(String noValueString) {
        for (int i = 0; i < noValueCombo.getItemCount(); i++) {
            if (noValueCombo.getItemAt(i).equals(noValueString)) {
                noValueCombo.setSelectedIndex(i);
                break;
            }
        }
    }

    public String getNoValueString() {
        return noValueCombo.getSelectedItem().toString();
    }

    public int getSelectedSpectrumViewType() {

        if (spectrumViewTypeCombo == null) {
            return -1;
        } else {
            return spectrumViewTypeCombo.getSelectedIndex();
        }
    }

    public void setSelectedSpectrumViewType(int type) {
        if (spectrumViewTypeCombo != null && type > -1) {
            spectrumViewTypeCombo.setSelectedIndex(type);
        }
    }

    public PlotProperties getDefaultPlotProperties() {
        return defaultDataViewOption.getPlotProperties();
    }

    public void setDefaultPlotProperties(PlotProperties properties) {
        if (properties != null) {
            defaultDataViewOption.setPlotProperties(properties);
        }
    }

    private PlotProperties initDefaultPlotProperties() {
        PlotProperties result = new PlotProperties();
        result.setBar(initBarBase());
        result.setCurve(initCurveBase());
        result.setMarker(initMarkerBase());
        result.setTransform(initTransformBase());
        result.setInterpolation(initInterpolationBase());
        result.setSmoothing(initSmoothingBase());
        result.setMath(initMathBase());
        return result;
    }

    private Bar initBarBase() {
        return new Bar(ColorTool.getCometeColor(Color.GRAY), 1, DEFAULT_BAR_FILL_STYLE, 0);
    }

    private Curve initCurveBase() {
        return new Curve(ColorTool.getCometeColor(Color.GRAY), DEFAULT_CURVE_WIDTH, DEFAULT_CURVE_LINE_STYLE,
                ObjectUtils.EMPTY_STRING);
    }

    private Marker initMarkerBase() {
        return new Marker(ColorTool.getCometeColor(Color.GRAY), DEFAULT_MARKER_SIZE, DEFAULT_MARKER_STYLE, true);
    }

    private Polynomial2OrderTransform initTransformBase() {
        return new Polynomial2OrderTransform(DEFAULT_TRANSFORM_A0, DEFAULT_TRANSFORM_A1, DEFAULT_TRANSFORM_A2);
    }

    private Interpolation initInterpolationBase() {
        return new Interpolation(DEFAULT_INTERPOLATION_METHOD, DEFAULT_INTERPOLATION_STEP,
                DEFAULT_INTERPOLATION_HERMITE_BIAS, DEFAULT_INTERPOLATION_HERMITE_TENSION);
    }

    private Smoothing initSmoothingBase() {
        return new Smoothing(DEFAULT_SMOOTHING_METHOD, DEFAULT_SMOOTHING_NEIGHBORS, DEFAULT_SMOOTHING_GAUSS_SIGMA,
                DEFAULT_SMOOTHING_EXTRAPOLATION);
    }

    private MathPlot initMathBase() {
        return new MathPlot(DEFAULT_MATH_FUNCTION);
    }

    // Curve
    private static final int DEFAULT_CURVE_WIDTH = 1; // 1
    private static final int DEFAULT_CURVE_LINE_STYLE = 0; // straits
    // bar
    private static final int DEFAULT_BAR_FILL_STYLE = 0; // no
    // marker
    private static final int DEFAULT_MARKER_SIZE = 5; // 5
    private static final int DEFAULT_MARKER_STYLE = 0; // None
    // transform
    private static final double DEFAULT_TRANSFORM_A0 = 0;
    private static final double DEFAULT_TRANSFORM_A1 = 1;
    private static final double DEFAULT_TRANSFORM_A2 = 0;

    // Interpolation
    private static final int DEFAULT_INTERPOLATION_METHOD = 0;
    private static final int DEFAULT_INTERPOLATION_STEP = 10;
    private static final double DEFAULT_INTERPOLATION_HERMITE_BIAS = 0;
    private static final double DEFAULT_INTERPOLATION_HERMITE_TENSION = 0;

    // Smoothing
    private static final int DEFAULT_SMOOTHING_METHOD = 0;
    private static final int DEFAULT_SMOOTHING_NEIGHBORS = 3;
    private static final double DEFAULT_SMOOTHING_GAUSS_SIGMA = 0.5;
    private static final int DEFAULT_SMOOTHING_EXTRAPOLATION = 2;

    // Math
    private static final int DEFAULT_MATH_FUNCTION = 0;
}
