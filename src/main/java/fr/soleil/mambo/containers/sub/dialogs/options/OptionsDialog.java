package fr.soleil.mambo.containers.sub.dialogs.options;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.CancelAction;
import fr.soleil.mambo.actions.ValidateOptionsAction;
import fr.soleil.mambo.actions.listeners.TabbedPaneListener;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.tools.Messages;

public class OptionsDialog extends JDialog {

    private static final long serialVersionUID = -1763601564875932691L;

    private Dimension dim = new Dimension(300, 400);

    private OptionsSaveOptionsTab saveTab;
    private OptionsGeneralTab miscTab;
    private OptionsACTab acTab;
    private OptionsVCTab vcTab;

    private JPanel myPanel;
    private JButton okButton;
    private JButton cancelButton;
    private JTabbedPane jTabbedPane;

    private static OptionsDialog menuDialogInstance = null;

    /**
     * @return 8 juil. 2005
     */
    public static OptionsDialog getInstance() {
        if (menuDialogInstance == null) {
            menuDialogInstance = new OptionsDialog();
        }

        return menuDialogInstance;
    }

    /**
     * @return 8 juil. 2005
     */
    public static void resetInstance() {
        if (menuDialogInstance != null) {
            menuDialogInstance.repaint();
        }

        menuDialogInstance = null;

        OptionsDisplayTab.resetInstance();
        OptionsSaveOptionsTab.resetInstance();
        OptionsACTab.resetInstance();
        OptionsVCTab.resetInstance();
    }

    private OptionsDialog() {
        super(MamboFrame.getInstance(), Messages.getMessage("DIALOGS_OPTIONS_TITLE"), true);
        initComponents();
        addComponents();
        initLayout();
    }

    /**
     * 5 juil. 2005
     */
    private void initComponents() {
        String msgOk = Messages.getMessage("DIALOGS_OPTIONS_OK");
        String msgCancel = Messages.getMessage("DIALOGS_OPTIONS_CANCEL");

        saveTab = OptionsSaveOptionsTab.getInstance();
        acTab = OptionsACTab.getInstance();
        vcTab = OptionsVCTab.getInstance();
        miscTab = OptionsGeneralTab.getInstance();

        okButton = new JButton(new ValidateOptionsAction(msgOk));
        cancelButton = new JButton(new CancelAction(msgCancel, this));

        okButton.setPreferredSize(new Dimension(20, 30));
        cancelButton.setPreferredSize(new Dimension(20, 30));

    }

    /**
     * 15 juin 2005
     */
    private void initLayout() {
        Box mainBox = new Box(BoxLayout.Y_AXIS);
        mainBox.add(jTabbedPane);
        repaint();

        mainBox.add(Box.createVerticalStrut(5));

        Box buttonsBox = new Box(BoxLayout.X_AXIS);
        buttonsBox.add(okButton);
        buttonsBox.add(cancelButton);

        mainBox.add(buttonsBox);

        myPanel.add(mainBox);
    }

    /**
     * 8 juil. 2005
     */
    private void addComponents() {
        myPanel = new JPanel();
        myPanel.setLayout(new GridLayout());
        setContentPane(myPanel);

        jTabbedPane = new JTabbedPane();
        jTabbedPane.setIgnoreRepaint(false);
        jTabbedPane.setMinimumSize(dim);
        jTabbedPane.setDoubleBuffered(true);
        String msgSave = Messages.getMessage("DIALOGS_OPTIONS_SAVE_TITLE");
        String msgMiscList = Messages.getMessage("DIALOGS_OPTIONS_GENERAL_TITLE");
        String msgAc = Messages.getMessage("DIALOGS_OPTIONS_AC_TITLE");
        String msgVc = Messages.getMessage("DIALOGS_OPTIONS_VC_TITLE");

        jTabbedPane.addTab(msgSave, saveTab);
        if (Mambo.hasACs()) {
            jTabbedPane.addTab(msgAc, acTab);
        }
        jTabbedPane.addTab(msgVc, vcTab);
        jTabbedPane.addTab(msgMiscList, miscTab);

        jTabbedPane.addChangeListener(new TabbedPaneListener());
    }
}
