// +======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/containers/MamboFrame.java,v $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class MamboFrame.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: chinkumo $
//
// $Revision: 1.2 $
//
// $Log: MamboFrame.java,v $
// Revision 1.2 2005/11/29 18:28:12 chinkumo
// no message
//
// Revision 1.1.2.2 2005/09/14 15:41:20 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.containers;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.components.MamboMenuBar;
import fr.soleil.mambo.lifecycle.LifeCycleManager;
import fr.soleil.mambo.tools.Messages;

public class MamboFrame extends JFrame {

    private static final long serialVersionUID = 8585305178851662217L;

    private static MamboFrame instance = null;

    /**
     * @param lifeCycleManager
     * @return 8 juil. 2005
     */
    public static MamboFrame getInstance(final LifeCycleManager lifeCycleManager) {
        if (instance == null) {
            instance = new MamboFrame(lifeCycleManager);
        }

        return instance;
    }

    /**
     * @return 8 juil. 2005
     */
    public static MamboFrame getInstance() {
        return instance;
    }

    /**
     * @param lifeCycleManager
     */
    private MamboFrame(final LifeCycleManager lifeCycleManager) {
        super();
        initParameters();

        addMamboComponents();
        initOpeningListener(lifeCycleManager);
    }

    public void close() {
        processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    /**
     * 22 juin 2005
     * 
     * @param lifeCycleManager
     */
    private void initOpeningListener(final LifeCycleManager lifeCycleManager) {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(final WindowEvent e) {
                lifeCycleManager.applicationStarted();
            }

            @Override
            public void windowClosing(final WindowEvent e) {
                lifeCycleManager.applicationClosed();
            }
        });

    }

    /**
     * 10 juin 2005
     */
    private void initParameters() {
        // setTitle ( "Mambo" );
        // String title = "Mambo";
        final String title = Messages.getAppMessage("project.name") + " v" + Messages.getAppMessage("project.version")
                + " (" + Messages.getAppMessage("build.date") + ")";

        AccountManager accountManager = Mambo.getAccountManager();
        final String name = accountManager == null ? null : accountManager.getSelectedAccountName();
        if (name != null) {
            setTitle(title + " - " + name);
        } else {
            setTitle(title);
        }
    }

    /**
     * 8 juil. 2005
     */
    private void addMamboComponents() {
        setJMenuBar(MamboMenuBar.getInstance());
        add(MamboMainPanel.getInstance());
        // getContentPane().add(MamboMainPanel.getInstance());

    }

}
