package fr.soleil.mambo.containers.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.view.VCViewSpectrumStackChartAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.data.view.DataBuilder;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.tools.Messages;

/**
 * @author awo
 */
public class ViewSpectrumStackPanel extends AbstractViewSpectrumPanel {

    private static final long serialVersionUID = 6678556891518537857L;

    private static final ImageIcon VIEW_ICON = new ImageIcon(Mambo.class.getResource("icons/View.gif"));

    private final JButton viewButton;
    private final JPanel curveChoicePanel;
    private final JCheckBox readCheckBox;
    private final JCheckBox writeCheckBox;
    private final JLabel noDataLabel;

    private final List<String> timeList;
    private final VCViewSpectrumStackChartAction stackChartAction;

    public ViewSpectrumStackPanel(ViewConfigurationBean viewConfigurationBean) throws ArchivingException {
        super(viewConfigurationBean);
        timeList = new ArrayList<String>();
        stackChartAction = new VCViewSpectrumStackChartAction(this, viewConfigurationBean);
        viewButton = new JButton();
        viewButton.setMargin(new Insets(0, 0, 0, 0));
        GUIUtilities.setObjectBackground(viewButton, GUIUtilities.VIEW_COLOR);
        viewButton.setIcon(VIEW_ICON);
        viewButton.addActionListener(stackChartAction);
        viewButton.setEnabled(true);
        viewButton.setText(Messages.getMessage("VIEW_ACTION_VIEW_BUTTON") + " " + getName());

        curveChoicePanel = new JPanel(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();

        readCheckBox = new JCheckBox(Messages.getMessage("VIEW_SPECTRUM_STACK_READ"));
        GUIUtilities.setObjectBackground(readCheckBox, GUIUtilities.VIEW_COLOR);
        readCheckBox.addActionListener(stackChartAction);

        writeCheckBox = new JCheckBox(Messages.getMessage("VIEW_SPECTRUM_STACK_WRITE"));
        writeCheckBox.addActionListener(stackChartAction);
        GUIUtilities.setObjectBackground(writeCheckBox, GUIUtilities.VIEW_COLOR);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        curveChoicePanel.add(readCheckBox, gbc);
        gbc.gridx = 1;
        curveChoicePanel.add(writeCheckBox, gbc);
        GUIUtilities.setObjectBackground(curveChoicePanel, GUIUtilities.VIEW_COLOR);

        readCheckBox.setSelected(false);
        readCheckBox.setEnabled(false);
        writeCheckBox.setSelected(false);
        writeCheckBox.setEnabled(false);

        noDataLabel = new JLabel(Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA"));
        noDataLabel.setVerticalAlignment(SwingConstants.NORTH);
        GUIUtilities.setObjectBackground(noDataLabel, GUIUtilities.VIEW_COLOR);
        noDataLabel.setForeground(Color.RED);
    }

    @Override
    protected void initCenterPanelLayout() {
        centerPanel.setLayout(new GridBagLayout());
    }

    @Override
    protected void prepareComponents() {
        super.prepareComponents();
        viewButton.setText(Messages.getMessage("VIEW_ACTION_VIEW_BUTTON") + " " + getName());
    }

    @Override
    protected void updateComponents() {
        // If we have both read and write values, we let the user choose
        // If only read or only write values exist, then we don't show radio
        // buttons, and the method returning the dao will display the non-null
        // dataview in chart
        if ((splitData == null) || (isNullOrEmpty(splitData[0]) && isNullOrEmpty(splitData[1]))) {
            readCheckBox.setSelected(false);
            readCheckBox.setEnabled(false);
            writeCheckBox.setSelected(false);
            writeCheckBox.setEnabled(false);

            GridBagConstraints noDataLabelConstraints = new GridBagConstraints();
            noDataLabelConstraints.fill = GridBagConstraints.BOTH;
            noDataLabelConstraints.gridx = 0;
            noDataLabelConstraints.gridy = 1;
            noDataLabelConstraints.weightx = 1;
            noDataLabelConstraints.weighty = 1;
            noDataLabelConstraints.anchor = GridBagConstraints.NORTH;
            noDataLabelConstraints.gridwidth = GridBagConstraints.REMAINDER;
            centerPanel.add(noDataLabel, noDataLabelConstraints);
        } else {
            boolean readOk = (!isNullOrEmpty(splitData[0]));
            readCheckBox.setSelected(readOk);
            readCheckBox.setEnabled(readOk);
            boolean writeOk = (!isNullOrEmpty(splitData[1]));
            writeCheckBox.setSelected(writeOk);
            writeCheckBox.setEnabled(writeOk);

            GridBagConstraints curveChoicePanelConstraints = new GridBagConstraints();
            curveChoicePanelConstraints.fill = GridBagConstraints.BOTH;
            curveChoicePanelConstraints.gridx = 0;
            curveChoicePanelConstraints.gridy = 1;
            curveChoicePanelConstraints.weightx = 1;
            curveChoicePanelConstraints.weighty = 1;
            curveChoicePanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
            centerPanel.add(curveChoicePanel, curveChoicePanelConstraints);

            GridBagConstraints viewButtonConstraints = new GridBagConstraints();
            viewButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
            viewButtonConstraints.gridx = 0;
            viewButtonConstraints.gridy = 2;
            viewButtonConstraints.weightx = 0.5;
            viewButtonConstraints.weighty = 0;
            centerPanel.add(viewButton, viewButtonConstraints);

            GridBagConstraints saveButtonConstraints = new GridBagConstraints();
            saveButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
            saveButtonConstraints.gridx = 1;
            saveButtonConstraints.gridy = 2;
            saveButtonConstraints.weightx = 0.5;
            saveButtonConstraints.weighty = 0;
            saveButtonConstraints.insets = new Insets(0, 20, 0, 0);
            centerPanel.add(saveButton, saveButtonConstraints);

            viewButton.setPreferredSize(saveButton.getPreferredSize());
        }
    }

    private DataBuilder extractDataArray(int timeIndex, DbData data) {
        DataBuilder dataArray = null;
        if ((data != null) && (data.getTimedData()) != null && (timeIndex < data.getTimedData().length)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(data.getTimedData()[timeIndex].getTime());
            dataArray = new DataBuilder(AbstractViewSpectrumPanel.DATE_FORMAT.format(calendar.getTime()));
            double[] value = new double[2 * data.getMaxX()];
            NullableTimedData timedData = data.getTimedData()[timeIndex];
            Object dataValue = timedData.getValue();
            if (dataValue == null) {
                for (int index = 0; index < data.getMaxX(); index++) {
                    value[2 * index] = index;
                    value[2 * index + 1] = MathConst.NAN_FOR_NULL;
                }
            } else if (dataValue instanceof byte[]) {
                fillDoubleArray(value, timedData, (byte[]) dataValue, null, data.getMaxX());
            } else if (dataValue instanceof short[]) {
                fillDoubleArray(value, timedData, (short[]) dataValue, null, data.getMaxX());
            } else if (dataValue instanceof int[]) {
                fillDoubleArray(value, timedData, (int[]) dataValue, null, data.getMaxX());
            } else if (dataValue instanceof long[]) {
                fillDoubleArray(value, timedData, (long[]) dataValue, null, data.getMaxX());
            } else if (dataValue instanceof float[]) {
                fillDoubleArray(value, timedData, (float[]) dataValue, null, data.getMaxX());
            } else if (dataValue instanceof double[]) {
                fillDoubleArray(value, timedData, (double[]) dataValue, null, data.getMaxX());
            } else if (dataValue instanceof Number[]) {
                fillDoubleArray(value, timedData, (Number[]) dataValue, null, data.getMaxX());
            } else {
                for (int index = 0; index < data.getMaxX(); index++) {
                    double y;
                    if (index < Array.getLength(dataValue)) {
                        Object temp = Array.get(dataValue, index);
                        if (temp == null) {
                            y = MathConst.NAN_FOR_NULL;
                        } else {
                            y = ((Number) temp).doubleValue();
                        }
                    } else {
                        y = MathConst.NAN_FOR_NULL;
                    }
                    value[2 * index] = index;
                    value[2 * index + 1] = y;
                }
            }
            dataArray.setData(value);
        }
        return dataArray;
    }

    protected void fillDoubleArray(double[] value, NullableTimedData timedData, byte[] byteValue, boolean[] nullValues,
            int maxX) {
        if (nullValues == null) {
            for (int index = 0; index < maxX; index++) {
                double y;
                if (index < byteValue.length) {
                    y = byteValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        } else {
            for (int index = 0; index < maxX; index++) {
                double y;
                if ((index < byteValue.length) && (!nullValues[index])) {
                    y = byteValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        }
    }

    protected void fillDoubleArray(double[] value, NullableTimedData timedData, short[] shortValue,
            boolean[] nullValues, int maxX) {
        if (nullValues == null) {
            for (int index = 0; index < maxX; index++) {
                double y;
                if (index < shortValue.length) {
                    y = shortValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        } else {
            for (int index = 0; index < maxX; index++) {
                double y;
                if ((index < shortValue.length) && (!nullValues[index])) {
                    y = shortValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        }
    }

    protected void fillDoubleArray(double[] value, NullableTimedData timedData, int[] intValue, boolean[] nullValues,
            int maxX) {
        if (nullValues == null) {
            for (int index = 0; index < maxX; index++) {
                double y;
                if (index < intValue.length) {
                    y = intValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        } else {
            for (int index = 0; index < maxX; index++) {
                double y;
                if ((index < intValue.length) && (!nullValues[index])) {
                    y = intValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        }
    }

    protected void fillDoubleArray(double[] value, NullableTimedData timedData, long[] longValue, boolean[] nullValues,
            int maxX) {
        if (nullValues == null) {
            for (int index = 0; index < maxX; index++) {
                double y;
                if (index < longValue.length) {
                    y = longValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        } else {
            for (int index = 0; index < maxX; index++) {
                double y;
                if ((index < longValue.length) && (!nullValues[index])) {
                    y = longValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        }
    }

    protected void fillDoubleArray(double[] value, NullableTimedData timedData, float[] floatValue,
            boolean[] nullValues, int maxX) {
        if (nullValues == null) {
            for (int index = 0; index < maxX; index++) {
                double y;
                if (index < floatValue.length) {
                    y = floatValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        } else {
            for (int index = 0; index < maxX; index++) {
                double y;
                if ((index < floatValue.length) && (!nullValues[index])) {
                    y = floatValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        }
    }

    protected void fillDoubleArray(double[] value, NullableTimedData timedData, double[] doubleValue,
            boolean[] nullValues, int maxX) {
        if (nullValues == null) {
            for (int index = 0; index < maxX; index++) {
                double y;
                if (index < doubleValue.length) {
                    y = doubleValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        } else {
            for (int index = 0; index < maxX; index++) {
                double y;
                if ((index < doubleValue.length) && (!nullValues[index])) {
                    y = doubleValue[index];
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        }
    }

    protected void fillDoubleArray(double[] value, NullableTimedData timedData, Number[] numberValue,
            boolean[] nullValues, int maxX) {
        if (nullValues == null) {
            for (int index = 0; index < maxX; index++) {
                double y;
                if (index < numberValue.length) {
                    y = (numberValue[index] == null ? MathConst.NAN_FOR_NULL : numberValue[index].doubleValue());
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        } else {
            for (int index = 0; index < maxX; index++) {
                double y;
                if ((index < numberValue.length) && (!nullValues[index])) {
                    y = (numberValue[index] == null ? MathConst.NAN_FOR_NULL : numberValue[index].doubleValue());
                } else {
                    y = MathConst.NAN_FOR_NULL;
                }
                value[2 * index] = index;
                value[2 * index + 1] = y;
            }
        }
    }

    public DbData[] getSplitedData() {
        return splitData;
    }

    public int getCheckBoxSelected() {
        int selected;
        if (readCheckBox.isSelected() && writeCheckBox.isSelected()) {
            // READ and WRITE selected
            selected = 3;
        } else if (readCheckBox.isSelected()) {
            // Only READ selected
            selected = 1;
        } else if (writeCheckBox.isSelected()) {
            // Only Write selected
            selected = 2;
        } else {
            // No data selected
            selected = 0;
        }
        return selected;
    }

    public List<String> getTimeList() {
        timeList.clear();
        if ((splitData != null) && (readCheckBox.isSelected() || !writeCheckBox.isSelected())) {
            int readSize = 0;
            int writeSize = 0;
            if (!isNullOrEmpty(splitData[0])) {
                readSize = splitData[0].getTimedData().length;
            }
            if (!isNullOrEmpty(splitData[1])) {
                writeSize = splitData[1].getTimedData().length;
            }
            if (readCheckBox.isSelected()) {

                if (writeCheckBox.isSelected()) {
                    // both checkboxes are selected
                    int size = Math.max(readSize, writeSize);
                    for (int i = 0; i < size; i++) {
                        DataBuilder readDataArray = extractDataArray(i, splitData[0]);
                        boolean filled = false;
                        if (readDataArray != null) {
                            timeList.add(readDataArray.getId());
                        }
                        readDataArray = null;
                        DataBuilder writeDataArray = extractDataArray(i, splitData[1]);
                        if (writeDataArray != null) {
                            if (!filled) {
                                timeList.add(writeDataArray.getId());
                            }
                        }
                    }
                } else {
                    // only readCheckBox is selected
                    for (int i = 0; i < readSize; i++) {
                        DataBuilder readDataArray = extractDataArray(i, splitData[0]);
                        if (readDataArray != null) {
                            if (isCleaning()) {
                                return null;
                            }
                            timeList.add(readDataArray.getId());
                        }
                        readDataArray = null;
                    }
                }
            } else {
                // only writeCheckBox is selected
                for (int i = 0; i < writeSize; i++) {
                    DataBuilder writeDataArray = extractDataArray(i, splitData[1]);
                    if (writeDataArray != null) {

                        timeList.add(writeDataArray.getId());
                    }
                }
            }
        }
        return timeList;
    }

    @Override
    protected void performDeepClean() {
        spectrum = null;
        cleanSplitData();
        timeList.clear();
    }

    @Override
    public void lightClean() {
        cleanSplitData();
        timeList.clear();
        setLoading();
        centerPanel.removeAll();
    }

    public boolean getWriteCheckBox() {
        return writeCheckBox.isSelected();
    }

    public boolean getReadCheckBox() {
        return readCheckBox.isSelected();
    }

    public void enableViewButton(boolean enable, boolean updateText) {
        if (viewButton != null) {
            viewButton.setEnabled(enable);
            if (updateText) {
                if (enable) {
                    viewButton.setText(Messages.getMessage("VIEW_ACTION_VIEW_BUTTON") + " " + getName());
                } else {
                    viewButton.setText(Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA") + " " + getName());
                }
            }
        }
    }

    @Override
    public DbData getReadData() {
        DbData result;
        if (readCheckBox.isSelected()) {
            result = super.getReadData();
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public DbData getWriteData() {
        DbData result;
        if (writeCheckBox.isSelected()) {
            result = super.getWriteData();
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public int getViewSpectrumType() {
        return ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME_STACK;
    }
}
