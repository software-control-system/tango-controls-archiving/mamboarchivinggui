// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/containers/view/ViewAttributesPanel.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class ViewAttributesPanel.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: ViewAttributesPanel.java,v $
// Revision 1.6 2008/04/09 18:00:00 achouri
// add popup menu right click
//
// Revision 1.5 2008/04/09 10:45:46 achouri
// add ViewAttributesGraphePanel
//
// Revision 1.4 2006/10/12 13:22:53 ounsy
// ViewAttributesDetailPanel removed (useless)
//
// Revision 1.3 2006/05/19 15:05:29 ounsy
// minor changes
//
// Revision 1.2 2005/11/29 18:27:07 chinkumo
// no message
//
// Revision 1.1.2.2 2005/09/14 15:41:20 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.containers.view;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.swing.ui.ColoredSplitPaneUI;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;

public class ViewAttributesPanel extends JPanel {

    private static final long serialVersionUID = -5087023109638462496L;

    private JSplitPane splitPane;
    private ViewAttributesTreePanel viewAttributesTreePanel;
    private ViewAttributesGraphPanel viewAttributesGraphPanel;

    private ViewConfigurationBean viewConfigurationBean;

    public ViewAttributesPanel(ViewConfigurationBean viewConfigurationBean) {
        super();
        this.viewConfigurationBean = viewConfigurationBean;
        initComponents();
        addComponents();
    }

    /**
     * 19 juil. 2005
     */
    private void addComponents() {
        setLayout(new GridLayout(1, 1));
        add(splitPane);
    }

    private void initSplitPane() {
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setBackground(GUIUtilities.getViewColor());
        splitPane.setLeftComponent(viewAttributesTreePanel);
        splitPane.setRightComponent(viewAttributesGraphPanel);
        splitPane.setDividerSize(8);
        splitPane.setDividerLocation(0.3);
        splitPane.setResizeWeight(0);
        splitPane.setOneTouchExpandable(true);
        ColoredSplitPaneUI ui = new ColoredSplitPaneUI();
        ui.setDividerColor(GUIUtilities.getViewColor());
        splitPane.setUI(ui);
    }

    private void clean() {
        removeAll();
        if (splitPane != null) {
            splitPane.removeAll();
            splitPane = null;
        }
    }

    /**
     * 19 juil. 2005
     */
    private void initComponents() {
        viewAttributesGraphPanel = new ViewAttributesGraphPanel(viewConfigurationBean);
        viewAttributesTreePanel = new ViewAttributesTreePanel(viewConfigurationBean, this);
        Dimension minimumSize = new Dimension(5, 5);
        viewAttributesTreePanel.setMinimumSize(minimumSize);
        viewAttributesGraphPanel.setMinimumSize(minimumSize);

        GUIUtilities.setObjectBackground(viewAttributesGraphPanel, GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);
        initSplitPane();
    }

    public void showTreePanel() {
        boolean doAdd = true;
        if (getComponentCount() > 0 && (getComponent(0) == splitPane)) {
            doAdd = false;
        }
        if (doAdd) {
            clean();
            initSplitPane();
            add(splitPane);
            revalidate();
            repaint();
        }
    }

    public void hideTreePanel() {
        clean();
        add(viewAttributesGraphPanel);
        revalidate();
        repaint();
    }

    public ViewAttributesTreePanel getViewAttributesTreePanel() {
        return viewAttributesTreePanel;
    }

    public ViewAttributesGraphPanel getViewAttributesGraphPanel() {
        return viewAttributesGraphPanel;
    }

}
