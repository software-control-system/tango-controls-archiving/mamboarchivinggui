package fr.soleil.mambo.containers.view.dialogs;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JScrollPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.data.view.MamboViewAdapterFactory;
import fr.soleil.mambo.data.view.ViewAdapterKey;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.tools.Messages;

public class VCVariationViewDialog extends JDialog {

    private static final long serialVersionUID = 9049420726796049787L;

    private static final Logger LOGGER = LoggerFactory.getLogger(VCVariationViewDialog.class);

    private static final Dimension DIM = new Dimension(600, 600);
    private Chart chart;
    private String id;

    public VCVariationViewDialog(final ViewConfiguration conf) {
        super(MamboFrame.getInstance(), Messages.getMessage("DIALOGS_VARIATION_VIEW"), true);
        initComponents(conf);
        addComponents();
        int mainWidth = MamboFrame.getInstance().getWidth();
        int width = getWidth();
        setLocation(mainWidth - width - 10, 0);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (chart != null) {
                    conf.cleanChart(chart);
                    MamboViewAdapterFactory daoFactory = MamboViewAdapterFactory.INSTANCE;
                    if ((daoFactory != null) && (id != null)) {
                        daoFactory.removeKeyForDaoScalar(id);
                    }
                }
                chart = null;
                id = null;
            }
        });
        repaint();
    }

    private void initComponents(ViewConfiguration vc) {
        id = vc.getCurrentId();
        chart = new Chart();
        vc.configureChart(chart);
        try {
            final MamboViewAdapterFactory daoFactory = MamboViewAdapterFactory.INSTANCE;
            ViewAdapterKey viewDAOKey = new ViewAdapterKey(ViewAdapterKey.TYPE_NUMBER_SCALAR, id);
            daoFactory.setKeyForChart(chart, viewDAOKey);
            daoFactory.createNumberDataArrayDAO(chart);

            vc.loadAttributes(chart);
        } catch (Exception e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
        }
    }

    /**
     * 8 juil. 2005
     */
    private void addComponents() {
        chart.setSize(DIM.width, DIM.height);
        JScrollPane scrollPane = new JScrollPane(chart);
        getContentPane().add(scrollPane);
        getContentPane().setLayout(new GridLayout());
        Dimension dim2 = new Dimension(DIM.width + 50, DIM.height + 50);
        setSize(dim2);
    }

}
