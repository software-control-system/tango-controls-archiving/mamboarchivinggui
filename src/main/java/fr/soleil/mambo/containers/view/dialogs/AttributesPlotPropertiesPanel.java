// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/containers/view/dialogs/AttributesPlotPropertiesPanel.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class AttributesPlotPropertiesPanel.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.13 $
//
// $Log: AttributesPlotPropertiesPanel.java,v $
// Revision 1.13 2007/08/24 09:23:00 ounsy
// Color index reset on new VC (Mantis bug 5210 part1)
//
// Revision 1.12 2007/03/20 14:15:09 ounsy
// added the possibility to hide a dataview
//
// Revision 1.11 2007/02/27 10:06:32 ounsy
// corrected the transformation property bug
//
// Revision 1.10 2007/01/11 14:05:47 ounsy
// Math Expressions Management (warning ! requires atk 2.7.0 or greater)
//
// Revision 1.9 2006/10/19 09:21:21 ounsy
// now the color rotation index of vc attributes is saved
//
// Revision 1.8 2006/10/17 14:31:06 ounsy
// extended color management
//
// Revision 1.7 2006/10/02 15:00:10 ounsy
// JLChart has now a white background : dataviews can not have white color
//
// Revision 1.6 2006/10/02 14:13:25 ounsy
// minor changes (look and feel)
//
// Revision 1.5 2006/09/20 13:03:29 chinkumo
// Mantis 2230: Separated Set addition in the "Attributes plot properties" tab
// of the New/Modify VC View
//
// Revision 1.4 2006/05/19 15:05:29 ounsy
// minor changes
//
// Revision 1.3 2005/12/15 11:31:15 ounsy
// minor changes
//
// Revision 1.2 2005/11/29 18:27:45 chinkumo
// no message
//
// Revision 1.1.2.3 2005/09/15 10:30:05 chinkumo
// Third commit !
//
// Revision 1.1.2.2 2005/09/14 15:41:20 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.containers.view.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.chart.DataViewOption;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.actions.view.dialogs.listeners.FactorFieldListener;
import fr.soleil.mambo.actions.view.dialogs.listeners.PlotPropertyComboListener;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.data.view.ViewConfigurationAttributeProperties;
import fr.soleil.mambo.tools.ColorGenerator;
import fr.soleil.mambo.tools.Messages;

public class AttributesPlotPropertiesPanel extends DataViewOption {

    private static final long serialVersionUID = -5066465249572105539L;

    private final static int totalColors = 31;
    private final static Color[] defaultColor = ColorGenerator.generateColors(totalColors, 0xFFFFFF);
    private int colorIndex;

    // balancing factor management
    private JTextField factorField;
    private FactorFieldListener factorFieldListener;
    // private JLabel factorLabel;

    // general objects
    private JPanel centerPanel;
    private JPanel topPanel;
    private JPanel generalPanel;
    private DefaultComboBoxModel<String> axisChoiceComboModel;

    // private JComboBox axisChoiceCombo;
    private PlotPropertyComboListener plotPropertyChoiceComboListener;
    private DefaultComboBoxModel<String> spectrumViewTypeComboModel;
    private JComboBox<String> spectrumViewTypeCombo;
    private JCheckBox hiddenCheckBox;

    public AttributesPlotPropertiesPanel(ViewConfigurationBean viewConfigurationBean, VCEditDialog editDialog) {
        super();
        plotPropertyChoiceComboListener.setViewConfigurationBean(viewConfigurationBean);
        plotPropertyChoiceComboListener.setEditDialog(editDialog);
        factorFieldListener.setViewConfigurationBean(viewConfigurationBean);
        factorFieldListener.setEditDialog(editDialog);
        this.addComponents();
        reduceHeight();
        colorIndex = 0;
        ViewConfigurationAttributeProperties currentProperties = new ViewConfigurationAttributeProperties();
        ViewConfigurationAttributeProperties.setCurrentProperties(currentProperties);
    }

    private void reduceHeight() {
        transformHelpLabel.setLineWrap(false);

        lineColorView.setPreferredSize(GUIUtilities.getViewLabelSize());
        lineColorView.setMinimumSize(GUIUtilities.getViewLabelSize());
        fillColorView.setPreferredSize(GUIUtilities.getViewLabelSize());
        fillColorView.setMinimumSize(GUIUtilities.getViewLabelSize());
        markerColorView.setPreferredSize(GUIUtilities.getViewLabelSize());
        markerColorView.setMinimumSize(GUIUtilities.getViewLabelSize());

        lineColorBtn.setMargin(CometeUtils.getzInset());
        setAllButton.setMargin(CometeUtils.getzInset());
        fillColorBtn.setMargin(CometeUtils.getzInset());
        markerColorBtn.setMargin(CometeUtils.getzInset());

        lineWidthSpinner.setPreferredSize(null);
        barWidthSpinner.setPreferredSize(null);
        markerSizeSpinner.setPreferredSize(null);

        transformA0Text.setMargin(CometeUtils.getzInset());
        transformA1Text.setMargin(CometeUtils.getzInset());
        transformA2Text.setMargin(CometeUtils.getzInset());
    }

    private void addComponents() {
        this.removeAll();
        this.setLayout(new BorderLayout());
        this.add(topPanel, BorderLayout.NORTH);
        this.add(centerPanel, BorderLayout.CENTER);
    }

    @Override
    protected void initComponents() {
        // This one became the upper pannel and the centerpanel became the lower
        // pannel
        super.initComponents();
        initTopPanel();
        initTabPanels();
        initCenterPanel();
    }

    private void initTopPanel() {
        topPanel = new JPanel(new GridBagLayout());

        plotPropertyChoiceComboListener = new PlotPropertyComboListener();

        JPanel axisPanel = initAxisChoicePanel();
        GridBagConstraints axisConstraints = new GridBagConstraints();
        axisConstraints.fill = GridBagConstraints.BOTH;
        axisConstraints.gridx = 0;
        axisConstraints.gridy = 0;
        axisConstraints.weightx = 0.5;
        axisConstraints.weighty = 0;
        axisConstraints.anchor = GridBagConstraints.NORTH;
        topPanel.add(axisPanel, axisConstraints);

        JPanel balFactorPanel = initFactorPanel();
        GridBagConstraints balFactorConstraints = new GridBagConstraints();
        balFactorConstraints.fill = GridBagConstraints.BOTH;
        balFactorConstraints.gridx = 1;
        balFactorConstraints.gridy = 0;
        balFactorConstraints.weightx = 0.5;
        balFactorConstraints.weighty = 0;
        balFactorConstraints.anchor = GridBagConstraints.NORTH;
        topPanel.add(balFactorPanel, balFactorConstraints);

        JPanel spectrumViewTypePanel = initSpectrumViewTypePanel();
        GridBagConstraints spectrumViewTypeConstraints = new GridBagConstraints();
        spectrumViewTypeConstraints.fill = GridBagConstraints.BOTH;
        spectrumViewTypeConstraints.gridx = 0;
        spectrumViewTypeConstraints.gridy = 1;
        spectrumViewTypeConstraints.weightx = 1;
        spectrumViewTypeConstraints.weighty = 0;
        spectrumViewTypeConstraints.gridwidth = GridBagConstraints.REMAINDER;
        spectrumViewTypeConstraints.anchor = GridBagConstraints.SOUTH;
        topPanel.add(spectrumViewTypePanel, spectrumViewTypeConstraints);
    }

    private JPanel initSpectrumViewTypePanel() {
        JPanel viewTypePanel = new JPanel();
        String msg = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_CHART_VIEW_TYPE");
        TitledBorder viewTypePanelBorder = GUIUtilities.getPlotSubPanelsLineBorder(msg, new Color(100, 100, 100));
        CompoundBorder cb = BorderFactory.createCompoundBorder(viewTypePanelBorder,
                BorderFactory.createEmptyBorder(2, 4, 4, 4));
        viewTypePanel.setBorder(cb);
        String[] msgListItem = new String[4];
        msgListItem[0] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_CHART_VIEW_TYPE_INDICE");
        msgListItem[1] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_CHART_VIEW_TYPE_TIME");
        msgListItem[2] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_CHART_VIEW_TYPE_TIME_STACK");
        msgListItem[3] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_CHART_VIEW_TYPE_TIME_IMAGE");
        spectrumViewTypeComboModel = new DefaultComboBoxModel<>(msgListItem);
        spectrumViewTypeCombo = new JComboBox<>(spectrumViewTypeComboModel);
        spectrumViewTypeCombo.addActionListener(plotPropertyChoiceComboListener);
        viewTypePanel.setLayout(new GridBagLayout());
        GridBagConstraints spectrumViewTypeComboConstraints = new GridBagConstraints();
        spectrumViewTypeComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        spectrumViewTypeComboConstraints.gridx = 0;
        spectrumViewTypeComboConstraints.gridy = 0;
        spectrumViewTypeComboConstraints.weightx = 1;
        spectrumViewTypeComboConstraints.weighty = 0;
        viewTypePanel.add(spectrumViewTypeCombo, spectrumViewTypeComboConstraints);
        setSpectrumViewTypeChoice(spectrumViewTypeCombo.getSelectedIndex());
        return viewTypePanel;
    }

    private JPanel initAxisChoicePanel() {
        JPanel axisChoicePanel = new JPanel();
        String msg = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PLOT_PROPERTIES_AXIS_CHOICE");
        TitledBorder axisPanelBorder = GUIUtilities.getPlotSubPanelsLineBorder(msg, new Color(0, 150, 0));
        CompoundBorder cb = BorderFactory.createCompoundBorder(axisPanelBorder,
                BorderFactory.createEmptyBorder(2, 4, 4, 4));
        axisChoicePanel.setBorder(cb);

        String[] msgListItem = new String[3];
        msgListItem[0] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PLOT_PROPERTIES_AXIS_CHOICE_Y1");
        msgListItem[1] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PLOT_PROPERTIES_AXIS_CHOICE_Y2");
        msgListItem[2] = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PLOT_PROPERTIES_AXIS_CHOICE_X");
        axisChoiceComboModel = new DefaultComboBoxModel<>(msgListItem);
        axisBox = new JComboBox<>(axisChoiceComboModel);
        axisBox.setFont(GUIUtilities.labelFont);
        axisBox.addActionListener(plotPropertyChoiceComboListener);
        axisChoicePanel.setLayout(new GridBagLayout());
        GridBagConstraints axisBoxConstraints = new GridBagConstraints();
        axisBoxConstraints.fill = GridBagConstraints.HORIZONTAL;
        axisBoxConstraints.gridx = 0;
        axisBoxConstraints.gridy = 0;
        axisBoxConstraints.weightx = 1;
        axisBoxConstraints.weighty = 0;
        axisChoicePanel.add(axisBox, axisBoxConstraints);
        return axisChoicePanel;
    }

    private void initCenterPanel() {
        centerPanel = new JPanel();

        String msg = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_GRAPHICAL_TITLE");
        TitledBorder centerPanelBorder = GUIUtilities.getPlotSubPanelsLineBorder(msg, Color.BLUE);
        CompoundBorder cb = BorderFactory.createCompoundBorder(centerPanelBorder,
                BorderFactory.createEmptyBorder(0, 4, 0, 4));
        centerPanel.setBorder(cb);
        msg = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_HIDDEN");
        hiddenCheckBox = new JCheckBox(msg);
        hiddenCheckBox.setSelected(false);

        this.getCloseButton().setVisible(false);
        centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
        centerPanel.add(hiddenCheckBox);
        centerPanel.add(this.tabPane);
        hiddenCheckBox.setAlignmentX(LEFT_ALIGNMENT);
        this.tabPane.setAlignmentX(LEFT_ALIGNMENT);
    }

    protected void initTabPanels() {
        // initTabPane();
        initGeneralPanel();
        this.tabPane.remove(curvePanel);
        this.tabPane.remove(barPanel);
        this.tabPane.remove(markerPanel);
        this.tabPane.remove(transformPanel);
        this.tabPane.add(generalPanel, 0);
        this.tabPane.setTitleAt(0, "General");
        this.tabPane.setSelectedIndex(0);
    }

    private void initGeneralPanel() {
        generalPanel = new JPanel();
        generalPanel.setLayout(new GridBagLayout());

        String msg;
        msg = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PLOT_PROPERTIES_CURVE_TITLE");
        TitledBorder curvePanelBorder = GUIUtilities.getPlotSubPanelsEtchedBorder(msg);
        lineNameLabel.setVisible(false);
        lineNameText.setVisible(false);
        curvePanel.setBorder(curvePanelBorder);
        TitledBorder barPanelBorder = GUIUtilities.getPlotSubPanelsEtchedBorder("Bar");
        barPanel.setBorder(barPanelBorder);
        TitledBorder markerPanelBorder = GUIUtilities.getPlotSubPanelsEtchedBorder("Marker");
        markerPanel.setBorder(markerPanelBorder);
        TitledBorder transformPanelBorder = GUIUtilities.getPlotSubPanelsEtchedBorder("Transform");
        transformPanel.setBorder(transformPanelBorder);

        GridBagConstraints linePanelConstraints = new GridBagConstraints();
        linePanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        linePanelConstraints.gridx = 0;
        linePanelConstraints.gridy = 0;
        linePanelConstraints.weightx = 1;
        linePanelConstraints.weighty = 1;
        generalPanel.add(curvePanel, linePanelConstraints);

        GridBagConstraints barPanelConstraints = new GridBagConstraints();
        barPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        barPanelConstraints.gridx = 0;
        barPanelConstraints.gridy = 1;
        barPanelConstraints.weightx = 1;
        barPanelConstraints.weighty = 1;
        generalPanel.add(barPanel, barPanelConstraints);

        GridBagConstraints markerPanelConstraints = new GridBagConstraints();
        markerPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        markerPanelConstraints.gridx = 0;
        markerPanelConstraints.gridy = 2;
        markerPanelConstraints.weightx = 1;
        markerPanelConstraints.weighty = 1;
        generalPanel.add(markerPanel, markerPanelConstraints);

        GridBagConstraints transformPanelConstraints = new GridBagConstraints();
        transformPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        transformPanelConstraints.gridx = 0;
        transformPanelConstraints.gridy = 3;
        transformPanelConstraints.weightx = 1;
        transformPanelConstraints.weighty = 1;
        generalPanel.add(transformPanel, transformPanelConstraints);
    }

    private JPanel initFactorPanel() {
        JPanel factorPanel = new JPanel();
        String msg = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_PROPERTIES_FACTOR");
        TitledBorder factorPanelBorder = GUIUtilities.getPlotSubPanelsLineBorder(msg, Color.RED);
        CompoundBorder cb = BorderFactory.createCompoundBorder(factorPanelBorder,
                BorderFactory.createEmptyBorder(2, 4, 4, 4));
        factorPanel.setBorder(cb);
        factorField = new JTextField("1.0");
        factorFieldListener = new FactorFieldListener();
        factorField.addActionListener(factorFieldListener);
        factorField.setToolTipText("Press Enter");
        factorPanel.setLayout(new GridBagLayout());
        GridBagConstraints factorFieldConstraints = new GridBagConstraints();
        factorFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        factorFieldConstraints.gridx = 0;
        factorFieldConstraints.gridy = 0;
        factorFieldConstraints.weightx = 1;
        factorFieldConstraints.weighty = 0;
        factorPanel.add(factorField, factorFieldConstraints);
        return factorPanel;
    }

    public void setSpectrumViewTypeChoice(int spectrumViewTypeChoice) {
        spectrumViewTypeCombo.removeActionListener(plotPropertyChoiceComboListener);
        spectrumViewTypeCombo.setSelectedIndex(spectrumViewTypeChoice);
        spectrumViewTypeCombo.addActionListener(plotPropertyChoiceComboListener);
    }

    public static int getColorIndexForSize(int size) {
        return size * 5;
    }

    public void setColorIndex(int index) {
        colorIndex = index % defaultColor.length;
    }

    /**
     * Method to set the curve color to a color selected in default colors.
     * 
     * @return the next available index in the color table (for dataview color
     *         rotation)
     */
    public int rotateCurveColor() {
        applyColorIndex();
        colorIndex = getDefaultNextColorIndex(colorIndex) % defaultColor.length;
        return colorIndex;
    }

    public void applyColorIndex() {
        PlotProperties plotProperties = super.getPlotProperties();
        plotProperties.getCurve().setColor(ColorTool.getCometeColor(defaultColor[colorIndex]));
        plotProperties.getBar().setFillColor(ColorTool.getCometeColor(defaultColor[colorIndex]));
        plotProperties.getMarker().setColor(ColorTool.getCometeColor(defaultColor[colorIndex]));
        setPlotProperties(plotProperties);
        applyProperties();
    }

    public static Color getColorFor(int index) {
        return defaultColor[index % defaultColor.length];
    }

    public static int getDefaultNextColorIndex(int index) {
        return (index + 5) % defaultColor.length;
    }

    @Override
    public int getSelectedAxis() {
        if ("---".equals(axisBox.getItemAt(0))) {
            return axisBox.getSelectedIndex() - 1;
        }
        return axisBox.getSelectedIndex();
    }

    public int getSpectrumViewTypeChoice() {
        if ("---".equals(spectrumViewTypeCombo.getItemAt(0))) {
            return spectrumViewTypeCombo.getSelectedIndex() - 1;
        }
        return spectrumViewTypeCombo.getSelectedIndex();
    }

    public String getAxisChoiceItem() {
        return (String) axisBox.getSelectedItem();
    }

    public double getFactor() {
        double factor;
        try {
            factor = Double.parseDouble(factorField.getText());
            if (Double.isNaN(factor) || Double.isInfinite(factor)) {
                factor = 1;
            }
        } catch (Exception e) {
            factor = 1;
        }
        return factor;
    }

    public void setFactor(double factor) {
        factorField.setText(factor + ObjectUtils.EMPTY_STRING);
    }

    public JComboBox<String> getSpectrumViewTypeCombo() {
        return spectrumViewTypeCombo;
    }

    public DefaultComboBoxModel<String> getSpectrumViewTypeComboModel() {
        return spectrumViewTypeComboModel;
    }

    public DefaultComboBoxModel<String> getAxisChoiceComboModel() {
        return axisChoiceComboModel;
    }

    public PlotPropertyComboListener getPlotPropertyChoiceComboListener() {
        return plotPropertyChoiceComboListener;
    }

    public JTextField getFactorField() {
        return factorField;
    }

    @Override
    public PlotProperties getPlotProperties() {
        return super.getPlotProperties();
    }

    public boolean isHidden() {
        return hiddenCheckBox.isSelected();
    }

    public JComboBox<String> getAxisChoiceCombo() {
        return axisBox;
    }

    public void setViewConfigurationAttributePlotProperties(ViewConfigurationAttributePlotProperties plotProperties) {
        super.setPlotProperties(plotProperties);
        hiddenCheckBox.setSelected(plotProperties.isHidden());
        spectrumViewTypeCombo.setSelectedIndex(plotProperties.getSpectrumViewType());
        axisBox.setSelectedIndex(plotProperties.getAxisChoice());
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.barWidthSpinner.setEnabled(enabled);
        this.biasText.setEnabled(enabled);
        this.cosineBtn.setEnabled(enabled);
        this.cubicBtn.setEnabled(enabled);
        this.derivativeBtn.setEnabled(enabled);
        this.fftModBtn.setEnabled(enabled);
        this.fftPhaseBtn.setEnabled(enabled);
        this.fillColorBtn.setEnabled(enabled);
        this.fillMethodCombo.setEnabled(enabled);
        this.fillStyleCombo.setEnabled(enabled);
        this.flatExtBtn.setEnabled(enabled);
        this.flatSmoothBtn.setEnabled(enabled);
        this.gaussianSmoothBtn.setEnabled(enabled);
        this.hermiteBtn.setEnabled(enabled);
        this.integralBtn.setEnabled(enabled);
        this.legendVisibleCheck.setEnabled(enabled);
        this.linearBtn.setEnabled(enabled);
        this.linearExtBtn.setEnabled(enabled);
        this.lineColorBtn.setEnabled(enabled);
        this.lineStyleCombo.setEnabled(enabled);
        this.lineNameText.setEnabled(enabled);
        this.lineWidthSpinner.setEnabled(enabled);
        this.markerColorBtn.setEnabled(enabled);
        this.markerSizeSpinner.setEnabled(enabled);
        this.markerStyleCombo.setEnabled(enabled);
        this.neighborSpinner.setEnabled(enabled);
        this.noExtBtn.setEnabled(enabled);
        this.noInterpBtn.setEnabled(enabled);
        this.noMathBtn.setEnabled(enabled);
        this.noSmoothBtn.setEnabled(enabled);
        this.sigmaText.setEnabled(enabled);
        this.stepSpinner.setEnabled(enabled);
        this.tensionText.setEnabled(enabled);
        this.transformA0Text.setEnabled(enabled);
        this.transformA1Text.setEnabled(enabled);
        this.transformA2Text.setEnabled(enabled);
        this.transformHelpLabel.setEnabled(enabled);
        this.triangularSmoothBtn.setEnabled(enabled);
        this.viewTypeCombo.setEnabled(enabled);
        this.setAllButton.setEnabled(enabled);
        hiddenCheckBox.setEnabled(enabled);
    }
}
