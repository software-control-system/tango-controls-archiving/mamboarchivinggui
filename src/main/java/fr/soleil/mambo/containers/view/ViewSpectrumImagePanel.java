package fr.soleil.mambo.containers.view;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JRadioButton;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.view.ViewImageAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.tools.Messages;

public class ViewSpectrumImagePanel extends AbstractViewSpectrumPanel {

    private static final long serialVersionUID = 8867188081947762462L;

    private static final ImageIcon VIEW_ICON = new ImageIcon(Mambo.class.getResource("icons/View.gif"));

    private Box radioBox;
    private Box buttonBox;
    private JButton viewButton;
    private ButtonGroup buttonGroup;
    private JRadioButton readValuesRButton, writeValuesRButton;
    private ViewImageAction viewSpectrumImageAction;

    private double[][] value;

    public ViewSpectrumImagePanel(ViewConfigurationBean viewConfigurationBean) throws ArchivingException {
        super(viewConfigurationBean);

        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);
        initComponents();

        initBorder();

        lightClean();
    }

    protected void initComponents() {
        viewSpectrumImageAction = new ViewImageAction(Messages.getMessage("VIEW_IMAGE_VIEW"));

        buttonGroup = new ButtonGroup();
        readValuesRButton = new JRadioButton("Read Values");
        readValuesRButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                updateSpectrumReadValues();
            }
        });
        buttonGroup.add(readValuesRButton);
        GUIUtilities.setObjectBackground(readValuesRButton, GUIUtilities.VIEW_COLOR);

        writeValuesRButton = new JRadioButton("Write Values");
        writeValuesRButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                updateSpectrumWriteValues();
            }
        });
        buttonGroup.add(writeValuesRButton);
        GUIUtilities.setObjectBackground(writeValuesRButton, GUIUtilities.VIEW_COLOR);

        viewButton = new JButton(viewSpectrumImageAction);
        viewButton.setMargin(new Insets(0, 0, 0, 0));
        GUIUtilities.setObjectBackground(viewButton, GUIUtilities.VIEW_COLOR);
        viewButton.setIcon(VIEW_ICON);

        viewButton.setEnabled(true);
        viewButton.setText(Messages.getMessage("VIEW_ACTION_VIEW_BUTTON") + " " + getName());

        radioBox = new Box(BoxLayout.X_AXIS);
        radioBox.add(Box.createHorizontalGlue());
        radioBox.add(readValuesRButton);
        radioBox.add(Box.createHorizontalGlue());
        radioBox.add(Box.createHorizontalStrut(10));
        radioBox.add(writeValuesRButton);
        radioBox.add(Box.createHorizontalGlue());

        buttonBox = new Box(BoxLayout.X_AXIS);
        buttonBox.add(Box.createHorizontalGlue());
        buttonBox.add(viewButton);
        buttonBox.add(Box.createHorizontalGlue());

        centerPanel.add(Box.createVerticalGlue());
        centerPanel.add(radioBox);
        centerPanel.add(Box.createVerticalGlue());
        centerPanel.add(buttonBox);
    }

    private double[] extractDataArray(int timeIndex, DbData data) {
        double[] rowData = null;
        if (data != null && data.getTimedData() != null && timeIndex < data.getTimedData().length) {
            rowData = new double[data.getMaxX()];
            Object value = data.getTimedData()[timeIndex].getValue();
            if (value instanceof double[]) {
                double[] dval = (double[]) value;
                int length = Math.min(rowData.length, dval.length);
                System.arraycopy(dval, 0, rowData, 0, length);
                for (int index = length; index < rowData.length; index++) {
                    rowData[index] = MathConst.NAN_FOR_NULL;
                }
            } else if (value instanceof byte[]) {
                byte[] bval = (byte[]) value;
                int length = Math.min(rowData.length, bval.length);
                for (int index = 0; index < length; index++) {
                    rowData[index] = bval[index];
                }
                for (int index = length; index < rowData.length; index++) {
                    rowData[index] = MathConst.NAN_FOR_NULL;
                }
            } else if (value instanceof short[]) {
                short[] bval = (short[]) value;
                int length = Math.min(rowData.length, bval.length);
                for (int index = 0; index < length; index++) {
                    rowData[index] = bval[index];
                }
                for (int index = length; index < rowData.length; index++) {
                    rowData[index] = MathConst.NAN_FOR_NULL;
                }
            } else if (value instanceof int[]) {
                int[] bval = (int[]) value;
                int length = Math.min(rowData.length, bval.length);
                for (int index = 0; index < length; index++) {
                    rowData[index] = bval[index];
                }
                for (int index = length; index < rowData.length; index++) {
                    rowData[index] = MathConst.NAN_FOR_NULL;
                }
            } else if (value instanceof long[]) {
                long[] bval = (long[]) value;
                int length = Math.min(rowData.length, bval.length);
                for (int index = 0; index < length; index++) {
                    rowData[index] = bval[index];
                }
                for (int index = length; index < rowData.length; index++) {
                    rowData[index] = MathConst.NAN_FOR_NULL;
                }
            } else if (value instanceof float[]) {
                float[] bval = (float[]) value;
                int length = Math.min(rowData.length, bval.length);
                for (int index = 0; index < length; index++) {
                    rowData[index] = bval[index];
                }
                for (int index = length; index < rowData.length; index++) {
                    rowData[index] = MathConst.NAN_FOR_NULL;
                }
            }
        }
        return rowData;
    }

    public void updateSpectrumReadValues() {
        updateSpectrumValues(0);
    }

    public void updateSpectrumWriteValues() {
        updateSpectrumValues(1);
    }

    private void updateSpectrumValues(int writableIndex) {
        if ((splitData != null) && (writableIndex > -1) && (writableIndex < splitData.length)) {
            DbData data = splitData[writableIndex];
            if (data != null) {
                value = new double[data.getTimedData().length][Array
                        .getLength(data.getTimedData()[writableIndex].getValue())];
                for (int i = 0; i < data.size(); i++) {
                    value[i] = extractDataArray(i, data);
                }
            } else {
                value = null;
            }
        } else {
            value = null;
        }
        transmitData(true);
    }

    protected void transmitData(boolean enabled) {
        if (viewSpectrumImageAction != null) {
            viewSpectrumImageAction.setData(value, getDisplayFormat(spectrum));
        }
        updateViewButton(enabled);
    }

    @Override
    protected void updateComponents() {
        enableButtons(true);
        if (readValuesRButton.isSelected()) {
            updateSpectrumReadValues();
        } else if (writeValuesRButton.isSelected()) {
            updateSpectrumWriteValues();
        }
        setLoaded();
    }

    @Override
    protected void performDeepClean() {
        value = null;
        transmitData(false);
        viewSpectrumImageAction = null;
        radioBox.removeAll();
        buttonBox.removeAll();
        if (buttonGroup != null) {
            buttonGroup.remove(readValuesRButton);
            buttonGroup.remove(writeValuesRButton);
        }
        buttonGroup = null;
        readValuesRButton = null;
        writeValuesRButton = null;
        radioBox = null;
        buttonBox = null;
        viewButton = null;
        spectrum = null;
        cleanSplitData();
    }

    @Override
    public void lightClean() {
        setLoading();
        enableButtons(false);
        value = null;
        cleanSplitData();
        transmitData(false);
    }

    @Override
    public void initCenterPanelLayout() {
        centerPanel.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    @Override
    public int getViewSpectrumType() {
        return ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_IMAGE;
    }

    protected void enableButtons(boolean enabled) {
        if (readValuesRButton != null) {
            readValuesRButton.setEnabled(enabled);
        }
        if (writeValuesRButton != null) {
            writeValuesRButton.setEnabled(enabled);
        }
        if (viewSpectrumImageAction != null) {
            viewSpectrumImageAction.setEnabled(enabled);
        }
    }

    protected void updateViewButton(boolean enabled) {
        if (viewButton != null) {
            if (enabled) {
                if (splitData == null) {
                    viewButton.setText(Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA") + " " + getName());
                    viewButton.setEnabled(false);
                } else {
                    viewButton.setText(Messages.getMessage("VIEW_ACTION_VIEW_BUTTON") + " " + getName());
                    viewButton.setEnabled(true);
                }
            } else {
                viewButton.setText(Messages.getMessage("VIEW_ACTION_VIEW_BUTTON") + " " + getName());
                viewButton.setEnabled(false);
            }
        }
    }

}
