// +======================================================================
// $Source$
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class ViewAttributesTreePanel.
// (Claisse Laurent) - 13 mars 2008
//
// $Author$
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.containers.view;

import java.awt.BorderLayout;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.swing.Chart;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.expression.jep.JEPExpressionParser;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.comparators.AlphaNumericComparator;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.data.view.ViewConfigurationAttributes;
import fr.soleil.mambo.tools.Messages;

public class ViewAttributesGraphPanel extends JPanel {

    private static final long serialVersionUID = 1716336439987599712L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewAttributesGraphPanel.class);

    private static final Insets TAB_BORDER_INSETS = new Insets(0, 1, 2, 1);
    private static final Insets TAB_AREA_INSETS = new Insets(0, 2, 2, 2);
    private static final Insets TAB_INSETS = new Insets(0, 2, 4, 2);
    private static final Insets SELECTED_TAB_INSETS = new Insets(4, 2, 0, 2);

    public static final String SETTING_PANELS = "setting panels";
    public static final String PANELS_SET = "panels set";
    public static final String ADDING_PANELS = "adding panels";
    public static final String PANELS_ADDED = "panels added";
    public static final String LOADING_PANELS = "loading panels";
    public static final String PANELS_LOADED = "panels loaded";
    public static final String CANCELING = "canceling";
    public static final String CANCELED = "canceled";

    public static final String NUMBER_SCALARS_KEY = "number scalars";
    public static final String OTHER_SCALARS_KEY = "other scalars";
    public static final String SPECTRA_KEY = "spectra";
    public static final String IMAGES_KEY = "images";

    private final JTabbedPane attributesTab;

    private ViewConfigurationAttribute[] strings;
    private final Chart chart;
    private ViewConfiguration lastVc;

    private final Map<String, AbstractViewSpectrumPanel> spectrumPanelsMap;
    private ViewImagePanel[] imagePanels;
    private final ViewNumberScalarPanel numberScalarsPanel;
    private final ViewStringStateBooleanScalarPanel otherScalarsPanel;

    protected PropertyChangeSupport support;

    private final ViewConfigurationBean viewConfigurationBean;

    private int selectedIndex;

    public ViewAttributesGraphPanel(final ViewConfigurationBean viewConfigurationBean) {
        super(new BorderLayout());
        this.viewConfigurationBean = viewConfigurationBean;
        support = new PropertyChangeSupport(this);
        chart = new Chart();
        chart.setDataViewComparator(new AlphaNumericComparator());
        chart.setExpressionParser(new JEPExpressionParser());
        numberScalarsPanel = new ViewNumberScalarPanel(chart, viewConfigurationBean);
        otherScalarsPanel = new ViewStringStateBooleanScalarPanel(viewConfigurationBean);
        spectrumPanelsMap = new TreeMap<>();
        imagePanels = new ViewImagePanel[0];
        selectedIndex = 0;
        initBorder();
        initBounds();
        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);
        attributesTab = new JTabbedPane();
        attributesTab.setUI(new AttributesTabUI());
        GUIUtilities.setObjectBackground(attributesTab, GUIUtilities.VIEW_COLOR);
    }

    /**
     * 19 juil. 2005
     */
    private void addComponents() {
        support.firePropertyChange(new PropertyChangeEvent(this, ADDING_PANELS, null, null));
        attributesTab.grabFocus();
        if (attributesTab.getTabCount() > 0) {
            for (int i = 0; i < attributesTab.getTabCount(); i++) {
                attributesTab.setIconAt(i, AbstractViewCleanablePanel.LOADING_ICON);
            }
            add(attributesTab, BorderLayout.CENTER);
        }
        support.firePropertyChange(new PropertyChangeEvent(this, PANELS_ADDED, null, null));
    }

    public Map<ViewConfigurationAttribute, Integer> extractDataTypeMap(ViewConfigurationAttributes attributes)
            throws ArchivingException {
        Map<ViewConfigurationAttribute, Integer> result;
        if ((attributes != null) && (viewConfigurationBean != null)
                && (viewConfigurationBean.getViewConfiguration() != null)) {
            final ViewConfiguration vc = viewConfigurationBean.getViewConfiguration();
            result = vc.extractDataTypeMap(attributes);
        } else {
            result = new HashMap<>();
        }
        return result;
    }

    /**
     * Split attributes per category.
     * 
     * @param attributes The attributes to split.
     * @return A {@link Map} that will contain following keys and values:
     *         <table border=1 style="border-collapse:collapse">
     *         <tr>
     *         <th>Key</th>
     *         <th>Value</th>
     *         </tr>
     *         <tr>
     *         <td>{@link #NUMBER_SCALARS_KEY}</td>
     *         <td><code>{@link String}[]</code>: the complete names of the number and boolean scalar attributes</td>
     *         </tr>
     *         <tr>
     *         <td>{@link #OTHER_SCALARS_KEY}</td>
     *         <td><code>{@link ViewConfigurationAttribute}[]</code>: the string and state scalar attributes</td>
     *         </tr>
     *         <tr>
     *         <td>{@link #SPECTRA_KEY}</td>
     *         <td><code>{@link ViewConfigurationAttribute}[]</code>: the spectrum attributes</td>
     *         </tr>
     *         <tr>
     *         <td>{@link #IMAGES_KEY}</td>
     *         <td><code>{@link ViewConfigurationAttribute}[]</code>: the image attributes</td>
     *         </tr>
     *         </table>
     * @throws ArchivingException If a problem occurred while accessing database.
     */
    public Map<String, Object[]> splitAttributes(ViewConfigurationAttributes attributes) throws ArchivingException {
        Map<String, Object[]> result = new HashMap<>();
        try {
            if ((attributes != null) && (viewConfigurationBean != null)
                    && (viewConfigurationBean.getViewConfiguration() != null)) {
                final ViewConfiguration vc = viewConfigurationBean.getViewConfiguration();
                final Map<String, ViewConfigurationAttribute> stringTable = attributes
                        .getStringStateScalarAttributes(vc.getData().isHistoric());
                result.put(OTHER_SCALARS_KEY,
                        stringTable.values().toArray(new ViewConfigurationAttribute[stringTable.size()]));
                stringTable.clear();
                result.put(NUMBER_SCALARS_KEY,
                        attributes.getNumberAndBooleanScalarAttributeNames(vc.getData().isHistoric()));
                final Map<String, ViewConfigurationAttribute> imageMap = attributes
                        .getImageAttributes(vc.getData().isHistoric());
                result.put(IMAGES_KEY, imageMap.values().toArray(new ViewConfigurationAttribute[imageMap.size()]));
                imageMap.clear();
                final Map<String, ViewConfigurationAttribute> spectrumMap = attributes
                        .getSpectrumAttributes(vc.getData().isHistoric());
                ViewConfigurationAttribute[] spectra = spectrumMap.values()
                        .toArray(new ViewConfigurationAttribute[spectrumMap.size()]);
                result.put(SPECTRA_KEY, spectra);
                spectrumMap.clear();
            }
        } catch (ArchivingException e) {
            // In order to optimize memory consumption, clear map in case of error
            result.clear();
            throw e;
        }
        return result;
    }

    /**
     * 19 juil. 2005
     */
    public void initComponents(Map<String, Object[]> attributes, Map<ViewConfigurationAttribute, Integer> dataTypes,
            int attrCount) {
        support.firePropertyChange(new PropertyChangeEvent(this, ADDING_PANELS, null, null));
        if ((attributes != null) && (viewConfigurationBean != null)
                && (viewConfigurationBean.getViewConfiguration() != null)) {
            final ViewConfiguration vc = viewConfigurationBean.getViewConfiguration();
            try {
                Object tmp = attributes.get(OTHER_SCALARS_KEY);
                if (tmp instanceof ViewConfigurationAttribute[]) {
                    strings = (ViewConfigurationAttribute[]) tmp;
                } else {
                    strings = null;
                }
                otherScalarsPanel.setAttributes(strings);
                tmp = attributes.get(NUMBER_SCALARS_KEY);
                if (tmp instanceof String[]) {
                    numberScalarsPanel.setAttributes((String[]) tmp);
                } else {
                    numberScalarsPanel.setAttributes((String[]) null);
                }

                ViewConfigurationAttribute[] images;
                tmp = attributes.get(IMAGES_KEY);
                if (tmp instanceof ViewConfigurationAttribute[]) {
                    images = (ViewConfigurationAttribute[]) tmp;
                } else {
                    images = null;
                }

                int imageSize = images == null ? 0 : images.length;
                if (imageSize != imagePanels.length) {
                    if (imagePanels.length > imageSize) {
                        for (int j = imageSize; j < imagePanels.length; j++) {
                            attributesTab.remove(imagePanels[j]);
                        }
                    }
                    imagePanels = Arrays.copyOf(imagePanels, imageSize);
                }
                int i = 0;
                if (images != null) {
                    for (ViewConfigurationAttribute image : images) {
                        if (imagePanels[i] == null) {
                            imagePanels[i] = new ViewImagePanel(viewConfigurationBean);
                        }
                        attributesTab.addTab(image.getCompleteName(), imagePanels[i]);
                        imagePanels[i++].setImage(image);
                    }
                }

                ViewConfigurationAttribute[] spectra;
                tmp = attributes.get(SPECTRA_KEY);
                if (tmp instanceof ViewConfigurationAttribute[]) {
                    spectra = (ViewConfigurationAttribute[]) tmp;
                } else {
                    spectra = null;
                }

                Collection<String> toRemove = new ArrayList<>(spectrumPanelsMap.keySet());
                int spectrumSize = 0;
                i = 0;
                if ((spectra != null) && (dataTypes != null)) {
                    for (ViewConfigurationAttribute attribute : spectra) {
                        Integer value = dataTypes.get(attribute);
                        int dataType = value == null ? -1 : value.intValue();
                        final String name = attribute.getCompleteName();
                        toRemove.remove(name);
                        spectrumSize++;
                        AbstractViewSpectrumPanel spectrumPanel = spectrumPanelsMap.get(name);
                        if (dataType == TangoConst.Tango_DEV_STRING || dataType == TangoConst.Tango_DEV_STATE
                                || dataType == TangoConst.Tango_DEV_BOOLEAN) {
                            if (!(spectrumPanel instanceof ViewStringStateBooleanSpectrumPanel)) {
                                if (spectrumPanel != null) {
                                    attributesTab.remove(spectrumPanel);
                                }
                                spectrumPanel = new ViewStringStateBooleanSpectrumPanel(viewConfigurationBean);
                                spectrumPanelsMap.put(name, spectrumPanel);
                            }
                        } else {
                            int attributeSpectrumType = attribute.getProperties().getPlotProperties()
                                    .getSpectrumViewType();
                            if (attributeSpectrumType == ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME_STACK) {
                                // Time stack mode
                                if (!(spectrumPanel instanceof ViewSpectrumStackPanel)) {
                                    if (spectrumPanel != null) {
                                        attributesTab.remove(spectrumPanel);
                                    }
                                    spectrumPanel = new ViewSpectrumStackPanel(viewConfigurationBean);
                                    spectrumPanelsMap.put(name, spectrumPanel);
                                }
                            } else if (attributeSpectrumType == ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_IMAGE) {
                                // Image view
                                if (!(spectrumPanel instanceof ViewSpectrumImagePanel)) {
                                    if (spectrumPanel != null) {
                                        attributesTab.remove(spectrumPanel);
                                    }
                                    spectrumPanel = new ViewSpectrumImagePanel(viewConfigurationBean);
                                    spectrumPanelsMap.put(name, spectrumPanel);
                                }
                            } else if (!(spectrumPanel instanceof ViewSpectrumPanel)) {
                                // index or time mode
                                if (spectrumPanel != null) {
                                    attributesTab.remove(spectrumPanel);
                                }
                                spectrumPanel = new ViewSpectrumPanel(viewConfigurationBean);
                                spectrumPanelsMap.put(name, spectrumPanel);
                            }
                        } // end dataType test
                        spectrumPanel.setSpectrum(attribute);
                        attributesTab.addTab(spectrumPanel.getName(), null, spectrumPanel, spectrumPanel.getFullName());
                    } // for (Entry<?, ?> entry : spectra.entrySet())
                } // end if (spectra != null)
                for (String name : toRemove) {
                    AbstractViewSpectrumPanel panel = spectrumPanelsMap.remove(name);
                    if (panel != null) {
                        attributesTab.remove(panel);
                    }
                }

                updateChartFromVc(vc);

                int stringSize = strings == null ? 0 : strings.length;
                int stringIndex;
                if (imageSize + spectrumSize + stringSize < attrCount) {
                    if (attributesTab.getTabCount() < 1) {
                        attributesTab.addTab(Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_TITLE"), null,
                                numberScalarsPanel, Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_TOOLTIP"));
                    } else {
                        attributesTab.insertTab(Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_TITLE"), null,
                                numberScalarsPanel, Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_TOOLTIP"), 0);
                    }
                    stringIndex = 1;
                } else {
                    attributesTab.remove(numberScalarsPanel);
                    stringIndex = 0;
                }
                if (stringSize > 0) {
                    String title = Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_TITLE");
                    if (stringIndex >= attributesTab.getTabCount()) {
                        attributesTab.addTab(title, null, otherScalarsPanel, title);
                    } else {
                        attributesTab.insertTab(title, null, otherScalarsPanel, title, stringIndex);
                    }
                }
                chart.setCometeBackground(ColorTool.getCometeColor(GUIUtilities.getViewColor()));
                if ((selectedIndex > -1) && (selectedIndex < attributesTab.getTabCount())) {
                    attributesTab.setSelectedIndex(selectedIndex);
                }
                support.firePropertyChange(new PropertyChangeEvent(this, PANELS_SET, null, null));
                vc.prepareExpressions(chart, dataTypes);
            } catch (final ArchivingException e) {
                LOGGER.error("error", e);
            }
        }
        if (attributes != null) {
            attributes.clear();
        }
        if (dataTypes != null) {
            dataTypes.clear();
        }
    }

    private void initBounds() {
        this.setBounds(new Rectangle(0, 0, 2000, 2000));
    }

    private void initBorder() {
        final TitledBorder tb = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                ObjectUtils.EMPTY_STRING, TitledBorder.CENTER, TitledBorder.TOP);
        final Border border = tb;
        setBorder(border);
    }

    public AbstractViewCleanablePanel[] getViewPanels() {
        AbstractViewCleanablePanel[] panels = new AbstractViewCleanablePanel[2 + spectrumPanelsMap.size()
                + imagePanels.length];
        int count;
        if ((viewConfigurationBean != null) && (viewConfigurationBean.getViewConfiguration() != null)) {
            count = viewConfigurationBean.getViewConfiguration().getNotNumberExpressionsCount();
        } else {
            count = 0;
        }
        // If there are some string expressions, string panel comes first, so that when number attributes are loaded,
        // string attributes and string expressions are already loaded (i.e. when number scalar panel is always right
        // when it considers being loaded). Otherwise, panels come in order of appearance for user
        if (count > 0) {
            panels[0] = otherScalarsPanel;
            panels[1] = numberScalarsPanel;
        } else {
            panels[0] = numberScalarsPanel;
            panels[1] = otherScalarsPanel;
        }
        int i = 2;
        for (final AbstractViewCleanablePanel panel : spectrumPanelsMap.values()) {
            panels[i++] = panel;
        }
        for (final AbstractViewCleanablePanel panel : imagePanels) {
            panels[i++] = panel;
        }
        return panels;
    }

    public void cancel() {
        support.firePropertyChange(new PropertyChangeEvent(this, CANCELING, null, null));
        numberScalarsPanel.setCanceled();
        otherScalarsPanel.setCanceled();
        if (spectrumPanelsMap != null) {
            for (final AbstractViewCleanablePanel spectrumPanel : spectrumPanelsMap.values()) {
                if (spectrumPanel != null) {
                    spectrumPanel.setCanceled();
                }
            }
        }
        if (imagePanels != null) {
            for (final AbstractViewCleanablePanel imagePanel : imagePanels) {
                if (imagePanel != null) {
                    imagePanel.setCanceled();
                }
            }
        }
        support.firePropertyChange(new PropertyChangeEvent(this, CANCELED, null, null));
    }

    public void clean() {
        if (attributesTab.getSelectedIndex() > -1) {
            selectedIndex = attributesTab.getSelectedIndex();
        }
        attributesTab.removeAll();
        if (spectrumPanelsMap != null) {
            for (final AbstractViewCleanablePanel spectrumPanel : spectrumPanelsMap.values()) {
                if (spectrumPanel != null) {
                    spectrumPanel.lightClean();
                }
            }
        }
        if (imagePanels != null) {
            for (final AbstractViewCleanablePanel imagePanel : imagePanels) {
                if (imagePanel != null) {
                    imagePanel.lightClean();
                }
            }
        }
        numberScalarsPanel.lightClean();
        otherScalarsPanel.lightClean();
        removeAll();
    }

    public void prepareComponentsReset() {
        setLoading();
        clean();
    }

    public void setComponents(Map<String, Object[]> attributes, Map<ViewConfigurationAttribute, Integer> dataTypes,
            int attrCount) {
        initComponents(attributes, dataTypes, attrCount);
        addComponents();
    }

    public void setLoading() {
        if (viewConfigurationBean != null && viewConfigurationBean.getViewConfiguration() != null) {
            viewConfigurationBean.getViewConfiguration().computeCurrentId();
        }
        support.firePropertyChange(new PropertyChangeEvent(this, LOADING_PANELS, null, null));
    }

    protected void setLoaded(AbstractViewCleanablePanel panel) {
        if ((panel != null) && !panel.isCanceled(null)) {
            panel.setLoaded();
        }
    }

    public void setLoaded() {
        setLoaded(numberScalarsPanel);
        setLoaded(otherScalarsPanel);
        if (imagePanels != null) {
            for (ViewImagePanel panel : imagePanels) {
                setLoaded(panel);
            }
        }
        support.firePropertyChange(new PropertyChangeEvent(this, PANELS_LOADED, null, null));
    }

    public void loadPanels() {
        support.firePropertyChange(new PropertyChangeEvent(this, PANELS_LOADED, null, null));
    }

    public JTabbedPane getAttributesTab() {
        return attributesTab;
    }

    public ViewNumberScalarPanel getNumberScalarsPanel() {
        return numberScalarsPanel;
    }

    public AbstractViewCleanablePanel getOtherScalarsPanel() {
        return otherScalarsPanel;
    }

    public Chart getChart() {
        return chart;
    }

    private void updateChartFromVc(ViewConfiguration vc) {
        if (numberScalarsPanel != null) {
            chart.removeChartViewerListener(numberScalarsPanel);
        }
        if (lastVc != null) {
            lastVc.cleanChart(chart);
        }
        if (vc != null) {
            vc.configureChart(chart);
        }
        lastVc = vc;
        if (numberScalarsPanel != null) {
            chart.addChartViewerListener(numberScalarsPanel);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * A {@link BasicTabbedPaneUI} that can use a special background for
     * selected tab
     * 
     * @author girardot
     */
    private static class AttributesTabUI extends BasicTabbedPaneUI {

        public AttributesTabUI() {
            super();
        }

        @Override
        protected void installDefaults() {
            Object contentOpaque = UIManager.get("TabbedPane.contentOpaque");
            Object opaque = UIManager.get("TabbedPane.opaque");
            UIManager.put("TabbedPane.opaque", Boolean.TRUE);
            UIManager.put("TabbedPane.contentOpaque", Boolean.TRUE);
            super.installDefaults();
            UIManager.put("TabbedPane.opaque", opaque);
            UIManager.put("TabbedPane.contentOpaque", contentOpaque);
            contentBorderInsets = TAB_BORDER_INSETS;
            tabAreaInsets = TAB_AREA_INSETS;
            tabInsets = TAB_INSETS;
            selectedTabPadInsets = SELECTED_TAB_INSETS;
        }

        /**
         * this function draws the border around each tab note that this
         * function does now draw the background of the tab. that is done
         * elsewhere
         */
        @Override
        protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h,
                boolean isSelected) {
            g.setColor(lightHighlight);

            switch (tabPlacement) {
                case LEFT:
                    g.drawLine(x + 1, y + h - 2, x + 1, y + h - 2); // bottom-left
                                                                    // highlight
                    g.drawLine(x, y + 2, x, y + h - 3); // left highlight
                    g.drawLine(x + 1, y + 1, x + 1, y + 1); // top-left highlight
                    g.drawLine(x + 2, y, x + w - 1, y); // top highlight

                    g.setColor(shadow);
                    g.drawLine(x + 2, y + h - 2, x + w - 1, y + h - 2); // bottom
                                                                        // shadow

                    g.setColor(darkShadow);
                    g.drawLine(x + 2, y + h - 1, x + w - 1, y + h - 1); // bottom
                                                                        // dark
                                                                        // shadow
                    break;
                case RIGHT:
                    g.drawLine(x, y, x + w - 3, y); // top highlight

                    g.setColor(shadow);
                    g.drawLine(x, y + h - 2, x + w - 3, y + h - 2); // bottom shadow
                    g.drawLine(x + w - 2, y + 2, x + w - 2, y + h - 3); // right
                                                                        // shadow

                    g.setColor(darkShadow);
                    g.drawLine(x + w - 2, y + 1, x + w - 2, y + 1); // top-right
                                                                    // dark shadow
                    g.drawLine(x + w - 2, y + h - 2, x + w - 2, y + h - 2); // bottom-right
                                                                            // dark
                                                                            // shadow
                    g.drawLine(x + w - 1, y + 2, x + w - 1, y + h - 3); // right
                                                                        // dark
                                                                        // shadow
                    g.drawLine(x, y + h - 1, x + w - 3, y + h - 1); // bottom dark
                                                                    // shadow
                    break;
                case BOTTOM:
                    g.drawLine(x, y, x, y + h - 3); // left highlight
                    g.drawLine(x + 1, y + h - 2, x + 1, y + h - 2); // bottom-left
                                                                    // highlight

                    g.setColor(shadow);
                    g.drawLine(x + 2, y + h - 2, x + w - 3, y + h - 2); // bottom
                                                                        // shadow
                    g.drawLine(x + w - 2, y, x + w - 2, y + h - 3); // right shadow

                    g.setColor(darkShadow);
                    g.drawLine(x + 2, y + h - 1, x + w - 3, y + h - 1); // bottom
                                                                        // dark
                                                                        // shadow
                    g.drawLine(x + w - 2, y + h - 2, x + w - 2, y + h - 2); // bottom-right
                                                                            // dark
                                                                            // shadow
                    g.drawLine(x + w - 1, y, x + w - 1, y + h - 3); // right dark
                                                                    // shadow
                    break;
                case TOP:
                default:
                    g.drawLine(x, y + 2, x, y + h - 1); // left highlight
                    g.drawLine(x + 1, y + 1, x + 1, y + 1); // top-left highlight
                    g.drawLine(x + 2, y, x + w - 3, y); // top highlight

                    g.setColor(shadow);
                    g.drawLine(x + w - 2, y + 2, x + w - 2, y + h - 1); // right
                                                                        // shadow

                    g.setColor(darkShadow);
                    g.drawLine(x + w - 1, y + 2, x + w - 1, y + h - 1); // right
                                                                        // dark-shadow
                    g.drawLine(x + w - 2, y + 1, x + w - 2, y + 1); // top-right
                                                                    // dark-shadow
                    if (tabIndex != tabPane.getSelectedIndex()) {
                        g.drawLine(x, y + h - 1, x + w - 3, y + h - 1); // bottom
                                                                        // dark-shadow
                    }
            }
        }

        @Override
        protected void paintTabBackground(final Graphics g, final int tabPlacement, final int tabIndex, final int x,
                final int y, final int w, final int h, final boolean isSelected) {
            final Graphics2D g2d;
            final Paint paint;
            if (g instanceof Graphics2D) {
                g2d = (Graphics2D) g;
                paint = g2d.getPaint();
            } else {
                g2d = null;
                paint = null;
            }
            if (isSelected) {
                if (g2d == null) {
                    g.setColor(GUIUtilities.getViewSelectionColor());
                } else {
                    // ensure a color gradient
                    final int x1, y1, x2, y2;
                    switch (tabPlacement) {
                        case LEFT:
                            x1 = x;
                            y1 = y;
                            x2 = x + w;
                            y2 = y;
                            break;
                        case RIGHT:
                            x1 = x + w;
                            y1 = y;
                            x2 = x;
                            y2 = y;
                            break;
                        case BOTTOM:
                            x1 = x;
                            y1 = y + h;
                            x2 = x;
                            y2 = y;
                            break;
                        case TOP:
                        default:
                            x1 = x;
                            y1 = y;
                            x2 = x;
                            y2 = y + h;
                    }
                    g2d.setPaint(new GradientPaint(x1, y1, GUIUtilities.getViewSelectionColor(), x2, y2,
                            GUIUtilities.getViewColor()));
                }
            } else {
                g.setColor(GUIUtilities.getViewColor());
            }

            switch (tabPlacement) {
                case LEFT:
                    g.fillRect(x + 1, y + 1, w - 1, h - 3);
                    break;
                case RIGHT:
                    g.fillRect(x, y + 1, w - 2, h - 3);
                    break;
                case BOTTOM:
                    g.fillRect(x + 1, y, w - 3, h - 1);
                    break;
                case TOP:
                default:
                    g.fillRect(x + 1, y + 1, w - 3, h - 1);
            }
            if (g2d != null) {
                g2d.setPaint(paint);
            }
        }

    }

}
