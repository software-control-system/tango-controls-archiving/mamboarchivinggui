package fr.soleil.mambo.containers.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.actions.view.ViewCopyAction;
import fr.soleil.mambo.actions.view.ViewEditCopyAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.MamboFormatableTable;
import fr.soleil.mambo.components.renderers.BooleanTableRenderer;
import fr.soleil.mambo.components.renderers.StateTableRenderer;
import fr.soleil.mambo.components.renderers.StringTableRenderer;
import fr.soleil.mambo.models.ViewStringStateBooleanSpectrumTableModel;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.tools.Messages;

public class ViewStringStateBooleanSpectrumPanel extends AbstractViewSpectrumPanel {

    private static final long serialVersionUID = 3128382120700092869L;

    private static final int MIN_COL_WIDTH = 80;
    public static final int READ_REFERENCE = 0;
    public static final int WRITE_REFERENCE = 1;
    private static final Dimension REF_SIZE = new Dimension(50, 50);
    private static final Insets DEFAULT_INSETS = new Insets(5, 5, 0, 5);

    private final JButton copyRead, copyWrite, editRead, editWrite;
    private final MamboFormatableTable readTable, writeTable;
    private final JPanel readButtonPanel, writeButtonPanel;
    private final JLabel readLabel, writeLabel;

    private int dataType = -1;

    public ViewStringStateBooleanSpectrumPanel(final ViewConfigurationBean viewConfigurationBean)
            throws ArchivingException {
        super(viewConfigurationBean);

        readLabel = new JLabel(Messages.getMessage("VIEW_SPECTRUM_READ"));
        readLabel.setVerticalAlignment(SwingConstants.NORTH);
        GUIUtilities.setObjectBackground(readLabel, GUIUtilities.VIEW_COLOR);
        readTable = new MamboFormatableTable(new ViewStringStateBooleanSpectrumTableModel());
        readTable.setAutoResizeMode(MamboFormatableTable.AUTO_RESIZE_OFF);
        GUIUtilities.setObjectBackground(readTable, GUIUtilities.VIEW_COLOR);
        copyRead = new JButton(new ViewCopyAction(Messages.getMessage("VIEW_SPECTRUM_COPY"), this, READ_REFERENCE));
        GUIUtilities.setObjectBackground(copyRead, GUIUtilities.VIEW_COPY_COLOR);
        copyRead.setMargin(new Insets(0, 0, 0, 0));
        editRead = new JButton(
                new ViewEditCopyAction(Messages.getMessage("VIEW_SPECTRUM_EDIT_COPY"), this, READ_REFERENCE));
        GUIUtilities.setObjectBackground(editRead, GUIUtilities.VIEW_COPY_COLOR);
        editRead.setMargin(new Insets(0, 0, 0, 0));
        readButtonPanel = new JPanel();
        GUIUtilities.setObjectBackground(readButtonPanel, GUIUtilities.VIEW_COLOR);
        readButtonPanel.setLayout(new GridBagLayout());
        GridBagConstraints copyConstraints = new GridBagConstraints();
        copyConstraints.fill = GridBagConstraints.NONE;
        copyConstraints.gridx = 0;
        copyConstraints.gridy = 0;
        copyConstraints.weightx = 0;
        copyConstraints.weighty = 0;
        GridBagConstraints glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.HORIZONTAL;
        glueConstraints.gridx = 1;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 0;
        readButtonPanel.add(copyRead, copyConstraints);
        readButtonPanel.add(Box.createHorizontalGlue(), glueConstraints);
        GridBagConstraints editConstraints = new GridBagConstraints();
        editConstraints.fill = GridBagConstraints.NONE;
        editConstraints.gridx = 2;
        editConstraints.gridy = 0;
        editConstraints.weightx = 0;
        editConstraints.weighty = 0;
        readButtonPanel.add(editRead, editConstraints);

        writeLabel = new JLabel(Messages.getMessage("VIEW_SPECTRUM_WRITE"));
        writeLabel.setVerticalAlignment(SwingConstants.NORTH);
        GUIUtilities.setObjectBackground(writeLabel, GUIUtilities.VIEW_COLOR);
        writeTable = new MamboFormatableTable(new ViewStringStateBooleanSpectrumTableModel());
        writeTable.setAutoResizeMode(MamboFormatableTable.AUTO_RESIZE_OFF);
        GUIUtilities.setObjectBackground(writeTable, GUIUtilities.VIEW_COLOR);
        copyWrite = new JButton(new ViewCopyAction(Messages.getMessage("VIEW_SPECTRUM_COPY"), this, WRITE_REFERENCE));
        GUIUtilities.setObjectBackground(copyWrite, GUIUtilities.VIEW_COPY_COLOR);
        copyWrite.setMargin(new Insets(0, 0, 0, 0));
        editWrite = new JButton(
                new ViewEditCopyAction(Messages.getMessage("VIEW_SPECTRUM_EDIT_COPY"), this, WRITE_REFERENCE));
        GUIUtilities.setObjectBackground(editWrite, GUIUtilities.VIEW_COPY_COLOR);
        editWrite.setMargin(new Insets(0, 0, 0, 0));
        writeButtonPanel = new JPanel();
        GUIUtilities.setObjectBackground(writeButtonPanel, GUIUtilities.VIEW_COLOR);
        writeButtonPanel.setLayout(new GridBagLayout());
        copyConstraints = new GridBagConstraints();
        copyConstraints.fill = GridBagConstraints.NONE;
        copyConstraints.gridx = 0;
        copyConstraints.gridy = 0;
        copyConstraints.weightx = 0;
        copyConstraints.weighty = 0;
        glueConstraints = new GridBagConstraints();
        glueConstraints.fill = GridBagConstraints.HORIZONTAL;
        glueConstraints.gridx = 1;
        glueConstraints.gridy = 0;
        glueConstraints.weightx = 1;
        glueConstraints.weighty = 0;
        writeButtonPanel.add(copyWrite, copyConstraints);
        writeButtonPanel.add(Box.createHorizontalGlue(), glueConstraints);
        editConstraints = new GridBagConstraints();
        editConstraints.fill = GridBagConstraints.NONE;
        editConstraints.gridx = 2;
        editConstraints.gridy = 0;
        editConstraints.weightx = 0;
        editConstraints.weighty = 0;
        writeButtonPanel.add(editWrite, editConstraints);

    }

    @Override
    protected void prepareComponents() {
        super.prepareComponents();
        if (spectrum == null) {
            dataType = -1;
        } else {
            try {
                dataType = spectrum.getDataType(isHistoric());
            } catch (ArchivingException e) {
                dataType = -1;
            }
        }
        if ((dataType != TangoConst.Tango_DEV_STRING) && (dataType != TangoConst.Tango_DEV_STATE)
                && (dataType != TangoConst.Tango_DEV_BOOLEAN)) {
            clean();
        }
        readLabel.setText(getFullName() + " " + Messages.getMessage("VIEW_SPECTRUM_READ"));
        writeLabel.setText(getFullName() + " " + Messages.getMessage("VIEW_SPECTRUM_WRITE"));
        writeLabel.setForeground(null);
    }

    @Override
    protected void updateComponents() {
        writeLabel.setForeground(null);
        int gridy = 1;
        if ((splitData != null)) {
            if (!isNullOrEmpty(splitData[0])) {
                ((ViewStringStateBooleanSpectrumTableModel) readTable.getModel()).setData(splitData[0], dataType);
                for (int i = 0; i < readTable.getColumnCount(); i++) {
                    readTable.getColumn(readTable.getColumnName(i)).setMinWidth(MIN_COL_WIDTH);
                }
                if (dataType == TangoConst.Tango_DEV_STATE) {
                    readTable.setDefaultRenderer(Object.class, new StateTableRenderer());
                } else if (dataType == TangoConst.Tango_DEV_STRING) {
                    readTable.setDefaultRenderer(Object.class, new StringTableRenderer());
                } else {
                    // boolean
                    readTable.setDefaultRenderer(Object.class, new BooleanTableRenderer());
                }
                final GridBagConstraints readLabelConstraints = new GridBagConstraints();
                readLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
                readLabelConstraints.gridx = 0;
                readLabelConstraints.gridy = gridy++;
                readLabelConstraints.weightx = 1;
                readLabelConstraints.weighty = 0;
                readLabelConstraints.insets = DEFAULT_INSETS;
                centerPanel.add(readLabel, readLabelConstraints);
                final GridBagConstraints readButtonPanelConstraints = new GridBagConstraints();
                readButtonPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
                readButtonPanelConstraints.gridx = 0;
                readButtonPanelConstraints.gridy = gridy++;
                readButtonPanelConstraints.weightx = 1;
                readButtonPanelConstraints.weighty = 0;
                readButtonPanelConstraints.insets = DEFAULT_INSETS;
                centerPanel.add(readButtonPanel, readButtonPanelConstraints);
                final GridBagConstraints readPaneConstraints = new GridBagConstraints();
                readPaneConstraints.fill = GridBagConstraints.BOTH;
                readPaneConstraints.gridx = 0;
                readPaneConstraints.gridy = gridy++;
                readPaneConstraints.weightx = 1;
                readPaneConstraints.weighty = 0.5;
                readPaneConstraints.insets = DEFAULT_INSETS;
                final JScrollPane readPane = new JScrollPane(readTable);
                readPane.setPreferredSize(REF_SIZE);
                readPane.setMinimumSize(REF_SIZE);
                GUIUtilities.setObjectBackground(readPane, GUIUtilities.VIEW_COLOR);
                GUIUtilities.setObjectBackground(readPane.getViewport(), GUIUtilities.VIEW_COLOR);
                centerPanel.add(readPane, readPaneConstraints);
            }
            if (!isNullOrEmpty(splitData[1])) {
                ((ViewStringStateBooleanSpectrumTableModel) writeTable.getModel()).setData(splitData[1], dataType);
                for (int i = 0; i < writeTable.getColumnCount(); i++) {
                    writeTable.getColumn(writeTable.getColumnName(i)).setMinWidth(MIN_COL_WIDTH);
                }
                if (dataType == TangoConst.Tango_DEV_STATE) {
                    writeTable.setDefaultRenderer(Object.class, new StateTableRenderer());
                } else if (dataType == TangoConst.Tango_DEV_STRING) {
                    writeTable.setDefaultRenderer(Object.class, new StringTableRenderer());
                } else {
                    // boolean
                    writeTable.setDefaultRenderer(Object.class, new BooleanTableRenderer());
                }
                final GridBagConstraints writeLabelConstraints = new GridBagConstraints();
                writeLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
                writeLabelConstraints.gridx = 0;
                writeLabelConstraints.gridy = gridy++;
                writeLabelConstraints.weightx = 1;
                writeLabelConstraints.weighty = 0;
                if (gridy > 1) {
                    writeLabelConstraints.insets = new Insets(20, 5, 5, 5);
                } else {
                    writeLabelConstraints.insets = DEFAULT_INSETS;
                }
                centerPanel.add(writeLabel, writeLabelConstraints);
                final GridBagConstraints writeButtonPanelConstraints = new GridBagConstraints();
                writeButtonPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
                writeButtonPanelConstraints.gridx = 0;
                writeButtonPanelConstraints.gridy = gridy++;
                writeButtonPanelConstraints.weightx = 1;
                writeButtonPanelConstraints.weighty = 0;
                writeButtonPanelConstraints.insets = DEFAULT_INSETS;
                centerPanel.add(writeButtonPanel, writeButtonPanelConstraints);
                final GridBagConstraints writePaneConstraints = new GridBagConstraints();
                writePaneConstraints.fill = GridBagConstraints.BOTH;
                writePaneConstraints.gridx = 0;
                writePaneConstraints.gridy = gridy++;
                writePaneConstraints.weightx = 1;
                writePaneConstraints.weighty = 0.5;
                writePaneConstraints.insets = DEFAULT_INSETS;
                final JScrollPane writePane = new JScrollPane(writeTable);
                writePane.setPreferredSize(REF_SIZE);
                writePane.setMinimumSize(REF_SIZE);
                GUIUtilities.setObjectBackground(writePane, GUIUtilities.VIEW_COLOR);
                GUIUtilities.setObjectBackground(writePane.getViewport(), GUIUtilities.VIEW_COLOR);
                centerPanel.add(writePane, writePaneConstraints);
            }
        }
        if (gridy == 1) {
            writeLabel.setText(Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA"));
            GUIUtilities.setObjectBackground(writeLabel, GUIUtilities.VIEW_COLOR);
            writeLabel.setForeground(Color.RED);
            final GridBagConstraints writeLabelConstraints = new GridBagConstraints();
            writeLabelConstraints.fill = GridBagConstraints.BOTH;
            writeLabelConstraints.gridx = 0;
            writeLabelConstraints.gridy = gridy++;
            writeLabelConstraints.weightx = 1;
            writeLabelConstraints.weighty = 1;
            writeLabelConstraints.anchor = GridBagConstraints.NORTH;
            centerPanel.add(writeLabel, writeLabelConstraints);
        }

    }

    @Override
    protected void initCenterPanelLayout() {
        centerPanel.setLayout(new GridBagLayout());
    }

    public String getReadValue() {
        StringBuilder result = new StringBuilder();
        if (readTable != null) {
            for (int i = 0; i < readTable.getColumnCount(); i++) {
                result.append(readTable.getColumnName(i))
                        .append(Options.getInstance().getGeneralOptions().getSeparator());
            }
            int length = result.length();
            if (length != 0) {
                result = result.deleteCharAt(length - 1);
            }
            result.append("\n");
            readTable.appendStringValue(result);
        }
        return result.toString();
    }

    public String getWriteValue() {
        StringBuilder result = new StringBuilder();
        if (writeTable != null) {
            for (int i = 0; i < writeTable.getColumnCount(); i++) {
                result.append(writeTable.getColumnName(i))
                        .append(Options.getInstance().getGeneralOptions().getSeparator());
            }
            int length = result.length();
            if (length != 0) {
                result = result.deleteCharAt(length - 1);
            }
            result.append("\n");
            writeTable.appendStringValue(result);
        }
        return result.toString();
    }

    @Override
    protected void performDeepClean() {
        cleanSplitData();
        ((ViewStringStateBooleanSpectrumTableModel) readTable.getModel()).clearData();
        ((ViewStringStateBooleanSpectrumTableModel) writeTable.getModel()).clearData();
    }

    @Override
    public void lightClean() {
        centerPanel.removeAll();
        cleanSplitData();
        ((ViewStringStateBooleanSpectrumTableModel) readTable.getModel()).clearData();
        ((ViewStringStateBooleanSpectrumTableModel) writeTable.getModel()).clearData();
        setLoading();
    }

    @Override
    protected void writeDataInFile(final File toSave) {
        // TODO not yet used
    }

}
