/*
 * Synchrotron Soleil File : ViewStringStateBooleanScalarPanel.java Project :
 * Mambo_CVS Description : Author : SOLEIL Original : 6 mars 2006 Revision:
 * Author: Date: State: Log: ViewStringStateBooleanScalarPanel.java,v
 */
package fr.soleil.mambo.containers.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.mambo.actions.view.ViewCopyAction;
import fr.soleil.mambo.actions.view.ViewEditCopyAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.MamboFormatableTable;
import fr.soleil.mambo.components.renderers.BooleanTableRenderer;
import fr.soleil.mambo.components.renderers.StateTableRenderer;
import fr.soleil.mambo.components.renderers.StringTableRenderer;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.models.ViewStringStateBooleanScalarTableModel;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.tools.Messages;

public class ViewStringStateBooleanScalarPanel extends AbstractViewCleanablePanel {

    private static final long serialVersionUID = 5011689005944604638L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewStringStateBooleanScalarPanel.class);
    private static final Insets GAP_BETWEEN_TABLES = new Insets(20, 5, 0, 5);
    private static final Dimension REF_SIZE = new Dimension(50, 50);
    private static final Insets DEFAULT_INSETS = new Insets(5, 5, 0, 5);
    private static final Insets DEFAULT_GAP = new Insets(0, 0, 0, 5);

    private final Map<String, Integer> indexMap;

    private final JPanel centerPanel;
    private final JScrollPane centerScrollPane;
    private ViewConfigurationAttribute[] attributes;
    private MamboFormatableTable[] viewTables;
    private JScrollPane[] tableScollPanes;
    private JPanel[] titlePanels;
    private JButton[] edit, copy;
    private JLabel[] nameLabels;
    private final Component glue;
    private final Collection<String> attributesToLoad;

    public ViewStringStateBooleanScalarPanel(final ViewConfigurationBean viewConfigurationBean) {
        super(viewConfigurationBean);
        indexMap = new ConcurrentHashMap<>();
        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);
        centerPanel = new JPanel(new GridBagLayout());
        GUIUtilities.setObjectBackground(centerPanel, GUIUtilities.VIEW_COLOR);
        centerScrollPane = new JScrollPane(centerPanel);
        GUIUtilities.setObjectBackground(centerScrollPane, GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(centerScrollPane.getViewport(), GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(centerScrollPane.getHorizontalScrollBar(), GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(centerScrollPane.getVerticalScrollBar(), GUIUtilities.VIEW_COLOR);
        glue = Box.createGlue();
        GUIUtilities.setObjectBackground(glue, GUIUtilities.VIEW_COLOR);
        add(centerScrollPane, BorderLayout.CENTER);
        addLoadingLabel();
        tableScollPanes = new JScrollPane[0];
        titlePanels = new JPanel[0];
        edit = new JButton[0];
        copy = new JButton[0];
        nameLabels = new JLabel[0];
        viewTables = new MamboFormatableTable[0];
        attributesToLoad = new ArrayList<>();
    }

    @Override
    public Collection<String> getAssociatedAttributes() {
        ViewConfigurationAttribute[] attributes = this.attributes;
        int length = (attributes == null ? 0 : attributes.length);
        List<String> names = new ArrayList<>(length);
        for (final ViewConfigurationAttribute attribute : attributes) {
            names.add(attribute.getCompleteName());
        }
        return names;
    }

    @Override
    public int getAttributeCount() {
        int count;
        if (attributes == null) {
            count = 0;
        } else {
            count = attributes.length;
        }
        return count;
    }

    protected void resetAttributesToLoad() {
        attributesToLoad.clear();
        if (attributes != null) {
            for (final ViewConfigurationAttribute attribute : attributes) {
                attributesToLoad.add(attribute.getCompleteName());
            }
        }
    }

    public void setAttributes(final ViewConfigurationAttribute[] stringScalars) {
        int formerLength = (attributes == null ? 0 : attributes.length);
        attributes = stringScalars;
        resetAttributesToLoad();
        configureComponents(formerLength);
    }

    protected void configureComponents(int formerLength) {
        lightClean();
        ViewConfigurationAttribute[] attributes = this.attributes;
        int length = (attributes == null ? 0 : attributes.length);
        int row = 0;
        final double weighty = 1.0d / length;
        if (length != formerLength) {
            for (int i = length; i < formerLength; i++) {
                titlePanels[i].removeAll();
                tableScollPanes[i].removeAll();
            }
            titlePanels = Arrays.copyOf(titlePanels, length);
            edit = Arrays.copyOf(edit, length);
            copy = Arrays.copyOf(copy, length);
            nameLabels = Arrays.copyOf(nameLabels, length);
            viewTables = Arrays.copyOf(viewTables, length);
            tableScollPanes = Arrays.copyOf(tableScollPanes, length);
        }
        for (int i = 0; i < length; i++) {
            indexMap.put(attributes[i].getCompleteName().toLowerCase(), Integer.valueOf(i));
            final GridBagConstraints titleConstraints = new GridBagConstraints();
            titleConstraints.fill = GridBagConstraints.HORIZONTAL;
            titleConstraints.gridx = 0;
            titleConstraints.gridy = row++;
            titleConstraints.weightx = 1;
            titleConstraints.weighty = 0;
            if (i > 0) {
                titleConstraints.insets = GAP_BETWEEN_TABLES;
            } else {
                titleConstraints.insets = DEFAULT_INSETS;
            }
            final GridBagConstraints tableConstraints = new GridBagConstraints();
            tableConstraints.fill = GridBagConstraints.BOTH;
            tableConstraints.gridx = 0;
            tableConstraints.gridy = row++;
            tableConstraints.weightx = 1;
            tableConstraints.weighty = weighty;
            tableConstraints.insets = DEFAULT_INSETS;
            final String editName = Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_EDIT_COPY") + " ("
                    + attributes[i].getName() + ")";
            if (edit[i] == null) {
                edit[i] = new JButton(new ViewEditCopyAction(editName, this, i));
                edit[i].setMargin(CometeUtils.getzInset());
                GUIUtilities.setObjectBackground(edit[i], GUIUtilities.VIEW_COPY_COLOR);
            } else {
                ((ViewEditCopyAction) edit[i].getAction()).putValue(Action.NAME, editName);
                ((ViewEditCopyAction) edit[i].getAction()).putValue(Action.SHORT_DESCRIPTION, editName);
                edit[i].setText(editName);
                edit[i].setToolTipText(editName);
            }
            edit[i].setEnabled(false);
            if (copy[i] == null) {
                copy[i] = new JButton(Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_COPY") + " ("
                        + attributes[i].getName() + ")");
                copy[i].setMargin(CometeUtils.getzInset());
                copy[i].addActionListener(new ViewCopyAction(
                        Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_COPY"), this, i));
                GUIUtilities.setObjectBackground(copy[i], GUIUtilities.VIEW_COPY_COLOR);
            }
            copy[i].setEnabled(false);
            if (nameLabels[i] == null) {
                nameLabels[i] = new JLabel(attributes[i].getCompleteName());
                GUIUtilities.setObjectBackground(nameLabels[i], GUIUtilities.VIEW_COLOR);
            } else {
                nameLabels[i].setText(attributes[i].getCompleteName());
            }
            if (titlePanels[i] == null) {
                titlePanels[i] = new JPanel(new GridBagLayout());
                GUIUtilities.setObjectBackground(titlePanels[i], GUIUtilities.VIEW_COLOR);
                final GridBagConstraints nameConstraints = new GridBagConstraints();
                nameConstraints.fill = GridBagConstraints.NONE;
                nameConstraints.gridx = 0;
                nameConstraints.gridy = 0;
                nameConstraints.weightx = 0;
                nameConstraints.weighty = 0;
                nameConstraints.insets = DEFAULT_GAP;
                titlePanels[i].add(nameLabels[i], nameConstraints);
                final GridBagConstraints copyConstraints = new GridBagConstraints();
                copyConstraints.fill = GridBagConstraints.NONE;
                copyConstraints.gridx = 1;
                copyConstraints.gridy = 0;
                copyConstraints.weightx = 0;
                copyConstraints.weighty = 0;
                copyConstraints.insets = DEFAULT_GAP;
                titlePanels[i].add(copy[i], copyConstraints);
                final GridBagConstraints editConstraints = new GridBagConstraints();
                editConstraints.fill = GridBagConstraints.NONE;
                editConstraints.gridx = 2;
                editConstraints.gridy = 0;
                editConstraints.weightx = 0;
                editConstraints.weighty = 0;
                editConstraints.insets = DEFAULT_GAP;
                titlePanels[i].add(edit[i], editConstraints);
                final GridBagConstraints glueConstraints = new GridBagConstraints();
                glueConstraints.fill = GridBagConstraints.HORIZONTAL;
                glueConstraints.gridx = 3;
                glueConstraints.gridy = 0;
                glueConstraints.weightx = 1;
                glueConstraints.weighty = 0;
                titlePanels[i].add(Box.createGlue(), glueConstraints);
            }
            centerPanel.add(titlePanels[i], titleConstraints);
            if (viewTables[i] == null) {
                viewTables[i] = new MamboFormatableTable(new ViewStringStateBooleanScalarTableModel());
                GUIUtilities.setObjectBackground(viewTables[i], GUIUtilities.VIEW_COLOR);
            }
            try {
                if (attributes[i].getDataType(isHistoric()) == TangoConst.Tango_DEV_STATE) {
                    viewTables[i].setDefaultRenderer(Object.class, new StateTableRenderer());
                } else if (attributes[i].getDataType(isHistoric()) == TangoConst.Tango_DEV_STRING) {
                    viewTables[i].setDefaultRenderer(Object.class, new StringTableRenderer());
                } else {
                    viewTables[i].setDefaultRenderer(Object.class, new BooleanTableRenderer());
                }
            } catch (ArchivingException e) {
                LOGGER.error(e.getMessage(), e);
                viewTables[i].setDefaultRenderer(Object.class, new BooleanTableRenderer());
            }
            if (tableScollPanes[i] == null) {
                tableScollPanes[i] = new JScrollPane(viewTables[i]);
                tableScollPanes[i].setMinimumSize(REF_SIZE);
                tableScollPanes[i].setPreferredSize(REF_SIZE);
                GUIUtilities.setObjectBackground(tableScollPanes[i], GUIUtilities.VIEW_COLOR);
                GUIUtilities.setObjectBackground(tableScollPanes[i].getViewport(), GUIUtilities.VIEW_COLOR);
                GUIUtilities.setObjectBackground(tableScollPanes[i].getHorizontalScrollBar(), GUIUtilities.VIEW_COLOR);
                GUIUtilities.setObjectBackground(tableScollPanes[i].getVerticalScrollBar(), GUIUtilities.VIEW_COLOR);
            }
            centerPanel.add(tableScollPanes[i], tableConstraints);
        }
        if (length == 0) {
            final GridBagConstraints glueConstraints = new GridBagConstraints();
            glueConstraints.fill = GridBagConstraints.BOTH;
            glueConstraints.gridx = 0;
            glueConstraints.gridy = 0;
            glueConstraints.weightx = 1;
            glueConstraints.weighty = 1;
            centerPanel.add(glue, glueConstraints);
        }
    }

    public String getSelectedToString(final int tableIndex) {
        StringBuilder s = new StringBuilder();
        if (viewTables != null && tableIndex < viewTables.length) {
            s.append(nameLabels[tableIndex].getText());
            s.append("\n");
            for (int i = 0; i < viewTables[tableIndex].getColumnCount(); i++) {
                s.append(viewTables[tableIndex].getColumnName(i))
                        .append(Options.getInstance().getGeneralOptions().getSeparator());
            }
            int index = s.lastIndexOf(Options.getInstance().getGeneralOptions().getSeparator());
            if (index > -1) {
                s.delete(index, s.length());
            }
            s.append("\n");
            viewTables[tableIndex].appendStringValue(s);
        }
        return s.toString();
    }

    @Override
    protected void performDeepClean() {
        centerPanel.removeAll();
        indexMap.clear();
        if (titlePanels != null) {
            for (final JPanel titlePanel : titlePanels) {
                titlePanel.removeAll();
            }
            titlePanels = null;
        }
        if (attributes != null) {
            for (int i = 0; i < attributes.length; i++) {
                attributes[i] = null;
            }
            attributes = null;
        }
        if (tableScollPanes != null) {
            for (final JScrollPane scrollPane : tableScollPanes) {
                scrollPane.removeAll();
            }
            tableScollPanes = null;
        }
        if (viewTables != null) {
            for (int i = 0; i < viewTables.length; i++) {
                if (viewTables[i] != null) {
                    if (viewTables[i].getModel() != null
                            && viewTables[i].getModel() instanceof ViewStringStateBooleanScalarTableModel) {
                        ((ViewStringStateBooleanScalarTableModel) viewTables[i].getModel()).clearData();
                    }
                    viewTables[i] = null;
                }
            }
            viewTables = null;
        }
        if (nameLabels != null) {
            for (int i = 0; i < nameLabels.length; i++) {
                nameLabels[i] = null;
            }
            nameLabels = null;
        }
        if (edit != null) {
            for (int i = 0; i < edit.length; i++) {
                edit[i] = null;
            }
            edit = null;
        }
        if (copy != null) {
            for (int i = 0; i < copy.length; i++) {
                copy[i] = null;
            }
            copy = null;
        }
    }

    @Override
    public void applyDataOutsideOfEDT(String attributeName, DbData... recoveredSplitData) {
        if (attributeName != null) {
            attributeName = attributeName.toLowerCase();
            Integer index = indexMap.get(attributeName);
            if (index != null) {
                ((ViewStringStateBooleanScalarTableModel) viewTables[index.intValue()].getModel())
                        .setDataOutsideOfEDT(recoveredSplitData);
            }
        }
    }

    @Override
    public void updateFromDataInEDT(String attributeName) {
        if (attributeName != null) {
            attributeName = attributeName.toLowerCase();
            Integer index = indexMap.get(attributeName);
            if (index != null) {
                int i = index.intValue();
                edit[i].setEnabled(true);
                copy[i].setEnabled(true);
                ((ViewStringStateBooleanScalarTableModel) viewTables[index.intValue()].getModel())
                        .fireTableStructureChanged();
            }
            attributesToLoad.remove(attributeName);
            if (attributesToLoad.isEmpty()) {
                setLoaded();
            }
        }
    }

    @Override
    public void applyData(String attributeName, DbData... recoveredSplitData) {
        if (attributeName != null) {
            attributeName = attributeName.toLowerCase();
            Integer index = indexMap.get(attributeName);
            if (index != null) {
                int i = index.intValue();
                edit[i].setEnabled(true);
                copy[i].setEnabled(true);
                ((ViewStringStateBooleanScalarTableModel) viewTables[i].getModel()).setData(recoveredSplitData);
            }
            attributesToLoad.remove(attributeName);
            if (attributesToLoad.isEmpty()) {
                setLoaded();
            }
        }
    }

    @Override
    public String getName() {
        return Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_TITLE");
    }

    @Override
    public String getFullName() {
        return Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_STRING_AND_STATE_TITLE");
    }

    @Override
    public void lightClean() {
        setLoading();
        resetAttributesToLoad();
        indexMap.clear();
        centerPanel.removeAll();
        MamboFormatableTable[] toClean = viewTables;
        for (final MamboFormatableTable viewTable : toClean) {
            ((ViewStringStateBooleanScalarTableModel) viewTable.getModel()).clearData();
        }
    }

}
