package fr.soleil.mambo.containers.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.AttrWriteType;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.containers.sub.dialogs.options.OptionsVCTab;
import fr.soleil.mambo.data.view.ExpressionAttribute;
import fr.soleil.mambo.data.view.MamboViewAdapterFactory;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.data.view.ViewConfigurationData;
import fr.soleil.mambo.data.view.ViewScalarAdapter;
import fr.soleil.mambo.datasources.db.extracting.ExtractingManagerFactory;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;
import fr.soleil.mambo.tools.Messages;

public class ViewNumberScalarPanel extends AbstractViewCleanablePanel implements IChartViewerListener {

    private static final long serialVersionUID = -5567557921412392651L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewNumberScalarPanel.class);

    protected Chart chart;
    protected JLabel empty;
    private Box topBox;

    private JTextField firstField;
    private JTextField secondField;
    private JTextField diffField;

    private double[] firstPoint;
    private double[] secondPoint;
    private boolean clickForFirst;
    private Box diffBox;

    private JCheckBox subtractMean;
    private final Collection<String> attributes;
    private final Collection<String> attributesToLoad;
    private final Map<String, DbData[]> dataMap;

    public ViewNumberScalarPanel(final Chart chart, final ViewConfigurationBean viewConfigurationBean) {
        super(viewConfigurationBean);
        this.chart = chart;
        firstPoint = null;
        secondPoint = null;
        clickForFirst = true;
        attributes = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        attributesToLoad = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        dataMap = new ConcurrentHashMap<>();
        initComponents();
        layoutComponents();
    }

    @Override
    public Collection<String> getAssociatedAttributes() {
        return new ArrayList<>(attributes);
    }

    @Override
    public int getAttributeCount() {
        int count;
        if (attributes == null) {
            count = 0;
        } else {
            count = attributes.size();
        }
        return count;
    }

    public void setAttributes(String... attributes) {
        lightClean();
        if (attributes != null) {
            for (String attribute : attributes) {
                attribute = attribute.toLowerCase();
                this.attributes.add(attribute);
                attributesToLoad.add(attribute);
            }
        }
        add(this.attributes.isEmpty() ? empty : chart, BorderLayout.CENTER);
    }

    @Override
    public void applyDataOutsideOfEDT(String attributeName, DbData... recoveredSplitData) {
        final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
        final ViewConfiguration vc = getSelectedVc();
        if (attributeName != null) {
            attributeName = attributeName.toLowerCase();
        }
        try {
            if ((vc != null) && (extractingManager != null) && (attributeName != null)
                    && attributes.contains(attributeName.toLowerCase())) {
                // Chart data and configuration can be done outside of EDT
                final String idViewSelected = vc.getCurrentId();
                if (idViewSelected != null) {
                    MamboViewAdapterFactory daoFactory = MamboViewAdapterFactory.INSTANCE;
                    ViewScalarAdapter adapter = daoFactory.getDAOScalar(idViewSelected, chart);
                    ViewConfigurationAttribute attr = vc.getAttribute(attributeName);
                    if ((adapter != null) && (attr != null)) {
                        if (recoveredSplitData == null) {
                            dataMap.remove(attributeName);
                        } else {
                            dataMap.put(attributeName, recoveredSplitData);
                        }
                        attr.prepareView(chart, recoveredSplitData);
                        updateFormat(attributeName);
                    }
                }
            }
        } catch (final OutOfMemoryError oome) {
            if (!isCanceled(extractingManager)) {
                outOfMemoryErrorManagement();
            }
        } catch (final Exception e) {
            if (!isCanceled(extractingManager)) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void updateFromDataInEDT(String attributeName) {
        if (!isCleaning() && !canceled) {
            final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
            final ViewConfiguration vc = getSelectedVc();
            if (attributeName != null) {
                attributeName = attributeName.toLowerCase();
            }
            try {
                if ((vc != null) && (extractingManager != null) && (attributeName != null)
                        && attributes.contains(attributeName)) {
                    final String idViewSelected = vc.getCurrentId();
                    if (idViewSelected != null) {
                        MamboViewAdapterFactory daoFactory = MamboViewAdapterFactory.INSTANCE;
                        ViewScalarAdapter adapter = daoFactory.getDAOScalar(idViewSelected, chart);
                        // XXX unfortunately, for now, chart data must be set in EDT
                        adapter.addData(extractingManager.isShowRead(), extractingManager.isShowWrite(),
                                dataMap.get(attributeName));
                        attributesToLoad.remove(attributeName);
                        if (attributesToLoad.isEmpty()) {
                            setLoaded();
                        }
                    }
                }
                setLoaded();
            } catch (final OutOfMemoryError oome) {
                if (!isCanceled(extractingManager)) {
                    outOfMemoryErrorManagement();
                }
            } catch (final Exception e) {
                if (!isCanceled(extractingManager)) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    public void applyData(String attributeName, DbData... recoveredSplitData) {
        final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
        final ViewConfiguration vc = getSelectedVc();
        if (attributeName != null) {
            attributeName = attributeName.toLowerCase();
        }
        try {
            if ((vc != null) && (extractingManager != null) && (attributeName != null)
                    && attributes.contains(attributeName)) {
                final String idViewSelected = vc.getCurrentId();
                if (idViewSelected != null) {
                    MamboViewAdapterFactory daoFactory = MamboViewAdapterFactory.INSTANCE;
                    ViewScalarAdapter adapter = daoFactory.getDAOScalar(idViewSelected, chart);
                    ViewConfigurationAttribute attr = vc.getAttribute(attributeName);
                    if ((adapter != null) && (attr != null)) {
                        if (recoveredSplitData == null) {
                            dataMap.remove(attributeName);
                        } else {
                            dataMap.put(attributeName, recoveredSplitData);
                        }
                        attr.prepareView(chart, recoveredSplitData);
                        adapter.addData(extractingManager.isShowRead(), extractingManager.isShowWrite(),
                                recoveredSplitData);
                        updateFormat(attributeName);
                        attributesToLoad.remove(attributeName);
                        if (attributesToLoad.isEmpty()) {
                            setLoaded();
                        }
                    }
                }
            }
        } catch (final OutOfMemoryError oome) {
            if (!isCanceled(extractingManager)) {
                outOfMemoryErrorManagement();
            }
        } catch (final Exception e) {
            if (!isCanceled(extractingManager)) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    private void initComponents() {
        firstField = new JTextField();
        firstField.setEditable(false);

        secondField = new JTextField();
        secondField.setEditable(false);

        diffField = new JTextField();
        diffField.setEditable(false);

        subtractMean = new JCheckBox(Messages.getMessage("VIEW_ATTRIBUTES_SUBSTRACT_MEAN"));
        GUIUtilities.setObjectBackground(subtractMean, GUIUtilities.VIEW_COLOR);
        subtractMean.setSelected(false);
        subtractMean.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                if (chart != null) {
                    final ViewConfigurationBean viewConfigurationBean = getViewConfigurationBean();
                    final MamboViewAdapterFactory daoFactory = MamboViewAdapterFactory.INSTANCE;
                    final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
                    if ((daoFactory != null) && (extractingManager != null) && (viewConfigurationBean != null)
                            && (viewConfigurationBean.getViewConfiguration() != null)) {
                        final String idViewSelected = viewConfigurationBean.getViewConfiguration().getCurrentId();
                        // Finally get the DAO!
                        final ViewScalarAdapter adapter = daoFactory.getDAOScalar(idViewSelected, chart);
                        if (adapter != null) {
                            // changes dao's mode
                            adapter.setSubtractMean(subtractMean.isSelected());
                            for (final DbData[] splitData : dataMap.values()) {
                                adapter.addData(extractingManager.isShowRead(), extractingManager.isShowWrite(),
                                        splitData);
                            }
                        }
                    }
                }
            }
        });
        empty = new JLabel(Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA"), SwingConstants.CENTER);
        empty.setOpaque(false);
    }

    private void layoutComponents() {
        topBox = Box.createHorizontalBox();
        topBox.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
        topBox.add(loadingLabel);
        topBox.add(Box.createHorizontalGlue());
        topBox.add(subtractMean);

        JLabel firstLabel = new JLabel(Messages.getMessage("VIEW_ATTRIBUTES_FIRST_POINT"));
        firstLabel.setLabelFor(firstField);
        JLabel secondLabel = new JLabel(Messages.getMessage("VIEW_ATTRIBUTES_SECOND_POINT"));
        secondLabel.setLabelFor(secondField);
        JLabel diffLabel = new JLabel(Messages.getMessage("VIEW_ATTRIBUTES_DIFF"));
        diffLabel.setLabelFor(diffField);

        diffBox = Box.createHorizontalBox();
        diffBox.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        diffBox.add(firstLabel);
        diffBox.add(Box.createHorizontalStrut(5));
        diffBox.add(firstField);
        diffBox.add(Box.createHorizontalStrut(10));
        diffBox.add(Box.createHorizontalGlue());
        diffBox.add(secondLabel);
        diffBox.add(Box.createHorizontalStrut(5));
        diffBox.add(secondField);
        diffBox.add(Box.createHorizontalStrut(10));
        diffBox.add(Box.createHorizontalGlue());
        diffBox.add(diffLabel);
        diffBox.add(Box.createHorizontalStrut(5));
        diffBox.add(diffField);

        final String msg = getFullName();
        final TitledBorder border = BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg, TitledBorder.CENTER, TitledBorder.TOP);
        setBorder(border);
        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);

        add(topBox, BorderLayout.NORTH);
        add(empty, BorderLayout.CENTER);
        add(diffBox, BorderLayout.SOUTH);
    }

    @Override
    public String getName() {
        return Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_TITLE");
    }

    @Override
    public String getFullName() {
        return Messages.getMessage("DIALOGS_VIEW_TAB_SCALAR_TITLE");
    }

    protected void clearDataMap() {
        for (DbData[] dataArray : dataMap.values()) {
            if (dataArray != null) {
                Arrays.fill(dataArray, null);
            }
        }
        dataMap.clear();
    }

    @Override
    protected void performDeepClean() {
        clearDataMap();
        if (chart != null) {
            // TODO maybe find a better/safer way to deep clean
            final MamboViewAdapterFactory daoFactory = MamboViewAdapterFactory.INSTANCE;
            final ViewConfiguration vc = getSelectedVc();
            if ((daoFactory != null) && (vc != null)) {
                final String idViewSelected = vc.getCurrentId();
                daoFactory.removeKeyForDaoScalar(idViewSelected);
            }
            chart.removeChartViewerListener(this);
            chart.resetAll();
        }
        attributes.clear();
        attributesToLoad.clear();
        chart = null;
        firstPoint = null;
        secondPoint = null;
    }

    @Override
    public void lightClean() {
        setLoading();
        clearDataMap();
        attributes.clear();
        attributesToLoad.clear();
        remove(empty);
        remove(chart);
        chart.removeChartViewerListener(this);
        chart.resetAll();
        removeCenterComponent();
        diffBox.setVisible(false);
        resetFields();
        revalidate();
        repaint();
    }

    protected void removeCenterComponent() {
        Component[] comps = getComponents();
        if (comps != null) {
            for (Component comp : comps) {
                if ((comp != topBox) && (comp != diffBox)) {
                    remove(comp);
                }
            }
        }
    }

    public void setChartAttributeExpressionOfString(final Map<String, DbData[]> dataMap, final Chart chart)
            throws ArchivingException {
        final ViewConfigurationBean viewConfigurationBean = getViewConfigurationBean();
        if ((viewConfigurationBean != null) && (viewConfigurationBean.getViewConfiguration() != null)) {
            viewConfigurationBean.getViewConfiguration().setChartAttributeExpressionOfString(dataMap, chart);
        }
    }

    private void configurationChanged(final String id) {
        if (id != null) {
            if (id.startsWith(IChartViewer.CHART_PROPERTY_ACTION_NAME)) {
                // Chart properties changed
                if (chart != null) {
                    final ChartProperties newChartProperties = chart.getChartProperties();
                    final String title = newChartProperties.getTitle();
                    if (title != null) {
                        if (title.contains(" - " + Messages.getMessage("VIEW_ATTRIBUTES_START_DATE"))
                                && title.contains(Messages.getMessage("VIEW_ATTRIBUTES_END_DATE"))) {
                            final int index = title.indexOf(" - " + Messages.getMessage("VIEW_ATTRIBUTES_START_DATE"));
                            final String newTitle = title.substring(0, index);
                            newChartProperties.setTitle(newTitle);
                        } else if (title.contains(Messages.getMessage("VIEW_ATTRIBUTES_START_DATE"))
                                && title.contains(Messages.getMessage("VIEW_ATTRIBUTES_END_DATE"))) {
                            final int index = title.indexOf(Messages.getMessage("VIEW_ATTRIBUTES_START_DATE"));

                            final String newTitle = title.substring(0, index);
                            newChartProperties.setTitle(newTitle);
                        }
                    }
                    final ViewConfigurationBean viewConfigurationBean = getViewConfigurationBean();
                    if ((viewConfigurationBean != null) && (viewConfigurationBean.getViewConfiguration() != null)) {
                        final ViewConfigurationData data = viewConfigurationBean.getViewConfiguration().getData();
                        data.setChartProperties(newChartProperties);
                    }
                } // end if (chart != null)
            } // end if (id.startsWith(IChartViewer.CHART_PROPERTY_ACTION_NAME))
            else if (chart != null) {
                // plot properties changed

                String nameIdPlot = id.replaceFirst(IChartViewer.DATAVIEW_PROPERTY_ACTION_NAME,
                        ObjectUtils.EMPTY_STRING);

                final PlotProperties newPlotProperties = chart.getDataViewPlotProperties(nameIdPlot);
                if (newPlotProperties != null && newPlotProperties.getCurve() != null
                        && newPlotProperties.getCurve().getName() != null) {
                    boolean isWrite = false;
                    if (nameIdPlot.endsWith(ViewConfigurationAttribute.SUFFIX_READ)) {
                        nameIdPlot = nameIdPlot.substring(0,
                                nameIdPlot.length() - ViewConfigurationAttribute.SUFFIX_READ.length());
                    } else if (nameIdPlot.endsWith(ViewConfigurationAttribute.SUFFIX_WRITE)) {
                        nameIdPlot = nameIdPlot.substring(0,
                                nameIdPlot.length() - ViewConfigurationAttribute.SUFFIX_WRITE.length());
                        isWrite = true;
                    }
                    final ViewConfigurationBean viewConfigurationBean = getViewConfigurationBean();
                    if ((viewConfigurationBean != null) && (viewConfigurationBean.getViewConfiguration() != null)) {
                        final ViewConfigurationAttribute attr = viewConfigurationBean.getViewConfiguration()
                                .getAttributes().getAttribute(nameIdPlot);
                        if (attr != null) {
                            // If it is an attribute
                            final ViewConfigurationAttributePlotProperties attrProperties = attr.getProperties()
                                    .getPlotProperties();
                            // newPlotProperties.getCurve().setName(attrProperties.getCurve().getName());
                            int writable = 0;
                            try {
                                writable = attr.getDataWritable(true);
                            } catch (ArchivingException e) {
                                LOGGER.error(e.toString());
                            }
                            switch (writable) {
                                case AttrWriteType._READ:
                                case AttrWriteType._WRITE:
                                    attrProperties.setPlotProperties(newPlotProperties);
                                    break;
                                case AttrWriteType._READ_WITH_WRITE:
                                case AttrWriteType._READ_WRITE:
                                    // no change if it is in writing for an
                                    // attribute of
                                    // type READ / WRITE
                                    if (!isWrite) {
                                        attrProperties.setPlotProperties(newPlotProperties);
                                    }
                                    break;
                            }
                        } // end if (attr != null)
                        else {
                            // if it is an expression
                            final TreeMap<String, ExpressionAttribute> expressions = viewConfigurationBean
                                    .getViewConfiguration().getExpressions();

                            final Set<String> keys = expressions.keySet();
                            final Iterator<String> iterator = keys.iterator();
                            while (iterator.hasNext()) {
                                final String key = iterator.next();
                                final ExpressionAttribute currentAttribut = expressions.get(key);
                                if (currentAttribut.getId().equals(nameIdPlot)) {
                                    currentAttribut.getProperties().setPlotProperties(newPlotProperties);
                                    break;
                                }
                            }
                        } // end end if (attr != null)...else
                    }
                } // end if ((newPlotProperties != null) &&
                  // (newPlotProperties.getCurve() !=
                  // null) && (newPlotProperties.getCurve().getName() !=
                  // null))
            } // end if (chart != null)
        } // end if (id != null)
    }

    private void selectedPointChanged(final double[] point) {
        if (point == null) {
            resetFields();
        } else if (!Double.isNaN(point[1])) {
            // NaN can happen if no point is visible on the graph
            if (clickForFirst) {
                firstPoint = point;
                secondPoint = null;
            } else {
                secondPoint = point;
            }
            updateFields();

            clickForFirst = !clickForFirst;
        }
    }

    private void resetFields() {
        firstPoint = null;
        secondPoint = null;
        clickForFirst = true;

        updateFields();
    }

    private void updateFields() {
        if (firstPoint != null) {
            firstField.setText(String.valueOf(firstPoint[1]));
        } else {
            firstField.setText(ObjectUtils.EMPTY_STRING);
        }

        if (secondPoint != null) {
            secondField.setText(String.valueOf(secondPoint[1]));
        } else {
            secondField.setText(ObjectUtils.EMPTY_STRING);
        }

        if (firstPoint != null && secondPoint != null) {
            diffField.setText(String.valueOf(firstPoint[1] - secondPoint[1]));
        } else {
            diffField.setText(ObjectUtils.EMPTY_STRING);
        }
    }

    private void setFormatToDataView(String key, String format) {
        DataView dv = chart.getDataViewList().getDataView(key, false);
        if (dv != null) {
            dv.setFormat(format);
        }
    }

    private void updateFormat(String attributeName) {
        OptionsVCTab vcTab = OptionsVCTab.getInstance();
        if ((vcTab != null) && (chart != null)) {
            String format = vcTab.getDataFormat();
            setFormatToDataView(attributeName + ViewConfigurationAttribute.SUFFIX_WRITE, format);
            setFormatToDataView(attributeName + ViewConfigurationAttribute.SUFFIX_READ, format);
        }
    }

    @Override
    public void chartViewerChanged(ChartViewerEvent event) {
        switch (event.getReason()) {
            case SELECTION:
                selectedPointChanged(event.getSelectedPoint());
                break;
            case CONFIGURATION:
                configurationChanged(event.getViewId());
                break;
            default:
                // nothing to do
                break;
        }
    }

}
