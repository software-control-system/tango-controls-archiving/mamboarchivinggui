package fr.soleil.mambo.containers.view.dialogs;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.actions.view.DynamicDateRangeCheckBoxAction;
import fr.soleil.mambo.actions.view.listeners.DateRangeComboBoxInnerListener;
import fr.soleil.mambo.actions.view.listeners.IDateRangeBoxListener;
import fr.soleil.mambo.data.view.ViewConfigurationData;
import fr.soleil.mambo.event.DateRangeBoxEvent;
import fr.soleil.mambo.tools.Messages;

public class DateRangeBox extends JPanel {

    private static final long serialVersionUID = -1858417371454113833L;

    private static final Insets DEFAULT_INSETS = new Insets(5, 5, 5, 5);

    private JComboBox<String> dateRangeComboBox;
    protected DateRangeComboBoxInnerListener dateRangeComboBoxListener;
    private JLabel dateRangeLabel;
    private JCheckBox dynamicDateRangeCheckBox;
    private JLabel dynamicDateRangeLabel;
    private JTextField endDateField;
    private JLabel endDateLabel;
    private final Set<IDateRangeBoxListener> listeners;

    private boolean longTerm = false;

    private JTextField startDateField;
    private JLabel startDateLabel;

    public DateRangeBox() {
        super(new GridBagLayout());
        listeners = Collections.newSetFromMap(new WeakHashMap<>());
        initComponents();
        addComponents();
    }

    private void addComponents() {
        final GridBagConstraints dateRangeLabelConstraints = new GridBagConstraints();
        dateRangeLabelConstraints.fill = GridBagConstraints.BOTH;
        dateRangeLabelConstraints.gridx = 0;
        dateRangeLabelConstraints.gridy = 0;
        dateRangeLabelConstraints.weightx = 0;
        dateRangeLabelConstraints.weighty = 0;
        dateRangeLabelConstraints.insets = DEFAULT_INSETS;
        final GridBagConstraints dateRangeComboBoxConstraints = new GridBagConstraints();
        dateRangeComboBoxConstraints.fill = GridBagConstraints.BOTH;
        dateRangeComboBoxConstraints.gridx = 1;
        dateRangeComboBoxConstraints.gridy = 0;
        dateRangeComboBoxConstraints.weightx = 1;
        dateRangeComboBoxConstraints.weighty = 0;
        dateRangeComboBoxConstraints.insets = DEFAULT_INSETS;

        final GridBagConstraints startDateLabelConstraints = new GridBagConstraints();
        startDateLabelConstraints.fill = GridBagConstraints.BOTH;
        startDateLabelConstraints.gridx = 0;
        startDateLabelConstraints.gridy = 1;
        startDateLabelConstraints.weightx = 0;
        startDateLabelConstraints.weighty = 0;
        startDateLabelConstraints.insets = DEFAULT_INSETS;
        final GridBagConstraints startDateFieldConstraints = new GridBagConstraints();
        startDateFieldConstraints.fill = GridBagConstraints.BOTH;
        startDateFieldConstraints.gridx = 1;
        startDateFieldConstraints.gridy = 1;
        startDateFieldConstraints.weightx = 1;
        startDateFieldConstraints.weighty = 0;
        startDateFieldConstraints.insets = DEFAULT_INSETS;

        final GridBagConstraints endDateLabelConstraints = new GridBagConstraints();
        endDateLabelConstraints.fill = GridBagConstraints.BOTH;
        endDateLabelConstraints.gridx = 0;
        endDateLabelConstraints.gridy = 2;
        endDateLabelConstraints.weightx = 0;
        endDateLabelConstraints.insets = DEFAULT_INSETS;
        final GridBagConstraints endDateFieldConstraints = new GridBagConstraints();
        endDateFieldConstraints.fill = GridBagConstraints.BOTH;
        endDateFieldConstraints.gridx = 1;
        endDateFieldConstraints.gridy = 2;
        endDateFieldConstraints.weightx = 1;
        endDateLabelConstraints.weighty = 0;
        endDateFieldConstraints.weighty = 0;
        endDateFieldConstraints.insets = DEFAULT_INSETS;

        final GridBagConstraints dynamicDateRangeLabelConstraints = new GridBagConstraints();
        dynamicDateRangeLabelConstraints.fill = GridBagConstraints.BOTH;
        dynamicDateRangeLabelConstraints.gridx = 0;
        dynamicDateRangeLabelConstraints.gridy = 3;
        dynamicDateRangeLabelConstraints.weightx = 0;
        dynamicDateRangeLabelConstraints.weighty = 0;
        dynamicDateRangeLabelConstraints.insets = DEFAULT_INSETS;
        final GridBagConstraints dynamicDateRangeCheckBoxConstraints = new GridBagConstraints();
        dynamicDateRangeCheckBoxConstraints.fill = GridBagConstraints.BOTH;
        dynamicDateRangeCheckBoxConstraints.gridx = 1;
        dynamicDateRangeCheckBoxConstraints.gridy = 3;
        dynamicDateRangeCheckBoxConstraints.weightx = 1;
        dynamicDateRangeCheckBoxConstraints.weighty = 0;
        dynamicDateRangeCheckBoxConstraints.insets = DEFAULT_INSETS;

        add(dateRangeLabel, dateRangeLabelConstraints);
        add(dateRangeComboBox, dateRangeComboBoxConstraints);
        add(startDateLabel, startDateLabelConstraints);
        add(startDateField, startDateFieldConstraints);
        add(endDateLabel, endDateLabelConstraints);
        add(endDateField, endDateFieldConstraints);
        add(dynamicDateRangeLabel, dynamicDateRangeLabelConstraints);
        add(dynamicDateRangeCheckBox, dynamicDateRangeCheckBoxConstraints);
    }

    public void addDateRangeBoxListener(final IDateRangeBoxListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeAllDateRangeBoxListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }

    public void removeDateRangeBoxListener(final IDateRangeBoxListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    private void fireDateRangeBoxEvent() {
        synchronized (listeners) {
            boolean canUpdate = true;
            try {
                getStartDate();
                getEndDate();
            } catch (final IllegalArgumentException e) {
                canUpdate = false;
            }
            if (canUpdate) {
                final DateRangeBoxEvent event = new DateRangeBoxEvent(this);
                List<IDateRangeBoxListener> copy = new ArrayList<IDateRangeBoxListener>();
                synchronized (listeners) {
                    copy.addAll(listeners);
                }
                for (IDateRangeBoxListener listener : copy) {
                    listener.dateRangeBoxChanged(event);
                }
                copy.clear();
            }
        }
    }

    public String getDateRange() {
        String ret = ObjectUtils.EMPTY_STRING;
        ret = (String) dateRangeComboBox.getSelectedItem();
        return ret;
    }

    public JComboBox<String> getDateRangeComboBox() {
        return dateRangeComboBox;
    }

    public DateRangeComboBoxInnerListener getDateRangeComboBoxListener() {
        return dateRangeComboBoxListener;
    }

    public JCheckBox getDynamicDateRangeCheckBox() {
        return dynamicDateRangeCheckBox;
    }

    public Timestamp[] getDynamicStartAndEndDates() {
        final String range = (String) dateRangeComboBox.getSelectedItem();
        return ViewConfigurationData.getDynamicStartAndEndDates(range);
    }

    public Timestamp getEndDate() throws IllegalArgumentException {
        Timestamp ret = null;
        try {
            final String s = endDateField.getText();
            ret = Timestamp.valueOf(s);
        } catch (final Exception e) {
            throw new IllegalArgumentException();
        }
        return ret;
    }

    public JTextField getEndDateField() {
        return endDateField;
    }

    public Timestamp getStartDate() throws IllegalArgumentException {
        Timestamp ret = null;
        try {
            final String s = startDateField.getText();
            ret = Timestamp.valueOf(s);
        } catch (final Exception e) {
            throw new IllegalArgumentException(e);
        }
        return ret;
    }

    public JTextField getStartDateField() {
        return startDateField;
    }

    private void initComponents() {
        String msg = ObjectUtils.EMPTY_STRING;

        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_START_DATE");
        startDateLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_END_DATE");
        endDateLabel = new JLabel(msg);

        final DocumentListener dateListener = new DocumentListener() {

            @Override
            public void changedUpdate(final DocumentEvent e) {
                DateRangeBox.this.fireDateRangeBoxEvent();
            }

            @Override
            public void insertUpdate(final DocumentEvent e) {
                DateRangeBox.this.fireDateRangeBoxEvent();
            }

            @Override
            public void removeUpdate(final DocumentEvent e) {
                DateRangeBox.this.fireDateRangeBoxEvent();
            }

        };

        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DATES_RANGE");
        dateRangeLabel = new JLabel(msg);
        dateRangeComboBox = new JComboBox<>();
        dateRangeComboBoxListener = new DateRangeComboBoxInnerListener(this);
        msg = dateRangeComboBoxListener.getDefaultSelection();
        dateRangeComboBox.addItem(msg);
        msg = dateRangeComboBoxListener.getLast1h();
        dateRangeComboBox.addItem(msg);
        msg = dateRangeComboBoxListener.getLast4h();
        dateRangeComboBox.addItem(msg);
        msg = dateRangeComboBoxListener.getLast8h();
        dateRangeComboBox.addItem(msg);
        msg = dateRangeComboBoxListener.getLast1d();
        dateRangeComboBox.addItem(msg);
        msg = dateRangeComboBoxListener.getLast3d();
        dateRangeComboBox.addItem(msg);
        msg = dateRangeComboBoxListener.getLast7d();
        dateRangeComboBox.addItem(msg);
        msg = dateRangeComboBoxListener.getLast30d();
        dateRangeComboBox.addItem(msg);
        dateRangeComboBox.addActionListener(dateRangeComboBoxListener);
        final Dimension dim = new Dimension(100, 25);
        startDateField = new JTextField();
        startDateField.setText(ObjectUtils.EMPTY_STRING);
        startDateField.getDocument().addDocumentListener(dateListener);
        // startDateField.setPreferredSize(dateRangeComboBox.getPreferredSize());

        startDateField.setMinimumSize(dim);
        endDateField = new JTextField();
        endDateField.setText(ObjectUtils.EMPTY_STRING);
        endDateField.getDocument().addDocumentListener(dateListener);
        // endDateField.setPreferredSize(dateRangeComboBox.getPreferredSize());
        endDateField.setMinimumSize(dim);
        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_BOXES_VC_DATA_RANGE");
        TitledBorder tb;
        tb = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg,
                TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP, GUIUtilities.getTitleFont());
        setBorder(tb);

        dynamicDateRangeLabel = new JLabel(Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DYNAMIC_DATE_RANGE"));

        dynamicDateRangeCheckBox = new JCheckBox();
        dynamicDateRangeCheckBox.setSelected(false);
        dynamicDateRangeCheckBox.addActionListener(new DynamicDateRangeCheckBoxAction(this));

    }

    public boolean isDynamicDateRange() {
        return dynamicDateRangeCheckBox.isSelected();
    }

    public boolean isLongTerm() {
        return longTerm;
    }

    public void setDateRangeComboBox(final JComboBox<String> dateRangeComboBox) {
        this.dateRangeComboBox = dateRangeComboBox;
    }

    public void setDateRangeComboBoxListener(final DateRangeComboBoxInnerListener dateRangeComboBoxListener) {
        this.dateRangeComboBoxListener = dateRangeComboBoxListener;
    }

    public void setDateRangeComboBoxSelectedItem(final String range) {
        dateRangeComboBox.removeActionListener(dateRangeComboBoxListener);
        dateRangeComboBox.setSelectedItem(range);
        dateRangeComboBox.addActionListener(dateRangeComboBoxListener);
        fireDateRangeBoxEvent();
    }

    public void setDynamicDateRangeCheckBox(final JCheckBox dynamicDateRangeCheckBox) {
        this.dynamicDateRangeCheckBox = dynamicDateRangeCheckBox;
    }

    public void setDynamicDateRangeDoClick() {
        dynamicDateRangeCheckBox.doClick();
    }

    public void setEnabledFields(final boolean enabled) {
        dateRangeComboBox.setEnabled(enabled);
        startDateField.setEnabled(enabled);
        endDateField.setEnabled(enabled);
        dynamicDateRangeCheckBox.setEnabled(enabled);
    }

    public void setEndDate(final String endDate) {
        endDateField.setText(endDate);
        endDateField.setCaretPosition(0);
    }

    public void setEndDateField(final JTextField endDateField) {
        this.endDateField = endDateField;
    }

    public void setLongTerm(final boolean longTerm) {
        this.longTerm = longTerm;
    }

    public void setStartAndEndDatesFieldsEnabled(final boolean enabled) {
        startDateField.setEnabled(enabled);
        endDateField.setEnabled(enabled);
        fireDateRangeBoxEvent();
    }

    public void setStartDate(final String startDate) {
        startDateField.setText(startDate);
        startDateField.setCaretPosition(0);
    }

    public void setStartDateField(final JTextField startDateField) {
        this.startDateField = startDateField;
    }

    public void update(final DateRangeBox dateRangeBox) {
        if (dateRangeBox != null) {
            startDateField.setText(dateRangeBox.getStartDateField().getText().trim());
            startDateField.setCaretPosition(0);
            endDateField.setText(dateRangeBox.getEndDateField().getText().trim());
            endDateField.setCaretPosition(0);
            startDateField.setEnabled(dateRangeBox.getStartDateField().isEnabled());
            endDateField.setEnabled(dateRangeBox.getEndDateField().isEnabled());
            dynamicDateRangeCheckBox.setSelected(dateRangeBox.getDynamicDateRangeCheckBox().isSelected());

            dateRangeComboBox.removeActionListener(dateRangeComboBoxListener);
            dateRangeComboBox.setSelectedItem(dateRangeBox.getDateRangeComboBox().getSelectedItem());
            dateRangeComboBox.addActionListener(dateRangeComboBoxListener);
        }
    }

    public void update(final ViewConfigurationData data) {
        update(data, true);
    }

    public void update(final ViewConfigurationData data, boolean calculateDynamicDates) {
        if (data == null) {
            startDateField.setText(ObjectUtils.EMPTY_STRING);
            endDateField.setText(ObjectUtils.EMPTY_STRING);
            startDateField.setEnabled(false);
            endDateField.setEnabled(false);
            dynamicDateRangeCheckBox.setSelected(false);

            dateRangeComboBox.removeActionListener(dateRangeComboBoxListener);
            dateRangeComboBox.setSelectedItem(dateRangeComboBoxListener.getDefaultSelection());
            dateRangeComboBox.addActionListener(dateRangeComboBoxListener);
        } else {
            Timestamp startDate, endDate;
            final boolean dynamicDateRange = data.isDynamicDateRange();
            final boolean longTermDateRange = data.isLongTerm();
            dynamicDateRangeCheckBox.setSelected(dynamicDateRange);
            String dateRange = data.getDateRange();
            if (dateRange == null) {
                dateRange = dateRangeComboBoxListener.getDefaultSelection();
            }
            dateRangeComboBox.removeActionListener(dateRangeComboBoxListener);
            dateRangeComboBox.setSelectedItem(dateRange);
            dateRangeComboBox.addActionListener(dateRangeComboBoxListener);
            if (dynamicDateRange) {
                final Timestamp[] dynamic = data.getDynamicStartAndEndDates();
                startDate = dynamic[0];
                endDate = dynamic[1];
            } else {
                startDate = data.getStartDate();
                endDate = data.getEndDate();
            }
            if (startDate == null) {
                startDateField.setText(ObjectUtils.EMPTY_STRING);
            } else {
                startDateField.setText(startDate.toString());
                startDateField.setCaretPosition(0);
            }
            if (endDate == null) {
                endDateField.setText(ObjectUtils.EMPTY_STRING);
            } else {
                endDateField.setText(endDate.toString());
                endDateField.setCaretPosition(0);
            }
            if (dynamicDateRange) {
                setEnabledFields(true);
                startDateField.setEnabled(!dynamicDateRange);
                endDateField.setEnabled(!dynamicDateRange);
            } else if (longTermDateRange) {
                setEnabledFields(false);
            }
        }
    }

}
