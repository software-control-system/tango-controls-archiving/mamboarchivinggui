package fr.soleil.mambo.containers.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Arrays;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.view.images.PartialImageDataTable;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.SpringUtilities;

/**
 * @author SOLEIL
 */
public class ViewImagePanel extends AbstractViewCleanablePanel {

    private static final long serialVersionUID = -2855478988915327291L;

    private ViewConfigurationAttribute image;

    private final JLabel startDateField;
    private final JLabel endDateField;
    private final JLabel startDateLabel;
    private final JLabel endDateLabel;
    private final JPanel rowsPanel;
    private final JScrollPane scrollPane;
    private final PartialImageDataTable table;
    private final JLabel noData;
    private final JPanel emptyPanel;
    private final JPanel centerPanel;

    private Collection<ImageData> partialImageData;

    public ViewImagePanel(ViewConfigurationBean viewConfigurationBean) throws ArchivingException {
        super(viewConfigurationBean);
        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);

        centerPanel = new JPanel();
        GUIUtilities.setObjectBackground(centerPanel, GUIUtilities.VIEW_COLOR);
        add(centerPanel, BorderLayout.CENTER);

        noData = new JLabel(Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA"), JLabel.CENTER);
        noData.setVerticalAlignment(JLabel.TOP);
        GUIUtilities.setObjectBackground(noData, GUIUtilities.VIEW_COLOR);
        emptyPanel = new JPanel();
        GUIUtilities.setObjectBackground(emptyPanel, GUIUtilities.VIEW_COLOR);

        startDateField = new JLabel();
        endDateField = new JLabel();

        startDateLabel = new JLabel(Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_START_DATE"));
        endDateLabel = new JLabel(Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_END_DATE"));
        Font newFont = startDateLabel.getFont().deriveFont(Font.ITALIC);
        startDateLabel.setFont(newFont);
        endDateLabel.setFont(newFont);

        rowsPanel = new JPanel();
        GUIUtilities.setObjectBackground(rowsPanel, GUIUtilities.VIEW_COLOR);

        JPanel emptyPanel = new JPanel();
        scrollPane = new JScrollPane(emptyPanel);
        GUIUtilities.setObjectBackground(emptyPanel, GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(scrollPane, GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(scrollPane.getViewport(), GUIUtilities.VIEW_COLOR);
        table = new PartialImageDataTable();

        Box topBox = new Box(BoxLayout.X_AXIS);

        topBox.add(Box.createHorizontalStrut(5));
        topBox.add(startDateLabel);
        topBox.add(Box.createHorizontalStrut(5));
        topBox.add(startDateField);
        topBox.add(Box.createHorizontalStrut(10));
        topBox.add(endDateLabel);
        topBox.add(Box.createHorizontalStrut(5));
        topBox.add(endDateField);
        topBox.add(Box.createHorizontalGlue());
        topBox.setPreferredSize(new Dimension(100, 40));
        topBox.setMaximumSize(new Dimension(Integer.MAX_VALUE, 40));

        centerPanel.add(topBox);

        rowsPanel.add(scrollPane);
        centerPanel.add(rowsPanel);

        addComponents();

        initLayout();
        initBorder();
    }

    public void setImage(ViewConfigurationAttribute image) {
        this.image = image;
        lightClean();
    }

    private void addComponents() {
    }

    public void initLayout() {
        centerPanel.setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(centerPanel, centerPanel.getComponentCount(), 1, 0, 0, 0, 0, true);
        rowsPanel.setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(rowsPanel, rowsPanel.getComponentCount(), 1, 0, 0, 0, 0, false);
    }

    private void initBorder() {
        String msg = getFullName();
        TitledBorder tb = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg,
                TitledBorder.CENTER, TitledBorder.TOP);
        Border border = tb;
        this.setBorder(border);
    }

    @Override
    public void setDateRange(String startDate, String endDate) {
        startDateField.setText(startDate == null ? ObjectUtils.EMPTY_STRING : startDate);
        endDateField.setText(endDate == null ? ObjectUtils.EMPTY_STRING : endDate);
    }

    @Override
    public void applyDataOutsideOfEDT(String attributeName, DbData... recoveredSplitData) {
        // TODO manage ImageData as DbData
    }

    @Override
    public void updateFromDataInEDT(String attributeName) {
        // TODO manage ImageData as DbData
        setLoaded();
    }

    @Override
    public void applyData(String attributeName, DbData... recoveredSplitData) {
        // TODO manage ImageData as DbData
        setLoaded();
    }

    @Override
    public String getName() {
        String name;
        if (image == null) {
            name = ObjectUtils.EMPTY_STRING;
        } else {
            name = image.getName();
        }
        return name;
    }

    @Override
    public String getFullName() {
        String name;
        if (image == null) {
            name = ObjectUtils.EMPTY_STRING;
        } else {
            name = image.getCompleteName();
        }
        return name;
    }

    @Override
    protected void performDeepClean() {
        if (partialImageData != null) {
            partialImageData.clear();
            partialImageData = null;
        }
        image = null;
        scrollPane.setViewportView(null);
        startDateField.setText(ObjectUtils.EMPTY_STRING);
        endDateField.setText(ObjectUtils.EMPTY_STRING);
        table.setData(null);
        table.setImageName(getFullName());
    }

    @Override
    public void lightClean() {
        scrollPane.setViewportView(emptyPanel);
        startDateField.setText(ObjectUtils.EMPTY_STRING);
        endDateField.setText(ObjectUtils.EMPTY_STRING);
        table.setData(null);
        table.setImageName(getFullName());
        setLoading();
    }

    @Override
    public Collection<String> getAssociatedAttributes() {
        return Arrays.asList(getFullName());
    }

    @Override
    public int getAttributeCount() {
        int count;
        if (image == null) {
            count = 0;
        } else {
            count = 1;
        }
        return count;
    }

    @Override
    public int getAttributeWeight() {
        // image weight is estimated at 10
        return 10;
    }
}
