package fr.soleil.mambo.containers.view;

import java.awt.BorderLayout;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.view.SaveSpectrumAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.datasources.db.extracting.ExtractingManagerFactory;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.SpectrumDataWriter;

public abstract class AbstractViewSpectrumPanel extends AbstractViewCleanablePanel {

    private static final long serialVersionUID = 5012006492138386368L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractViewSpectrumPanel.class);
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    protected ViewConfigurationAttribute spectrum;
    protected DbData[] splitData;
    protected final JPanel centerPanel;
    protected String fileDirectory;
    protected JButton saveButton;

    public AbstractViewSpectrumPanel(final ViewConfigurationBean viewConfigurationBean) throws ArchivingException {
        super(viewConfigurationBean);
        centerPanel = new JPanel();
        initCenterPanelLayout();
        GUIUtilities.setObjectBackground(centerPanel, GUIUtilities.VIEW_COLOR);
        fileDirectory = Mambo.getPathToResources();
        splitData = null;
        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);
        spectrum = null;
        saveButton = new JButton(new SaveSpectrumAction(this));
        GUIUtilities.setObjectBackground(saveButton, GUIUtilities.VIEW_COLOR);
        add(centerPanel, BorderLayout.CENTER);
        afterInit();
    }

    protected abstract void initCenterPanelLayout();

    /**
     * Builds or cleans subcomponents, one spectrum attribute is known
     */
    protected void prepareComponents() {
        initBorder();
    }

    /**
     * Applies extracted spectrum data on subcomponents
     */
    protected abstract void updateComponents();

    protected final void afterInit() {
        initBorder();
        addLoadingLabel();
    }

    protected void initBorder() {
        final String msg = getFullName();
        final TitledBorder titledBorder = new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED), msg,
                TitledBorder.CENTER, TitledBorder.TOP);
        this.setBorder(titledBorder);
    }

    public ViewConfigurationAttribute getSpectrum() {
        return spectrum;
    }

    public void setSpectrum(final ViewConfigurationAttribute spectrum) {
        if (!ObjectUtils.sameObject(this.spectrum, spectrum)) {
            this.spectrum = spectrum;
            lightClean();
            prepareComponents();
        }
    }

    @Override
    public void applyDataOutsideOfEDT(String attributeName, DbData... recoveredSplitData) {
        String fullName = getFullName();
        if (((attributeName == null) && (fullName == null))
                || ((attributeName != null) && (fullName != null) && attributeName.equalsIgnoreCase(fullName))) {
            final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
            try {
                splitData = recoveredSplitData;
                if (!isCleaning()) {
                    if ((splitData != null) && (splitData.length == 2)) {
                        // if no data on read, splitedData[0] = null
                        if (!isNullOrEmpty(splitData[0])) {
                            if (!extractingManager.isShowRead()) {
                                splitData[0] = null;
                            }
                        }
                        // if no data on write, splitedData[1] = null
                        if (!isNullOrEmpty(splitData[1])) {
                            if (!extractingManager.isShowWrite()) {
                                splitData[1] = null;
                            }
                        }
                        // if no data on read and write, splitedData = null
                        if (isNullOrEmpty(splitData[0]) && isNullOrEmpty(splitData[1])) {
                            splitData = null;
                        }
                    }
                }
            } catch (final OutOfMemoryError oome) {
                if (!extractingManager.isCanceled(isHistoric()) && !isCleaning()) {
                    outOfMemoryErrorManagement();
                }
            } catch (final Exception e) {
                if (!extractingManager.isCanceled(isHistoric()) && !isCleaning()) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    public void updateFromDataInEDT(String attributeName) {
        final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
        try {
            if (!isCleaning()) {
                updateComponents();
                setLoaded();
                revalidate();
                repaint();
            }
        } catch (final OutOfMemoryError oome) {
            if (!extractingManager.isCanceled(isHistoric()) && !isCleaning()) {
                outOfMemoryErrorManagement();
            }
        } catch (final Exception e) {
            if (!extractingManager.isCanceled(isHistoric()) && !isCleaning()) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void applyData(String attributeName, DbData... recoveredSplitData) {
        if (ObjectUtils.sameObject(attributeName, getFullName())) {
            final IExtractingManager extractingManager = ExtractingManagerFactory.getCurrentImpl();
            try {
                splitData = recoveredSplitData;
                if (!isCleaning()) {
                    if ((splitData != null) && (splitData.length == 2)) {
                        // if no data on read, splitedData[0] = null
                        if (!isNullOrEmpty(splitData[0])) {
                            if (!extractingManager.isShowRead()) {
                                splitData[0] = null;
                            }
                        }
                        // if no data on write, splitedData[1] = null
                        if (!isNullOrEmpty(splitData[1])) {
                            if (!extractingManager.isShowWrite()) {
                                splitData[1] = null;
                            }
                        }
                        // if no data on read and write, splitedData = null
                        if (isNullOrEmpty(splitData[0]) && isNullOrEmpty(splitData[1])) {
                            splitData = null;
                        }
                    }
                    updateComponents();
                    setLoaded();
                    revalidate();
                    repaint();
                }
            } catch (final OutOfMemoryError oome) {
                if (!extractingManager.isCanceled(isHistoric()) && !isCleaning()) {
                    outOfMemoryErrorManagement();
                }
            } catch (final Exception e) {
                if (!extractingManager.isCanceled(isHistoric()) && !isCleaning()) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    public String getName() {
        String name;
        if (spectrum == null) {
            name = ObjectUtils.EMPTY_STRING;
        } else {
            name = spectrum.getName();
        }
        return name;
    }

    @Override
    public String getFullName() {
        String name;
        if (spectrum == null) {
            name = ObjectUtils.EMPTY_STRING;
        } else {
            name = spectrum.getCompleteName();
        }
        return name;
    }

    @Override
    public Collection<String> getAssociatedAttributes() {
        return Arrays.asList(getFullName());
    }

    @Override
    public int getAttributeCount() {
        int count;
        if (spectrum == null) {
            count = 0;
        } else {
            count = 1;
        }
        return count;
    }

    @Override
    public int getAttributeWeight() {
        // spectrum weight is estimated at 5
        return 5;
    }

    public String getFileDirectory() {
        return fileDirectory;
    }

    public void setFileDirectory(final String fileDirectory) {
        this.fileDirectory = fileDirectory;
    }

    public void saveDataToFile(final String path) {
        try {
            if (path != null) {
                final File toSave = new File(path);
                final File directory = toSave.getParentFile();
                if (directory != null) {
                    setFileDirectory(directory.getAbsolutePath());
                    writeDataInFile(toSave);
                }
            }
        } catch (final OutOfMemoryError oome) {
            if (!ExtractingManagerFactory.getCurrentImpl().isCanceled(isHistoric()) && !isCleaning()) {
                outOfMemoryErrorManagement();
            }
        }
    }

    protected void writeDataInFile(final File toSave) {
        int ok = JOptionPane.YES_OPTION;
        if (toSave != null) {
            if (toSave.exists()) {
                ok = JOptionPane.showConfirmDialog(this, Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS"),
                        Messages.getMessage("DIALOGS_FILE_CHOOSER_FILE_EXISTS_TITLE"), JOptionPane.YES_NO_OPTION);
            }
            if (ok == JOptionPane.YES_OPTION) {
                SpectrumDataWriter.writeDataInFile(toSave, getReadData(), getWriteData(), getReadFilter(),
                        getWriteFilter(), getViewSpectrumType(), this);
            }
        }
    }

    protected Collection<Integer> getReadFilter() {
        return null;
    }

    protected Collection<Integer> getWriteFilter() {
        return null;
    }

    public DbData getReadData() {
        DbData result;
        if (splitData == null) {
            result = null;
        } else {
            result = splitData[0];
        }
        return result;
    }

    public DbData getWriteData() {
        DbData result;
        if (splitData == null) {
            result = null;
        } else {
            result = splitData[1];
        }
        return result;
    }

    public int getViewSpectrumType() {
        return ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX;
    }

    protected void cleanSplitData() {
        if (splitData != null) {
            Arrays.fill(splitData, null);
        }
        splitData = null;
    }

    @Override
    public void removeAll() {
        super.removeAll();
        if (centerPanel != null) {
            centerPanel.removeAll();
        }
    }

}
