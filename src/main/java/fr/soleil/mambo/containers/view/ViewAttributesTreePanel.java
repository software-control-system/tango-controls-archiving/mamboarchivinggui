package fr.soleil.mambo.containers.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.view.VCAttributesRecapTreeExpandAllAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.view.VCAttributesRecapTree;
import fr.soleil.mambo.containers.view.dialogs.AveragingOptionsPanel;
import fr.soleil.mambo.containers.view.dialogs.DateRangeBox;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationData;

public class ViewAttributesTreePanel extends JPanel {

    private static final long serialVersionUID = 2655328668362876763L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewAttributesTreePanel.class);

    private static final ImageIcon EXPAND_ALL_ICON = new ImageIcon(Mambo.class.getResource("icons/expand_all.gif"));

    private DateRangeBox dateRangeBox;
    private AveragingOptionsPanel averagingOptionsPanel;
    private JButton expandAllButton;
    private JScrollPane treeScrollPane, dateScrollPane, viewActionScrollPane;
    private VCAttributesRecapTree tree;
    private ViewActionPanel viewActionPanel;

    public ViewAttributesTreePanel(ViewConfigurationBean bean, ViewAttributesPanel attributesPanel) {
        super(new GridBagLayout());
        initComponents(bean, attributesPanel);
        addComponents();
    }

    private void addComponents() {
        GridBagConstraints treeConstraints = new GridBagConstraints();
        treeConstraints.fill = GridBagConstraints.BOTH;
        treeConstraints.gridx = 0;
        treeConstraints.gridy = 0;
        treeConstraints.weightx = 1;
        treeConstraints.weighty = 1;

        GridBagConstraints dateConstraints = new GridBagConstraints();
        dateConstraints.fill = GridBagConstraints.BOTH;
        dateConstraints.gridx = 0;
        dateConstraints.gridy = 1;
        dateConstraints.weightx = 1;
        dateConstraints.weighty = 0;

        GridBagConstraints averagingConstraints = new GridBagConstraints();
        averagingConstraints.fill = GridBagConstraints.HORIZONTAL;
        averagingConstraints.gridx = 0;
        averagingConstraints.gridy = 2;
        averagingConstraints.weightx = 1;
        averagingConstraints.weighty = 0;

        GridBagConstraints actionConstraints = new GridBagConstraints();
        actionConstraints.fill = GridBagConstraints.BOTH;
        actionConstraints.gridx = 0;
        actionConstraints.gridy = 3;
        actionConstraints.weightx = 1;
        actionConstraints.weighty = 0;

        add(treeScrollPane, treeConstraints);
        add(dateScrollPane, dateConstraints);
        add(averagingOptionsPanel, averagingConstraints);
        add(viewActionScrollPane, actionConstraints);
    }

    public DateRangeBox getDateRangeBox() {
        return dateRangeBox;
    }

    public AveragingOptionsPanel getAveragingOptionsPanel() {
        return averagingOptionsPanel;
    }

    public VCAttributesRecapTree getTree() {
        return tree;
    }

    public ViewActionPanel getViewActionPanel() {
        return viewActionPanel;
    }

    protected void setBackground(JScrollPane scrollPane, Color bg) {
        scrollPane.setBackground(bg);
        scrollPane.getViewport().setBackground(bg);
        scrollPane.getHorizontalScrollBar().setBackground(bg);
        scrollPane.getVerticalScrollBar().setBackground(bg);
    }

    /**
     * 19 juil. 2005
     */
    private void initComponents(ViewConfigurationBean viewConfigurationBean, ViewAttributesPanel attributesPanel) {
        tree = new VCAttributesRecapTree(viewConfigurationBean.getValidatedModel(), viewConfigurationBean);

        expandAllButton = new JButton(new VCAttributesRecapTreeExpandAllAction(tree));
        expandAllButton.setIcon(EXPAND_ALL_ICON);
        expandAllButton.setMargin(new Insets(0, 0, 0, 0));
        expandAllButton.setBackground(Color.WHITE);
        expandAllButton.setBorderPainted(false);
        expandAllButton.setFocusable(false);
        expandAllButton.setFocusPainted(false);
        expandAllButton.setHorizontalAlignment(JButton.LEFT);
        expandAllButton.setFont(GUIUtilities.expandButtonFont);
        expandAllButton.setForeground(Color.BLACK);

        Dimension refSize = new Dimension(100, 100);
        treeScrollPane = new JScrollPane(tree);
        setBackground(treeScrollPane, Color.WHITE);
        treeScrollPane.setPreferredSize(refSize);
        treeScrollPane.setMinimumSize(refSize);
        treeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        treeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        treeScrollPane.setColumnHeaderView(expandAllButton);

        dateRangeBox = new DateRangeBox();
        GUIUtilities.setObjectBackground(dateRangeBox, GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(dateRangeBox.getDynamicDateRangeCheckBox(), GUIUtilities.VIEW_COLOR);
        dateScrollPane = new JScrollPane(dateRangeBox);
        dateScrollPane.setMinimumSize(new Dimension(0, dateRangeBox.getPreferredSize().height
                + dateScrollPane.getHorizontalScrollBar().getPreferredSize().height));
        dateScrollPane.setBorder(dateRangeBox.getBorder());
        dateRangeBox.setBorder(null);
        setBackground(dateScrollPane, GUIUtilities.getViewColor());

        averagingOptionsPanel = new AveragingOptionsPanel();
        averagingOptionsPanel.setAveragingConfigurationVisible(false);
        GUIUtilities.setObjectBackground(averagingOptionsPanel, GUIUtilities.VIEW_COLOR);

        ViewConfiguration selectedVC = viewConfigurationBean.getViewConfiguration();
        if (selectedVC != null) {
            ViewConfigurationData vcData = selectedVC.getData();
            if ((vcData != null) && (vcData.getStartDate() != null)) {
                dateRangeBox.setStartDate(vcData.getStartDate().toString().trim());
            } else {
                LOGGER.warn("null VC data or start date!");
            }
        }

        viewActionPanel = new ViewActionPanel(viewConfigurationBean, attributesPanel);
        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);
        dateRangeBox.getDateRangeComboBoxListener()
                .itemStateChanged(new ItemEvent(dateRangeBox.getDateRangeComboBox(), ItemEvent.ITEM_STATE_CHANGED,
                        dateRangeBox.getDateRangeComboBoxListener().getLast1h(), ItemEvent.SELECTED));
        viewActionScrollPane = new JScrollPane(viewActionPanel);
        viewActionScrollPane.setMinimumSize(new Dimension(5, viewActionPanel.getPreferredSize().height
                + viewActionScrollPane.getHorizontalScrollBar().getPreferredSize().height));
        viewActionScrollPane.setBorder(viewActionPanel.getBorder());
        viewActionPanel.setBorder(null);
        setBackground(viewActionScrollPane, GUIUtilities.getViewColor());
    }

}
