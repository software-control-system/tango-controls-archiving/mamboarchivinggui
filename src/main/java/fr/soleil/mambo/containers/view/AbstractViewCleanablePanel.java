package fr.soleil.mambo.containers.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.lang.ref.WeakReference;
import java.util.Collection;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.db.DbConnectionParameters;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.datasources.db.extracting.IExtractingManager;
import fr.soleil.mambo.tools.Messages;

public abstract class AbstractViewCleanablePanel extends JPanel {

    private static final long serialVersionUID = -3460749149022142751L;

    public static final Color LOADING_COLOR = Color.RED;
    public static final Color OK_COLOR = new Color(0x16B84E);
    public static final Color CANCELED_COLOR = new Color(0xFFFF6B);

    public static final Icon LOADING_ICON = new ImageIcon(
            AbstractViewCleanablePanel.class.getResource("/com/famfamfam/silk/hourglass.png"));
    public static final Icon OK_ICON = new ImageIcon(
            AbstractViewCleanablePanel.class.getResource("/com/famfamfam/silk/tick.png"));
    public static final Icon CANCELED_ICON = new ImageIcon(
            AbstractViewCleanablePanel.class.getResource("/com/famfamfam/silk/error.png"));

    public static final Font LOADING_STATUS_FONT = new Font(Font.DIALOG, Font.BOLD, 14);

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractViewCleanablePanel.class);
    protected static final String MEMORY_ERROR = "Memory Error";

    private final WeakReference<ViewConfigurationBean> viewConfigurationBeanRef;
    protected final JLabel loadingLabel;
    protected volatile boolean canceled;
    protected volatile boolean cleaning;

    protected AbstractViewCleanablePanel(ViewConfigurationBean viewConfigurationBean) {
        super(new BorderLayout());
        viewConfigurationBeanRef = (viewConfigurationBean == null ? null
                : new WeakReference<ViewConfigurationBean>(viewConfigurationBean));
        canceled = false;
        cleaning = false;
        loadingLabel = new JLabel(Messages.getMessage("VIEW_ATTRIBUTES_NOT_LOADED"));
        loadingLabel.setFont(LOADING_STATUS_FONT);
        loadingLabel.setForeground(Color.RED);
        loadingLabel.setIcon(LOADING_ICON);
    }

    public void setDateRange(String startDate, String endDate) {
        // nothing to do by default;
    }

    protected ViewConfigurationBean getViewConfigurationBean() {
        return ObjectUtils.recoverObject(viewConfigurationBeanRef);
    }

    protected final ViewConfiguration getSelectedVc(ViewConfigurationBean viewConfigurationBean) {
        ViewConfiguration vc;
        if (viewConfigurationBean == null) {
            vc = null;
        } else {
            vc = viewConfigurationBean.getViewConfiguration();
        }
        return vc;
    }

    protected final ViewConfiguration getSelectedVc() {
        return getSelectedVc(getViewConfigurationBean());
    }

    protected final Boolean isHistoric(ViewConfiguration vc) {
        Boolean historic;
        if (vc == null) {
            historic = DbConnectionParameters.getDefaultDatabase(false);
        } else {
            historic = vc.getData().isHistoric();
        }
        return historic;
    }

    protected final Boolean isHistoric() {
        return isHistoric(getSelectedVc());
    }

    protected String getDisplayFormat(ViewConfigurationAttribute attr) {
        String displayFormat;
        if (attr == null) {
            displayFormat = null;
        } else {
            try {
                displayFormat = attr.getDisplayFormat(isHistoric());
            } catch (Exception e) {
                displayFormat = null;
                LOGGER.error(e.getMessage(), e);
            }
        }
        return displayFormat;
    }

    protected void addLoadingLabel() {
        add(loadingLabel, BorderLayout.NORTH);
    }

    protected boolean isNullOrEmpty(DbData data) {
        boolean nullOrEmpty;
        if (data == null) {
            nullOrEmpty = true;
        } else {
            NullableTimedData[] timedData = data.getTimedData();
            nullOrEmpty = ((timedData == null) || (timedData.length == 0));
        }
        return nullOrEmpty;
    }

    protected abstract void performDeepClean();

    protected final void clean() {
        cleaning = true;
        removeAll();
        performDeepClean();
    }

    public abstract void lightClean();

    @Override
    public abstract String getName();

    public abstract String getFullName();

    public abstract Collection<String> getAssociatedAttributes();

    public abstract int getAttributeCount();

    public int getAttributeWeight() {
        return 1;
    }

    /**
     * Method that can be called outside of EDT to apply extracted data from DB for an attribute.
     * 
     * @param attributeName The concerned attribute.
     * @param recoveredSplitData The extracted data for the attribute.
     */
    public abstract void applyDataOutsideOfEDT(String attributeName, DbData... recoveredSplitData);

    public abstract void updateFromDataInEDT(String attributeName);

    public abstract void applyData(String attributeName, DbData... recoveredSplitData);

    protected final void changeLoadingIcon(Icon icon) {
        loadingLabel.setIcon(icon);
        Container parent = getParent();
        if (parent instanceof JTabbedPane) {
            JTabbedPane tab = (JTabbedPane) parent;
            int index = tab.indexOfComponent(this);
            if (index > -1) {
                tab.setIconAt(index, icon);
            }
        }
    }

    protected final void setLoading() {
        canceled = false;
        loadingLabel.setText(Messages.getMessage("VIEW_ATTRIBUTES_NOT_LOADED"));
        loadingLabel.setForeground(LOADING_COLOR);
        changeLoadingIcon(LOADING_ICON);
    }

    public final void setLoaded() {
        loadingLabel.setText(Messages.getMessage("VIEW_ATTRIBUTES_LOADED"));
        loadingLabel.setToolTipText(Messages.getMessage("VIEW_ATTRIBUTES_LOADED"));
        loadingLabel.setForeground(OK_COLOR);
        changeLoadingIcon(OK_ICON);
    }

    public void setCanceled() {
        if (!Messages.getMessage("VIEW_ATTRIBUTES_LOADED").equals(loadingLabel.getText())) {
            canceled = true;
            String message = Messages.getMessage("VIEW_ATTRIBUTES_CANCELED");
            loadingLabel.setText(message);
            loadingLabel.setToolTipText(message);
            loadingLabel.setForeground(CANCELED_COLOR);
            changeLoadingIcon(CANCELED_ICON);
        }
    }

    public void outOfMemoryErrorManagement() {
        JOptionPane.showMessageDialog(MamboFrame.getInstance(), MEMORY_ERROR,
                Messages.getMessage("DIALOGS_VIEW_MEMORY_ERROR_TITLE"), JOptionPane.ERROR_MESSAGE);
        LOGGER.error(MEMORY_ERROR);
    }

    public boolean isCleaning() {
        return cleaning;
    }

    public boolean isCanceled(IExtractingManager extractingManager) {
        return isCleaning() || canceled || ((extractingManager != null) && extractingManager.isCanceled(isHistoric()));
    }

    @Override
    protected void finalize() throws Throwable {
        clean();
        super.finalize();
    }

}
