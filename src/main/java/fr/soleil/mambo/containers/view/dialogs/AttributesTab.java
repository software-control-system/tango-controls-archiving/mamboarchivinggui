// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/containers/view/dialogs/AttributesTab.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class AttributesTab.
// (Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.10 $
//
// $Log: AttributesTab.java,v $
// Revision 1.10 2007/01/11 14:05:47 ounsy
// Math Expressions Management (warning ! requires atk 2.7.0 or greater)
//
// Revision 1.9 2006/11/06 09:28:05 ounsy
// icons reorganization
//
// Revision 1.8 2006/10/02 14:13:25 ounsy
// minor changes (look and feel)
//
// Revision 1.7 2006/07/28 10:07:12 ounsy
// icons moved to "icons" package
//
// Revision 1.6 2006/07/18 10:26:10 ounsy
// Less time consuming by setting tree expanding on demand only
//
// Revision 1.5 2006/07/05 12:36:38 ounsy
// minor changes
//
// Revision 1.4 2006/06/28 12:28:51 ounsy
// do not expand VCPossibleAttributesTree directly : click on a button instead
// to do so
//
// Revision 1.3 2006/02/24 12:20:05 ounsy
// small modifications
//
// Revision 1.2 2005/11/29 18:27:45 chinkumo
// no message
//
// Revision 1.1.2.2 2005/09/14 15:41:20 chinkumo
// Second commit !
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.containers.view.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.tree.TreePath;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.view.AddSelectedVCAttributesAction;
import fr.soleil.mambo.actions.view.MatchPossibleVCAttributesAction;
import fr.soleil.mambo.actions.view.MatchVCAttributesAction;
import fr.soleil.mambo.actions.view.RemoveSelectedVCAttributesAction;
import fr.soleil.mambo.actions.view.VCAttributesSelectTreeExpandAllAction;
import fr.soleil.mambo.actions.view.VCAttributesSelectTreeExpandOneAction;
import fr.soleil.mambo.actions.view.VCAttributesSelectTreeExpandSelectedAction;
import fr.soleil.mambo.actions.view.VCPossibleAttributesTreeExpandAllAction;
import fr.soleil.mambo.actions.view.VCPossibleAttributesTreeExpandOneAction;
import fr.soleil.mambo.actions.view.VCPossibleAttributesTreeExpandSelectedAction;
import fr.soleil.mambo.actions.view.VCRefreshPossibleAttributesAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.TreeBox;
import fr.soleil.mambo.components.view.VCAttributesPropertiesTree;
import fr.soleil.mambo.components.view.VCAttributesSelectTree;
import fr.soleil.mambo.components.view.VCPossibleAttributesTree;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.SpringUtilities;

public class AttributesTab extends JPanel {

    private static final long serialVersionUID = -2532616834397980895L;

    private static final ImageIcon REFRESH_LEFT_ICON = new ImageIcon(Mambo.class.getResource("icons/update.gif"));
    private static final ImageIcon EXPAND_ALL_LEFT_ICON = new ImageIcon(
            Mambo.class.getResource("icons/expand_all.gif"));
    private static final ImageIcon EXPAND_SELECTED_LEFT_ICON = new ImageIcon(
            Mambo.class.getResource("icons/expand_selected.gif"));
    private static final ImageIcon EXPAND_FIRST_LEFT_ICON = new ImageIcon(Mambo.class.getResource("icons/expand.gif"));
    private static final ImageIcon EXPAND_ALL_RIGHT_ICON = new ImageIcon(
            Mambo.class.getResource("icons/expand_all.gif"));
    private static final ImageIcon EXPAND_SELECTED_RIGHT_ICON = new ImageIcon(
            Mambo.class.getResource("icons/expand_selected.gif"));
    private static final ImageIcon EXPAND_FIRST_RIGTH_ICON = new ImageIcon(Mambo.class.getResource("icons/expand.gif"));
    private static final ImageIcon WARNING_ICON = new ImageIcon(Mambo.class.getResource("icons/warningBig.gif"));

    private JButton addSelectedAttributesButton;
    private JButton removeSelectedAttributesButton;

    private JLabel warningLabel1;
    private JLabel warningLabel2;

    // selected attributes
    private VCAttributesSelectTree rightTree;
    private TreeBox rightTreeBox;
    private JTextField rightRegexpField;
    private JButton rightSearchButton;
    private JButton expandAllRightButton;
    private JButton expandSelectedRightButton;
    private JButton expandFirstRightButton;

    // possible attributes
    private VCPossibleAttributesTree possibleAttributesTree;
    private TreeBox leftTreeBox;
    private JTextField leftRegexpField;
    private JButton leftSearchButton;
    private JButton refreshLeftButton;
    private JButton expandAllLeftButton;
    private JButton expandSelectedLeftButton;
    private JButton expandFirstLeftButton;

    private JPanel transferBox;
    private JPanel treeActionPanel;
    private JPanel leftActionPanel;
    private JPanel rightActionPanel;
    private JPanel box;

    private final ViewConfigurationBean viewConfigurationBean;
    private final VCAttributesPropertiesTree linkedPropertiesTree;
    private final VCEditDialog editDialog;

    public AttributesTab(ViewConfigurationBean viewConfigurationBean, VCEditDialog editDialog,
            VCAttributesPropertiesTree linkedPropertiesTree) {
        super();
        this.viewConfigurationBean = viewConfigurationBean;
        this.editDialog = editDialog;
        this.linkedPropertiesTree = linkedPropertiesTree;
        initComponents();
        initLayout();
    }

    public void setPossibleAttributesSearchEnabled(boolean enabled) {
        leftRegexpField.setEnabled(enabled);
        leftSearchButton.setEnabled(enabled);
    }

    public void setVCAttributesSearchEnabled(boolean enabled) {
        rightRegexpField.setEnabled(enabled);
        rightSearchButton.setEnabled(enabled);
    }

    /**
     * 19 juil. 2005
     */
    private void initComponents() {
        initLeftTreeBox();
        initRightTreeBox();
        initActionComponents();
        initTransferComponents();
    }

    private void initActionComponents() {
        warningLabel1 = new JLabel(Messages.getMessage("VIEW_ACTION_EXPAND_WARNING"), JLabel.CENTER);
        warningLabel1.setIcon(WARNING_ICON);

        refreshLeftButton = new JButton(new VCRefreshPossibleAttributesAction(editDialog));
        refreshLeftButton.setIcon(REFRESH_LEFT_ICON);

        expandAllLeftButton = new JButton(new VCPossibleAttributesTreeExpandAllAction(getPossibleAttributesTree()));
        expandAllLeftButton.setIcon(EXPAND_ALL_LEFT_ICON);
        expandAllLeftButton.setMargin(new Insets(0, 0, 0, 0));

        expandFirstLeftButton = new JButton(new VCPossibleAttributesTreeExpandOneAction(getPossibleAttributesTree()));
        expandFirstLeftButton.setIcon(EXPAND_FIRST_LEFT_ICON);
        expandFirstLeftButton.setMargin(new Insets(0, 0, 0, 0));

        expandSelectedLeftButton = new JButton(
                new VCPossibleAttributesTreeExpandSelectedAction(getPossibleAttributesTree()));
        expandSelectedLeftButton.setIcon(EXPAND_SELECTED_LEFT_ICON);
        expandSelectedLeftButton.setMargin(new Insets(0, 0, 0, 0));

        warningLabel2 = new JLabel(Messages.getMessage("VIEW_ACTION_EXPAND_WARNING"), JLabel.CENTER);
        warningLabel2.setIcon(WARNING_ICON);

        expandAllRightButton = new JButton(new VCAttributesSelectTreeExpandAllAction(getSelectedAttributesTree()));
        expandAllRightButton.setIcon(EXPAND_ALL_RIGHT_ICON);
        expandAllRightButton.setMargin(new Insets(0, 0, 0, 0));

        expandFirstRightButton = new JButton(new VCAttributesSelectTreeExpandOneAction(getSelectedAttributesTree()));
        expandFirstRightButton.setIcon(EXPAND_FIRST_RIGTH_ICON);
        expandFirstRightButton.setMargin(new Insets(0, 0, 0, 0));

        expandSelectedRightButton = new JButton(
                new VCAttributesSelectTreeExpandSelectedAction(getSelectedAttributesTree()));
        expandSelectedRightButton.setIcon(EXPAND_SELECTED_RIGHT_ICON);
        expandSelectedRightButton.setMargin(new Insets(0, 0, 0, 0));
    }

    private void initLeftTreeBox() {
        Boolean historic;
        if ((viewConfigurationBean.getEditingViewConfiguration() == null)
                || (viewConfigurationBean.getEditingViewConfiguration().getData() == null)) {
            historic = viewConfigurationBean.getVcPossibleAttributesTreeModel().isHistoric();
        } else {
            historic = viewConfigurationBean.getEditingViewConfiguration().getData().isHistoric();
        }
        if (!ObjectUtils.sameObject(historic, viewConfigurationBean.getVcPossibleAttributesTreeModel().isHistoric())) {
            viewConfigurationBean.getVcPossibleAttributesTreeModel().setHistoric(historic);
        }
        possibleAttributesTree = new VCPossibleAttributesTree(viewConfigurationBean.getVcPossibleAttributesTreeModel(),
                viewConfigurationBean);

        String msg = Messages.getMessage("ACTIONS_MATCH");
        MatchPossibleVCAttributesAction matchAction = new MatchPossibleVCAttributesAction(msg, viewConfigurationBean,
                this);
        leftRegexpField = new JTextField();
        leftRegexpField.addActionListener(matchAction);
        leftSearchButton = new JButton(matchAction);

        leftRegexpField.setToolTipText(Messages.getMessage("ACTIONS_MATCH_TOOLTIP"));
        leftSearchButton.setToolTipText(Messages.getMessage("ACTIONS_MATCH_TOOLTIP"));

        leftTreeBox = new TreeBox();
        leftTreeBox.build(possibleAttributesTree, leftRegexpField, leftSearchButton);
    }

    private void initRightTreeBox() {
        rightTree = new VCAttributesSelectTree(viewConfigurationBean.getEditingModel(), viewConfigurationBean);

        String msg = Messages.getMessage("ACTIONS_MATCH");
        MatchVCAttributesAction matchAction = new MatchVCAttributesAction(msg, viewConfigurationBean, this);
        rightRegexpField = new JTextField();
        rightRegexpField.addActionListener(matchAction);
        rightSearchButton = new JButton(matchAction);

        rightRegexpField.setToolTipText(Messages.getMessage("ACTIONS_MATCH_TOOLTIP"));
        rightSearchButton.setToolTipText(Messages.getMessage("ACTIONS_MATCH_TOOLTIP"));

        rightTreeBox = new TreeBox();
        rightTreeBox.build(rightTree, rightRegexpField, rightSearchButton);
    }

    private void initTransferComponents() {
        ImageIcon plusIcon = new ImageIcon(Mambo.class.getResource("icons/right.gif"));
        ImageIcon minusIcon = new ImageIcon(Mambo.class.getResource("icons/remove.gif"));

        addSelectedAttributesButton = new JButton(plusIcon);
        String msg = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_ADD_SELECTED_ATTR");
        addSelectedAttributesButton.addActionListener(new AddSelectedVCAttributesAction(msg, viewConfigurationBean,
                getSelectedAttributesTree(), linkedPropertiesTree, getPossibleAttributesTree()));
        addSelectedAttributesButton.setMargin(new Insets(10, 5, 10, 5));

        removeSelectedAttributesButton = new JButton(minusIcon);
        msg = Messages.getMessage("DIALOGS_EDIT_VC_ATTRIBUTES_REMOVE_SELECTED_ATTR");
        removeSelectedAttributesButton
                .addActionListener(new RemoveSelectedVCAttributesAction(msg, viewConfigurationBean, editDialog));
        removeSelectedAttributesButton.setMargin(new Insets(10, 5, 10, 5));
    }

    private void initLayout() {
        // lay out left action panel
        leftActionPanel = new JPanel();
        leftActionPanel.add(warningLabel1);
        leftActionPanel.add(refreshLeftButton);
        leftActionPanel.add(expandAllLeftButton);
        leftActionPanel.add(expandSelectedLeftButton);
        leftActionPanel.add(expandFirstLeftButton);

        leftActionPanel.setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(leftActionPanel, leftActionPanel.getComponentCount(), 1, 0, 0, 0, 5, true);

        // lay out right action panel
        rightActionPanel = new JPanel();
        rightActionPanel.add(warningLabel2);
        rightActionPanel.add(Box.createVerticalGlue());
        rightActionPanel.add(expandAllRightButton);
        rightActionPanel.add(expandSelectedRightButton);
        rightActionPanel.add(expandFirstRightButton);

        rightActionPanel.setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(rightActionPanel, rightActionPanel.getComponentCount(), 1, 0, 0, 0, 5, true);

        // lay out action panel
        treeActionPanel = new JPanel();
        treeActionPanel.setBorder(GUIUtilities.getPlotSubPanelsLineBorder(Messages.getMessage("VIEW_ACTION_BORDER"),
                Color.BLACK, Color.BLACK));
        treeActionPanel.setLayout(new BoxLayout(treeActionPanel, BoxLayout.X_AXIS));

        leftActionPanel.setAlignmentY(TOP_ALIGNMENT);
        rightActionPanel.setAlignmentY(TOP_ALIGNMENT);

        // leftActionPanel's width is shorter, because of button's label.
        // To get actions panels centered with trees, we need actionPanels to
        // have the same size.
        treeActionPanel.add(Box.createHorizontalGlue());
        treeActionPanel.add(leftActionPanel);
        treeActionPanel.add(Box.createHorizontalGlue());
        treeActionPanel.add(Box.createRigidArea(new Dimension(25, 0)));// buttons'
        // width
        // in
        // transferBox
        treeActionPanel.add(Box.createHorizontalGlue());
        treeActionPanel.add(rightActionPanel);
        treeActionPanel.add(Box.createHorizontalGlue());

        // lay out transfertBox
        transferBox = new JPanel();
        transferBox.setLayout(new BoxLayout(transferBox, BoxLayout.Y_AXIS));
        transferBox.add(Box.createVerticalGlue());
        transferBox.add(addSelectedAttributesButton);
        transferBox.add(Box.createRigidArea(new Dimension(0, 20)));
        transferBox.add(removeSelectedAttributesButton);
        transferBox.add(Box.createVerticalGlue());

        // lay out the trees's panel
        box = new JPanel(); // panel containing the 2 TreeBoxes
        box.add(leftTreeBox);
        box.add(transferBox);
        box.add(rightTreeBox);
        box.setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(box, 1, box.getComponentCount(), 2, 0, 2, 0, true);

        // lay out the main panel
        this.setLayout(new BorderLayout(0, 5));
        this.add(treeActionPanel, BorderLayout.NORTH);
        this.add(box, BorderLayout.CENTER);
    }

    /**
     * @param aPath
     *            8 juil. 2005
     */
    public void makeVisible(TreePath aPath) {
        rightTree.makeVisible(aPath);
    }

    /**
     * 8 juil. 2005
     */
    public void enableButtons() {
        addSelectedAttributesButton.setEnabled(true);
        removeSelectedAttributesButton.setEnabled(true);
    }

    public TreeBox getRightTreeBox() {
        return rightTreeBox;
    }

    /**
     * @return 11 juil. 2005
     */
    public TreeBox getLeftTreeBox() {

        return leftTreeBox;
    }

    public String getRightRegexp() {
        return rightRegexpField.getText();
    }

    public String getLeftRegexp() {
        return leftRegexpField.getText();
    }

    public VCPossibleAttributesTree getPossibleAttributesTree() {
        return possibleAttributesTree;
    }

    public VCAttributesSelectTree getSelectedAttributesTree() {
        return rightTree;
    }

}
