package fr.soleil.mambo.containers.view.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.ref.WeakReference;
import java.sql.Timestamp;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.models.VCAttributesTreeModel;
import fr.soleil.mambo.models.VCPossibleAttributesTreeModel;
import fr.soleil.mambo.tools.MamboDbUtils;
import fr.soleil.mambo.tools.Messages;

public class GeneralTab extends JPanel implements ItemListener {

    private static final long serialVersionUID = 7893402002143449964L;

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralTab.class);

    private AveragingOptionsPanel averagingOptionsPanel;
    private ChartGeneralTabbedPane chartGeneralTabbedPane;
    private JLabel creationDateLabel;
    private JLabel creationDateValue;

    private JPanel dataSubPanel;
    private DateRangeBox dateRangeBox;

    private JPanel dbButtonPanel;
    private JRadioButton hdbButton, tdbButton, ttsButton;
    private ButtonGroup dbButtonGroup;

    private JLabel lastUpdateDateLabel;

    private JLabel lastUpdateDateValue;
    private JTextField nameField;

    private JLabel nameLabel;

    private JPanel vcIdentificationPanel;
    private final WeakReference<ViewConfigurationBean> viewConfigurationBeanRef;

    private Boolean historic;

    public GeneralTab(ViewConfigurationBean viewConfigurationBean, VCEditDialog editDialog) {
        super();
        this.viewConfigurationBeanRef = viewConfigurationBean == null ? null
                : new WeakReference<>(viewConfigurationBean);
        historic = Boolean.TRUE;
        initComponents(viewConfigurationBean, editDialog);
        addComponents();
        setLayout();
    }

    /**
     * 19 juil. 2005
     */
    private void addComponents() {
        GridBagConstraints vcIdentificationPanelConstraints = new GridBagConstraints();
        vcIdentificationPanelConstraints.fill = GridBagConstraints.BOTH;
        vcIdentificationPanelConstraints.gridx = 0;
        vcIdentificationPanelConstraints.gridy = 0;
        vcIdentificationPanelConstraints.weightx = 1;
        vcIdentificationPanelConstraints.weighty = 0.33;
        dataSubPanel.add(vcIdentificationPanel, vcIdentificationPanelConstraints);

        GridBagConstraints dateRangeBoxConstraints = new GridBagConstraints();
        dateRangeBoxConstraints.fill = GridBagConstraints.BOTH;
        dateRangeBoxConstraints.gridx = 0;
        dateRangeBoxConstraints.gridy = 1;
        dateRangeBoxConstraints.weightx = 1;
        dateRangeBoxConstraints.weighty = 0.34;
        dataSubPanel.add(dateRangeBox, dateRangeBoxConstraints);

        GridBagConstraints averagingOptionsPanelConstraints = new GridBagConstraints();
        averagingOptionsPanelConstraints.fill = GridBagConstraints.BOTH;
        averagingOptionsPanelConstraints.gridx = 0;
        averagingOptionsPanelConstraints.gridy = 2;
        averagingOptionsPanelConstraints.weightx = 1;
        averagingOptionsPanelConstraints.weighty = 0.33;
        dataSubPanel.add(averagingOptionsPanel, averagingOptionsPanelConstraints);

        this.add(dataSubPanel);
        JPanel chartPropertiesPanel = new JPanel();
        chartPropertiesPanel.add(chartGeneralTabbedPane);
        this.add(chartPropertiesPanel);
    }

    public ChartGeneralTabbedPane getChartGeneralTabbedPane() {
        return chartGeneralTabbedPane;
    }

    public DateRangeBox getDateRangeBox() {
        return dateRangeBox;

    }

    public Timestamp[] getDynamicStartAndEndDates() {
        return dateRangeBox.getDynamicStartAndEndDates();
    }

    @Override
    public String getName() {
        return nameField.getText();
    }

    public SamplingType getSamplingType() {
        return averagingOptionsPanel.getSamplingType();
    }

    private void initBoxes() {
        String msg;
        TitledBorder tb;

        // ---------------------------
        dbButtonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints hdbButtonConstraints = new GridBagConstraints();
        hdbButtonConstraints.fill = GridBagConstraints.NONE;
        hdbButtonConstraints.gridx = 0;
        hdbButtonConstraints.gridy = 0;
        dbButtonPanel.add(hdbButton, hdbButtonConstraints);
        GridBagConstraints tdbButtonConstraints = new GridBagConstraints();
        tdbButtonConstraints.fill = GridBagConstraints.NONE;
        tdbButtonConstraints.gridx = 1;
        tdbButtonConstraints.gridy = 0;
        tdbButtonConstraints.insets = new Insets(0, 30, 0, 0);
        dbButtonPanel.add(tdbButton, tdbButtonConstraints);
        GridBagConstraints ttsButtonConstraints = new GridBagConstraints();
        ttsButtonConstraints.fill = GridBagConstraints.NONE;
        ttsButtonConstraints.gridx = 2;
        ttsButtonConstraints.gridy = 0;
        ttsButtonConstraints.insets = new Insets(0, 30, 0, 0);
        dbButtonPanel.add(ttsButton, ttsButtonConstraints);

        vcIdentificationPanel = new JPanel(new GridBagLayout());

        GridBagConstraints dbButtonPanelConstraints = new GridBagConstraints();
        dbButtonPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        dbButtonPanelConstraints.gridx = 0;
        dbButtonPanelConstraints.gridy = 0;
        dbButtonPanelConstraints.weightx = 1;
        dbButtonPanelConstraints.weighty = 0;
        dbButtonPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        dbButtonPanelConstraints.anchor = GridBagConstraints.LINE_START;
        dbButtonPanelConstraints.insets = new Insets(5, 5, 5, 5);
        vcIdentificationPanel.add(dbButtonPanel, dbButtonPanelConstraints);

        GridBagConstraints creationDateLabelConstraints = new GridBagConstraints();
        creationDateLabelConstraints.fill = GridBagConstraints.NONE;
        creationDateLabelConstraints.gridx = 0;
        creationDateLabelConstraints.gridy = 1;
        creationDateLabelConstraints.weightx = 0;
        creationDateLabelConstraints.weighty = 0;
        creationDateLabelConstraints.anchor = GridBagConstraints.LINE_START;
        creationDateLabelConstraints.insets = new Insets(0, 5, 5, 0);
        vcIdentificationPanel.add(creationDateLabel, creationDateLabelConstraints);

        GridBagConstraints creationDateValueConstraints = new GridBagConstraints();
        creationDateValueConstraints.fill = GridBagConstraints.NONE;
        creationDateValueConstraints.gridx = 1;
        creationDateValueConstraints.gridy = 1;
        creationDateValueConstraints.weightx = 1;
        creationDateValueConstraints.weighty = 0;
        creationDateValueConstraints.anchor = GridBagConstraints.WEST;
        creationDateValueConstraints.insets = new Insets(0, 5, 5, 0);
        vcIdentificationPanel.add(creationDateValue, creationDateValueConstraints);

        GridBagConstraints lastUpdateDateLabelConstraints = new GridBagConstraints();
        lastUpdateDateLabelConstraints.fill = GridBagConstraints.NONE;
        lastUpdateDateLabelConstraints.gridx = 0;
        lastUpdateDateLabelConstraints.gridy = 2;
        lastUpdateDateLabelConstraints.weightx = 0;
        lastUpdateDateLabelConstraints.weighty = 0;
        lastUpdateDateLabelConstraints.anchor = GridBagConstraints.LINE_START;
        lastUpdateDateLabelConstraints.insets = new Insets(0, 5, 5, 0);
        vcIdentificationPanel.add(lastUpdateDateLabel, lastUpdateDateLabelConstraints);

        GridBagConstraints lastUpdateDateValueConstraints = new GridBagConstraints();
        lastUpdateDateValueConstraints.fill = GridBagConstraints.NONE;
        lastUpdateDateValueConstraints.gridx = 1;
        lastUpdateDateValueConstraints.gridy = 2;
        lastUpdateDateValueConstraints.weightx = 1;
        lastUpdateDateValueConstraints.weighty = 0;
        lastUpdateDateValueConstraints.anchor = GridBagConstraints.WEST;
        lastUpdateDateValueConstraints.insets = new Insets(0, 5, 5, 0);
        vcIdentificationPanel.add(lastUpdateDateValue, lastUpdateDateValueConstraints);

        GridBagConstraints nameLabelConstraints = new GridBagConstraints();
        nameLabelConstraints.fill = GridBagConstraints.NONE;
        nameLabelConstraints.gridx = 0;
        nameLabelConstraints.gridy = 3;
        nameLabelConstraints.weightx = 0;
        nameLabelConstraints.weighty = 0;
        nameLabelConstraints.anchor = GridBagConstraints.LINE_START;
        nameLabelConstraints.insets = new Insets(0, 5, 5, 0);
        vcIdentificationPanel.add(nameLabel, nameLabelConstraints);

        GridBagConstraints nameFieldConstraints = new GridBagConstraints();
        nameFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        nameFieldConstraints.gridx = 1;
        nameFieldConstraints.gridy = 3;
        nameFieldConstraints.weightx = 1;
        nameFieldConstraints.weighty = 0;
        nameFieldConstraints.anchor = GridBagConstraints.WEST;
        nameFieldConstraints.insets = new Insets(0, 5, 5, 0);
        vcIdentificationPanel.add(nameField, nameFieldConstraints);

        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_BOXES_VC_IDENTIFICATION");
        tb = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg,
                TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP, GUIUtilities.getTitleFont());
        vcIdentificationPanel.setBorder(tb);
        // ---------------------------

        // ---------------------------
        dateRangeBox = new DateRangeBox();

        // ---------------------------

        // ---------------------------
        averagingOptionsPanel = new AveragingOptionsPanel();
    }

    /**
     * 19 juil. 2005
     */
    private void initComponents(ViewConfigurationBean viewConfigurationBean, VCEditDialog editDialog) {
        String msg = ObjectUtils.EMPTY_STRING;

        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_CREATION_DATE");
        creationDateLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_LAST_UPDATE_DATE");
        lastUpdateDateLabel = new JLabel(msg);

        dbButtonGroup = new ButtonGroup();

        msg = Messages.getMessage("DATABASE_HDB");
        hdbButton = new JRadioButton(msg);
        hdbButton.setSelected(Boolean.TRUE.equals(historic));
        dbButtonGroup.add(hdbButton);

        msg = Messages.getMessage("DATABASE_TDB");
        tdbButton = new JRadioButton(msg);
        tdbButton.setSelected(Boolean.FALSE.equals(historic));
        dbButtonGroup.add(tdbButton);

        msg = Messages.getMessage("DATABASE_TTS");
        ttsButton = new JRadioButton(msg);
        ttsButton.setSelected(historic == null);
        dbButtonGroup.add(ttsButton);

        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_NAME");
        nameLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_AVERAGING_TYPE");

        creationDateValue = new JLabel();
        lastUpdateDateValue = new JLabel();

        nameField = new JTextField();
        nameField.setMargin(new Insets(2, 1, 2, 1));

        dataSubPanel = new JPanel();
        dataSubPanel.setLayout(new GridBagLayout());
        this.initBoxes();

        dateRangeBox.getDateRangeComboBoxListener()
                .itemStateChanged(new ItemEvent(dateRangeBox.getDateRangeComboBox(), ItemEvent.ITEM_STATE_CHANGED,
                        dateRangeBox.getDateRangeComboBoxListener().getLast1h(), ItemEvent.SELECTED));
        // must be after initialization of DateRangeBox
        initializeHistoricCheck();
        hdbButton.addItemListener(this);
        tdbButton.addItemListener(this);
        ttsButton.addItemListener(this);

        chartGeneralTabbedPane = new ChartGeneralTabbedPane();
        chartGeneralTabbedPane
                .setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                        Messages.getMessage("DIALOGS_OPTIONS_VC_CHART_BORDER"), TitledBorder.DEFAULT_JUSTIFICATION,
                        TitledBorder.TOP, GUIUtilities.getTitleFont()));
    }

    private void initializeHistoricCheck() {
        MamboDbUtils.updateDbRadioButtons(hdbButton, tdbButton, ttsButton);
    }

    public boolean isDynamicDateRange() {
        return dateRangeBox.getDynamicDateRangeCheckBox().isSelected();
    }

    public void setDynamicDateRange(boolean dynamic) {
        if (dynamic != isDynamicDateRange()) {
            dateRangeBox.setDynamicDateRangeDoClick();
        }
    }

    public boolean isAutoAveraging() {
        return averagingOptionsPanel.isAutoAveraging();
    }

    public void setAutoAveraging(boolean autoAveraging) {
        if (autoAveraging != isAutoAveraging()) {
            averagingOptionsPanel.setAutoAveraging(autoAveraging);
        }
    }

    /**
     * @param b
     *            25 août 2005
     */
    public Boolean isHistoric() {
        return MamboDbUtils.isHistoricAccordingToAvailableDb(hdbButton.isSelected(), tdbButton.isSelected(),
                ttsButton.isSelected());
    }

    /**
     * @param historic
     *            25 août 2005
     */
    public void setHistoric(Boolean historic) {
        this.historic = historic;
        hdbButton.removeItemListener(this);
        tdbButton.removeItemListener(this);
        ttsButton.removeItemListener(this);
        if (historic == null) {
            ttsButton.setSelected(true);
        } else if (historic.booleanValue()) {
            hdbButton.setSelected(true);
        } else {
            tdbButton.setSelected(true);
        }
        hdbButton.addItemListener(this);
        tdbButton.addItemListener(this);
        ttsButton.addItemListener(this);
    }

    public void setCreationDate(Timestamp creationDate) {
        if (creationDate != null) {
            creationDateValue.setText(creationDate.toString());
        } else {
            creationDateValue.setText(ObjectUtils.EMPTY_STRING);
        }
    }

    public void setDateRange(String range) {
        dateRangeBox.setDateRangeComboBoxSelectedItem(range);

    }

    public void setEndDate(Timestamp endDate) {
        if (endDate != null) {
            dateRangeBox.setEndDate(endDate.toString());
        } else {
            dateRangeBox.setEndDate(ObjectUtils.EMPTY_STRING);
            dateRangeBox.setDateRangeComboBoxSelectedItem("---");
        }
    }

    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        if (lastUpdateDate != null) {
            lastUpdateDateValue.setText(lastUpdateDate.toString());
        } else {
            lastUpdateDateValue.setText(ObjectUtils.EMPTY_STRING);
        }
    }

    /**
     * 19 juil. 2005
     */
    private void setLayout() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    @Override
    public void setName(String _name) {
        nameField.setText(_name);
    }

    public void setSamplingType(SamplingType samplingType) {
        averagingOptionsPanel.setSamplingType(samplingType);
    }

    public void setStartDate(Timestamp startDate) {
        if (startDate != null) {
            dateRangeBox.setStartDate(startDate.toString());
        } else {
            dateRangeBox.setStartDate(ObjectUtils.EMPTY_STRING);
            dateRangeBox.setDateRangeComboBoxSelectedItem("---");
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if ((e != null) && (e.getStateChange() == ItemEvent.SELECTED) && (e.getItemSelectable() == hdbButton
                || e.getItemSelectable() == tdbButton || e.getItemSelectable() == ttsButton)) {
            ViewConfigurationBean viewConfigurationBean = ObjectUtils.recoverObject(viewConfigurationBeanRef);
            if (viewConfigurationBean != null) {
                Boolean wasHistoric = historic;
                Boolean historic;
                if (e.getItemSelectable() == ttsButton) {
                    historic = null;
                } else if (e.getItemSelectable() == hdbButton) {
                    historic = Boolean.TRUE;
                } else {
                    historic = Boolean.FALSE;
                }
                if (!ObjectUtils.sameObject(wasHistoric, historic)) {
                    // title
                    String msgTitle = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_HISTORIC_CONFIRM_TITLE");
                    // message
                    String msgConfirm = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_HISTORIC_CONFIRM_LABEL");
                    String msgCancel = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_HISTORIC_CONFIRM_CANCEL");
                    String msgValidate = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_HISTORIC_CONFIRM_VALIDATE");
                    Object[] options = { msgValidate, msgCancel };

                    int confirm = JOptionPane.showOptionDialog(null, msgConfirm, msgTitle, JOptionPane.DEFAULT_OPTION,
                            JOptionPane.WARNING_MESSAGE, null, options, options[0]);

                    if (confirm == JOptionPane.OK_OPTION) {
                        viewConfigurationBean.getVcPossibleAttributesTreeModel().setHistoric(wasHistoric);
                        VCAttributesTreeModel vCAttributesTreeModel = viewConfigurationBean.getEditingModel();
                        VCPossibleAttributesTreeModel vCPossibleAttributesTreeModel = viewConfigurationBean
                                .getVcPossibleAttributesTreeModel();
                        try {
                            try {
                                vCPossibleAttributesTreeModel.setHistoric(historic);
                            } catch (Exception ex) {
                                LOGGER.error("Unexpected error while updating vCPossibleAttributesTreeModel", ex);
                            } finally {
                                vCAttributesTreeModel.setHistoric(historic, true);
                            }
                        } catch (Exception ex) {
                            LOGGER.error("Unexpected error while updating vCAttributesTreeModel", ex);
                        } finally {
                            if (ObjectUtils.sameObject(historic, vCPossibleAttributesTreeModel.isHistoric())
                                    && ObjectUtils.sameObject(historic, vCAttributesTreeModel.isHistoric())) {
                                this.historic = historic;
                            } else {
                                setHistoric(wasHistoric);
                            }
                        }
                    } else {
                        setHistoric(wasHistoric);
                    }
                }
            }
        }
    }

}
