package fr.soleil.mambo.containers.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Calendar;
import java.util.Collection;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.view.VCViewSpectrumChartAction;
import fr.soleil.mambo.actions.view.VCViewSpectrumFilterAction;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.renderers.ViewSpectrumTableBooleanRenderer;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.models.ViewSpectrumTableModel;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.SpringUtilities;

/**
 * @author SOLEIL
 */
public class ViewSpectrumPanel extends AbstractViewSpectrumPanel implements TableModelListener, ItemListener {

    private static final long serialVersionUID = -229727490404000677L;

    private static final ImageIcon VIEW_ICON = new ImageIcon(Mambo.class.getResource("icons/View.gif"));
    private static final Dimension REF_SIZE = new Dimension(50, 50);
    private static final Dimension TABLE_MIN_SIZE = new Dimension(110, 200);

    private final JButton viewButton, allRead, allWrite, noneRead, noneWrite;
    private final JCheckBox subtractMean, compareMode;
    private final ViewSpectrumTableModel model;
    private final JTable choiceTable;
    private final JScrollPane choiceTableScrollPane;
    private final JPanel buttonPanel;
    private final JLabel noDataLabel;
    private final Component glue1, glue2, glue3, glue4;

    private int writable;

    public ViewSpectrumPanel(final ViewConfigurationBean viewConfigurationBean) throws ArchivingException {
        super(viewConfigurationBean);
        model = new ViewSpectrumTableModel();
        buttonPanel = new JPanel();
        GUIUtilities.setObjectBackground(buttonPanel, GUIUtilities.VIEW_COLOR);
        buttonPanel.setLayout(new SpringLayout());

        viewButton = new JButton();
        viewButton.setMargin(new Insets(0, 0, 0, 0));
        viewButton.setIcon(VIEW_ICON);
        GUIUtilities.setObjectBackground(viewButton, GUIUtilities.VIEW_COLOR);
        viewButton.addActionListener(new VCViewSpectrumChartAction(this));
        viewButton.setText(Messages.getMessage("VIEW_ACTION_VIEW_BUTTON") + " " + getName());
        viewButton.setPreferredSize(saveButton.getPreferredSize());

        compareMode = new JCheckBox(Messages.getMessage("VIEW_SPECTRUM_COMPARE_MODE"));
        GUIUtilities.setObjectBackground(compareMode, GUIUtilities.VIEW_COLOR);
        compareMode.setSelected(false);
        compareMode.addItemListener(this);

        subtractMean = new JCheckBox(Messages.getMessage("VIEW_SPECTRUM_SUBTRACT_MEAN"));
        GUIUtilities.setObjectBackground(subtractMean, GUIUtilities.VIEW_COLOR);
        subtractMean.setSelected(false);
        subtractMean.addItemListener(this);

        noDataLabel = new JLabel(Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA"));
        noDataLabel.setVerticalAlignment(SwingConstants.NORTH);
        GUIUtilities.setObjectBackground(noDataLabel, GUIUtilities.VIEW_COLOR);
        noDataLabel.setForeground(Color.RED);

        choiceTable = new JTable(model);
        choiceTable.setDefaultRenderer(Boolean.class, new ViewSpectrumTableBooleanRenderer());
        choiceTable.setFont(choiceTable.getFont().deriveFont(Font.BOLD));
        choiceTable.setMinimumSize(TABLE_MIN_SIZE);
        GUIUtilities.setObjectBackground(choiceTable, GUIUtilities.VIEW_COLOR);
        choiceTableScrollPane = new JScrollPane(choiceTable);
        choiceTableScrollPane.setPreferredSize(REF_SIZE);
        choiceTableScrollPane.setMinimumSize(REF_SIZE);
        GUIUtilities.setObjectBackground(choiceTableScrollPane, GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(choiceTableScrollPane.getViewport(), GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(choiceTableScrollPane.getHorizontalScrollBar(), GUIUtilities.VIEW_COLOR);
        GUIUtilities.setObjectBackground(choiceTableScrollPane.getVerticalScrollBar(), GUIUtilities.VIEW_COLOR);

        allRead = new JButton(new VCViewSpectrumFilterAction(Messages.getMessage("VIEW_SPECTRUM_FILTER_SELECT_ALL")
                + " " + Messages.getMessage("VIEW_SPECTRUM_READ"), this, true, true));
        allRead.setMargin(CometeUtils.getzInset());
        noneRead = new JButton(new VCViewSpectrumFilterAction(Messages.getMessage("VIEW_SPECTRUM_FILTER_SELECT_NONE")
                + " " + Messages.getMessage("VIEW_SPECTRUM_READ"), this, false, true));
        noneRead.setMargin(CometeUtils.getzInset());
        allWrite = new JButton(new VCViewSpectrumFilterAction(Messages.getMessage("VIEW_SPECTRUM_FILTER_SELECT_ALL")
                + " " + Messages.getMessage("VIEW_SPECTRUM_WRITE"), this, true, false));
        allWrite.setMargin(CometeUtils.getzInset());
        noneWrite = new JButton(new VCViewSpectrumFilterAction(Messages.getMessage("VIEW_SPECTRUM_FILTER_SELECT_NONE")
                + " " + Messages.getMessage("VIEW_SPECTRUM_WRITE"), this, false, false));
        noneWrite.setMargin(CometeUtils.getzInset());

        glue1 = Box.createHorizontalGlue();
        glue2 = Box.createHorizontalGlue();
        glue3 = Box.createHorizontalGlue();
        glue4 = Box.createHorizontalGlue();
    }

    @Override
    protected void initCenterPanelLayout() {
        centerPanel.setLayout(new GridBagLayout());
    }

    private String[] generateNames(final DbData data, final int viewType, final String toEnd) {
        String[] names = null;

        if (data != null) {
            switch (viewType) {
                case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX:
                    names = new String[data.getMaxX()];
                    for (int i = 0; i < names.length; i++) {
                        names[i] = Integer.toString(i + 1) + " " + toEnd;
                    }
                    break;
                case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME:
                case ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_TIME_STACK:
                    names = new String[data.getTimedData() == null ? 0 : data.getTimedData().length];
                    for (int i = 0; i < names.length; i++) {
                        final long time = data.getTimedData()[i].getTime();
                        if (time == 0) {
                            names[i] = ObjectUtils.EMPTY_STRING;
                        } else {
                            final Calendar calendar = Calendar.getInstance();
                            calendar.setTimeInMillis(data.getTimedData()[i].getTime());
                            names[i] = DATE_FORMAT.format(calendar.getTime()) + " " + toEnd;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return names;
    }

    @Override
    protected void updateComponents() {
        if (!isCleaning()) {
            if ((splitData == null) || (isNullOrEmpty(splitData[0]) && isNullOrEmpty(splitData[1]))) {
                writable = ViewSpectrumTableModel.UNKNOWN;
                model.setNames(null, null);
                final GridBagConstraints noDataLabelConstraints = new GridBagConstraints();
                noDataLabelConstraints.fill = GridBagConstraints.BOTH;
                noDataLabelConstraints.gridx = 0;
                noDataLabelConstraints.gridy = 1;
                noDataLabelConstraints.weightx = 1;
                noDataLabelConstraints.weighty = 1;
                noDataLabelConstraints.anchor = GridBagConstraints.NORTH;
                noDataLabelConstraints.gridwidth = GridBagConstraints.REMAINDER;
                centerPanel.add(noDataLabel, noDataLabelConstraints);
            } else {
                enableViewButton(true, false);

                final int viewType = getViewSpectrumType();

                String[] readNames = null;
                String[] writeNames = null;

                if (splitData[0] != null) {
                    allRead.setEnabled(true);
                    noneRead.setEnabled(true);
                    readNames = generateNames(splitData[0], viewType, Messages.getMessage("VIEW_SPECTRUM_READ"));
                    writable = ViewSpectrumTableModel.R;
                }
                if (splitData[1] != null) {
                    allWrite.setEnabled(true);
                    noneWrite.setEnabled(true);
                    writeNames = generateNames(splitData[1], viewType, Messages.getMessage("VIEW_SPECTRUM_WRITE"));
                    writable = ViewSpectrumTableModel.W;
                }
                if ((splitData[0] != null) && (splitData[1] != null)) {
                    writable = ViewSpectrumTableModel.RW;
                }
                model.setNames(readNames, writeNames);
                model.setViewSpectrumType(viewType);
                model.addTableModelListener(this);

                // this is ok because when using model.setNames, read values are
                // initially selected...
                compareMode.setEnabled(model.getSelectedReadRowCount() == 2);

                switch (writable) {
                    case ViewSpectrumTableModel.R:
                        buttonPanel.add(allRead);
                        buttonPanel.add(glue1);
                        buttonPanel.add(subtractMean);
                        buttonPanel.add(glue2);
                        buttonPanel.add(noneRead);
                        buttonPanel.add(glue3);
                        buttonPanel.add(compareMode);
                        buttonPanel.add(glue4);
                        SpringUtilities.makeCompactGrid(buttonPanel, 2, 4, 5, 5, 5, 5, true);
                        break;
                    case ViewSpectrumTableModel.W:
                        buttonPanel.add(allWrite);
                        buttonPanel.add(glue1);
                        buttonPanel.add(subtractMean);
                        buttonPanel.add(noneWrite);
                        buttonPanel.add(glue2);
                        buttonPanel.add(compareMode);
                        SpringUtilities.makeCompactGrid(buttonPanel, 2, 4, 5, 5, 5, 5, true);
                        break;
                    case ViewSpectrumTableModel.RW:
                        buttonPanel.add(allRead);
                        buttonPanel.add(glue1);
                        buttonPanel.add(subtractMean);
                        buttonPanel.add(glue2);
                        buttonPanel.add(allWrite);
                        buttonPanel.add(noneRead);
                        buttonPanel.add(glue3);
                        buttonPanel.add(compareMode);
                        buttonPanel.add(glue4);
                        buttonPanel.add(noneWrite);
                        SpringUtilities.makeCompactGrid(buttonPanel, 2, 5, 5, 5, 5, 5, true);
                        break;
                    default:
                        break;
                }
                final GridBagConstraints buttonPanelConstraints = new GridBagConstraints();
                buttonPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
                buttonPanelConstraints.gridx = 0;
                buttonPanelConstraints.gridy = 1;
                buttonPanelConstraints.weightx = 1;
                buttonPanelConstraints.weighty = 0;
                buttonPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
                centerPanel.add(buttonPanel, buttonPanelConstraints);

                final GridBagConstraints choiceTableConstraints = new GridBagConstraints();
                choiceTableConstraints.fill = GridBagConstraints.BOTH;
                choiceTableConstraints.gridx = 0;
                choiceTableConstraints.gridy = 2;
                choiceTableConstraints.weightx = 1;
                choiceTableConstraints.weighty = 1;
                choiceTableConstraints.gridwidth = GridBagConstraints.REMAINDER;
                centerPanel.add(choiceTableScrollPane, choiceTableConstraints);

                final GridBagConstraints viewButtonConstraints = new GridBagConstraints();
                viewButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
                viewButtonConstraints.gridx = 0;
                viewButtonConstraints.gridy = 3;
                viewButtonConstraints.weightx = 0.5;
                viewButtonConstraints.weighty = 0;
                centerPanel.add(viewButton, viewButtonConstraints);

                final GridBagConstraints saveButtonConstraints = new GridBagConstraints();
                saveButtonConstraints.fill = GridBagConstraints.HORIZONTAL;
                saveButtonConstraints.gridx = 1;
                saveButtonConstraints.gridy = 3;
                saveButtonConstraints.weightx = 0.5;
                saveButtonConstraints.weighty = 0;
                saveButtonConstraints.insets = new Insets(0, 20, 0, 0);
                centerPanel.add(saveButton, saveButtonConstraints);
            }
        }
    }

    public DbData[] getSplitedData() {
        return splitData;
    }

    public ViewSpectrumTableModel getModel() {
        return model;
    }

    @Override
    public int getViewSpectrumType() {
        return spectrum.getProperties().getPlotProperties().getSpectrumViewType();
    }

    public void setAllReadSelected(final boolean selected) {
        if ((choiceTable == null) && (choiceTable.getRowCount() > 0) && (writable != ViewSpectrumTableModel.UNKNOWN)
                && (writable != ViewSpectrumTableModel.W)) {
            final Boolean valueOf = Boolean.valueOf(selected);
            int readLength = model.getReadLength();
            for (int i = 0; i < readLength; i++) {
                model.setValueAt(valueOf, i, 0);
            }
        }
    }

    public void setAllWriteSelected(final boolean selected) {
        if (choiceTable.getRowCount() > 0 && writable != ViewSpectrumTableModel.UNKNOWN
                && writable != ViewSpectrumTableModel.R) {
            if (writable == ViewSpectrumTableModel.W) {
                final Boolean valueOf = Boolean.valueOf(selected);
                for (int i = 0; i < choiceTable.getRowCount(); i++) {
                    model.setValueAt(valueOf, i, 0);
                }
            } else { // RW
                final Boolean valueOf = Boolean.valueOf(selected);
                for (int i = 0; i < model.getWriteLength(); i++) {
                    model.setValueAt(valueOf, i, 3);
                }
            }
        }
    }

    @Override
    protected void performDeepClean() {
        centerPanel.removeAll();
        buttonPanel.removeAll();
        model.setNames(null, null);
        spectrum = null;
        cleanSplitData();
    }

    @Override
    public void lightClean() {
        setLoading();
        cleanSplitData();
        centerPanel.removeAll();
        buttonPanel.removeAll();
        model.setNames(null, null);
        enableViewButton(false, false);
    }

    public void enableViewButton(final boolean enable, final boolean updateText) {
        if (viewButton != null) {
            viewButton.setEnabled(enable);
            if (updateText) {
                if (enable) {
                    viewButton.setText(Messages.getMessage("VIEW_ACTION_VIEW_BUTTON") + " " + getName());
                } else {
                    viewButton.setText(Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA") + " " + getName());
                }
            }
        }
    }

    @Override
    protected Collection<Integer> getReadFilter() {
        Collection<Integer> result;
        if (model == null) {
            result = null;
        } else {
            result = model.getSelectedRead();
        }
        return result;
    }

    @Override
    protected Collection<Integer> getWriteFilter() {
        Collection<Integer> result;
        if (model == null) {
            result = null;
        } else {
            result = model.getSelectedWrite();
        }
        return result;
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        // enable compare button if only 2 read spectrums or 2 write spectrums are selected
        int selectedReadRowCount = model.getSelectedReadRowCount();
        int selectedWriteRowCount = model.getSelectedWriteRowCount();

        boolean enableReadCompare = selectedReadRowCount == 2 && selectedWriteRowCount == 0;
        boolean enableWriteCompare = selectedReadRowCount == 0 && selectedWriteRowCount == 2;

        boolean b = enableReadCompare || enableWriteCompare;
        compareMode.setEnabled(b);

        // if disabled, deselect as well
        compareMode.setSelected(compareMode.isSelected() && b);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        // simulate a group as for radio buttons, but both can be unselected
        if (e.getStateChange() == ItemEvent.SELECTED) {
            if (e.getSource() == subtractMean) {
                compareMode.setSelected(false);
            } else if (e.getSource() == compareMode) {
                subtractMean.setSelected(false);
            }
        }
    }

    public boolean isSubtractMean() {
        return subtractMean.isSelected();
    }

    public boolean isCompareMode() {
        return compareMode.isSelected();
    }

}
