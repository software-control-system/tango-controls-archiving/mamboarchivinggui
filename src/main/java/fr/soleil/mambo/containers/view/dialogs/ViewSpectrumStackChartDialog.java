package fr.soleil.mambo.containers.view.dialogs;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.IPlayerAnimationBehavior;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.Player;
import fr.soleil.comete.swing.util.AnimationTool;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.expression.jep.JEPExpressionParser;
import fr.soleil.mambo.bean.manager.ViewConfigurationBeanManager;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.containers.view.ViewSpectrumStackPanel;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationData;
import fr.soleil.mambo.data.view.ViewSpectrumStackAdapter;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.tools.Messages;

/**
 * @author awo
 */
public class ViewSpectrumStackChartDialog extends JDialog {

    private static final long serialVersionUID = 939982592629825955L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewSpectrumStackChartDialog.class);

    private static final Dimension DIM = new Dimension(700, 700);

    private final Player player;
    private final IPlayerAnimationBehavior animationBehavior;
    private final Chart chart;
    private final ViewSpectrumStackAdapter stackNumberDataArrayDAO;

    private JPanel mainPanel;

    private WeakReference<ViewSpectrumStackPanel> specPanelRef;

    public ViewSpectrumStackChartDialog(Window parent, ViewSpectrumStackPanel specPanel,
            ViewConfigurationBean viewConfigurationBean) {
        super(parent);
        setModal(true);
        this.specPanelRef = (specPanel == null ? null : new WeakReference<ViewSpectrumStackPanel>(specPanel));
        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);

        mainPanel = new JPanel(new BorderLayout());
        GUIUtilities.setObjectBackground(mainPanel, GUIUtilities.VIEW_COLOR);

        chart = new Chart();
        chart.setExpressionParser(new JEPExpressionParser());
        chart.setRefreshLater(true);
        chart.setPreferredSnapshotExtension("png");
        chart.setManagementPanelVisible(false);
        chart.setPreferDialogForTable(true, true);

        animationBehavior = new IPlayerAnimationBehavior() {
            @Override
            public String generateAnimation(IPlayer player, String targetDirectory) {
                return generateBounds();
            }
        };

        player = new Player();
        player.setPeriodInMs(100);
        player.getSlider().setCometeForeground(CometeColor.BLACK);
        player.setAnimationButtonVisible(true);
        player.setPlayerAnimationBehavior(animationBehavior);

        stackNumberDataArrayDAO = new ViewSpectrumStackAdapter(chart, player);

        ViewConfiguration selectedViewConfiguration = ViewConfigurationBeanManager.getInstance()
                .getSelectedConfiguration();
        if (selectedViewConfiguration != null) {
            ViewConfigurationData data = selectedViewConfiguration.getData();
            String title = ObjectUtils.EMPTY_STRING;
            if (data != null) {
                if (chart.getHeader() != null) {
                    title += chart.getHeader() + " - ";
                }
                title += Messages.getMessage("VIEW_ATTRIBUTES_START_DATE") + data.getStartDate().toString() + ", "
                        + Messages.getMessage("VIEW_ATTRIBUTES_END_DATE") + data.getEndDate().toString();
            }
            chart.setHeader(title);
        }
        chart.setCometeBackground(ColorTool.getCometeColor(GUIUtilities.getViewColor()));
        player.setCometeBackground(ColorTool.getCometeColor(GUIUtilities.getViewColor()));

        mainPanel.add(chart, BorderLayout.CENTER);
        mainPanel.add(player, BorderLayout.SOUTH);
        setContentPane(mainPanel);

        setSize(DIM);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                player.stop();
                ViewSpectrumStackPanel specPanel = getSpecPanel();
                if (specPanel != null) {
                    specPanel.setFileDirectory(chart.getDataDirectory());
                }
                clean();
            }
        });
    }

    protected ViewSpectrumStackPanel getSpecPanel() {
        return ObjectUtils.recoverObject(specPanelRef);
    }

    public void updateContent() {
        clean();

        ViewSpectrumStackPanel specPanel = getSpecPanel();

        int checkBoxSelected;
        DbData[] splitedData;
        List<String> timeList;

        chart.setLegendVisible(false);
        chart.setAxisScaleEditionEnabled(false, IChartViewer.X);
        chart.setNoValueString(Options.getInstance().getVcOptions().getNoValueString());

        if (specPanel == null) {
            checkBoxSelected = -1;
            splitedData = null;
            timeList = null;
        } else {
            checkBoxSelected = specPanel.getCheckBoxSelected();
            splitedData = specPanel.getSplitedData();
            chart.setDataDirectory(specPanel.getFileDirectory());
            timeList = specPanel.getTimeList();
        }

        stackNumberDataArrayDAO.setData(splitedData, checkBoxSelected);

        // Slider legend preparation
        int increment;
        if ((timeList == null) || (timeList.isEmpty())) {
            increment = 0;
            player.getSlider().setLabelTable(null);
        } else {
            increment = timeList.size() / 3;
            String[] label = new String[(timeList.size() / increment) + 1];
            int[] value = new int[(timeList.size() / increment) + 1];

            for (int i = 0; i < timeList.size() / increment; i++) {
                value[i] = i * increment;
                label[i] = timeList.get(i * increment);
            }
            value[timeList.size() / increment] = timeList.size() - 1;
            label[timeList.size() / increment] = timeList.get(timeList.size() - 1);

            player.getSlider().setLabelTable(label, value);
        }
        // slider legend preparation END

        // Though it should not, setMajorTickSpacing(0) throws an exception.
        // That's why such a test is done.
        if (increment == 0) {
            player.getSlider().setPaintLabels(false);
        } else {
            player.getSlider().setPaintLabels(true);
            player.getSlider().setMajorTickSpacing(increment);
            player.getSlider().setMinorTickSpacing(0);
        }

        // Force spectrumStackViewer to browse all spectrums
        player.getSlider().setStep(1);

        mainPanel.add(chart, BorderLayout.CENTER);
        mainPanel.add(player, BorderLayout.SOUTH);
    }

    public void clean() {
        stackNumberDataArrayDAO.clean();
    }

    private String generateBounds() {
        String output = null;
        Component chartComponent = chart;
        Dimension boundSize = chartComponent.getSize();
        boundSize.height += player.getSize().getHeight();
        Rectangle bounds = new Rectangle(chartComponent.getLocationOnScreen(), boundSize);
        try {
            File tmp = AnimationTool.createAnimation(player, null, bounds);
            if (tmp != null) {
                output = tmp.getAbsolutePath();
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return output;
    }

}
