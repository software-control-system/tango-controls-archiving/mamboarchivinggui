package fr.soleil.mambo.containers.view.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.mambo.components.view.SamplingTypeComboBox;
import fr.soleil.mambo.tools.Messages;

/**
 * {@link JPanel} used to configure averaging options
 * 
 * @author Rapha&euml;l GIRARDOT
 */
public class AveragingOptionsPanel extends JPanel {

    private static final long serialVersionUID = -4584884786624007510L;

    private static final Insets SAMPLING_TYPE_FACTOR_FIELD_MARGIN = new Insets(2, 1, 2, 1);
    private static final Insets FIRST_LINE_INSETS = new Insets(5, 5, 0, 0);
    private static final Insets FIRST_LINE_LAST_INSETS = new Insets(5, 5, 0, 5);
    private static final Insets SECOND_LINE_LAST_INSETS = new Insets(5, 5, 5, 5);

    private JLabel samplingTypeLabel;
    private JTextField samplingTypeFactorField;
    private SamplingTypeComboBox samplingTypeComboBox;
    private ConstrainedCheckBox autoAveragingCheckBox;

    public AveragingOptionsPanel() {
        super(new GridBagLayout());

        String msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_AVERAGING_TYPE");
        samplingTypeLabel = new JLabel(msg);
        samplingTypeComboBox = new SamplingTypeComboBox();
        samplingTypeFactorField = new JTextField();
        samplingTypeFactorField.setMargin(SAMPLING_TYPE_FACTOR_FIELD_MARGIN);
        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_DYNAMIC_AUTOMATIC_AVERAGING");
        autoAveragingCheckBox = new ConstrainedCheckBox(msg);
        autoAveragingCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
        autoAveragingCheckBox.setFont(samplingTypeLabel.getFont());
        autoAveragingCheckBox.setBorder(null);
        autoAveragingCheckBox.setOpaque(false);
        autoAveragingCheckBox.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                setAveragingConfigurationEnabled(false);
            } else if (e.getStateChange() == ItemEvent.DESELECTED) {
                setAveragingConfigurationEnabled(true);
            }
        });
        setAveragingConfigurationEnabled(!autoAveragingCheckBox.isSelected());

        GridBagConstraints samplingTypeLabelConstraints = new GridBagConstraints();
        samplingTypeLabelConstraints.fill = GridBagConstraints.NONE;
        samplingTypeLabelConstraints.gridx = 0;
        samplingTypeLabelConstraints.gridy = 0;
        samplingTypeLabelConstraints.weightx = 0;
        samplingTypeLabelConstraints.weighty = 0;
        samplingTypeLabelConstraints.anchor = GridBagConstraints.LINE_START;
        samplingTypeLabelConstraints.insets = FIRST_LINE_INSETS;
        add(samplingTypeLabel, samplingTypeLabelConstraints);

        GridBagConstraints samplingTypeFactorFieldConstraints = new GridBagConstraints();
        samplingTypeFactorFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        samplingTypeFactorFieldConstraints.gridx = 1;
        samplingTypeFactorFieldConstraints.gridy = 0;
        samplingTypeFactorFieldConstraints.weightx = 0.4;
        samplingTypeFactorFieldConstraints.weighty = 0;
        samplingTypeFactorFieldConstraints.insets = FIRST_LINE_INSETS;
        add(samplingTypeFactorField, samplingTypeFactorFieldConstraints);

        GridBagConstraints samplingTypeComboBoxConstraints = new GridBagConstraints();
        samplingTypeComboBoxConstraints.fill = GridBagConstraints.HORIZONTAL;
        samplingTypeComboBoxConstraints.gridx = 2;
        samplingTypeComboBoxConstraints.gridy = 0;
        samplingTypeComboBoxConstraints.weightx = 0.6;
        samplingTypeComboBoxConstraints.weighty = 0;
        samplingTypeComboBoxConstraints.insets = FIRST_LINE_LAST_INSETS;
        add(samplingTypeComboBox, samplingTypeComboBoxConstraints);

        GridBagConstraints autoAveragingCheckBoxConstraints = new GridBagConstraints();
        autoAveragingCheckBoxConstraints.fill = GridBagConstraints.HORIZONTAL;
        autoAveragingCheckBoxConstraints.gridx = 0;
        autoAveragingCheckBoxConstraints.gridy = 1;
        autoAveragingCheckBoxConstraints.weightx = 1;
        autoAveragingCheckBoxConstraints.weighty = 0;
        autoAveragingCheckBoxConstraints.gridwidth = GridBagConstraints.REMAINDER;
        autoAveragingCheckBoxConstraints.insets = SECOND_LINE_LAST_INSETS;
        add(autoAveragingCheckBox, autoAveragingCheckBoxConstraints);

        msg = Messages.getMessage("DIALOGS_EDIT_VC_GENERAL_BOXES_VC_AVERAGING_OPTIONS");
        setBorder(new TitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg,
                TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP, GUIUtilities.getTitleFont()));
    }

    public void setAveragingConfigurationVisible(boolean visible) {
        samplingTypeLabel.setVisible(visible);
        samplingTypeFactorField.setVisible(visible);
        samplingTypeComboBox.setVisible(visible);
    }

    public boolean isAutoAveraging() {
        return autoAveragingCheckBox.isSelected();
    }

    public void setAutoAveraging(boolean autoAveraging) {
        autoAveragingCheckBox.setSelected(autoAveraging);
    }

    protected void setAveragingConfigurationEnabled(boolean enabled) {
        samplingTypeLabel.setEnabled(enabled);
        samplingTypeFactorField.setEnabled(enabled);
        samplingTypeComboBox.setEnabled(enabled);
    }

    public SamplingType getSamplingType() {
        SamplingType ret = (SamplingType) samplingTypeComboBox.getSelectedItem();
        short factor = 0;

        try {
            String factor_s = samplingTypeFactorField.getText();
            factor = Short.parseShort(factor_s);
        } catch (Exception e) {

        }

        if (factor > 0) {
            ret.setHasAdditionalFiltering(true);
            ret.setAdditionalFilteringFactor(factor);
        } else {
            ret.setHasAdditionalFiltering(false);
        }

        return ret;
    }

    public void setSamplingType(SamplingType samplingType) {
        samplingTypeComboBox.setSelectedSamplingType(samplingType);
        if (samplingType.hasAdditionalFiltering()) {
            String factor = ObjectUtils.EMPTY_STRING + samplingType.getAdditionalFilteringFactor();
            samplingTypeFactorField.setText(factor);
        } else {
            samplingTypeFactorField.setText("1");
        }
    }

    public void addAutomaticAveragingListener(ItemListener listener) {
        if (listener != null) {
            autoAveragingCheckBox.addItemListener(listener);
        }
    }

    public void removeAutomaticAveragingListener(ItemListener listener) {
        if (listener != null) {
            autoAveragingCheckBox.removeItemListener(listener);
        }
    }

}
