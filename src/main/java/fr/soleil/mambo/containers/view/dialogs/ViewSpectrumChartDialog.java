package fr.soleil.mambo.containers.view.dialogs;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.ref.WeakReference;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.Chart;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.bean.manager.ViewConfigurationBeanManager;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.containers.view.ViewSpectrumPanel;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.data.view.ViewConfigurationData;
import fr.soleil.mambo.data.view.ViewSpectrumAdapter;
import fr.soleil.mambo.models.ViewSpectrumTableModel;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.tools.Messages;

/**
 * @author SOLEIL
 */
public class ViewSpectrumChartDialog extends JDialog implements IChartViewerListener {

    private static final long serialVersionUID = -1453451949765053959L;

    private final Chart chartRecorder;
    private final JPanel mainPanel;

    private final JTextField firstField;
    private final JTextField secondField;
    private final JTextField diffField;

    private double[] firstPoint = null;
    private double[] secondPoint = null;
    private boolean clickForFirst = true;

    private final WeakReference<ViewSpectrumPanel> specPanelRef;
    private final ViewSpectrumAdapter spectrumAdapter;

    public ViewSpectrumChartDialog(final ViewSpectrumPanel specPanel) {
        super(MamboFrame.getInstance(), specPanel.getFullName(), true);
        specPanelRef = (specPanel == null ? null : new WeakReference<ViewSpectrumPanel>(specPanel));
        GUIUtilities.setObjectBackground(this, GUIUtilities.VIEW_COLOR);
        mainPanel = new JPanel();
        GUIUtilities.setObjectBackground(mainPanel, GUIUtilities.VIEW_COLOR);
        chartRecorder = new Chart();
        chartRecorder.setRefreshLater(true);
        chartRecorder.setPreferredSnapshotExtension("png");
        chartRecorder.setFreezePanelVisible(false);
        chartRecorder.setManagementPanelVisible(false);

        chartRecorder.setPreferDialogForTable(true, true);
        chartRecorder.setLegendVisible(false);
        chartRecorder.setNoValueString(Options.getInstance().getVcOptions().getNoValueString());

        chartRecorder.addChartViewerListener(this);

        spectrumAdapter = new ViewSpectrumAdapter(chartRecorder);

        ViewConfiguration selectedViewConfiguration = ViewConfigurationBeanManager.getInstance()
                .getSelectedConfiguration();
        if (selectedViewConfiguration != null) {
            ViewConfigurationData data = selectedViewConfiguration.getData();
            String title = ObjectUtils.EMPTY_STRING;
            if (data != null) {
                if (chartRecorder.getHeader() != null) {
                    title += chartRecorder.getHeader() + " - ";
                }
                title += Messages.getMessage("VIEW_ATTRIBUTES_START_DATE") + data.getStartDate().toString() + ", "
                        + Messages.getMessage("VIEW_ATTRIBUTES_END_DATE") + data.getEndDate().toString();
            }
            chartRecorder.setHeader(title);
        }
        chartRecorder.setCometeBackground(ColorTool.getCometeColor(GUIUtilities.getViewColor()));

        firstField = new JTextField();
        firstField.setEditable(false);

        secondField = new JTextField();
        secondField.setEditable(false);

        diffField = new JTextField();
        diffField.setEditable(false);

        layoutComponents();
        initBounds();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                if (chartRecorder != null) {
                    ViewSpectrumPanel specPanel = getSpecPanel();
                    if (specPanel != null) {
                        specPanel.setFileDirectory(chartRecorder.getDataDirectory());
                    }
                    clean();
                }
            }
        });
    }

    protected ViewSpectrumPanel getSpecPanel() {
        return ObjectUtils.recoverObject(specPanelRef);
    }

    public void updateContent() {
        clean();
        ViewSpectrumPanel specPanel = getSpecPanel();
        DbData[] splitedData;
        ViewSpectrumTableModel model;
        int spectrumType;
        boolean subtractMean;
        boolean compareMode;
        if (specPanel == null) {
            splitedData = null;
            model = null;
            spectrumType = -1;
            subtractMean = false;
            compareMode = false;
        } else {
            splitedData = specPanel.getSplitedData();
            model = specPanel.getModel();
            spectrumType = specPanel.getViewSpectrumType();
            chartRecorder.setDataDirectory(specPanel.getFileDirectory());
            subtractMean = specPanel.isSubtractMean();
            compareMode = specPanel.isCompareMode();
        }

        spectrumAdapter.setModel(model);
        spectrumAdapter.setData(splitedData, spectrumType, subtractMean, compareMode);

        chartRecorder.addChartViewerListener(this);

        if (spectrumType != ViewConfigurationAttributePlotProperties.SPECTRUM_VIEW_TYPE_INDEX) {
            chartRecorder.setAxisScaleEditionEnabled(false, IChartViewer.X);
        }

        // update dialog title to let user know about what is displayed
        String mode = null;
        if (subtractMean) {
            mode = Messages.getMessage("VIEW_SPECTRUM_SUBTRACT_MEAN");
        } else if (compareMode) {
            mode = Messages.getMessage("VIEW_SPECTRUM_COMPARE_MODE");
        }
        if (mode != null) {
            // here, we are sure specPanel is not null, due to previous test
            setTitle(specPanel.getFullName() + " (" + mode + ")");
        }

    }

    public void clean() {
        if (chartRecorder != null) {
            chartRecorder.removeChartViewerListener(this);
            spectrumAdapter.clean();
        }
        resetFields();
    }

    private void layoutComponents() {
        JLabel firstLabel = new JLabel(Messages.getMessage("DIALOGS_VIEW_FIRST_POINT"));
        firstLabel.setLabelFor(firstField);

        JLabel secondLabel = new JLabel(Messages.getMessage("DIALOGS_VIEW_SECOND_POINT"));
        secondLabel.setLabelFor(secondField);

        JLabel diffLabel = new JLabel(Messages.getMessage("DIALOGS_VIEW_DIFF"));
        diffLabel.setLabelFor(diffField);

        Box box = Box.createHorizontalBox();
        box.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        box.add(firstLabel);
        box.add(Box.createHorizontalStrut(5));
        box.add(firstField);
        box.add(Box.createHorizontalStrut(10));
        box.add(Box.createHorizontalGlue());
        box.add(secondLabel);
        box.add(Box.createHorizontalStrut(5));
        box.add(secondField);
        box.add(Box.createHorizontalStrut(10));
        box.add(Box.createHorizontalGlue());
        box.add(diffLabel);
        box.add(Box.createHorizontalStrut(5));
        box.add(diffField);

        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(chartRecorder, BorderLayout.CENTER);
        mainPanel.add(box, BorderLayout.SOUTH);

        this.setContentPane(mainPanel);
    }

    private void initBounds() {
        int x = MamboFrame.getInstance().getX() + MamboFrame.getInstance().getWidth() - 700;
        int y = MamboFrame.getInstance().getY() + MamboFrame.getInstance().getHeight() - 700;
        y /= 2;
        if (x < 0)
            x = 0;
        if (y < 0)
            y = 0;
        this.setBounds(x, y, 700, 700);
    }

    private void selectedPointChanged(final double[] point) {
        // NaN can happen if no point is visible on the graph
        if (!Double.isNaN(point[1])) {
            if (clickForFirst) {
                firstPoint = point;
                secondPoint = null;
            } else {
                secondPoint = point;
            }
            updateFields();

            clickForFirst = !clickForFirst;
        }
    }

    private void resetFields() {
        firstPoint = null;
        secondPoint = null;
        clickForFirst = true;

        updateFields();
    }

    private void updateFields() {
        if (firstPoint != null) {
            firstField.setText(String.valueOf(firstPoint[1]));
        } else {
            firstField.setText(ObjectUtils.EMPTY_STRING);
        }

        if (secondPoint != null) {
            secondField.setText(String.valueOf(secondPoint[1]));
        } else {
            secondField.setText(ObjectUtils.EMPTY_STRING);
        }

        if (firstPoint != null && secondPoint != null) {
            diffField.setText(String.valueOf(firstPoint[1] - secondPoint[1]));
        } else {
            diffField.setText(ObjectUtils.EMPTY_STRING);
        }
    }

    @Override
    public void chartViewerChanged(ChartViewerEvent event) {
        switch (event.getReason()) {
            case SELECTION:
                double[] point = event.getSelectedPoint();
                if (point == null) {
                    resetFields();
                } else {
                    selectedPointChanged(point);
                }
                break;
            default:
                // nothing to do
                break;
        }
    }

}
