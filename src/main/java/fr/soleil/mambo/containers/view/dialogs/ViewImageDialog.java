package fr.soleil.mambo.containers.view.dialogs;

import java.awt.BorderLayout;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.tools.SpringUtilities;

public class ViewImageDialog extends JDialog {

    private static final long serialVersionUID = 8339000354012251430L;

    private ImageViewer imageViewer;
    protected JPanel myPanel;

    public ViewImageDialog(Window parent) {
        super(parent);
        setModal(true);
        initComponents();
        addComponents();
        initBounds();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                clean();
            }
        });
    }

    public void setImageName(String name) {
        setTitle(name);
        if (imageViewer != null) {
            imageViewer.setImageName(name);
        }
    }

    public void setData(double[][] data, String format) {
        if (imageViewer != null) {
            imageViewer.setNumberMatrix(data);
            imageViewer.setFormat(format);
        }
    }

    private void clean() {
        setData(null, null);
    }

    private void initBounds() {
        int x = MamboFrame.getInstance().getX() + MamboFrame.getInstance().getWidth();
        x -= this.getWidth() + 10;
        if (x < 0) {
            x = 0;
        }
        int y = MamboFrame.getInstance().getY() + MamboFrame.getInstance().getHeight();
        y -= this.getHeight();
        y /= 2;
        if (y < 0) {
            y = 0;
        }
        this.setSize(1000, 1000);
        this.setLocation(x, y);
    }

    private void initComponents() {
        myPanel = new JPanel(new BorderLayout());
        GUIUtilities.setObjectBackground(myPanel, GUIUtilities.VIEW_COLOR);
        prepareViewer();
    }

    private void prepareViewer() {
        imageViewer = new ImageViewer();
        imageViewer.setApplicationId("Mambo - " + CometeUtils.generateIdForClass(getClass()));
        imageViewer.setPreferredSnapshotExtension("png");
        imageViewer.setUseMaskManagement(false);
        imageViewer.setEditable(false);
        imageViewer.setCometeBackground(ColorTool.getCometeColor(GUIUtilities.getViewColor()));
    }

    private void addComponents() {
        myPanel.add(imageViewer);
        this.setContentPane(myPanel);
    }

    protected void initLayout() {
        myPanel.setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(myPanel, myPanel.getComponentCount(), 1, 0, 0, 0, 0, true);
    }

}
