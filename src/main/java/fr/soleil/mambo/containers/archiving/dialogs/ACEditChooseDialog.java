package fr.soleil.mambo.containers.archiving.dialogs;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SpringLayout;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.tools.MamboDbUtils;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.SpringUtilities;

/**
 * 
 * @author SOLEIL
 */
public class ACEditChooseDialog extends JDialog {

    private static final long serialVersionUID = 7301069986377508093L;

    public static final int WIDTH = 400, HEIGHT = 130;

    private final JRadioButton hdb, tdb, tts;
    private static ACEditChooseDialog instance;
    private boolean validated = false;

    private ACEditChooseDialog() {
        super(MamboFrame.getInstance(), Messages.getMessage("ARCHIVING_CHOOSE_EDIT_TITLE"), true);
        JPanel panel = new JPanel();
        ButtonGroup dbGroup = new ButtonGroup();
        hdb = new JRadioButton(Messages.getMessage("ARCHIVING_CHOOSE_EDIT_HDB"));
        hdb.setSelected(true);
        tdb = new JRadioButton(Messages.getMessage("ARCHIVING_CHOOSE_EDIT_TDB"));
        tdb.setSelected(false);
        tts = new JRadioButton(Messages.getMessage("ARCHIVING_CHOOSE_EDIT_TTS"));
        tts.setSelected(false);
        MamboDbUtils.updateDbRadioButtons(hdb, tdb, tts);

        dbGroup.add(hdb);
        dbGroup.add(tdb);
        dbGroup.add(tts);
        JButton okButton = new JButton("ok");
        okButton.setToolTipText("ok");
        okButton.addActionListener(new ACECCloseAction());
        panel.setLayout(new SpringLayout());
        panel.add(hdb);
        panel.add(tdb);
        panel.add(tts);
        panel.add(okButton);
        setContentPane(panel);
        setBounds(0, 400, WIDTH, HEIGHT);
        SpringUtilities.makeCompactGrid(panel, 4, 1, // rows, cols
                0, 5, // initX, initY
                0, 5, // xPad, yPad
                true); // every component same size
        GUIUtilities.setObjectBackground(hdb, GUIUtilities.ARCHIVING_COLOR);
        GUIUtilities.setObjectBackground(tdb, GUIUtilities.ARCHIVING_COLOR);
        GUIUtilities.setObjectBackground(tts, GUIUtilities.ARCHIVING_COLOR);
        GUIUtilities.setObjectBackground(panel, GUIUtilities.ARCHIVING_COLOR);
        GUIUtilities.setObjectBackground(this, GUIUtilities.ARCHIVING_COLOR);

        setupCloseOperation();
    }

    public static ACEditChooseDialog getInstance() {
        if (instance == null) {
            instance = new ACEditChooseDialog();
        }
        return instance;
    }

    private void setupCloseOperation() {
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                ACEditChooseDialog.getInstance().setValidated(false);
                ACEditChooseDialog.getInstance().setVisible(false);
            }
        });
    }

    public Boolean isHistoric() {
        Boolean historic;
        if (tts.isSelected()) {
            historic = null;
        } else {
            historic = Boolean.valueOf(hdb.isSelected());
        }
        return historic;
    }

    public boolean hasOnlyOneChoice() {
        return (hdb.isEnabled() && (!tdb.isEnabled()) && (!tts.isEnabled()))
                || (tdb.isEnabled() && (!hdb.isEnabled()) && (!tts.isEnabled()))
                || (tts.isEnabled() && (!hdb.isEnabled()) && (!tdb.isEnabled()));
    }

    /**
     * @return Returns the validated.
     */
    public boolean isValidated() {
        return validated;
    }

    /**
     * @param validated
     *            The validated to set.
     */
    public void setValidated(boolean validated) {
        this.validated = validated;
    }
}
