package fr.soleil.mambo.containers.archiving.dialogs;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

public class ACECCloseAction extends AbstractAction {

    private static final long serialVersionUID = 4478674188313871716L;

    public ACECCloseAction() {
        super();
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        ACEditChooseDialog.getInstance().setValidated(true);
        ACEditChooseDialog.getInstance().setVisible(false);
    }
}