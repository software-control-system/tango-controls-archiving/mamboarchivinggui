//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/containers/archiving/ArchivingAttributesPanel.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ArchivingAttributesPanel.
//						(Claisse Laurent) - 5 juil. 2005
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: ArchivingAttributesPanel.java,v $
// Revision 1.4  2006/05/19 15:05:29  ounsy
// minor changes
//
// Revision 1.3  2006/02/24 12:18:56  ounsy
// modified for HDB/TDB separation
//
// Revision 1.2  2005/11/29 18:28:26  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/14 15:41:20  chinkumo
// Second commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.containers.archiving;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.swing.ui.ColoredSplitPaneUI;
import fr.soleil.mambo.tools.Messages;

public class ArchivingAttributesPanel extends JPanel {

    private static final long serialVersionUID = -6684884163432708226L;

    private static ArchivingAttributesPanel instance = null;

    private JSplitPane splitPane;
    private ArchivingAttributesTreePanel archivingAttributesTreePanel;
    private ArchivingAttributesDetailPanel archivingAttributesDetailPanel;

    private Boolean historic;

    /**
     * @return 8 juil. 2005
     */
    public static ArchivingAttributesPanel getInstance(Boolean historic) {
        if (instance == null) {
            instance = new ArchivingAttributesPanel(historic);
            GUIUtilities.setObjectBackground(instance, GUIUtilities.ARCHIVING_COLOR);
        }

        return instance;
    }

    /**
     *
     */
    private ArchivingAttributesPanel(Boolean historic) {
        super();
        this.historic = historic;

        initComponents();
        addComponents();
    }

    /**
     * 19 juil. 2005
     */
    private void addComponents() {
        setLayout(new GridLayout(1, 1));
        add(splitPane);

        String msg = Messages.getMessage("ARCHIVING_ATTRIBUTES_BORDER");
        TitledBorder tb = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg,
                TitledBorder.CENTER, TitledBorder.TOP);
        setBorder(tb);
    }

    /**
     * 19 juil. 2005
     */
    private void initComponents() {
        archivingAttributesTreePanel = ArchivingAttributesTreePanel.getInstance();
        archivingAttributesDetailPanel = ArchivingAttributesDetailPanel.getInstance(historic);
        Dimension minimumSize = new Dimension(5, 5);
        archivingAttributesTreePanel.setMinimumSize(minimumSize);
        archivingAttributesDetailPanel.setMinimumSize(minimumSize);
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setBackground(GUIUtilities.getArchivingColor());
        splitPane.setLeftComponent(archivingAttributesTreePanel);
        splitPane.setRightComponent(archivingAttributesDetailPanel);
        splitPane.setDividerSize(8);
        splitPane.setDividerLocation(0.3);
        splitPane.setResizeWeight(0);
        splitPane.setOneTouchExpandable(true);
        ColoredSplitPaneUI ui = new ColoredSplitPaneUI();
        ui.setDividerColor(GUIUtilities.getArchivingColor());
        splitPane.setUI(ui);
    }

}
