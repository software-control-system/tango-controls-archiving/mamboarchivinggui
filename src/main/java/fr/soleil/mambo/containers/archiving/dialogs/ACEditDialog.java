package fr.soleil.mambo.containers.archiving.dialogs;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.bean.AttributeTableSelectionBean;
import fr.soleil.archiving.tango.entity.datasource.ITangoEntitiesSelectionManager;
import fr.soleil.mambo.actions.CancelAction;
import fr.soleil.mambo.actions.archiving.ACBackAction;
import fr.soleil.mambo.actions.archiving.ACFinishAction;
import fr.soleil.mambo.actions.archiving.ACNextAction;
import fr.soleil.mambo.components.archiving.ACCustomTabbedPane;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.options.sub.GeneralOptions;
import fr.soleil.mambo.tools.Messages;

public class ACEditDialog extends JDialog {

    private static final long serialVersionUID = 4366658183245969954L;

    private static ACEditDialog instance = null;
    private static ITangoEntitiesSelectionManager defaultTangoManager = GeneralOptions.BUFFERED_TANGO_SELECTION_MANAGER;

    private GeneralTab generalTab;
    private final AttributeTableSelectionBean attributeTableSelectionBean;
    private AttributesPropertiesTab attributesPropertiesTab;
    private JScrollPane attributesScrollPane;

    private JComponent attributesPanel;
    private JPanel myPanel;
    private JButton backButton;
    private JButton nextButton;
    private JButton finishButton;
    private JButton cancelButton;
    private ACCustomTabbedPane jTabbedPane;
    private boolean newAC;

    private String msgAttributes;

    private boolean alternateSelectionMode;
    private final Boolean historic;

    /**
     * @return 8 juil. 2005
     * @throws ArchivingException
     */
    public static ACEditDialog getInstance(final boolean isAlternateSelectionMode, final Boolean historic)
            throws ArchivingException {
        if (instance == null || instance.isAlternateSelectionMode() != isAlternateSelectionMode
                || instance.isHistoric() != historic) {
            instance = new ACEditDialog(isAlternateSelectionMode, historic);
        }

        return instance;
    }

    public static ACEditDialog getInstance() {
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    public static void setDefaultTangoManager(final ITangoEntitiesSelectionManager defaultTangoManager) {
        ACEditDialog.defaultTangoManager = defaultTangoManager;
    }

    private ACEditDialog(final boolean alternateSelectionMode, final Boolean historic) throws ArchivingException {
        super(MamboFrame.getInstance(), Messages.getMessage("DIALOGS_EDIT_AC_TITLE"), true);
        // First, initialize tangoManagerType and selection mode
        this.alternateSelectionMode = false;
        this.historic = historic;
        attributeTableSelectionBean = new AttributeTableSelectionBean();
        attributeTableSelectionBean.setTangoManager(defaultTangoManager);

        initComponents();
        addComponents();
        setSizeAndLocation();

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new ACEditWindowAdapter(this));
        newAC = false;

        // Then, update tangoManagerType and selection mode to their expected
        // value, in order to start the bean if necessary
        setAlternateSelectionMode(alternateSelectionMode);
    }

    public void setNewAC(final boolean ac) {
        this.newAC = ac;
    }

    public boolean isNewAC() {
        return newAC;
    }

    /**
     * 13 sept. 2005
     */
    private void setSizeAndLocation() {
        pack();
        int x = MamboFrame.getInstance().getX() + 50;
        if (x < 0) {
            x = 0;
        }
        int y = MamboFrame.getInstance().getY() + 100;
        final Rectangle r = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        if (y < 0 || y + getHeight() > r.y + r.height) {
            y = 0;
        }

        setLocation(x, y);
    }

    private void initAttributesScrollPane() {
        attributesScrollPane = new JScrollPane(attributeTableSelectionBean.getSelectionPanel());
        attributesScrollPane.setPreferredSize(GUIUtilities.getEditScrollPaneSize());
    }

    /**
     * 5 juil. 2005
     * 
     * @throws ArchivingException
     */
    private void initComponents() throws ArchivingException {
        final String msgBack = Messages.getMessage("DIALOGS_EDIT_AC_BACK");
        final String msgNext = Messages.getMessage("DIALOGS_EDIT_AC_NEXT");
        final String msgFinish = Messages.getMessage("DIALOGS_EDIT_AC_FINISH");
        final String msgCancel = Messages.getMessage("DIALOGS_EDIT_AC_CANCEL");

        generalTab = GeneralTab.getInstance();

        // --------CLA
        if (alternateSelectionMode) {
            initAttributesScrollPane();
            attributesPanel = attributesScrollPane;
        } else {
            attributesPanel = AttributesTab.getInstance();
        }
        // --------CLA

        AttributesPropertiesTab.resetInstance();
        attributesPropertiesTab = AttributesPropertiesTab.getInstance(historic);

        backButton = new JButton(ACBackAction.getInstance(msgBack));
        nextButton = new JButton(ACNextAction.getInstance(msgNext));
        finishButton = new JButton(ACFinishAction.getInstance(msgFinish));
        cancelButton = new JButton(new CancelAction(msgCancel, this));

        backButton.setPreferredSize(new Dimension(20, 30));
        nextButton.setPreferredSize(new Dimension(20, 30));
        finishButton.setPreferredSize(new Dimension(20, 30));
        cancelButton.setPreferredSize(new Dimension(20, 30));

    }

    /**
     * 8 juil. 2005
     */
    private void addComponents() {
        myPanel = new JPanel();
        myPanel.setLayout(new BoxLayout(myPanel, BoxLayout.Y_AXIS));

        setContentPane(myPanel);

        jTabbedPane = ACCustomTabbedPane.getInstance();
        jTabbedPane.removeAll();

        final String msgGeneral = Messages.getMessage("DIALOGS_EDIT_AC_GENERAL_TITLE");
        msgAttributes = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_TITLE");
        final String msgArchivingModes = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_TITLE");

        jTabbedPane.addTab(msgGeneral, generalTab);
        jTabbedPane.addTab(msgAttributes, attributesPanel);
        jTabbedPane.addTab(msgArchivingModes, attributesPropertiesTab);

        initBackAndNextStatus();

        final Box buttonsBox = new Box(BoxLayout.X_AXIS);
        buttonsBox.add(backButton);
        buttonsBox.add(nextButton);
        buttonsBox.add(finishButton);
        buttonsBox.add(cancelButton);

        myPanel.add(jTabbedPane);
        myPanel.add(Box.createVerticalStrut(5));
        myPanel.add(buttonsBox);
    }

    /**
     * 
     */
    private void initBackAndNextStatus() {
        jTabbedPane.setSelectedIndex(0);
        jTabbedPane.setEnabledAt(1, false);
        jTabbedPane.setEnabledAt(2, false);
        final ACBackAction backAction = ACBackAction.getInstance();
        final ACNextAction nextAction = ACNextAction.getInstance();
        final ACFinishAction finishAction = ACFinishAction.getInstance();
        backAction.setEnabled(false);
        nextAction.setEnabled(true);
        finishAction.setEnabled(false);
    }

    /**
     * @return Returns the isAlternateSelectionMode.
     */
    public boolean isAlternateSelectionMode() {
        return alternateSelectionMode;
    }

    /**
     * @param alternateSelectionMode
     *            The isAlternateSelectionMode to set.
     */
    public void setAlternateSelectionMode(final boolean alternateSelectionMode) {
        if (this.alternateSelectionMode != alternateSelectionMode) {
            this.alternateSelectionMode = alternateSelectionMode;
            int index = jTabbedPane.indexOfTab(msgAttributes);
            jTabbedPane.removeTabAt(index);
            if (this.alternateSelectionMode) {
                if (attributesScrollPane == null) {
                    initAttributesScrollPane();
                }
                attributesPanel = attributesScrollPane;
            } else {
                attributesPanel = AttributesTab.getInstance();
            }
            jTabbedPane.insertTab(msgAttributes, null, attributesPanel, null, index);
        }

    }

    public void resetTabbedPane() {
        jTabbedPane.setEnabledAt(0, true);
        initBackAndNextStatus();
    }

    /**
     * @return Returns the isHistoric.
     */
    public Boolean isHistoric() {
        return historic;
    }

    public AttributeTableSelectionBean getAttributeTableSelectionBean() {
        return attributeTableSelectionBean;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class ACEditWindowAdapter extends WindowAdapter {

        private final ACEditDialog toClose;

        public ACEditWindowAdapter(final ACEditDialog toClose) {
            this.toClose = toClose;
        }

        @Override
        public void windowClosing(final WindowEvent e) {
            CancelAction.performCancel(toClose);
        }
    }
}
