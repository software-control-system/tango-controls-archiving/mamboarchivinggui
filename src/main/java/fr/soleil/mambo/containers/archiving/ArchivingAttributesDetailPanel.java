package fr.soleil.mambo.containers.archiving;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.tools.Messages;

public class ArchivingAttributesDetailPanel extends JPanel {

    private static final long serialVersionUID = -5074817574497484666L;

    private static ArchivingAttributesDetailPanel instance = null;

    private JTabbedPane tabbedPane;

    private Boolean historic;

    /**
     * @return 8 juil. 2005
     */
    public static ArchivingAttributesDetailPanel getInstance(Boolean historic) {
        if (instance == null) {
            instance = new ArchivingAttributesDetailPanel(historic);
            GUIUtilities.setObjectBackground(instance, GUIUtilities.ARCHIVING_COLOR);
        }

        return instance;
    }

    public static ArchivingAttributesDetailPanel getInstance() {
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    /**
     *  
     */
    private ArchivingAttributesDetailPanel(Boolean historic) {
        super();
        this.historic = historic;

        initComponents();
        addComponents();
        setLayout();
    }

    /**
     * 19 juil. 2005
     */
    private void setLayout() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    /**
     * 19 juil. 2005
     */
    private void addComponents() {
        this.add(tabbedPane);
    }

    /**
     * 19 juil. 2005
     */
    private void initComponents() {
        tabbedPane = new JTabbedPane();

        if (historic == null) {
            tabbedPane.add(ApiConstants.TTS, ACAttributeModesPanel.getInstance(null));
        } else if (historic.booleanValue()) {
            tabbedPane.add(ApiConstants.HDB, ACAttributeModesPanel.getInstance(Boolean.TRUE));
        } else {
            tabbedPane.add(ApiConstants.TDB, ACAttributeModesPanel.getInstance(Boolean.FALSE));
        }
        tabbedPane.setBackground(new Color(210, 190, 190));

        tabbedPane.revalidate();
        tabbedPane.repaint();

        initBorder();
    }

    /**
     * 19 juil. 2005
     */
    private void initBorder() {
        String msg = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_BORDER");
        TitledBorder tb = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), msg,
                TitledBorder.CENTER, TitledBorder.TOP);
        Border border = (tb);
        this.setBorder(border);

    }

    /**
     * @param historic
     */
    public void refresh(Boolean historic) {
        tabbedPane.removeAll();
        if (historic == null) {
            tabbedPane.add(ApiConstants.TTS, ACAttributeModesPanel.getInstance(null));
        } else if (historic.booleanValue()) {
            tabbedPane.add(ApiConstants.HDB, ACAttributeModesPanel.getInstance(Boolean.TRUE));
        } else {
            tabbedPane.add(ApiConstants.TDB, ACAttributeModesPanel.getInstance(Boolean.FALSE));
        }
    }

}
