package fr.soleil.mambo.containers.archiving.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.archiving.hdbtdb.api.tools.mode.TdbSpec;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.archiving.SetAttributesGroupProperties;
import fr.soleil.mambo.api.archiving.ArchiverFactory;
import fr.soleil.mambo.api.archiving.ArchivingUtils;
import fr.soleil.mambo.api.archiving.IArchiver;
import fr.soleil.mambo.components.archiving.ACAttributesPropertiesTree;
import fr.soleil.mambo.components.archiving.ACTdbExportPeriodComboBox;
import fr.soleil.mambo.components.archiving.ArchiversComboBox;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.mambo.tools.Messages;
import fr.soleil.mambo.tools.SpringUtilities;

public class AttributesPropertiesPanel extends JPanel {

    private static final long serialVersionUID = 2387424016366828846L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AttributesPropertiesPanel.class);

    public static final String DEFAULT_HDB_PERIOD_VALUE = "60000";
    public static final String DEFAULT_TDB_PERIOD_VALUE = "1000";
    public static final String DEFAULT_TTS_PERIOD_VALUE = DEFAULT_TDB_PERIOD_VALUE;

    private final static Dimension CLASSIC_MAX_DIMENSION = new Dimension(Integer.MAX_VALUE, 21);
    private final static Dimension CLASSIC_PREF_DIMENSION = new Dimension(60, 21);
    private final static Dimension CLASSIC_FIXED_DIMENSION = new Dimension(90, 21);

    private static AttributesPropertiesPanel instance = null;

    // Archiver choice objects
    private ArchiverChoiceDefaultCheckBox archiverChoiceDefaultCheckBox;
    private ArchiversComboBox archiverChoiceRunningComboBox;
    private ArchiverChoiceNameTextField archiverChoiceNameTextField;
    private JTextField currentArchiverTextField;
    private JTextField dedicatedArchiverTextField;
    // end Archiver choice objects

    // HDB objects
    private JPanel hdbPanel, hdbPeriodPanel, hdbModePanel;
    private JLabel hdbLabel;
    private JLabel hdbPeriodLabel;
    private JTextField hdbPeriodField;
    private JLabel hdbModesLabel;
    private JCheckBox hdbPeriodicalCheck;
    private JCheckBox hdbAbsoluteCheck;
    private JCheckBox hdbRelativeCheck;
    private JCheckBox hdbThresholdCheck;
    private JCheckBox hdbDifferenceCheck;
    // /////////////////////////
    private JCheckBox hdbAbsoluteDLCheck;
    private JCheckBox hdbRelativeDLCheck;
    // end HDB objects

    // TDB objects
    private JPanel tdbPanel, tdbPeriodPanel, tdbModePanel;
    private JLabel tdbLabel;
    private JLabel tdbPeriodLabel;
    private JTextField tdbPeriodField;
    private JLabel tdbExportPeriodLabel;
    private ACTdbExportPeriodComboBox tdbExportPeriodComboBox;

    private JLabel tdbModesLabel;
    private JCheckBox tdbPeriodicalCheck;
    private JCheckBox tdbAbsoluteCheck;
    private JCheckBox tdbRelativeCheck;
    private JCheckBox tdbThresholdCheck;
    private JCheckBox tdbDifferenceCheck;
    // /////////////////////////
    private JCheckBox tdbAbsoluteDLCheck;
    private JCheckBox tdbRelativeDLCheck;
    // end TDB objects

    // TTS objects
    private JPanel ttsPanel, ttsPeriodPanel, ttsModePanel;
    private JLabel ttsLabel;
    private JLabel ttsPeriodLabel;
    private JTextField ttsPeriodField;
    private JLabel ttsModesLabel;
    private JCheckBox ttsPeriodicalCheck;
    private JCheckBox ttsAbsoluteCheck;
    private JCheckBox ttsRelativeCheck;
    private JCheckBox ttsThresholdCheck;
    private JCheckBox ttsDifferenceCheck;
    // /////////////////////////
    private JCheckBox ttsAbsoluteDLCheck;
    private JCheckBox ttsRelativeDLCheck;
    // end TTS objects

    private JLabel hdbPeriodicalLabel;
    private JLabel hdbAbsoluteLabel;
    private JLabel hdbRelativeLabel;
    private JLabel hdbThresholdLabel;
    private JLabel hdbDifferenceLabel;

    private JLabel tdbPeriodicalLabel;
    private JLabel tdbAbsoluteLabel;
    private JLabel tdbRelativeLabel;
    private JLabel tdbThresholdLabel;
    private JLabel tdbDifferenceLabel;

    private JLabel ttsPeriodicalLabel;
    private JLabel ttsAbsoluteLabel;
    private JLabel ttsRelativeLabel;
    private JLabel ttsThresholdLabel;
    private JLabel ttsDifferenceLabel;

    private JTextField hdbAbsolutePeriodField;
    private JTextField tdbAbsolutePeriodField;
    private JTextField ttsAbsolutePeriodField;

    private JTextField hdbAbsoluteLowerField;
    private JTextField tdbAbsoluteLowerField;
    private JTextField ttsAbsoluteLowerField;

    private JTextField hdbAbsoluteUpperField;
    private JTextField tdbAbsoluteUpperField;
    private JTextField ttsAbsoluteUpperField;

    private JTextField hdbRelativePeriodField;
    private JTextField tdbRelativePeriodField;
    private JTextField ttsRelativePeriodField;

    private JTextField hdbRelativeLowerField;
    private JTextField tdbRelativeLowerField;
    private JTextField ttsRelativeLowerField;

    private JTextField hdbRelativeUpperField;
    private JTextField tdbRelativeUpperField;
    private JTextField ttsRelativeUpperField;

    private JTextField hdbThresholdPeriodField;
    private JTextField tdbThresholdPeriodField;
    private JTextField ttsThresholdPeriodField;

    private JTextField hdbThresholdLowerField;
    private JTextField tdbThresholdLowerField;
    private JTextField ttsThresholdLowerField;

    private JTextField hdbThresholdUpperField;
    private JTextField tdbThresholdUpperField;
    private JTextField ttsThresholdUpperField;

    private JTextField hdbDifferencePeriodField;
    private JTextField tdbDifferencePeriodField;
    private JTextField ttsDifferencePeriodField;

    private JLabel hdbAbsolutePeriodLabel;
    private JLabel tdbAbsolutePeriodLabel;
    private JLabel ttsAbsolutePeriodLabel;

    private JLabel hdbAbsoluteLowerLabel;
    private JLabel tdbAbsoluteLowerLabel;
    private JLabel ttsAbsoluteLowerLabel;

    private JLabel hdbAbsoluteUpperLabel;
    private JLabel tdbAbsoluteUpperLabel;
    private JLabel ttsAbsoluteUpperLabel;

    private JLabel hdbRelativePeriodLabel;
    private JLabel tdbRelativePeriodLabel;
    private JLabel ttsRelativePeriodLabel;

    private JLabel hdbRelativeLowerLabel;
    private JLabel tdbRelativeLowerLabel;
    private JLabel ttsRelativeLowerLabel;

    private JLabel hdbRelativeUpperLabel;
    private JLabel tdbRelativeUpperLabel;
    private JLabel ttsRelativeUpperLabel;

    private JLabel hdbThresholdPeriodLabel;
    private JLabel tdbThresholdPeriodLabel;
    private JLabel ttsThresholdPeriodLabel;

    private JLabel hdbThresholdLowerLabel;
    private JLabel tdbThresholdLowerLabel;
    private JLabel ttsThresholdLowerLabel;

    private JLabel hdbThresholdUpperLabel;
    private JLabel tdbThresholdUpperLabel;
    private JLabel ttsThresholdUpperLabel;

    private JLabel hdbDifferencePeriodLabel;
    private JLabel tdbDifferencePeriodLabel;
    private JLabel ttsDifferencePeriodLabel;

    // general objects
    private JButton setButton;
    private JPanel centerPanel;
    private JScrollPane centerScrollPane;

    // end general objects

    private final Boolean historic;

    private Mode lastMode;

    /**
     * @param b
     * @return 8 juil. 2005
     * @throws ArchivingException
     */
    public static AttributesPropertiesPanel getInstance(final Boolean historic) throws ArchivingException {
        if (instance == null || !ObjectUtils.sameObject(instance.isHistoric(), historic)) {
            instance = new AttributesPropertiesPanel(historic);
        }
        return instance;
    }

    public static AttributesPropertiesPanel getInstance() {
        return instance;
    }

    /**
     * 
     */
    public static void resetInstance() {
        instance = null;
    }

    /**
     * @throws ArchivingException
     * 
     */
    private AttributesPropertiesPanel(final Boolean historic) throws ArchivingException {
        super(new BorderLayout(5, 5));
        this.historic = historic;

        initComponents(historic);
        addComponents();

        ArchivingConfigurationAttributeProperties.resetCurrentProperties();
    }

    /**
     * 19 juil. 2005
     */
    private void addComponents() {
        add(getWarningPanel(), BorderLayout.NORTH);
        add(centerScrollPane, BorderLayout.CENTER);
        add(setButton, BorderLayout.SOUTH);
    }

    /**
     * 19 juil. 2005
     * 
     * @throws ArchivingException
     */
    private void initComponents(final Boolean historic) throws ArchivingException {
        final String msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_SET");
        setButton = new JButton(new SetAttributesGroupProperties(msg));
        setButton.setMaximumSize(CLASSIC_MAX_DIMENSION);
        setButton.setPreferredSize(CLASSIC_PREF_DIMENSION);

        initCenterPanel(historic);
    }

    /**
     * 20 juil. 2005
     * 
     * @throws ArchivingException
     */
    private void initCenterPanel(final Boolean historic) throws ArchivingException {
        initCenterComponents(historic);
        centerPanel.setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(centerPanel, centerPanel.getComponentCount(), 1, 0, 0, 0, 0, true);
        centerScrollPane = new JScrollPane(centerPanel);
        centerScrollPane.setMinimumSize(new Dimension(0, 0));
        final ArchivingConfigurationAttributeProperties currentProperties = new ArchivingConfigurationAttributeProperties();
        ArchivingConfigurationAttributeProperties.setCurrentProperties(currentProperties);
    }

    private void initCenterComponents(final Boolean historic) throws ArchivingException {
        centerPanel = new JPanel();

        archiverChoiceDefaultCheckBox = new ArchiverChoiceDefaultCheckBox();
        archiverChoiceRunningComboBox = new ArchiversComboBox(this.historic);
        archiverChoiceNameTextField = new ArchiverChoiceNameTextField(" ");
        archiverChoiceNameTextField.setEditable(false);

        currentArchiverTextField = new JTextField(" ");
        currentArchiverTextField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        currentArchiverTextField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        currentArchiverTextField.setEditable(false);

        dedicatedArchiverTextField = new JTextField(" ");
        dedicatedArchiverTextField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        dedicatedArchiverTextField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        dedicatedArchiverTextField.setEditable(false);

        final boolean initialConditions = true;
        archiverChoiceDefaultCheckBox.setSelected(initialConditions);
        setArchiverDefault(initialConditions);

        String msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_MODES");
        String tooltip;
        hdbModesLabel = new JLabel(msg);
        hdbModesLabel.setFont(hdbModesLabel.getFont().deriveFont(Font.ITALIC));
        hdbModesLabel.setPreferredSize(CLASSIC_PREF_DIMENSION);
        hdbModesLabel.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbModesLabel = new JLabel(msg);
        tdbModesLabel.setFont(tdbModesLabel.getFont().deriveFont(Font.ITALIC));
        tdbModesLabel.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbModesLabel.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsModesLabel = new JLabel(msg);
        ttsModesLabel.setFont(ttsModesLabel.getFont().deriveFont(Font.ITALIC));
        ttsModesLabel.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsModesLabel.setMaximumSize(CLASSIC_MAX_DIMENSION);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIODICAL_MODE");
        tooltip = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIODICAL_MODE_HELP");
        hdbPeriodicalLabel = new JLabel(msg);
        hdbPeriodicalLabel.setToolTipText(tooltip);
        tdbPeriodicalLabel = new JLabel(msg);
        tdbPeriodicalLabel.setToolTipText(tooltip);
        ttsPeriodicalLabel = new JLabel(msg);
        ttsPeriodicalLabel.setToolTipText(tooltip);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_ABSOLUTE_MODE");
        tooltip = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_ABSOLUTE_MODE_HELP");
        hdbAbsoluteLabel = new JLabel(msg);
        hdbAbsoluteLabel.setToolTipText(tooltip);
        tdbAbsoluteLabel = new JLabel(msg);
        tdbAbsoluteLabel.setToolTipText(tooltip);
        ttsAbsoluteLabel = new JLabel(msg);
        ttsAbsoluteLabel.setToolTipText(tooltip);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_RELATIVE_MODE");
        tooltip = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_RELATIVE_MODE_HELP");
        hdbRelativeLabel = new JLabel(msg);
        hdbRelativeLabel.setToolTipText(tooltip);
        tdbRelativeLabel = new JLabel(msg);
        hdbRelativeLabel.setToolTipText(tooltip);
        ttsRelativeLabel = new JLabel(msg);
        hdbRelativeLabel.setToolTipText(tooltip);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_THRESHOLD_MODE");
        tooltip = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_THRESHOLD_MODE_HELP");
        hdbThresholdLabel = new JLabel(msg);
        hdbThresholdLabel.setToolTipText(tooltip);
        tdbThresholdLabel = new JLabel(msg);
        tdbThresholdLabel.setToolTipText(tooltip);
        ttsThresholdLabel = new JLabel(msg);
        ttsThresholdLabel.setToolTipText(tooltip);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_DIFFERENCE_MODE");
        tooltip = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_DIFFERENCE_MODE_HELP");
        hdbDifferenceLabel = new JLabel(msg);
        hdbDifferenceLabel.setToolTipText(tooltip);
        tdbDifferenceLabel = new JLabel(msg);
        tdbDifferenceLabel.setToolTipText(tooltip);
        ttsDifferenceLabel = new JLabel(msg);
        ttsDifferenceLabel.setToolTipText(tooltip);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_HDB");
        hdbLabel = new JLabel(msg);
        hdbLabel.setFont(hdbLabel.getFont().deriveFont(Font.BOLD));
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_TDB");
        tdbLabel = new JLabel(msg);
        tdbLabel.setFont(tdbLabel.getFont().deriveFont(Font.BOLD));
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_TTS");
        ttsLabel = new JLabel(msg);
        ttsLabel.setFont(ttsLabel.getFont().deriveFont(Font.BOLD));

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_HDB");
        hdbPeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_TDB");
        tdbPeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_TTS");
        ttsPeriodLabel = new JLabel(msg);

        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD");
        tdbExportPeriodLabel = new JLabel(msg);
        tdbExportPeriodComboBox = new ACTdbExportPeriodComboBox();

        // set this parameter not visible, its inserted as tdbarchiver class property
        tdbExportPeriodLabel.setVisible(false);
        tdbExportPeriodComboBox.setVisible(false);

        hdbPeriodField = new JTextField();
        tdbPeriodField = new JTextField();
        ttsPeriodField = new JTextField();

        hdbPeriodField.setMaximumSize(CLASSIC_FIXED_DIMENSION);
        hdbPeriodField.setPreferredSize(CLASSIC_FIXED_DIMENSION);
        tdbPeriodField.setMaximumSize(CLASSIC_FIXED_DIMENSION);
        tdbPeriodField.setPreferredSize(CLASSIC_FIXED_DIMENSION);
        ttsPeriodField.setMaximumSize(CLASSIC_FIXED_DIMENSION);
        ttsPeriodField.setPreferredSize(CLASSIC_FIXED_DIMENSION);

        tdbExportPeriodComboBox.setMaximumSize(CLASSIC_FIXED_DIMENSION);
        tdbExportPeriodComboBox.setPreferredSize(CLASSIC_FIXED_DIMENSION);

        hdbPeriodicalCheck = new JCheckBox();
        hdbAbsoluteCheck = new JCheckBox();
        hdbRelativeCheck = new JCheckBox();
        hdbThresholdCheck = new JCheckBox();
        hdbDifferenceCheck = new JCheckBox();

        tdbPeriodicalCheck = new JCheckBox();
        tdbAbsoluteCheck = new JCheckBox();
        tdbRelativeCheck = new JCheckBox();
        tdbThresholdCheck = new JCheckBox();
        tdbDifferenceCheck = new JCheckBox();

        ttsPeriodicalCheck = new JCheckBox();
        ttsAbsoluteCheck = new JCheckBox();
        ttsRelativeCheck = new JCheckBox();
        ttsThresholdCheck = new JCheckBox();
        ttsDifferenceCheck = new JCheckBox();

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_PERIOD_HDB");
        hdbAbsolutePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_PERIOD_TDB");
        tdbAbsolutePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_PERIOD_TTS");
        ttsAbsolutePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_LOWER");
        hdbAbsoluteLowerLabel = new JLabel(msg);
        tdbAbsoluteLowerLabel = new JLabel(msg);
        ttsAbsoluteLowerLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_ABSOLUTE_UPPER");
        hdbAbsoluteUpperLabel = new JLabel(msg);
        tdbAbsoluteUpperLabel = new JLabel(msg);
        ttsAbsoluteUpperLabel = new JLabel(msg);

        hdbAbsolutePeriodField = new JTextField();
        tdbAbsolutePeriodField = new JTextField();
        ttsAbsolutePeriodField = new JTextField();
        hdbAbsoluteLowerField = new JTextField();
        tdbAbsoluteLowerField = new JTextField();
        ttsAbsoluteLowerField = new JTextField();
        hdbAbsoluteUpperField = new JTextField();
        tdbAbsoluteUpperField = new JTextField();
        ttsAbsoluteUpperField = new JTextField();
        hdbAbsolutePeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbAbsolutePeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbAbsolutePeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbAbsolutePeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsAbsolutePeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsAbsolutePeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        hdbAbsoluteLowerField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbAbsoluteLowerField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbAbsoluteLowerField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbAbsoluteLowerField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsAbsoluteLowerField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsAbsoluteLowerField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        hdbAbsoluteUpperField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbAbsoluteUpperField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbAbsoluteUpperField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbAbsoluteUpperField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsAbsoluteUpperField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsAbsoluteUpperField.setPreferredSize(CLASSIC_PREF_DIMENSION);

        msg = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_SLOW_DRIFT");
        tooltip = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODEA_SLOW_DRIFT_HELP");
        hdbAbsoluteDLCheck = new JCheckBox(msg);
        hdbAbsoluteDLCheck.setToolTipText(tooltip);
        tdbAbsoluteDLCheck = new JCheckBox(msg);
        tdbAbsoluteDLCheck.setToolTipText(tooltip);
        ttsAbsoluteDLCheck = new JCheckBox(msg);
        ttsAbsoluteDLCheck.setToolTipText(tooltip);

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_PERIOD_HDB");
        hdbRelativePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_PERIOD_TDB");
        tdbRelativePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_PERIOD_TTS");
        ttsRelativePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_LOWER");
        hdbRelativeLowerLabel = new JLabel(msg);
        tdbRelativeLowerLabel = new JLabel(msg);
        ttsRelativeLowerLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_RELATIVE_UPPER");
        hdbRelativeUpperLabel = new JLabel(msg);
        tdbRelativeUpperLabel = new JLabel(msg);
        ttsRelativeUpperLabel = new JLabel(msg);

        hdbRelativePeriodField = new JTextField();
        tdbRelativePeriodField = new JTextField();
        ttsRelativePeriodField = new JTextField();
        hdbRelativeLowerField = new JTextField();
        tdbRelativeLowerField = new JTextField();
        ttsRelativeLowerField = new JTextField();
        hdbRelativeUpperField = new JTextField();
        tdbRelativeUpperField = new JTextField();
        ttsRelativeUpperField = new JTextField();
        hdbRelativePeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbRelativePeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbRelativePeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbRelativePeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsRelativePeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsRelativePeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        hdbRelativeLowerField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbRelativeLowerField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbRelativeLowerField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbRelativeLowerField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsRelativeLowerField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsRelativeLowerField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        hdbRelativeUpperField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbRelativeUpperField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbRelativeUpperField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbRelativeUpperField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsRelativeUpperField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsRelativeUpperField.setPreferredSize(CLASSIC_PREF_DIMENSION);

        msg = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_SLOW_DRIFT");
        tooltip = Messages.getMessage("ARCHIVING_ATTRIBUTES_DETAIL_MODER_SLOW_DRIFT_HELP");
        hdbRelativeDLCheck = new JCheckBox(msg);
        hdbRelativeDLCheck.setToolTipText(tooltip);
        tdbRelativeDLCheck = new JCheckBox(msg);
        tdbRelativeDLCheck.setToolTipText(tooltip);
        ttsRelativeDLCheck = new JCheckBox(msg);
        ttsRelativeDLCheck.setToolTipText(tooltip);

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_PERIOD_HDB");
        hdbThresholdPeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_PERIOD_TDB");
        tdbThresholdPeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_PERIOD_TTS");
        ttsThresholdPeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_LOWER");
        hdbThresholdLowerLabel = new JLabel(msg);
        tdbThresholdLowerLabel = new JLabel(msg);
        ttsThresholdLowerLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_THRESHOLD_UPPER");
        hdbThresholdUpperLabel = new JLabel(msg);
        tdbThresholdUpperLabel = new JLabel(msg);
        ttsThresholdUpperLabel = new JLabel(msg);

        hdbThresholdPeriodField = new JTextField();
        tdbThresholdPeriodField = new JTextField();
        ttsThresholdPeriodField = new JTextField();
        hdbThresholdLowerField = new JTextField();
        tdbThresholdLowerField = new JTextField();
        ttsThresholdLowerField = new JTextField();
        hdbThresholdUpperField = new JTextField();
        tdbThresholdUpperField = new JTextField();
        ttsThresholdUpperField = new JTextField();
        hdbThresholdPeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbThresholdPeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbThresholdPeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbThresholdPeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsThresholdPeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsThresholdPeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        hdbThresholdLowerField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbThresholdLowerField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbThresholdLowerField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbThresholdLowerField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsThresholdLowerField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsThresholdLowerField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        hdbThresholdUpperField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbThresholdUpperField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbThresholdUpperField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbThresholdUpperField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsThresholdUpperField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsThresholdUpperField.setPreferredSize(CLASSIC_PREF_DIMENSION);

        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_DIFFERENCE_PERIOD_HDB");
        hdbDifferencePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_DIFFERENCE_PERIOD_TDB");
        tdbDifferencePeriodLabel = new JLabel(msg);
        msg = Messages.getMessage("DIALOGS_OPTIONS_AC_DEFAULT_DIFFERENCE_PERIOD_TTS");
        ttsDifferencePeriodLabel = new JLabel(msg);

        hdbDifferencePeriodField = new JTextField();
        tdbDifferencePeriodField = new JTextField();
        ttsDifferencePeriodField = new JTextField();
        hdbDifferencePeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        hdbDifferencePeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        tdbDifferencePeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        tdbDifferencePeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);
        ttsDifferencePeriodField.setMaximumSize(CLASSIC_MAX_DIMENSION);
        ttsDifferencePeriodField.setPreferredSize(CLASSIC_PREF_DIMENSION);

        initCenterSubPanel(historic);
    }

    /**
     * 20 juil. 2005
     */
    private void initCenterSubPanel(final Boolean historic) {
        final Dimension size = tdbExportPeriodComboBox.getPreferredSize();
        size.height = 20;

        if (historic == null) {
            ttsPanel = new JPanel();
            ttsPanel.setLayout(new SpringLayout());

            ttsPeriodPanel = new JPanel();
            ttsPeriodPanel.setLayout(new SpringLayout());

            ttsModePanel = new JPanel();
            ttsModePanel.setLayout(new SpringLayout());

            ttsPeriodField.setPreferredSize(size);

            ttsPeriodPanel.add(ttsPeriodLabel);
            ttsPeriodPanel.add(ttsPeriodField);
            ttsPeriodPanel.add(Box.createHorizontalGlue());

            final JPanel panelM = new JPanel();
            final JPanel panelP = new JPanel();
            final JPanel panelA = new JPanel();
            final JPanel panelR = new JPanel();
            final JPanel panelT = new JPanel();
            final JPanel panelD = new JPanel();
            ttsModePanel.add(panelM);
            ttsModePanel.add(panelP);
            ttsModePanel.add(panelA);
            ttsModePanel.add(panelR);
            ttsModePanel.add(panelT);
            ttsModePanel.add(panelD);
            SpringUtilities.makeCompactGrid(ttsModePanel, 6, 1, 3, 3, 3, 5, true);

            panelM.setLayout(new SpringLayout());
            panelM.add(ttsModesLabel);
            SpringUtilities.makeCompactGrid(panelM, 1, 1, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            // Periodic Mode
            panelP.setBorder(BorderFactory.createEtchedBorder());
            panelP.setLayout(new SpringLayout());
            panelP.add(ttsPeriodicalLabel);
            panelP.add(ttsPeriodicalCheck);
            SpringUtilities.makeCompactGrid(panelP, 1, 2, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            // Absolute Mode
            panelA.setBorder(BorderFactory.createEtchedBorder());
            panelA.add(ttsAbsoluteLabel);
            panelA.add(ttsAbsoluteCheck);
            panelA.add(Box.createHorizontalGlue());
            panelA.add(Box.createHorizontalGlue());
            panelA.add(Box.createHorizontalGlue());

            panelA.add(ttsAbsolutePeriodLabel);
            panelA.add(ttsAbsolutePeriodField);
            panelA.add(Box.createHorizontalGlue());

            panelA.add(ttsAbsoluteDLCheck);
            panelA.add(Box.createHorizontalGlue());

            panelA.add(ttsAbsoluteLowerLabel);
            panelA.add(ttsAbsoluteLowerField);
            panelA.add(Box.createHorizontalGlue());
            panelA.add(ttsAbsoluteUpperLabel);
            panelA.add(ttsAbsoluteUpperField);
            panelA.setLayout(new SpringLayout());
            SpringUtilities.makeCompactGrid(panelA, 3, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            setAbsoluteModeParameters(ttsAbsoluteCheck.isSelected(), historic);
            ttsAbsoluteCheck.addChangeListener(e -> {
                setAbsoluteModeParameters(ttsAbsoluteCheck.isSelected(), historic);
            });

            // Relative Mode
            panelR.setLayout(new SpringLayout());

            panelR.setBorder(BorderFactory.createEtchedBorder());

            panelR.add(ttsRelativeLabel);
            panelR.add(ttsRelativeCheck);
            panelR.add(Box.createHorizontalGlue());
            panelR.add(Box.createHorizontalGlue());
            panelR.add(Box.createHorizontalGlue());

            panelR.add(ttsRelativePeriodLabel);
            panelR.add(ttsRelativePeriodField);
            panelR.add(Box.createHorizontalGlue());
            panelR.add(ttsRelativeDLCheck);
            panelR.add(Box.createHorizontalGlue());

            panelR.add(ttsRelativeLowerLabel);
            panelR.add(ttsRelativeLowerField);
            panelR.add(Box.createHorizontalGlue());
            panelR.add(ttsRelativeUpperLabel);
            panelR.add(ttsRelativeUpperField);
            SpringUtilities.makeCompactGrid(panelR, 3, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            setRelativeModeParameters(ttsThresholdCheck.isSelected(), historic);
            ttsRelativeCheck.addChangeListener(e -> {
                setRelativeModeParameters(ttsRelativeCheck.isSelected(), historic);
            });

            // Threshhold Mode
            panelT.setLayout(new SpringLayout());
            panelT.setBorder(BorderFactory.createEtchedBorder());

            panelT.add(ttsThresholdLabel);
            panelT.add(ttsThresholdCheck);
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());

            panelT.add(ttsThresholdPeriodLabel);
            panelT.add(ttsThresholdPeriodField);
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());

            panelT.add(ttsThresholdLowerLabel);
            panelT.add(ttsThresholdLowerField);
            panelT.add(Box.createHorizontalGlue());
            panelT.add(ttsThresholdUpperLabel);
            panelT.add(ttsThresholdUpperField);
            SpringUtilities.makeCompactGrid(panelT, 3, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            setThresholdModeParameters(ttsThresholdCheck.isSelected(), historic);
            ttsThresholdCheck.addChangeListener(e -> {
                setThresholdModeParameters(ttsThresholdCheck.isSelected(), historic);
            });

            // Difference Mode
            panelD.setLayout(new SpringLayout());
            panelD.setBorder(BorderFactory.createEtchedBorder());

            panelD.add(ttsDifferenceLabel);
            panelD.add(ttsDifferenceCheck);
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());

            panelD.add(ttsDifferencePeriodLabel);
            panelD.add(ttsDifferencePeriodField);
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());

            SpringUtilities.makeCompactGrid(panelD, 2, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);
            setDifferenceModeParameters(ttsDifferenceCheck.isSelected(), historic);
            ttsDifferenceCheck.addChangeListener(e -> {
                setDifferenceModeParameters(ttsDifferenceCheck.isSelected(), historic);
            });

            // ----------------------------

            ttsPanel.add(ttsPeriodPanel);
            ttsPanel.add(Box.createVerticalGlue());
            ttsPanel.add(ttsModePanel);

            // Lay out the panels.
            SpringUtilities.makeCompactGrid(ttsPeriodPanel, 1, ttsPeriodPanel.getComponentCount(), // rows,
                    // cols
                    0, 0, // initX, initY
                    0, 5, // xPad, yPad
                    true);
            SpringUtilities.makeCompactGrid(ttsPanel, ttsPanel.getComponentCount(), 1, // rows,
                    // cols
                    0, 0, // initX, initY
                    0, 0, // xPad, yPad
                    true);

            centerPanel.add(ttsPanel);
        } else if (historic.booleanValue()) {
            hdbPanel = new JPanel();
            hdbPanel.setLayout(new SpringLayout());

            hdbPeriodPanel = new JPanel();
            hdbPeriodPanel.setLayout(new SpringLayout());

            hdbModePanel = new JPanel();
            hdbModePanel.setLayout(new SpringLayout());

            hdbPeriodField.setPreferredSize(size);

            hdbPeriodPanel.add(hdbPeriodLabel);
            hdbPeriodPanel.add(hdbPeriodField);
            hdbPeriodPanel.add(Box.createHorizontalGlue());

            final JPanel panelM = new JPanel();
            final JPanel panelP = new JPanel();
            final JPanel panelA = new JPanel();
            final JPanel panelR = new JPanel();
            final JPanel panelT = new JPanel();
            final JPanel panelD = new JPanel();
            hdbModePanel.add(panelM);
            hdbModePanel.add(panelP);
            hdbModePanel.add(panelA);
            hdbModePanel.add(panelR);
            hdbModePanel.add(panelT);
            hdbModePanel.add(panelD);
            SpringUtilities.makeCompactGrid(hdbModePanel, 6, 1, 3, 3, 3, 5, true);

            panelM.setLayout(new SpringLayout());
            panelM.add(hdbModesLabel);
            SpringUtilities.makeCompactGrid(panelM, 1, 1, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            // Periodic Mode
            panelP.setBorder(BorderFactory.createEtchedBorder());
            panelP.setLayout(new SpringLayout());
            panelP.add(hdbPeriodicalLabel);
            panelP.add(hdbPeriodicalCheck);
            SpringUtilities.makeCompactGrid(panelP, 1, 2, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            // Absolute Mode
            panelA.setBorder(BorderFactory.createEtchedBorder());
            panelA.add(hdbAbsoluteLabel);
            panelA.add(hdbAbsoluteCheck);
            panelA.add(Box.createHorizontalGlue());
            panelA.add(Box.createHorizontalGlue());
            panelA.add(Box.createHorizontalGlue());

            panelA.add(hdbAbsolutePeriodLabel);
            panelA.add(hdbAbsolutePeriodField);
            panelA.add(Box.createHorizontalGlue());

            panelA.add(hdbAbsoluteDLCheck);
            panelA.add(Box.createHorizontalGlue());

            panelA.add(hdbAbsoluteLowerLabel);
            panelA.add(hdbAbsoluteLowerField);
            panelA.add(Box.createHorizontalGlue());
            panelA.add(hdbAbsoluteUpperLabel);
            panelA.add(hdbAbsoluteUpperField);
            panelA.setLayout(new SpringLayout());
            SpringUtilities.makeCompactGrid(panelA, 3, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            setAbsoluteModeParameters(hdbAbsoluteCheck.isSelected(), historic);
            hdbAbsoluteCheck.addChangeListener(e -> {
                setAbsoluteModeParameters(hdbAbsoluteCheck.isSelected(), historic);
            });

            // Relative Mode
            panelR.setLayout(new SpringLayout());

            panelR.setBorder(BorderFactory.createEtchedBorder());

            panelR.add(hdbRelativeLabel);
            panelR.add(hdbRelativeCheck);
            panelR.add(Box.createHorizontalGlue());
            panelR.add(Box.createHorizontalGlue());
            panelR.add(Box.createHorizontalGlue());

            panelR.add(hdbRelativePeriodLabel);
            panelR.add(hdbRelativePeriodField);
            panelR.add(Box.createHorizontalGlue());
            panelR.add(hdbRelativeDLCheck);
            panelR.add(Box.createHorizontalGlue());

            panelR.add(hdbRelativeLowerLabel);
            panelR.add(hdbRelativeLowerField);
            panelR.add(Box.createHorizontalGlue());
            panelR.add(hdbRelativeUpperLabel);
            panelR.add(hdbRelativeUpperField);
            SpringUtilities.makeCompactGrid(panelR, 3, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            setRelativeModeParameters(hdbThresholdCheck.isSelected(), historic);
            hdbRelativeCheck.addChangeListener(e -> {
                setRelativeModeParameters(hdbRelativeCheck.isSelected(), historic);
            });

            // Threshhold Mode
            panelT.setLayout(new SpringLayout());
            panelT.setBorder(BorderFactory.createEtchedBorder());

            panelT.add(hdbThresholdLabel);
            panelT.add(hdbThresholdCheck);
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());

            panelT.add(hdbThresholdPeriodLabel);
            panelT.add(hdbThresholdPeriodField);
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());

            panelT.add(hdbThresholdLowerLabel);
            panelT.add(hdbThresholdLowerField);
            panelT.add(Box.createHorizontalGlue());
            panelT.add(hdbThresholdUpperLabel);
            panelT.add(hdbThresholdUpperField);
            SpringUtilities.makeCompactGrid(panelT, 3, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            setThresholdModeParameters(hdbThresholdCheck.isSelected(), historic);
            hdbThresholdCheck.addChangeListener(e -> {
                setThresholdModeParameters(hdbThresholdCheck.isSelected(), historic);
            });

            // Difference Mode
            panelD.setLayout(new SpringLayout());
            panelD.setBorder(BorderFactory.createEtchedBorder());

            panelD.add(hdbDifferenceLabel);
            panelD.add(hdbDifferenceCheck);
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());

            panelD.add(hdbDifferencePeriodLabel);
            panelD.add(hdbDifferencePeriodField);
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());

            SpringUtilities.makeCompactGrid(panelD, 2, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);
            setDifferenceModeParameters(hdbDifferenceCheck.isSelected(), historic);
            hdbDifferenceCheck.addChangeListener(e -> {
                setDifferenceModeParameters(hdbDifferenceCheck.isSelected(), historic);
            });

            // ----------------------------

            hdbPanel.add(hdbPeriodPanel);
            hdbPanel.add(Box.createVerticalGlue());
            hdbPanel.add(hdbModePanel);

            // Lay out the panels.
            SpringUtilities.makeCompactGrid(hdbPeriodPanel, 1, hdbPeriodPanel.getComponentCount(), // rows,
                    // cols
                    0, 0, // initX, initY
                    0, 5, // xPad, yPad
                    true);
            SpringUtilities.makeCompactGrid(hdbPanel, hdbPanel.getComponentCount(), 1, // rows,
                    // cols
                    0, 0, // initX, initY
                    0, 0, // xPad, yPad
                    true);

            centerPanel.add(hdbPanel);
        } else {
            tdbPanel = new JPanel();
            tdbPanel.setLayout(new SpringLayout());

            tdbPeriodPanel = new JPanel();
            tdbPeriodPanel.setLayout(new SpringLayout());

            tdbModePanel = new JPanel();
            tdbModePanel.setLayout(new SpringLayout());

            tdbPeriodPanel.add(tdbPeriodLabel);
            tdbPeriodPanel.add(tdbPeriodField);
            tdbPeriodPanel.add(Box.createHorizontalGlue());

            // --------------------

            final JPanel panelM = new JPanel();
            final JPanel panelP = new JPanel();
            final JPanel panelA = new JPanel();
            final JPanel panelR = new JPanel();
            final JPanel panelT = new JPanel();
            final JPanel panelD = new JPanel();
            tdbModePanel.add(panelM);
            tdbModePanel.add(panelP);
            tdbModePanel.add(panelA);
            tdbModePanel.add(panelR);
            tdbModePanel.add(panelT);
            tdbModePanel.add(panelD);
            SpringUtilities.makeCompactGrid(tdbModePanel, 6, 1, 3, 3, 3, 5, true);

            panelM.setLayout(new SpringLayout());
            panelM.add(tdbModesLabel);
            SpringUtilities.makeCompactGrid(panelM, 1, 1, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            // Periodic Mode
            panelP.setBorder(BorderFactory.createEtchedBorder());
            panelP.setLayout(new SpringLayout());
            panelP.add(tdbPeriodicalLabel);
            panelP.add(tdbPeriodicalCheck);
            SpringUtilities.makeCompactGrid(panelP, 1, 2, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            // Absolute Mode

            panelA.setBorder(BorderFactory.createEtchedBorder());
            panelA.add(tdbAbsoluteLabel);
            panelA.add(tdbAbsoluteCheck);
            panelA.add(Box.createHorizontalGlue());
            panelA.add(Box.createHorizontalGlue());
            panelA.add(Box.createHorizontalGlue());

            panelA.add(tdbAbsolutePeriodLabel);
            panelA.add(tdbAbsolutePeriodField);
            panelA.add(Box.createHorizontalGlue());

            panelA.add(tdbAbsoluteDLCheck);
            panelA.add(Box.createHorizontalGlue());

            panelA.add(tdbAbsoluteLowerLabel);
            panelA.add(tdbAbsoluteLowerField);
            panelA.add(Box.createHorizontalGlue());
            panelA.add(tdbAbsoluteUpperLabel);
            panelA.add(tdbAbsoluteUpperField);
            panelA.setLayout(new SpringLayout());
            SpringUtilities.makeCompactGrid(panelA, 3, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            setAbsoluteModeParameters(tdbAbsoluteCheck.isSelected(), historic);
            tdbAbsoluteCheck.addChangeListener(e -> {
                setAbsoluteModeParameters(tdbAbsoluteCheck.isSelected(), historic);
            });

            // Relative Mode
            panelR.setLayout(new SpringLayout());

            panelR.setBorder(BorderFactory.createEtchedBorder());

            panelR.add(tdbRelativeLabel);
            panelR.add(tdbRelativeCheck);
            panelR.add(Box.createHorizontalGlue());
            panelR.add(Box.createHorizontalGlue());
            panelR.add(Box.createHorizontalGlue());

            panelR.add(tdbRelativePeriodLabel);
            panelR.add(tdbRelativePeriodField);
            panelR.add(Box.createHorizontalGlue());
            panelR.add(tdbRelativeDLCheck);
            panelR.add(Box.createHorizontalGlue());

            panelR.add(tdbRelativeLowerLabel);
            panelR.add(tdbRelativeLowerField);
            panelR.add(Box.createHorizontalGlue());
            panelR.add(tdbRelativeUpperLabel);
            panelR.add(tdbRelativeUpperField);
            SpringUtilities.makeCompactGrid(panelR, 3, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            setRelativeModeParameters(tdbThresholdCheck.isSelected(), historic);
            tdbRelativeCheck.addChangeListener(e -> {
                setRelativeModeParameters(tdbRelativeCheck.isSelected(), historic);
            });

            // Threshhold Mode
            panelT.setLayout(new SpringLayout());
            panelT.setBorder(BorderFactory.createEtchedBorder());

            panelT.add(tdbThresholdLabel);
            panelT.add(tdbThresholdCheck);
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());

            panelT.add(tdbThresholdPeriodLabel);
            panelT.add(tdbThresholdPeriodField);
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());
            panelT.add(Box.createHorizontalGlue());

            panelT.add(tdbThresholdLowerLabel);
            panelT.add(tdbThresholdLowerField);
            panelT.add(Box.createHorizontalGlue());
            panelT.add(tdbThresholdUpperLabel);
            panelT.add(tdbThresholdUpperField);
            SpringUtilities.makeCompactGrid(panelT, 3, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);

            setThresholdModeParameters(tdbThresholdCheck.isSelected(), historic);
            tdbThresholdCheck.addChangeListener(e -> {
                setThresholdModeParameters(tdbThresholdCheck.isSelected(), historic);
            });

            // Difference Mode
            panelD.setLayout(new SpringLayout());
            panelD.setBorder(BorderFactory.createEtchedBorder());

            panelD.add(tdbDifferenceLabel);
            panelD.add(tdbDifferenceCheck);
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());

            panelD.add(tdbDifferencePeriodLabel);
            panelD.add(tdbDifferencePeriodField);
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());
            panelD.add(Box.createHorizontalGlue());

            SpringUtilities.makeCompactGrid(panelD, 2, 5, // rows, cols
                    3, 3, // initX, initY
                    3, 5, // xPad, yPad
                    true);
            setDifferenceModeParameters(tdbDifferenceCheck.isSelected(), historic);
            tdbDifferenceCheck.addChangeListener(e -> {
                setDifferenceModeParameters(tdbDifferenceCheck.isSelected(), historic);
            });

            // ----------------------------
            // -------------------------

            tdbPanel.add(tdbPeriodPanel);
            tdbPanel.add(Box.createVerticalGlue());
            tdbPanel.add(tdbModePanel);

            // Lay out the panels.
            SpringUtilities.makeCompactGrid(tdbPeriodPanel, 1, tdbPeriodPanel.getComponentCount(), // rows,
                    // cols
                    0, 0, // initX, initY
                    0, 5, // xPad, yPad
                    true);
            SpringUtilities.makeCompactGrid(tdbPanel, tdbPanel.getComponentCount(), 1, // rows,
                    // cols
                    0, 0, // initX, initY
                    0, 0, // xPad, yPad
                    true);

            centerPanel.add(tdbPanel);
        }

        final JPanel archiverChoicePanel = getArchiverChoicePanel();

        if (Mambo.canChooseArchivers()) {
            centerPanel.add(Box.createVerticalGlue());
            centerPanel.add(archiverChoicePanel);
        }
    }

    private void setDifferenceModeParameters(final boolean b, final Boolean historic) {
        if (historic == null) {
            ttsDifferencePeriodLabel.setEnabled(b);
            ttsDifferencePeriodField.setEnabled(b);
        } else if (historic.booleanValue()) {
            hdbDifferencePeriodLabel.setEnabled(b);
            hdbDifferencePeriodField.setEnabled(b);
        } else {
            tdbDifferencePeriodLabel.setEnabled(b);
            tdbDifferencePeriodField.setEnabled(b);
        }
    }

    private void setThresholdModeParameters(final boolean b, final Boolean historic) {
        if (historic == null) {
            ttsThresholdPeriodLabel.setEnabled(b);
            ttsThresholdPeriodField.setEnabled(b);
            ttsThresholdUpperField.setEnabled(b);
            ttsThresholdUpperLabel.setEnabled(b);
            ttsThresholdLowerLabel.setEnabled(b);
            ttsThresholdLowerField.setEnabled(b);
        } else if (historic.booleanValue()) {
            hdbThresholdPeriodLabel.setEnabled(b);
            hdbThresholdPeriodField.setEnabled(b);
            hdbThresholdUpperField.setEnabled(b);
            hdbThresholdUpperLabel.setEnabled(b);
            hdbThresholdLowerLabel.setEnabled(b);
            hdbThresholdLowerField.setEnabled(b);
        } else {
            tdbThresholdPeriodLabel.setEnabled(b);
            tdbThresholdPeriodField.setEnabled(b);
            tdbThresholdUpperField.setEnabled(b);
            tdbThresholdUpperLabel.setEnabled(b);
            tdbThresholdLowerLabel.setEnabled(b);
            tdbThresholdLowerField.setEnabled(b);
        }
    }

    private void setRelativeModeParameters(final boolean b, final Boolean historic) {
        if (historic == null) {
            ttsRelativePeriodLabel.setEnabled(b);
            ttsRelativePeriodField.setEnabled(b);
            ttsRelativeDLCheck.setEnabled(b);
            ttsRelativeLowerLabel.setEnabled(b);
            ttsRelativeLowerLabel.setEnabled(b);
            ttsRelativeUpperLabel.setEnabled(b);
            ttsRelativeUpperField.setEnabled(b);
        } else if (historic.booleanValue()) {
            hdbRelativePeriodLabel.setEnabled(b);
            hdbRelativePeriodField.setEnabled(b);
            hdbRelativeDLCheck.setEnabled(b);
            hdbRelativeLowerLabel.setEnabled(b);
            hdbRelativeLowerLabel.setEnabled(b);
            hdbRelativeUpperLabel.setEnabled(b);
            hdbRelativeUpperField.setEnabled(b);
        } else {
            tdbRelativePeriodLabel.setEnabled(b);
            tdbRelativePeriodField.setEnabled(b);
            tdbRelativeDLCheck.setEnabled(b);
            tdbRelativeLowerLabel.setEnabled(b);
            tdbRelativeLowerLabel.setEnabled(b);
            tdbRelativeUpperLabel.setEnabled(b);
            tdbRelativeUpperField.setEnabled(b);
        }
    }

    private void setAbsoluteModeParameters(final boolean b, final Boolean historic) {
        if (historic == null) {
            ttsAbsolutePeriodLabel.setEnabled(b);
            ttsAbsolutePeriodField.setEnabled(b);
            ttsAbsoluteDLCheck.setEnabled(b);
            ttsAbsoluteLowerLabel.setEnabled(b);
            ttsAbsoluteLowerField.setEnabled(b);
            ttsAbsoluteUpperLabel.setEnabled(b);
            ttsAbsoluteUpperField.setEnabled(b);
        } else if (historic.booleanValue()) {
            hdbAbsolutePeriodLabel.setEnabled(b);
            hdbAbsolutePeriodField.setEnabled(b);
            hdbAbsoluteDLCheck.setEnabled(b);
            hdbAbsoluteLowerLabel.setEnabled(b);
            hdbAbsoluteLowerField.setEnabled(b);
            hdbAbsoluteUpperLabel.setEnabled(b);
            hdbAbsoluteUpperField.setEnabled(b);
        } else {
            tdbAbsolutePeriodLabel.setEnabled(b);
            tdbAbsolutePeriodField.setEnabled(b);
            tdbAbsoluteDLCheck.setEnabled(b);
            tdbAbsoluteLowerLabel.setEnabled(b);
            tdbAbsoluteLowerField.setEnabled(b);
            tdbAbsoluteUpperLabel.setEnabled(b);
            tdbAbsoluteUpperField.setEnabled(b);
        }
    }

    /**
     * @return
     */
    private JPanel getWarningPanel() {
        final JPanel warningPanel = new JPanel(new BorderLayout(5, 5));
        String warning = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_WARNING1");
        warning += Integer.MAX_VALUE + "ms (=" + Integer.MAX_VALUE / 1000 + "s). ";
        warning += Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_PERIOD_WARNING2");
        final JLabel warningLabel = new JLabel(warning);
        warningLabel.setToolTipText(warning);
        warningLabel.setForeground(Color.RED);
        final JLabel label = new JLabel("*");
        label.setToolTipText(warning);
        warningPanel.setToolTipText(warning);
        warningPanel.add(label, BorderLayout.WEST);
        warningPanel.add(warningLabel, BorderLayout.CENTER);
        warningPanel.setMinimumSize(new Dimension(0, warningPanel.getPreferredSize().height));
        return warningPanel;
    }

    private JPanel getArchiverChoicePanel() {
        final JPanel archiverChoicePanel = new JPanel();

        final String dedicatedArchiverLabelMessage = Messages
                .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_DEDICATED_ARCHIVER");
        final JLabel dedicatedArchiverLabel = new JLabel(dedicatedArchiverLabelMessage);
        final String currentArchiverLabelMessage = Messages
                .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_CURRENT_ARCHIVER");
        final JLabel currentArchiverLabel = new JLabel(currentArchiverLabelMessage);
        final String archiverDefaultLabelMessage = Messages
                .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_ARCHIVER_DEFAULT");
        final JLabel archiverDefaultLabel = new JLabel(archiverDefaultLabelMessage);
        final String archiverRunningLabelMessage = Messages
                .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_ARCHIVER_RUNNING");
        final JLabel archiverRunningLabel = new JLabel(archiverRunningLabelMessage);
        final String archiverNameLabelMessage = Messages
                .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_ARCHIVER_NAME");
        final JLabel archiverNameLabel = new JLabel(archiverNameLabelMessage);

        archiverChoicePanel.add(dedicatedArchiverLabel);
        archiverChoicePanel.add(dedicatedArchiverTextField);
        archiverChoicePanel.add(currentArchiverLabel);
        archiverChoicePanel.add(currentArchiverTextField);
        archiverChoicePanel.add(archiverDefaultLabel);
        archiverChoicePanel.add(archiverChoiceDefaultCheckBox);
        archiverChoicePanel.add(archiverRunningLabel);
        archiverChoicePanel.add(archiverChoiceRunningComboBox);
        archiverChoicePanel.add(archiverNameLabel);
        archiverChoicePanel.add(archiverChoiceNameTextField);

        final String archiverTitleLabelMessage = Messages
                .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_ARCHIVER_TITLE");
        final TitledBorder tb = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                archiverTitleLabelMessage, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP,
                GUIUtilities.getTitleFont());

        archiverChoicePanel.setBorder(tb);

        archiverChoicePanel.setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(archiverChoicePanel, 5, 2, // rows, cols
                0, 0, // initX, initY
                0, 5, // xPad, yPad
                true);

        return archiverChoicePanel;
    }

    /**
     * @return 22 juil. 2005
     */
    public long getExportPeriod() {
        return tdbExportPeriodComboBox.getExportPeriod();
        /*
         * int selected = TDBexportPeriodComboBox.getSelectedIndex(); return
         * exportPeriodsTable[ selected ];
         */
    }

    /**
     * @param historic
     * @param type_a
     *            22 juil. 2005
     */
    public void doSelectMode(final Boolean historic, final int type) {
        JCheckBox checkBoxToSelect = null;

        switch (type) {
            case ArchivingConfigurationMode.TYPE_A:
                if (historic == null) {
                    checkBoxToSelect = ttsAbsoluteCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = hdbAbsoluteCheck;
                } else {
                    checkBoxToSelect = tdbAbsoluteCheck;
                }
                break;

            case ArchivingConfigurationMode.TYPE_D:
                if (historic == null) {
                    checkBoxToSelect = ttsDifferenceCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = hdbDifferenceCheck;
                } else {
                    checkBoxToSelect = tdbDifferenceCheck;
                }
                break;

            case ArchivingConfigurationMode.TYPE_P:
                if (historic == null) {
                    checkBoxToSelect = ttsPeriodicalCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = hdbPeriodicalCheck;
                } else {
                    checkBoxToSelect = tdbPeriodicalCheck;
                }
                break;

            case ArchivingConfigurationMode.TYPE_T:
                if (historic == null) {
                    checkBoxToSelect = ttsThresholdCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = hdbThresholdCheck;
                } else {
                    checkBoxToSelect = tdbThresholdCheck;
                }
                break;

            case ArchivingConfigurationMode.TYPE_R:
                if (historic == null) {
                    checkBoxToSelect = ttsRelativeCheck;
                } else if (historic.booleanValue()) {
                    checkBoxToSelect = hdbRelativeCheck;
                } else {
                    checkBoxToSelect = tdbRelativeCheck;
                }
                break;
        }

        checkBoxToSelect.setSelected(true);
    }

    /**
     * @param modesTypes
     *            25 juil. 2005
     */
    public void setSelectedModes(final Boolean historic, final Collection<Integer> modesTypes) {
        if (historic == null) {
            ttsRelativeCheck.setSelected(false);
            ttsPeriodicalCheck.setSelected(false);
            ttsAbsoluteCheck.setSelected(false);
            ttsDifferenceCheck.setSelected(false);
            ttsThresholdCheck.setSelected(false);
        } else if (historic.booleanValue()) {
            hdbRelativeCheck.setSelected(false);
            hdbPeriodicalCheck.setSelected(false);
            hdbAbsoluteCheck.setSelected(false);
            hdbDifferenceCheck.setSelected(false);
            hdbThresholdCheck.setSelected(false);
        } else {
            tdbRelativeCheck.setSelected(false);
            tdbPeriodicalCheck.setSelected(false);
            tdbAbsoluteCheck.setSelected(false);
            tdbDifferenceCheck.setSelected(false);
            tdbThresholdCheck.setSelected(false);
        }

        if (modesTypes != null) {
            for (Integer _nextModeType : modesTypes) {
                final int nextModeType = _nextModeType.intValue();
                doSelectMode(historic, nextModeType);
            }
        }
    }

    /**
     * @param period
     *            25 juil. 2005
     */
    public void setExportPeriod(final long period) {
        tdbExportPeriodComboBox.setExportPeriod(period);
    }

    /**
     * 13 sept. 2005
     */
    public void doUnselectAllModes() {
        tdbPeriodicalCheck.setSelected(false);
        tdbAbsoluteCheck.setSelected(false);
        tdbThresholdCheck.setSelected(false);
        tdbDifferenceCheck.setSelected(false);

        hdbPeriodicalCheck.setSelected(false);
        hdbAbsoluteCheck.setSelected(false);
        hdbThresholdCheck.setSelected(false);
        hdbDifferenceCheck.setSelected(false);
    }

    public boolean verifyValues() {
        final boolean firstTests = verifyThatSelectedModesAreCoherent() && verifyEachMode();
        if (firstTests) {
            try {
                final ACAttributesPropertiesTree tree = ACAttributesPropertiesTree.getInstance();
                if (historic == null) {
                    for (final ArchivingConfigurationAttribute a : tree.getListOfAttributesToSet()) {
                        ArchivingUtils.checkMode(getLastMode(), a.getCompleteName(), historic);
                    }
                } else if (historic.booleanValue() && Mambo.IS_FAST_HDB) {
                    for (final ArchivingConfigurationAttribute a : tree.getListOfAttributesToSet()) {
                        ArchivingUtils.checkMode(getLastMode(), a.getCompleteName(), Boolean.FALSE);
                    }
                } else {
                    for (final ArchivingConfigurationAttribute a : tree.getListOfAttributesToSet()) {
                        ArchivingUtils.checkMode(getLastMode(), a.getCompleteName(), historic);
                    }
                }
                return true;
            } catch (final ArchivingException e) {
                return treatModeException(e);
            }
        } else {
            return false;
        }
    }

    private boolean treatModeException(final ArchivingException ae) {
        final String title = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_MODES_TITLE");
        final String msgGeneral = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_MODES_MESSAGE");
        final String msgSpecific = ae.getLastExceptionMessage();
        final String msg = msgGeneral + GUIUtilities.CRLF + msgSpecific;

        JOptionPane.showMessageDialog(this, msg, title, JOptionPane.ERROR_MESSAGE);
        return false;
    }

    /**
     * @return
     */
    private boolean verifyEachMode() {
        final Mode mode = new Mode();

        if (historic == null) {
            if (ttsPeriodicalCheck.isSelected()) {
                final String s = ttsPeriodField.getText();
                try {
                    Integer.parseInt(s);
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_PERIODIC_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_PERIODIC_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModePeriode modeP = new ModePeriode();
                modeP.setPeriod(getTTSperiod());
                mode.setModeP(modeP);
            }

            if (ttsDifferenceCheck.isSelected()) {
                final String s = ttsDifferencePeriodField.getText();
                try {
                    Integer.parseInt(s);
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_DIFFERENCE_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_DIFFERENCE_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeDifference modeD = new ModeDifference();
                modeD.setPeriod(getTTSDifferencePeriod());
                mode.setModeD(modeD);
            }

            if (ttsAbsoluteCheck.isSelected()) {
                final String s = ttsAbsolutePeriodField.getText();
                final String s1 = ttsAbsoluteLowerField.getText();
                final String s2 = ttsAbsoluteUpperField.getText();
                try {
                    Integer.parseInt(s);
                    Double.parseDouble(s1);
                    Double.parseDouble(s2);
                } catch (final Exception e) {
                    LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_ABSOLUTE_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_ABSOLUTE_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeAbsolu modeA = new ModeAbsolu();
                modeA.setPeriod(getTTSAbsolutePeriod());
                modeA.setValInf(Math.abs(Double.parseDouble(getTTSAbsoluteLower())));
                modeA.setValSup(Math.abs(Double.parseDouble(getTTSAbsoluteUpper())));
                modeA.setSlow_drift(isTTSAbsoluteDLCheck());
                mode.setModeA(modeA);
            }

            if (ttsRelativeCheck.isSelected()) {
                final String s = ttsRelativePeriodField.getText();
                final String s1 = ttsRelativeLowerField.getText();
                final String s2 = ttsRelativeUpperField.getText();
                try {
                    Integer.parseInt(s);
                    Double.parseDouble(s1);
                    Double.parseDouble(s2);
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_RELATIVE_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_RELATIVE_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeRelatif modeR = new ModeRelatif();
                modeR.setPeriod(getTTSRelativePeriod());
                modeR.setPercentInf(Double.parseDouble(getTTSRelativeLower()));
                modeR.setPercentSup(Double.parseDouble(getTTSRelativeUpper()));
                modeR.setSlow_drift(isTTSRelativeDLCheck());
                mode.setModeR(modeR);
            }

            if (ttsThresholdCheck.isSelected()) {
                final String s = ttsThresholdPeriodField.getText();
                final String s1 = ttsThresholdLowerField.getText();
                final String s2 = ttsThresholdUpperField.getText();
                try {
                    Integer.parseInt(s);
                    final double d1 = Double.parseDouble(s1);
                    final double d2 = Double.parseDouble(s2);
                    if (d1 >= d2) {
                        throw new IllegalStateException();
                    }
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_THRESHOLD_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TTS_THRESHOLD_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeSeuil modeS = new ModeSeuil();
                modeS.setPeriod(getTTSThresholdPeriod());
                modeS.setThresholdInf(Double.parseDouble(getTTSThresholdLower()));
                modeS.setThresholdSup(Double.parseDouble(getTTSThresholdUpper()));
                mode.setModeT(modeS);
            }
        } else if (historic.booleanValue()) {
            if (hdbPeriodicalCheck.isSelected()) {
                final String s = hdbPeriodField.getText();
                try {
                    Integer.parseInt(s);
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_PERIODIC_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_PERIODIC_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModePeriode modeP = new ModePeriode();
                modeP.setPeriod(getHDBperiod());
                mode.setModeP(modeP);
            }

            if (hdbDifferenceCheck.isSelected()) {
                final String s = hdbDifferencePeriodField.getText();
                try {
                    Integer.parseInt(s);
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_DIFFERENCE_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_DIFFERENCE_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeDifference modeD = new ModeDifference();
                modeD.setPeriod(getHDBDifferencePeriod());
                mode.setModeD(modeD);
            }

            if (hdbAbsoluteCheck.isSelected()) {
                final String s = hdbAbsolutePeriodField.getText();
                final String s1 = hdbAbsoluteLowerField.getText();
                final String s2 = hdbAbsoluteUpperField.getText();
                double d1;
                double d2;

                try {
                    Integer.parseInt(s);
                    d1 = Math.abs(Double.parseDouble(s1));
                    // d1 = -1.0 * Math.abs ( d1 );
                    d2 = Math.abs(Double.parseDouble(s2));
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_ABSOLUTE_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_ABSOLUTE_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);
                    return false;
                }

                final ModeAbsolu modeA = new ModeAbsolu();
                modeA.setPeriod(getHDBAbsolutePeriod());
                modeA.setValInf(d1);
                modeA.setValSup(d2);
                modeA.setSlow_drift(isHDBAbsoluteDLCheck());
                mode.setModeA(modeA);
            }

            if (hdbRelativeCheck.isSelected()) {
                final String s = hdbRelativePeriodField.getText();
                final String s1 = hdbRelativeLowerField.getText();
                final String s2 = hdbRelativeUpperField.getText();
                double d1;
                double d2;

                try {
                    Integer.parseInt(s);
                    d1 = Double.parseDouble(s1);
                    d1 = Math.abs(d1);
                    d2 = Double.parseDouble(s2);
                    d2 = Math.abs(d2);
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_RELATIVE_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_RELATIVE_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeRelatif modeR = new ModeRelatif();
                modeR.setPeriod(getHDBRelativePeriod());
                modeR.setPercentInf(d1);
                modeR.setPercentSup(d2);
                modeR.setSlow_drift(isHDBRelativeDLCheck());
                mode.setModeR(modeR);
            }

            if (hdbThresholdCheck.isSelected()) {
                final String s = hdbThresholdPeriodField.getText();
                final String s1 = hdbThresholdLowerField.getText();
                final String s2 = hdbThresholdUpperField.getText();
                try {
                    Integer.parseInt(s);
                    final double d1 = Double.parseDouble(s1);
                    final double d2 = Double.parseDouble(s2);
                    if (d1 >= d2) {
                        throw new IllegalStateException();
                    }
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_THRESHOLD_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_HDB_THRESHOLD_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeSeuil modeS = new ModeSeuil();
                modeS.setPeriod(getHDBThresholdPeriod());
                modeS.setThresholdInf(Double.parseDouble(getHDBThresholdLower()));
                modeS.setThresholdSup(Double.parseDouble(getHDBThresholdUpper()));
                mode.setModeT(modeS);
            }
        } else {
            if (tdbPeriodicalCheck.isSelected()) {
                final String s = tdbPeriodField.getText();
                try {
                    Integer.parseInt(s);
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_PERIODIC_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_PERIODIC_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModePeriode modeP = new ModePeriode();
                modeP.setPeriod(getTDBperiod());
                mode.setModeP(modeP);

                mode.setTdbSpec(new TdbSpec(getExportPeriod(), 0));
            }

            if (tdbDifferenceCheck.isSelected()) {
                final String s = tdbDifferencePeriodField.getText();
                try {
                    Integer.parseInt(s);
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_DIFFERENCE_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_DIFFERENCE_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeDifference modeD = new ModeDifference();
                modeD.setPeriod(getTDBDifferencePeriod());
                mode.setModeD(modeD);
            }

            if (tdbAbsoluteCheck.isSelected()) {
                final String s = tdbAbsolutePeriodField.getText();
                final String s1 = tdbAbsoluteLowerField.getText();
                final String s2 = tdbAbsoluteUpperField.getText();
                try {
                    Integer.parseInt(s);
                    Double.parseDouble(s1);
                    Double.parseDouble(s2);
                } catch (final Exception e) {
                    LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_ABSOLUTE_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_ABSOLUTE_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeAbsolu modeA = new ModeAbsolu();
                modeA.setPeriod(getTDBAbsolutePeriod());
                modeA.setValInf(Math.abs(Double.parseDouble(getTDBAbsoluteLower())));
                modeA.setValSup(Math.abs(Double.parseDouble(getTDBAbsoluteUpper())));
                modeA.setSlow_drift(isTDBAbsoluteDLCheck());
                mode.setModeA(modeA);
            }

            if (tdbRelativeCheck.isSelected()) {
                final String s = tdbRelativePeriodField.getText();
                final String s1 = tdbRelativeLowerField.getText();
                final String s2 = tdbRelativeUpperField.getText();
                try {
                    Integer.parseInt(s);
                    Double.parseDouble(s1);
                    Double.parseDouble(s2);
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_RELATIVE_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_RELATIVE_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeRelatif modeR = new ModeRelatif();
                modeR.setPeriod(getTDBRelativePeriod());
                modeR.setPercentInf(Double.parseDouble(getTDBRelativeLower()));
                modeR.setPercentSup(Double.parseDouble(getTDBRelativeUpper()));
                modeR.setSlow_drift(isTDBRelativeDLCheck());
                mode.setModeR(modeR);
            }

            if (tdbThresholdCheck.isSelected()) {
                final String s = tdbThresholdPeriodField.getText();
                final String s1 = tdbThresholdLowerField.getText();
                final String s2 = tdbThresholdUpperField.getText();
                try {
                    Integer.parseInt(s);
                    final double d1 = Double.parseDouble(s1);
                    final double d2 = Double.parseDouble(s2);
                    if (d1 >= d2) {
                        throw new IllegalStateException();
                    }
                } catch (final Exception e) {
                    final String title = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_THRESHOLD_MODE_TITLE");
                    final String msg = Messages
                            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INVALID_TDB_THRESHOLD_MODE_MESSAGE");
                    JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                    return false;
                }

                final ModeSeuil modeS = new ModeSeuil();
                modeS.setPeriod(getTDBThresholdPeriod());
                modeS.setThresholdInf(Double.parseDouble(getTDBThresholdLower()));
                modeS.setThresholdSup(Double.parseDouble(getTDBThresholdUpper()));
                mode.setModeT(modeS);
            }
        }

        setLastMode(mode);
        return true;
    }

    /**
     * @param mode
     */
    private void setLastMode(final Mode mode) {
        lastMode = mode;
    }

    /**
     * @return
     */
    public boolean verifyThatSelectedModesAreCoherent() {
        boolean coherent = true;
        if (historic == null) {
            final boolean hasTTSNonPeriodicMode = ttsAbsoluteCheck.isSelected() || ttsThresholdCheck.isSelected()
                    || ttsDifferenceCheck.isSelected();
            final boolean hasTTSPeriodicMode = ttsPeriodicalCheck.isSelected();

            if (hasTTSNonPeriodicMode && !hasTTSPeriodicMode) {
                final String title = Messages
                        .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INCOHERENT_TTS_MODES_TITLE");
                final String msg = Messages
                        .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INCOHERENT_TTS_MODES_MESSAGE");
                JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                coherent = false;
            }
        } else if (historic.booleanValue()) {
            final boolean hasHDBNonPeriodicMode = hdbAbsoluteCheck.isSelected() || hdbThresholdCheck.isSelected()
                    || hdbDifferenceCheck.isSelected();
            final boolean hasHDBPeriodicMode = hdbPeriodicalCheck.isSelected();

            if (hasHDBNonPeriodicMode && !hasHDBPeriodicMode) {
                final String title = Messages
                        .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INCOHERENT_HDB_MODES_TITLE");
                final String msg = Messages
                        .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INCOHERENT_HDB_MODES_MESSAGE");
                JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                coherent = false;
            }
        } else {
            final boolean hasTDBNonPeriodicMode = tdbAbsoluteCheck.isSelected() || tdbThresholdCheck.isSelected()
                    || tdbDifferenceCheck.isSelected();
            final boolean hasTDBPeriodicMode = tdbPeriodicalCheck.isSelected();

            if (hasTDBNonPeriodicMode && !hasTDBPeriodicMode) {
                final String title = Messages
                        .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INCOHERENT_TDB_MODES_TITLE");
                final String msg = Messages
                        .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_INCOHERENT_TDB_MODES_MESSAGE");
                JOptionPane.showMessageDialog(null, msg, title, JOptionPane.ERROR_MESSAGE);

                coherent = false;
            }
        }

        return coherent;
    }

    // ------------up/low getters
    // -------------- HDB ----------------//
    public String getHDBAbsoluteUpper() {
        return hdbAbsoluteUpperField.getText();
    }

    public String getHDBRelativeUpper() {
        return hdbRelativeUpperField.getText();
    }

    public String getHDBThresholdUpper() {
        return hdbThresholdUpperField.getText();
    }

    public String getHDBAbsoluteLower() {
        return hdbAbsoluteLowerField.getText();
    }

    public String getHDBRelativeLower() {
        return hdbRelativeLowerField.getText();
    }

    public String getHDBThresholdLower() {
        return hdbThresholdLowerField.getText();
    }

    public boolean isHDBAbsoluteDLCheck() {
        return hdbAbsoluteDLCheck.isSelected();
    }

    public boolean isHDBRelativeDLCheck() {
        return hdbRelativeDLCheck.isSelected();
    }

    public void setHDBAbsoluteDLCheck(final boolean check) {
        hdbAbsoluteDLCheck.setSelected(check);
    }

    public void setHDBRelativeDLCheck(final boolean check) {
        hdbRelativeDLCheck.setSelected(check);
    }

    public void setHDBAbsoluteUpper(final String val) {
        hdbAbsoluteUpperField.setText(val);
    }

    public void setHDBRelativeUpper(final String val) {
        hdbRelativeUpperField.setText(val);
    }

    public void setHDBThresholdUpper(final String val) {
        hdbThresholdUpperField.setText(val);
    }

    public void setHDBAbsoluteLower(String val) {
        try {
            double d = Double.parseDouble(val);
            d = Math.abs(d);
            val = String.valueOf(d);
        } catch (final Exception e) {
            // do nothing
        }

        hdbAbsoluteLowerField.setText(val);
    }

    public void setHDBRelativeLower(String val) {
        try {
            double d = Double.parseDouble(val);
            d = Math.abs(d);
            val = String.valueOf(d);
        } catch (final Exception e) {
            // do nothing
        }

        hdbRelativeLowerField.setText(val);
    }

    public void setHDBThresholdLower(final String val) {
        hdbThresholdLowerField.setText(val);
    }

    public int getHDBAbsolutePeriod() {
        try {
            final String s = hdbAbsolutePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public int getHDBRelativePeriod() {
        try {
            final String s = hdbRelativePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public int getHDBThresholdPeriod() {
        try {
            final String s = hdbThresholdPeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public int getHDBDifferencePeriod() {
        try {
            final String s = hdbDifferencePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public void setHDBAbsolutePeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        hdbAbsolutePeriodField.setText(text);
    }

    public void setHDBRelativePeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        hdbRelativePeriodField.setText(text);
    }

    public void setHDBThresholdPeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        hdbThresholdPeriodField.setText(text);
    }

    public void setHDBDifferencePeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        hdbDifferencePeriodField.setText(text);
    }

    /**
     * @param period
     *            25 juil. 2005
     */
    public void setHDBPeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period / 1000);
        hdbPeriodField.setText(text);
    }

    public int getHDBperiod() {
        try {
            final String s = hdbPeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                } else if (i > Integer.MAX_VALUE / 1000) {
                    i = Integer.MAX_VALUE / 1000;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE / 1000;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            i *= 1000;
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeHDB_A() {
        return hdbAbsoluteCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeHDB_P() {
        return hdbPeriodicalCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeHDB_R() {
        return hdbRelativeCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeHDB_T() {
        return hdbThresholdCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeHDB_D() {
        return hdbDifferenceCheck.isSelected();
    }
    // -----------------------------------//

    // -------------- TDB ----------------//
    public boolean isTDBAbsoluteDLCheck() {
        return tdbAbsoluteDLCheck.isSelected();
    }

    public boolean isTDBRelativeDLCheck() {
        return tdbRelativeDLCheck.isSelected();
    }

    public void setTDBAbsoluteDLCheck(final boolean check) {
        tdbAbsoluteDLCheck.setSelected(check);
    }

    public void setTDBRelativeDLCheck(final boolean check) {
        tdbRelativeDLCheck.setSelected(check);
    }

    public String getTDBAbsoluteUpper() {
        return tdbAbsoluteUpperField.getText();
    }

    public String getTDBRelativeUpper() {
        return tdbRelativeUpperField.getText();
    }

    public String getTDBThresholdUpper() {
        return tdbThresholdUpperField.getText();
    }

    public String getTDBAbsoluteLower() {
        return tdbAbsoluteLowerField.getText();
    }

    public String getTDBRelativeLower() {
        return tdbRelativeLowerField.getText();
    }

    public String getTDBThresholdLower() {
        return tdbThresholdLowerField.getText();
    }

    public void setTDBAbsoluteUpper(final String val) {
        tdbAbsoluteUpperField.setText(val);
    }

    public void setTDBRelativeUpper(final String val) {
        tdbRelativeUpperField.setText(val);
    }

    public void setTDBThresholdUpper(final String val) {
        tdbThresholdUpperField.setText(val);
    }

    public void setTDBAbsoluteLower(final String val) {
        tdbAbsoluteLowerField.setText(val);
    }

    public void setTDBRelativeLower(final String val) {
        tdbRelativeLowerField.setText(val);
    }

    public void setTDBThresholdLower(final String val) {
        tdbThresholdLowerField.setText(val);
    }

    public int getTDBAbsolutePeriod() {
        try {
            final String s = tdbAbsolutePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public int getTDBRelativePeriod() {
        try {
            final String s = tdbRelativePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public int getTDBThresholdPeriod() {
        try {
            final String s = tdbThresholdPeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public int getTDBDifferencePeriod() {
        try {
            final String s = tdbDifferencePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public void setTDBAbsolutePeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        tdbAbsolutePeriodField.setText(text);
    }

    public void setTDBRelativePeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        tdbRelativePeriodField.setText(text);
    }

    public void setTDBThresholdPeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        tdbThresholdPeriodField.setText(text);
    }

    public void setTDBDifferencePeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        tdbDifferencePeriodField.setText(text);
    }

    public int getTDBperiod() {
        try {
            final String s = tdbPeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            return -1;
        }
    }

    /**
     * @param period
     *            25 juil. 2005
     */
    public void setTDBPeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        tdbPeriodField.setText(text);
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_A() {
        return tdbAbsoluteCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_P() {
        return tdbPeriodicalCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_R() {
        return tdbRelativeCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_T() {
        return tdbThresholdCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTDB_D() {
        return tdbDifferenceCheck.isSelected();
    }
    // -----------------------------------//

    // -------------- TTS ----------------//
    public boolean isTTSAbsoluteDLCheck() {
        return ttsAbsoluteDLCheck.isSelected();
    }

    public boolean isTTSRelativeDLCheck() {
        return ttsRelativeDLCheck.isSelected();
    }

    public void setTTSAbsoluteDLCheck(final boolean check) {
        ttsAbsoluteDLCheck.setSelected(check);
    }

    public void setTTSRelativeDLCheck(final boolean check) {
        ttsRelativeDLCheck.setSelected(check);
    }

    public String getTTSAbsoluteUpper() {
        return ttsAbsoluteUpperField.getText();
    }

    public String getTTSRelativeUpper() {
        return ttsRelativeUpperField.getText();
    }

    public String getTTSThresholdUpper() {
        return ttsThresholdUpperField.getText();
    }

    public String getTTSAbsoluteLower() {
        return ttsAbsoluteLowerField.getText();
    }

    public String getTTSRelativeLower() {
        return ttsRelativeLowerField.getText();
    }

    public String getTTSThresholdLower() {
        return ttsThresholdLowerField.getText();
    }

    public void setTTSAbsoluteUpper(final String val) {
        ttsAbsoluteUpperField.setText(val);
    }

    public void setTTSRelativeUpper(final String val) {
        ttsRelativeUpperField.setText(val);
    }

    public void setTTSThresholdUpper(final String val) {
        ttsThresholdUpperField.setText(val);
    }

    public void setTTSAbsoluteLower(final String val) {
        ttsAbsoluteLowerField.setText(val);
    }

    public void setTTSRelativeLower(final String val) {
        ttsRelativeLowerField.setText(val);
    }

    public void setTTSThresholdLower(final String val) {
        ttsThresholdLowerField.setText(val);
    }

    public int getTTSAbsolutePeriod() {
        try {
            final String s = ttsAbsolutePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public int getTTSRelativePeriod() {
        try {
            final String s = ttsRelativePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public int getTTSThresholdPeriod() {
        try {
            final String s = ttsThresholdPeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public int getTTSDifferencePeriod() {
        try {
            final String s = ttsDifferencePeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            return -1;
        }
    }

    public void setTTSAbsolutePeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        ttsAbsolutePeriodField.setText(text);
    }

    public void setTTSRelativePeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        ttsRelativePeriodField.setText(text);
    }

    public void setTTSThresholdPeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        ttsThresholdPeriodField.setText(text);
    }

    public void setTTSDifferencePeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        ttsDifferencePeriodField.setText(text);
    }

    public int getTTSperiod() {
        try {
            final String s = ttsPeriodField.getText();
            int i;
            try {
                i = Integer.parseInt(s);
                if (i < 0) {
                    i = 0;
                }
            } catch (final NumberFormatException n) {
                double d;
                try {
                    d = Double.parseDouble(s);
                    if (d > 0) {
                        i = Integer.MAX_VALUE;
                    } else {
                        i = 0;
                    }
                } catch (final Exception e) {
                    throw n;
                }
            }
            return i;
        } catch (final Exception e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
            return -1;
        }
    }

    /**
     * @param period
     *            25 juil. 2005
     */
    public void setTTSPeriod(final int period) {
        final String text = period == -1 ? ObjectUtils.EMPTY_STRING : String.valueOf(period);
        ttsPeriodField.setText(text);
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_A() {
        return ttsAbsoluteCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_P() {
        return ttsPeriodicalCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_R() {
        return ttsRelativeCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_T() {
        return ttsThresholdCheck.isSelected();
    }

    /**
     * @return 7 sept. 2005
     */
    public boolean hasModeTTS_D() {
        return ttsDifferenceCheck.isSelected();
    }
    // ---------------------------------------//

    /**
     * @return Returns the lastMode.
     */
    public Mode getLastMode() {
        return lastMode;
    }

    public void setArchiverDefault(final boolean isDefault) {
        if (isDefault) {
            archiverChoiceNameTextField.setText(ArchiversComboBox.DEFAULT);
        } else {
            archiverChoiceNameTextField.setText(ObjectUtils.EMPTY_STRING);
        }
        archiverChoiceRunningComboBox.selectDefault();
        archiverChoiceRunningComboBox.setEnabled(!isDefault);
    }

    public void setArchiverChoiceText(final String newArchiver) {
        archiverChoiceNameTextField.setText(newArchiver);
    }

    public String getDedicatedArchiver() {
        final String rawValue = archiverChoiceNameTextField.getText();
        if (rawValue == null || rawValue.isEmpty() || rawValue.equals(ArchiversComboBox.DEFAULT)) {
            return ObjectUtils.EMPTY_STRING;
        }

        return archiverChoiceNameTextField.getText();
    }

    public void pushDefaultArchiver(final String dedicatedArchiver) {
        final boolean archiverDefaultIsEmpty = dedicatedArchiver == null || dedicatedArchiver.isEmpty()
                || dedicatedArchiver.equals(ArchiversComboBox.DEFAULT);
        archiverChoiceDefaultCheckBox.setSelected(archiverDefaultIsEmpty);
        archiverChoiceRunningComboBox.selectArchiver(dedicatedArchiver);
        setArchiverDefault(archiverDefaultIsEmpty);
        setArchiverChoiceText(dedicatedArchiver);
    }

    /**
     * @return Returns the isHistoric.
     */
    public Boolean isHistoric() {
        return historic;
    }

    public void pushCurrentArchiver(final IArchiver currentArchiver, final String attributeCompleteName)
            throws ArchivingException {
        final String name = currentArchiver == null ? ObjectUtils.EMPTY_STRING : currentArchiver.getName();
        currentArchiverTextField.setText(name);

        Color color = Color.BLACK;
        if (currentArchiver != null) {
            color = currentArchiver.getAssociationColor(attributeCompleteName);
        }
        currentArchiverTextField.setForeground(color);

        archiverChoiceRunningComboBox.setCurrentlySelectedAttribute(attributeCompleteName);
    }

    public void pushDedicatedArchiver(final String attributeCompleteName) {
        String text = ObjectUtils.EMPTY_STRING;
        final Map<String, IArchiver> attributesToDedicatedArchiver = ArchiverFactory
                .getAttributesToDedicatedArchiver(historic);
        if (attributesToDedicatedArchiver != null) {
            if (attributeCompleteName != null) {
                final IArchiver dedicatedArch = attributesToDedicatedArchiver.get(attributeCompleteName);
                if (dedicatedArch != null) {
                    text = dedicatedArch.getName();
                    final String dedicatedStatusDescription = dedicatedArch.isExported() ? " (EXPORTED)"
                            : " (NOT EXPORTED)";
                    text += dedicatedStatusDescription;
                }
            }
            attributesToDedicatedArchiver.clear();
        }
        dedicatedArchiverTextField.setText(text);
    }

    public void setCurrentlySelectedAttribute(final String attributeCompleteName) {
        archiverChoiceRunningComboBox.setCurrentlySelectedAttribute(attributeCompleteName);
    }

    public void pushChoiceArchiverColor(final String completeName) {
        if (archiverChoiceNameTextField != null) {
            archiverChoiceNameTextField.setColorForAttribute(completeName);
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class ArchiverChoiceDefaultCheckBox extends JCheckBox {

        private static final long serialVersionUID = -701576358357868162L;

        public ArchiverChoiceDefaultCheckBox() {
            super();
            addActionListener(new ArchiverChoiceDefaultCheckBoxActionListener());
        }
    }

    private class ArchiverChoiceDefaultCheckBoxActionListener implements ActionListener {

        public ArchiverChoiceDefaultCheckBoxActionListener() {
            super();
        }

        @Override
        public void actionPerformed(final ActionEvent e) {
            final boolean isDefault = archiverChoiceDefaultCheckBox.isSelected();
            setArchiverDefault(isDefault);
        }
    }

    private class ArchiverChoiceNameTextField extends JTextField {

        private static final long serialVersionUID = -639168112516045680L;

        public ArchiverChoiceNameTextField(final String initialValue) {
            super(initialValue);

            setMaximumSize(CLASSIC_MAX_DIMENSION);
        }

        @Override
        public void setText(final String archiver) {
            super.setText(archiver);
        }

        public void setColorForAttribute(final String completeName) {
            final String archiver = getText();
            if ((archiver != null) && (!archiver.isEmpty()) && (!archiver.equals(ArchiversComboBox.DEFAULT))) {
                try {
//                    final Archiver arch = new Archiver(archiver, isHistoric());
                    final IArchiver arch = ArchiverFactory.getArchiver(archiver, isHistoric());
                    final Color color = arch.getAssociationColor(completeName);
                    super.setForeground(color);
                } catch (final Exception e) {
                    // do nothing
                    LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
                }
            }
        }
    }

}
