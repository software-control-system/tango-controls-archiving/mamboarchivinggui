package fr.soleil.mambo.containers.archiving.dialogs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.archiving.ACAttributesPropertiesTreeExpandAllAction;
import fr.soleil.mambo.components.archiving.ACAttributesPropertiesTree;
import fr.soleil.mambo.models.ACAttributesTreeModel;

public class AttributesPropertiesTab extends JPanel {

    private static final long serialVersionUID = -6427807004615145935L;

    private static final ImageIcon EXPAND_ALL_ICON = new ImageIcon(Mambo.class.getResource("icons/expand_all.gif"));
    private static final double SPLIT_PANE_PROPORTION = 0.3;

    private static AttributesPropertiesTab instance = null;

    private JButton expandAllButton;

    private ACAttributesPropertiesTree tree;
    private JScrollPane treeScrollPane;
    private JSplitPane splitPane;

    private final Boolean historic;

    private AttributesPropertiesPanel propertiesPanel;

    public static AttributesPropertiesTab getInstance(final Boolean historic) throws ArchivingException {
        if (instance == null || !ObjectUtils.sameObject(instance.isHistoric(), historic)) {
            instance = new AttributesPropertiesTab(historic);
        }

        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    /**
     * @throws ArchivingException
     * 
     */
    private AttributesPropertiesTab(final Boolean historic) throws ArchivingException {
        super(new BorderLayout());
        this.historic = historic;
        initComponents(historic);
        addComponents();
    }

    /**
     * 19 juil. 2005
     */
    private void addComponents() {
        splitPane.setLeftComponent(treeScrollPane);
        splitPane.setRightComponent(propertiesPanel);
        add(splitPane, BorderLayout.CENTER);
    }

    /**
     * 19 juil. 2005
     * 
     * @throws ArchivingException
     */
    private void initComponents(final Boolean historic) throws ArchivingException {
        expandAllButton = new JButton(new ACAttributesPropertiesTreeExpandAllAction());
        expandAllButton.setIcon(EXPAND_ALL_ICON);
        expandAllButton.setMargin(new Insets(0, 0, 0, 0));
        expandAllButton.setBackground(Color.WHITE);
        expandAllButton.setBorderPainted(false);
        expandAllButton.setFocusable(false);
        expandAllButton.setFocusPainted(false);
        expandAllButton.setHorizontalAlignment(JButton.LEFT);
        expandAllButton.setFont(GUIUtilities.expandButtonFont);
        expandAllButton.setForeground(Color.BLACK);

        final ACAttributesTreeModel model = ACAttributesTreeModel.getInstance();
        tree = ACAttributesPropertiesTree.getInstance(model);

        treeScrollPane = new JScrollPane(tree);
        treeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        treeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        treeScrollPane.setColumnHeaderView(expandAllButton);
        treeScrollPane.setMinimumSize(new Dimension(0, 0));

        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerSize(8);
        splitPane.setDividerLocation(SPLIT_PANE_PROPORTION);
        splitPane.setResizeWeight(0.3);

        propertiesPanel = AttributesPropertiesPanel.getInstance(historic);
    }

    public Boolean isHistoric() {
        return historic;
    }
}
