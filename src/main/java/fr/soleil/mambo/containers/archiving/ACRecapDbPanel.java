package fr.soleil.mambo.containers.archiving;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.components.archiving.ACRecapTable;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributes;
import fr.soleil.mambo.models.ACRecapTableModel;
import fr.soleil.mambo.tools.SpringUtilities;

public class ACRecapDbPanel extends JPanel {

    private static final long serialVersionUID = 5302790531523428267L;

    private int nbTables;
    private final Boolean historic;

    public ACRecapDbPanel(Boolean historic) {
        super();
        this.historic = historic;
        nbTables = 0;
        GUIUtilities.setObjectBackground(this, GUIUtilities.ARCHIVING_COLOR);
        initComponents();
        initLayout();
        setDoubleBuffered(true);
    }

    private void initComponents() {
        ArchivingConfiguration ac = ArchivingConfiguration.getSelectedArchivingConfiguration();
        ArchivingConfigurationAttribute[] attrs;
        if (ac == null) {
            attrs = new ArchivingConfigurationAttribute[0];
        } else {
            ArchivingConfigurationAttributes acAttrs = ac.getAttributes();
            attrs = acAttrs.getAttributesList();
        }
        for (int i = 0; i < attrs.length; i++) {
            ACRecapTable tableG = new ACRecapTable();
            ACRecapTableModel modelG = new ACRecapTableModel();

            modelG.load(attrs[i], historic, ACRecapTableModel.TITLE_OR_GENERAL);
            if (modelG.getRowCount() > 0) {
                GUIUtilities.setObjectBackground(tableG, GUIUtilities.ARCHIVING_COLOR);
                add(tableG);
                nbTables++;
            }

            tableG.setModel(modelG);
            tableG.setDoubleBuffered(true);

            ACRecapTable tableP = new ACRecapTable();
            ACRecapTableModel modelP = new ACRecapTableModel();

            modelP.load(attrs[i], historic, ACRecapTableModel.MODE_P);
            if (modelP.getRowCount() > 0) {
                tableP.setModel(modelP);
                add(tableP);
                nbTables++;
            }

            tableP.setModel(modelP);
            tableP.setDoubleBuffered(true);

            ACRecapTable tableA = new ACRecapTable();
            ACRecapTableModel modelA = new ACRecapTableModel();

            modelA.load(attrs[i], historic, ACRecapTableModel.MODE_A);
            if (modelA.getRowCount() > 0) {
                add(tableA);
                nbTables++;
            }

            tableA.setModel(modelA);
            tableA.setDoubleBuffered(true);

            ACRecapTable tableR = new ACRecapTable();
            ACRecapTableModel modelR = new ACRecapTableModel();

            modelR.load(attrs[i], historic, ACRecapTableModel.MODE_R);
            if (modelR.getRowCount() > 0) {
                add(tableR);
                nbTables++;
            }

            tableR.setModel(modelR);
            tableR.setDoubleBuffered(true);

            ACRecapTable tableT = new ACRecapTable();
            ACRecapTableModel modelT = new ACRecapTableModel();

            modelT.load(attrs[i], historic, ACRecapTableModel.MODE_T);
            if (modelT.getRowCount() > 0) {
                add(tableT);
                nbTables++;
            }

            tableT.setModel(modelT);
            tableT.setDoubleBuffered(true);

            ACRecapTable tableD = new ACRecapTable();
            ACRecapTableModel modelD = new ACRecapTableModel();

            modelD.load(attrs[i], historic, ACRecapTableModel.MODE_D);
            if (modelD.getRowCount() > 0) {
                add(tableD);
                nbTables++;
            }

            tableD.setModel(modelD);
            tableD.setDoubleBuffered(true);

            ACRecapTable tableC = new ACRecapTable();
            ACRecapTableModel modelC = new ACRecapTableModel();

            modelC.load(attrs[i], historic, ACRecapTableModel.MODE_C);
            if (modelC.getRowCount() > 0) {
                add(tableC);
                nbTables++;
            }

            tableC.setModel(modelC);
            tableC.setDoubleBuffered(true);

            ACRecapTable tableE = new ACRecapTable();
            ACRecapTableModel modelE = new ACRecapTableModel();

            modelE.load(attrs[i], historic, ACRecapTableModel.MODE_E);
            if (modelE.getRowCount() > 0) {
                add(tableE);
                nbTables++;
            }
            tableE.setModel(modelE);
            tableE.setDoubleBuffered(true);
        }
    }

    public void initLayout() {
        setLayout(new SpringLayout());
        SpringUtilities.makeCompactGrid(this, nbTables, 1, // rows, cols
                0, 0, // initX, initY
                0, 0, // xPad, yPad
                true); // every component same size
    }

    @Override
    public String toString() {
        String stringValue = ObjectUtils.EMPTY_STRING;
        for (int i = 0; i < nbTables; i++) {
            stringValue += this.getComponent(i).toString() + "\n";
        }
        if (stringValue.isEmpty()) {
            return stringValue;
        } else {
            return stringValue.substring(0, stringValue.lastIndexOf("\n"));
        }
    }

}
