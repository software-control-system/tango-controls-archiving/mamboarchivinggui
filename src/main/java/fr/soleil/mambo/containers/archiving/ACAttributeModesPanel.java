package fr.soleil.mambo.containers.archiving;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.components.archiving.ACAttributeModeATable;
import fr.soleil.mambo.components.archiving.ACAttributeModeCTable;
import fr.soleil.mambo.components.archiving.ACAttributeModeDTable;
import fr.soleil.mambo.components.archiving.ACAttributeModeETable;
import fr.soleil.mambo.components.archiving.ACAttributeModePTable;
import fr.soleil.mambo.components.archiving.ACAttributeModeRTable;
import fr.soleil.mambo.components.archiving.ACAttributeModeTTable;
import fr.soleil.mambo.components.archiving.ACAttributeModesTitleTable;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.models.ACAttributeModeATableModel;
import fr.soleil.mambo.models.ACAttributeModeCTableModel;
import fr.soleil.mambo.models.ACAttributeModeDTableModel;
import fr.soleil.mambo.models.ACAttributeModeETableModel;
import fr.soleil.mambo.models.ACAttributeModePTableModel;
import fr.soleil.mambo.models.ACAttributeModeRTableModel;
import fr.soleil.mambo.models.ACAttributeModeTTableModel;
import fr.soleil.mambo.models.ACAttributeModesTitleTableModel;

public class ACAttributeModesPanel extends JPanel {

    private static final long serialVersionUID = -4196449943270222521L;

    private static ACAttributeModesPanel hdbInstance, tdbInstance, ttsInstance;

    protected final Boolean historic;

    protected ACAttributeModesPanel(Boolean historic) {
        super();
        this.historic = historic;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        removeComponents();
        placeComponents();
        GUIUtilities.setObjectBackground(this, GUIUtilities.ARCHIVING_COLOR);
    }

    public static ACAttributeModesPanel getInstance(Boolean historic) {
        ACAttributeModesPanel instance;
        if (historic == null) {
            if (ttsInstance == null) {
                ttsInstance = new ACAttributeModesPanel(historic);
            }
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            if (hdbInstance == null) {
                hdbInstance = new ACAttributeModesPanel(historic);
            }
            instance = hdbInstance;
        } else {
            if (tdbInstance == null) {
                tdbInstance = new ACAttributeModesPanel(historic);
            }
            instance = tdbInstance;
        }
        return instance;
    }

    public void load(ArchivingConfigurationAttribute attribute) {
        ACAttributeModesTitleTableModel.getInstance(historic).load(attribute);
        ACAttributeModePTableModel.getInstance(historic).load(attribute);
        ACAttributeModeATableModel.getInstance(historic).load(attribute);
        ACAttributeModeRTableModel.getInstance(historic).load(attribute);
        ACAttributeModeTTableModel.getInstance(historic).load(attribute);
        ACAttributeModeDTableModel.getInstance(historic).load(attribute);
        ACAttributeModeCTableModel.getInstance(historic).load(attribute);
        ACAttributeModeETableModel.getInstance(historic).load(attribute);
        removeComponents();
        placeComponents();
        repaint();
    }

    private void removeComponents() {
        remove(ACAttributeModesTitleTable.getInstance(historic));
        remove(ACAttributeModePTable.getInstance(historic));
        remove(ACAttributeModeATable.getInstance(historic));
        remove(ACAttributeModeRTable.getInstance(historic));
        remove(ACAttributeModeTTable.getInstance(historic));
        remove(ACAttributeModeDTable.getInstance(historic));
        remove(ACAttributeModeCTable.getInstance(historic));
        remove(ACAttributeModeETable.getInstance(historic));
    }

    private void placeComponents() {
        if (ACAttributeModesTitleTableModel.getInstance(historic).toPaint()) {
            add(ACAttributeModesTitleTable.getInstance(historic));
        }
        if (ACAttributeModePTableModel.getInstance(historic).toPaint()) {
            add(ACAttributeModePTable.getInstance(historic));
        }
        if (ACAttributeModeATableModel.getInstance(historic).toPaint()) {
            add(ACAttributeModeATable.getInstance(historic));
        }
        if (ACAttributeModeRTableModel.getInstance(historic).toPaint()) {
            add(ACAttributeModeRTable.getInstance(historic));
        }
        if (ACAttributeModeTTableModel.getInstance(historic).toPaint()) {
            add(ACAttributeModeTTable.getInstance(historic));
        }
        if (ACAttributeModeDTableModel.getInstance(historic).toPaint()) {
            add(ACAttributeModeDTable.getInstance(historic));
        }
        if (ACAttributeModeCTableModel.getInstance(historic).toPaint()) {
            add(ACAttributeModeCTable.getInstance(historic));
        }
        if (ACAttributeModeETableModel.getInstance(historic).toPaint()) {
            add(ACAttributeModeETable.getInstance(historic));
        }
    }

}
