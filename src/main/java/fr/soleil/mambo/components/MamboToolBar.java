package fr.soleil.mambo.components;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.PrintAction;
import fr.soleil.mambo.actions.ResetAction;
import fr.soleil.mambo.actions.SaveAllToDiskAction;
import fr.soleil.mambo.actions.SaveToDiskAction;
import fr.soleil.mambo.actions.archiving.LoadACAction;
import fr.soleil.mambo.actions.archiving.OpenACEditDialogAction;
import fr.soleil.mambo.actions.view.LoadVCAction;
import fr.soleil.mambo.actions.view.OpenVCEditDialogNewAction;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.sub.ACOptions;
import fr.soleil.mambo.tools.Messages;

public class MamboToolBar extends JMenuBar {

    private static final long serialVersionUID = 585341777838994059L;

    private static final String NEW_ACTION_NAME = Messages.getMessage("TOOLBAR_NEW");
    private static final String LOAD_ACTION_NAME = Messages.getMessage("TOOLBAR_LOAD");
    private static final String AC_ACTION_NAME = Messages.getMessage("TOOLBAR_ARCHIVING_CONFIGURATION");
    private static final String VC_ACTION_NAME = Messages.getMessage("TOOLBAR_VIEW_CONFIGURATION");
    private static final String SAVE_TO_DISK_ACTION_NAME = Messages.getMessage("TOOLBAR_SAVE_TO_DISK");
    private static final String SAVE_ALL_TO_DISK_ACTION_NAME = Messages.getMessage("TOOLBAR_SAVE_ALL_TO_DISK");
    private static final String PRINT_ACTION_NAME = Messages.getMessage("TOOLBAR_PRINT");
    private static final String RESET_ACTION_NAME = Messages.getMessage("TOOLBAR_RESET");

    private static final ImageIcon NEW_ACTION_ICON = new ImageIcon(Mambo.class.getResource("icons/New.gif"));
    private static final ImageIcon LOAD_ACTION_ICON = new ImageIcon(Mambo.class.getResource("icons/Open.gif"));
    private static final ImageIcon SAVE_TO_DISK_ACTION_ICON = new ImageIcon(Mambo.class.getResource("icons/Save.gif"));
    private static final ImageIcon SAVE_ALL_TO_DISK_ACTION_ICON = new ImageIcon(
            Mambo.class.getResource("icons/Quick_Save_All.gif"));
    private static final ImageIcon PRINT_ACTION_ICON = new ImageIcon(Mambo.class.getResource("icons/Print.gif"));
    private static final ImageIcon RESET_ACTION_ICON = new ImageIcon(
            Mambo.class.getResource("icons/Trash_-_Reset.gif"));

    private JMenu newMenu;
    private JMenu loadMenu;
    private JMenu saveMenu;
    private JMenuItem newACItem;
    private JMenuItem newVCItem;
    private JButton newVCButton;
    private JMenuItem loadACItem;
    private JMenuItem loadVCItem;
    private JButton loadVCButton;
    private JMenuItem saveACItem;
    private JMenuItem saveVCItem;
    private JButton saveVCButton;
    private JMenu printMenu;
    private JMenuItem printACItem;
    private JMenuItem printVCItem;
    private JButton printVCButton;
    private JButton saveAllButton;
    private JButton resetButton;
    private PrintAction printACAction;
    private PrintAction printVCAction;
    private ResetAction resetAction;
    private LoadACAction loadACAction;
    private LoadVCAction loadVCAction;
    private SaveToDiskAction saveACAction;
    private SaveToDiskAction saveVCAction;
    private SaveAllToDiskAction saveAllAction;

    private OpenACEditDialogAction newACAction;
    private OpenVCEditDialogNewAction newVCAction;

    private static MamboToolBar instance;

    public static MamboToolBar getInstance() {
        if (instance == null) {
            instance = new MamboToolBar();
            GUIUtilities.setObjectBackground(instance, GUIUtilities.TOOLBAR_COLOR);
        }
        return instance;
    }

    public static MamboToolBar getCurrentInstance() {
        return instance;
    }

    private MamboToolBar() {
        super();
        setFocusTraversalKeysEnabled(true);
        final ToolBarMouseListener listener = new ToolBarMouseListener();
        newMenu = new ToolBarMenu(NEW_ACTION_NAME, NEW_ACTION_ICON, listener);
        GUIUtilities.setObjectBackground(newMenu, GUIUtilities.TOOLBAR_COLOR);

        Options options = Options.getInstance();
        ACOptions acOptions = options.getAcOptions();
        newACAction = new OpenACEditDialogAction(Messages.getMessage("TOOLBAR_NEW_ARCHIVING_CONFIGURATION"), true,
                acOptions.isAlternateSelectionMode());

        newVCAction = OpenVCEditDialogNewAction.getInstance(Messages.getMessage("TOOLBAR_NEW_VIEW_CONFIGURATION"));

        newACItem = new JMenuItem(newACAction);
        newACItem.setText(Messages.getMessage("ACTION_NEW_AC"));
        GUIUtilities.setObjectBackground(newACItem, GUIUtilities.TOOLBAR_COLOR);

        newVCItem = new JMenuItem(newVCAction);
        newVCItem.setText(Messages.getMessage("ACTION_NEW_VC"));
        GUIUtilities.setObjectBackground(newVCItem, GUIUtilities.TOOLBAR_COLOR);

        newVCButton = new ToolBarButton(newVCAction, true, listener);
        newVCButton.setIcon(NEW_ACTION_ICON);
        newVCButton.setToolTipText(Messages.getMessage("TOOLBAR_NEW_VIEW_CONFIGURATION"));
        GUIUtilities.setObjectBackground(newVCButton, GUIUtilities.TOOLBAR_COLOR);

        if (Mambo.hasACs()) {
            newMenu.add(newACItem);
            newMenu.add(newVCItem);
            add(newMenu);
        } else {
            add(newVCButton);
        }

        loadMenu = new ToolBarMenu(LOAD_ACTION_NAME, LOAD_ACTION_ICON, listener);
        GUIUtilities.setObjectBackground(loadMenu, GUIUtilities.TOOLBAR_COLOR);

        loadACItem = new JMenuItem(AC_ACTION_NAME);
        loadACAction = new LoadACAction(AC_ACTION_NAME, false);
        loadACItem.addActionListener(loadACAction);
        GUIUtilities.setObjectBackground(loadACItem, GUIUtilities.TOOLBAR_COLOR);

        loadVCItem = new JMenuItem(VC_ACTION_NAME);
        loadVCAction = new LoadVCAction(Messages.getMessage("TOOLBAR_LOAD_VIEW_CONFIGURATION"), false);
        loadVCItem.addActionListener(loadVCAction);
        GUIUtilities.setObjectBackground(loadVCItem, GUIUtilities.TOOLBAR_COLOR);

        loadVCButton = new ToolBarButton(loadVCAction, true, listener);
        loadVCButton.setIcon(LOAD_ACTION_ICON);
        GUIUtilities.setObjectBackground(loadVCButton, GUIUtilities.TOOLBAR_COLOR);

        if (Mambo.hasACs()) {
            loadMenu.add(loadACItem);
            loadMenu.add(loadVCItem);
            add(loadMenu);
        } else {
            add(loadVCButton);
        }

        saveMenu = new ToolBarMenu(SAVE_TO_DISK_ACTION_NAME, SAVE_TO_DISK_ACTION_ICON, listener);
        GUIUtilities.setObjectBackground(saveMenu, GUIUtilities.TOOLBAR_COLOR);

        saveACItem = new JMenuItem(AC_ACTION_NAME);
        saveACAction = new SaveToDiskAction(Messages.getMessage("TOOLBAR_SAVE_ARCHIVING_CONFIGURATION"),
                SAVE_TO_DISK_ACTION_ICON, SaveToDiskAction.AC_TYPE);
        saveACItem.addActionListener(saveACAction);
        GUIUtilities.setObjectBackground(saveACItem, GUIUtilities.TOOLBAR_COLOR);

        saveVCItem = new JMenuItem(VC_ACTION_NAME);
        saveVCAction = new SaveToDiskAction(Messages.getMessage("TOOLBAR_SAVE_VIEW_CONFIGURATION"),
                SAVE_TO_DISK_ACTION_ICON, SaveToDiskAction.VC_TYPE);
        saveVCItem.addActionListener(saveVCAction);
        GUIUtilities.setObjectBackground(saveVCItem, GUIUtilities.TOOLBAR_COLOR);
        saveVCButton = new ToolBarButton(saveVCAction, true, listener);
        saveVCButton.setIcon(SAVE_TO_DISK_ACTION_ICON);
        GUIUtilities.setObjectBackground(saveVCButton, GUIUtilities.TOOLBAR_COLOR);

        if (Mambo.hasACs()) {
            saveMenu.add(saveACItem);
            saveMenu.add(saveVCItem);

            add(saveMenu);
        } else {
            add(saveVCButton);
        }

        saveAllAction = new SaveAllToDiskAction(SAVE_ALL_TO_DISK_ACTION_NAME, SAVE_ALL_TO_DISK_ACTION_ICON,
                SaveAllToDiskAction.BOTH_TYPE);
        saveAllButton = new ToolBarButton(saveAllAction, true, listener);
        GUIUtilities.setObjectBackground(saveAllButton, GUIUtilities.TOOLBAR_COLOR);
        add(saveAllButton);

        printMenu = new ToolBarMenu(PRINT_ACTION_NAME, PRINT_ACTION_ICON, listener);
        GUIUtilities.setObjectBackground(printMenu, GUIUtilities.TOOLBAR_COLOR);

        printACItem = new JMenuItem(AC_ACTION_NAME);
        printACAction = new PrintAction(Messages.getMessage("TOOLBAR_PRINT_ARCHIVING_CONFIGURATION"), PRINT_ACTION_ICON,
                PrintAction.AC_TYPE);
        printACItem.addActionListener(printACAction);
        GUIUtilities.setObjectBackground(printACItem, GUIUtilities.TOOLBAR_COLOR);

        printVCItem = new JMenuItem(VC_ACTION_NAME);
        printVCAction = new PrintAction(Messages.getMessage("TOOLBAR_PRINT_VIEW_CONFIGURATION"), PRINT_ACTION_ICON,
                PrintAction.VC_TYPE);
        printVCItem.addActionListener(printVCAction);
        GUIUtilities.setObjectBackground(printVCItem, GUIUtilities.TOOLBAR_COLOR);

        printVCButton = new ToolBarButton(printVCAction, true, listener);
        printVCButton.setIcon(PRINT_ACTION_ICON);
        GUIUtilities.setObjectBackground(printVCButton, GUIUtilities.TOOLBAR_COLOR);

        if (Mambo.hasACs()) {
            printMenu.add(printACItem);
            printMenu.add(printVCItem);
            add(printMenu);
        } else {
            add(printVCButton);
        }

        resetAction = new ResetAction(RESET_ACTION_NAME, RESET_ACTION_ICON);
        resetButton = new ToolBarButton(resetAction, true, listener);
        GUIUtilities.setObjectBackground(resetButton, GUIUtilities.TOOLBAR_COLOR);
        add(resetButton);

    }

    public void setAlternateSelection(boolean isAlternate) {
        newACAction.setAlternateSelectionMode(isAlternate);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private interface IToolBarElement {
        public Border getBorder();

        public void setBorder(Border border);

        public Border getDefaultBorder();

        public Border getHidhlightBorder();
    }

    /**
     * A toolbar item, whether it directly calls an action or not.
     * 
     * @author CLAISSE
     */
    private class ToolBarButton extends JButton implements IToolBarElement {

        private static final long serialVersionUID = 4519643326720157768L;

        private final Border defaultBorder, highlightBorder;

        /**
         * @param a
         *            The action to put on the button
         * @param doSomething
         *            True if the action has to be called upon button press;
         *            false if the button has sub-menus
         */
        public ToolBarButton(Action a, boolean doSomething, ToolBarMouseListener listener) {
            super((Icon) a.getValue(Action.SMALL_ICON));

            defaultBorder = new EmptyBorder(2, 5, 2, 5);
            highlightBorder = new CompoundBorder(new LineBorder(Color.DARK_GRAY, 1), new EmptyBorder(1, 4, 1, 4));
            setBorder(defaultBorder);

            String toolTip = (String) a.getValue(Action.SHORT_DESCRIPTION);
            if (toolTip == null) {
                toolTip = (String) a.getValue(Action.NAME);
            }

            if (toolTip != null) {
                setToolTipText(toolTip);
            }

            if (doSomething) {
                addActionListener(a);
            }
            addMouseListener(listener);

            setFocusable(false);
            setOpaque(false);
        }

        @Override
        public Border getDefaultBorder() {
            return defaultBorder;
        }

        @Override
        public Border getHidhlightBorder() {
            return highlightBorder;
        }
    }

    /**
     * A toolbar item, whether it directly calls an action or not.
     * 
     * @author CLAISSE
     */
    private class ToolBarMenu extends JMenu implements IToolBarElement {

        private static final long serialVersionUID = -5360085596445353097L;

        private final Border defaultBorder, highlightBorder;

        /**
         * @param a
         *            The action to put on the button
         * @param doSomething
         *            True if the action has to be called upon button press;
         *            false if the button has sub-menus
         */
        public ToolBarMenu(String text, ImageIcon icon, ToolBarMouseListener listener) {
            super();

            defaultBorder = new EmptyBorder(1, 3, 1, 3);
            highlightBorder = new CompoundBorder(new LineBorder(Color.DARK_GRAY, 1), new EmptyBorder(0, 2, 0, 2));
            setBorder(defaultBorder);

            setToolTipText(text);
            setIcon(icon);

            addMouseListener(listener);
            setFocusable(false);

            setBackground(GUIUtilities.getToolBarColor());
        }

        @Override
        public Border getDefaultBorder() {
            return defaultBorder;
        }

        @Override
        public Border getHidhlightBorder() {
            return highlightBorder;
        }
    }

    private class ToolBarMouseListener extends MouseAdapter {

        public ToolBarMouseListener() {
            super();
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if (e.getSource() instanceof IToolBarElement) {
                IToolBarElement element = (IToolBarElement) e.getSource();
                element.setBorder(element.getHidhlightBorder());
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if (e.getSource() instanceof IToolBarElement) {
                IToolBarElement element = (IToolBarElement) e.getSource();
                element.setBorder(element.getDefaultBorder());
            }
        }
    }

}
