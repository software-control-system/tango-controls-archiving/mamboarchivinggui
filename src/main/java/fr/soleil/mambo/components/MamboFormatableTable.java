/*	Synchrotron Soleil 
 *  
 *   File          :  MamboFormatableTable.java
 *  
 *   Project       :  mambo
 *  
 *   Description   :  
 *  
 *   Author        :  SOLEIL
 *  
 *   Original      :  13 d�c. 2005 
 *  
 *   Revision:  					Author:  
 *   Date: 							State:  
 *  
 *   Log: MamboFormatableTable.java,v 
 *
 */
package fr.soleil.mambo.components;

import javax.swing.JTable;
import javax.swing.table.TableModel;

import fr.soleil.mambo.options.Options;

/**
 * 
 * @author SOLEIL
 */
public class MamboFormatableTable extends JTable {

    private static final long serialVersionUID = 8578524898539027832L;

    public MamboFormatableTable() {
        super();
    }

    public MamboFormatableTable(TableModel model) {
        super(model);
    }

    @Override
    public String toString() {
        return appendStringValue(null).toString();
    }

    /**
     * Appends the String representation of this table to a
     * {@link StringBuilder}
     * 
     * @param builder
     *            The {@link StringBuilder}. can be <code>null</code>, in which
     *            case the resulting {@link StringBuilder} is a new one
     * @return The given {@link StringBuilder}, to which the string
     *         representation was appended.
     */
    public StringBuilder appendStringValue(StringBuilder builder) {
        StringBuilder stringValue = builder;
        if (stringValue == null) {
            stringValue = new StringBuilder();
        }
        boolean added = false;
        for (int row = 0; row < getRowCount(); row++) {
            for (int col = 0; col < getColumnCount(); col++) {
                stringValue.append(getModel().getValueAt(row, col)).append(
                        Options.getInstance().getGeneralOptions().getSeparator());
            }
            if (getColumnCount() > 0) {
                int index = builder.lastIndexOf(Options.getInstance().getGeneralOptions().getSeparator());
                if (index > -1) {
                    stringValue.delete(index, stringValue.length());
                }
            }
            added = true;
            stringValue.append("\n");
        }
        if (added) {
            int length = stringValue.length();
            stringValue.delete(length - 1, length);
        }
        return stringValue;
    }

}
