package fr.soleil.mambo.components.renderers;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.tango.data.adapter.StateAdapter;
import fr.soleil.mambo.tools.Messages;

public class StateTableRenderer implements TableCellRenderer {

    private final MyStateAdapter stateAdapter;

    public StateTableRenderer() {
        super();
        stateAdapter = new MyStateAdapter();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        String val = Messages.getMessage("VIEW_ATTRIBUTES_NO_DATA");
        boolean needsColor = false;
        if (value != null) {
            if (value instanceof Integer) {
                needsColor = true;
                val = TangoConst.Tango_DevStateName[(Integer) value];
            } else {
                val = value.toString();
                needsColor = false;
            }
        }
        JLabel label = new JLabel(val);
        label.setToolTipText(val);
        if (needsColor) {
            CometeColor color = null;
            if (val != null) {
                val = val.trim().toUpperCase();
            }
            color = stateAdapter.getColor(val);

            if (color != null) {
                label.setBackground(ColorTool.getColor(color));
                label.setOpaque(true);
            } else {
                label.setOpaque(false);
            }
        } else {
            label.setOpaque(false);
        }
        return label;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class MyStateAdapter extends StateAdapter<String> {
        public MyStateAdapter() {
            super(null, String.class);
        }

        public CometeColor getColor(String name) {
            return adaptSourceData(name);
        }
    };
}
