package fr.soleil.mambo.components.renderers;

import java.awt.Component;
import java.awt.Font;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.data.IConfiguration;
import fr.soleil.mambo.datasources.db.archiving.ArchivingManagerFactory;
import fr.soleil.mambo.tools.TreeUtils;

public abstract class AConfTreeRenderer<C extends IConfiguration> extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = -367066285705673998L;

    protected static final Icon CLOSED_ICON = UIManager.getIcon("Tree.closedIcon");
    protected static final ImageIcon DISABLED_ICON = new ImageIcon(Mambo.class.getResource("icons/bulbDisabled.gif"));
    protected static final ImageIcon ENABLED_HDB_ICON = new ImageIcon(Mambo.class.getResource("icons/bulbEnabled.gif"));
    protected static final ImageIcon ENABLED_TDB_ICON = new ImageIcon(
            Mambo.class.getResource("icons/bulbEnabledTemp.gif"));
    protected static final ImageIcon ENABLED_TTS_ICON = new ImageIcon(
            Mambo.class.getResource("icons/bulbEnabledTTS.gif"));
    protected static final ImageIcon ENABLED_HDB_TDB_ICON = new ImageIcon(
            Mambo.class.getResource("icons/bulbHDBTDB.gif"));
    protected static final ImageIcon ENABLED_HDB_TTS_ICON = new ImageIcon(
            Mambo.class.getResource("icons/bulbHDBTTS.gif"));
    protected static final ImageIcon ENABLED_TDB_TTS_ICON = new ImageIcon(
            Mambo.class.getResource("icons/bulbTDBTTS.gif"));
    protected static final ImageIcon ENABLED_HDB_TDB_TTS_ICON = new ImageIcon(
            Mambo.class.getResource("icons/bulbHDBTDBTTS.gif"));
    protected static final ImageIcon UNKNOWN_ICON = new ImageIcon(Mambo.class.getResource("icons/unknown.png"),
            "unknown state");

    protected static final Font PLAIN = new Font(Font.SANS_SERIF, Font.PLAIN, 12);
    protected static final Font ITALIC = new Font(Font.SANS_SERIF, Font.ITALIC, 12);
    protected static final Font BOLD = new Font(Font.SANS_SERIF, Font.BOLD, 12);

    public AConfTreeRenderer() {
        super();
    }

    protected abstract Icon getRootIcon();

    protected abstract C getConfiguration();

    protected abstract Boolean isHistoric(C configuration);

    protected abstract boolean isEmpty(C configuration, String attributeName);

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
            int row, boolean hasFocus) {
        final DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        final JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
                hasFocus);
        Font font = label.getFont();
        C configuration = getConfiguration();
        boolean iah = false, iat = false, iatts = false, hdbDown = false, tdbDown = false, ttsDown = false;
        if (node.getLevel() == 4) {
            final String nameH = TreeUtils.getFullAttributeNameFromNode(tree, Boolean.TRUE, node);
            final String nameT = TreeUtils.getFullAttributeNameFromNode(tree, Boolean.FALSE, node);
            final String nameTTS = TreeUtils.getFullAttributeNameFromNode(tree, null, node);
            if (configuration != null) {
                String name;
                Boolean historic = isHistoric(configuration);
                if (historic == null) {
                    name = nameTTS;
                } else if (historic.booleanValue()) {
                    name = nameH;
                } else {
                    name = nameT;
                }
                if (configuration.containsAttribute(name)) {
                    if (isEmpty(configuration, name)) {
                        font = ITALIC;
                    } else {
                        font = BOLD;
                    }
                } else {
                    font = ITALIC;
                }
            }
            try {
                iah = ArchivingManagerFactory.getCurrentImpl().isArchived(nameH, Boolean.TRUE);
            } catch (final Exception e) {
                hdbDown = true;
            }
            try {
                iat = ArchivingManagerFactory.getCurrentImpl().isArchived(nameT, Boolean.FALSE);
            } catch (final Exception e) {
                tdbDown = true;
            }
            try {
                iatts = ArchivingManagerFactory.getCurrentImpl().isArchived(nameT, null);
            } catch (final Exception e) {
                ttsDown = true;
            }
            if (iatts) {
                if (iah) {
                    if (iat) {
                        label.setIcon(ENABLED_HDB_TDB_TTS_ICON);
                    } else {
                        label.setIcon(ENABLED_HDB_TTS_ICON);
                    }
                } else if (iat) {
                    label.setIcon(ENABLED_TDB_TTS_ICON);
                } else {
                    label.setIcon(ENABLED_TTS_ICON);
                }
            } else if (iah) {
                if (iat) {
                    label.setIcon(ENABLED_HDB_TDB_ICON);
                } else {
                    label.setIcon(ENABLED_HDB_ICON);
                }
            } else if (iat) {
                label.setIcon(ENABLED_TDB_ICON);
            } else if (hdbDown && tdbDown && ttsDown) {
                // connection error
                label.setIcon(UNKNOWN_ICON);
            } else {
                // no archiving
                label.setIcon(DISABLED_ICON);
            }
        } else {
            if (node.isRoot()) {
                label.setIcon(getRootIcon());
            } else {
                label.setIcon(CLOSED_ICON);
            }
            font = PLAIN;
        }
        label.setFont(font);
        label.setToolTipText(label.getText());
        return label;
    }

}
