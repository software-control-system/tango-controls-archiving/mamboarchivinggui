package fr.soleil.mambo.components.renderers;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.UIResource;
import javax.swing.table.TableCellRenderer;

/**
 * This renders boolean as a checkbox ,just like the default table renderer for boolean. But in case of null value, it
 * will display an empty label.
 * 
 * @author guest
 * 
 */
public class ViewSpectrumTableBooleanRenderer extends JCheckBox implements TableCellRenderer, UIResource {

    private static final long serialVersionUID = -4748865928517656233L;

    private static final Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

    private static final JComponent EMPTY_VALUE = new JLabel();

    public ViewSpectrumTableBooleanRenderer() {
	super();
	setHorizontalAlignment(SwingConstants.CENTER);
	setBorderPainted(true);

	// opaque to let user see the row selected state
	EMPTY_VALUE.setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
	    int row, int column) {

	if (value == null) {
	    if (isSelected) {
		EMPTY_VALUE.setForeground(table.getSelectionForeground());
		EMPTY_VALUE.setBackground(table.getSelectionBackground());
	    } else {
		EMPTY_VALUE.setForeground(table.getForeground());
		EMPTY_VALUE.setBackground(table.getBackground());
	    }

	    if (hasFocus) {
		EMPTY_VALUE.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
	    } else {
		EMPTY_VALUE.setBorder(noFocusBorder);
	    }

	    return EMPTY_VALUE;
	}

	if (isSelected) {
	    setForeground(table.getSelectionForeground());
	    setBackground(table.getSelectionBackground());
	} else {
	    setForeground(table.getForeground());
	    setBackground(table.getBackground());
	}
	setSelected((value != null && ((Boolean) value).booleanValue()));

	if (hasFocus) {
	    setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
	} else {
	    setBorder(noFocusBorder);
	}

	return this;
    }
}
