package fr.soleil.mambo.components.renderers;

import javax.swing.ImageIcon;

import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.data.view.ViewConfiguration;

public class VCTreeRenderer extends AConfTreeRenderer<ViewConfiguration> {

    private static final long serialVersionUID = -2953209529198384420L;

    protected static final ImageIcon ROOT_ICON = new ImageIcon(Mambo.class.getResource("icons/database.gif"));

    private final ViewConfigurationBean viewConfigurationBean;
    private final boolean isEditingView;

    public VCTreeRenderer(final ViewConfigurationBean viewConfigurationBean, final boolean isEditingView) {
        super();
        this.viewConfigurationBean = viewConfigurationBean;
        this.isEditingView = isEditingView;
    }

    @Override
    protected ImageIcon getRootIcon() {
        return ROOT_ICON;
    }

    @Override
    protected ViewConfiguration getConfiguration() {
        ViewConfiguration currentViewConfiguration;
        if (isEditingView) {
            currentViewConfiguration = viewConfigurationBean.getEditingViewConfiguration();
        } else {
            currentViewConfiguration = viewConfigurationBean.getViewConfiguration();
        }
        return currentViewConfiguration;
    }

    @Override
    protected Boolean isHistoric(ViewConfiguration configuration) {
        return configuration.getData().isHistoric();
    }

    @Override
    protected boolean isEmpty(ViewConfiguration configuration, String attributeName) {
        return configuration.getAttributes().getAttribute(attributeName).isEmpty();
    }

}
