package fr.soleil.mambo.components.renderers;

public class ACRecapTableRenderer extends AACTableRenderer {

    private static final long serialVersionUID = -2208360782927103199L;

    public ACRecapTableRenderer() {
        super();
    }

    public ACRecapTableRenderer(boolean forceTitle) {
        super(forceTitle);
    }

    @Override
    protected int getValueColumn() {
        return 4;
    }

}
