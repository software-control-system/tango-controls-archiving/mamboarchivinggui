package fr.soleil.mambo.components.renderers;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;

public class ACTreeRenderer extends AConfTreeRenderer<ArchivingConfiguration> {

    private static final long serialVersionUID = -710841662854649696L;

    protected static final ImageIcon ROOT_ICON = new ImageIcon(Mambo.class.getResource("icons/host.gif"));

    public ACTreeRenderer() {
        super();
    }

    @Override
    protected Icon getRootIcon() {
        return ROOT_ICON;
    }

    @Override
    protected ArchivingConfiguration getConfiguration() {
        return ArchivingConfiguration.getCurrentArchivingConfiguration();
    }

    @Override
    protected Boolean isHistoric(ArchivingConfiguration configuration) {
        return configuration.isHistoric();
    }

    @Override
    protected boolean isEmpty(ArchivingConfiguration configuration, String attributeName) {
        return configuration.getAttributes().getAttribute(attributeName).isEmpty();
    }

}
