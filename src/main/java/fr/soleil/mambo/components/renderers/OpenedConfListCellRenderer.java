package fr.soleil.mambo.components.renderers;

import java.awt.Component;
import java.awt.Image;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.data.IConfiguration;

public abstract class OpenedConfListCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = 7588964933137689966L;

    protected static final String SPACE = " ";

    protected static final ImageIcon MODIFIED_ICON = new ImageIcon(Mambo.class.getResource("icons/star_red.gif"));
    static {
        MODIFIED_ICON.setImage(MODIFIED_ICON.getImage().getScaledInstance(10, 10, Image.SCALE_DEFAULT));
    }

    protected OpenedConfListCellRenderer() {
        super();
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index,
            boolean isSelected, boolean cellHasFocus) {
        JLabel ret = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (value instanceof IConfiguration) {
            IConfiguration conf = (IConfiguration) value;
            index++;
            StringBuilder text = new StringBuilder(SPACE);
            if (index != 0) {
                text.append(index);
                text.append(IConfiguration.DISPAY_SEPARATOR);
            }
            text = conf.displayableTitleToStringBuilder(text);
            ret.setText(text.toString());
            ret.setIcon(conf.isModified() ? MODIFIED_ICON : null);
        } else {
            ret.setText(ObjectUtils.EMPTY_STRING);
            ret.setIcon(null);
        }
        return ret;
    }

}
