package fr.soleil.mambo.components.renderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.mambo.components.archiving.ACAttributeModesTitleTable;

public abstract class AACTableRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -1784673563403143078L;

    protected static final Font FONT = new Font("Arial", Font.PLAIN, 11);
    protected static final Font TITLE_FONT = new Font("Arial", Font.BOLD, 11);
    protected static final Color DARK_RED = new Color(150, 0, 0);
    protected final JLabel label;
    protected final JCheckBoxMenuItem check;

    protected boolean forceTitle;

    public AACTableRenderer() {
        this(false);
    }

    public AACTableRenderer(boolean forceTitle) {
        super();
        this.forceTitle = forceTitle;
        label = new JLabel();
        label.setOpaque(true);
        check = new JCheckBoxMenuItem();
        check.setBorderPainted(false);
        check.setSelected(false);
        check.setEnabled(false);
        check.setFocusable(false);
        check.setContentAreaFilled(false);
    }

    protected abstract int getValueColumn();

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        Component comp;
        if (value instanceof Boolean) {
            boolean hasMode = ((Boolean) value).booleanValue();
            Component c = table.getParent();
            if (c != null) {
                check.setBackground(c.getBackground());
            }
            check.setSelected(hasMode);
            comp = check;
        } else if (value instanceof String) {
            label.setText((String) value);
            GUIUtilities.setObjectBackground(label, GUIUtilities.ARCHIVING_COLOR);
            label.setForeground(Color.BLACK);
            if (isSelected || hasFocus) {
                label.setBackground(Color.LIGHT_GRAY);
            }
            if (forceTitle || ((table instanceof ACAttributeModesTitleTable) && row == 0)) {
                label.setFont(TITLE_FONT);
            } else {
                label.setFont(FONT);
                if (value != null && column == getValueColumn()
                        && !value.equals(table.getModel().getValueAt(row, getValueColumn() - 1))) {
                    label.setForeground(DARK_RED);
                } else {
                    label.setForeground(Color.BLACK);
                }
            }
            comp = label;
        } else if (value instanceof JScrollPane) {
            comp = (JScrollPane) value;
        } else {
            comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
        return comp;
    }

}
