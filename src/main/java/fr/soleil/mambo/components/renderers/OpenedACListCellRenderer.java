//+======================================================================
// $Source: /cvsroot/tango-cs/tango/tools/mambo/components/renderers/OpenedACListCellRenderer.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  OpenedACListCellRenderer.
//						(Claisse Laurent) - nov. 2005
//
// $Author: ounsy $
//
// $Revision: 1.2 $
//
// $Log: OpenedACListCellRenderer.java,v $
// Revision 1.2  2006/07/28 10:07:12  ounsy
// icons moved to "icons" package
//
// Revision 1.1  2005/11/29 18:27:45  chinkumo
// no message
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.mambo.components.renderers;

public class OpenedACListCellRenderer extends OpenedConfListCellRenderer {

    private static final long serialVersionUID = 2996124181059537408L;

    public OpenedACListCellRenderer() {
        super();
    }

}
