package fr.soleil.mambo.components.renderers;

public class ACAttributeDetailTableRenderer extends AACTableRenderer {

    private static final long serialVersionUID = -5999220872525478686L;

    public ACAttributeDetailTableRenderer() {
        super();
    }

    public ACAttributeDetailTableRenderer(boolean forceTitle) {
        super(forceTitle);
    }

    @Override
    protected int getValueColumn() {
        return 3;
    }

}
