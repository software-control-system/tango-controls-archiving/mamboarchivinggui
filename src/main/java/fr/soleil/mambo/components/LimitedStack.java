package fr.soleil.mambo.components;

import java.util.Vector;

import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.options.sub.VCOptions;

// LimitedStack must be a Vector! see TANGOARCH-739
public class LimitedStack<H> extends Vector<H> {

    private static final long serialVersionUID = 8403702844224805194L;

    // MULTI-CONF
    private int maxSize;

    public LimitedStack(int maxSize) {
        super();
        this.maxSize = maxSize;
    }

    public LimitedStack() {
        super();
        Options options = Options.getInstance();
        VCOptions vcOptions = options.getVcOptions();
        this.maxSize = vcOptions.getStackDepth();
    }

    public void push(H item) {
        remove(item);
        if (size() == this.maxSize) {
            remove(lastElement());
        }
        insertElementAt(item, 0);
    }

    /**
     * @return Returns the maxSize.
     */
    public int getMaxSize() {
        return maxSize;
    }

    /**
     * @param maxSize The maxSize to set.
     */
    public void setMaxSize(int maxSize) {
        while (maxSize < size()) {
            remove(lastElement());
        }

        this.maxSize = maxSize;
    }

    public void selectElement(int idx) {
        H item = get(idx);
        push(item);
    }

}
