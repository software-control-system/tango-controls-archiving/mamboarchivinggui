package fr.soleil.mambo.components.view.images;

import java.util.Collection;

import javax.swing.table.DefaultTableModel;

import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.mambo.tools.Messages;

/**
 * The table model used by PartialImageDataTable, this model lists partially
 * loaded images in a given date range. Its rows are the partially loaded
 * images.
 * 
 * @author CLAISSE
 */
public class PartialImageDataTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -2550761049245802753L;

    private ImageData[] rows;
    private String dateMsg;
    private String dimXMsg;
    private String dimYMsg;

    public PartialImageDataTableModel() {
        super();
        dateMsg = Messages.getMessage("VIEW_IMAGE_DATE");
        dimXMsg = Messages.getMessage("VIEW_IMAGE_DIM_X");
        dimYMsg = Messages.getMessage("VIEW_IMAGE_DIM_Y");
    }

    public void setData(Collection<ImageData> partialImageData) {
        if ((partialImageData == null) || partialImageData.isEmpty()) {
            this.rows = null;
        } else {
            this.rows = partialImageData.toArray(new ImageData[partialImageData.size()]);
        }
        fireTableStructureChanged();
    }

    /**
     * Returns the image row located at row <code>rowIndex</code>.
     * 
     * @param rowIndex
     *            The specified row
     * @return The image row located at row <code>rowIndex</code>
     */
    public ImageData getRow(int rowIndex) {
        return rows[rowIndex];
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return 3;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        if (rows == null) {
            return 0;
        }

        return rows.length;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ImageData row = this.getRow(rowIndex);
        if (row == null) {
            return null;
        }

        switch (columnIndex) {
        case 0:
            return row.getDate();
        case 1:
            return String.valueOf(row.getDimX());
        case 2:
            return String.valueOf(row.getDimY());
            /*
             * case 3: return row;
             */
        default:
            return null;
        }

    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return dateMsg;
        case 1:
            return dimXMsg;
        case 2:
            return dimYMsg;
            /*
             * case 3: return " ";
             */
        default:
            return null;
        }
    }
}
