package fr.soleil.mambo.components.view;

import java.beans.PropertyChangeListener;

import fr.soleil.mambo.actions.view.listeners.OpenedVCItemListener;
import fr.soleil.mambo.bean.manager.ViewConfigurationBeanManager;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.OpenedConfComboBox;
import fr.soleil.mambo.components.renderers.OpenedVCListCellRenderer;
import fr.soleil.mambo.containers.archiving.ArchivingActionPanel;
import fr.soleil.mambo.data.view.ViewConfiguration;

public class OpenedVCComboBox extends OpenedConfComboBox<ViewConfiguration> {

    private static final long serialVersionUID = 8889179374457612272L;

    // MULTI-CONF
    private static OpenedVCComboBox instance = null;
    private final OpenedVCItemListener openedVCComboBoxItemListener;

    /**
     * @return 8 juil. 2005
     */
    public static OpenedVCComboBox getInstance(LimitedVCStack stack) {
        if (instance == null) {
            instance = new OpenedVCComboBox(stack);
        }
        return instance;
    }

    public static OpenedVCComboBox getInstance() {
        return instance;
    }

    private OpenedVCComboBox(LimitedVCStack stack) {
        super(stack);
        openedVCComboBoxItemListener = new OpenedVCItemListener();
        addItemListener(openedVCComboBoxItemListener);
        setRenderer(new OpenedVCListCellRenderer());
    }

    public void empty() {
        stack.clear();
        setElements(new LimitedVCStack());
    }

    public static void refresh() {
        if (instance != null) {
            instance.repaint();
        }
    }

    public void setElements(LimitedVCStack stack) {
        super.setElements(stack);
        ViewConfigurationBeanManager.getInstance().clean();
    }

    public LimitedVCStack getVCElements() {
        return (LimitedVCStack) stack;
    }

    /**
     * @param currentDomainName
     */
    public void selectElement(ViewConfiguration vc) {
        ViewConfiguration previous = null;
        if (vc != null) {
            previous = getSelectedVC();
            setSelectedItem(vc);
        }
        updateUIWithSelected(previous);
    }

    /**
     * 
     */
    public static void removeSelectedElement() {
        if (instance != null) {
            ViewConfiguration selectedVC = ViewConfigurationBeanManager.getInstance().getSelectedConfiguration();
            instance.removeElement(selectedVC);
            instance.repaint();
        }
    }

    /**
     * @param selectedVC
     */
    public void removeElement(ViewConfiguration selectedVC) {
        if ((selectedVC != null) && (stack.size() > 0)) {
            stack.removeElement(selectedVC);
            super.removeItem(selectedVC);
            ViewConfigurationBeanManager.getInstance().removeConfiguration(selectedVC);
            updateUIWithSelected(selectedVC);
            // RG : Even if following code looks useless, it is necessary.
            // Otherwise, the combobox is not fully cleaned when removing last
            // element
            if (getItemCount() == 0) {
                removeAllItems();
            }
        }
    }

    public ViewConfiguration getSelectedVC() {
        return ((LimitedVCStack) stack).getSelectedVC();
    }

    public OpenedVCItemListener getOpenedVCComboBoxItemListener() {
        return openedVCComboBoxItemListener;
    }

    public void updateUIWithSelected() {
        updateUIWithSelected(null);
    }

    private void updateUIWithSelected(ViewConfiguration previousSelection) {
        if (previousSelection != null) {
            ViewConfigurationBean bean = ViewConfigurationBeanManager.getInstance().getBeanFor(previousSelection);
            if (bean != null) {
                bean.getAttributesPanel().getViewAttributesGraphPanel()
                        .removePropertyChangeListener(ArchivingActionPanel.getInstance());
            }
        }
        ViewConfigurationBeanManager.getInstance().setSelectedConfiguration(getSelectedVC());
        ViewConfigurationBean bean = ViewConfigurationBeanManager.getInstance().getBeanFor(getSelectedVC());
        if (bean != null) {
            PropertyChangeListener listener = ArchivingActionPanel.getInstance();
            // remove before add to avoid double registration
            bean.getAttributesPanel().getViewAttributesGraphPanel().removePropertyChangeListener(listener);
            bean.getAttributesPanel().getViewAttributesGraphPanel().addPropertyChangeListener(listener);
        }
        repaint();
    }

}
