package fr.soleil.mambo.components.view;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.actions.view.listeners.ExpressionTreeListener;
import fr.soleil.mambo.components.renderers.ExpressionTreeRenderer;
import fr.soleil.mambo.containers.view.dialogs.ExpressionTab;
import fr.soleil.mambo.data.view.ExpressionAttribute;
import fr.soleil.mambo.models.ExpressionTreeModel;

public class ExpressionTree extends JTree {

    private static final long serialVersionUID = 8027931060874400291L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ExpressionTree.class);

    private TreePath[] lastSelectionsPath;
    private TreePath[] currentSelectionsPath;
    private final ExpressionTreeListener expressionTreeListener;

    public ExpressionTree(final ExpressionTab expressionTab) {
        super(new ExpressionTreeModel());
        setCellRenderer(ExpressionTreeRenderer.getInstance());
        expressionTreeListener = new ExpressionTreeListener(expressionTab);
        addTreeSelectionListener(expressionTreeListener);
        lastSelectionsPath = null;
        currentSelectionsPath = null;
    }

    public Collection<ExpressionAttribute> getLastListOfAttributesToSet() {
        final TreePath[] selectedPath = lastSelectionsPath;
        return treePathToVector(selectedPath);
    }

    private Collection<ExpressionAttribute> treePathToVector(final TreePath[] selectedPath) {
        final Collection<ExpressionAttribute> attributes;
        if (selectedPath == null || selectedPath.length == 0) {
            attributes = null;
        } else {
            attributes = new ArrayList<ExpressionAttribute>();
            for (final TreePath currentSelectedTreePath : selectedPath) {
                if (currentSelectedTreePath.getPathCount() != 1) {
                    final DefaultMutableTreeNode currentSelectedNode = (DefaultMutableTreeNode) currentSelectedTreePath
                            .getLastPathComponent();
                    if (!currentSelectedNode.isRoot()) {
                        final ExpressionAttribute attr = getModel().getExpressionAttributes()
                                .get(currentSelectedNode.getUserObject());
                        if (attr != null) {
                            attributes.add(attr);
                        }
                    }
                }
            }
        }
        return attributes;
    }

    public Collection<ExpressionAttribute> getSelectedAttributes() {
        saveCurrentSelection();
        return treePathToVector(currentSelectionsPath);
    }

    public ExpressionAttribute getSelectedAttribute() {
        ExpressionAttribute attr = null;
        TreePath selectedPath = getSelectionPath();
        if ((selectedPath != null) && (selectedPath.getPathCount() != 1)) {
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
            if (!selectedNode.isRoot()) {
                final String attributeName = (String) selectedNode.getUserObject();
                attr = getModel().getExpressionAttribute(attributeName);
            }
        }
        return attr;
    }

    public ExpressionAttribute expressionTab() {
        ExpressionAttribute attr = null;
        TreePath selectedPath = getSelectionPath();
        if ((selectedPath != null) && (selectedPath.getPathCount() != 1)) {
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
            if (!selectedNode.isRoot()) {
                final String attributeName = (String) selectedNode.getUserObject();
                attr = getModel().getExpressionAttribute(attributeName);
            }
        }
        return attr;
    }

    public void saveCurrentSelection() {
        lastSelectionsPath = currentSelectionsPath;
        currentSelectionsPath = getSelectionPaths();
    }

    public void reloadSelection() {
        setSelectionPaths(currentSelectionsPath);
        try {
            getExpressionTreeListener().treeSelectionChange();
        } catch (final ArchivingException e) {
            final JPopupMenu popup = new JPopupMenu();
            popup.add(e.getLocalizedMessage());
            popup.setVisible(true);
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
        }
    }

    @Override
    public void setModel(final TreeModel model) {
        if (model instanceof ExpressionTreeModel) {
            super.setModel(model);
        }
    }

    @Override
    public ExpressionTreeModel getModel() {
        return (ExpressionTreeModel) super.getModel();
    }

    public ExpressionTreeListener getExpressionTreeListener() {
        return expressionTreeListener;
    }

}
