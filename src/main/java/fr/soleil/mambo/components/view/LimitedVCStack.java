// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/components/view/LimitedVCStack.java,v $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class LimitedVCStack.
// (Claisse Laurent) - nov. 2005
//
// $Author: ounsy $
//
// $Revision: 1.2 $
//
// $Log: LimitedVCStack.java,v $
// Revision 1.2 2006/05/16 09:36:27 ounsy
// minor changes
//
// Revision 1.1 2005/11/29 18:28:12 chinkumo
// no message
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.components.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.slf4j.Logger;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.bean.manager.ViewConfigurationBeanManager;
import fr.soleil.mambo.components.LimitedStack;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationData;

public class LimitedVCStack extends LimitedStack<ViewConfiguration> {

    private static final long serialVersionUID = -756984569528923834L;

    /**
     * @param maxSize
     */
    public LimitedVCStack(final int maxSize) {
        super(maxSize);
    }

    /**
     * 
     */
    public LimitedVCStack() {
        super();
    }

    @Override
    public boolean removeElement(final Object item) {
        boolean ret = super.removeElement(item);
        if ((!ret) && (item instanceof ViewConfiguration)) {
            final ViewConfiguration vc = (ViewConfiguration) item;
            final Iterator<ViewConfiguration> it = iterator();
            while (it.hasNext()) {
                final ViewConfiguration next = it.next();
                if (next.equals(vc)) {
                    ret = super.removeElement(next);
                    break;
                }
            }
        }
        return ret;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (ViewConfiguration vc : this) {
            builder.append(vc).append(GUIUtilities.CRLF);
        }
        return builder.toString();
    }

    /**
     * @return
     */
    public Object toString2() {
        StringBuilder builder = new StringBuilder();
        for (ViewConfiguration vc : this) {
            final ViewConfigurationData vcData = vc.getData();
            builder.append(vcData.getName()).append("|").append(vcData.getPath()).append(GUIUtilities.CRLF);
        }
        return builder.toString();
    }

    public boolean save(final Logger logger) {
        boolean oneMissed = false;
        // Create a copy to avoid ConcurrentModificationException on successful saving
        Collection<ViewConfiguration> toSave = new ArrayList<>(this);
        try {
            for (ViewConfiguration openedViewConfiguration : toSave) {
                if (!openedViewConfiguration.isModified()) {
                    continue;
                }
                try {
                    ViewConfigurationBeanManager.getInstance().saveVC(openedViewConfiguration, false);
                } catch (final Exception e) {
                    openedViewConfiguration.setModified(true);
                    openedViewConfiguration.setPath(null);
                    oneMissed = true;
                    logger.warn(ObjectUtils.EMPTY_STRING, e);
                }
            }
        } finally {
            toSave.clear();
        }

        return oneMissed;
    }

    public ViewConfiguration getSelectedVC() {
        return isEmpty() ? null : firstElement();
    }
}
