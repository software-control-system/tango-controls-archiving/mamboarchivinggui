package fr.soleil.mambo.components.view.images;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.mambo.actions.view.ViewImageTableAction;
import fr.soleil.mambo.tools.Messages;

public class ImagesListSelectionListener implements ListSelectionListener {
    private PartialImageDataTableModel model;
    private String viewReadMsg;

    public ImagesListSelectionListener(PartialImageDataTableModel model) {
        super();
        this.model = model;
        viewReadMsg = Messages.getMessage("VIEW_IMAGE_READ");
    }

    @Override
    public void valueChanged(ListSelectionEvent event) {
        if (!event.getValueIsAdjusting()) {
            ListSelectionModel lsm = (ListSelectionModel) event.getSource();
            if (!lsm.isSelectionEmpty()) {
                int selectedRow = lsm.getMinSelectionIndex();
                ImageData imageData = this.model.getRow(selectedRow);
                ViewImageTableAction viewImageAction = new ViewImageTableAction(viewReadMsg, imageData);
                viewImageAction.actionPerformed(null);
            }
        }
    }

}
