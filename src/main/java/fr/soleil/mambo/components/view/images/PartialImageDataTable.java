package fr.soleil.mambo.components.view.images;

import java.util.Collection;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import fr.soleil.archiving.hdbtdb.api.ImageData;

public class PartialImageDataTable extends JTable {

    private static final long serialVersionUID = -4257559858251664032L;

    private String imageName;

    public PartialImageDataTable() {
        super(new PartialImageDataTableModel());
        this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel rowSM = this.getSelectionModel();
        rowSM.addListSelectionListener(new ImagesListSelectionListener(((PartialImageDataTableModel) getModel())));
    }

    public void setData(Collection<ImageData> partialImageData) {
        ((PartialImageDataTableModel) getModel()).setData(partialImageData);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return (column == 3);
    }

    public void setImageName(String _imageName) {
        this.imageName = _imageName;
    }

    /**
     * @return Returns the imageName.
     */
    public String getImageName() {
        return imageName;
    }
}
