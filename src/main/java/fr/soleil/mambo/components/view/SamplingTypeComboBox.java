package fr.soleil.mambo.components.view;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;

public class SamplingTypeComboBox extends JComboBox<SamplingType> {

    private static final long serialVersionUID = 3594174441825640445L;

    public SamplingTypeComboBox() {
        super();
        setRenderer(new SamplingTypeComboBoxRenderer());
        initChoices();

        Dimension maxDimension = new Dimension(300, 25);
        setMaximumSize(new Dimension(Integer.MAX_VALUE, maxDimension.height));
        setMinimumSize(maxDimension);
        setPreferredSize(maxDimension);
        setSize(maxDimension);
    }

    private void initChoices() {
        SamplingType[] choices = SamplingType.getChoices();
        for (int i = 0; i < choices.length; i++) {
            SamplingType nextChoice = choices[i];
            super.addItem(nextChoice);
        }
    }

    public void setSelectedSamplingType(Object anObject) {
        SamplingType convert = (SamplingType) anObject;
        for (int i = 0; i < getItemCount(); i++) {
            SamplingType aSamplingType = getItemAt(i);
            if (aSamplingType.isSameTypeAs(convert)) {
                setSelectedIndex(i);
                break;
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public class SamplingTypeComboBoxRenderer extends DefaultListCellRenderer {

        private static final long serialVersionUID = -287954200008314862L;

        public SamplingTypeComboBoxRenderer() {
            super();
        }

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
                boolean cellHasFocus) {
            JLabel comp = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            if (value instanceof SamplingType) {
                comp.setText(((SamplingType) value).getLabel());
            }
            return comp;
        }
    }
}
