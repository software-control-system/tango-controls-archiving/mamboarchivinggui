package fr.soleil.mambo.components.archiving;

import fr.soleil.mambo.actions.archiving.listeners.OpenedACItemListener;
import fr.soleil.mambo.components.OpenedConfComboBox;
import fr.soleil.mambo.components.renderers.OpenedACListCellRenderer;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;

public class OpenedACComboBox extends OpenedConfComboBox<ArchivingConfiguration> {

    private static final long serialVersionUID = 4339464372602933991L;

    // MULTI-CONF
    private static OpenedACComboBox instance = null;

    /**
     * @return 8 juil. 2005
     */
    public static OpenedACComboBox getInstance(LimitedACStack stack) {
        if (instance == null) {
            instance = new OpenedACComboBox(stack);
        }

        return instance;
    }

    public static OpenedACComboBox getInstance() {
        return instance;
    }

    private OpenedACComboBox(LimitedACStack stack) {
        super(stack);
        addItemListener(new OpenedACItemListener());
        setRenderer(new OpenedACListCellRenderer());
    }

    @Override
    public void setMaxSize(int size) {
        stack.setMaxSize(size);
    }

    public void empty() {
        stack.clear();
        setElements(new LimitedACStack());
    }

    public static void refresh() {
        if (instance != null) {
            instance.repaint();
        }
    }

    public void setElements(LimitedACStack stack) {
        super.setElements(stack);
    }

    public LimitedACStack getACElements() {
        return (LimitedACStack) stack;
    }

    /**
     * @param currentDomainName
     */
    public void selectElement(ArchivingConfiguration vc) {
        setSelectedItem(vc);
    }

    /**
    * 
    */
    public static void removeSelectedElement() {
        if (instance != null) {
            ArchivingConfiguration selectedAC = ArchivingConfiguration.getSelectedArchivingConfiguration();
            instance.removeElement(selectedAC);
            instance.repaint();
        }
    }

    /**
     * @param selectedAC
     */
    public void removeElement(ArchivingConfiguration selectedAC) {
        if (stack.size() > 0) {
            stack.removeElement(selectedAC);
            removeItem(selectedAC);
            if (!stack.isEmpty()) {
                ArchivingConfiguration nextSelectedAC = stack.firstElement();
                selectElement(nextSelectedAC);
            } else {
                ArchivingConfiguration newAC = new ArchivingConfiguration();
                ArchivingConfiguration.setCurrentArchivingConfiguration(newAC);
                ArchivingConfiguration.setSelectedArchivingConfiguration(newAC);

                newAC.push();
                empty();
            }

            stack.removeElement(selectedAC);
            removeItem(selectedAC);
        }
    }

    public ArchivingConfiguration getSelectedAC() {
        return ((LimitedACStack) stack).getSelectedAC();
    }
}
