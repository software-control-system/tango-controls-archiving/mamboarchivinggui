package fr.soleil.mambo.components.archiving;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import fr.soleil.mambo.actions.archiving.listeners.ACAttributesPropertiesTreeSelectionListener;
import fr.soleil.mambo.components.AttributesTree;
import fr.soleil.mambo.components.renderers.ACTreeRenderer;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.models.ACTreeModel;
import fr.soleil.mambo.models.AttributesTreeModel;

public class ACAttributesPropertiesTree extends AttributesTree {

    private static final long serialVersionUID = 2356667117528325067L;

    /**
     * @param newModel
     */
    private ACAttributesPropertiesTree(ACTreeModel newModel) {
        super(newModel);
        addTreeSelectionListener(new ACAttributesPropertiesTreeSelectionListener());
        setCellRenderer(new ACTreeRenderer());

        setExpandsSelectedPaths(true);
        setScrollsOnExpand(true);
        setShowsRootHandles(true);
        setToggleClickCount(1);
    }

    private static ACAttributesPropertiesTree instance = null;

    /**
     * @param newModel
     * @return 8 juil. 2005
     */
    public static ACAttributesPropertiesTree getInstance(ACTreeModel newModel) {
        if (instance == null) {
            instance = new ACAttributesPropertiesTree(newModel);
        } else
            instance.setModel(newModel);

        return instance;
    }

    /**
     * @return 8 juil. 2005
     */
    public static ACAttributesPropertiesTree getInstance() {
        return instance;
    }

    public Collection<ArchivingConfigurationAttribute> getListOfAttributesToSet() {
        TreePath[] selectedPath = getSelectionPaths();
        Collection<ArchivingConfigurationAttribute> attributes;
        if (selectedPath == null || selectedPath.length == 0) {
            attributes = null;
        } else {
            attributes = new ArrayList<ArchivingConfigurationAttribute>();
            for (int i = 0; i < selectedPath.length; i++) {
                TreePath currentSelectedTreePath = selectedPath[i];
                DefaultMutableTreeNode currentSelectedNode = (DefaultMutableTreeNode) currentSelectedTreePath
                        .getLastPathComponent();

                Enumeration<?> enumeration = currentSelectedNode.preorderEnumeration();
                while (enumeration.hasMoreElements()) {
                    DefaultMutableTreeNode currentTraversedNode = (DefaultMutableTreeNode) enumeration.nextElement();

                    if (currentTraversedNode.getLevel() == AttributesTreeModel.CONTEXT_TREE_DEPTH - 1) {
                        TreeNode[] path = currentTraversedNode.getPath();
                        String completeName = AttributesTreeModel.translatePathIntoKey(path);
                        ArchivingConfigurationAttribute attr = new ArchivingConfigurationAttribute();
                        attr.setCompleteName(completeName);
                        attributes.add(attr);
                    }
                }
            }
        }
        return attributes;
    }
}
