package fr.soleil.mambo.components.archiving;

import fr.soleil.mambo.models.ACAttributeModeDTableModel;

public class ACAttributeModeDTable extends AACAttributesDetailsTable<ACAttributeModeDTableModel> {

    private static final long serialVersionUID = 2798920048905546390L;

    private static ACAttributeModeDTable hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeDTable(Boolean historic) {
        super(ACAttributeModeDTableModel.class, historic);
    }

    public static ACAttributeModeDTable getInstance(Boolean historic) {
        ACAttributeModeDTable instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeDTable.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeDTable.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeDTable.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

}
