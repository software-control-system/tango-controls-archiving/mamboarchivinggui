package fr.soleil.mambo.components.archiving;

import fr.soleil.mambo.models.ACAttributeModeATableModel;

public class ACAttributeModeATable extends AACAttributesDetailsTable<ACAttributeModeATableModel> {

    private static final long serialVersionUID = -3303347115344319366L;

    private static ACAttributeModeATable hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeATable(Boolean historic) {
        super(ACAttributeModeATableModel.class, historic);
    }

    public static ACAttributeModeATable getInstance(Boolean historic) {
        ACAttributeModeATable instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeATable.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeATable.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeATable.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

}
