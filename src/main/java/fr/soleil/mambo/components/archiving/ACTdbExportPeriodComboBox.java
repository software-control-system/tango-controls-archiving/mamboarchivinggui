package fr.soleil.mambo.components.archiving;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JComboBox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.options.manager.ACDefaultsManagerFactory;
import fr.soleil.mambo.options.manager.IACDefaultsManager;
import fr.soleil.mambo.tools.Messages;

public class ACTdbExportPeriodComboBox extends JComboBox<String> {

    private static final long serialVersionUID = 6778133909565000852L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ACTdbExportPeriodComboBox.class);

    // Its value come from the default configuration
    private long defaultExportValue;
    // Use only if the defaultExportValue is not valid
    private static final String DEFAULT_TDB_EXPORT_PERIOD = Messages
            .getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD_1H");

    private long[] exportPeriodsTable;
    private Map<Long, Integer> reverseExportPeriodsTable;

    public ACTdbExportPeriodComboBox() {
        super();

        IACDefaultsManager manager = ACDefaultsManagerFactory.getCurrentImpl();
        try {
            defaultExportValue = Long.parseLong(manager.getACDefaultsPropertyValue("TDB_EXPORT_PERIOD"));
        } catch (NumberFormatException e) {
            LOGGER.debug("NumberFormatException has been received during TDB_EXPORT_PERIOD reading");
            defaultExportValue = IACDefaultsManager.DEFAULT_TDB_EXPORT_PERIOD_WHEN_BAD_VALUE;
        }

        initComponent();
    }

    public long getExportPeriod() {
        int selected = getSelectedIndex();
        return exportPeriodsTable[selected];
    }

    /**
     * The export period can be modified only by the admin profile
     */
    public void setExportPeriod(long period) {
        Integer key = null;
        try {
            key = reverseExportPeriodsTable
                    .get(Long.valueOf((!Mambo.canModifyExportPeriod() ? defaultExportValue : period)));
        } catch (Exception e) {
            LOGGER.error(TangoExceptionHelper.getErrorMessage(e), e);
        }

        if (key == null) {
            setSelectedItem(DEFAULT_TDB_EXPORT_PERIOD);
        } else {
            setSelectedIndex(key.intValue());
        }
    }

    private void initComponent() {
        initExportPeriodsTable();

        String msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD_5MN");
        addItem(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD_10MN");
        addItem(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD_30MN");
        addItem(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD_1H");
        addItem(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD_2H");
        addItem(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD_4H");
        addItem(msg);
        msg = Messages.getMessage("DIALOGS_EDIT_AC_ATTRIBUTES_PROPERTIES_EXPORT_PERIOD_6H");
        addItem(msg);

        if (!Mambo.canModifyExportPeriod()) {
            setEnabled(false);
        }
        // Select item is done via the setMethod
    }

    private void initExportPeriodsTable() {
        exportPeriodsTable = new long[7];
        reverseExportPeriodsTable = new ConcurrentHashMap<Long, Integer>(7);

        long millisecondsInOneMinute = 60000;
        exportPeriodsTable[0] = 5 * millisecondsInOneMinute;
        exportPeriodsTable[1] = 10 * millisecondsInOneMinute;
        exportPeriodsTable[2] = 30 * millisecondsInOneMinute;
        exportPeriodsTable[3] = 1 * 60 * millisecondsInOneMinute;
        exportPeriodsTable[4] = 2 * 60 * millisecondsInOneMinute;
        exportPeriodsTable[5] = 4 * 60 * millisecondsInOneMinute;
        exportPeriodsTable[6] = 6 * 60 * millisecondsInOneMinute;

        reverseExportPeriodsTable.put(Long.valueOf(exportPeriodsTable[0]), Integer.valueOf(0));
        reverseExportPeriodsTable.put(Long.valueOf(exportPeriodsTable[1]), Integer.valueOf(1));
        reverseExportPeriodsTable.put(Long.valueOf(exportPeriodsTable[2]), Integer.valueOf(2));
        reverseExportPeriodsTable.put(Long.valueOf(exportPeriodsTable[3]), Integer.valueOf(3));
        reverseExportPeriodsTable.put(Long.valueOf(exportPeriodsTable[4]), Integer.valueOf(4));
        reverseExportPeriodsTable.put(Long.valueOf(exportPeriodsTable[5]), Integer.valueOf(5));
        reverseExportPeriodsTable.put(Long.valueOf(exportPeriodsTable[6]), Integer.valueOf(6));
    }

}
