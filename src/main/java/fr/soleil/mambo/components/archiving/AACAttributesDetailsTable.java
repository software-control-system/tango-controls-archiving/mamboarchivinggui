package fr.soleil.mambo.components.archiving;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JTable;

import fr.soleil.mambo.components.renderers.ACAttributeDetailTableRenderer;
import fr.soleil.mambo.models.AACAttributeDetailsTableModel;

public class AACAttributesDetailsTable<M extends AACAttributeDetailsTableModel> extends JTable {

    private static final long serialVersionUID = -7340257639927526253L;

    private static final String GET_INSTANCE = "getInstance";

    protected static <TM extends AACAttributeDetailsTableModel> TM getModelInstance(Class<TM> modelClass,
            Boolean historic) {
        TM model;
        if (modelClass == null) {
            model = null;
        } else {
            try {
                Method m = modelClass.getMethod(GET_INSTANCE, Boolean.class);
                @SuppressWarnings("unchecked")
                TM tmp = (TM) m.invoke(null, historic);
                model = tmp;
            } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException e) {
                model = null;
            }
        }
        return model;
    }

    protected static <T extends AACAttributesDetailsTable<?>> T checkInstance(T instance, Class<T> modelClass,
            Boolean historic) {
        T model = instance;
        if (model == null) {
            Constructor<T> constructor;
            try {
                constructor = modelClass.getDeclaredConstructor(Boolean.class);
                if (constructor == null) {
                    model = null;
                } else {
                    model = constructor.newInstance(historic);
                }
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException e) {
                model = null;
                e.printStackTrace();
            }
        }
        return model;
    }

    protected AACAttributesDetailsTable(Class<M> modelClass, Boolean historic) {
        super(getModelInstance(modelClass, historic));
        setAutoCreateColumnsFromModel(true);
        setDefaultRenderer(Object.class, new ACAttributeDetailTableRenderer());
        setEnabled(false);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    @Override
    public boolean isColumnSelected(int column) {
        return false;
    }

}
