package fr.soleil.mambo.components.archiving;

import fr.soleil.mambo.models.ACAttributeModesTitleTableModel;

public class ACAttributeModesTitleTable extends AACAttributesDetailsTable<ACAttributeModesTitleTableModel> {

    private static final long serialVersionUID = -115466064171258661L;

    private static ACAttributeModesTitleTable hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModesTitleTable(Boolean historic) {
        super(ACAttributeModesTitleTableModel.class, historic);
    }

    public static ACAttributeModesTitleTable getInstance(Boolean historic) {
        ACAttributeModesTitleTable instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModesTitleTable.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModesTitleTable.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModesTitleTable.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

}
