package fr.soleil.mambo.components.archiving;

import fr.soleil.mambo.models.ACAttributeModeCTableModel;

public class ACAttributeModeCTable extends AACAttributesDetailsTable<ACAttributeModeCTableModel> {

    private static final long serialVersionUID = -4569556293397073776L;

    private static ACAttributeModeCTable hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeCTable(Boolean historic) {
        super(ACAttributeModeCTableModel.class, historic);
    }

    public static ACAttributeModeCTable getInstance(Boolean historic) {
        ACAttributeModeCTable instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeCTable.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeCTable.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeCTable.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

}
