// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/components/archiving/LimitedACStack.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class LimitedACStack.
// (Claisse Laurent) - nov. 2005
//
// $Author: ounsy $
//
// $Revision: 1.2 $
//
// $Log: LimitedACStack.java,v $
// Revision 1.2 2006/05/16 09:36:11 ounsy
// minor changes
//
// Revision 1.1 2005/11/29 18:27:24 chinkumo
// no message
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.components.archiving;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.slf4j.Logger;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.components.LimitedStack;
import fr.soleil.mambo.data.archiving.ArchivingConfiguration;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationData;
import fr.soleil.mambo.datasources.file.IArchivingConfigurationManager;

public class LimitedACStack extends LimitedStack<ArchivingConfiguration> {

    private static final long serialVersionUID = 3644744721417613516L;

    /**
     * @param maxSize
     */
    public LimitedACStack(final int maxSize) {
        super(maxSize);
    }

    /**
     * 
     */
    public LimitedACStack() {
        super();
    }

    @Override
    public boolean removeElement(final Object item) {
        boolean ret = super.removeElement(item);
        if ((!ret) && (item instanceof ArchivingConfiguration)) {
            final ArchivingConfiguration ac = (ArchivingConfiguration) item;
            final Iterator<ArchivingConfiguration> it = iterator();
            while (it.hasNext()) {
                final ArchivingConfiguration next = it.next();
                if (next.equals(ac)) {
                    ret = super.removeElement(next);
                    break;
                }
            }
        }
        return ret;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (ArchivingConfiguration ac : this) {
            builder.append(ac).append(GUIUtilities.CRLF);
        }
        return builder.toString();
    }

    /**
     * @return
     */
    public Object toString2() {
        StringBuilder builder = new StringBuilder();
        for (ArchivingConfiguration ac : this) {
            final ArchivingConfigurationData vcData = ac.getData();
            builder.append(vcData.getName()).append("|").append(vcData.getPath()).append(GUIUtilities.CRLF);
        }
        return builder.toString();
    }

    /**
     * @param viewManager
     * @throws Exception
     */
    public boolean save(final IArchivingConfigurationManager viewManager, final Logger logger) {
        boolean oneMissed = false;
        // Create a copy to avoid ConcurrentModificationException on successful saving
        Collection<ArchivingConfiguration> toSave = new ArrayList<>(this);
        try {
            for (ArchivingConfiguration openedArchivingConfiguration : toSave) {
                if (!openedArchivingConfiguration.isModified()) {
                    continue;
                }
                try {
                    openedArchivingConfiguration.save(viewManager, false);
                } catch (final Exception e) {
                    openedArchivingConfiguration.setModified(true);
                    openedArchivingConfiguration.setPath(null);
                    oneMissed = true;
                    logger.warn(ObjectUtils.EMPTY_STRING, e);
                }
            }
        } finally {
            toSave.clear();
        }
        return oneMissed;
    }

    public ArchivingConfiguration getSelectedAC() {
        return isEmpty() ? null : firstElement();
    }
}
