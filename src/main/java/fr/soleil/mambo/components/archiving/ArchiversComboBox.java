package fr.soleil.mambo.components.archiving;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.archiving.IArchiver;
import fr.soleil.mambo.containers.archiving.dialogs.AttributesPropertiesPanel;
import fr.soleil.mambo.datasources.tango.standard.ITangoManager;
import fr.soleil.mambo.datasources.tango.standard.TangoManagerFactory;

public class ArchiversComboBox extends JComboBox<String> {

    private static final long serialVersionUID = -3515648904842724162L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchiversComboBox.class);
    public static final String NO_SELECTION = "---";
    public static final String DEFAULT = "DEFAULT";

    private final Map<String, IArchiver> elements;
    private final Boolean historic;
    private String currentlySelectedAttribute;

    /**
     * @param type
     * @throws IllegalStateException
     * @throws ArchivingException
     */
    public ArchiversComboBox(final Boolean historic) throws ArchivingException {
        super();
        this.historic = historic;
        elements = new ConcurrentHashMap<>();
        reset();

        setArchivers();

        Dimension maxDimension = new Dimension(Integer.MAX_VALUE, 20);
        setMaximumSize(maxDimension);

        addActionListener(new ArchiversComboBoxActionListener());
        setRenderer(new ArchiversComboBoxRenderer());
    }

    /**
     * @throws ArchivingException
     * 
     */
    public void setArchivers() throws ArchivingException {
        reset();
        final ITangoManager manager = TangoManagerFactory.getCurrentImpl();
        IArchiver[] archivers = manager.findArchivers(this.historic);
        if (archivers != null) {
            for (IArchiver archiver : archivers) {
                archiver.load();
                if (!archiver.isDedicated()) {
                    addItem(archiver.getName());
                    elements.put(archiver.getName(), archiver);
                }
            }
        }
    }

    public void reset() {
        removeAllItems();
        addItem(NO_SELECTION);

        elements.clear();
    }

    public IArchiver getSelectedArchiver() {
        final IArchiver ret;
        final String currentAttributeName = (String) getSelectedItem();
        if (currentAttributeName == null || currentAttributeName.equals(ArchiversComboBox.NO_SELECTION)) {
            ret = null;
        } else {
            ret = this.elements.get(currentAttributeName);
        }
        return ret;
    }

    public void selectDefault() {
        super.setSelectedItem(NO_SELECTION);
    }

    public void selectArchiver(String selected) {
        selected = selected == null ? ObjectUtils.EMPTY_STRING : selected;
        if (elements.containsKey(selected)) {
            setSelectedItem(selected);
        } else {
            selectDefault();
        }
    }

    public void setCurrentlySelectedAttribute(final String attributeCompleteName) {
        this.currentlySelectedAttribute = attributeCompleteName;
    }

    /**
     * @return Returns the currentlySelectedAttribute.
     */
    public String getCurrentlySelectedAttribute() {
        return currentlySelectedAttribute;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private class ArchiversComboBoxActionListener implements ActionListener {

        public ArchiversComboBoxActionListener() {
            super();
        }

        @Override
        public void actionPerformed(final ActionEvent e) {
            final IArchiver selectedArchiver = ArchiversComboBox.this.getSelectedArchiver();
            final String selectedArchiverText = selectedArchiver == null ? DEFAULT : selectedArchiver.getName();

            final AttributesPropertiesPanel attributesPropertiesPanel = AttributesPropertiesPanel.getInstance();
            if (attributesPropertiesPanel != null) {
                attributesPropertiesPanel.setArchiverChoiceText(selectedArchiverText);
            }
        }
    }

    private class ArchiversComboBoxRenderer extends DefaultListCellRenderer /*extends BasicComboBoxRenderer*/ {

        private static final long serialVersionUID = 1566850506180023521L;

        public ArchiversComboBoxRenderer() {
            super();
        }

        @Override
        public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index,
                final boolean isSelected, final boolean cellHasFocus) {
            final Component ret = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            final String archiverName = (String) value;
            final IArchiver archiver = ArchiversComboBox.this.elements.get(archiverName);
            if (archiver != null) {
                try {
                    final Color color = archiver.getAssociationColor(ArchiversComboBox.this.currentlySelectedAttribute);
                    ret.setForeground(color);
                } catch (final ArchivingException e) {
                    LOGGER.error("error", e);
                }
            }
            return ret;
        }
    }

}
