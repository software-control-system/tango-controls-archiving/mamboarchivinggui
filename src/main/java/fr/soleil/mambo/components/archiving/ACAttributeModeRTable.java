package fr.soleil.mambo.components.archiving;

import fr.soleil.mambo.models.ACAttributeModeRTableModel;

public class ACAttributeModeRTable extends AACAttributesDetailsTable<ACAttributeModeRTableModel> {

    private static final long serialVersionUID = 8337255524735649952L;

    private static ACAttributeModeRTable hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeRTable(Boolean historic) {
        super(ACAttributeModeRTableModel.class, historic);
    }

    public static ACAttributeModeRTable getInstance(Boolean historic) {
        ACAttributeModeRTable instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeRTable.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeRTable.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeRTable.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

}
