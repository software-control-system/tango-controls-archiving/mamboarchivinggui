package fr.soleil.mambo.components.archiving;

import fr.soleil.mambo.models.ACAttributeModePTableModel;

public class ACAttributeModePTable extends AACAttributesDetailsTable<ACAttributeModePTableModel> {

    private static final long serialVersionUID = 2414132790200244897L;

    private static ACAttributeModePTable hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModePTable(Boolean historic) {
        super(ACAttributeModePTableModel.class, historic);
    }

    public static ACAttributeModePTable getInstance(Boolean historic) {
        ACAttributeModePTable instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModePTable.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModePTable.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModePTable.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

}
