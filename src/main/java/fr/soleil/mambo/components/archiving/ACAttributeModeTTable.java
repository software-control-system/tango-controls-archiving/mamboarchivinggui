package fr.soleil.mambo.components.archiving;

import fr.soleil.mambo.models.ACAttributeModeTTableModel;

public class ACAttributeModeTTable extends AACAttributesDetailsTable<ACAttributeModeTTableModel> {

    private static final long serialVersionUID = 729011892309733110L;

    private static ACAttributeModeTTable hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeTTable(Boolean historic) {
        super(ACAttributeModeTTableModel.class, historic);
    }

    public static ACAttributeModeTTable getInstance(Boolean historic) {
        ACAttributeModeTTable instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeTTable.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeTTable.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeTTable.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

}
