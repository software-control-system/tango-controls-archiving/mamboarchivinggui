package fr.soleil.mambo.components.archiving;

import fr.soleil.mambo.models.ACAttributeModeETableModel;

public class ACAttributeModeETable extends AACAttributesDetailsTable<ACAttributeModeETableModel> {

    private static final long serialVersionUID = -4418719405785266860L;

    private static ACAttributeModeETable hdbInstance, tdbInstance, ttsInstance;

    protected ACAttributeModeETable(Boolean historic) {
        super(ACAttributeModeETableModel.class, historic);
    }

    public static ACAttributeModeETable getInstance(Boolean historic) {
        ACAttributeModeETable instance;
        if (historic == null) {
            ttsInstance = checkInstance(ttsInstance, ACAttributeModeETable.class, historic);
            instance = ttsInstance;
        } else if (historic.booleanValue()) {
            hdbInstance = checkInstance(hdbInstance, ACAttributeModeETable.class, historic);
            instance = hdbInstance;
        } else {
            tdbInstance = checkInstance(tdbInstance, ACAttributeModeETable.class, historic);
            instance = tdbInstance;
        }
        return instance;
    }

}
