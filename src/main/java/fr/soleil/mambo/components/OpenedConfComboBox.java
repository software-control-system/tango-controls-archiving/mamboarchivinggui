// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/mambo/components/OpenedConfComboBox.java,v $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class OpenedConfComboBox.
// (Claisse Laurent) - oct. 2005
//
// $Author: ounsy $
//
// $Revision: 1.2 $
//
// $Log: OpenedConfComboBox.java,v $
// Revision 1.2 2006/04/05 13:42:53 ounsy
// function centralization
//
// Revision 1.1 2005/11/29 18:27:24 chinkumo
// no message
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.mambo.components;

import java.awt.Dimension;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import fr.soleil.lib.project.ObjectUtils;

public class OpenedConfComboBox<H> extends JComboBox<H> {

    private static final long serialVersionUID = -3175169383794857156L;

    // MULTI-CONF
    protected LimitedStack<H> stack;

    protected OpenedConfComboBox(LimitedStack<H> stack) {
        super();
        setElements(stack);
        Dimension maxDimension = new Dimension(300, 25);
        setMaximumSize(new Dimension(Integer.MAX_VALUE, maxDimension.height));
        setMinimumSize(maxDimension);
        setPreferredSize(maxDimension);
        setSize(maxDimension);
    }

    public LimitedStack<H> getElements() {
        return stack;
    }

    protected void setElements(LimitedStack<H> stack) {
        this.stack = stack;
        setModel(new DefaultComboBoxModel<>(stack));
    }

    public void setMaxSize(int _size) {
        stack.setMaxSize(_size);
    }

    public int getMaxSize() {
        return stack.getMaxSize();
    }

    public int getStackSize() {
        return stack.size();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setSelectedItem(Object anObject) {
        stack.push((H) anObject);
        boolean canSelect;
        Object previous = getSelectedItem();
        if (ObjectUtils.sameObject(anObject, previous)) {
            if (anObject != previous) {
                canSelect = true;
                super.setSelectedItem(null);
            } else {
                canSelect = false;
            }
        } else {
            canSelect = true;
        }
        if (canSelect) {
            super.setSelectedItem(anObject);
        }
    }

    public void push(H anObject) {
        stack.push(anObject);
    }

}
