package fr.soleil.mambo;

import java.awt.Color;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.AccountDelegate;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.swing.Splash;
import fr.soleil.mambo.actions.view.LoadVCAction;
import fr.soleil.mambo.api.db.DbConnectionParameters;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.datasources.db.properties.PropertyManagerFactory;
import fr.soleil.mambo.datasources.file.IViewConfigurationManager;
import fr.soleil.mambo.datasources.file.ViewConfigurationManagerFactory;
import fr.soleil.mambo.lifecycle.DefaultLifeCycleManager;
import fr.soleil.mambo.lifecycle.LifeCycleManager;
import fr.soleil.mambo.lifecycle.LifeCycleManagerFactory;
import fr.soleil.mambo.tools.Messages;

public class Mambo {

    public static final String DEFAULT_LOG_ID = Mambo.class.getName();
    private static AccountManager accountManager;
    private static final ImageIcon APPLI_START = new ImageIcon(Mambo.class.getResource("icons/splash-11986-2.png"));
    private static final Logger LOGGER = LoggerFactory.getLogger(DEFAULT_LOG_ID);
    private static final String USER_PROFILE_ADMIN = "Admin";
    private static final String USER_PROFILE_EXPERT = "Expert";
    private static final String USER_PROFILE_VIEWER = "Viewer";

    private static boolean initTangoAccess = true;

    public static final int USER_RIGHTS_ADMIN = 3;
    public static final int USER_RIGHTS_ALL = 2;
    public static final int USER_RIGHTS_VC_ONLY = 1;

    public static final boolean IS_FAST_HDB = System.getProperty("fr.soleil.hdb.isfast", "false")
            .equalsIgnoreCase("true") ? true : false;

    public static LifeCycleManager lifeCycleManager;

    private static String pathToResources;
    private static Splash splash;

    private static int userRights = USER_RIGHTS_VC_ONLY;

    // //////////////
    // Rights methods
    // //////////////

    public static boolean canChooseArchivers() {
        return userRights == USER_RIGHTS_ADMIN;
    }

    public static boolean canModifyExportPeriod() {
        return userRights == USER_RIGHTS_ADMIN;
    }

    public static void setUserRights(final int rights) {
        userRights = rights;
    }

    public static boolean hasACs() {
        return userRights != USER_RIGHTS_VC_ONLY;
    }

    public static boolean isInitTangoAccess() {
        return initTangoAccess;
    }

    public static void setInitTangoAccess(final boolean initTangoAccess) {
        Mambo.initTangoAccess = initTangoAccess;
    }

    /**
     * 8 juil. 2005
     *
     * @throws ArchivingException
     */
    private static void createAndShowGUI(final String vc) throws ArchivingException {
        // String title = "Mambo v" + Messages.getMessage("ABOUT_RELEASE") +
        // " (" + Messages.getMessage("ABOUT_RELEASE_DATE") + ")";
        LOGGER.info("starting mambo");
        final long startTime = System.currentTimeMillis();
        final String title = Messages.getAppMessage("project.name") + " v" + Messages.getAppMessage("project.version")
                + " (" + Messages.getAppMessage("build.date") + ")";

        splash = new Splash(APPLI_START, Color.BLACK);
        splash.setTitle(title);
        splash.setCopyright("Synchrotron SOLEIL");
        splash.setMaxProgress(28);
        splash.progress(1);
        LOGGER.info(Messages.getLogMessage("APPLICATION_WILL_START_BUILD_ENVIRONMENT"));
        splash.setMessage(Messages.getLogMessage("APPLICATION_WILL_START_BUILD_ENVIRONMENT"));

        final LifeCycleManager lifeCycleManager;
        if (LifeCycleManagerFactory.getCurrentImpl() == null) {
            lifeCycleManager = new DefaultLifeCycleManager();
            LifeCycleManagerFactory.setCurrentImpl(lifeCycleManager);
        } else {
            lifeCycleManager = LifeCycleManagerFactory.getCurrentImpl();
        }

        splash.progress(2);
        String msg = Messages.getLogMessage("APPLICATION_WILL_START_SET_ENVIRONMENT");
        LOGGER.info(msg);
        splash.setMessage(msg);
        lifeCycleManager.applicationWillStart(splash);
        // Create and set up the window.
        splash.progress(26);
        msg = Messages.getLogMessage("APPLICATION_WILL_START_BUILD_FRAME");
        LOGGER.info(msg);
        splash.setMessage(msg);
        final JFrame frame = MamboFrame.getInstance(lifeCycleManager);

        splash.progress(27);
        msg = Messages.getLogMessage("APPLICATION_WILL_START_SET_FRAME");
        LOGGER.info(msg);
        splash.setMessage(msg);
        frame.pack();

        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Notifying GUI for degrad mode
        if (PropertyManagerFactory.getCurrentImpl().isHdbDegrad()) {
            if (PropertyManagerFactory.getCurrentImpl().isTdbDegrad()) {
                notifyTitleBarWithHdbAndTdbDegrad(frame);
            } else {
                notifyTitleBarWithHdbDegrad(frame);
            }
        } else if (PropertyManagerFactory.getCurrentImpl().isTdbDegrad()) {
            notifyTitleBarWithTdbDegrad(frame);
        }
        Runtime.getRuntime().addShutdownHook(new Thread("Clean mambo resources once closed") {
            @Override
            public void run() {
                LifeCycleManagerFactory.getCurrentImpl().applicationWillClose();
            }
        });

        if (vc != null) {
            final IViewConfigurationManager manager = ViewConfigurationManagerFactory.getCurrentImpl();
            manager.setNonDefaultSaveLocation(vc);
            LoadVCAction.loadVCOnceThePathIsSet(false);
        }

        // Display the window.
        splash.progress(28);
        msg = Messages.getLogMessage("APPLICATION_WILL_START_PREPARE_DISPLAY");
        LOGGER.info(msg);
        splash.setMessage(msg);

        splash.setVisible(false);
        splash.dispose();

        frame.setVisible(true);
        final long endTime = System.currentTimeMillis();
        LOGGER.info(DateUtil.elapsedTimeToStringBuilder(new StringBuilder("Mambo started in "), endTime - startTime)
                .toString());
    }

    public static AccountManager getAccountManager() {
        return accountManager;
    }

    /**
     * @return Returns the pathToResources.
     */
    public static String getPathToResources() {
        return pathToResources;
    }

    public static void treatError(final Exception e, final Splash splash) {
        treatError(e, null, splash);
    }

    public static void treatError(final Exception e, String message, final Splash splash) {
        final String msg = "Mambo encountered an undesired error and will close:\n" + e.getMessage();
        if (!GraphicsEnvironment.isHeadless()) {
            JOptionPane.showMessageDialog(splash, msg, "Mambo Error!", JOptionPane.ERROR_MESSAGE);
        }
        if (message == null) {
            message = msg;
        }
        LOGGER.error(message, e);
        System.exit(1);
    }

    protected static boolean gettingStarted() {
        boolean canceled;
        final AccountDelegate delegate = new AccountDelegate();
        canceled = delegate.launchAccountSelector(Mambo.class.getSimpleName(), accountManager, splash, LOGGER);
        if (!canceled) {
            pathToResources = delegate.getSelectedAccountPath();
        }
        return canceled;
    }

    public static void main(final String[] args) {
        try {
            if (args == null || args.length > 7 || args.length < 4) {
                throw new ArchivingException(
                        "Incorrect arguments. Correct syntax: HDBuser HDBpassword TDBuser TDBpassword [userRights/vc]");
            }

            pathToResources = System.getProperty(GUIUtilities.WORKING_DIR);

            final File f = new File(pathToResources);
            if (!f.exists()) {
                f.mkdirs();
            }
            if (!f.isDirectory()) {
                LOGGER.error("{} is not a directory", f);
                throw new ArchivingException("Not a directory");
            }
            if (!f.canRead()) {
                LOGGER.error("{} cannot be read", f);
                throw new ArchivingException("Invalid path");
            }

            if (!f.canWrite()) {
                LOGGER.error("{} cannot be write", f);
                throw new ArchivingException("The path is read only");
            }

            LOGGER.debug("using resource : {}", f);
            final String vc;

            if ((args.length == 5) || (args.length == 7)) {
                final String argValue = args[args.length - 1];
                if (argValue.compareToIgnoreCase(USER_PROFILE_VIEWER) == 0) {
                    userRights = USER_RIGHTS_VC_ONLY;
                    vc = null;
                } else if (argValue.compareToIgnoreCase(USER_PROFILE_EXPERT) == 0) {
                    userRights = USER_RIGHTS_ALL;
                    vc = null;
                } else if (argValue.compareToIgnoreCase(USER_PROFILE_ADMIN) == 0) {
                    userRights = USER_RIGHTS_ADMIN;
                    vc = null;
                } else {
                    final File vcFile = new File(argValue);
                    if (vcFile.exists()) {
                        userRights = USER_RIGHTS_VC_ONLY;
                        vc = argValue;
                    } else {
                        vc = null;
                        LOGGER.error("unknown user rigths {}", argValue);
                        throw new ArchivingException("The userRights parameter has to be either " + USER_PROFILE_VIEWER
                                + " or " + USER_PROFILE_EXPERT);
                    }
                }
            } else {
                vc = null;
            }

            if (vc == null) {
                setAccountManager(new AccountManager(pathToResources, "Mambo", null, false, true));
            }
            Messages.initResourceBundle(Locale.US);

            if (isInitTangoAccess()) {
                // HDB
                DbConnectionParameters.performAllDBInit(Boolean.TRUE, args[0], args[1]);
                // TDB
                DbConnectionParameters.performAllDBInit(Boolean.FALSE, args[2], args[3]);
                // TTS
                if (args.length > 5) {
                    DbConnectionParameters.performAllDBInit(null, args[4], args[5]);
                }
            }

            if (vc == null && gettingStarted()) {
                System.exit(0);
            } else {
                SwingUtilities.invokeLater(() -> {
                    try {
                        createAndShowGUI(vc);
                    } catch (final ArchivingException e) {
                        treatError(e, splash);
                    }
                });
            }
        } catch (final Exception e) {
            treatError(e, splash);
        }
    }

    private static void notifyTitleBarWithHdbAndTdbDegrad(final JFrame frame) {
        final StringBuilder titleBuffer = new StringBuilder(frame.getTitle());
        titleBuffer.append(' ');
        titleBuffer.append(Messages.getMessage("DEGRADED_NOTIFICATION_ALL"));
        frame.setTitle(titleBuffer.toString());
        frame.setIconImage(new ImageIcon(Mambo.class.getResource("icons/degrad-all.png")).getImage());
    }

    private static void notifyTitleBarWithHdbDegrad(final JFrame frame) {
        final StringBuilder titleBuffer = new StringBuilder(frame.getTitle());
        titleBuffer.append(' ');
        titleBuffer.append(Messages.getMessage("DEGRADED_NOTIFICATION_HDB"));
        frame.setTitle(titleBuffer.toString());
        frame.setIconImage(new ImageIcon(Mambo.class.getResource("icons/degrad-hdb.png")).getImage());
    }

    private static void notifyTitleBarWithTdbDegrad(final JFrame frame) {
        final StringBuilder titleBuffer = new StringBuilder(frame.getTitle());
        titleBuffer.append(' ');
        titleBuffer.append(Messages.getMessage("DEGRADED_NOTIFICATION_TDB"));
        frame.setTitle(titleBuffer.toString());
        frame.setIconImage(new ImageIcon(Mambo.class.getResource("icons/degrad-tdb.png")).getImage());
    }

    public static void setAccountManager(final AccountManager accountManager) {
        Mambo.accountManager = accountManager;
    }

    public static void setHDBuser(final String user) {
        DbConnectionParameters.setDBUser(Boolean.TRUE, user);
    }

    public static void setHDBpassword(final String password) {
        DbConnectionParameters.setDBPassword(Boolean.TRUE, password);
    }

    public static void setTDBuser(final String user) {
        DbConnectionParameters.setDBUser(Boolean.FALSE, user);
    }

    public static void setTDBpassword(final String password) {
        DbConnectionParameters.setDBPassword(Boolean.FALSE, password);
    }

    public static void setTTSuser(final String user) {
        DbConnectionParameters.setDBUser(null, user);
    }

    public static void setTTSpassword(final String password) {
        DbConnectionParameters.setDBPassword(null, password);
    }

}
