package fr.soleil.mambo.api.extract;

import java.util.Collection;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.AttributeExtractor;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.DataExtractor;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.lib.project.math.MathConst;

/**
 * {@link IAttributeExtractor} dedicated to HDB/TDB.
 * 
 * @author GIRARDOT
 */
public class LegacyAttributeExtractor implements IAttributeExtractor {

    protected final AttributeExtractor extractor;
    protected final boolean historic;

    public LegacyAttributeExtractor(AttributeExtractor extractor, boolean historic) {
        this.extractor = extractor;
        this.historic = historic;
    }

    @Override
    public boolean isCanceled() {
        return extractor == null ? false : extractor.isCanceled();
    }

    @Override
    public void setCanceled(boolean canceled) throws ArchivingException {
        if (extractor != null) {
            extractor.setCanceled(canceled);
            setExtractorCanceled(extractor.getDataGetters(), canceled);
            setExtractorCanceled(extractor.getDataGettersBetweenDates(), canceled);
            setExtractorCanceled(extractor.getInfSupGetters(), canceled);
            setExtractorCanceled(extractor.getInfSupGettersBetweenDates(), canceled);
            setExtractorCanceled(extractor.getMinMaxAvgGetters(), canceled);
            setExtractorCanceled(extractor.getMinMaxAvgGettersBetweenDates(), canceled);
        }
    }

    @Override
    public double getAttDataMinBetweenDates(String... param) throws ArchivingException {
        return extractor == null ? MathConst.NAN_FOR_NULL
                : extractor.getMinMaxAvgGettersBetweenDates().getAttDataMinBetweenDates(param);
    }

    private void setExtractorCanceled(DataExtractor extractor, boolean canceled) throws ArchivingException {
        if (extractor != null) {
            extractor.setCanceled(canceled);
        }
    }

    @Override
    public double getAttDataMaxBetweenDates(String... param) throws ArchivingException {
        return extractor == null ? MathConst.NAN_FOR_NULL
                : extractor.getMinMaxAvgGettersBetweenDates().getAttDataMaxBetweenDates(param);
    }

    @Override
    public DbData[] getAttDataBetweenDates(SamplingType samplingType, String... param) throws ArchivingException {
        return extractor == null ? null
                : extractor.getDataGettersBetweenDates().getAttDataBetweenDates(samplingType, param);
    }

    @Override
    public Collection<ImageData> getAttImageDataBetweenDates(SamplingType samplingType, String... param)
            throws ArchivingException {
        return extractor == null ? null : extractor.getAttPartialImageDataBetweenDates(param, samplingType, historic);
    }

}
