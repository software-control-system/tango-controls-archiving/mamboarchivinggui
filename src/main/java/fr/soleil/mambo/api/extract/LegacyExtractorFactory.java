package fr.soleil.mambo.api.extract;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.AttributeExtractor;

public class LegacyExtractorFactory {

    private static final Map<AttributeExtractor, LegacyAttributeExtractor> KNOWN_EXTRACTORS = new ConcurrentHashMap<>();

    private LegacyExtractorFactory() {
        // hide constructor
    }

    public static LegacyAttributeExtractor getLegacyAttributeExtractor(AttributeExtractor extractor, boolean historic) {
        LegacyAttributeExtractor result;
        if (extractor == null) {
            result = null;
        } else {
            result = KNOWN_EXTRACTORS.get(extractor);
            if (result == null) {
                result = new LegacyAttributeExtractor(extractor, historic);
                LegacyAttributeExtractor tmp = KNOWN_EXTRACTORS.putIfAbsent(extractor, result);
                if (tmp != null) {
                    result = tmp;
                }
            }
        }
        return result;
    }

    public void forgetAttributeExtractor(AttributeExtractor extractor) {
        if (extractor != null) {
            KNOWN_EXTRACTORS.remove(extractor);
        }
    }

    public void forgetLegacyAttributeExtractor(LegacyAttributeExtractor extractor) {
        if (extractor != null) {
            forgetAttributeExtractor(extractor.extractor);
        }
    }

}
