package fr.soleil.mambo.api.extract;

import java.sql.Timestamp;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.lib.project.BasicCancelable;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.build.TangoArchivingServicesBuilder;
import fr.soleil.tango.archiving.event.select.AttributeValueSeries;
import fr.soleil.tango.archiving.services.TangoArchivingConfigService;
import fr.soleil.tango.archiving.services.TangoArchivingFetcherService;

public class TTSAttributeExtractor implements IAttributeExtractor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TTSAttributeExtractor.class);
    private static final String UNEXPECTED_ERROR_AT_TTS_ATTRIBUTE_EXTRACTOR_INITIALIZATION = "Unexpected error at TTSAttributeExtractor initialization";

    protected TangoArchivingFetcherService fetcherService;
    protected TangoArchivingConfigService configService;
    protected ICancelable cancelable;

    public TTSAttributeExtractor(TangoArchivingFetcherService fetcherService,
            TangoArchivingConfigService configService) {
        super();
        this.fetcherService = fetcherService;
        this.configService = configService;
        this.cancelable = new BasicCancelable();
    }

    public TTSAttributeExtractor(DatabaseConnectionConfig config) {
        super();
        TangoArchivingServicesBuilder builder;
        try {
            builder = new TangoArchivingServicesBuilder();
        } catch (Exception e) {
            LOGGER.error(UNEXPECTED_ERROR_AT_TTS_ATTRIBUTE_EXTRACTOR_INITIALIZATION, e);
            builder = null;
        }
        if (builder == null) {
            fetcherService = null;
            configService = null;
        } else {
            try {
                fetcherService = builder.buildFetcher(config);
            } catch (Exception e) {
                LOGGER.error(UNEXPECTED_ERROR_AT_TTS_ATTRIBUTE_EXTRACTOR_INITIALIZATION, e);
                fetcherService = null;
            }
            try {
                configService = builder.buildConfigFetcher(config);
            } catch (Exception e) {
                LOGGER.error(UNEXPECTED_ERROR_AT_TTS_ATTRIBUTE_EXTRACTOR_INITIALIZATION, e);
                configService = null;
            }
        }
        cancelable = new BasicCancelable();
    }

    public TangoArchivingConfigService getConfigService() {
        return configService;
    }

    public TangoArchivingFetcherService getFetcherService() {
        return fetcherService;
    }

    @Override
    public boolean isCanceled() {
        return cancelable.isCanceled();
    }

    @Override
    public void setCanceled(boolean canceled) {
        cancelable.setCanceled(canceled);
    }

    @Override
    public double getAttDataMinBetweenDates(String... param) throws ArchivingException {
        double min;
        if (isCanceled() || (fetcherService == null) || (configService == null)) {
            min = MathConst.NAN_FOR_NULL;
        } else if (param.length == 3) {
            String attributeName = param[0].toLowerCase();
            final Timestamp from = ExtractionUtils.getTimestamp(param[1]);
            final Timestamp to = ExtractionUtils.getTimestamp(param[2]);
            min = ExtractionUtils.getDouble(fetcherService.getAggregateValue(attributeName, "min", from, to),
                    cancelable);
        } else {
            throw new ArchivingException("Wrong number of parameters");
        }
        return min;
    }

    @Override
    public double getAttDataMaxBetweenDates(String... param) throws ArchivingException {
        double max;
        if (isCanceled() || (fetcherService == null) || (configService == null)) {
            max = MathConst.NAN_FOR_NULL;
        } else if (param.length == 3) {
            String attributeName = param[0].toLowerCase();
            final Timestamp from = ExtractionUtils.getTimestamp(param[1]);
            final Timestamp to = ExtractionUtils.getTimestamp(param[2]);
            max = ExtractionUtils.getDouble(fetcherService.getAggregateValue(attributeName, "max", from, to),
                    cancelable);
        } else {
            throw new ArchivingException("Wrong number of parameters");
        }
        return max;
    }

    protected AttributeValueSeries getDataBetweenDates(SamplingType samplingType, String... param)
            throws ArchivingException {
        final AttributeValueSeries avs;
        final String attributeName = param[0].toLowerCase();
        final Timestamp from = ExtractionUtils.getTimestamp(param[1]);
        final Timestamp to = ExtractionUtils.getTimestamp(param[2]);
        if (isCanceled() || (fetcherService == null) || (configService == null)) {
            avs = null;
        } else if ((samplingType == null) || samplingType.getType() == SamplingType.ALL) {
            avs = fetcherService.getBetween(attributeName, from, to, false);
        } else {
            String period = ExtractionUtils.getPeriod(samplingType);
            String function = ExtractionUtils.getAggregateFunction(samplingType);
//            LOGGER.info(attributeName + " sampling:\nperiod: " + period + "\nfunction: " + function);
            avs = fetcherService.getWithSampling(attributeName, period, function, from, to);
        }
        return avs;
    }

    @Override
    public DbData[] getAttDataBetweenDates(SamplingType samplingType, String... param) throws ArchivingException {
        final DbData[] extraction;
        final AttributeValueSeries avs = getDataBetweenDates(samplingType, param);
        if (isCanceled() || (avs == null)) {
            extraction = new DbData[2];
        } else {
            extraction = ExtractionUtils.toDbDataArray(avs, cancelable);
        }
        return extraction;
    }

    @Override
    public Collection<ImageData> getAttImageDataBetweenDates(SamplingType samplingType, String... param)
            throws ArchivingException {
        // TODO TTS readapt once API is compatible with something like ImageData
        final AttributeValueSeries avs = getDataBetweenDates(samplingType, param);
        return ExtractionUtils.toImageData(avs, cancelable);
    }

}
