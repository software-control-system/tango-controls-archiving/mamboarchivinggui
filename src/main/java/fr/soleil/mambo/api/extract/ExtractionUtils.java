package fr.soleil.mambo.api.extract;

import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.lib.project.math.MathConst;
import fr.soleil.lib.project.math.NumberArrayUtils;
import fr.soleil.lib.project.math.NumberMatrixUtils;
import fr.soleil.lib.project.math.UnsignedConverter;
import fr.soleil.tango.archiving.config.AttributeConfig;
import fr.soleil.tango.archiving.event.select.AttributeValue;
import fr.soleil.tango.archiving.event.select.AttributeValueSeries;
import fr.soleil.tango.archiving.services.TangoArchivingConfigService;

/**
 * Utility class dedicated to extraction.
 * 
 * @author GIRARDOT
 */
public class ExtractionUtils {

    private static final String DOT = ".", ESCAPED_DOT = "\\.", JOKER = ".*";
    private static final String MIN = "min";
    private static final String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";
    private static final DateTimeFormatter FORMATTER = DateUtil.getDateTimeFormatter(DATE_FORMAT);
    public static final String TTS_JOKER = "%";
    private static final String ESCAPED_TTS_JOKER = "%";

    private ExtractionUtils() {
        // hide constructor
    }

    /**
     * Returns whether current job should be considered as cancelled according to given {@link ICancelable}.
     * 
     * @param cancelable The {@link ICancelable}.
     * @return A <code>boolean</code>.
     */
    protected static boolean isCancelled(ICancelable cancelable) {
        return cancelable != null && cancelable.isCanceled();
    }

    /**
     * Extracts the period from a {@link SamplingType}.
     * 
     * @param samplingType The {@link SamplingType}.
     * @return A {@link String}.
     */
    public static String getPeriod(SamplingType samplingType) {
        String period;
        if (samplingType == null) {
            period = null;
        } else {
            StringBuilder builder = new StringBuilder();
            short factor = samplingType.hasAdditionalFiltering() ? samplingType.getAdditionalFilteringFactor() : 1;
            builder.append(factor);
            builder.append(' ').append(samplingType.getLabel().toLowerCase());
            if ((factor == 1) && (builder.charAt(builder.length() - 1) == 's')) {
                builder.deleteCharAt(builder.length() - 1);
            }
            period = builder.toString();
            builder.delete(0, builder.length());
        }
        return period;
    }

    /**
     * Extracts the aggregate function from a {@link SamplingType}.
     * 
     * @param samplingType The {@link SamplingType}.
     * @return A {@link String}.
     */
    public static String getAggregateFunction(SamplingType samplingType) {
        return samplingType == null ? null : MIN;
    }

    /**
     * Recovers an {@link AttributeConfig} from an attribute name and a {@link TangoArchivingConfigService}.
     * 
     * @param attributeName The attribute name.
     * @param configService The {@link TangoArchivingConfigService}.
     * @return An {@link AttributeConfig}.
     */
    public static AttributeConfig getAttributeConfig(String attributeName, TangoArchivingConfigService configService) {
        AttributeConfig config;
        if (attributeName == null) {
            config = null;
        } else {
            Optional<AttributeConfig> optional = configService.getAttributeConfig(attributeName);
            if ((optional == null) || !optional.isPresent()) {
                config = null;
            } else {
                config = optional.get();
            }
        }
        return config;
    }

    /**
     * Recovers the data format of an attribute, using given {@link TangoArchivingConfigService}.
     * 
     * @param attributeName The attribute name.
     * @param configService The {@link TangoArchivingConfigService}.
     * @return An <code>int</code>
     */
    public static int getDataFormat(String attributeName, TangoArchivingConfigService configService) {
        int dataFormat;
        AttributeConfig config = getAttributeConfig(attributeName, configService);
        dataFormat = config == null ? AttrDataFormat._FMT_UNKNOWN : config.getFormat();
        return dataFormat;
    }

    /**
     * Returns whether given attribute is a scalar according to given {@link TangoArchivingConfigService}.
     * 
     * @param attributeName The attribute name.
     * @param configService The {@link TangoArchivingConfigService}.
     * @return A <code>boolean</code>.
     */
    public static boolean isScalar(String attributeName, TangoArchivingConfigService configService) {
        return getDataFormat(attributeName, configService) == AttrDataFormat._SCALAR;
    }

    /**
     * Returns whether given attribute is a spectrum according to given {@link TangoArchivingConfigService}.
     * 
     * @param attributeName The attribute name.
     * @param configService The {@link TangoArchivingConfigService}.
     * @return A <code>boolean</code>.
     */
    public static boolean isSpectrum(String attributeName, TangoArchivingConfigService configService) {
        return getDataFormat(attributeName, configService) == AttrDataFormat._SPECTRUM;
    }

    /**
     * Returns whether given attribute is an image according to given {@link TangoArchivingConfigService}.
     * 
     * @param attributeName The attribute name.
     * @param configService The {@link TangoArchivingConfigService}.
     * @return A <code>boolean</code>.
     */
    public static boolean isImage(String attributeName, TangoArchivingConfigService configService) {
        return getDataFormat(attributeName, configService) == AttrDataFormat._IMAGE;
    }

    /**
     * Creates a {@link Timestamp} from its {@link String} representation.
     * 
     * @param timestampString The {@link String}.
     * @return A {@link Timestamp}.
     * @throws ArchivingException If a problem occurred.
     */
    public static Timestamp getTimestamp(String timestampString) throws ArchivingException {
        Timestamp timestamp;
        try {
            timestamp = new Timestamp(DateUtil.stringToMilli(timestampString, FORMATTER));
        } catch (DateTimeParseException e) {
            throw new ArchivingException(e);
        }
        return timestamp;
    }

    /**
     * Extracts the expected value from an {@link AttributeValue}.
     * 
     * @param attr The {@link AttributeValue}.
     * @return An {@link Object}.
     */
    protected static Object getValue(AttributeValue attr, ICancelable cancelable) {
        Object value;
        if (attr == null || isCancelled(cancelable)) {
            value = null;
        } else if (attr.getValueR() == null) {
            value = attr.getValueW();
        } else {
            value = attr.getValueR();
        }
        return value;
    }

    /**
     * Extracts the expected <code>double</code> value from an {@link AttributeValue}.
     * 
     * @param attrThe {@link AttributeValue}.
     * @param cancelable The {@link ICancelable} that indicates whether to cancel current job.
     * @return A <code>double</code.
     */
    public static double getDouble(AttributeValue attr, ICancelable cancelable) {
        Object value = getValue(attr, cancelable);
        double dblValue;
        try {
            dblValue = (value == null ? MathConst.NAN_FOR_NULL : Double.parseDouble(value.toString()));
        } catch (Exception e) {
            dblValue = Double.NaN;
        }
        return dblValue;
    }

    /**
     * Extracts scalar data representation from a method and adds it to a known {@link NullableTimedData} list.
     * 
     * @param dataList The {@link NullableTimedData} list.
     * @param date The known data date.
     * @param dataType The known data type.
     * @param nullData Whether data is already known as <code>null</code>.
     * @param method The method.
     * @param cancelable The {@link ICancelable} that indicates whether to cancel current job.
     */
    protected static void extractScalarData(List<NullableTimedData> dataList, long date, int dataType, boolean nullData,
            Supplier<?> method, ICancelable cancelable) {
        NullableTimedData data;
        boolean[] nullElements;
        Object value;
        if (nullData || isCancelled(cancelable)) {
            data = null;
            nullElements = null;
            value = null;
        } else {
            Object extracted = method.get();
            data = DbData.initNullableTimedData(dataType, date, 1);
            nullElements = new boolean[1];
            value = DbData.initPrimitiveArray(dataType, 1);
            if (extracted == null) {
                nullElements[0] = true;
            } else {
                Class<?> contentType = ArrayUtils.recoverDataType(value);
                if (contentType != null) {
                    if ((value != null) && extracted.getClass().equals(contentType)) {
                        Array.set(value, 0, extracted);
                    } else if (contentType.isPrimitive()) {
                        if (value instanceof boolean[]) {
                            boolean[] array = (boolean[]) value;
                            if (extracted instanceof Boolean) {
                                array[0] = ((Boolean) extracted).booleanValue();
                            } else if (extracted instanceof Number) {
                                array[0] = ((Number) extracted).byteValue() != 0;
                            }
                        } else if (value instanceof char[]) {
                            char[] array = (char[]) value;
                            if (extracted instanceof Character) {
                                array[0] = ((Character) extracted).charValue();
                            } else if (extracted instanceof Number) {
                                array[0] = (char) ((Number) extracted).intValue();
                            }
                        } else if (value instanceof byte[]) {
                            byte[] array = (byte[]) value;
                            if (extracted instanceof Number) {
                                array[0] = ((Number) extracted).byteValue();
                            }
                        } else if (value instanceof short[]) {
                            short[] array = (short[]) value;
                            if (extracted instanceof Number) {
                                array[0] = ((Number) extracted).shortValue();
                            }
                        } else if (value instanceof int[]) {
                            int[] array = (int[]) value;
                            if (extracted instanceof Number) {
                                array[0] = ((Number) extracted).intValue();
                            }
                        } else if (value instanceof long[]) {
                            long[] array = (long[]) value;
                            if (extracted instanceof Number) {
                                array[0] = ((Number) extracted).longValue();
                            }
                        } else if (value instanceof float[]) {
                            float[] array = (float[]) value;
                            if (extracted instanceof Number) {
                                array[0] = ((Number) extracted).floatValue();
                            }
                        } else if (value instanceof double[]) {
                            double[] array = (double[]) value;
                            if (extracted instanceof Number) {
                                array[0] = ((Number) extracted).doubleValue();
                            }
                        }
                    }
                }
            }
            switch (dataType) {
                case TangoConst.Tango_DEV_USHORT:
                case TangoConst.Tango_DEV_UCHAR:
                case TangoConst.Tango_DEV_ULONG:
                    value = UnsignedConverter.convertUnsigned(value);
            }
            data.setValue(value, nullElements);
            dataList.add(data);
        }
    }

    /**
     * Extracts the scalar data from an {@link AttributeValue} and adds it to known {@link NullableTimedData} lists.
     * 
     * @param attrValue The {@link AttributeValue}.
     * @param nullRead Whether read value is already known as <code>null</code>.
     * @param nullWrite Whether write value is already known as <code>null</code>.
     * @param dataType The known data type.
     * @param readList The {@link NullableTimedData} read list.
     * @param writeList The {@link NullableTimedData} write list.
     * @param cancelable The {@link ICancelable} that indicates whether to cancel current job.
     */
    protected static void extractScalarData(AttributeValue attrValue, boolean nullRead, boolean nullWrite, int dataType,
            List<NullableTimedData> readList, List<NullableTimedData> writeList, ICancelable cancelable) {
        if (attrValue != null) {
            Timestamp ts = attrValue.getDataTime();
            if (ts != null) {
                long date = ts.getTime();
                extractScalarData(readList, date, dataType, nullRead, attrValue::getValueR, cancelable);
                extractScalarData(writeList, date, dataType, nullWrite, attrValue::getValueW, cancelable);
            }
        }
    }

    /**
     * Extracts spectrum data representation from a method and adds it to a known {@link NullableTimedData} list,
     * updating some {@link DbData} information if necessary.
     * 
     * @param dataList The {@link NullableTimedData} list.
     * @param dbData The {@link DbData}.
     * @param date The known data date.
     * @param dataType The known data type.
     * @param nullData Whether data is already known as <code>null</code>.
     * @param method The method.
     * @param cancelable The {@link ICancelable} that indicates whether to cancel current job.
     */
    protected static void extractSpectrumData(List<NullableTimedData> dataList, DbData dbData, long date, int dataType,
            boolean nullData, Supplier<?> method, ICancelable cancelable) {
        NullableTimedData data;
        boolean[] nullElements;
        Object value;
        if (nullData || isCancelled(cancelable)) {
            data = null;
            nullElements = null;
            value = null;
        } else {
            Object extracted = method.get();
            if (extracted == null) {
                data = DbData.initNullableTimedData(dataType, date, 1);
                nullElements = new boolean[1];
                value = DbData.initPrimitiveArray(dataType, 1);
                nullElements[0] = true;
            } else {
                int dimX = Array.getLength(extracted);
                nullElements = new boolean[dimX];
                data = DbData.initNullableTimedData(dataType, date, dimX);
                if (data.getX() > dbData.getMaxX()) {
                    dbData.setMaxX(data.getX());
                }
                switch (dataType) {
                    case TangoConst.Tango_DEV_USHORT:
                    case TangoConst.Tango_DEV_SHORT:
                    case TangoConst.Tango_DEV_UCHAR:
                        value = NumberArrayUtils.extractShortArray(extracted);
                        break;
                    case TangoConst.Tango_DEV_DOUBLE:
                        value = NumberArrayUtils.extractDoubleArray(extracted);
                        break;
                    case TangoConst.Tango_DEV_FLOAT:
                        value = NumberArrayUtils.extractFloatArray(extracted);
                        break;
                    case TangoConst.Tango_DEV_STATE:
                    case TangoConst.Tango_DEV_ULONG:
                    case TangoConst.Tango_DEV_LONG:
                        value = NumberArrayUtils.extractIntArray(extracted);
                        break;
                    case TangoConst.Tango_DEV_LONG64:
                    case TangoConst.Tango_DEV_ULONG64:
                        value = NumberArrayUtils.extractLongArray(extracted);
                        break;
                    case TangoConst.Tango_DEV_BOOLEAN:
                        if (extracted instanceof boolean[]) {
                            value = extracted;
                        } else if (extracted instanceof Boolean[]) {
                            Boolean[] tmp = (Boolean[]) extracted;
                            boolean[] array = new boolean[tmp.length];
                            int index = 0;
                            for (Boolean bool : tmp) {
                                array[index++] = (bool != null) && bool.booleanValue();
                            }
                            value = array;
                        } else {
                            byte[] tmp = NumberArrayUtils.extractByteArray(extracted);
                            if (tmp == null) {
                                value = null;
                            } else {
                                boolean[] array = new boolean[tmp.length];
                                int i = 0;
                                for (byte val : tmp) {
                                    array[i++] = (val != 0);
                                }
                                value = array;
                            }
                        }
                        break;
                    case TangoConst.Tango_DEV_STRING:
                        if (extracted instanceof String[]) {
                            value = extracted;
                        } else {
                            value = null;
                        }
                        break;
                    default:
                        value = null;
                        break;
                }
                switch (dataType) {
                    case TangoConst.Tango_DEV_USHORT:
                    case TangoConst.Tango_DEV_UCHAR:
                    case TangoConst.Tango_DEV_ULONG:
                        value = UnsignedConverter.convertUnsigned(value);
                }
            } // end if (extracted == null) ... else
            data.setValue(value, nullElements);
            dataList.add(data);
        } // end if (nullData) ... else
    }

    /**
     * Extracts the spectrum data from an {@link AttributeValue} and adds it to known {@link NullableTimedData} lists,
     * updating some {@link DbData} information if necessary.
     * 
     * @param attrValue The {@link AttributeValue}.
     * @param result The {@link DbData} array that stores {@link DbData} for both read and write values.
     * @param nullRead Whether read value is already known as <code>null</code>.
     * @param nullWrite Whether write value is already known as <code>null</code>.
     * @param dataType The known data type.
     * @param readList The {@link NullableTimedData} read list.
     * @param writeList The {@link NullableTimedData} write list.
     * @param cancelable The {@link ICancelable} that indicates whether to cancel current job.
     */
    protected static void extractSpectrumData(AttributeValue attrValue, DbData[] result, boolean nullRead,
            boolean nullWrite, int dataType, List<NullableTimedData> readList, List<NullableTimedData> writeList,
            ICancelable cancelable) {
        if (attrValue != null) {
            Timestamp ts = attrValue.getDataTime();
            if (ts != null) {
                long date = ts.getTime();
                extractSpectrumData(readList, result[DbData.READ_INDEX], date, dataType, nullRead, attrValue::getValueR,
                        cancelable);
                extractSpectrumData(writeList, result[DbData.WRITE_INDEX], date, dataType, nullWrite,
                        attrValue::getValueW, cancelable);
            }
        }
    }

    //

    protected static boolean[][] initNullElementsForNullImageValue() {
        boolean[][] nullElements = new boolean[1][1];
        nullElements[0][0] = true;
        return nullElements;
    }

    protected static Object initNullImageValue(int dataType) {
        Object value = null;
        Object array = DbData.initPrimitiveArray(dataType, 1);
        if ((array != null) && (array.getClass().isArray())) {
            value = Array.newInstance(array.getClass(), 1);
            Array.set(value, 0, array);
        }
        return value;
    }

    /**
     * Extracts image data representation from a method and adds it to a known {@link NullableTimedData} list.
     * 
     * @param dataList The {@link NullableTimedData} list.
     * @param date The known data date.
     * @param dataType The known data type.
     * @param nullData Whether data is already known as <code>null</code>.
     * @param method The method.
     * @param cancelable The {@link ICancelable} that indicates whether to cancel current job.
     */
    protected static void extractImageData(List<NullableTimedData> dataList, long date, int dataType, boolean nullData,
            Supplier<?> method, ICancelable cancelable) {
        NullableTimedData data;
        boolean[][] nullElements;
        Object value;
        if (nullData || isCancelled(cancelable)) {
            data = null;
            nullElements = null;
            value = null;
        } else {
            Object extracted = method.get();
            if (extracted == null) {
                data = DbData.initNullableTimedData(dataType, date, 1, 1);
                nullElements = initNullElementsForNullImageValue();
                value = initNullImageValue(dataType);
            } else {
                int[] shape = ArrayUtils.recoverShape(extracted);
                if (shape.length == 2) {
                    int dimY = shape[0], dimX = shape[1];
                    nullElements = new boolean[dimY][dimX];
                    data = DbData.initNullableTimedData(dataType, date, dimX, dimY);
                    switch (dataType) {
                        case TangoConst.Tango_DEV_USHORT:
                        case TangoConst.Tango_DEV_SHORT:
                        case TangoConst.Tango_DEV_UCHAR:
                            value = NumberMatrixUtils.extractShortMatrix(extracted);
                            break;
                        case TangoConst.Tango_DEV_DOUBLE:
                            value = NumberMatrixUtils.extractDoubleMatrix(extracted);
                            break;
                        case TangoConst.Tango_DEV_FLOAT:
                            value = NumberMatrixUtils.extractFloatMatrix(extracted);
                            break;
                        case TangoConst.Tango_DEV_STATE:
                        case TangoConst.Tango_DEV_ULONG:
                        case TangoConst.Tango_DEV_LONG:
                            value = NumberMatrixUtils.extractIntMatrix(extracted);
                            break;
                        case TangoConst.Tango_DEV_LONG64:
                        case TangoConst.Tango_DEV_ULONG64:
                            value = NumberMatrixUtils.extractLongMatrix(extracted);
                            break;
                        case TangoConst.Tango_DEV_BOOLEAN:
                            if (extracted instanceof boolean[][]) {
                                value = extracted;
                            } else if (extracted instanceof Boolean[][]) {
                                Boolean[][] tmp = (Boolean[][]) extracted;
                                boolean[][] matrix = new boolean[tmp.length][];
                                int y = 0, x = 0;
                                for (Boolean[] line : tmp) {
                                    x = 0;
                                    if (line != null) {
                                        matrix[y] = new boolean[line.length];
                                        for (Boolean bool : line) {
                                            matrix[y][x++] = (bool != null) && bool.booleanValue();
                                        }
                                        y++;
                                    }
                                }
                                value = matrix;
                            } else {
                                byte[][] tmp = NumberMatrixUtils.extractByteMatrix(extracted);
                                if (tmp == null) {
                                    value = null;
                                } else {
                                    boolean[][] matrix = new boolean[dimY][dimX];
                                    for (int y = 0; y < dimY; y++) {
                                        for (int x = 0; x < dimX; x++) {
                                            matrix[y][x] = (tmp[y][x] != 0);
                                        }
                                    }
                                    value = matrix;
                                }
                            }
                            break;
                        case TangoConst.Tango_DEV_STRING:
                            if (extracted instanceof String[]) {
                                value = extracted;
                            } else {
                                value = null;
                            }
                            break;
                        default:
                            value = null;
                            break;
                    }
                    switch (dataType) {
                        case TangoConst.Tango_DEV_USHORT:
                        case TangoConst.Tango_DEV_UCHAR:
                        case TangoConst.Tango_DEV_ULONG:
                            value = UnsignedConverter.convertUnsigned(value);
                    }
                } else {
                    data = DbData.initNullableTimedData(dataType, date, 1, 1);
                    nullElements = initNullElementsForNullImageValue();
                    value = initNullImageValue(dataType);
                } // end if (shape.length == 2) ... else
            } // end if (extracted == null) ... else
            data.setValue(value, nullElements);
            dataList.add(data);
        } // end if (nullData) ... else
    }

    /**
     * Extracts the image data from an {@link AttributeValue} and adds it to known {@link NullableTimedData} lists.
     * 
     * @param attrValue The {@link AttributeValue}.
     * @param nullRead Whether read value is already known as <code>null</code>.
     * @param nullWrite Whether write value is already known as <code>null</code>.
     * @param dataType The known data type.
     * @param readList The {@link NullableTimedData} read list.
     * @param writeList The {@link NullableTimedData} write list.
     * @param cancelable The {@link ICancelable} that indicates whether to cancel current job.
     */
    protected static void extractImageData(AttributeValue attrValue, DbData[] result, boolean nullRead,
            boolean nullWrite, int dataType, List<NullableTimedData> readList, List<NullableTimedData> writeList,
            ICancelable cancelable) {
        if (attrValue != null) {
            Timestamp ts = attrValue.getDataTime();
            if (ts != null) {
                long date = ts.getTime();
                extractImageData(readList, date, dataType, nullRead, attrValue::getValueR, cancelable);
                extractImageData(writeList, date, dataType, nullWrite, attrValue::getValueW, cancelable);
            }
        }
    }

    /**
     * Converts an {@link AttributeValueSeries} into a {@link DbData} array.
     * 
     * @param avs The {@link AttributeValueSeries}.
     * @param cancelable The {@link ICancelable} that indicates whether to cancel current job.
     * @return A {@link DbData} array.
     */
    public static DbData[] toDbDataArray(AttributeValueSeries avs, ICancelable cancelable) {
        DbData[] result;
        if (avs == null || isCancelled(cancelable)) {
            result = new DbData[2];
        } else {
            int dataType = avs.getAttributeConfig().getType();
            final String attributeName = avs.getAttributeConfig().getFullName();
            final int writeType = avs.getAttributeConfig().getWriteType();
            final int dataFormat = avs.getAttributeConfig().getFormat();
            result = DbData.initExtractionResult(attributeName, dataType, dataFormat, writeType);
            final List<AttributeValue> valuesFromDb = avs.getAttributeValues();
            List<NullableTimedData> readList = new ArrayList<>(), writeList = new ArrayList<>();
            final boolean nullRead = writeType == AttrWriteType._WRITE, nullWrite = writeType == AttrWriteType._READ;
            switch (dataFormat) {
                case AttrDataFormat._SCALAR:
                    for (AttributeValue attrValue : valuesFromDb) {
                        if (isCancelled(cancelable)) {
                            break;
                        }
                        extractScalarData(attrValue, nullRead, nullWrite, dataType, readList, writeList, cancelable);
                    }
                    break;
                case AttrDataFormat._SPECTRUM:
                    for (AttributeValue attrValue : valuesFromDb) {
                        if (isCancelled(cancelable)) {
                            break;
                        }
                        extractSpectrumData(attrValue, result, nullRead, nullWrite, dataType, readList, writeList,
                                cancelable);
                    }
                    break;
                case AttrDataFormat._IMAGE:
                    for (AttributeValue attrValue : valuesFromDb) {
                        if (isCancelled(cancelable)) {
                            break;
                        }
                        extractImageData(attrValue, result, nullRead, nullWrite, dataType, readList, writeList,
                                cancelable);
                    }
                    break;
                default:
                    break;
            }
            if (!isCancelled(cancelable)) {
                DbData.afterExtraction(readList, writeList, result);
            }
        }
        return result;
    }

    /**
     * Converts an {@link AttributeValueSeries} into a {@link Collection} of {@link ImageData}.
     * 
     * @param avs The {@link AttributeValueSeries}.
     * @param cancelable The {@link ICancelable} that indicates whether to cancel current job.
     * @return A {@link Collection}, never <code>null</code>, of {@link ImageData}.
     */
    public static Collection<ImageData> toImageData(AttributeValueSeries avs, ICancelable cancelable) {
        Collection<ImageData> result = new ArrayList<>();
        // TODO TTS not supported yet!
        return result;
    }

    /**
     * Recovers the regular expression for a simplified one.
     * 
     * @param simpleRegExp The simplified regular expression (in which "*" is used instead of ".*").
     * @return A {@link String}.
     */
    public static String getRegularExpression(String simpleRegExp) {
        return simpleRegExp == null ? null
                : simpleRegExp.replace(DOT, ESCAPED_DOT).replace(GUIUtilities.TANGO_JOKER, JOKER);
    }

    /**
     * Returns whether given {@link String} matches given regular expression.
     * 
     * @param str The {@link String} to test.
     * @param regExp The regular expression.
     * @return A <code>boolean</code>.
     */
    public static boolean matches(String str, String regExp) {
        boolean matches;
        if ((str == null) || regExp == null) {
            matches = false;
        } else {
            matches = str.matches(regExp);
        }
        return matches;
    }

    /**
     * Extracts all values in given {@link Collection} matching given regular expression.
     * 
     * @param values The {@link Collection} of values.
     * @param regExp The regular expression.
     * @return A {@link String} array.
     */
    public static String[] getValuesMatchingRegExp(Collection<String> values, String regExp) {
        String[] result;
        if (values == null) {
            result = null;
        } else {
            Collection<String> accumulator = new ArrayList<>();
            for (String value : values) {
                if (ExtractionUtils.matches(value, regExp)) {
                    accumulator.add(value);
                }
            }
            result = accumulator.toArray(new String[accumulator.size()]);
            accumulator.clear();
        }
        return result;
    }

    public static String toTTSCriterions(String criterions) {
        return criterions == null ? null
                : criterions.replace(TTS_JOKER, ESCAPED_TTS_JOKER).replace(GUIUtilities.TANGO_JOKER, TTS_JOKER);
    }

    public static String[] getCleanedValues(Collection<String> list) {
        String[] values;
        if (list == null) {
            values = null;
        } else {
            values = list.toArray(new String[list.size()]);
            try {
                list.clear();
            } catch (Exception e) {
                // Ignore: it just means list can't be cleaned
            }
        }
        return values;
    }

}
