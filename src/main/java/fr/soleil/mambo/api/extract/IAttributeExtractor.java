package fr.soleil.mambo.api.extract;

import java.util.Collection;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;

// alternative for fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.AttributeExtractor, compatible with TTS too
public interface IAttributeExtractor {

    boolean isCanceled();

    void setCanceled(boolean canceled) throws ArchivingException;

    double getAttDataMinBetweenDates(String... param) throws ArchivingException;

    double getAttDataMaxBetweenDates(String... param) throws ArchivingException;

    DbData[] getAttDataBetweenDates(SamplingType samplingType, String... param) throws ArchivingException;

    Collection<ImageData> getAttImageDataBetweenDates(final SamplingType samplingType, final String... param)
            throws ArchivingException;
}
