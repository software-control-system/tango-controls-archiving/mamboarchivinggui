package fr.soleil.mambo.api;

import fr.soleil.archiving.common.api.exception.ArchivingException;

/**
 * {@link FunctionalInterface} for the declaration of archiving functions in lambda expressions.<br />
 * Based on the idea of <a href="https://stackoverflow.com/users/45914/jason">Jason</a> found on <a href=
 * "https://stackoverflow.com/questions/18198176/java-8-lambda-function-that-throws-exception">stackoverflow</a>.
 *
 * @param <T> The type of data the function takes as input (argument).
 * @param <R> The type of data returned by the function as output.
 * 
 * @author GIRARDOT, Jason
 */
@FunctionalInterface
public interface ArchivingFunction<T, R> {
    R apply(T t) throws ArchivingException;
}