package fr.soleil.mambo.api;

public interface ApiConstants {

    public static final String[] EMPTY = new String[0];
    public static final String HDB = "HDB";
    public static final String TDB = "TDB";
    public static final String TTS = "TTS";
    public static final String TTS_DEVICE_CLASS = "TimeseriesArchiver";
    public static final String PORT = "Port";
    public static final String DB_NAME = "Db Name";
    public static final String DEFAULT = "default one";

    // ///////////////////////////////////////// //
    // Java properties and environment variables //
    // ///////////////////////////////////////// //
    // Expected behavior:
    // - Mambo gets the information from the Java property.
    // - If the Java property is not set, then mambo gets the information from the environment variable.

    /** Java property that indicates whether TTS is available */
    public static final String TTS_AVAILABLE_PROPERTY = "ttsAvailable";
    /** Environment variable that indicates whether TTS is available */
    public static final String TTS_AVAILABLE_ENV = "TTS_AVAILABLE";

    /** Java property that indicates the TTS host */
    public static final String TTS_HOST_PROPERTY = "ttsHost";
    /** Environment variable that indicates the TTS host */
    public static final String TTS_HOST_ENV = "TTS_HOST";

    /** Java property that indicates the TTS port */
    public static final String TTS_PORT_PROPERTY = "ttsPort";
    /** Environment variable that indicates the TTS port */
    public static final String TTS_PORT_ENV = "TTS_PORT";

    /** Java property that indicates the TTS database type */
    public static final String TTS_DB_TYPE_PROPERTY = "ttsDbType";
    /** Environment variable that indicates the TTS database type */
    public static final String TTS_DB_TYPE_ENV = "TTS_DB_TYPE";

    /** Java property that indicates the TTS database name */
    public static final String TTS_NAME_PROPERTY = "ttsName";
    /** Environment variable that indicates the TTS database name */
    public static final String TTS_NAME_ENV = "TTS_NAME";

    /** Java property that indicates the TTS database schema */
    public static final String TTS_SCHEMA_PROPERTY = "ttsSchema";
    /** Environment variable that indicates the TTS database schema */
    public static final String TTS_SCHEMA_ENV = "TTS_SCHEMA";

    /** Java property that indicates the TTS database minimum pool size */
    public static final String TTS_MIN_POOL_SIZE_PROPERTY = "ttsMinPoolSize";
    /** Environment variable that indicates the TTS database minimum pool size */
    public static final String TTS_MIN_POOL_SIZE_ENV = "TTS_MIN_POOL_SIZE";

    /** Java property that indicates the TTS database maximum pool size */
    public static final String TTS_MAX_POOL_SIZE_PROPERTY = "ttsMaxPoolSize";
    /** Environment variable that indicates the TTS database maximum pool size */
    public static final String TTS_MAX_POOL_SIZE_ENV = "TTS_MAX_POOL_SIZE";

    /** Java property that indicates the TTS connection inactivity timeout */
    public static final String TTS_INACTIVITY_TIMEOUT_PROPERTY = "ttsInactivityTimeout";
    /** Environment variable that indicates the TTS connection inactivity timeout */
    public static final String TTS_INACTIVITY_TIMEOUT_ENV = "TTS_INACTIVITY_TIMEOUT";

    /** Java property that indicates whether to buffer archiving information */
    public static final String BUFFERED_ARCHIVING_INFO_PROPERTY = "bufferedArchivingInfo";
    /** Environment variable that indicates whether to buffer archiving information */
    public static final String BUFFERED_ARCHIVING_INFO_ENV = "BUFFERED_ARCHIVING_INFO";

    /** Java property that indicates the archiving information buffer lifetime, in seconds */
    public static final String ATTRIBUTE_INFO_BUFFER_LIFETIME_PROPERTY = "attributeInfoBufferLifetime";
    /** Environment variable that indicates the archiving information buffer lifetime, in seconds */
    public static final String ATTRIBUTE_INFO_BUFFER_LIFETIME_ENV = "ATTRIBUTE_INFO_BUFFER_LIFETIME";

    /**
     * Java property that indicates the maximum memory percentage at which all archiving information buffers should be
     * cleaned.<br />
     * If used memory goes above this percentage of maximum allowed memory <i>(Xmx in Java properties)</i>, then all
     * archiving information buffers are automatically cleaned in order to free memory.
     */
    public static final String BUFFER_AUTOCLEAN_MEMORY_PERCENTAGE_PROPERTY = "bufferAutoCleanMemoryPercentage";
    /**
     * Environment variable that indicates the maximum memory percentage at which all archiving information buffers
     * should be cleaned.<br />
     * If used memory goes above this percentage of maximum allowed memory <i>(Xmx in Java properties)</i>, then all
     * archiving information buffers are automatically cleaned in order to free memory.
     */
    public static final String BUFFER_AUTOCLEAN_MEMORY_PERCENTAGE_ENV = "BUFFER_AUTOCLEAN_MEMORY_PERCENTAGE";

}
