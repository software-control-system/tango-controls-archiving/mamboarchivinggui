package fr.soleil.mambo.api.db;

import fr.soleil.archiving.hdbtdb.api.manager.HdbArchivingManagerApiRef;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class HDBDataBaseAccess extends ALegacyDataBaseAccess<HdbArchivingManagerApiRef> {

    public HDBDataBaseAccess(HdbArchivingManagerApiRef manager, AbstractDataBaseConnector connector) {
        super(manager, connector);
    }

    @Override
    protected boolean isHistoric() {
        return true;
    }

}
