package fr.soleil.mambo.api.db;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.TangoApi.DbDatum;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.utils.DbConnectionInfo;
import fr.soleil.archiving.hdbtdb.api.HdbTdbConnectionParameters;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ShortPeriodAttributesManager;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.ArrayUtils;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.api.ApiUtils;

// alternative for fr.soleil.archiving.hdbtdb.api.HdbTdbConnectionParameters, compatible with TTS too
public class DbConnectionParameters implements ApiConstants {

    private static boolean ttsAvailable = CometeConstants.TRUE.equalsIgnoreCase(
            ApiUtils.getProperty(TTS_AVAILABLE_PROPERTY, TTS_AVAILABLE_ENV, Boolean.FALSE.toString()));
    private static String user, password;
    private static FastHdbAttributesTarget fastHdbAttributesTarget;
    private static final String HDB_ARCHIVER = "HdbArchiver";
    private static final String SHORT_PERIOD_ATTRIBUTES = "shortPeriodAttributes";
    private static final Timer TIMER = new Timer("HDB fast attributes check");
    private static final Object TASK_LOCK = new Object();
    private static final Logger LOGGER = LoggerFactory.getLogger(DbConnectionParameters.class);

    private DbConnectionParameters() {
        // hide constructor
    }

    public static String getUser() {
        return user;
    }

    public static String getPassword() {
        return password;
    }

    public static boolean isHdbAvailable() {
        return HdbTdbConnectionParameters.isHdbAvailable();
    }

    public static boolean isTdbAvailable() {
        return HdbTdbConnectionParameters.isTdbAvailable();
    }

    public static boolean isTtsAvailable() {
        return ttsAvailable;
    }

    public static void performAllDBInit(final Boolean historic, final String user, final String password)
            throws ArchivingException {
        if (historic == null) {
            DbConnectionParameters.user = user;
            DbConnectionParameters.password = password;
            ttsAvailable = ttsAvailable && (user != null) && (!user.trim().isEmpty()) && (password != null)
                    && (!password.trim().isEmpty());
        } else if (historic.booleanValue()) {
            HdbTdbConnectionParameters.performAllHDBInit(null, ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING,
                    ObjectUtils.EMPTY_STRING, user, password, ObjectUtils.EMPTY_STRING);
            if (isHdbAvailable()) {
                if (fastHdbAttributesTarget == null) {
                    synchronized (TASK_LOCK) {
                        if (fastHdbAttributesTarget == null) {
                            fastHdbAttributesTarget = new FastHdbAttributesTarget();
                            // TANGOARCH-885: Check for fast HDB attributes every 30s
                            TIMER.schedule(new FashtAttributesCheckTask(), 200, 30000);
                        }
                    }
                }
            }
        } else {
            HdbTdbConnectionParameters.performAllTDBInit(null, ObjectUtils.EMPTY_STRING, ObjectUtils.EMPTY_STRING,
                    ObjectUtils.EMPTY_STRING, user, password, ObjectUtils.EMPTY_STRING);
        }

    }

    public static void setDBUser(final Boolean historic, final String user) {
        if (historic == null) {
            DbConnectionParameters.user = user;
        } else if (historic.booleanValue()) {
            HdbTdbConnectionParameters.setHDBUser(user, DbConnectionInfo.ORIGIN_ARGIN);
        } else {
            HdbTdbConnectionParameters.setTDBUser(user, DbConnectionInfo.ORIGIN_ARGIN);
        }
    }

    public static void setDBPassword(final Boolean historic, final String password) {
        if (historic == null) {
            DbConnectionParameters.password = password;
        } else if (historic.booleanValue()) {
            HdbTdbConnectionParameters.setHDBPassword(password, DbConnectionInfo.ORIGIN_ARGIN);
        } else {
            HdbTdbConnectionParameters.setTDBPassword(password, DbConnectionInfo.ORIGIN_ARGIN);
        }
    }

    public static Boolean getDefaultDatabase(boolean preferredDefault) {
        Boolean historic;
        if (isTtsAvailable()) {
            historic = null;
        } else if (preferredDefault) {
            if (isHdbAvailable()) {
                historic = Boolean.TRUE;
            } else {
                historic = Boolean.FALSE;
            }
        } else if (isTdbAvailable()) {
            historic = Boolean.FALSE;
        } else {
            historic = Boolean.TRUE;
        }
        return historic;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private static class FastHdbAttributesTarget {
        private volatile String[] attributes;

        public FastHdbAttributesTarget() {
            attributes = null;
        }

        public void setFastHdbAttributes(String[] value) {
            if (!ArrayUtils.equals(value, this.attributes)) {
                String[] attributes = value == null ? null : value.clone();
                this.attributes = attributes;
                Map<String, Integer> shortPeriodAttributes = new HashMap<>();
                if (attributes != null && attributes.length > 0) {
                    attrLoop: for (final String attribute : attributes) {
                        if (attribute != null && !attribute.trim().isEmpty()) {
                            String temp = attribute;
                            int delimiterCount = 0;
                            while (temp.indexOf(',') > -1) {
                                // Doing so, we are sure to test all bad cases, even
                                // the ones not detectable by split
                                delimiterCount++;
                                if (delimiterCount > 1) {
                                    temp = null;
                                    shortPeriodAttributes.clear();
                                    break attrLoop;
                                }
                                temp = temp.replaceFirst(CometeConstants.COMMA, ObjectUtils.EMPTY_STRING);
                            }
                            temp = null;
                            final String[] attributeParts = attribute.split(CometeConstants.COMMA);
                            if (attributeParts.length > 2) {
                                // property is not well formatted (too many separators)
                                shortPeriodAttributes.clear();
                                break attrLoop;
                            } else {
                                final String attributeName = attributeParts[0].trim().toLowerCase();
                                if (attributeName.isEmpty()) {
                                    shortPeriodAttributes.clear();
                                    break attrLoop;
                                } else if (shortPeriodAttributes.containsKey(attributeName)) {
                                    shortPeriodAttributes.clear();
                                    break attrLoop;
                                } else {
                                    final Integer attributePeriod = Integer.parseInt(attributeParts[1]);
                                    if (attributePeriod > 0 && attributePeriod < 10) {
                                        shortPeriodAttributes.put(attributeName, attributePeriod);
                                    } else {
                                        shortPeriodAttributes.clear();
                                        break attrLoop;
                                    }
                                }
                            } // end if (attributeParts.length > 2) ... else
                        } // end if (attribute != null && !attribute.trim().isEmpty())
                    } // end attrLoop: for (final String attribute : attributes)
                } // end if (attributes != null && attributes.length > 0)
                ShortPeriodAttributesManager.setShortPeriodAttributes(shortPeriodAttributes);
            } // end if (!ArrayUtils.equals(value, this.attributes))
        }
    }

    private static class FashtAttributesCheckTask extends TimerTask {

        public FashtAttributesCheckTask() {
            super();
        }

        @Override
        public void run() {
            try {
                DbDatum prop = TangoDeviceHelper.getDatabase().get_class_property(HDB_ARCHIVER,
                        SHORT_PERIOD_ATTRIBUTES);
                if (prop != null) {
                    fastHdbAttributesTarget.setFastHdbAttributes(prop.extractStringArray());
                }
            } catch (Exception e) {
                LOGGER.error("Failed to check HDB short period attributes:\n" + TangoExceptionHelper.getErrorMessage(e),
                        e);
            }
        }
    }

}
