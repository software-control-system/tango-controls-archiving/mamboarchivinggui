package fr.soleil.mambo.api.db;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.TDBDataBaseManager;
import fr.soleil.archiving.hdbtdb.api.management.database.tdb.data.export.ITdbDataExport;
import fr.soleil.archiving.hdbtdb.api.manager.TdbArchivingManagerApiRef;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class TDBDataBaseAccess extends ALegacyDataBaseAccess<TdbArchivingManagerApiRef> {

    public TDBDataBaseAccess(TdbArchivingManagerApiRef manager, AbstractDataBaseConnector connector) {
        super(manager, connector);
    }

    public ITdbDataExport getTdbExport() throws ArchivingException {
        TDBDataBaseManager db = (TDBDataBaseManager) getDataBaseManager();
        return db == null ? null : db.getTdbExport();
    }

    @Override
    protected boolean isHistoric() {
        return false;
    }

}
