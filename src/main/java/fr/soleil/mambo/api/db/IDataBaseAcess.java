package fr.soleil.mambo.api.db;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.api.extract.IAttributeExtractor;

// alternative for fr.soleil.archiving.hdbtdb.api.DataBaseManager, compatible with TTS too
public interface IDataBaseAcess {

    IAttributeExtractor getExtractor() throws ArchivingException;

    boolean hasUSDateFormat();

    String[] getAttributesByCriterion(String domainCriterions, String familyCriterions, String memberCriterions,
            String attributeCriterions, ICancelable cancelable) throws ArchivingException;

    String[] getAttributesCompleteNames() throws ArchivingException;

    String[] getAttributes(String domain, String family, String member) throws ArchivingException;

    int getType(String attributeName) throws ArchivingException;

    int getWritable(String attributeName) throws ArchivingException;

    int getDataFormat(String attributeName) throws ArchivingException;

    String getFormat(String attributeName) throws ArchivingException;

    String getArchiverForAttribute(String attributeName) throws ArchivingException;

}
