package fr.soleil.mambo.api.db;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.api.ApiUtils;
import fr.soleil.mambo.api.archiving.TTSArchivingManagerApi;
import fr.soleil.mambo.api.extract.ExtractionUtils;
import fr.soleil.mambo.api.extract.IAttributeExtractor;
import fr.soleil.mambo.api.extract.TTSAttributeExtractor;
import fr.soleil.tango.archiving.TangoArchivingSystemConfigurationService;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.config.AttributeConfig;
import fr.soleil.tango.archiving.config.AttributeParameters;
import fr.soleil.tango.archiving.services.TangoArchivingConfigService;
import fr.soleil.tango.archiving.services.TangoArchivingFetcherService;

public class TTSDataBaseAccess implements IDataBaseAcess, ApiConstants {

    private static final Logger LOGGER = LoggerFactory.getLogger(TTSDataBaseAccess.class);
    private static final String FAILED_TO_GET_ATTRIBUTE_CONFIG_FOR = "Failed to get AttributeConfig for ";

    protected TTSAttributeExtractor attributeExtractor;
    protected WeakReference<TTSArchivingManagerApi> apiRef;

    public TTSDataBaseAccess(final DataBaseParameters params) {
        DatabaseConnectionConfig config = new DatabaseConnectionConfig();
        config.setDbType(DatabaseConnectionConfig.DataBaseType.parseDataBaseType(params.getDbType().name()));
        config.setName(params.getName());
        config.setHost(params.getHost());
        // TANGOARCH-833: possibility to connect to another port
        config.setPort(ApiUtils.getProperty(TTS_PORT_PROPERTY, TTS_PORT_ENV, config.getPort()));
        config.setUser(params.getUser());
        config.setPassword(params.getPassword());
        config.setIdleTimeout(params.getInactivityTimeout());
        config.setMaxPoolSize(params.getMaxPoolSize());
        if (params.getHealthRegistry() != null) {
            config.setHealthRegistry(params.getHealthRegistry());
        }
        if (params.getMetricRegistry() != null) {
            config.setMetricRegistry(params.getMetricRegistry());
        }
        LOGGER.info("connecting to TTS {}", config);
        attributeExtractor = new TTSAttributeExtractor(config);
    }

    protected TTSArchivingManagerApi getApi() {
        return ObjectUtils.recoverObject(apiRef);
    }

    public void setApi(TTSArchivingManagerApi api) {
        apiRef = api == null ? null : new WeakReference<>(api);
    }

    protected TangoArchivingSystemConfigurationService getConfigurationService() {
        TTSArchivingManagerApi api = getApi();
        return api == null ? null : api.getService();
    }

    protected TangoArchivingFetcherService getFetcherService() {
        return attributeExtractor == null ? null : attributeExtractor.getFetcherService();
    }

    protected TangoArchivingConfigService getConfigService() {
        return attributeExtractor == null ? null : attributeExtractor.getConfigService();
    }

    protected AttributeConfig getAttributeConfig(String attributeName) {
        AttributeConfig config;
        TangoArchivingConfigService service = getConfigService();
        if (service == null) {
            config = null;
        } else {
            try {
                config = ExtractionUtils.getAttributeConfig(attributeName, service);
            } catch (Exception e) {
                LOGGER.error(FAILED_TO_GET_ATTRIBUTE_CONFIG_FOR + attributeName, e);
                config = null;
            }
        }
        return config;
    }

    protected AttributeParameters getAttributeParameters(String attributeName) {
        Optional<AttributeParameters> optional;
        TangoArchivingFetcherService service = attributeExtractor == null ? null
                : attributeExtractor.getFetcherService();
        if (service == null) {
            optional = null;
        } else {
            optional = service.getAttributeParameters(attributeName);
        }
        return optional == null || !optional.isPresent() ? null : optional.get();
    }

    protected String[] getAttributes(String ttsCriterions, boolean completeName) {
        String[] result;
        TangoArchivingConfigService service = getConfigService();
        if ((service == null) || (ttsCriterions == null)) {
            result = null;
        } else {
            Collection<AttributeConfig> configs = service.getAttributesByAttributePattern(ttsCriterions);
            if (configs == null) {
                result = null;
            } else {
                Collection<String> attributes = new ArrayList<>();
                if (completeName) {
                    for (AttributeConfig config : configs) {
                        if ((config != null) && (config.getFullName() != null)) {
                            attributes.add(config.getFullName());
                        }
                    }
                } else {
                    for (AttributeConfig config : configs) {
                        if ((config != null) && (config.getName() != null)) {
                            attributes.add(config.getName());
                        }
                    }
                }
                result = ExtractionUtils.getCleanedValues(attributes);
            }
        }
        return result;
    }

    @Override
    public IAttributeExtractor getExtractor() throws ArchivingException {
        return attributeExtractor;
    }

    @Override
    public boolean hasUSDateFormat() {
        // date format is EU one
        return false;
    }

    @Override
    public String[] getAttributesByCriterion(String domainCriterions, String familyCriterions, String memberCriterions,
            String attributeCriterions, ICancelable cancelable) throws ArchivingException {
        TangoArchivingConfigService service = getConfigService();
        return service == null ? null
                : ExtractionUtils.getCleanedValues(service.getFilteredAttributes(domainCriterions, familyCriterions,
                        memberCriterions, ExtractionUtils.toTTSCriterions(attributeCriterions)));
    }

    @Override
    public String[] getAttributesCompleteNames() throws ArchivingException {
        return getAttributes(ExtractionUtils.TTS_JOKER, true);
    }

    @Override
    public String[] getAttributes(String domain, String family, String member) throws ArchivingException {
        String[] result;
        if ((domain == null) || (family == null) || (member == null)) {
            result = null;
        } else {
            result = getAttributes(domain + TangoDeviceHelper.SLASH + family + TangoDeviceHelper.SLASH + member
                    + TangoDeviceHelper.SLASH + ExtractionUtils.TTS_JOKER, false);
        }
        return result;
    }

    @Override
    public int getType(String attributeName) throws ArchivingException {
        AttributeConfig config = getAttributeConfig(attributeName);
        return config == null ? -1 : config.getType();
    }

    @Override
    public int getWritable(String attributeName) throws ArchivingException {
        AttributeConfig config = getAttributeConfig(attributeName);
        return config == null ? -1 : config.getWriteType();
    }

    @Override
    public int getDataFormat(String attributeName) throws ArchivingException {
        AttributeConfig config = getAttributeConfig(attributeName);
        return config == null ? -1 : config.getFormat();
    }

    @Override
    public String getFormat(String attributeName) throws ArchivingException {
        AttributeParameters parameters = getAttributeParameters(attributeName);
        return parameters == null ? null : parameters.getFormat();
    }

    @Override
    public String getArchiverForAttribute(String attributeName) throws ArchivingException {
        String archiver = null;
        if (attributeName != null) {
            TangoArchivingSystemConfigurationService service = getConfigurationService();
            if (service != null) {
                archiver = service.getArchiverForAttribute(attributeName);
            }
        }
        return archiver;
    }

}
