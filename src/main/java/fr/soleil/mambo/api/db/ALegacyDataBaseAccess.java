package fr.soleil.mambo.api.db;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.manager.IArchivingManagerApiRef;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.MySQLDataBaseConnector;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.mambo.api.extract.ExtractionUtils;
import fr.soleil.mambo.api.extract.IAttributeExtractor;
import fr.soleil.mambo.api.extract.LegacyExtractorFactory;

public abstract class ALegacyDataBaseAccess<M extends IArchivingManagerApiRef> implements IDataBaseAcess {

    protected final M manager;
    protected final AbstractDataBaseConnector connector;

    public ALegacyDataBaseAccess(M manager, AbstractDataBaseConnector connector) {
        this.manager = manager;
        this.connector = connector;
    }

    protected DataBaseManager getDataBaseManager() {
        return manager == null ? null : manager.getDataBase();
    }

    protected abstract boolean isHistoric();

    @Override
    public IAttributeExtractor getExtractor() throws ArchivingException {
        DataBaseManager db = getDataBaseManager();
        return db == null ? null : LegacyExtractorFactory.getLegacyAttributeExtractor(db.getExtractor(), isHistoric());
    }

    @Override
    public boolean hasUSDateFormat() {
        return connector instanceof MySQLDataBaseConnector;
    }

    @Override
    public String[] getAttributesByCriterion(String domainCriterions, String familyCriterions, String memberCriterions,
            String attributeCriterions, ICancelable cancelable) throws ArchivingException {
        String[] attributes;
        DataBaseManager db = getDataBaseManager();
        if ((db == null) || ((cancelable != null) && cancelable.isCanceled())) {
            attributes = null;
        } else {
            List<String> attrList = new ArrayList<>();
            String[] domains = db.getAttribute().getDomains().getDomainsByCriterion(domainCriterions);
            if (domains != null) {
                for (String domain : domains) {
                    if ((cancelable != null) && cancelable.isCanceled()) {
                        break;
                    }
                    String[] families = db.getAttribute().getFamilies().getFamiliesByCriterion(domain,
                            familyCriterions);
                    if (families != null) {
                        for (String family : families) {
                            if ((cancelable != null) && cancelable.isCanceled()) {
                                break;
                            }
                            String[] members = db.getAttribute().getMembers().getMembersByCriterion(domain, family,
                                    memberCriterions);
                            if (members != null) {
                                for (String member : members) {
                                    if ((cancelable != null) && cancelable.isCanceled()) {
                                        break;
                                    }
                                    String[] attrs = db.getAttribute().getNames().getAttributesByCriterion(domain,
                                            family, member, attributeCriterions);
                                    if (attrs != null) {
                                        for (String attr : attrs) {
                                            if ((cancelable != null) && cancelable.isCanceled()) {
                                                break;
                                            }
                                            attrList.add(attr);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            attributes = ExtractionUtils.getCleanedValues(attrList);
        }
        return attributes;
    }

    @Override
    public String[] getAttributesCompleteNames() throws ArchivingException {
        DataBaseManager db = getDataBaseManager();
        return db == null ? null : db.getAttribute().getNames().getAttributesCompleteNames();
    }

    @Override
    public String[] getAttributes(String domain, String family, String member) throws ArchivingException {
        DataBaseManager db = getDataBaseManager();
        return db == null ? null : db.getAttribute().getNames().getAttributes(domain, family, member);
    }

    @Override
    public int getType(String attributeName) throws ArchivingException {
        DataBaseManager db = getDataBaseManager();
        return db == null ? 0 : db.getAttribute().getAttDataType(attributeName);
    }

    @Override
    public int getWritable(String attributeName) throws ArchivingException {
        DataBaseManager db = getDataBaseManager();
        return db == null ? 0 : db.getAttribute().getAttDataWritable(attributeName);
    }

    @Override
    public int getDataFormat(String attributeName) throws ArchivingException {
        DataBaseManager db = getDataBaseManager();
        return db == null ? 0 : db.getAttribute().getAttDataFormat(attributeName);
    }

    @Override
    public String getFormat(String attributeName) throws ArchivingException {
        DataBaseManager db = getDataBaseManager();
        String format;
        if (db == null) {
            format = null;
        } else {
            IAdtAptAttributes adtAptAttributes = db.getAttribute();
            if (adtAptAttributes == null) {
                format = null;
            } else {
                format = adtAptAttributes.getProperties().getDisplayFormat(attributeName,
                        adtAptAttributes.isCaseSentitiveDatabase());
            }
        }
        return format;
    }

    @Override
    public String getArchiverForAttribute(String attributeName) throws ArchivingException {
        DataBaseManager db = getDataBaseManager();
        return db == null ? null : db.getMode().getArchiverForAttribute(attributeName);
    }

}
