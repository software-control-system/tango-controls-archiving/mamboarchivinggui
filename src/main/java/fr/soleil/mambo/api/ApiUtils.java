package fr.soleil.mambo.api;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.HdbTdbConnectionParameters;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.database.connection.DataBaseParameters.DataBaseType;
import fr.soleil.mambo.api.db.DbConnectionParameters;

/**
 * Utility class for api.
 * 
 * @author GIRARDOT
 */
public class ApiUtils implements ApiConstants {

    private static final String SEPARATOR = ": ";
    private static final String SEPARATOR_ERROR = ": error";
    private static final String INVALID = "Invalid ";

    private ApiUtils() {
        // hide constructor
    }

    /**
     * Extracts an <code>int</code> value from a {@link Map} supposed to contain its {@link String} representation,
     * throwing an {@link ArchivingException} if the value was not found or if a problem occurred.
     * 
     * @param accumulated The {@link Map}.
     * @param key The key to recover the value.
     * @param keyName The name that helps identifying the key in potential error message.
     * @return An <code>int</code>.
     * @throws ArchivingException If the value was not found or if a problem occurred.
     */
    public static int getInt(Map<String, String> accumulated, String key, String keyName) throws ArchivingException {
        String val = accumulated.get(key);
        int value;
        try {
            value = Integer.parseInt(val);
        } catch (Exception e) {
            throw new ArchivingException(INVALID + keyName);
        }
        return value;
    }

    /**
     * Extracts an <code>double</code> value from a {@link Map} supposed to contain its {@link String} representation,
     * throwing an {@link ArchivingException} if the value was not found or if a problem occurred.
     * 
     * @param accumulated The {@link Map}.
     * @param key The key to recover the value.
     * @param keyName The name that helps identifying the key in potential error message.
     * @return An <code>double</code>.
     * @throws ArchivingException If the value was not found or if a problem occurred.
     */
    public static double getDouble(Map<String, String> accumulated, String key, String keyName)
            throws ArchivingException {
        String val = accumulated.get(key);
        double value;
        try {
            value = Double.parseDouble(val);
        } catch (Exception e) {
            throw new ArchivingException(INVALID + keyName);
        }
        return value;
    }

    /**
     * Utility method to recover a <code>boolean</code> value from its {@link String} representation.
     * 
     * @param val The {@link String}.
     * @param defaultValue The default boolean value to return if a problem occurred while parsing the {@link String}.
     * @return A <code>boolean</code>.
     */
    public static boolean parseBoolean(String val, boolean defaultValue) {
        return val == null ? defaultValue : CometeConstants.TRUE.equalsIgnoreCase(val.trim());
    }

    /**
     * Utility method to recover a <code>boolean</code> value from its {@link String} representation.
     * 
     * @param val The {@link String}.
     * @return A <code>boolean</code>.
     */
    public static boolean parseBoolean(String val) {
        return parseBoolean(val, false);
    }

    public static String getProperty(String property, String env, String defaultValue) {
        String result;
        try {
            result = System.getProperty(property);
            if (result == null) {
                result = System.getenv(env);
                if (result == null) {
                    result = defaultValue;
                }
            }
        } catch (Exception e) {
            result = defaultValue;
        }
        return result;
    }

    /**
     * Parses a system property, or an environment variable if the property doesn't exist, as an <code>int</code> value.
     * 
     * @param property The property key.
     * @param env The environment variable
     * @param defaultValue The default <code>int</code> value to return (if property is not found or can't be parsed as
     *            an <code>int</code>).
     * @return An <code>int</code>.
     */
    public static int getIntProperty(String property, String env, int defaultValue) {
        int result;
        try {
            String sreadConf = getProperty(property, env, Integer.toString(defaultValue));
            result = Integer.parseInt(sreadConf);
        } catch (Exception e) {
            result = defaultValue;
        }
        return result;
    }

    /**
     * Parses a system property, or an environment variable if the property doesn't exist, as a <code>boolean</code>
     * value.
     * 
     * @param property The property key.
     * @param env The environment variable
     * @param defaultValue The default <code>boolean</code> value to return (if property is not found or can't be parsed
     *            as a <code>boolean</code>).
     * @return A <code>boolean</code>.
     */
    public static boolean getBooleanProperty(String property, String env, boolean defaultValue) {
        boolean result;
        try {
            String sreadConf = getProperty(property, env, Boolean.toString(defaultValue));
            result = parseBoolean(sreadConf, defaultValue);
        } catch (Exception e) {
            result = defaultValue;
        }
        return result;
    }

    /**
     * Applies a {@link String} value to a setter according to 2 getters. It will try to get it from 1st one.
     * If obtained value is <code>null</code> or empty after trim, it will obtain it from second one.
     * 
     * @param getter1 The 1st getter.
     * @param getter2 The 2nd getter.
     * @param setter The setter.
     */
    protected static void applyConfiguration(Supplier<String> getter1, Supplier<String> getter2,
            Consumer<String> setter) {
        String param = getter1.get();
        if ((param == null) || param.trim().isEmpty()) {
            param = getter2.get();
        }
        setter.accept(param);
    }

    /**
     * Returns an array containing all of the elements in given {@link Collection}, cleaning that {@link Collection}
     * after array extraction.
     * 
     * @param accumulator The {@link Collection}.
     * @return A {@link String} array.
     */
    public static String[] cleanToStringArray(Collection<String> accumulator) {
        String[] result;
        if (accumulator == null) {
            result = null;
        } else {
            result = accumulator.toArray(new String[accumulator.size()]);
            accumulator.clear();
        }
        return result;
    }

    /**
     * Puts the {@link String} array representation of an {@link Object} in a {@link Collection}.
     * 
     * @param accumulator The {@link Collection}.
     * @param toConvert The {@link Object}.
     * @return <code>accumulator</code>, in order to help chaining methods.
     */
    public static Collection<String> toStringArray(Collection<String> accumulator, Object toConvert) {
        if ((accumulator != null) && (toConvert != null)) {
            Arrays.stream(toConvert.getClass().getDeclaredFields()).forEach(f -> {
                try {
                    f.setAccessible(true);
                    Object value = f.get(toConvert);
                    if (value != null) {
                        accumulator.add(f.getName() + SEPARATOR + f.get(toConvert));
                    }
                } catch (IllegalAccessException e) {
                    accumulator.add(f.getName() + SEPARATOR_ERROR);
                }
            });
        }
        return accumulator;
    }

    /**
     * Recovers the {@link DataBaseParameters} to use for given database.
     * 
     * @param historic The {@link Boolean} that determines which database is concerned.
     * @return A {@link DataBaseParameters} object, never <code>null</code>.
     * @throws ArchivingException If a problem occurred while trying to read parameters from Tango.
     */
    public static DataBaseParameters getDataBaseParameters(Boolean historic) throws ArchivingException {
        final DataBaseParameters params = new DataBaseParameters();
        try {
            if (historic == null) {
                params.setParametersFromTango(TTS_DEVICE_CLASS);
                params.setHost(getProperty(TTS_HOST_PROPERTY, TTS_HOST_ENV, params.getHost()));
                params.setName(getProperty(TTS_NAME_PROPERTY, TTS_NAME_ENV, params.getName()));
                params.setDbType(DataBaseType.parseDataBaseType(
                        getProperty(TTS_DB_TYPE_PROPERTY, TTS_DB_TYPE_ENV, params.getDbType().toString())));
                params.setSchema(getProperty(TTS_SCHEMA_PROPERTY, TTS_SCHEMA_ENV, params.getSchema()));
                params.setMinPoolSize((short) getIntProperty(TTS_MIN_POOL_SIZE_PROPERTY, TTS_MIN_POOL_SIZE_ENV,
                        params.getMinPoolSize()));
                params.setMaxPoolSize((short) getIntProperty(TTS_MAX_POOL_SIZE_PROPERTY, TTS_MAX_POOL_SIZE_ENV,
                        params.getMaxPoolSize()));
                params.setInactivityTimeout(getIntProperty(TTS_INACTIVITY_TIMEOUT_PROPERTY, TTS_INACTIVITY_TIMEOUT_ENV,
                        params.getInactivityTimeout()));
                applyConfiguration(DbConnectionParameters::getUser, params::getUser, params::setUser);
                applyConfiguration(DbConnectionParameters::getPassword, params::getPassword, params::setPassword);
            } else if (historic.booleanValue()) {
                params.setParametersFromTango(ConfigConst.HDB_CLASS_DEVICE);
                params.setUser(HdbTdbConnectionParameters.getHDBUser());
                params.setPassword(HdbTdbConnectionParameters.getHDBPassword());
                params.setName(HdbTdbConnectionParameters.getHDbName());
                params.setSchema(HdbTdbConnectionParameters.getHDbSchema());
            } else {
                // prefix = TDB;
                params.setParametersFromTango(ConfigConst.TDB_CLASS_DEVICE);
                params.setUser(HdbTdbConnectionParameters.getTDBUser());
                params.setPassword(HdbTdbConnectionParameters.getTDBPassword());
                params.setName(HdbTdbConnectionParameters.getTDbName());
                params.setSchema(HdbTdbConnectionParameters.getTDbSchema());
            }
        } catch (DevFailed df) {
            throw ArchivingException.toArchivingException(df);
        }
        return params;
    }

    /**
     * Splits a {@link String} into a key and a value, and puts them in a {@link Map}.
     * 
     * @param val The {@link String}.
     * @param accumulated The {@link Map}
     * @return <code>accumulated</code>, in order to help chaining methods.
     */
    private static Map<String, String> split(String val, Map<String, String> accumulated) {
        if (val != null) {
            int index = val.indexOf(SEPARATOR);
            if (index > -1) {
                index += SEPARATOR.length();
                accumulated.put(val.substring(0, index), val.substring(index));
            }
        }
        return accumulated;
    }

    /**
     * Extracts key-value pairs from a {@link String} array and puts them in a {@link Map}.
     * 
     * @param values The {@link String} array.
     * @param accumulated The {@link Map}.
     * @return <code>accumulated</code>, in order to help chaining methods.
     */
    public static Map<String, String> split(String[] values, Map<String, String> accumulated) {
        if ((values != null) && (values.length > 0) && (accumulated != null)) {
            for (String val : values) {
                split(val, accumulated);
            }
        }
        return accumulated;
    }

    /**
     * Extracts key-value pairs from a {@link String} {@link Collection} and puts them in a {@link Map}.
     * 
     * @param values The {@link Collection}.
     * @param accumulated The {@link Map}.
     * @return <code>accumulated</code>, in order to help chaining methods.
     */
    public static Map<String, String> split(Collection<String> values, Map<String, String> accumulated) {
        if ((values != null) && (!values.isEmpty()) && (accumulated != null)) {
            for (String val : values) {
                split(val, accumulated);
            }
        }
        return accumulated;
    }

    /**
     * Returns the names of some {@link Attribute}s.
     * 
     * @param defaultValue The default value to return when <code>attributes</code> is <code>null</code>.
     * @param attributes The {@link Attribute}s.
     * @return A {@link String} array: the attribute names.
     */
    public static String[] getAttributeNames(String[] defaultValue, Attribute... attributes) {
        String[] names;
        if (attributes == null) {
            names = defaultValue;
        } else {
            names = new String[attributes.length];
            int index = 0;
            for (Attribute attribute : attributes) {
                names[index++] = attribute == null ? null : attribute.getCompleteName();
            }
        }
        return names;
    }

}
