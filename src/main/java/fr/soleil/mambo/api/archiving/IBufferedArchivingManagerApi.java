package fr.soleil.mambo.api.archiving;

/**
 * An {@link IArchivingManagerApi} that buffers its archiving information.
 * 
 * @author GIRARDOT
 */
public interface IBufferedArchivingManagerApi extends IArchivingManagerApi {

    /**
     * Cleans all buffers.
     */
    public void cleanBuffer();

    /**
     * Cleans the buffers for given attributes.
     * 
     * @param attributes The attribute names.
     */
    public void cleanBuffer(String... attributes);

}
