package fr.soleil.mambo.api.archiving;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.mambo.api.db.IDataBaseAcess;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;

// alternative for fr.soleil.archiving.hdbtdb.api.manager.IArchivingManagerApiRef, compatible with TTS too
public interface IArchivingManagerApi {

    int getArchiverListSize();

    String getStatus(String attributeName) throws ArchivingException;

    void archivingStart(ArchivingConfigurationAttribute... attributes) throws ArchivingException;

    void archivingStop(String... attributeNameList) throws ArchivingException;

    boolean isArchived(String attributeName) throws ArchivingException;

    IDataBaseAcess getDataBase();

    boolean isFacility();

    String[] getExportedArchivers();

    public String[] getArchivingMode(String attributeName) throws ArchivingException;

    void archivingConfigure() throws ArchivingException;

    void archivingConfigureWithoutArchiverListInit() throws ArchivingException;

}
