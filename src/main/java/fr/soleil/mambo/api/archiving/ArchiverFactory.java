package fr.soleil.mambo.api.archiving;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.Archiver;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.map.ConcurrentWeakHashMap;

public class ArchiverFactory {

    private static final ConcurrentMap<Archiver, LegacyArchiver> LEGACY_ARCHIVERS = new ConcurrentWeakHashMap<>();
    private static final ConcurrentMap<String, TTSArchiver> TTS_ARCHIVERS = new ConcurrentHashMap<>();

    private ArchiverFactory() {
        // hide constructor
    }

    protected static LegacyArchiver getLegacyArchiver(Archiver archiver) {
        LegacyArchiver result;
        if (archiver == null) {
            result = null;
        } else {
            result = LEGACY_ARCHIVERS.get(archiver);
            if (result == null) {
                result = new LegacyArchiver(archiver);
                LegacyArchiver tmp = LEGACY_ARCHIVERS.putIfAbsent(archiver, result);
                if (tmp != null) {
                    result = tmp;
                }
            }
        }
        return result;
    }

    protected static TTSArchiver getTTSArchiver(String archiverName) {
        TTSArchiver result;
        if ((archiverName == null) || archiverName.trim().isEmpty()) {
            result = null;
        } else {
            result = TTS_ARCHIVERS.get(archiverName);
            if (result == null) {
                result = new TTSArchiver(archiverName);
                TTSArchiver tmp = TTS_ARCHIVERS.putIfAbsent(archiverName, result);
                if (tmp != null) {
                    result = tmp;
                }
            }
        }
        return result;
    }

    public static IArchiver getArchiver(String archiverName, Boolean historic) throws ArchivingException {
        final IArchiver ret;
        if (archiverName == null || archiverName.trim().isEmpty()) {
            ret = null;
        } else if (historic == null) {
            TTSArchiver tts = getTTSArchiver(archiverName);
            String[] exported = TTSArchivingManagerApi.getRunningArchivers();
            boolean running = false;
            for (String archiver : exported) {
                if (ObjectUtils.sameObject(archiverName, archiver)) {
                    running = true;
                    break;
                }
            }
            tts.setExported(running);
            tts.setDedicated(TTSArchivingManagerApi.isDedicated(archiverName));
            tts.setReservedAttributes(TTSArchivingManagerApi.getReservedAttributes(archiverName));
            ret = tts;
        } else {
            ret = ArchiverFactory.getLegacyArchiver(Archiver.findArchiver(archiverName, historic.booleanValue()));
        }
        return ret;
    }

    public static Map<String, IArchiver> getAttributesToDedicatedArchiver(Boolean historic) {
        Map<String, IArchiver> map = new HashMap<>();
        if (historic == null) {
            for (TTSArchiver archiver : TTS_ARCHIVERS.values()) {
                if (archiver != null) {
                    String[] reserved = archiver.getReservedAttributes();
                    for (String attribute : reserved) {
                        if (attribute != null) {
                            map.put(attribute, archiver);
                        }
                    }
                }
            }
        } else {
            Map<String, Archiver> dedicated = Archiver.getAttributesToDedicatedArchiver(historic.booleanValue());
            if (dedicated != null) {
                for (Entry<String, Archiver> entry : dedicated.entrySet()) {
                    String attribute = entry.getKey();
                    Archiver archiver = entry.getValue();
                    if ((attribute != null) && (archiver != null)) {
                        map.put(attribute, getLegacyArchiver(archiver));
                    }
                }
            }
        }
        return map;
    }

}
