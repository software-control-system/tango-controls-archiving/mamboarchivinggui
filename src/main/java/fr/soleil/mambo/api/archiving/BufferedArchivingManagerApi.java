package fr.soleil.mambo.api.archiving;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.api.ApiUtils;
import fr.soleil.mambo.api.ArchivingFunction;
import fr.soleil.mambo.api.db.IDataBaseAcess;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;

/**
 * Implementation of {@link IBufferedArchivingManagerApi}.<br />
 * It uses another {@link IArchivingManagerApi} to access database.
 * 
 * @author GIRARDOT
 */
public class BufferedArchivingManagerApi implements IBufferedArchivingManagerApi, ApiConstants {

    private static final Timer TIMER = new Timer("Archiving buffer auto cleaner");
    // TANGOARCH-838: 8 hours (28800s) buffer lifetime
    public static final int DEFAULT_LIFETIME = 28800;
    // TANGOARCH-838: 85% of maximum memory before auto cleaning buffer
    public static final int DEFAULT_MAX_MEMORY_PERCENTAGE = 85;
    // effective buffer lifetime
    private static final long LIFETIME;
    // effective maximum memory proportion before auto cleaning buffer
    private static final double MAX_MEMORY_PROPORTION;
    static {
        // TANGOARCH-838: recover expected lifetime and maximum memory percentage
        int lifetime = ApiUtils.getIntProperty(ATTRIBUTE_INFO_BUFFER_LIFETIME_PROPERTY,
                ATTRIBUTE_INFO_BUFFER_LIFETIME_ENV, DEFAULT_LIFETIME);
        if (lifetime < 1) {
            lifetime = DEFAULT_LIFETIME;
        }
        LIFETIME = lifetime * 1000;
        int maxMemoryPercentage = ApiUtils.getIntProperty(BUFFER_AUTOCLEAN_MEMORY_PERCENTAGE_PROPERTY,
                BUFFER_AUTOCLEAN_MEMORY_PERCENTAGE_ENV, DEFAULT_MAX_MEMORY_PERCENTAGE);
        if (maxMemoryPercentage < 1 || maxMemoryPercentage > 99) {
            maxMemoryPercentage = DEFAULT_MAX_MEMORY_PERCENTAGE;
        }
        MAX_MEMORY_PROPORTION = maxMemoryPercentage / 100.0d;
    }

    private final IArchivingManagerApi api;
    private final Map<String, ArchivingInfo> archivedMap;
    private final Map<String, ArchivingModeInfo> modeMap;

    /**
     * Constructs a new {@link BufferedArchivingManagerApi}.
     * 
     * @param api The {@link IArchivingManagerApi} that may access database.
     */
    public BufferedArchivingManagerApi(IArchivingManagerApi api) {
        this.api = api;
        archivedMap = new ConcurrentHashMap<>();
        modeMap = new ConcurrentHashMap<>();
        // TANGOARCH-838: Try to reduce memory usage by regularly cleaning maps for too old values,
        // or when there is not enough memory left
        TIMER.scheduleAtFixedRate(generateTimerTask(this::checkValidities, archivedMap), 0, 1000);
        TIMER.scheduleAtFixedRate(generateTimerTask(this::checkValidities, modeMap), 0, 1000);
        TIMER.scheduleAtFixedRate(generateTimerTask(this::checkMemory, archivedMap), 100, 1000);
        TIMER.scheduleAtFixedRate(generateTimerTask(this::checkMemory, modeMap), 100, 1000);
    }

    /**
     * Creates a {@link TimerTask} for a method that takes an argument.
     * 
     * @param <M> The type of argument of the method.
     * @param method The method.
     * @param value The argument to transmit to the method.
     * 
     * @return A {@link TimerTask}.
     */
    protected <M> TimerTask generateTimerTask(Consumer<M> method, M value) {
        return new TimerTask() {
            @Override
            public void run() {
                method.accept(value);
            }
        };
    }

    /**
     * Cleans too old entries in a {@link Map} that has some {@link AInfo} as values.
     * 
     * @param <I> The type of {@link AInfo}.
     * @param infoMap The {@link Map}.
     */
    protected <I extends AInfo<?>> void checkValidities(Map<String, I> infoMap) {
        if (infoMap != null) {
            long time = System.currentTimeMillis();
            infoMap.entrySet().removeIf(entry -> (entry != null) && (entry.getValue() != null)
                    && time - entry.getValue().getExtractionDate() > LIFETIME);
        }
    }

    /**
     * Cleans a {@link Map} when there is not enough memory left.
     * 
     * @param infoMap The {@link Map} to clean.
     */
    protected void checkMemory(Map<?, ?> infoMap) {
        if (infoMap != null) {
            MemoryMXBean mxBean = ManagementFactory.getMemoryMXBean();
            MemoryUsage usage = mxBean.getHeapMemoryUsage();
            long used = usage.getUsed();
            long max = usage.getMax();
            if (used >= max * MAX_MEMORY_PROPORTION) {
                infoMap.clear();
            }
        }
    }

    /**
     * Register in {@link AInfo} in a {@link Map}, if not already present, and returns the effectively registered
     * {@link AInfo}.
     * 
     * @param <I> The type of {@link AInfo}.
     * @param name The attribute name.
     * @param info The {@link AInfo} to register.
     * @param infoMap The {@link Map}.
     * @return The effectively registered {@link AInfo}.
     */
    protected <I extends AInfo<?>> I addInfo(String name, I info, Map<String, I> infoMap) {
        I result = info;
        if ((info != null) && (name != null) && (infoMap != null)) {
            I tmp = infoMap.putIfAbsent(name, info);
            if (tmp != null) {
                result = tmp;
            }
        }
        return result;
    }

    /**
     * Returns some archiving information for an attribute, reading it from buffer or database, depending on its
     * lifetime.
     * 
     * @param <O> The type of archiving information.
     * @param <I> The type of {@link AInfo} that may store this archiving information.
     * @param attributeName The attribute name.
     * @param infoMap The {@link Map} that stores the {@link AInfo}.
     * @param infoClass The {@link AInfo} class.
     * @param dataClass The archiving information class.
     * @param method The method that reads information in database.
     * @param defaultValue The default value to return if any parameter is invalid.
     * @return The archiving information.
     * @throws ArchivingException If a problem occurred while reading information in database.
     */
    protected <O, I extends AInfo<O>> O getArchivingData(String attributeName, Map<String, I> infoMap,
            Class<I> infoClass, Class<O> dataClass, ArchivingFunction<String, O> method, O defaultValue)
            throws ArchivingException {
        O archivingData;
        if ((method == null) || (infoMap == null) || (attributeName == null) || attributeName.trim().isEmpty()) {
            archivingData = defaultValue;
        } else {
            String name = attributeName.toLowerCase();
            I info = infoMap.get(name);
            try {
                if ((info == null) || System.currentTimeMillis() - info.getExtractionDate() > LIFETIME) {
                    Constructor<I> constructor = infoClass.getConstructor(dataClass);
                    if (constructor == null) {
                        info = null;
                    } else {
                        info = addInfo(name, constructor.newInstance(method.apply(name)), infoMap);
                    }
                }
            } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
                    | IllegalArgumentException | InvocationTargetException e) {
                info = null;
            }
            if (info == null) {
                archivingData = defaultValue;
            } else {
                archivingData = info.getArchivingData();
                if (archivingData == null) {
                    archivingData = defaultValue;
                }
            }
        }
        return archivingData;
    }

    @Override
    public int getArchiverListSize() {
        return api == null ? 0 : api.getArchiverListSize();
    }

    @Override
    public String getStatus(String attributeName) throws ArchivingException {
        return api == null ? null : api.getStatus(attributeName);
    }

    @Override
    public void archivingStart(ArchivingConfigurationAttribute... attributes) throws ArchivingException {
        if (api != null) {
            try {
                api.archivingStart(attributes);
            } finally {
                cleanBuffer();
            }
        }
    }

    @Override
    public void archivingStop(String... attributeNameList) throws ArchivingException {
        if (api != null) {
            try {
                api.archivingStop(attributeNameList);
            } finally {
                cleanBuffer();
            }
        }
    }

    @Override
    public boolean isArchived(String attributeName) throws ArchivingException {
        return getArchivingData(attributeName, archivedMap, ArchivingInfo.class, Boolean.TYPE,
                api == null ? null : api::isArchived, Boolean.FALSE);
    }

    @Override
    public IDataBaseAcess getDataBase() {
        return api == null ? null : api.getDataBase();
    }

    @Override
    public boolean isFacility() {
        return api == null ? false : api.isFacility();
    }

    @Override
    public String[] getExportedArchivers() {
        return api == null ? EMPTY : api.getExportedArchivers();
    }

    @Override
    public String[] getArchivingMode(String attributeName) throws ArchivingException {
        return getArchivingData(attributeName, modeMap, ArchivingModeInfo.class, String[].class,
                api == null ? null : api::getArchivingMode, EMPTY);
    }

    @Override
    public void archivingConfigure() throws ArchivingException {
        if (api != null) {
            api.archivingConfigure();
        }
    }

    @Override
    public void archivingConfigureWithoutArchiverListInit() throws ArchivingException {
        if (api != null) {
            api.archivingConfigureWithoutArchiverListInit();
        }
    }

    @Override
    public void cleanBuffer() {
        archivedMap.clear();
        modeMap.clear();
    }

    @Override
    public void cleanBuffer(String... attributes) {
        if (attributes != null) {
            for (String attribute : attributes) {
                if (attribute != null) {
                    String name = attribute.toLowerCase();
                    archivedMap.remove(name);
                    modeMap.remove(name);
                }
            }
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Abstract class for some archiving information concerning an attribute.
     * 
     * @author GIRARDOT
     */
    private static abstract class AInfo<T> {

        private final long extractionDate;
        private final T archivingData;

        public AInfo(T archivingData) {
            this.archivingData = archivingData;
            this.extractionDate = System.currentTimeMillis();
        }

        /**
         * Returns when the information was obtained.
         * 
         * @return A <code>boolean</code>.
         */
        public long getExtractionDate() {
            return extractionDate;
        }

        /**
         * Returns the archiving information concerning the attribute.
         * 
         * @return The archiving information concerning the attribute.
         */
        public T getArchivingData() {
            return archivingData;
        }

    }

    /**
     * A class that stores information about the archiving of an attribute, and when this information was obtained.
     * 
     * @author GIRARDOT
     */
    private static class ArchivingInfo extends AInfo<Boolean> {

        public ArchivingInfo(boolean archived) {
            super(Boolean.valueOf(archived));
        }

    }

    /**
     * A class that stores information about the archiving mode of an attribute, and when this information was
     * obtained.
     * 
     * @author GIRARDOT
     */
    private static class ArchivingModeInfo extends AInfo<String[]> {

        public ArchivingModeInfo(String... archivingMode) {
            super(archivingMode);
        }

    }

}
