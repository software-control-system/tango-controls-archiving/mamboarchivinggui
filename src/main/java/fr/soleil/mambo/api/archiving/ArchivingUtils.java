package fr.soleil.mambo.api.archiving;

import java.util.Collection;
import java.util.Map;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.mode.EventMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRoot;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ShortPeriodAttributesManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.api.ApiUtils;
import fr.soleil.tango.archiving.config.InsertionModes;

/**
 * Utility class dedicated to archiving.
 * 
 * @author GIRARDOT
 */
public class ArchivingUtils implements ApiConstants {

    private static final String ATTRIBUTE_NAME_KEY = "attributeName: ";
    private static final String ATTRIBUTE_ID_KEY = "attributeID: ";
    private static final String IS_PERIODIC_KEY = "isPeriodic: ";
    private static final String PERIOD_PERIODIC_KEY = "periodPeriodic: ";
    private static final String IS_ABSOLUTE_KEY = "isAbsolute: ";
    private static final String IS_SLOW_DRIFT_ABSOLUTE_KEY = "isSlowDriftAbsolute: ";
    private static final String PERIOD_ABSOLUTE_KEY = "periodAbsolute: ";
    private static final String DECREASE_DELTA_ABSOLUTE_KEY = "decreaseDeltaAbsolute: ";
    private static final String INCREASE_DELTA_ABSOLUTE_KEY = "increaseDeltaAbsolute: ";
    private static final String IS_RELATIVE_KEY = "isRelative: ";
    private static final String PERIOD_RELATIVE_KEY = "periodRelative: ";
    private static final String IS_SLOW_DRIFT_RELATIVE_KEY = "isSlowDriftRelative: ";
    private static final String DECREASE_PERCENT_RELATIVE_KEY = "decreasePercentRelative: ";
    private static final String INCREASE_PERCENT_RELATIVE_KEY = "increasePercentRelative: ";
    private static final String IS_THRESHOLD_KEY = "isThreshold: ";
    private static final String PERIOD_THRESHOLD_KEY = "periodThreshold: ";
    private static final String MIN_THRESHOLD_VALUE_KEY = "minThresholdValue: ";
    private static final String MAX_THRESHOLD_VALUE_KEY = "maxThresholdValue: ";
    private static final String IS_DIFFERENCE_KEY = "isDifference: ";
    private static final String PERIOD_DIFFERENCE_KEY = "periodDifference: ";
    private static final String IS_EVENT_KEY = "isEvent: ";

    private static final String PERIOD = "period";
    private static final String ABSOLUTE_PERIOD = "absolute period";
    private static final String ABSOLUTE_LOWER_LIMIT = "absolute lower limit";
    private static final String ABSOLUTE_UPPER_LIMIT = "absolute upper limit";
    private static final String RELATIVE_PERIOD = "relative period";
    private static final String LOWER_PERCENT_LIMIT = "lower % limit";
    private static final String UPPER_PERCENT_LIMIT = "upper % limit";
    private static final String THRESHOLD_PERIOD = "threshold period";
    private static final String THRESHOLD_LOWER_LIMIT = "threshold lower limit";
    private static final String THRESHOLD_UPPER_LIMIT = "threshold upper limit";
    private static final String DIFFERENCE_PERIOD = "difference period";
    private static final String ATTRIBUTE_ID = "attribute ID";
    private static final String PERIODIC_MODE_MISSING = "Periodic mode missing!";
    private static final String SHORT_ARCHIVING_PERIOD_ERROR = "Archiving period too low! (%s's period must be greater or equal to %s ms)";
    private static final String ARCHIVING_PERIOD_ERROR = "Archiving period too low! (%s<%s ms)";

    private static final int HDB_PERIOD_MIN_VALUE = 10000; // ms
    private static final int TDB_PERIOD_MIN_VALUE = 100; // ms
    private static final int TTS_PERIOD_MIN_VALUE = TDB_PERIOD_MIN_VALUE;

    private ArchivingUtils() {
        // hide constructor
    }

    /**
     * Puts the {@link String} array representation of an {@link InsertionModes} in a {@link Collection}.
     * 
     * @param accumulator The {@link Collection}.
     * @param modes The {@link InsertionModes}.
     * @return <code>accumulator</code>, in order to help chaining methods.
     */
    public static Collection<String> insertionModesToStringArray(Collection<String> accumulator, InsertionModes modes) {
        return ApiUtils.toStringArray(accumulator, modes);
    }

    /**
     * Puts the {@link String} array representation of a {@link Mode} in a {@link Collection}.
     * 
     * @param accumulator The {@link Collection}.
     * @param mode The {@link Mode}.
     * @return <code>accumulator</code>, in order to help chaining methods.
     */
    public static Collection<String> modeToStringArray(Collection<String> accumulator, Mode mode) {
        if ((accumulator != null) && (mode != null)) {
            ModePeriode period = mode.getModeP();
            if (period == null) {
                accumulator.add(IS_PERIODIC_KEY + false);
            } else {
                accumulator.add(IS_PERIODIC_KEY + true);
                accumulator.add(PERIOD_PERIODIC_KEY + period.getPeriod());
            }
            ModeAbsolu abs = mode.getModeA();
            if (abs == null) {
                accumulator.add(IS_ABSOLUTE_KEY + false);
            } else {
                accumulator.add(IS_ABSOLUTE_KEY + true);
                accumulator.add(IS_SLOW_DRIFT_ABSOLUTE_KEY + abs.isSlow_drift());
                accumulator.add(PERIOD_ABSOLUTE_KEY + abs.getPeriod());
                accumulator.add(DECREASE_DELTA_ABSOLUTE_KEY + abs.getValInf());
                accumulator.add(INCREASE_DELTA_ABSOLUTE_KEY + abs.getValSup());
            }
            ModeRelatif rel = mode.getModeR();
            if (rel == null) {
                accumulator.add(IS_RELATIVE_KEY + false);
            } else {
                accumulator.add(IS_RELATIVE_KEY + true);
                accumulator.add(PERIOD_RELATIVE_KEY + rel.getPeriod());
                accumulator.add(IS_SLOW_DRIFT_RELATIVE_KEY + rel.isSlow_drift());
                accumulator.add(DECREASE_PERCENT_RELATIVE_KEY + rel.getPercentInf());
                accumulator.add(INCREASE_PERCENT_RELATIVE_KEY + rel.getPercentSup());
            }
            ModeSeuil thr = mode.getModeT();
            if (thr == null) {
                accumulator.add(IS_THRESHOLD_KEY + false);
            } else {
                accumulator.add(IS_THRESHOLD_KEY + true);
                accumulator.add(PERIOD_THRESHOLD_KEY + thr.getPeriod());
                accumulator.add(MIN_THRESHOLD_VALUE_KEY + thr.getThresholdInf());
                accumulator.add(MAX_THRESHOLD_VALUE_KEY + thr.getThresholdSup());
            }
            ModeDifference diff = mode.getModeD();
            if (diff == null) {
                accumulator.add(IS_DIFFERENCE_KEY + false);
            } else {
                accumulator.add(IS_DIFFERENCE_KEY + true);
                accumulator.add(PERIOD_DIFFERENCE_KEY + diff.getPeriod());
            }
            accumulator.add(IS_EVENT_KEY + (mode.getEventMode() != null));
        } // end if ((accumulator != null) && (mode != null))
        return accumulator;
    }

    /**
     * Recovers a {@link Mode} from its {@link String} representation stored in a {@link Map}.
     * 
     * @param accumulated The {@link Map}.
     * @return A {@link Mode}. May be <code>null</code>.
     * @throws ArchivingException If there was a problem recovering informations to build the {@link Mode}.
     */
    public static Mode toMode(Map<String, String> accumulated) throws ArchivingException {
        Mode mode;
        if ((accumulated == null) || (accumulated.isEmpty())) {
            mode = null;
        } else {
            mode = new Mode();
            if (ApiUtils.parseBoolean(accumulated.get(IS_PERIODIC_KEY))) {
                ModePeriode period = new ModePeriode();
                period.setPeriod(ApiUtils.getInt(accumulated, PERIOD_PERIODIC_KEY, PERIOD));
                mode.setModeP(period);
            }
            if (ApiUtils.parseBoolean(accumulated.get(IS_ABSOLUTE_KEY))) {
                ModeAbsolu abs = new ModeAbsolu();
                abs.setPeriod(ApiUtils.getInt(accumulated, PERIOD_ABSOLUTE_KEY, ABSOLUTE_PERIOD));
                abs.setSlow_drift(ApiUtils.parseBoolean(accumulated.get(IS_SLOW_DRIFT_ABSOLUTE_KEY)));
                abs.setValInf(ApiUtils.getDouble(accumulated, DECREASE_DELTA_ABSOLUTE_KEY, ABSOLUTE_LOWER_LIMIT));
                abs.setValSup(ApiUtils.getDouble(accumulated, INCREASE_DELTA_ABSOLUTE_KEY, ABSOLUTE_UPPER_LIMIT));
                mode.setModeA(abs);
            }
            if (ApiUtils.parseBoolean(accumulated.get(IS_RELATIVE_KEY))) {
                ModeRelatif rel = new ModeRelatif();
                rel.setPeriod(ApiUtils.getInt(accumulated, PERIOD_RELATIVE_KEY, RELATIVE_PERIOD));
                rel.setSlow_drift(ApiUtils.parseBoolean(accumulated.get(IS_SLOW_DRIFT_RELATIVE_KEY)));
                rel.setPercentInf(ApiUtils.getDouble(accumulated, DECREASE_PERCENT_RELATIVE_KEY, LOWER_PERCENT_LIMIT));
                rel.setPercentSup(ApiUtils.getDouble(accumulated, INCREASE_PERCENT_RELATIVE_KEY, UPPER_PERCENT_LIMIT));
                mode.setModeR(rel);
            }
            if (ApiUtils.parseBoolean(accumulated.get(IS_THRESHOLD_KEY))) {
                ModeSeuil thr = new ModeSeuil();
                thr.setPeriod(ApiUtils.getInt(accumulated, PERIOD_THRESHOLD_KEY, THRESHOLD_PERIOD));
                thr.setThresholdInf(ApiUtils.getDouble(accumulated, MIN_THRESHOLD_VALUE_KEY, THRESHOLD_LOWER_LIMIT));
                thr.setThresholdSup(ApiUtils.getDouble(accumulated, MAX_THRESHOLD_VALUE_KEY, THRESHOLD_UPPER_LIMIT));
                mode.setModeT(thr);
            }
            if (ApiUtils.parseBoolean(accumulated.get(IS_DIFFERENCE_KEY))) {
                ModeDifference diff = new ModeDifference();
                diff.setPeriod(ApiUtils.getInt(accumulated, PERIOD_DIFFERENCE_KEY, DIFFERENCE_PERIOD));
                mode.setModeD(diff);
            }
            if (ApiUtils.parseBoolean(accumulated.get(IS_EVENT_KEY))) {
                mode.setEventMode(new EventMode());
            }
        } // end if ((accumulated == null) || (accumulated.isEmpty())) ... else
        return mode;
    }

    /**
     * Recovers an {@link InsertionModes} from its {@link String} representation stored in a {@link Map}.
     * 
     * @param accumulated The {@link Map}.
     * @return An {@link InsertionModes}. May be <code>null</code>.
     * @throws ArchivingException If there was a problem recovering informations to build the {@link InsertionModes}.
     */
    public static InsertionModes toInsertionModes(Map<String, String> accumulated) throws ArchivingException {
        InsertionModes modes;
        if ((accumulated == null) || (accumulated.isEmpty())) {
            modes = null;
        } else {
            modes = new InsertionModes();
            String attributeName = accumulated.get(ATTRIBUTE_NAME_KEY);
            modes.setAttributeName(attributeName == null ? ObjectUtils.EMPTY_STRING : attributeName);
            modes.setAttributeID(ApiUtils.getInt(accumulated, ATTRIBUTE_ID_KEY, ATTRIBUTE_ID));
            if (ApiUtils.parseBoolean(accumulated.get(IS_PERIODIC_KEY))) {
                modes.setPeriodic(true);
                modes.setPeriodPeriodic(ApiUtils.getInt(accumulated, PERIOD_PERIODIC_KEY, PERIOD));
            } else {
                modes.setPeriodic(false);
            }
            if (ApiUtils.parseBoolean(accumulated.get(IS_ABSOLUTE_KEY))) {
                modes.setAbsolute(true);
                modes.setPeriodAbsolute(ApiUtils.getInt(accumulated, PERIOD_ABSOLUTE_KEY, ABSOLUTE_PERIOD));
                modes.setSlowDriftAbsolute(ApiUtils.parseBoolean(accumulated.get(IS_SLOW_DRIFT_ABSOLUTE_KEY)));
                modes.setDecreaseDeltaAbsolute(
                        ApiUtils.getDouble(accumulated, DECREASE_DELTA_ABSOLUTE_KEY, ABSOLUTE_LOWER_LIMIT));
                modes.setIncreaseDeltaAbsolute(
                        ApiUtils.getDouble(accumulated, INCREASE_DELTA_ABSOLUTE_KEY, ABSOLUTE_UPPER_LIMIT));
            } else {
                modes.setAbsolute(false);
            }
            if (ApiUtils.parseBoolean(accumulated.get(IS_RELATIVE_KEY))) {
                modes.setRelative(true);
                modes.setPeriodRelative(ApiUtils.getInt(accumulated, PERIOD_RELATIVE_KEY, RELATIVE_PERIOD));
                modes.setSlowDriftRelative(ApiUtils.parseBoolean(accumulated.get(IS_SLOW_DRIFT_RELATIVE_KEY)));
                modes.setDecreasePercentRelative(
                        ApiUtils.getDouble(accumulated, DECREASE_PERCENT_RELATIVE_KEY, LOWER_PERCENT_LIMIT));
                modes.setIncreasePercentRelative(
                        ApiUtils.getDouble(accumulated, INCREASE_PERCENT_RELATIVE_KEY, UPPER_PERCENT_LIMIT));
            } else {
                modes.setRelative(false);
            }
            if (ApiUtils.parseBoolean(accumulated.get(IS_THRESHOLD_KEY))) {
                modes.setThreshold(true);
                modes.setPeriodThreshold(ApiUtils.getInt(accumulated, PERIOD_THRESHOLD_KEY, THRESHOLD_PERIOD));
                modes.setMinThresholdValue(
                        ApiUtils.getDouble(accumulated, MIN_THRESHOLD_VALUE_KEY, THRESHOLD_LOWER_LIMIT));
                modes.setMaxThresholdValue(
                        ApiUtils.getDouble(accumulated, MAX_THRESHOLD_VALUE_KEY, THRESHOLD_UPPER_LIMIT));
            } else {
                modes.setThreshold(false);
            }
            if (ApiUtils.parseBoolean(accumulated.get(IS_DIFFERENCE_KEY))) {
                modes.setDifference(true);
                modes.setPeriodDifference(ApiUtils.getInt(accumulated, PERIOD_DIFFERENCE_KEY, DIFFERENCE_PERIOD));
            } else {
                modes.setDifference(false);
            }
            modes.setEvent(ApiUtils.parseBoolean(accumulated.get(IS_EVENT_KEY)));
        } // end if ((accumulated == null) || (accumulated.isEmpty())) ... else
        return modes;
    }

    /**
     * Checks the validity of a {@link ModeRoot} for an attribute.
     * 
     * @param mode The {@link ModeRoot}.
     * @param attributeName The attribute.
     * @param historic The {@link Boolean} that identifies which database is concerned.
     * @throws ArchivingException If the {@link ModeRoot} is invalid.
     */
    private static void doCheck(final ModeRoot mode, final String attributeName, final Boolean historic)
            throws ArchivingException {
        if (mode != null) {
            int minVal;
            if (mode instanceof EventMode) {
                // there no minimum period for event mode
                minVal = 0;
            } else if (historic == null) {
                minVal = TTS_PERIOD_MIN_VALUE;
            } else if (historic.booleanValue()) {
                minVal = HDB_PERIOD_MIN_VALUE;
            } else {
                minVal = TDB_PERIOD_MIN_VALUE;
            }
            if (mode.getPeriod() < minVal) {
                // allowed short period in HDB only for previously declared attributes
                if ((attributeName != null) && ShortPeriodAttributesManager.isShortPeriodAttribute(attributeName)
                        && (historic != null) && historic.booleanValue()) {
                    final int attributePeriod = ShortPeriodAttributesManager
                            .getPeriodFromShortAttributeName(attributeName.trim());
                    if (mode.getPeriod() / 1000 < attributePeriod) {
                        throw new ArchivingException(
                                String.format(SHORT_ARCHIVING_PERIOD_ERROR, attributeName, attributePeriod));
                    }
                } else {
                    throw new ArchivingException(String.format(ARCHIVING_PERIOD_ERROR, mode.getPeriod(), minVal));
                }
            }
        }
    }

    /**
     * Checks whether a {@link Mode} is correctly set for given attribute, throwing an {@link ArchivingException} if
     * not.
     * 
     * @param mode The {@link Mode}. Can not be <code>null</code>.
     * @param attributeName The attribute name.
     * @param historic The {@link Boolean} that indicates whether to archive on HDB, TDB or TTS.
     * @return A <code>boolean</code>
     * @throws ArchivingException If the {@link Mode} is not correctly set.
     */
    public static void checkMode(Mode mode, String attributeName, Boolean historic) throws ArchivingException {
        if ((mode.getEventMode() == null) && mode.getModeP() == null) {
            // for event mode, periodic mode is not mandatory
            throw new ArchivingException(PERIODIC_MODE_MISSING);
        }
        doCheck(mode.getModeP(), attributeName, historic);
        doCheck(mode.getModeA(), attributeName, historic);
        doCheck(mode.getModeR(), attributeName, historic);
        doCheck(mode.getModeT(), attributeName, historic);
        doCheck(mode.getModeC(), attributeName, historic);
        doCheck(mode.getModeD(), attributeName, historic);
        doCheck(mode.getEventMode(), attributeName, historic);
    }

}
