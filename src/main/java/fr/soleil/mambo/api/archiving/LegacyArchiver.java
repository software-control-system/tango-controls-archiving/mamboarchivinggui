package fr.soleil.mambo.api.archiving;

import java.awt.Color;
import java.lang.ref.WeakReference;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.Archiver;
import fr.soleil.lib.project.ObjectUtils;

public class LegacyArchiver implements IArchiver {

    private final WeakReference<Archiver> archiverRef;

    protected LegacyArchiver(Archiver archiver) {
        this.archiverRef = archiver == null ? null : new WeakReference<>(archiver);
    }

    @Override
    public Color getAssociationColor(String attributeName) throws ArchivingException {
        Archiver archiver = ObjectUtils.recoverObject(archiverRef);
        return archiver == null ? null : archiver.getAssociationColor(attributeName);
    }

    @Override
    public String getName() {
        Archiver archiver = ObjectUtils.recoverObject(archiverRef);
        return archiver == null ? null : archiver.getName();
    }

    @Override
    public boolean isExported() {
        Archiver archiver = ObjectUtils.recoverObject(archiverRef);
        return archiver == null ? false : archiver.isExported();
    }

    @Override
    public boolean isDedicated() {
        Archiver archiver = ObjectUtils.recoverObject(archiverRef);
        return archiver == null ? false : archiver.isDedicated();
    }

    @Override
    public void load() throws ArchivingException {
        Archiver archiver = ObjectUtils.recoverObject(archiverRef);
        if (archiver != null) {
            archiver.load();
        }
    }

}
