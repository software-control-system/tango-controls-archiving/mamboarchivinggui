package fr.soleil.mambo.api.archiving;

import java.awt.Color;

import fr.soleil.archiving.common.api.exception.ArchivingException;

// alternative for fr.soleil.archiving.hdbtdb.api.tools.Archiver
// TODO maybe use class with static methods only
// see org.tango.archiving.server.manager.TimeseriesAccess
public interface IArchiver {

    public Color getAssociationColor(final String attributeName) throws ArchivingException;

    public String getName();

    public boolean isExported();

    public boolean isDedicated();

    public void load() throws ArchivingException;

}
