package fr.soleil.mambo.api.archiving;

import java.awt.Color;
import java.util.Collection;
import java.util.HashSet;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.Archiver;
import fr.soleil.mambo.api.ApiConstants;

public class TTSArchiver implements IArchiver {

    private final String deviceName;
    private boolean dedicated, exported;
    private String[] reservedAttributes;

    public TTSArchiver(String deviceName) {
        this.deviceName = deviceName;
        reservedAttributes = ApiConstants.EMPTY;
    }

    private int nonDedicated(final String attributeName) {
        String[] tmp = this.reservedAttributes;
        Collection<String> attributesToDedicatedArchiver = new HashSet<>();
        if (tmp != null) {
            for (String att : tmp) {
                attributesToDedicatedArchiver.add(att);
            }
        }
        final int ret;
        if (attributesToDedicatedArchiver.isEmpty() || (attributeName == null)) {
            ret = Archiver.NON_DEDICATED_ARCHIVER_RIGHT;
        } else if (attributesToDedicatedArchiver.contains(attributeName)) {
            ret = Archiver.NON_DEDICATED_ARCHIVER_WRONG;
        } else {
            ret = Archiver.NON_DEDICATED_ARCHIVER_RIGHT;
        }
        return ret;
    }

    private int hasReservedAttribute(final String attributeName) throws ArchivingException {
        load();
        final int ret;
        if (!dedicated) {
            ret = nonDedicated(attributeName);
        } else if (reservedAttributes == null || reservedAttributes.length == 0) {
            ret = nonDedicated(attributeName);
        } else if (attributeName == null || attributeName.isEmpty()) {
            ret = Archiver.NON_DEDICATED_ARCHIVER_RIGHT;
        } else {
            boolean hasAttribute = false;
            for (final String nextAttribute : reservedAttributes) {
                if (nextAttribute == null) {
                    continue;
                }
                if (nextAttribute.equals(attributeName)) {
                    hasAttribute = true;
                    break;
                }
            }
            ret = hasAttribute ? Archiver.DEDICATED_ARCHIVER_RIGHT : Archiver.DEDICATED_ARCHIVER_WRONG;
        }
        return ret;
    }

    @Override
    public Color getAssociationColor(String attributeName) throws ArchivingException {
        final int colorCase = hasReservedAttribute(attributeName);
        Color color;
        switch (colorCase) {
            case Archiver.DEDICATED_ARCHIVER_RIGHT:
                color = Archiver.DARKER_GREEN;
                break;
            case Archiver.DEDICATED_ARCHIVER_WRONG:
                color = Archiver.DARKER_RED;
                break;
            default:
                color = Color.BLACK;
        }
        return color;
    }

    @Override
    public String getName() {
        return deviceName;
    }

    @Override
    public boolean isExported() {
        return exported;
    }

    public void setExported(boolean exported) {
        this.exported = exported;
    }

    @Override
    public boolean isDedicated() {
        return dedicated;
    }

    public void setDedicated(boolean dedicated) {
        this.dedicated = dedicated;
    }

    public String[] getReservedAttributes() {
        return reservedAttributes;
    }

    public void setReservedAttributes(String[] reservedAttributes) {
        this.reservedAttributes = reservedAttributes == null ? ApiConstants.EMPTY : reservedAttributes;
    }

    @Override
    public void load() throws ArchivingException {
        TTSArchivingManagerApi.updateArchiverInfo(deviceName, null);
        dedicated = TTSArchivingManagerApi.isDedicated(deviceName);
        reservedAttributes = TTSArchivingManagerApi.getReservedAttributes(deviceName);
    }

}
