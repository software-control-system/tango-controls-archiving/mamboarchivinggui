package fr.soleil.mambo.api.archiving;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.manager.IArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.tools.ArchivingMessConfig;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.ApiUtils;
import fr.soleil.mambo.api.db.IDataBaseAcess;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeTDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;

public class LegacyArchivingManagerApi implements IArchivingManagerApi {

    private static final int KEEPING_PERIOD = 259200000; // 3 days

    private final WeakReference<IDataBaseAcess> dbRef;
    private final IArchivingManagerApiRef manager;
    private final boolean historic;

    public LegacyArchivingManagerApi(IDataBaseAcess db, IArchivingManagerApiRef manager, boolean historic) {
        this.dbRef = db == null ? null : new WeakReference<>(db);
        this.manager = manager;
        this.historic = historic;
    }

    @Override
    public int getArchiverListSize() {
        return manager.getArchiverListSize();
    }

    @Override
    public String getStatus(String attributeName) throws ArchivingException {
        return manager.getStatus(attributeName);
    }

    @Override
    public void archivingStart(ArchivingConfigurationAttribute... attributes) throws ArchivingException {
        if ((attributes != null) && (attributes.length > 0)) {
            ArchivingMessConfig archivingMessConfig = null;
            for (final ArchivingConfigurationAttribute next : attributes) {
                final ArchivingConfigurationAttributeProperties nextProperties = next.getProperties();
                ArchivingConfigurationAttributeDBProperties nextDBProperties;
                if (historic) {
                    nextDBProperties = nextProperties.getHDBProperties();
                } else {
                    nextDBProperties = nextProperties.getTDBProperties();
                }
                final String nextDBDedicatedArchiver = nextDBProperties.getDedicatedArchiver();
                if (!nextDBProperties.isEmpty()) {
                    if (archivingMessConfig == null) {
                        archivingMessConfig = ArchivingMessConfig.basicObjectCreation();
                    }

                    final ArchivingConfigurationMode[] DBModes = nextDBProperties.getModes();

                    final AttributeLightMode attributeLightMode = new AttributeLightMode();
                    attributeLightMode.setAttributeCompleteName(next.getCompleteName());
                    Mode mode;
                    if (historic) {
                        mode = ArchivingConfigurationMode.buildCompleteHDBMode(DBModes);
                    } else {
                        mode = ArchivingConfigurationMode.buildCompleteTDBMode(DBModes,
                                ((ArchivingConfigurationAttributeTDBProperties) nextDBProperties).getExportPeriod(),
                                KEEPING_PERIOD);
                    }
                    attributeLightMode.setMode(mode);

                    archivingMessConfig.add(attributeLightMode);
                    archivingMessConfig.setDeviceInChargeForAttribute(attributeLightMode, nextDBDedicatedArchiver);
                } // end if (!nextDBProperties.isEmpty())
            } // end for (int i = 0; i < nbOfAttributes; i++)

            if (archivingMessConfig != null) {
                archivingMessConfig.setGrouped(false);
                manager.archivingStart(archivingMessConfig);
            }
        }
    }

    @Override
    public void archivingStop(String... attributeNameList) throws ArchivingException {
        manager.archivingStop(attributeNameList);
    }

    @Override
    public boolean isArchived(String attributeName) throws ArchivingException {
        return manager.isArchived(attributeName);
    }

    @Override
    public IDataBaseAcess getDataBase() {
        return ObjectUtils.recoverObject(dbRef);
    }

    @Override
    public boolean isFacility() {
        return manager.getFacility();
    }

    @Override
    public String[] getExportedArchivers() {
        return manager.getMExportedArchiverList();
    }

    @Override
    public String[] getArchivingMode(String attributeName) throws ArchivingException {
        return manager == null ? null
                : ApiUtils.cleanToStringArray(
                        ArchivingUtils.modeToStringArray(new ArrayList<>(), manager.getArchivingMode(attributeName)));
    }

    @Override
    public void archivingConfigure() throws ArchivingException {
        if (manager != null) {
            manager.archivingConfigure();
        }
    }

    @Override
    public void archivingConfigureWithoutArchiverListInit() throws ArchivingException {
        if (manager != null) {
            manager.archivingConfigureWithoutArchiverListInit();
        }
    }

}
