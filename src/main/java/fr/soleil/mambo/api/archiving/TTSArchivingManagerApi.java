package fr.soleil.mambo.api.archiving;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.esrf.TangoApi.Group.Group;
import fr.esrf.TangoApi.Group.GroupAttrReply;
import fr.esrf.TangoApi.Group.GroupAttrReplyList;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.GetArchivingConf;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.mambo.api.ApiConstants;
import fr.soleil.mambo.api.ApiUtils;
import fr.soleil.mambo.api.db.IDataBaseAcess;
import fr.soleil.mambo.api.db.TTSDataBaseAccess;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttribute;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeDBProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationAttributeProperties;
import fr.soleil.mambo.data.archiving.ArchivingConfigurationMode;
import fr.soleil.tango.archiving.TangoArchivingSystemConfigurationBuilder;
import fr.soleil.tango.archiving.TangoArchivingSystemConfigurationService;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.build.TangoArchivingServicesBuilder;
import fr.soleil.tango.archiving.config.AttributeConfig;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfigs;
import fr.soleil.tango.archiving.infra.tango.TangoArchiverProperties;
import fr.soleil.tango.archiving.services.TangoArchivingConfigService;

// see org.tango.archiving.server.manager.TimeseriesAccess
public class TTSArchivingManagerApi implements IArchivingManagerApi {

    private final static String IS_DEDICATED = "IsDedicated";
    private final static String RESERVED_ATTRIBUTES = "ReservedAttributes";
    private static final String DOUBLE_SLASH = "//";
    private static final String GROUP_NAME = "Check-Exported-TTS-Archivers";
    private static final String FAILED_TO_GET_ARCHIVER_LIST = "Failed to get archiver list: ";
    private static final String NO_ID_FOUND_FOR = "no id found for ";

    private static final Logger LOGGER = LoggerFactory.getLogger(TTSArchivingManagerApi.class);

    private static final ScheduledExecutorService EXECUTOR = Executors.newScheduledThreadPool(1);
    private static final Map<String, Boolean> DEDICATED_MAP = new ConcurrentHashMap<>();
    private static final Map<String, String[]> RESERVED_MAP = new ConcurrentHashMap<>();

    private static volatile ArchiverList archiverList;
    private static Future<?> archiverListRefresher;

    protected final TTSDataBaseAccess databaseManager;
    protected final TangoArchivingConfigService configService;
    protected final TangoArchivingSystemConfigurationService service;
    protected final String archiverClassName;

    public TTSArchivingManagerApi(TTSDataBaseAccess databaseManager, DataBaseParameters params,
            String archiverClassName) {
        this.databaseManager = databaseManager;
        this.archiverClassName = archiverClassName;
        DatabaseConnectionConfig config = new DatabaseConnectionConfig();
        config.setDbType(DatabaseConnectionConfig.DataBaseType.parseDataBaseType(params.getDbType().name()));
        config.setName(params.getName());
        config.setHost(params.getHost());
        config.setUser(params.getUser());
        config.setPassword(params.getPassword());
        // TANGOARCH-833: possibility to connect to another port
        config.setPort(ApiUtils.getProperty(ApiConstants.TTS_PORT_PROPERTY, ApiConstants.TTS_PORT_ENV, config.getPort()));
        config.setIdleTimeout(params.getInactivityTimeout());
        config.setMaxPoolSize(params.getMaxPoolSize());
        if (params.getHealthRegistry() != null) {
            config.setHealthRegistry(params.getHealthRegistry());
        }
        if (params.getMetricRegistry() != null) {
            config.setMetricRegistry(params.getMetricRegistry());
        }
        TangoArchiverProperties properties = new TangoArchiverProperties();
        properties.setArchiverClassName(archiverClassName);
        TangoArchivingSystemConfigurationService service;
        LOGGER.info("connecting to TTS {}", config);
        try {
            service = new TangoArchivingSystemConfigurationBuilder().build(config, properties);
        } catch (Exception e) {
            service = null;
            LOGGER.error("Failed to init TangoArchivingSystemConfigurationBuilder", e);
        }
        this.service = service;
        if (service != null) {
            try {
                service.updateArchiversList();
            } catch (Exception e) {
                LOGGER.error("Failed to update archivers list", e);
            }
        }
        TangoArchivingConfigService configService;
        try {
            configService = new TangoArchivingServicesBuilder().buildConfigFetcher(config);
        } catch (Exception e) {
            LOGGER.error("Failed to init TangoArchivingConfigService", e);
            configService = null;
        }
        this.configService = configService;
        if (databaseManager != null) {
            databaseManager.setApi(this);
        }
    }

    public static String[] getRunningArchivers() {
        ArchiverList archiverList = TTSArchivingManagerApi.archiverList;
        return archiverList == null ? ApiConstants.EMPTY : archiverList.getExportedArchivers();
    }

    public static String[] getDeadArchivers() {
        ArchiverList archiverList = TTSArchivingManagerApi.archiverList;
        return archiverList == null ? ApiConstants.EMPTY : archiverList.getUnexportedArchivers();
    }

    protected static void updateArchiverInfo(String device, Database database) {
        if (device != null) {
            Database db;
            try {
                db = database == null ? ApiUtil.get_db_obj() : database;
            } catch (DevFailed e) {
                db = null;
            }
            if (db != null) {
                Boolean dedicated;
                try {
                    DbDatum property = db.get_device_property(device, IS_DEDICATED);
                    dedicated = Boolean.valueOf(property.extractBoolean());
                } catch (Exception e) {
                    dedicated = Boolean.FALSE;
                }
                DEDICATED_MAP.put(device, dedicated);
                String[] reserved;
                try {
                    DbDatum property = db.get_device_property(device, RESERVED_ATTRIBUTES);
                    reserved = property.extractStringArray();
                } catch (Exception e) {
                    reserved = ApiConstants.EMPTY;
                }
                RESERVED_MAP.put(device, reserved);
            }
        }
    }

    protected static <D> D getData(Map<String, D> map, String key, D defaultValue) {
        D data;
        if ((map == null) || (key == null)) {
            data = defaultValue;
        } else {
            data = map.get(key);
            if (data == null) {
                data = defaultValue;
            }
        }
        return data;
    }

    public static boolean isDedicated(String archiver) {
        return getData(DEDICATED_MAP, archiver, Boolean.FALSE).booleanValue();
    }

    public static String[] getReservedAttributes(String archiver) {
        return getData(RESERVED_MAP, archiver, ApiConstants.EMPTY);
    }

    public TangoArchivingSystemConfigurationService getService() {
        return service;
    }

    @Override
    public int getArchiverListSize() {
        return service == null ? 0 : service.getArchivers().size();
    }

    @Override
    public String getStatus(String attributeName) throws ArchivingException {
        String status;
        if ((attributeName == null) || (service == null)) {
            status = null;
        } else {
            try {
                String archiver = service.getArchiverForAttribute(attributeName);
                final DeviceProxy proxy = DeviceProxyFactory.get(archiver);
                status = proxy.status();
            } catch (DevFailed df) {
                throw new ArchivingException(df);
            }
        }
        return status;
    }

    @Override
    public void archivingStart(ArchivingConfigurationAttribute... attributes) throws ArchivingException {
        if ((attributes != null) && (attributes.length > 0) && (service != null)) {
            ArchivingConfigs archivingConfigs = null;
            for (ArchivingConfigurationAttribute attribute : attributes) {
                if (attribute != null) {
                    ArchivingConfigurationAttributeProperties acProperties = attribute.getProperties();
                    if (acProperties != null) {
                        ArchivingConfigurationAttributeDBProperties dbProperties = acProperties.getTTSProperties();
                        if ((dbProperties != null) && !dbProperties.isEmpty()) {
                            ArchivingConfigurationMode[] dbModes = dbProperties.getModes();
                            InsertionModes modes = new InsertionModes();
                            boolean modeOk = false;
                            if (dbModes != null) {
                                for (ArchivingConfigurationMode acMode : dbModes) {
                                    int type = acMode.getType();
                                    Mode mode = acMode.getMode();
                                    if (mode != null) {
                                        switch (type) {
                                            case ArchivingConfigurationMode.TYPE_A:
                                                ModeAbsolu abs = mode.getModeA();
                                                modes.setAbsolute(true);
                                                modes.setPeriodAbsolute(abs.getPeriod());
                                                modes.setSlowDriftAbsolute(abs.isSlow_drift());
                                                modes.setDecreaseDeltaAbsolute(abs.getValInf());
                                                modes.setIncreaseDeltaAbsolute(abs.getValSup());
                                                modeOk = true;
                                                break;
                                            case ArchivingConfigurationMode.TYPE_C:
                                                // ModeCalcul not managed in TTS (it was never really managed by mambo)
                                                break;
                                            case ArchivingConfigurationMode.TYPE_D:
                                                ModeDifference diff = mode.getModeD();
                                                modes.setDifference(true);
                                                modes.setPeriodDifference(diff.getPeriod());
                                                modeOk = true;
                                                break;
                                            case ArchivingConfigurationMode.TYPE_E:
                                                modes.setEvent(true);
                                                modeOk = true;
                                                break;
                                            case ArchivingConfigurationMode.TYPE_P:
                                                ModePeriode period = mode.getModeP();
                                                modes.setPeriodic(true);
                                                modes.setPeriodPeriodic(period.getPeriod());
                                                modeOk = true;
                                                break;
                                            case ArchivingConfigurationMode.TYPE_R:
                                                ModeRelatif rel = mode.getModeR();
                                                modes.setRelative(true);
                                                modes.setPeriodRelative(rel.getPeriod());
                                                modes.setSlowDriftRelative(rel.isSlow_drift());
                                                modes.setDecreasePercentRelative(rel.getPercentInf());
                                                modes.setIncreasePercentRelative(rel.getPercentSup());
                                                modeOk = true;
                                                break;
                                            case ArchivingConfigurationMode.TYPE_T:
                                                ModeSeuil threshold = mode.getModeT();
                                                modes.setThreshold(true);
                                                modes.setPeriodThreshold(threshold.getPeriod());
                                                modes.setMinThresholdValue(threshold.getThresholdInf());
                                                modes.setMaxThresholdValue(threshold.getThresholdSup());
                                                modeOk = true;
                                                break;
                                        }
                                    } // end if (mode != null)
                                } // end for (ArchivingConfigurationMode acMode : dbModes)
                            } // end if (dbModes != null)
                            if (modeOk) {
                                if (archivingConfigs == null) {
                                    archivingConfigs = new ArchivingConfigs();
                                }
                                AttributeConfig attributeConfig = new AttributeConfig();
                                attributeConfig.setFullName(attribute.getCompleteName());
                                ArchivingConfig archivingConfig = new ArchivingConfig();
                                String archiver = dbProperties.getDedicatedArchiver();
                                if (archiver == null) {
                                    archiver = ObjectUtils.EMPTY_STRING;
                                } else {
                                    archiver = archiver.trim();
                                }
                                archivingConfig.setArchiver(archiver);
                                archivingConfig.setAttributeConfig(attributeConfig);
                                archivingConfig.setModes(modes);
                                archivingConfigs.addConfiguration(archivingConfig);
                            } // end if (modeOk)
                        } // end if ((dbProperties != null) && !dbProperties.isEmpty())
                    } // end if (acProperties != null)
                } // end if (attribute != null)
            } // end for (ArchivingConfigurationAttribute attribute : attributes)
            if (archivingConfigs != null) {
                service.startArchiving(archivingConfigs);
            }
        } // end if ((attributes != null) && (attributes.length > 0) && (service != null))
    }

    @Override
    public void archivingStop(String... attributeNameList) throws ArchivingException {
        if (service != null) {
            service.stopArchiving(attributeNameList);
        }
    }

    @Override
    public boolean isArchived(String attributeName) throws ArchivingException {
        boolean archived = configService == null ? false : configService.isArchived(attributeName);
        return archived;
    }

    @Override
    public IDataBaseAcess getDataBase() {
        return databaseManager;
    }

    @Override
    public boolean isFacility() {
        boolean facility;
        try {
            facility = GetArchivingConf.getFacility(archiverClassName);
        } catch (ArchivingException e) {
            facility = false;
        }
        return facility;
    }

    @Override
    public String[] getExportedArchivers() {
        return getRunningArchivers();
    }

    public InsertionModes getInsertionModes(String attributeName) throws ArchivingException {
        OptionalInt id;
        if (configService == null) {
            id = null;
        } else {
            id = configService.getAttributeID(attributeName);
        }
        if (id == null || !id.isPresent()) {
            throw new ArchivingException(NO_ID_FOUND_FOR + attributeName);
        }
        Optional<InsertionModes> modes = configService.getCurrentInsertionModes(id.getAsInt());
        return modes == null || !modes.isPresent() ? null : modes.get();
    }

    @Override
    public String[] getArchivingMode(String attributeName) throws ArchivingException {
        InsertionModes modes = getInsertionModes(attributeName);
        return ApiUtils.cleanToStringArray(ArchivingUtils.insertionModesToStringArray(new ArrayList<>(), modes));
    }

    @Override
    public void archivingConfigure() throws ArchivingException {
        // update archiver list
        if ((archiverListRefresher != null) && (!archiverListRefresher.isCancelled())) {
            archiverListRefresher.cancel(true);
        }
        archiverListRefresher = EXECUTOR.submit(new ArchiverListRefresher());
    }

    @Override
    public void archivingConfigureWithoutArchiverListInit() throws ArchivingException {
        // nothing to do: database connection is done at object creation
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private static class ArchiverList {

        private final String[] exportedArchivers, unexportedArchivers;

        public ArchiverList(String[] exportedArchivers, String[] unexportedArchivers) {
            super();
            this.exportedArchivers = exportedArchivers;
            this.unexportedArchivers = unexportedArchivers;
        }

        public String[] getExportedArchivers() {
            return exportedArchivers;
        }

        public String[] getUnexportedArchivers() {
            return unexportedArchivers;
        }

    }

    // code based on fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRef.initDbArchiverList()
    private class ArchiverListRefresher implements Runnable {

        protected String getEffectiveDeviceName(String deviceName, boolean facility, Database db) throws DevFailed {
            return (facility ? DOUBLE_SLASH + db.get_tango_host() + TangoDeviceHelper.SLASH : ObjectUtils.EMPTY_STRING)
                    + deviceName;
        }

        @Override
        public void run() {
            try {
                final boolean facility = isFacility();
                final Database db = ApiUtil.get_db_obj();
                Collection<String> runningArchivers = new ArrayList<>(), deadArchivers = new ArrayList<>();
                String[] exported = TangoDeviceHelper.getExportedDevicesOfClass(ApiConstants.TTS_DEVICE_CLASS, db);
                String[] unexported = TangoDeviceHelper.getAllDevicesOfClass(ApiConstants.TTS_DEVICE_CLASS, db);
                Collection<String> devices = new ArrayList<>(unexported.length);
                for (String archiver : unexported) {
                    deadArchivers.add(getEffectiveDeviceName(archiver, facility, db));
                    devices.add(archiver);
                }
                Group group = new Group(GROUP_NAME);
                group.add(exported);
                GroupAttrReplyList result = group.read_attribute(TangoAttributeHelper.STATE, false);
                for (GroupAttrReply reply : result) {
                    try {
                        reply.get_data().extractDevState();
                        String device = getEffectiveDeviceName(reply.dev_name(), facility, db);
                        runningArchivers.add(device);
                        deadArchivers.remove(device);
                        updateArchiverInfo(device, db);
                    } catch (DevFailed e) {
                        // nothing to do: deadArchivers already contains device name and runningArchivers doesn't
                    }
                }
                Collection<String> toRemove = new ArrayList<>(DEDICATED_MAP.keySet());
                toRemove.removeAll(devices);
                for (String device : toRemove) {
                    DEDICATED_MAP.remove(device);
                }
                toRemove.clear();
                toRemove.addAll(RESERVED_MAP.keySet());
                toRemove.removeAll(devices);
                for (String device : toRemove) {
                    RESERVED_MAP.remove(device);
                }
                toRemove.clear();
                devices.clear();
                exported = runningArchivers.toArray(new String[runningArchivers.size()]);
                runningArchivers.clear();
                unexported = deadArchivers.toArray(new String[deadArchivers.size()]);
                deadArchivers.clear();
                ArchiverList current = archiverList;
                if ((current == null) || (!ObjectUtils.sameObject(exported, current.getExportedArchivers()))
                        || (!ObjectUtils.sameObject(unexported, current.getUnexportedArchivers()))) {
                    archiverList = new ArchiverList(exported, unexported);
                }
            } catch (final DevFailed devFailed) {
                LOGGER.error(FAILED_TO_GET_ARCHIVER_LIST + TangoExceptionHelper.getErrorMessage(devFailed), devFailed);
            }
        }
    }

}
