package fr.soleil.mambo.bean.manager;

import java.awt.Dimension;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.ImageIcon;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.docking.ADockingManager;
import fr.soleil.docking.infonode.InfoNodeDockingManager;
import fr.soleil.docking.infonode.view.InfoNodeView;
import fr.soleil.docking.infonode.view.InfoNodeViewFactory;
import fr.soleil.docking.perspective.PerspectiveFactory;
import fr.soleil.docking.view.IView;
import fr.soleil.docking.view.IViewFactory;
import fr.soleil.mambo.Mambo;
import fr.soleil.mambo.actions.archiving.ArchivingTransferAction;
import fr.soleil.mambo.actions.view.CloseSelectedVCAction;
import fr.soleil.mambo.actions.view.OpenSelectedVCEditDialogModifyAction;
import fr.soleil.mambo.actions.view.OpenVCEditDialogNewAction;
import fr.soleil.mambo.actions.view.SelectedVCHideAction;
import fr.soleil.mambo.actions.view.SelectedVCHideGeneralAction;
import fr.soleil.mambo.actions.view.SelectedVCRefreshAction;
import fr.soleil.mambo.actions.view.SelectedVCVariationAction;
import fr.soleil.mambo.actions.view.VCTabViewListener;
import fr.soleil.mambo.actions.view.listeners.IViewConfigurationBeanListener;
import fr.soleil.mambo.bean.view.ViewConfigurationBean;
import fr.soleil.mambo.components.view.OpenedVCComboBox;
import fr.soleil.mambo.containers.view.ViewSelectionPanel;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.datasources.file.IViewConfigurationManager;
import fr.soleil.mambo.datasources.file.ViewConfigurationManagerFactory;
import fr.soleil.mambo.event.ViewConfigurationBeanEvent;
import fr.soleil.mambo.options.Options;
import fr.soleil.mambo.tools.Messages;
import net.infonode.docking.RootWindow;
import net.infonode.docking.properties.RootWindowProperties;

public class ViewConfigurationBeanManager implements IViewConfigurationBeanListener {

    private static final ViewConfigurationBeanManager instance = new ViewConfigurationBeanManager();
    public static final int DEFAULT_MAXIMUM_DISPLAYED_VIEWS = 2;
    private static final ImageIcon MODIFIED_VC_ICON = new ImageIcon(Mambo.class.getResource("icons/star_red.gif"));

    private final HashMap<ViewConfiguration, ViewConfigurationBean> beanMap;
    private ViewConfiguration selectedConfiguration;
    private final ADockingManager dockingManager;
    private final LinkedList<ViewConfiguration> displayedConfigurations;
    private int maximumDisplayedViews;
    private final IViewFactory viewFactory;

    private final VCTabViewListener defaultTabViewListener;

    public static ViewConfigurationBeanManager getInstance() {
        return instance;
    }

    private ViewConfigurationBeanManager() {
        super();
        defaultTabViewListener = new VCTabViewListener();
        maximumDisplayedViews = Options.getInstance().getVcOptions().getMaxDisplayedViews();
        viewFactory = new InfoNodeViewFactory();
        dockingManager = new InfoNodeDockingManager(viewFactory, new PerspectiveFactory());
        GUIUtilities.setObjectBackground(dockingManager.getDockingArea(), GUIUtilities.VIEW_COLOR);
        // We force preferred size in order not to have a too big area.
        dockingManager.getDockingArea().setPreferredSize(new Dimension(300, 300));
        // XXX not a clean code. Find a better way to do this
        RootWindowProperties properties = ((RootWindow) dockingManager.getDockingArea()).getRootWindow()
                .getRootWindowProperties();
        properties.getWindowAreaProperties().setBackgroundColor(GUIUtilities.getViewColor());
        properties.getTabWindowProperties().getMaximizeButtonProperties().setVisible(false);
        properties.getTabWindowProperties().getMinimizeButtonProperties().setVisible(false);
        properties.getTabWindowProperties().getCloseButtonProperties().setVisible(false);
        beanMap = new HashMap<ViewConfiguration, ViewConfigurationBean>();
        displayedConfigurations = new LinkedList<ViewConfiguration>();
        selectedConfiguration = null;
        maximumDisplayedViews = DEFAULT_MAXIMUM_DISPLAYED_VIEWS;
    }

    public ViewConfiguration getSelectedConfiguration() {
        return selectedConfiguration;
    }

    public void setSelectedConfiguration(final ViewConfiguration selectedConfiguration) {
        if (selectedConfiguration != null) {
            InfoNodeView view = (InfoNodeView) viewFactory.getView(selectedConfiguration);
            if (selectedConfiguration != this.selectedConfiguration) {
                InfoNodeView previousView = null;
                if (this.selectedConfiguration != null) {
                    previousView = (InfoNodeView) viewFactory.getView(this.selectedConfiguration);
                    // beanMap.get(this.selectedConfiguration).
                }
                this.selectedConfiguration = selectedConfiguration;
                ViewConfigurationBean bean = null;

                synchronized (displayedConfigurations) {
                    displayedConfigurations.remove(selectedConfiguration);
                    displayedConfigurations.addFirst(selectedConfiguration);
                    reduceViewCountIfNecessary();
                }
                bean = beanMap.get(selectedConfiguration);
                if (bean == null) {
                    bean = new ViewConfigurationBean(selectedConfiguration);
                    bean.addViewConfigurationBeanListener(this);
                    beanMap.put(selectedConfiguration, bean);
                }
                bean.enableDisplayPanel(true);
                bean.refreshMainUI();
                bean.updateHideShowPanel();

                if (view == null) {
                    ImageIcon icon = null;
                    if (selectedConfiguration.isModified()) {
                        icon = MODIFIED_VC_ICON;
                    }
                    view = new InfoNodeView(selectedConfiguration.getDisplayableTitle(), icon,
                            bean.getViewDisplayPanel(), selectedConfiguration);
                    GUIUtilities.setObjectBackground(view, GUIUtilities.VIEW_COLOR);
                    // XXX not a clean code. Find a better way to do this
                    view.addViewListener(defaultTabViewListener);
                    viewFactory.addView(view);
                }
                ViewSelectionPanel.getInstance().updateForceExport();
                bean.getViewDisplayPanel().grabFocus();
                if (previousView != null) {
                    previousView.getWindowProperties().getTabProperties().getTitledTabProperties().getNormalProperties()
                            .getComponentProperties().setBackgroundColor(GUIUtilities.getViewColor());
                    previousView.getWindowProperties().getTabProperties().getTitledTabProperties()
                            .getHighlightedProperties().getComponentProperties()
                            .setBackgroundColor(GUIUtilities.getViewColor());
                    previousView.getWindowProperties().setCloseEnabled(true);
                    previousView.getWindowProperties().setMinimizeEnabled(true);
                    beanMap.get(previousView.getId()).enableDisplayPanel(false);
                }
            }
            if (view != null) {
                view.getWindowProperties().getTabProperties().getTitledTabProperties().getNormalProperties()
                        .getComponentProperties().setBackgroundColor(GUIUtilities.getViewSelectionColor());
                view.getWindowProperties().getTabProperties().getTitledTabProperties().getHighlightedProperties()
                        .getComponentProperties().setBackgroundColor(GUIUtilities.getViewSelectionColor());
                view.getViewProperties().setTitle(selectedConfiguration.getDisplayableTitle());
                view.getWindowProperties().setCloseEnabled(false);
                view.getWindowProperties().setMinimizeEnabled(false);
                // view.getWindowProperties().getTabProperties()
                // .getTitledTabProperties().setEnabled(false);
                view.restore();
                view.makeVisible();
            }
        }
    }

    public void setVCTabsEnabled(final boolean enable) {
        OpenedVCComboBox.getInstance().setEnabled(enable);
        CloseSelectedVCAction.getInstance().setEnabled(enable);
        OpenVCEditDialogNewAction.getInstance().setEnabled(enable);
        final Collection<IView> views = viewFactory.getViews();
        for (IView view : views) {
            if (view instanceof InfoNodeView) {
                InfoNodeView infoNodeView = (InfoNodeView) view;
                if (enable) {
                    infoNodeView.addViewListener(defaultTabViewListener);
                } else {
                    infoNodeView.removeViewListener(defaultTabViewListener);
                }
                if (infoNodeView.getId() != selectedConfiguration) {
                    infoNodeView.getWindowProperties().getTabProperties().getTitledTabProperties().setEnabled(enable);
                    infoNodeView.getWindowProperties().getTabProperties().getTitledTabProperties().setFocusable(enable);
                }
            }
        }
    }

    public void newConfiguration() {
        final ViewConfigurationBean bean = new ViewConfigurationBean(null);
        bean.editViewConfiguration();
        if (bean.getViewConfiguration() != null) {
            bean.addViewConfigurationBeanListener(this);
            beanMap.put(bean.getViewConfiguration(), bean);
            setSelectedConfiguration(bean.getViewConfiguration());
        }
    }

    public void removeConfiguration(final ViewConfiguration viewConfiguration) {
        if (viewConfiguration != null) {
            ViewConfigurationBean bean = beanMap.get(viewConfiguration);
            if (bean != null) {
                bean.removeViewConfigurationBeanListener(this);
            }
            beanMap.remove(viewConfiguration);
            displayedConfigurations.remove(viewConfiguration);
            viewFactory.removeView(viewConfiguration);
        }
    }

    public void clean() {
        beanMap.clear();
        displayedConfigurations.clear();
        setSelectedConfiguration(null);
    }

    public ViewConfigurationBean getBeanFor(final ViewConfiguration configuration) {
        if (configuration == null) {
            return null;
        } else {
            return beanMap.get(configuration);
        }
    }

    public void saveVC(final ViewConfiguration viewConfiguration, final boolean isSaveAs) {
        final IViewConfigurationManager manager = ViewConfigurationManagerFactory.getCurrentImpl();
        if (viewConfiguration != null) {
            viewConfiguration.save(manager, isSaveAs);
            final ViewConfigurationBean bean = beanMap.get(viewConfiguration);
            if (bean != null) {
                bean.getGeneralPanel().setPath(viewConfiguration.getPath());
            }
        }
        refreshIcon(viewConfiguration);
    }

    public ADockingManager getDockingManager() {
        return dockingManager;
    }

    public int getMaximumDisplayedViews() {
        return maximumDisplayedViews;
    }

    public void setMaximumDisplayedViews(final int maximumDisplayedViews) {
        this.maximumDisplayedViews = maximumDisplayedViews;
        reduceViewCountIfNecessary();
    }

    public void refreshTitle(final ViewConfiguration conf) {
        if (conf != null) {
            final InfoNodeView view = (InfoNodeView) viewFactory.getView(conf);
            if (view != null) {
                view.getViewProperties().setTitle(conf.getDisplayableTitle());
            }
        }
    }

    public void refreshIcon(final ViewConfiguration conf) {
        if (conf != null) {
            ImageIcon icon = null;
            if (conf.isModified()) {
                icon = MODIFIED_VC_ICON;
            }
            final InfoNodeView view = (InfoNodeView) viewFactory.getView(conf);
            if (view != null) {
                view.getViewProperties().setIcon(icon);
            }
        }
    }

    private void reduceViewCountIfNecessary() {
        synchronized (displayedConfigurations) {
            while (displayedConfigurations.size() > maximumDisplayedViews) {
                viewFactory.removeView(displayedConfigurations.getLast());
                displayedConfigurations.removeLast();
            }
        }
    }

    @Override
    public void configurationDateChanged(final ViewConfigurationBeanEvent event) {
        if (event != null && event.getSource() != null) {
            refreshIcon(event.getSource().getViewConfiguration());
        }
        ViewSelectionPanel.getInstance().repaint();
    }

    @Override
    public void configurationStructureChanged(final ViewConfigurationBeanEvent event) {
        if (event != null && event.getSource() != null) {
            ViewSelectionPanel.getInstance().updateForceExport();
            refreshTitle(event.getSource().getViewConfiguration());
            refreshIcon(event.getSource().getViewConfiguration());
        }
    }

    @Override
    public void visibilityChanged(final ViewConfigurationBeanEvent event) {
        final ViewConfigurationBean bean = event.getSource();
        String name;
        if (bean.isShowingTreePanel()) {
            name = Messages.getMessage("VIEW_ACTION_HIDE_TREE_VIEW");
        } else {
            name = Messages.getMessage("VIEW_ACTION_SHOW_TREE_VIEW");
        }
        SelectedVCHideAction.setName(name);

        if (bean.isShowingGeneralPanel()) {
            name = Messages.getMessage("VIEW_ACTION_HIDE_GENERAL_VIEW");
        } else {
            name = Messages.getMessage("VIEW_ACTION_SHOW_GENERAL_VIEW");
        }
        SelectedVCHideGeneralAction.setName(name);
    }

    @Override
    public void refreshChanged(final ViewConfigurationBeanEvent event, final boolean refreshing) {
        if (event != null && event.getSource() != null) {
            setVCTabsEnabled(!refreshing);
            SelectedVCRefreshAction.getInstance().setEnabled(!refreshing);
            final SelectedVCVariationAction selectedVCVariationAction = SelectedVCVariationAction.getCurrentInstance();
            if (selectedVCVariationAction != null) {
                selectedVCVariationAction.setEnabled(!refreshing);
            }
            final OpenSelectedVCEditDialogModifyAction openSelectedVCEditDialogModifyAction = OpenSelectedVCEditDialogModifyAction
                    .getCurrentInstance();
            if (openSelectedVCEditDialogModifyAction != null) {
                openSelectedVCEditDialogModifyAction.setEnabled(!refreshing);
            }
            ArchivingTransferAction.getInstance().setEnabled(!refreshing);
        }
    }
}
