package fr.soleil.mambo.bean.view;

import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.WeakHashMap;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Family;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;
import fr.soleil.mambo.actions.view.listeners.IDateRangeBoxListener;
import fr.soleil.mambo.actions.view.listeners.IViewConfigurationBeanListener;
import fr.soleil.mambo.components.view.OpenedVCComboBox;
import fr.soleil.mambo.components.view.VCPopupMenu;
import fr.soleil.mambo.containers.MamboFrame;
import fr.soleil.mambo.containers.view.ViewAttributesPanel;
import fr.soleil.mambo.containers.view.ViewGeneralPanel;
import fr.soleil.mambo.containers.view.dialogs.AttributesPlotPropertiesPanel;
import fr.soleil.mambo.containers.view.dialogs.ChartGeneralTabbedPane;
import fr.soleil.mambo.containers.view.dialogs.GeneralTab;
import fr.soleil.mambo.containers.view.dialogs.VCEditDialog;
import fr.soleil.mambo.containers.view.dialogs.VCVariationDialog;
import fr.soleil.mambo.containers.view.dialogs.VCVariationEditCopyDialog;
import fr.soleil.mambo.data.view.ExpressionAttribute;
import fr.soleil.mambo.data.view.ViewConfiguration;
import fr.soleil.mambo.data.view.ViewConfigurationAttribute;
import fr.soleil.mambo.data.view.ViewConfigurationAttributePlotProperties;
import fr.soleil.mambo.data.view.ViewConfigurationAttributes;
import fr.soleil.mambo.data.view.ViewConfigurationData;
import fr.soleil.mambo.event.DateRangeBoxEvent;
import fr.soleil.mambo.event.ViewConfigurationBeanEvent;
import fr.soleil.mambo.models.VCAttributesTreeModel;
import fr.soleil.mambo.models.VCPossibleAttributesTreeModel;
import fr.soleil.mambo.tools.Messages;

public class ViewConfigurationBean implements IDateRangeBoxListener, ItemListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewConfigurationBean.class);

    private ViewAttributesPanel attributesPanel;
    private VCEditDialog editDialog;

    private VCAttributesTreeModel editingModel;
    private ViewConfiguration editingViewConfiguration;
    private ViewGeneralPanel generalPanel;

    private boolean isShowingTreePanel;
    private final Set<IViewConfigurationBeanListener> listeners;
    private final VCPopupMenu popupMenu1, popupMenu2;

    private VCAttributesTreeModel validatedModel;
    private VCVariationDialog variationDialog;
    private VCVariationEditCopyDialog variationEditCopyDialog;

    private VCPossibleAttributesTreeModel vcPossibleAttributesTreeModel;

    private ViewConfiguration viewConfiguration;

    private JPanel viewDisplayPanel;

    private ProgressDialog waitingDialog;

    public ViewConfigurationBean(final ViewConfiguration viewConfiguration) {
        super();
        this.viewConfiguration = viewConfiguration;
        editingViewConfiguration = null;
        listeners = Collections.newSetFromMap(new WeakHashMap<>());
        waitingDialog = null;
        popupMenu1 = new VCPopupMenu(this);
        popupMenu2 = new VCPopupMenu(this);
        initUI();
    }

    public ProgressDialog getWaitingDialog() {
        if (waitingDialog == null) {
            Window owner = WindowSwingUtils.getWindowForComponent(attributesPanel);
            if (owner == null) {
                owner = MamboFrame.getInstance();
            }
            waitingDialog = new ProgressDialog(owner);
        }
        return waitingDialog;
    }

    public void cancelEdition() {
        editingViewConfiguration = null;
        refreshEditingUI();
    }

    @Override
    public void dateRangeBoxChanged(final DateRangeBoxEvent event) {
        if (viewConfiguration != null && viewConfiguration.getData() != null && event != null
                && event.getSource() != null) {
            // Some changes occurred in DateRangeBox
            // --> update ViewConfigurationData
            viewConfiguration.getData().updateDateRangeBox(event.getSource());
            viewConfiguration.setModified(true);
            fireConfigurationChangeEvent();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (viewConfiguration != null && viewConfiguration.getData() != null && e != null && e.getSource() != null
                && ((e.getStateChange() == ItemEvent.SELECTED || e.getStateChange() == ItemEvent.DESELECTED))) {
            // Some changes occurred in automatic averaging
            // --> update ViewConfigurationData
            boolean autoAveraging = e.getStateChange() == ItemEvent.SELECTED;
            if (viewConfiguration.getData().isAutoAveraging() != autoAveraging) {
                viewConfiguration.getData().setAutoAveraging(autoAveraging);
                viewConfiguration.setModified(true);
                fireConfigurationChangeEvent();
            }
        }
    }

    public void editViewConfiguration() {
        boolean newVC;
        if (viewConfiguration == null) {
            editingViewConfiguration = new ViewConfiguration();
            newVC = true;
        } else {
            editingViewConfiguration = viewConfiguration.cloneVC();
            newVC = false;
        }
        for (int i = 1; i < getEditDialog().getVcCustomTabbedPane().getTabCount(); i++) {
            getEditDialog().getVcCustomTabbedPane().setEnabledAt(i, false);
        }
        refreshEditingUI();
        if (newVC) {
            // TANGOARCH-808: select 1h by default
            JComboBox<String> dateRangeBox = getEditDialog().getGeneralTab().getDateRangeBox().getDateRangeComboBox();
            if ((dateRangeBox != null) && (dateRangeBox.getItemCount() > 1)) {
                dateRangeBox.setSelectedIndex(1);
            }
        }
        getEditDialog().getVcCustomTabbedPane().setEnabledAt(0, true);
        getEditDialog().getVcCustomTabbedPane().setSelectedIndex(0);
        getEditDialog().pack();
        getEditDialog().setLocationRelativeTo(viewDisplayPanel);
        Rectangle maxBounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        Rectangle bounds = getEditDialog().getBounds();
        if (!maxBounds.contains(bounds)) {
            Rectangle newBounds = new Rectangle(bounds);
            if (newBounds.x < maxBounds.x) {
                newBounds.x = maxBounds.x;
            }
            if (newBounds.y < maxBounds.y) {
                newBounds.y = maxBounds.y;
            }
            final int maxWidth = maxBounds.x + maxBounds.width;
            final int currentWidth = newBounds.x + newBounds.width;
            if (currentWidth > maxWidth) {
                newBounds.width -= currentWidth - maxWidth;
                if (newBounds.width < 50) {
                    newBounds.x = maxBounds.x;
                    newBounds.width = maxBounds.width;
                }
            }
            final int maxHeight = maxBounds.y + maxBounds.height;
            final int currentHeight = newBounds.y + newBounds.height;
            if (currentHeight > maxHeight) {
                newBounds.height -= currentHeight - maxHeight;
                if (newBounds.height < 50) {
                    newBounds.y = maxBounds.y;
                    newBounds.height = maxBounds.height;
                }
            }
            getEditDialog().setBounds(newBounds);
        }
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                getEditDialog().scrollBackTo0();
            }
        });
        getEditDialog().setVisible(true);
    }

    public void enableDisplayPanel(final boolean enabled) {
        if (viewDisplayPanel != null) {
            viewDisplayPanel.setEnabled(enabled);
            if (!enabled) {
                popupMenu1.setVisible(false);
            }
        }
        if (attributesPanel != null) {
            attributesPanel.getViewAttributesTreePanel().getViewActionPanel().setEnableAllComponent(enabled);
        }
    }

    public void addViewConfigurationBeanListener(final IViewConfigurationBeanListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    public void removeAllViewConfigurationBeanListeners() {
        synchronized (listeners) {
            listeners.clear();
        }
    }

    public void removeViewConfigurationBeanListener(final IViewConfigurationBeanListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    private void fireRefreshEvent(final boolean refreshing) {
        ViewConfigurationBeanEvent event = new ViewConfigurationBeanEvent(this);
        List<IViewConfigurationBeanListener> copy = new ArrayList<>();
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (IViewConfigurationBeanListener listener : copy) {
            listener.refreshChanged(event, refreshing);
        }
        copy.clear();
    }

    private void fireConfigurationChangeEvent() {
        ViewConfigurationBeanEvent event = new ViewConfigurationBeanEvent(this);
        List<IViewConfigurationBeanListener> copy = new ArrayList<>();
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (IViewConfigurationBeanListener listener : copy) {
            listener.configurationDateChanged(event);
        }
        copy.clear();
    }

    private void fireSelectedStructureEvent() {
        ViewConfigurationBeanEvent event = new ViewConfigurationBeanEvent(this);
        List<IViewConfigurationBeanListener> copy = new ArrayList<>();
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (IViewConfigurationBeanListener listener : copy) {
            listener.configurationStructureChanged(event);
        }
        copy.clear();
    }

    private void fireVisibilityEvent() {
        ViewConfigurationBeanEvent event = new ViewConfigurationBeanEvent(this);
        List<IViewConfigurationBeanListener> copy = new ArrayList<>();
        synchronized (listeners) {
            copy.addAll(listeners);
        }
        for (IViewConfigurationBeanListener listener : copy) {
            listener.visibilityChanged(event);
        }
        copy.clear();
    }

    public synchronized ViewAttributesPanel getAttributesPanel() {
        if (attributesPanel == null) {
            attributesPanel = new ViewAttributesPanel(this);
            isShowingTreePanel = true;
            attributesPanel.getViewAttributesTreePanel().getDateRangeBox().addDateRangeBoxListener(this);
            attributesPanel.getViewAttributesTreePanel().getAveragingOptionsPanel().addAutomaticAveragingListener(this);
            attributesPanel.getViewAttributesTreePanel().getTree().setComponentPopupMenu(popupMenu2);
            List<Domain> domains = getSelectedDomainsToPush();
            ViewConfigurationAttributes attributes = viewConfiguration.getAttributes();
            prepareGraphPanel(attributes, domains);
        }
        return attributesPanel;
    }

    public VCEditDialog getEditDialog() {
        if (editDialog == null) {
            editDialog = new VCEditDialog(this);
        }
        return editDialog;
    }

    public VCAttributesTreeModel getEditingModel() {
        return editingModel;
    }

    public ViewConfiguration getEditingViewConfiguration() {
        return editingViewConfiguration;
    }

    public synchronized ViewGeneralPanel getGeneralPanel() {
        if (generalPanel == null) {
            generalPanel = new ViewGeneralPanel();
        }
        return generalPanel;
    }

    public VCAttributesTreeModel getValidatedModel() {
        return validatedModel;
    }

    public VCVariationDialog getVariationDialog() {
        if (variationDialog == null) {
            variationDialog = new VCVariationDialog(this);
        }
        return variationDialog;
    }

    public VCVariationEditCopyDialog getVariationEditCopyDialog() {
        if (variationEditCopyDialog == null) {
            variationEditCopyDialog = new VCVariationEditCopyDialog(this);
        }
        return variationEditCopyDialog;
    }

    public VCPossibleAttributesTreeModel getVcPossibleAttributesTreeModel() {
        return vcPossibleAttributesTreeModel;
    }

    public ViewConfiguration getViewConfiguration() {
        return viewConfiguration;
    }

    public synchronized JPanel getViewDisplayPanel() {
        if (viewDisplayPanel == null) {
            viewDisplayPanel = new JPanel(new GridBagLayout());
            viewDisplayPanel.setComponentPopupMenu(popupMenu1);

            final GridBagConstraints generalConstraints = new GridBagConstraints();
            generalConstraints.fill = GridBagConstraints.HORIZONTAL;
            generalConstraints.gridx = 0;
            generalConstraints.gridy = 0;
            generalConstraints.weightx = 1;
            generalConstraints.weighty = 0;
            viewDisplayPanel.add(getGeneralPanel(), generalConstraints);
            final GridBagConstraints attributesConstraints = new GridBagConstraints();
            attributesConstraints.fill = GridBagConstraints.BOTH;
            attributesConstraints.gridx = 0;
            attributesConstraints.gridy = 1;
            attributesConstraints.weightx = 1;
            attributesConstraints.weighty = 1;
            viewDisplayPanel.add(getAttributesPanel(), attributesConstraints);
            GUIUtilities.setObjectBackground(viewDisplayPanel, GUIUtilities.VIEW_COLOR);
        }
        return viewDisplayPanel;
    }

    public void prepareComponentsReset() {
        fireRefreshEvent(true);
        getAttributesPanel().getViewAttributesGraphPanel().prepareComponentsReset();
        getAttributesPanel().getViewAttributesTreePanel().getViewActionPanel().refreshButtonDefaultColor();
        getAttributesPanel().getViewAttributesTreePanel().getViewActionPanel().putCancelButton();
    }

    public void resetComponents(Map<String, Object[]> attributes, Map<ViewConfigurationAttribute, Integer> dataTypes,
            int attrCount) {
        getAttributesPanel().getViewAttributesGraphPanel().setComponents(attributes, dataTypes, attrCount);
    }

    public Map<String, Object[]> splitAttributes(ViewConfigurationAttributes attributes) throws ArchivingException {
        return getAttributesPanel().getViewAttributesGraphPanel().splitAttributes(attributes);
    }

    public Map<ViewConfigurationAttribute, Integer> extractDataTypeMap(ViewConfigurationAttributes attributes)
            throws ArchivingException {
        return getAttributesPanel().getViewAttributesGraphPanel().extractDataTypeMap(attributes);
    }

    private void initUI() {
        Boolean historic = Boolean.TRUE;
        if (viewConfiguration != null && viewConfiguration.getData() != null) {
            historic = viewConfiguration.getData().isHistoric();
        }
        vcPossibleAttributesTreeModel = new VCPossibleAttributesTreeModel(historic);
        editingModel = new VCAttributesTreeModel(vcPossibleAttributesTreeModel);
        editingModel.setHistoric(historic, false);
        validatedModel = new VCAttributesTreeModel(vcPossibleAttributesTreeModel);
        validatedModel.setHistoric(historic, false);
    }

    public boolean isShowingGeneralPanel() {
        return generalPanel.isVisible();
    }

    public boolean isShowingTreePanel() {
        return isShowingTreePanel;
    }

    protected void prepareGraphPanel(ViewConfigurationAttributes attributes, List<Domain> domains) {
        Map<String, Object[]> splittedAttributes;
        try {
            splittedAttributes = splitAttributes(attributes);
        } catch (ArchivingException e) {
            splittedAttributes = new HashMap<>();
            LOGGER.error("Failed to split attributes during ViewAttributesPanel preparation", e);
        }
        Map<ViewConfigurationAttribute, Integer> dataTypes;
        try {
            dataTypes = extractDataTypeMap(attributes);
        } catch (ArchivingException e) {
            dataTypes = new HashMap<>();
            LOGGER.error("Failed to extract data types during ViewAttributesPanel preparation", e);
        }
        prepareGraphPanel(domains, splittedAttributes, dataTypes,
                attributes == null ? 0 : attributes.countAttributes());
    }

    public void prepareGraphPanel(List<Domain> domainList, Map<String, Object[]> attributes,
            Map<ViewConfigurationAttribute, Integer> dataTypes, int attrCount) {
        pushAttributeDomains(domainList);
        getAttributesPanel().getViewAttributesGraphPanel().clean();
        getAttributesPanel().getViewAttributesGraphPanel().initComponents(attributes, dataTypes, attrCount);
    }

    public void pushAttributesPlotProperties(final ViewConfigurationAttributePlotProperties plotProperties) {
        if (plotProperties != null) {
            final AttributesPlotPropertiesPanel plotPropertiesPanel = getEditDialog().getAttributesPlotPropertiesTab()
                    .getPropertiesPanel();
            plotPropertiesPanel.setViewConfigurationAttributePlotProperties(plotProperties);
        }
    }

    private void pushEditingAttributes() {
        ViewConfigurationAttributes attributes = null;
        TreeMap<String, ExpressionAttribute> expressions = null;
        if (editingViewConfiguration != null) {
            attributes = editingViewConfiguration.getAttributes();
            expressions = editingViewConfiguration.getExpressions();
        }
        // Save expanded Row
        Map<String, ViewConfigurationAttribute> attributesHash = null;
        if (attributes != null) {
            attributesHash = attributes.getAttributesCopy();
        }
        if (attributesHash == null) {
            attributesHash = new TreeMap<>();
        }
        List<Domain> domainList = new ArrayList<>();
        for (final Entry<String, ViewConfigurationAttribute> entry : attributesHash.entrySet()) {
            final String completeName = entry.getKey();
            final ViewConfigurationAttribute next = entry.getValue();
            final String domain_s = next.getDomainName();
            final String family_s = next.getFamilyName();
            final String member_s = next.getMemberName();
            final String attr_s = next.getName();
            final ViewConfigurationAttribute attr = new ViewConfigurationAttribute(completeName);
            attr.setName(attr_s);
            attr.setDevice(member_s);
            attr.setDomain(domain_s);
            attr.setFamily(family_s);
            final Domain domain = new Domain(domain_s);
            final Family family = new Family(family_s);
            final Member member = new Member(member_s);
            final Domain dom = Domain.hasDomain(domainList, domain_s);
            if (dom != null) {
                final Family fam = dom.getFamily(family_s);
                if (fam != null) {
                    final Member mem = fam.getMember(member_s);
                    if (mem != null) {
                        mem.addAttribute(attr);
                        fam.addMember(mem);
                        dom.addFamily(fam);
                        domainList = Domain.addDomain(domainList, dom);
                    } else {
                        member.addAttribute(attr);
                        fam.addMember(member);
                        dom.addFamily(fam);
                        domainList = Domain.addDomain(domainList, dom);
                    }
                } else {
                    member.addAttribute(attr);
                    family.addMember(member);
                    dom.addFamily(family);
                    domainList = Domain.addDomain(domainList, dom);
                }
            } else {
                member.addAttribute(attr);
                family.addMember(member);
                domain.addFamily(family);
                domainList = Domain.addDomain(domainList, domain);
            }
        }
        attributesHash.clear();
        editingModel.removeAll();
        editingModel.build(domainList);
        editingModel.reload();
        int size = 0;
        if (attributes != null) {
            size = attributes.countAttributes();
        }
        getEditDialog().getAttributesPlotPropertiesTab().getPropertiesPanel()
                .setColorIndex(AttributesPlotPropertiesPanel.getColorIndexForSize(size));
        getEditDialog().getAttributesPlotPropertiesTab().getPropertiesPanel().rotateCurveColor();
        size = 0;
        if (expressions != null) {
            size = expressions.size();
        }
        if (getEditDialog().isVisible()) {
            getEditDialog().repaint();
        }
    }

    private void pushEditingViewConfigurationData() {
        ViewConfigurationData data = null;
        if (editingViewConfiguration != null) {
            data = editingViewConfiguration.getData();
        }
        if (data != null) {
            final GeneralTab generalTab = getEditDialog().getGeneralTab();
            generalTab.setLastUpdateDate(data.getLastUpdateDate());
            generalTab.setCreationDate(data.getCreationDate());
            generalTab.setName(data.getName());
            generalTab.setSamplingType(data.getSamplingType());

            final boolean dynamic = data.isDynamicDateRange();
            if (dynamic) {
                final Timestamp[] range = generalTab.getDynamicStartAndEndDates();
                generalTab.setStartDate(range[0]);
                generalTab.setEndDate(range[1]);
            } else {
                generalTab.setStartDate(data.getStartDate());
                generalTab.setEndDate(data.getEndDate());
            }
            final boolean autoAveraging = data.isAutoAveraging();
            generalTab.setDynamicDateRange(dynamic);
            generalTab.setAutoAveraging(autoAveraging);
            generalTab.setDateRange(data.getDateRange());
            generalTab.setHistoric(data.isHistoric());
            generalTab.getDateRangeBox().setEnabledFields(
                    (data.isHistoric() == null) || data.isHistoric().booleanValue() || !data.isLongTerm());

            // we need to update the trees according to the "historic" state
            final VCAttributesTreeModel vCAttributesTreeModel = getEditDialog().getAttributesTab()
                    .getSelectedAttributesTree().getModel();
            vCAttributesTreeModel.setRootName(data.isHistoric());

            vcPossibleAttributesTreeModel.setHistoric(data.isHistoric());
            editingModel.setHistoric(data.isHistoric(), false);

            // General chart properties
            ChartProperties chartProperties = data.getChartProperties();
            if (chartProperties != null) {
                final ChartGeneralTabbedPane generalTabbedPane = getEditDialog().getGeneralTab()
                        .getChartGeneralTabbedPane();
                generalTabbedPane.setProperties(chartProperties);
            }
        }
    }

    public List<Domain> getSelectedDomainsToPush() {
        ViewConfigurationAttributes attributes = null;
        if (viewConfiguration != null) {
            attributes = viewConfiguration.getAttributes();
        }
        // Save expanded Row
        Map<String, ViewConfigurationAttribute> attributesHash = null;
        if (attributes != null) {
            attributesHash = attributes.getAttributesCopy();
        }
        if (attributesHash == null) {
            attributesHash = new TreeMap<>();
        }
        List<Domain> domainList = new ArrayList<>();
        for (final Entry<String, ViewConfigurationAttribute> entry : attributesHash.entrySet()) {
            final String completeName = entry.getKey();
            final ViewConfigurationAttribute next = entry.getValue();
            final String domain_s = next.getDomainName();
            final String family_s = next.getFamilyName();
            final String member_s = next.getMemberName();
            final String attr_s = next.getName();
            final ViewConfigurationAttribute attr = new ViewConfigurationAttribute(completeName);
            attr.setName(attr_s);
            attr.setDevice(member_s);
            attr.setDomain(domain_s);
            attr.setFamily(family_s);
            final Domain domain = new Domain(domain_s);
            final Family family = new Family(family_s);
            final Member member = new Member(member_s);
            final Domain dom = Domain.hasDomain(domainList, domain_s);
            if (dom != null) {
                final Family fam = dom.getFamily(family_s);
                if (fam != null) {
                    final Member mem = fam.getMember(member_s);
                    if (mem != null) {
                        mem.addAttribute(attr);
                        fam.addMember(mem);
                        dom.addFamily(fam);
                        domainList = Domain.addDomain(domainList, dom);
                    } else {
                        member.addAttribute(attr);
                        fam.addMember(member);
                        dom.addFamily(fam);
                        domainList = Domain.addDomain(domainList, dom);
                    }
                } else {
                    member.addAttribute(attr);
                    family.addMember(member);
                    dom.addFamily(family);
                    domainList = Domain.addDomain(domainList, dom);
                }
            } else {
                member.addAttribute(attr);
                family.addMember(member);
                domain.addFamily(family);
                domainList = Domain.addDomain(domainList, domain);
            }
        }
        attributesHash.clear();
        return domainList;
    }

    private void pushAttributeDomains(List<Domain> domainList) {
        validatedModel.removeAll();
        validatedModel.build(domainList);
        validatedModel.reload();
        getAttributesPanel().repaint();
    }

    private void pushSelectedViewConfigurationData(boolean computeDynamicDates) {
        ViewConfigurationData data = null;
        if (viewConfiguration != null) {
            data = viewConfiguration.getData();
        }
        if (data == null) {
            getGeneralPanel().setLastUpdateDate(null);
            getGeneralPanel().setCreationDate(null);
            getGeneralPanel().setName(null);
            getGeneralPanel().setPath(null);
        } else {
            if (!ObjectUtils.sameObject(validatedModel.isHistoric(), data.isHistoric())) {
                validatedModel.setHistoric(data.isHistoric(), false);
            }
            getGeneralPanel().setLastUpdateDate(data.getLastUpdateDate());
            getGeneralPanel().setCreationDate(data.getCreationDate());
            getGeneralPanel().setName(data.getName());
            getGeneralPanel().setPath(data.getPath());
        }
        // No need to listen to the DateRangeBox while updating it
        getAttributesPanel().getViewAttributesTreePanel().getDateRangeBox().removeDateRangeBoxListener(this);
        getAttributesPanel().getViewAttributesTreePanel().getAveragingOptionsPanel()
                .removeAutomaticAveragingListener(this);
        getAttributesPanel().getViewAttributesTreePanel().getDateRangeBox().update(data, computeDynamicDates);
        getAttributesPanel().getViewAttributesTreePanel().getAveragingOptionsPanel()
                .setAutoAveraging(data.isAutoAveraging());
        getAttributesPanel().getViewAttributesTreePanel().getDateRangeBox().addDateRangeBoxListener(this);
        getAttributesPanel().getViewAttributesTreePanel().getAveragingOptionsPanel()
                .addAutomaticAveragingListener(this);
    }

    public void refreshEditingUI() {
        pushEditingViewConfigurationData();
        pushEditingAttributes();
        if (editingViewConfiguration == null) {
            getEditDialog().getExpressionTab().getExpressionTree().getModel().setExpressionAttributes(new TreeMap<>());
        } else {
            getEditDialog().getExpressionTab().getExpressionTree().getModel()
                    .setExpressionAttributes(editingViewConfiguration.getExpressions());
        }
    }

    public void refreshMainUI() {
        refreshMainUI(true);
    }

    public void refreshMainUI(boolean forceBoxesToRecalculateDynamicDates) {
        pushSelectedViewConfigurationData(forceBoxesToRecalculateDynamicDates);
        // pushSelectedAttributes();
        if (viewConfiguration != null) {
            final OpenedVCComboBox openedVCComboBox = OpenedVCComboBox.getInstance();
            if (openedVCComboBox != null) {
                openedVCComboBox.selectElement(viewConfiguration);
            }
        }
        getAttributesPanel().getViewAttributesTreePanel().getTree().expandAll(true);
        getAttributesPanel().revalidate();
        getAttributesPanel().repaint();
        getViewDisplayPanel().revalidate();
        getViewDisplayPanel().repaint();
    }

    public void setEnableRefreshButton(final boolean enabled) {
        popupMenu1.setEnableRefreshItem(enabled);
        popupMenu2.setEnableRefreshItem(enabled);
        fireRefreshEvent(!enabled);
    }

    public void setShowGeneralPanel() {
        if (generalPanel != null) {
            String name;
            if (generalPanel.isVisible()) {
                name = Messages.getMessage("VIEW_ACTION_SHOW_GENERAL_VIEW");
                popupMenu1.setNameGeneralMenuItem(name);
                popupMenu2.setNameGeneralMenuItem(name);
                generalPanel.setVisible(false);
            } else {
                name = Messages.getMessage("VIEW_ACTION_HIDE_GENERAL_VIEW");
                popupMenu1.setNameGeneralMenuItem(name);
                popupMenu2.setNameGeneralMenuItem(name);
                generalPanel.setVisible(true);
            }
            fireVisibilityEvent();
        }
    }

    public void setShowTreePanelManagement() {
        if (attributesPanel != null) {
            String name;
            if (isShowingTreePanel) {
                isShowingTreePanel = false;
                name = Messages.getMessage("VIEW_ACTION_SHOW_TREE_VIEW");
                popupMenu1.setNameTreeMenuItem(name);
                popupMenu2.setNameTreeMenuItem(name);
                attributesPanel.hideTreePanel();
                popupMenu2.setVisible(false);
            } else {
                isShowingTreePanel = true;
                name = Messages.getMessage("VIEW_ACTION_HIDE_TREE_VIEW");
                popupMenu1.setNameTreeMenuItem(name);
                popupMenu2.setNameTreeMenuItem(name);
                attributesPanel.showTreePanel();
            }
            fireVisibilityEvent();
        }
    }

    public void updateHideShowPanel() {
        fireVisibilityEvent();
    }

    public void validateEdition() {
        getEditDialog().setVisible(false);
        if (editingViewConfiguration != null) {
            if (attributesPanel != null) {
                // clean panels to clean dao factory
                attributesPanel.getViewAttributesGraphPanel().clean();
                attributesPanel.repaint();
            }
            if (viewConfiguration == null) {
                viewConfiguration = editingViewConfiguration;
            } else {
                viewConfiguration.setData(editingViewConfiguration.getData());
                viewConfiguration.setAttributes(editingViewConfiguration.getAttributes());
                viewConfiguration.setExpressions(editingViewConfiguration.getExpressions());
                viewConfiguration.setPath(editingViewConfiguration.getPath());
                viewConfiguration.setModified(editingViewConfiguration.isModified());
            }
        }
        editingViewConfiguration = null;
        refreshMainUI();
        List<Domain> domains = getSelectedDomainsToPush();
        prepareGraphPanel(viewConfiguration == null ? null : viewConfiguration.getAttributes(), domains);
        domains.clear();
        fireSelectedStructureEvent();
        getAttributesPanel().getViewAttributesTreePanel().getViewActionPanel().refreshButtonChangeColor();
        cancelEdition();
    }

}
